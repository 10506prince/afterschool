//
//  UserAgreementViewController.m
//  AfterSchool
//
//  Created by lg on 15/12/19.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "UserAgreementViewController.h"
#import "NavigationBarView.h"
#import "UserAgreementRootView.h"

@interface UserAgreementViewController ()

@property (nonatomic, strong) UserAgreementRootView * rootView;

@end

@implementation UserAgreementViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initData];
    [self initUserInterface];
}

- (void)initData {

}

- (void)initUserInterface {
    [self.view addSubview:self.rootView];
}

- (UserAgreementRootView *)rootView {
    if (!_rootView) {
        _rootView = [[UserAgreementRootView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        [_rootView.navigationBarView.leftButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        
        NSString *file = [[NSBundle mainBundle] pathForResource:@"UserAgreement" ofType:@"txt"];
        
        NSFileHandle *fileHandle=[NSFileHandle fileHandleForReadingAtPath:file];
        NSData *data = [fileHandle readDataToEndOfFile];
        NSString *str = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        
        [_rootView.textView setText:str];
    }
    return _rootView;
}

- (void)backAction {
    [self.navigationController popViewControllerAnimated:YES];
}
@end



