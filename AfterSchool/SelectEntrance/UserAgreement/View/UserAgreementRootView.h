//
//  UserAgreementRootView.h
//  AfterSchool
//
//  Created by lg on 15/12/19.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavigationBarView.h"

@interface UserAgreementRootView : UIView

@property (nonatomic, strong) NavigationBarView *navigationBarView;

@property (nonatomic, strong) UITextView * textView;

@end
