//
//  UserAgreementRootView.m
//  AfterSchool
//
//  Created by lg on 15/12/19.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "UserAgreementRootView.h"

@implementation UserAgreementRootView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithRed:240/255.f green:239/255.f blue:244/255.f alpha:1];
        [self addSubview:self.textView];
        [self addSubview:self.navigationBarView];
    }
    
    return self;
}

- (NavigationBarView *)navigationBarView {
    if (!_navigationBarView) {
        _navigationBarView = [[NavigationBarView alloc] initWithTitle:@"用户协议" titleColor:[UIColor whiteColor] backgroundColor:nil leftButtonImageName:@"navigation_bar_return_button" rightButtonImageName:nil];
    }
    return _navigationBarView;
}

- (UITextView *)textView {
    if (!_textView) {
        _textView = [[UITextView alloc] initWithFrame:CGRectMake(0, 64, self.bounds.size.width, self.bounds.size.height - 64)];
        _textView.editable = NO;
    }
    return _textView;
}

@end






