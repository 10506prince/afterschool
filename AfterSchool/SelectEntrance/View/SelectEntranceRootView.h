//
//  SelectEntranceRootView.h
//  AfterSchool
//
//  Created by Teson Draw on 10/6/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface SelectEntranceRootView : UIView

@property (nonatomic, strong) UIButton *loginButton;
@property (nonatomic, strong) UIButton *registerButton;

@property (nonatomic, strong) UIImageView *backgroundImageView;

@end
