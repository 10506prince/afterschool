//
//  SelectEntranceRootView.m
//  AfterSchool
//
//  Created by Teson Draw on 10/6/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "SelectEntranceRootView.h"
#import "MacrosDefinition.h"
#import "UIButton+SetBackgroundColor.h"
#import "Common.h"

@implementation SelectEntranceRootView


- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
//        self.backgroundColor = [UIColor colorWithRed:240/255.f green:239/255.f blue:244/255.f alpha:1];
        
        [self addSubview:self.backgroundImageView];
        
        [self addSubview:self.loginButton];
        [self addSubview:self.registerButton];
//        [self addSubview:self.userAgreementBtn];
    }
    return self;
}

- (UIImageView *)backgroundImageView {
    if (!_backgroundImageView) {
        _backgroundImageView = [[UIImageView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        
        _backgroundImageView.image = [UIImage imageNamed:@"select_entrance_background_iPhone6Plus"];
        
        if (iPhone4s) {
            _backgroundImageView.image = [UIImage imageNamed:@"select_entrance_background_iPhone4s"];
        }
        
        if (iPhone5s) {
            _backgroundImageView.image = [UIImage imageNamed:@"select_entrance_background_iPhone5"];
        }
        
        if (iPhone6) {
            _backgroundImageView.image = [UIImage imageNamed:@"select_entrance_background_iPhone6"];
        }
        
        if (iPhone6Plus) {
            _backgroundImageView.image = [UIImage imageNamed:@"select_entrance_background_iPhone6Plus"];
        }
    }
    return _backgroundImageView;
}

- (UIButton *)loginButton {
    if (!_loginButton) {
        _loginButton = [[UIButton alloc] initWithFrame:CGRectMake((SCREEN_WIDTH - 296) / 2, SCREEN_HEIGHT - 38 - 46 - 32 - 46 - 20 - 10, 296, 46)];
        
        [_loginButton setTitle:@"登录" forState:UIControlStateNormal];
        [_loginButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_loginButton setBackgroundColor:APPLightColor forState:UIControlStateNormal];
        [_loginButton setBackgroundColor:APPDarkColor forState:UIControlStateHighlighted];
        
        [_loginButton setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
        
        _loginButton.titleLabel.font = [UIFont systemFontOfSize:20];
        _loginButton.layer.cornerRadius = BUTTON_CORNER_RADIUS;
        _loginButton.layer.masksToBounds = YES;
        
    }
    
    return _loginButton;
}

- (UIButton *)registerButton {
    if (!_registerButton) {
        _registerButton = [[UIButton alloc] initWithFrame:CGRectMake((SCREEN_WIDTH - 296) / 2, CGRectGetMaxY(_loginButton.frame) + 32, 296, 46)];
        
        [_registerButton setTitle:@"注册" forState:UIControlStateNormal];
//        [_registerButton setTitleColor:APPLightColor forState:UIControlStateNormal];
        [_registerButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_registerButton setTitleColor:APPDarkColor forState:UIControlStateHighlighted];
        
//        [_registerButton setBackgroundColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_registerButton setBackgroundColor:LgColor(0, 0.5) forState:UIControlStateHighlighted];
//        [_registerButton setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
        
        _registerButton.titleLabel.font = [UIFont systemFontOfSize:20];
        _registerButton.layer.cornerRadius = BUTTON_CORNER_RADIUS;
//        [_registerButton.layer setBorderColor:APPLightColor.CGColor];
        [_registerButton.layer setBorderColor:[UIColor whiteColor].CGColor];
        [_registerButton.layer setBorderWidth:1/[UIScreen mainScreen].scale];
        _registerButton.layer.masksToBounds = YES;
    }
    
    return _registerButton;
}


@end
