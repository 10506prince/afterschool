//
//  SelectEntranceViewController.m
//  AfterSchool
//
//  Created by Teson Draw on 10/6/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "SelectEntranceViewController.h"
#import "MacrosDefinition.h"

#import "LoginViewController.h"
#import "RegisterViewController.h"


#import "TDSingleton.h"
#import "Common.h"
#import "TalkingData.h"


@interface SelectEntranceViewController ()

@end

@implementation SelectEntranceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initUserInterface];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [TalkingData trackPageBegin:selfControllerName];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [TalkingData trackPageEnd:selfControllerName];
}
- (void)initUserInterface {
    [self.view addSubview:self.rootView];
    
    if ([TDSingleton instance].autoLogin == YES) {
        LoginViewController *vc = [[LoginViewController alloc] init];
        [self.navigationController pushViewController:vc animated:NO];
    }
}

- (SelectEntranceRootView *)rootView {
    if (!_rootView) {
        _rootView = [[SelectEntranceRootView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
        
        [_rootView.registerButton addTarget:self action:@selector(registerButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        
        [_rootView.loginButton addTarget:self action:@selector(loginButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return _rootView;
}

- (void)registerButtonClicked {
    RegisterViewController *vc = [[RegisterViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)loginButtonClicked {
    LoginViewController *vc = [[LoginViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)loginSuccess {
    [self.navigationController popToRootViewControllerAnimated:NO];
    
    LoginViewController *vc = [[LoginViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
//    LoginViewController *vc = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
//    [self.navigationController pushViewController:vc animated:NO];
}

@end
