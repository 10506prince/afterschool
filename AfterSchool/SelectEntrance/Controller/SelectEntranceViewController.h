//
//  SelectEntranceViewController.h
//  AfterSchool
//
//  Created by Teson Draw on 10/6/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SelectEntranceRootView.h"

@interface SelectEntranceViewController : UIViewController

@property (nonatomic, strong) SelectEntranceRootView *rootView;

/**
 *  退出登录，返回选择注册登录界面
 */
//- (void)logout;

/**
 *  登录成功，进入用户主界面（地图界面）
 */
- (void)loginSuccess;

@end
