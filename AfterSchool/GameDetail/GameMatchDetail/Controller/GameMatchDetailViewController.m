//
//  GameFightingDetailViewController.m
//  AfterSchool
//
//  Created by lg on 15/11/13.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "GameMatchDetailViewController.h"
#import "LGDefineNetServer.h"
#import "Common.h"
#import "SVProgressHUD.h"
#import "NSString+Format.h"
#import "TalkingData.h"

@interface GameMatchDetailViewController () <UITableViewDataSource,UITableViewDelegate> {
    
    LGGameMatchInfo * _gameMatchInfo;
    
    LGGameMatch * _gameMatchDetail;
}

@property (nonatomic, strong) NSArray <NSArray<LGGameTeamMemberInfo *> *> * matchTeams;
@property (nonatomic, strong) NSArray <LGGameMatchTopData *> * topDatas;

@end

@implementation GameMatchDetailViewController

- (void)dealloc
{
    NSLogSelfMethodName;
}

- (instancetype)init
{
    return nil;
}

- (instancetype)initWithGameMatchInfo:(LGGameMatchInfo *)gameMatchInfo {
    self = [super init];
    if (self) {
        _gameMatchInfo = gameMatchInfo;
    }
    return self;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [TalkingData trackPageBegin:selfControllerName];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [TalkingData trackPageEnd:selfControllerName];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initData];
    [self initUserInterface];
    
    [self getMatchDetail];
}

- (void)initData {
}

- (void)initUserInterface {
    [self.view addSubview:self.rootView];
}

- (GameMatchDetailRootView *)rootView {
    if (!_rootView) {
        _rootView = [[GameMatchDetailRootView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        
        [_rootView.navigationBarView.leftButton addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
        
        [_rootView.tableView setDataSource:self];
        [_rootView.tableView setDelegate:self];
    }
    return _rootView;
}

#pragma mark - Action
- (IBAction)backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableViewDatasourceDelegate 
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.matchTeams.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSArray * team = [self.matchTeams objectAtIndex:section];
    return team.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    LGGameTeamMemberInfo * teamMember = self.matchTeams[indexPath.section][indexPath.row];
    
    GameMatchDetailTableViewCellFour * cell = [tableView dequeueReusableCellWithClass:[GameMatchDetailTableViewCellFour class]];
    cell.teamMemberInfo = teamMember;
    
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    GameMatchDetailTableViewHeaderViewOne * header = [tableView dequeueReusableHeaderFooterViewWithClass:[GameMatchDetailTableViewHeaderViewOne class]];
    header.topData = self.topDatas[section];
    
    return header;
}

#pragma makr - UITabelViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    LGGameTeamMemberInfo * teamMember = self.matchTeams[indexPath.section][indexPath.row];
    teamMember.expand = !teamMember.expand;
    
    GameMatchDetailTableViewCellFour * cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.teamMemberInfo = teamMember;
    
    [tableView beginUpdates];
    [tableView endUpdates];
    
    [tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionNone animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    LGGameTeamMemberInfo * teamMember = self.matchTeams[indexPath.section][indexPath.row];
    if (teamMember.expand) {
        return GameMatchDetailTableViewCellFourExpandHeight;
    } else {
        return GameMatchDetailTableViewCellFourNormalHeight;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return GameMatchDetailTableViewHeaderViewOneHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.1;
}

#pragma mark - Networking
- (void)getMatchDetail {
    __weak typeof(self) weakSelf = self;
    
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
    [LGDefineNetServer getGameLOLGamePlayerMatchDetailWithGameMatch:_gameMatchInfo success:^(id result) {
        [SVProgressHUD dismiss];
        if ([result isKindOfClass:[LGGameMatch class]]) {
            _gameMatchDetail = result;
            
            NSArray * dataSource = @[_gameMatchDetail.winTeam,_gameMatchDetail.loseTeam];
            
            NSString * winKda = nil;
            NSString * loseKda = nil;
            
            for (int i = 0; i < 2; i++) {
                NSArray * team = dataSource[i];
                NSUInteger kill = 0, death = 0, assist = 0;
                
                for (LGGameTeamMemberInfo * member in team) {
                    NSArray * kda = [member.kill componentsSeparatedByString:@"/"];
                    kill += [kda[0] integerValue];
                    death += [kda[1] integerValue];
                    assist += [kda[2] integerValue];
                }
                if (i == 0) {
                    winKda = [NSString stringWithFormat:@"%ld/%ld/%ld", (unsigned long)kill, (unsigned long)death, (unsigned long)assist];
                } else {
                    loseKda = [NSString stringWithFormat:@"%ld/%ld/%ld", (unsigned long)kill, (unsigned long)death, (unsigned long)assist];
                }
            }
            
            NSString * winMoney = nil;
            NSString * loseMoney = nil;
            
            for (LGGameTeamDataDetail * data in _gameMatchDetail.top) {
                if ([data.key isEqualToString:@"金钱"]) {
                    NSArray * moneyArray = [data.value componentsSeparatedByString:@"/"];
                    winMoney = moneyArray[0];
                    loseMoney = moneyArray[1];
                    break;
                }
            }
            
            LGGameMatchTopData * winTop = [[LGGameMatchTopData alloc] init];
            winTop.win = YES;
            winTop.kdam = [NSString stringWithFormat:@"%@/%@", winKda,winMoney];
            
            LGGameMatchTopData * loseTop = [[LGGameMatchTopData alloc] init];
            loseTop.win = NO;
            loseTop.kdam = [NSString stringWithFormat:@"%@/%@", loseKda,loseMoney];
            
            
            
            NSArray * topDataSource = @[winTop,loseTop];
            
            self.matchTeams = dataSource;
            self.topDatas = topDataSource;
            
            
            [self.rootView.tableView reloadData];
            
            for (LGGameTeamDataDetail * dic in _gameMatchDetail.top) {
                if ([dic.key isEqualToString:@"类型"]) {
                    [self.rootView.matchTypeLabel setText:dic.value];
                } else if ([dic.key isEqualToString:@"时长"]) {
                    [self.rootView.timeLabel setText:[NSString stringWithFormat:@"总时长 %@",dic.value]];
                }
            }
            
            NSString * dateStr = _gameMatchDetail.time;
            NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"MM-dd"];
            NSDate * dateTemp = [dateFormatter dateFromString:dateStr];
            
            [dateFormatter setDateFormat:@"MM月dd日"];
            
            [self.rootView.dateLabel setText:[dateFormatter stringFromDate:dateTemp]];
        }
    } failure:^(NSString *msg) {
        [SVProgressHUD showErrorWithStatus:msg];
    }];
}

@end
