//
//  GameFightingDetailViewController.h
//  AfterSchool
//
//  Created by lg on 15/11/13.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGViewController.h"
#import "GameMatchDetailRootView.h"
#import "Common.h"
#import "LGGameMatch.h"

@interface GameMatchDetailViewController : LGViewController

- (instancetype)init LGDeprecated("用initWithGameMatchInfo替代");

- (instancetype)initWithGameMatchInfo:(LGGameMatchInfo *)gameMatchInfo;

@property (nonatomic, strong) GameMatchDetailRootView * rootView;
//@property (nonatomic, strong) LGTreeTableView * treeTableView;

@end
