//
//  LGExpandProtocol.h
//  AfterSchool
//
//  Created by lg on 15/11/18.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol LGExpandProtocol <NSObject>

@required

@property (nonatomic, assign) BOOL expand;///<展开状态

@end
