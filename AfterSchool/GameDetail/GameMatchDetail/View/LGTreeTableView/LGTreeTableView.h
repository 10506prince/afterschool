//
//  LGTreeTableView.h
//  AfterSchool
//
//  Created by lg on 15/11/18.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGTableView.h"

#import "CustomNoteItem.h"
#import "GameMatchDetailTableViewCellOne.h"
#import "GameMatchDetailTableViewCellTwo.h"
#import "GameMatchDetailTableViewCellThree.h"

#import "GameMatchDetailTableViewHeaderViewOne.h"

@interface LGTreeTableView : LGTableView

-(instancetype)initWithFrame:(CGRect)frame withData : (NSArray *)data;

@end
