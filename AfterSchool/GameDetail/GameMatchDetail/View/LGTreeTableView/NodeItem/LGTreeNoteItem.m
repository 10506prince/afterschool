//
//  LGTreeNoteItem.m
//  AfterSchool
//
//  Created by lg on 15/11/18.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGTreeNoteItem.h"

@implementation LGTreeNoteItem

- (instancetype)initWithParentId : (int)parentId nodeId : (int)nodeId name : (NSString *)name depth : (int)depth expand : (BOOL)expand{
    self = [self init];
    if (self) {
        self.parentId = parentId;
        self.nodeId = nodeId;
        self.name = name;
        self.depth = depth;
        self.expand = expand;
    }
    return self;
}

@end
