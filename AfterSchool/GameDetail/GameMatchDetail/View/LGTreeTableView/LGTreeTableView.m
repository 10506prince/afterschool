//
//  LGTreeTableView.m
//  AfterSchool
//
//  Created by lg on 15/11/18.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGTreeTableView.h"

@interface LGTreeTableView () <UITableViewDataSource,UITableViewDelegate>

@property (nonatomic , strong) NSArray *data;///<传递过来已经组织好的数据（全量数据）
@property (nonatomic , strong) NSMutableArray *tempData;///<用于存储数据源（部分数据）

@end

static NSString * cellDefaultId = @"cellDefaultId";
static NSString * cellOneId = @"cellOneId";
static NSString * cellTwoId = @"cellTwoId";
//static NSString * cellThreeId = @"cellThreeId";

@implementation LGTreeTableView

-(instancetype)initWithFrame:(CGRect)frame withData : (NSArray *)data{
    self = [super initWithFrame:frame style:UITableViewStyleGrouped];
    if (self) {
        self.dataSource = self;
        self.delegate = self;
        _data = data;
        _tempData = [self createTempData:data];
        
        [self registerNib:[UINib nibWithNibName:NSStringFromClass([GameMatchDetailTableViewCellOne class]) bundle:[NSBundle bundleWithIdentifier:NSStringFromClass([GameMatchDetailTableViewCellOne class])]] forCellReuseIdentifier:cellOneId];
        [self registerNib:[UINib nibWithNibName:NSStringFromClass([GameMatchDetailTableViewCellThree class]) bundle:[NSBundle bundleWithIdentifier:NSStringFromClass([GameMatchDetailTableViewCellThree class])]] forCellReuseIdentifier:cellTwoId];
//        [self registerNib:[UINib nibWithNibName:NSStringFromClass([CustomTableViewCellThree class]) bundle:[NSBundle bundleWithIdentifier:NSStringFromClass([CustomTableViewCellThree class])]] forCellReuseIdentifier:cellThreeId];
        
        [self registerNibHeaderFooterViewWithClass:[GameMatchDetailTableViewHeaderViewOne class]];
    }
    return self;
}

/**
 * 初始化数据源
 */
-(NSMutableArray *)createTempData : (NSArray *)data{
    NSMutableArray *tempArray = [NSMutableArray array];
    for (int i=0; i<data.count; i++) {
        LGTreeNoteItem *node = [_data objectAtIndex:i];
        if (node.expand) {
            [tempArray addObject:node];
        }
    }
    return tempArray;
}


#pragma mark - UITableViewDataSource

#pragma mark - Required

- (NSInteger)numberOfRowsInSection:(NSInteger)section {
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return 0;
    }
    return _tempData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    LGTreeNoteItem *node = [_tempData objectAtIndex:indexPath.row];
    NSMutableString *name = [NSMutableString string];
    for (int i=0; i<node.depth; i++) {
        [name appendString:@"     "];
    }
    [name appendString:node.name];
    
    
    UITableViewCell * cell = nil;
    
    if (node.depth == 0) {
        cell = [tableView dequeueReusableCellWithIdentifier:cellOneId];
        
        cell.textLabel.text = name;
    }
    
    else if (node.depth == 1) {
        cell = [tableView dequeueReusableCellWithIdentifier:cellTwoId];
        cell.textLabel.text = name;
    }
    
//    else if (node.depth == 2) {
//        cell = [tableView dequeueReusableCellWithIdentifier:cellThreeId];
//        cell.textLabel.text = name;
//    }
    else {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellDefaultId];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellDefaultId];
        }
        cell.textLabel.text = name;
    }
    
    return cell;
}


#pragma mark - Optional
//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
//    return 0.01;
//}
//
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    return 40;
//}
//
//- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
//    return 0.01;
//}
//
//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//
//}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case 0:
            return GameMatchDetailTableViewCellTwoHeight;
            break;

        default:
            return GameMatchDetailTableViewCellOneHeight;
            break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    switch (section) {
        case 0:
        {
            return 0.1;
        }
            break;
        case 1:
        {
            return GameMatchDetailTableViewCellOneHeight;
        }
            break;
        case 2:
        {
            return GameMatchDetailTableViewCellThreeHeight;
        }
            break;

        default:
            return 0.1;
            break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.1;
}


#pragma mark - UITableViewDelegate

#pragma mark - Optional
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //先修改数据源
    LGTreeNoteItem *parentNode = [_tempData objectAtIndex:indexPath.row];
    NSUInteger startPosition = indexPath.row+1;
    NSUInteger endPosition = startPosition;
    BOOL expand = NO;
    for (int i=0; i<_data.count; i++) {
        LGTreeNoteItem *node = [_data objectAtIndex:i];
        if (node.parentId == parentNode.nodeId) {
            node.expand = !node.expand;
            if (node.expand) {
                [_tempData insertObject:node atIndex:endPosition];
                expand = YES;
            }else{
                expand = NO;
                endPosition = [self removeAllNodesAtParentNode:parentNode];
                break;
            }
            endPosition++;
        }
    }
    
    //获得需要修正的indexPath
    NSMutableArray *indexPathArray = [NSMutableArray array];
    for (NSUInteger i=startPosition; i<endPosition; i++) {
        NSIndexPath *tempIndexPath = [NSIndexPath indexPathForRow:i inSection:0];
        [indexPathArray addObject:tempIndexPath];
    }
    
    //插入或者删除相关节点
    if (expand) {
        [self insertRowsAtIndexPaths:indexPathArray withRowAnimation:UITableViewRowAnimationNone];
    }else{
        [self deleteRowsAtIndexPaths:indexPathArray withRowAnimation:UITableViewRowAnimationNone];
    }
}

/**
 *  删除该父节点下的所有子节点（包括孙子节点）
 *
 *  @param parentNode 父节点
 *
 *  @return 邻接父节点的位置距离该父节点的长度，也就是该父节点下面所有的子孙节点的数量
 */
-(NSUInteger)removeAllNodesAtParentNode : (LGTreeNoteItem *)parentNode{
    NSUInteger startPosition = [_tempData indexOfObject:parentNode];
    NSUInteger endPosition = startPosition;
    for (NSUInteger i=startPosition+1; i<_tempData.count; i++) {
        LGTreeNoteItem *node = [_tempData objectAtIndex:i];
        endPosition++;
        if (node.depth == parentNode.depth) {
            break;
        }
        node.expand = NO;
    }
    if (endPosition>startPosition) {
        [_tempData removeObjectsInRange:NSMakeRange(startPosition+1, endPosition-startPosition-1)];
    }
    return endPosition;
}


#pragma mark - cell
//- (void)setCellOne:(Class)CellOne {
//    _CellOne = CellOne;
//    
//    static NSString * cellOneId = @"cellOneId";
//    [self registerClass:CellOne forCellReuseIdentifier:cellOneId];
//}
//
//- (void)setCellTwo:(Class)CellTwo {
//    _CellTwo = CellTwo;
//    
//    static NSString * cellTwoId = @"cellTwoId";
//    [self registerClass:CellTwo forCellReuseIdentifier:cellTwoId];
//}
//
//- (void)setCellThree:(Class)CellThree {
//    _CellThree = CellThree;
//    
//    static NSString * cellThreeId = @"cellThreeId";
//    [self registerClass:CellThree forCellReuseIdentifier:cellThreeId];
//}

@end
