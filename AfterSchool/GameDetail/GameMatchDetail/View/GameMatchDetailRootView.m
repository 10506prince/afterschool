//
//  GameFightingDetailRootView.m
//  AfterSchool
//
//  Created by lg on 15/11/13.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "GameMatchDetailRootView.h"
#import "Common.h"

@implementation GameMatchDetailRootView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self addSubview:self.tableView];
        
        [self addSubview:self.navigationBarView];
        [self addSubview:self.topView];
    }
    return self;
}

- (NavigationBarView *)navigationBarView {
    if (!_navigationBarView) {
        _navigationBarView = [[NavigationBarView alloc] initWithTitle:@"比赛详情"
                                                           titleColor:nil
                                                      backgroundColor:nil
                                              leftButtonBgViewOriginX:0
                                                 leftButtonImageWidth:22
                                                 leftButtonTitleColor:[UIColor whiteColor]
                                                       leftButtonName:@"返回"
                                                  leftButtonImageName:@"navigation_bar_return_button_left_arrow" rightButtonName:nil
                                                 rightButtonImageName:nil];
    }
    return _navigationBarView;
}

- (LGTableView *)tableView {
    if (!_tableView) {
        _tableView = [[LGTableView alloc] initWithFrame:self.bounds style:UITableViewStyleGrouped];
        
        UIEdgeInsets ed = UIEdgeInsetsMake(64 + 36, 0, 0, 0);
        
        [_tableView setContentInset:ed];
        [_tableView setScrollIndicatorInsets:ed];
        
//        [_tableView registerNibCellWithClass:[GameMatchDetailTableViewCellOne class]];
//        [_tableView registerNibCellWithClass:[GameMatchDetailTableViewCellTwo class]];
//        [_tableView registerNibCellWithClass:[GameMatchDetailTableViewCellThree class]];
        [_tableView registerNibCellWithClass:[GameMatchDetailTableViewCellFour class]];
        
        [_tableView registerNibHeaderFooterViewWithClass:[GameMatchDetailTableViewHeaderViewOne class]];
    }
    return _tableView;
}

- (UIView *)topView {
    if (!_topView) {
        
        CGFloat height = 36;
        
        _topView = [[UIView alloc] initWithFrame:CGRectMake(0, 64, [UIScreen mainScreen].bounds.size.width, height)];
        [_topView setBackgroundColor:[UIColor whiteColor]];
        
        
        self.dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(12, 0, 60, height)];
        [self.dateLabel setTextAlignment:NSTextAlignmentLeft];
        [self.dateLabel setText:@"日期"];
        [_topView addSubview:self.dateLabel];
        
        
        self.timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 120, height)];
        [self.timeLabel setCenter:CGPointMake(_topView.bounds.size.width/2, height/2)];
        [self.timeLabel setTextAlignment:NSTextAlignmentCenter];
        [self.timeLabel setText:@"总时长"];
        [_topView addSubview:self.timeLabel];
        
        
        self.matchTypeLabel = [[UILabel alloc] initWithFrame:CGRectMake(_topView.bounds.size.width - 112, 0, 100, height)];
        [self.matchTypeLabel setTextAlignment:NSTextAlignmentRight];
        [self.matchTypeLabel setText:@"比赛类型"];
        [_topView addSubview:self.matchTypeLabel];
        
        NSArray * controls = @[self.dateLabel,self.timeLabel,self.matchTypeLabel];
        for (UILabel * lab in controls) {
            [lab setFont:[UIFont systemFontOfSize:12]];
            [lab setTextColor:LgColor(120, 1)];
        }
    }
    return _topView;
}


@end
