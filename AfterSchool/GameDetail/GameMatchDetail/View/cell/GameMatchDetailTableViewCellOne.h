//
//  GameFightingDetailTableViewCellOne.h
//  AfterSchool
//
//  Created by lg on 15/11/13.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LGGameTeamInfo.h"

#define GameMatchDetailTableViewCellOneHeight 102.0f

@interface GameMatchDetailTableViewCellOne : UITableViewCell

@property (nonatomic, strong) LGGameTeamMemberInfo * teamMemberInfo;

@end
