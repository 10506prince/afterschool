//
//  GameFightingDetailTableViewHeaderViewOne.m
//  AfterSchool
//
//  Created by lg on 15/11/13.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "GameMatchDetailTableViewHeaderViewOne.h"
#import "Common.h"

///数据源
@implementation LGGameMatchTopData
@end

///View
@interface GameMatchDetailTableViewHeaderViewOne ()

@property (nonatomic, weak) IBOutlet UIImageView * icon;

@property (nonatomic, weak) IBOutlet UIImageView * killIcon;
@property (nonatomic, weak) IBOutlet UIImageView * deathIcon;
@property (nonatomic, weak) IBOutlet UIImageView * assistIcon;
@property (nonatomic, weak) IBOutlet UIImageView * moneyIcon;

@property (nonatomic, weak) IBOutlet UILabel * kill;
@property (nonatomic, weak) IBOutlet UILabel * assist;
@property (nonatomic, weak) IBOutlet UILabel * death;
@property (nonatomic, weak) IBOutlet UILabel * money;

@end

@implementation GameMatchDetailTableViewHeaderViewOne

- (void)awakeFromNib {
    [self setUp];
}

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if (self) {
        [self setUp];
    }
    return self;
}

- (void)setUp {
    
}

- (void)setTopData:(LGGameMatchTopData *)topData {
    if (_topData == topData) {
        return;
    }
    
    _topData = topData;
    
    NSArray * lables = @[self.kill, self.death, self.assist, self.money];
    NSArray * imageViews = @[self.icon, self.killIcon, self.deathIcon, self.assistIcon, self.moneyIcon];
    
    NSArray * images = nil;
    if (_topData.win) {
        images = @[@"game_match_winer",@"game_match_kill_win",@"game_match_death_win",@"game_match_assist_win",@"game_match_gold_win"];
    } else {
        images = @[@"game_match_loser",@"game_match_kill_lose",@"game_match_death_lose",@"game_match_assist_lose",@"game_match_gold_lose"];
    }
    
    for (int i = 0; i < 5; i++) {
        UIImageView * imageView = imageViews[i];
        NSString * imageName = images[i];
        
        [imageView setImage:[UIImage imageNamed:imageName]];
    }
    
    
    NSArray * arr = [_topData.kdam componentsSeparatedByString:@"/"];
    for (int i = 0; i < 4; i++) {
        UILabel * lable = lables[i];
        NSString * value = arr[i];
        [lable setText:value];
        if (_topData.win) {
            [lable setTextColor:LgColorRGB(37, 179, 243, 1)];
        } else {
            [lable setTextColor:LgColorRGB(250, 2, 2, 1)];
        }
    }
    
}

@end
