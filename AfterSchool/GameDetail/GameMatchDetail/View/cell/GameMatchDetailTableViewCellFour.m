//
//  GameMatchDetailTableViewCellFour.m
//  AfterSchool
//
//  Created by lg on 15/11/18.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "GameMatchDetailTableViewCellFour.h"
#import "UIImageView+WebCache.h"
#import "Common.h"

@interface GameMatchDetailTableViewCellFour ()

@property (nonatomic, weak) IBOutlet UIImageView * icon;

@property (nonatomic, weak) IBOutlet UIImageView * equip1;
@property (nonatomic, weak) IBOutlet UIImageView * equip2;
@property (nonatomic, weak) IBOutlet UIImageView * equip3;
@property (nonatomic, weak) IBOutlet UIImageView * equip4;
@property (nonatomic, weak) IBOutlet UIImageView * equip5;
@property (nonatomic, weak) IBOutlet UIImageView * equip6;
@property (nonatomic, weak) IBOutlet UIImageView * equip7;

@property (nonatomic, weak) IBOutlet UIImageView * kill;
@property (nonatomic, weak) IBOutlet UIImageView * death;
@property (nonatomic, weak) IBOutlet UIImageView * assist;

@property (nonatomic, weak) IBOutlet UIImageView * mvp;

@property (nonatomic, weak) IBOutlet UIImageView * detailImage;

@property (nonatomic, weak) IBOutlet UILabel * name;
@property (nonatomic, weak) IBOutlet UILabel * level;
@property (nonatomic, weak) IBOutlet UILabel * rank;
@property (nonatomic, weak) IBOutlet UILabel * killStr;
@property (nonatomic, weak) IBOutlet UILabel * deathStr;
@property (nonatomic, weak) IBOutlet UILabel * assistStr;

#pragma mark - 详情

@property (nonatomic, weak) IBOutlet UIView * detailView;///<详情界面
@property (nonatomic, weak) IBOutlet UILabel * evaluate;///<评价
@property (nonatomic, weak) IBOutlet UILabel * barrack;///<兵营
@property (nonatomic, weak) IBOutlet UILabel * lastHit;///<补兵
@property (nonatomic, weak) IBOutlet UILabel * lastHitTower;///<推塔
@property (nonatomic, weak) IBOutlet UILabel * proficiency;///<熟练度（数据中没有，暂时显示最大暴击）
@property (nonatomic, weak) IBOutlet UILabel * totalTreatment;///<总治疗
@property (nonatomic, weak) IBOutlet UILabel * maxContinuousKill;///<最大连杀
@property (nonatomic, weak) IBOutlet UILabel * maxMoreKill;///<最大多杀
@property (nonatomic, weak) IBOutlet UILabel * finalDamage;///<输出伤害
@property (nonatomic, weak) IBOutlet UILabel * inflictHarm;///<承受伤害
@property (nonatomic, weak) IBOutlet UILabel * finalDamageToHero;///<对英雄造成总伤害
@property (nonatomic, weak) IBOutlet UILabel * finalADdamageToHero;///<对英雄造成物理伤害
@property (nonatomic, weak) IBOutlet UILabel * finalAPdamageToHero;///<对英雄造成魔法伤害

@property (nonatomic, weak) IBOutlet UIImageView * skill1;
@property (nonatomic, weak) IBOutlet UIImageView * skill2;



@end

@implementation GameMatchDetailTableViewCellFour

- (void)awakeFromNib {
    [self setUp];
}

- (void)setUp {
    [self.kill setImage:[UIImage imageNamed:@"game_match_kill_player"]];
    [self.death setImage:[UIImage imageNamed:@"game_match_death_player"]];
    [self.assist setImage:[UIImage imageNamed:@"game_match_assist_player"]];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)setTeamMemberInfo:(LGGameTeamMemberInfo *)teamMemberInfo {
    
    if (_teamMemberInfo == teamMemberInfo) {
        
        if (teamMemberInfo.expand) {
            [self.detailView setHidden:NO];
            [self.detailImage setTransform:CGAffineTransformMakeRotation(M_PI)];
        } else {
            [self.detailView setHidden:YES];
            [self.detailImage setTransform:CGAffineTransformMakeRotation(0)];
        }
        return;
    }
    
    
    _teamMemberInfo = teamMemberInfo;
    
    ///详情页是否显示
    if (_teamMemberInfo.expand) {
        [self.detailView setHidden:NO];
        [self.detailImage setTransform:CGAffineTransformMakeRotation(M_PI)];
    } else {
        [self.detailView setHidden:YES];
        [self.detailImage setTransform:CGAffineTransformMakeRotation(0)];
    }
    
    ///装备
    NSArray * equips = @[self.equip1,self.equip2,self.equip3,self.equip4,self.equip5,self.equip6,self.equip7];
    int i = 0;
    for (LGGameEquipInfo * equip in _teamMemberInfo.equips) {
        NSURL * url = [NSURL URLWithString:equip.img];
        UIImageView * imageView = equips[i];
        [imageView sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"user_default_head"]];
        i++;
    }
    
    ///头像
    NSURL * iconUrl = [NSURL URLWithString:_teamMemberInfo.icon];
    [self.icon sd_setImageWithURL:iconUrl placeholderImage:[UIImage imageNamed:@"user_default_head"]];
    
    ///MVP
    [self.mvp setImage:(_teamMemberInfo.mvp ? [UIImage imageNamed:@"game_detail_mvp"] : nil)];
    
    ///等级
    [self.level setText:[NSString stringWithFormat:@"Lv.%@", _teamMemberInfo.level]];
    ///名字
    [self.name setText:_teamMemberInfo.name];
    
    ///段位
    [self.rank setText:_teamMemberInfo.grade];
    if (_teamMemberInfo.grade > 0) {
        [self.rank setText:_teamMemberInfo.grade];
        
        if ([_teamMemberInfo.grade containsString:@"黄铜"]) {
            [self.rank setBackgroundColor:LgColorRGB(217, 142, 53, 1)];
        } else if ([_teamMemberInfo.grade containsString:@"白银"]) {
            [self.rank setBackgroundColor:LgColorRGB(171, 201, 240, 1)];
        } else if ([_teamMemberInfo.grade containsString:@"黄金"]) {
            [self.rank setBackgroundColor:LgColorRGB(255, 192, 0, 1)];
        } else if ([_teamMemberInfo.grade containsString:@"铂金"]) {
            [self.rank setBackgroundColor:LgColorRGB(137, 201, 151, 1)];
        } else if ([_teamMemberInfo.grade containsString:@"钻石"]) {
            [self.rank setBackgroundColor:LgColorRGB(255, 150, 217, 1)];
        } else if ([_teamMemberInfo.grade containsString:@"大师"]) {
            [self.rank setBackgroundColor:LgColorRGB(255, 118, 50, 1)];
        } else if ([_teamMemberInfo.grade containsString:@"王者"]) {
            [self.rank setBackgroundColor:LgColorRGB(255, 88, 88, 1)];
        } else {
            [self.rank setBackgroundColor:[UIColor clearColor]];
        }
    } else {
        [self.rank setText:@""];
        [self.rank setBackgroundColor:[UIColor clearColor]];
    }

    
    ///KDA
    NSArray * kda = [_teamMemberInfo.kill componentsSeparatedByString:@"/"];
    [self.killStr setText:kda[0]];
    [self.deathStr setText:kda[1]];
    [self.assistStr setText:kda[2]];
    
    ///详情
    for (LGGameTeamDataDetail * detail in _teamMemberInfo.detail) {
        if ([detail.key isEqualToString:@"战局评分"]) {
            //评价
            [self.evaluate setText:[NSString stringWithFormat:@"评价:%@",detail.value]];
        } else if ([detail.key isEqualToString:@"补兵"]) {
            //补兵
            [self.lastHit setText:[NSString stringWithFormat:@"%@:%@",detail.key,detail.value]];
            
        } else if ([detail.key isEqualToString:@"推塔"]) {
            //推塔
            [self.lastHitTower setText:[NSString stringWithFormat:@"%@:%@",detail.key,detail.value]];
        } else if ([detail.key isEqualToString:@"兵营"]) {
            //兵营
            [self.barrack setText:[NSString stringWithFormat:@"%@:%@",detail.key,detail.value]];
        } else if ([detail.key isEqualToString:@"最大连杀"]) {
            //最大连杀
            [self.maxContinuousKill setText:[NSString stringWithFormat:@"%@:%@",detail.key,detail.value]];
        } else if ([detail.key isEqualToString:@"最大多杀"]) {
            //最大多杀
            [self.maxMoreKill setText:[NSString stringWithFormat:@"%@:%@",detail.key,detail.value]];
        } else if ([detail.key isEqualToString:@"总治疗"]) {
            //总治疗
            [self.totalTreatment setText:[NSString stringWithFormat:@"%@:%@",detail.key,detail.value]];
        } else if ([detail.key isEqualToString:@"输出伤害"]) {
            //输出伤害
            [self.finalDamage setText:[NSString stringWithFormat:@"%@:%@",detail.key,detail.value]];
        } else if ([detail.key isEqualToString:@"承受伤害"]) {
            //承受伤害
            [self.inflictHarm setText:[NSString stringWithFormat:@"%@:%@",detail.key,detail.value]];
        } else if ([detail.key isEqualToString:@"给对方英雄造成总伤害"]) {
            //对英雄总伤害
            [self.finalDamageToHero setText:[NSString stringWithFormat:@"%@:%@",detail.key,detail.value]];
        } else if ([detail.key isEqualToString:@"给对方英雄的物理伤害"]) {
            //对英雄物理伤害
            [self.finalADdamageToHero setText:[NSString stringWithFormat:@"%@:%@",detail.key,detail.value]];
        } else if ([detail.key isEqualToString:@"给对方英雄的魔法伤害"]) {
            //对英雄魔法伤害
            [self.finalAPdamageToHero setText:[NSString stringWithFormat:@"%@:%@",detail.key,detail.value]];
        } else if ([detail.key isEqualToString:@"最大暴击"]) {
            //熟练度
            [self.proficiency setText:[NSString stringWithFormat:@"%@:%@",detail.key,detail.value]];
        }
    }

    
    
    //技能
    NSArray * skills = @[self.skill1,self.skill2];
    for (int i = 0; i < 2; i++) {
        UIImageView * imageView = skills[i];
        if (_teamMemberInfo.skills.count > i) {
            LGGameSkillInfo * skillInfo = _teamMemberInfo.skills[i];
            NSURL * url = [NSURL URLWithString:skillInfo.img];
            [imageView sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"user_default_head"]];
        } else {
            [imageView sd_setImageWithURL:nil placeholderImage:[UIImage imageNamed:@"user_default_head"]];
        }
    }
}
@end
