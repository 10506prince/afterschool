//
//  GameFightingDetailTableViewHeaderViewOne.m
//  AfterSchool
//
//  Created by lg on 15/11/13.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "GameFightingDetailTableViewHeaderViewOne.h"

@interface GameFightingDetailTableViewHeaderViewOne ()

@property (nonatomic, weak) IBOutlet UIImageView * icon;

@property (nonatomic, weak) IBOutlet UIImageView * killIcon;
@property (nonatomic, weak) IBOutlet UIImageView * deathIcon;
@property (nonatomic, weak) IBOutlet UIImageView * assistIcon;
@property (nonatomic, weak) IBOutlet UIImageView * moneyIcon;

@property (nonatomic, weak) IBOutlet UILabel * kill;
@property (nonatomic, weak) IBOutlet UILabel * assist;
@property (nonatomic, weak) IBOutlet UILabel * death;
@property (nonatomic, weak) IBOutlet UILabel * money;

@end

@implementation GameFightingDetailTableViewHeaderViewOne

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if (self) {
        [self setUp];
    }
    return self;
}

- (void)setUp {
    
}

//- (UIImageView *)icon {
//    if (!_icon) {
//        _icon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 66, self.bounds.size.height)];
//        
//        [_icon setImage:[UIImage imageNamed:@"user_default_head.png"]];
//    }
//    return _icon;
//}
//
//- (UIImageView *)killIcon {
//    if (!_killIcon) {
//        _killIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
//        
//        [_killIcon setCenter:CGPointMake(0, self.bounds.size.height/2)];
//        [_killIcon setImage:[UIImage imageNamed:@"user_default_head.png"]];
//    }
//    return _killIcon;
//}
//- (UIImageView *)assistIcon {
//    if (!_assistIcon) {
//        _assistIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
//        [_assistIcon setCenter:CGPointMake(0, self.bounds.size.height/2)];
//        [_assistIcon setImage:[UIImage imageNamed:@"user_default_head.png"]];
//    }
//    return _assistIcon;
//}
//- (UIImageView *)deathIcon {
//    if (!_deathIcon) {
//        _deathIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
//        [_deathIcon setCenter:CGPointMake(0, self.bounds.size.height/2)];
//        [_deathIcon setImage:[UIImage imageNamed:@"user_default_head.png"]];
//    }
//    return _deathIcon;
//}
//- (UIImageView *)moneyIcon {
//    if (!_moneyIcon) {
//        _moneyIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
//        [_moneyIcon setCenter:CGPointMake(self.bounds.size.width - 57, self.bounds.size.height/2)];
//        [_moneyIcon setImage:[UIImage imageNamed:@"user_default_head.png"]];
//    }
//    return _moneyIcon;
//}
//
//- (UILabel *)money {
//    if (!_money) {
//        _money = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 40, 10)];
//        [_money setCenter:CGPointMake(self.bounds.size.width - 32, self.bounds.size.height/2)];
//        
//        [_money setText:@"41438"];
//    }
//    return _money;
//}
//
//- (UILabel *)kill {
//    if (!_kill) {
//        _kill = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 40, 10)];
//        [_kill setCenter:CGPointMake(0, self.bounds.size.height/2)];
//        
//        [_kill setText:@"23"];
//    }
//    return _kill;
//}
//
//- (UILabel *)assist {
//    if (!_assist) {
//        _assist = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 40, 10)];
//        [_assist setCenter:CGPointMake(0, self.bounds.size.height/2)];
//        
//        [_assist setText:@"28"];
//    }
//    return _assist;
//}
//
//- (UILabel *)death {
//    if (!_death) {
//        _death = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 40, 10)];
//        [_death setCenter:CGPointMake(0, self.bounds.size.height/2)];
//        
//        [_death setText:@"18"];
//    }
//    return _death;
//}


@end
