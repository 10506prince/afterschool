//
//  GameFightingDetailTableViewHeaderViewOne.h
//  AfterSchool
//
//  Created by lg on 15/11/13.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>

///数据源
@interface LGGameMatchTopData : NSObject

@property (nonatomic, assign) BOOL win;
@property (nonatomic, copy) NSString * kdam;

@end


#define GameMatchDetailTableViewHeaderViewOneHeight 30.0f

@interface GameMatchDetailTableViewHeaderViewOne : UITableViewHeaderFooterView

@property (nonatomic, strong) LGGameMatchTopData * topData;

@end


