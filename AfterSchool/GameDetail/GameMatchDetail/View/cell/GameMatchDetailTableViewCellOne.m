//
//  GameFightingDetailTableViewCellOne.m
//  AfterSchool
//
//  Created by lg on 15/11/13.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "GameMatchDetailTableViewCellOne.h"
#import "UIImageView+WebCache.h"

@interface GameMatchDetailTableViewCellOne ()

@property (nonatomic, weak) IBOutlet UIImageView * icon;

@property (nonatomic, weak) IBOutlet UIImageView * equip1;
@property (nonatomic, weak) IBOutlet UIImageView * equip2;
@property (nonatomic, weak) IBOutlet UIImageView * equip3;
@property (nonatomic, weak) IBOutlet UIImageView * equip4;
@property (nonatomic, weak) IBOutlet UIImageView * equip5;
@property (nonatomic, weak) IBOutlet UIImageView * equip6;
@property (nonatomic, weak) IBOutlet UIImageView * equip7;

@property (nonatomic, weak) IBOutlet UIImageView * skill1;
@property (nonatomic, weak) IBOutlet UIImageView * skill2;

@property (nonatomic, weak) IBOutlet UIImageView * kill;
@property (nonatomic, weak) IBOutlet UIImageView * death;
@property (nonatomic, weak) IBOutlet UIImageView * assist;

@property (nonatomic, weak) IBOutlet UIImageView * mvp;
@property (nonatomic, weak) IBOutlet UIImageView * detail;

@property (nonatomic, weak) IBOutlet UILabel * name;
@property (nonatomic, weak) IBOutlet UILabel * rank;
@property (nonatomic, weak) IBOutlet UILabel * killStr;
@property (nonatomic, weak) IBOutlet UILabel * deathStr;
@property (nonatomic, weak) IBOutlet UILabel * assistStr;

@end

@implementation GameMatchDetailTableViewCellOne

- (void)awakeFromNib {
    [self setUp];
}

- (void)setUp {
    [self.kill setImage:[UIImage imageNamed:@"game_match_kill_player"]];
    [self.death setImage:[UIImage imageNamed:@"game_match_death_player"]];
    [self.assist setImage:[UIImage imageNamed:@"game_match_assist_player"]];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)setTeamMemberInfo:(LGGameTeamMemberInfo *)teamMemberInfo {
    _teamMemberInfo = teamMemberInfo;
    
    NSArray * equips = @[self.equip1,self.equip2,self.equip3,self.equip4,self.equip5,self.equip6,self.equip7];
    
    ///装备
    int i = 0;
    for (LGGameEquipInfo * equip in _teamMemberInfo.equips) {
        NSURL * url = [NSURL URLWithString:equip.img];
        UIImageView * imageView = equips[i];
        [imageView sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"user_default_head"]];
        i++;
    }
    
    NSURL * iconUrl = [NSURL URLWithString:_teamMemberInfo.icon];
    [self.icon sd_setImageWithURL:iconUrl placeholderImage:[UIImage imageNamed:@"user_default_head"]];
    
    [self.mvp setImage:(_teamMemberInfo.mvp ? [UIImage imageNamed:@"game_detail_mvp"] : nil)];
    
    [self.name setText:_teamMemberInfo.name];
    [self.rank setText:_teamMemberInfo.grade];
    
    
    [self.killStr setText:@""];
    [self.deathStr setText:@""];
    [self.assistStr setText:@""];
}
@end
