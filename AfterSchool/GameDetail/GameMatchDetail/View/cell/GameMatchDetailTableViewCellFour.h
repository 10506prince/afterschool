//
//  GameMatchDetailTableViewCellFour.h
//  AfterSchool
//
//  Created by lg on 15/11/18.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LGGameTeamInfo.h"

#define GameMatchDetailTableViewCellFourNormalHeight 102.0f
#define GameMatchDetailTableViewCellFourExpandHeight 308.0f

@interface GameMatchDetailTableViewCellFour : UITableViewCell

@property (nonatomic, strong) LGGameTeamMemberInfo * teamMemberInfo;

@end
