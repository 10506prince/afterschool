//
//  GameFightingDetailRootView.h
//  AfterSchool
//
//  Created by lg on 15/11/13.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavigationBarView.h"
#import "LGTableView.h"
//#import "GameMatchDetailTableViewCellOne.h"
//#import "GameMatchDetailTableViewCellTwo.h"
//#import "GameMatchDetailTableViewCellThree.h"
#import "GameMatchDetailTableViewCellFour.h"
#import "GameMatchDetailTableViewHeaderViewOne.h"

@interface GameMatchDetailRootView : UIView

@property (nonatomic, strong) NavigationBarView * navigationBarView;

@property (nonatomic, strong) UIView * topView;
@property (nonatomic, strong) UILabel * dateLabel;
@property (nonatomic, strong) UILabel * timeLabel;
@property (nonatomic, strong) UILabel * matchTypeLabel;

@property (nonatomic, strong) LGTableView * tableView;

@end
