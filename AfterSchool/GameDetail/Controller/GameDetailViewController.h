//
//  CityViewController.h
//  AfterSchool
//
//  Created by lg on 15/10/22.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGViewController.h"
#import "GameDetailRootView.h"
#import "LGGamePlayerInfo.h"

@interface GameDetailViewController : LGViewController

@property (nonatomic, strong) GameDetailRootView * rootView;

- (instancetype)init __deprecated_msg("Method deprecated. Use `initWithGamePlayerInfo:`");
- (instancetype)initWithGamePlayerInfo:(LGGamePlayerInfo *)gamePlayerInfo;

@end
