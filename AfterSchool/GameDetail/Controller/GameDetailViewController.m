//
//  CityViewController.m
//  AfterSchool
//
//  Created by lg on 15/10/22.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "GameDetailViewController.h"
#import "Common.h"
#import "SVProgressHUD.h"
#import "LGDefineNetServer.h"
#import "TalkingData.h"

#import "GameMatchDetailViewController.h"
#import "GameMatchListViewController.h"

#import "LGShareViewController.h"
#import "UIScrollView+ScreenShot.h"

@interface GameDetailViewController () <UICollectionViewDataSource,UICollectionViewDelegateFlowLayout> {
    
    LGGamePlayerInfo * _gamePlayerInfo;
    
    NSArray * _headStrArray;
}

@property (nonatomic, strong) NSArray <LGGameTypeInfo *> * summary;
@property (nonatomic, strong) NSArray <LGGameHeroInfo *> * useHeros;
@property (nonatomic, strong) NSArray <LGGameMatchInfo *> * recentMatches;

@property (nonatomic, strong) LGGamePlayerInfo * gamePlayerInfo;
@property (nonatomic, strong) LGShareViewController * shareVC;

@end

@implementation GameDetailViewController
- (void)dealloc
{
    NSLogSelfMethodName;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
    }
    return self;
}

- (instancetype)initWithGamePlayerInfo:(LGGamePlayerInfo *)gamePlayerInfo {
    self = [super init];
    if (self) {
        self.gamePlayerInfo = gamePlayerInfo;
    }
    return self;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [TalkingData trackPageBegin:selfControllerName];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [TalkingData trackPageEnd:selfControllerName];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initData];
    [self initUserInterface];
    
    [self getGamePlayerDetail];
}

- (void)initData {
    _headStrArray = @[@"",@"战绩概览",@"常用英雄",@"近期比赛"];
}
- (void)initUserInterface {
    _shareVC = [[LGShareViewController alloc] initWithNibName:@"LGShareViewController" bundle:nil];
    [self addChildViewController:_shareVC];
    
    [self.view addSubview:self.rootView];
}

- (GameDetailRootView *)rootView {
    if (!_rootView) {
        _rootView = [[GameDetailRootView alloc] initWithFrame:self.view.bounds];
        [_rootView.navigationBarView.leftButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        [_rootView.navigationBarView.rightButton addTarget:self action:@selector(shareAction:) forControlEvents:UIControlEventTouchUpInside];
        
        [_rootView.collectionView setDataSource:self];
        [_rootView.collectionView setDelegate:self];
    }
    
    return _rootView;
}

#pragma mark - Action
- (void)backAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)pushAllMatchAction:(id)sender {
    GameMatchListViewController * allMatchListVC = [[GameMatchListViewController alloc] initWithGamePlayerInfo:_gamePlayerInfo];
    [self.navigationController pushViewController:allMatchListVC animated:YES];
}

- (IBAction)shareAction:(id)sender {
//    UIImage * image = [self imageFromView:self.view atFrame:CGRectMake(0, 64, self.view.bounds.size.width, self.view.bounds.size.height - 64)];
    
    UIImage * image = [self.rootView.collectionView screenShot];
    
    if (image != nil) {
        self.shareVC.publishContentImage = image;
        [self.shareVC.view setFrame:[UIScreen mainScreen].bounds];
        [self.view addSubview:self.shareVC.view];
    } else {
        [SVProgressHUD showErrorWithStatus:@"截屏失败"];
    }
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return _headStrArray.count;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return 1;
            break;
        case 1:
            return self.summary.count;
            break;
        case 2:
            return self.useHeros.count;
            break;
        case 3:
            return self.recentMatches.count;
            break;
            
        default:
            break;
    }
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case 0:
        {
            GameDetailCollectionViewCellOne * cell = [collectionView dequeueReusableCellWithClass:[GameDetailCollectionViewCellOne class] forIndexPath:indexPath];
            cell.gamePlayerInfo = _gamePlayerInfo;
            
            return cell;
        }
            break;
        case 1:
        {
            GameDetailCollectionViewCellTwo * cell = [collectionView dequeueReusableCellWithClass:[GameDetailCollectionViewCellTwo class] forIndexPath:indexPath];
            cell.gameTypeInfo = self.summary[indexPath.row];
            return cell;
        }
            break;
        case 2:
        {
            GameDetailCollectionViewCellThree * cell = [collectionView dequeueReusableCellWithClass:[GameDetailCollectionViewCellThree class] forIndexPath:indexPath];
            cell.gameHeroInfo = self.useHeros[indexPath.row];
            return cell;
        }
            break;
        case 3:
        {
            GameDetailCollectionViewCellFour * cell = [collectionView dequeueReusableCellWithClass:[GameDetailCollectionViewCellFour class] forIndexPath:indexPath];
            cell.indexPath = indexPath;
            cell.gameMatchInfo = self.recentMatches[indexPath.row];
            return cell;
        }
            break;
            
        default:
            return nil;
            break;
    }
    return nil;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    
    UICollectionReusableView * reusableView = nil;
    
    if (kind==UICollectionElementKindSectionFooter) {
        GameDetailCollectionFooterView *footer = [collectionView  dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:GameDetailCollectionFooterViewId forIndexPath:indexPath];
        
        __weak typeof(self) weakSelf = self;
        footer.clickBlock = ^() {
            [weakSelf pushAllMatchAction:nil];
        };
        
        reusableView = footer;
    }
    
    if (kind==UICollectionElementKindSectionHeader) {
        GameDetailCollectionHeadView *header = [collectionView  dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:GameDetailCollectionHeadViewId forIndexPath:indexPath];
        
        header.title = _headStrArray[indexPath.section];
        
//        if (indexPath.section == 3) {
//            [header.image setHidden:NO];
//        } else {
//            [header.image setHidden:YES];
//        }
        
        reusableView = header;
    }
    
    return reusableView;
}

#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case 2:///英雄比赛
        {
            LGGameHeroInfo * gameHeroInfo = self.useHeros[indexPath.row];
            GameMatchListViewController * listVC = [[GameMatchListViewController alloc] initWithGamePlayerInfo:_gamePlayerInfo gameHero:gameHeroInfo];
            [self.navigationController pushViewController:listVC animated:YES];
        }
            break;
        case 3:///比赛详情
        {
            LGGameMatchInfo * gameMatchInfo = self.recentMatches[indexPath.row];
            gameMatchInfo.playerName = _gamePlayerInfo.playerName;
            gameMatchInfo.serverName = _gamePlayerInfo.serverName;
            
            GameMatchDetailViewController * detailVC = [[GameMatchDetailViewController alloc] initWithGameMatchInfo:gameMatchInfo];
            [self.navigationController pushViewController:detailVC animated:YES];
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case 0:
            return CGSizeMake(LgScreenWidth, GameDetailCollectionViewCellOneHeight);
            break;
        case 1:
            return CGSizeMake(LgScreenWidth, GameDetailCollectionViewCellTwoHeight);
            break;
        case 2:
            return CGSizeMake(55, GameDetailCollectionViewCellThreeHeight);
            break;
        case 3:
            return CGSizeMake(LgScreenWidth, GameDetailCollectionViewCellFourHeight);
            break;
            
        default:
            return CGSizeZero;
            break;
    }
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    switch (section) {
        case 0:
        case 1:
        case 3:
            return UIEdgeInsetsMake(0, 0, 0, 0);
            break;
        case 2:
            return UIEdgeInsetsMake(0, 20, 25, 20);
            break;
            
        default:
            return UIEdgeInsetsZero;
            break;
    }
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    if (section == 2) {
//        return 10;
        return floor((LgScreenWidth - 5 * 55 - 40)/4);
    }
    return 0;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    if (section == 1 || section == 2 || section == 3) {
        return CGSizeMake(LgScreenWidth, 60);
    }
    return CGSizeZero;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section {
    if (section == 3) {
        return CGSizeMake(LgScreenWidth, 44);
    }
    return CGSizeZero;
}

#pragma mark - netWorking
- (void)getGamePlayerDetail {
    
    __weak typeof(self) weakSelf = self;
    
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
    [LGDefineNetServer getGameLOLGamePlayerDetailWithGamePlayer:weakSelf.gamePlayerInfo success:^(id result) {
        [SVProgressHUD dismiss];
        
        if ([result isKindOfClass:[LGGamePlayer class]]) {
            LGGamePlayer * gamePlayerDetail = (LGGamePlayer *)result;
            
            weakSelf.gamePlayerInfo.level = gamePlayerDetail.level;
            weakSelf.gamePlayerInfo.attack = gamePlayerDetail.attack;
            
            if (gamePlayerDetail.summary.length > 0) {
                NSData * summaryData = [gamePlayerDetail.summary dataUsingEncoding:NSUTF8StringEncoding];
                NSError * error1 = nil;
                id resultObj1 = [NSJSONSerialization JSONObjectWithData:summaryData options:NSJSONReadingMutableContainers error:&error1];
                //NSLog(@"%@", resultObj1);
                NSArray <LGGameTypeInfo *> * summary = [LGGameTypeInfo objectArrayWithKeyValuesArray:resultObj1];
                
                weakSelf.summary = summary;
            }
            
            if (gamePlayerDetail.useHeros.length > 0) {
                NSData * useHerosData = [gamePlayerDetail.useHeros dataUsingEncoding:NSUTF8StringEncoding];
                NSError * error2 = nil;
                id resultObj2 = [NSJSONSerialization JSONObjectWithData:useHerosData options:NSJSONReadingMutableContainers error:&error2];
                //NSLog(@"%@", resultObj2);
                if (error2.code == 0) {
                    NSArray <LGGameHeroInfo *> * useHeros = [LGGameHeroInfo objectArrayWithKeyValuesArray:resultObj2];
                    weakSelf.useHeros = useHeros;
                }
            }
            
            if (gamePlayerDetail.recentMatches.length > 0) {
                NSData * recentMatchesData = [gamePlayerDetail.recentMatches dataUsingEncoding:NSUTF8StringEncoding];
                NSError * error3 = nil;
                id resultObj3 = [NSJSONSerialization JSONObjectWithData:recentMatchesData options:NSJSONReadingMutableContainers error:&error3];
                
                //NSLog(@"%@", resultObj3);
                
                if (error3.code == 0) {
                    NSArray <LGGameMatchInfo *> * recentMatchs = [LGGameMatchInfo objectArrayWithKeyValuesArray:resultObj3];
                    weakSelf.recentMatches = recentMatchs;
                }
            }
            
            [self.rootView.collectionView reloadData];
        }
    } failure:^(NSString *msg) {
        [SVProgressHUD dismissWithError:msg];
    }];
}

@end
