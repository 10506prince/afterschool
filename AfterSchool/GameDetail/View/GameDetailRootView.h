//
//  CityRootView.h
//  AfterSchool
//
//  Created by lg on 15/10/22.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavigationBarView.h"
#import "LGCollectionView.h"

#import "GameDetailCollectionViewCellOne.h"
#import "GameDetailCollectionViewCellTwo.h"
#import "GameDetailCollectionViewCellThree.h"
#import "GameDetailCollectionViewCellFour.h"

#import "GameDetailCollectionHeadView.h"
#import "GameDetailCollectionFooterView.h"

@interface GameDetailRootView : UIView

@property (nonatomic, strong) NavigationBarView * navigationBarView;
@property (nonatomic, strong) LGCollectionView * collectionView;
@property (nonatomic, strong) UICollectionViewFlowLayout * collectionViewFlowLayout;
@end
