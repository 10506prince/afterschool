//
//  CityRootView.m
//  AfterSchool
//
//  Created by lg on 15/10/22.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "GameDetailRootView.h"
#import "Common.h"

@implementation GameDetailRootView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self addSubview:self.collectionView];
        
        [self addSubview:self.navigationBarView];
    }
    return self;
}

- (NavigationBarView *)navigationBarView {
    if (!_navigationBarView) {
        _navigationBarView = [[NavigationBarView alloc] initWithTitle:@"战绩查询"
                                                           titleColor:nil
                                                      backgroundColor:nil
                                              leftButtonBgViewOriginX:0
                                                 leftButtonImageWidth:22
                                                 leftButtonTitleColor:[UIColor whiteColor]
                                                       leftButtonName:@"返回"
                                                  leftButtonImageName:@"navigation_bar_return_button_left_arrow"
                                                      rightButtonName:nil
                                                 rightButtonImageName:@"game_detail_share"];
    }
    
    return _navigationBarView;
}

- (LGCollectionView *)collectionView {
    if (!_collectionView) {
        _collectionView = [[LGCollectionView alloc] initWithFrame:self.bounds collectionViewLayout:self.collectionViewFlowLayout];
        [_collectionView setBackgroundType:LGCollectionViewBackgroundTypeDark];
        
        [_collectionView registerNibCellWithClass:[GameDetailCollectionViewCellOne class]];
        [_collectionView registerNibCellWithClass:[GameDetailCollectionViewCellTwo class]];
        [_collectionView registerNibCellWithClass:[GameDetailCollectionViewCellThree class]];
        [_collectionView registerNibCellWithClass:[GameDetailCollectionViewCellFour class]];
        
        [_collectionView registerClass:[GameDetailCollectionHeadView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader  withReuseIdentifier:GameDetailCollectionHeadViewId];
        
        [_collectionView registerClass:[GameDetailCollectionFooterView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter  withReuseIdentifier:GameDetailCollectionFooterViewId];
        
    }
    return _collectionView;
}

- (UICollectionViewFlowLayout *)collectionViewFlowLayout {
    if (!_collectionViewFlowLayout) {
        _collectionViewFlowLayout = [[UICollectionViewFlowLayout alloc] init];
        _collectionViewFlowLayout.minimumLineSpacing=0;
        _collectionViewFlowLayout.minimumInteritemSpacing=0;
        _collectionViewFlowLayout.scrollDirection=UICollectionViewScrollDirectionVertical;
        
        _collectionViewFlowLayout.itemSize = CGSizeMake(100, 200);
    }
    return _collectionViewFlowLayout;
}


@end
