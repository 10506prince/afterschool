//
//  GameDetailCollectionHeadView.h
//  AfterSchool
//
//  Created by lg on 15/11/12.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>

static NSString * GameDetailCollectionHeadViewId = @"GameDetailCollectionHeadViewId";

@interface GameDetailCollectionHeadView : UICollectionReusableView

@property (nonatomic, copy) NSString * title;

@end
