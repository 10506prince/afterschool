//
//  LGScaleView.m
//  AfterSchool
//
//  Created by lg on 15/12/9.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGScaleView.h"
#import "Common.h"

@interface LGScaleView ()

@property (nonatomic, assign) IBInspectable CGFloat lineWidth;

@property (nonatomic, strong) IBInspectable UIColor * fillColor;
@property (nonatomic, strong) IBInspectable UIColor * backStrokeColor;
@property (nonatomic, strong) IBInspectable UIColor * scaleStrokeColor;

@property (nonatomic, strong) CAShapeLayer * backgroundRound;
@property (nonatomic, strong) CAShapeLayer * scaleArc;

@property (nonatomic, strong) UILabel * scaleLabel;

@end

IB_DESIGNABLE
@implementation LGScaleView

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setUp];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUp];
    }
    return self;
}
- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self setUp];
    }
    return self;
}
- (void)setUp {
    
    self.backgroundRound = [self getBackgroundRoundLayer];
    self.scaleArc = [self getArclayer];
    
    [self.layer addSublayer:self.backgroundRound];
    [self.layer addSublayer:self.scaleArc];

    
    self.scaleLabel = [[UILabel alloc] init];
    [self.scaleLabel setFont:[UIFont systemFontOfSize:16]];
    [self.scaleLabel setTextAlignment:NSTextAlignmentCenter];
    
    [self addSubview:self.scaleLabel];
    
    self.scale = 0;
    self.lineWidth = 8.0;
    
    self.backStrokeColor = LgColor(194, 1);
    self.scaleStrokeColor = LgColorRGB(37, 179, 243, 1);
    self.fillColor = [UIColor clearColor];

}

- (void)layoutSubviews {
    
    CGPathRef path = [self getRoundPathWithRect:self.bounds];
    
    self.backgroundRound.path = path;
    self.scaleArc.path = path;
    
    [self.scaleLabel sizeToFit];
    [self.scaleLabel setCenter:CGPointMake(self.bounds.size.width/2, self.bounds.size.height/2)];
}

///获取背景图层
- (CAShapeLayer *)getBackgroundRoundLayer {
    
    CAShapeLayer * layer = [CAShapeLayer layer];
    
    return layer;
}

///获取进度条图层
- (CAShapeLayer *)getArclayer {
    CAShapeLayer * layer= [CAShapeLayer layer];
    layer.fillColor = [UIColor clearColor].CGColor;
    layer.strokeStart = 0;
    layer.strokeEnd = 0;
    
    return layer;
}

///获取路径
- (CGPathRef)getRoundPathWithRect:(CGRect)rect {
    
    CGFloat edgeWidth = rect.size.height < rect.size.width ? rect.size.height : rect.size.width;
    edgeWidth -= self.lineWidth;
    CGFloat edgeHor = rect.size.width/2 - edgeWidth/2;
    CGFloat edgeVer = rect.size.height/2 - edgeWidth/2;
    
    CGRect roundedRect = CGRectMake(edgeHor, edgeVer, edgeWidth, edgeWidth);
    UIBezierPath * path = [UIBezierPath bezierPathWithRoundedRect:roundedRect cornerRadius:edgeWidth];
    
    return path.CGPath;
}

#pragma mark - UIColor
- (void)setBackStrokeColor:(UIColor *)backStrokeColor {
    _backStrokeColor = backStrokeColor;
    
    self.backgroundRound.strokeColor = _backStrokeColor.CGColor;
}

- (void)setScaleStrokeColor:(UIColor *)scaleStrokeColor {
    _scaleStrokeColor = scaleStrokeColor;
    
    self.scaleArc.strokeColor = _scaleStrokeColor.CGColor;
    [self.scaleLabel setTextColor:_scaleStrokeColor];
}

- (void)setFillColor:(UIColor *)fillColor {
    _fillColor = fillColor;
    
    self.backgroundRound.fillColor = _fillColor.CGColor;
}


#pragma mark - Value

- (void)setLineWidth:(CGFloat)lineWidth {
    _lineWidth = lineWidth > 0 ? lineWidth : 0;
    
    self.backgroundRound.lineWidth = _lineWidth;
    self.scaleArc.lineWidth = _lineWidth;
}

- (void)setScale:(float)scale {
    _scale = scale < 0 ? 0 : scale > 100 ? 100 : scale;
    
    self.scaleArc.strokeEnd = _scale/100.0;
    [self.scaleLabel setText:[NSString stringWithFormat:@"%.f%%", _scale]];
}

@end
