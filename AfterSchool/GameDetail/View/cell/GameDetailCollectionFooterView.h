//
//  GameDetailCollectionFooterView.h
//  AfterSchool
//
//  Created by lg on 15/11/12.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^GameDetailCollectionFooterViewClickBlock)(void);

static NSString * GameDetailCollectionFooterViewId = @"GameDetailCollectionFooterViewId";

@interface GameDetailCollectionFooterView : UICollectionReusableView

///设置点击回调Block
@property (copy) GameDetailCollectionFooterViewClickBlock clickBlock;

///设置点击回调方法
- (void)setClickTarget:(id)target clickAction:(SEL)action;
///设置回调对象
@property (nonatomic, weak) id clickTarget;
///设置回调方法
@property (nonatomic, assign) SEL clickAction;




@end
