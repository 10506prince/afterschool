//
//  GameDetailCollectionViewCellOne.h
//  AfterSchool
//
//  Created by lg on 15/11/12.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LGGamePlayerInfo.h"

typedef void(^GameDetailCollectionViewCellOneShareActionBlock)();

#define GameDetailCollectionViewCellOneHeight 201.0f

@interface GameDetailCollectionViewCellOne : UICollectionViewCell

@property (nonatomic, weak) LGGamePlayerInfo * gamePlayerInfo;

@property (copy) GameDetailCollectionViewCellOneShareActionBlock shareActionBlock;

@end
