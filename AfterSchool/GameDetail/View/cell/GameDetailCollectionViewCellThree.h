//
//  GameDetailCollectionViewCellThree.h
//  AfterSchool
//
//  Created by lg on 15/11/12.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LGGameHeroInfo.h"

#define GameDetailCollectionViewCellThreeHeight 108.0f

@interface GameDetailCollectionViewCellThree : UICollectionViewCell

@property (nonatomic, strong) LGGameHeroInfo * gameHeroInfo;

@end
