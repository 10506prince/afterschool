//
//  GameDetailCollectionHeadView.m
//  AfterSchool
//
//  Created by lg on 15/11/12.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "GameDetailCollectionHeadView.h"
#import "Common.h"

@interface GameDetailCollectionHeadView ()

@property (nonatomic, strong) UILabel * label;
@property (nonatomic, strong) UIImageView * image;
@property (nonatomic, strong) UIView * backView;

@end

@implementation GameDetailCollectionHeadView

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setUp];
    }
    return self;
}
- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self setUp];
    }
    return self;
}
-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self setUp];
    }
    return self;
}

- (void)setUp {
    
    [self addSubview:self.backView];
    [self addSubview:self.label];
    [self addSubview:self.image];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self.label sizeToFit];
    [self.label setFrame:CGRectMake(20, 35, self.label.bounds.size.width, self.label.bounds.size.height)];
    
    [self.backView setFrame:CGRectMake(0, 10, self.bounds.size.width, self.bounds.size.height - 10)];
}

- (UILabel *)label {
    if (!_label) {
        _label = [[UILabel alloc]init];
        [_label setFont:[UIFont systemFontOfSize:16]];
        [_label setTextColor:[UIColor whiteColor]];
        
    }
    return _label;
}

- (UIImageView *)image {
    if (!_image) {
        _image = [[UIImageView alloc] init];
    }
    return _image;
}

- (UIView *)backView {
    if (!_backView) {
        _backView = [[UIView alloc] init];
        [_backView setBackgroundColor:LgColor(56, 1)];
    }
    return _backView;
}

#pragma mark - setValue
- (void)setTitle:(NSString *)title {
    _title = title;
    
    [self.label setText:_title];
    [self setNeedsLayout];
}


@end
