//
//  GameDetailCollectionViewCellFour.h
//  AfterSchool
//
//  Created by lg on 15/11/12.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LGGameMatchInfo.h"

#define GameDetailCollectionViewCellFourHeight 65.0f

@interface GameDetailCollectionViewCellFour : UICollectionViewCell

@property (nonatomic, strong) LGGameMatchInfo * gameMatchInfo;

@property (nonatomic, strong) NSIndexPath * indexPath;

@end
