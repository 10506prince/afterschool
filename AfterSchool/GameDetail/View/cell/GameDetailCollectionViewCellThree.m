//
//  GameDetailCollectionViewCellThree.m
//  AfterSchool
//
//  Created by lg on 15/11/12.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "GameDetailCollectionViewCellThree.h"
#import "UIImageView+WebCache.h"
#import "NSString+Null.h"

@interface GameDetailCollectionViewCellThree ()

@property (nonatomic, weak) IBOutlet UIImageView * icon;
@property (nonatomic, weak) IBOutlet UILabel * winRate;
@property (nonatomic, weak) IBOutlet UILabel * winRounds;

@end

@implementation GameDetailCollectionViewCellThree

- (void)awakeFromNib {
    
    [self.icon.layer setBorderWidth:3/[UIScreen mainScreen].scale];
    [self.icon.layer setBorderColor:[UIColor whiteColor].CGColor];
}

- (void)setGameHeroInfo:(LGGameHeroInfo *)gameHeroInfo {
    _gameHeroInfo = gameHeroInfo;
    
    if (_gameHeroInfo.img.length > 0) {
        NSURL * url = [NSURL URLWithString:_gameHeroInfo.img];
        [self.icon sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"user_default_head"]];
    } else {
        [self.icon setImage:[UIImage imageNamed:@"user_default_head"]];
    }
    
    [self.winRate setText:[NSString stringWithFormat:@"%ld%%胜", (unsigned long)_gameHeroInfo.winRate]];
    
    [self.winRounds setText:[NSString stringWithFormat:@"%ld场", (unsigned long)_gameHeroInfo.matchStat]];
}
@end
