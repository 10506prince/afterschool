//
//  GameDetailCollectionViewCellTwo.h
//  AfterSchool
//
//  Created by lg on 15/11/12.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LGGameTypeInfo.h"

#define GameDetailCollectionViewCellTwoHeight 74.0f

@interface GameDetailCollectionViewCellTwo : UICollectionViewCell

@property (nonatomic, strong) LGGameTypeInfo * gameTypeInfo;

@end
