//
//  GameDetailCollectionFooterView.m
//  AfterSchool
//
//  Created by lg on 15/11/12.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "GameDetailCollectionFooterView.h"
#import "UIColor+RGB.h"
#import <objc/message.h>
#import "Common.h"

@interface GameDetailCollectionFooterView ()

@property (nonatomic, strong) UIButton * btn;

@end

@implementation GameDetailCollectionFooterView

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.btn = [[UIButton alloc]initWithFrame:self.bounds];
        [self.btn setTitle:@"查看全部" forState:UIControlStateNormal];
        [self.btn.titleLabel setFont:[UIFont systemFontOfSize:14]];
        [self.btn setTitleColor:[UIColor getColorDec:@"120120120"] forState:UIControlStateNormal];
        
        [self.btn addTarget:self action:@selector(clickAction:) forControlEvents:UIControlEventTouchUpInside];

        [self addSubview:self.btn];
    }
    return self;
}

-(void)setText:(NSString *)text
{
    
}

- (IBAction)clickAction:(id)sender {
    
    GameDetailCollectionFooterViewClickBlock block = self.clickBlock;
    
    if (block) {
        block();
    }
    
#define LGMsgSend(...) ((void (*)(void *, SEL, UIView *))objc_msgSend)(__VA_ARGS__)
#define LGMsgTarget(target) (__bridge void *)(target)
    
    //
    //        // 运行时objc_msgSend
    //#define MJRefreshMsgSend(...) ((void (*)(void *, SEL, UIView *))objc_msgSend)(__VA_ARGS__)
    //#define MJRefreshMsgTarget(target) (__bridge void *)(target)
    if ([self.clickTarget respondsToSelector:self.clickAction]) {
        //            MJRefreshMsgSend(MJRefreshMsgTarget(self.refreshingTarget), self.refreshingAction, self);
        //            ((__bridge void)self.clickTarget,self.clickAction,self)objc_msgSend;
        //            objc_msgSend((__bridge void *)(self.clickTarget), self.clickAction,self);
        
        //            (void (*)(void *, SEL, UIView *))objc_msgSend;
        //            (void (*)(void *, SEL, self))objc_msgSend;
        
        
        //            LGMsgSend((__bridge void *)(self.clickTarget), self.clickAction, self);
        LGMsgSend(LGMsgTarget(self.clickTarget), self.clickAction, self);
    }
}

- (void)setClickTarget:(id)target clickAction:(SEL)action {
    self.clickTarget = target;
    self.clickAction = action;
}

@end
