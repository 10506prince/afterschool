//
//  GameDetailCollectionViewCellOne.m
//  AfterSchool
//
//  Created by lg on 15/11/12.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "GameDetailCollectionViewCellOne.h"
#import "UIImageView+WebCache.h"
#import "Common.h"

@interface GameDetailCollectionViewCellOne ()

@property (nonatomic, weak) IBOutlet UIImageView * icon;
@property (nonatomic, weak) IBOutlet UILabel * name;

@property (nonatomic, weak) IBOutlet UIView * otherView;
@property (nonatomic, weak) IBOutlet UILabel * server;
@property (nonatomic, weak) IBOutlet UILabel * rank;
@property (nonatomic, weak) IBOutlet UILabel * attack;

@property (nonatomic, strong) UIView * line1;
@property (nonatomic, strong) UIView * line2;

@end

@implementation GameDetailCollectionViewCellOne

- (void)awakeFromNib {
    
    [self.icon.layer setBorderColor:[UIColor whiteColor].CGColor];
    [self.icon.layer setBorderWidth:2/[UIScreen mainScreen].scale];
    [self.icon.layer setCornerRadius:3];
    [self.icon.layer setMasksToBounds:YES];
    
    [self.otherView.layer setBorderWidth:2/[UIScreen mainScreen].scale];
    [self.otherView.layer setBorderColor:APPLightColor.CGColor];
    [self.otherView.layer setCornerRadius:3];
    
    self.line1 = [self getLineView];
    self.line2 = [self getLineView];
    [self.otherView addSubview:self.line1];
    [self.otherView addSubview:self.line2];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat width = self.bounds.size.width - 40;
    CGFloat height = self.otherView.bounds.size.height;
    CGFloat width1 = 2/[UIScreen mainScreen].scale;
    
    [self.line1 setFrame:CGRectMake(width/3, 0, width1, height)];
    [self.line2 setFrame:CGRectMake(width/3*2, 0, width1, height)];
}

- (UIView *)getLineView {
    UIView * lineView = [[UIView alloc] init];
    [lineView setBackgroundColor:APPLightColor];
    
    return lineView;
}


- (void)setGamePlayerInfo:(LGGamePlayerInfo *)gamePlayerInfo {
    
    if (_gamePlayerInfo == gamePlayerInfo) {
        return;
    }
    
    _gamePlayerInfo = gamePlayerInfo;

    ///头像
    NSURL * url = [NSURL URLWithString:_gamePlayerInfo.icon];
    [self.icon sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"user_default_head"]];
    
    ///昵称
    [self.name setText:_gamePlayerInfo.playerName];
    
    ///服务器
    if (_gamePlayerInfo.serverName.length > 0) {
        [self.server setText:_gamePlayerInfo.serverName];
    } else {
        [self.server setText:@"无"];
    }
    
    ///段位
    if (_gamePlayerInfo.grade.length > 0) {
        [self.rank setText:_gamePlayerInfo.grade];
    } else {
        [self.rank setText:@"无"];
    }
    
    ///战斗力
    [self.attack setText:[NSString stringWithFormat:@"%ld", (unsigned long)_gamePlayerInfo.attack]];
}

- (IBAction)shareAction:(id)sender {
    
    GameDetailCollectionViewCellOneShareActionBlock block = self.shareActionBlock;
    if (block) {
        block();
    }
}
@end
