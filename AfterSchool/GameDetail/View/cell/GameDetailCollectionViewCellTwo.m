//
//  GameDetailCollectionViewCellTwo.m
//  AfterSchool
//
//  Created by lg on 15/11/12.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "GameDetailCollectionViewCellTwo.h"
#import "UIColor+RGB.h"
#import "Common.h"
#import "NSString+Null.h"
#import "LGScaleView.h"

@interface GameDetailCollectionViewCellTwo ()

@property (nonatomic, strong) CAShapeLayer * lineLayer;

@property (nonatomic, weak) IBOutlet LGScaleView * winRate;
@property (nonatomic, weak) IBOutlet UILabel * type;
@property (nonatomic, weak) IBOutlet UILabel * rank;
@property (nonatomic, weak) IBOutlet UILabel * rounds;
@property (nonatomic, weak) IBOutlet UILabel * winRounds;

@end

@implementation GameDetailCollectionViewCellTwo

- (void)awakeFromNib {

    self.lineLayer = [self getLineLayer];
    [self.layer addSublayer:self.lineLayer];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.lineLayer.path = [self getLinePath];
}

- (CAShapeLayer *)getLineLayer {
    CAShapeLayer * line = [CAShapeLayer layer];
    line.lineWidth = 1/[UIScreen mainScreen].scale;
    line.fillColor = [UIColor clearColor].CGColor;
    line.strokeColor = LgColor(102, 1).CGColor;
    
    return line;
}

- (CGPathRef)getLinePath {
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathMoveToPoint(path, NULL, 20, 0);
    CGPathAddLineToPoint(path, NULL, self.bounds.size.width - 20, 0);
    
    UIBezierPath * bezierPath = [UIBezierPath bezierPathWithCGPath:path];
    CGPathRelease(path);
    
    return bezierPath.CGPath;
}

- (void)setUp {
    [self.winRate setScale:48];
    
    [self.type setText:@"经典对战"];
    
    [self.rank setText:@"钻石V"];
    
    [self.rounds setText:@"场次：1991"];
    
    [self.winRounds setText:@"胜场：953"];
    
}

- (void)setGameTypeInfo:(LGGameTypeInfo *)gameTypeInfo {
    
    if (_gameTypeInfo == gameTypeInfo) {
        return;
    }
    
    _gameTypeInfo = gameTypeInfo;
    
    [self.winRate setScale:[_gameTypeInfo.o floatValue]];
    
    [self.type setText:_gameTypeInfo.m];
    
    /*
    if (_gameTypeInfo.rank > 0) {
        [self.rank setText:_gameTypeInfo.rank];
        
        if ([_gameTypeInfo.rank containsString:@"黄铜"]) {
            [self.rank setBackgroundColor:LgColorRGB(217, 142, 53, 1)];
        } else if ([_gameTypeInfo.rank containsString:@"白银"]) {
            [self.rank setBackgroundColor:LgColorRGB(171, 201, 240, 1)];
        } else if ([_gameTypeInfo.rank containsString:@"黄金"]) {
            [self.rank setBackgroundColor:LgColorRGB(255, 192, 0, 1)];
        } else if ([_gameTypeInfo.rank containsString:@"铂金"]) {
            [self.rank setBackgroundColor:LgColorRGB(137, 201, 151, 1)];
        } else if ([_gameTypeInfo.rank containsString:@"钻石"]) {
            [self.rank setBackgroundColor:LgColorRGB(255, 150, 217, 1)];
        } else if ([_gameTypeInfo.rank containsString:@"大师"]) {
            [self.rank setBackgroundColor:LgColorRGB(255, 118, 50, 1)];
        } else if ([_gameTypeInfo.rank containsString:@"王者"]) {
            [self.rank setBackgroundColor:LgColorRGB(255, 88, 88, 1)];
        } else {
            [self.rank setBackgroundColor:[UIColor clearColor]];
        }
    } else {
        [self.rank setText:@""];
        [self.rank setBackgroundColor:[UIColor clearColor]];
    }
     */
    
    [self.rounds setText:[NSString stringWithFormat:@"%ld", (unsigned long)(_gameTypeInfo.w + _gameTypeInfo.l)]];
    
    [self.winRounds setText:[NSString stringWithFormat:@"%ld", (unsigned long)_gameTypeInfo.w]];
}

@end
