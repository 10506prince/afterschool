//
//  GameDetailCollectionViewCellFour.m
//  AfterSchool
//
//  Created by lg on 15/11/12.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "GameDetailCollectionViewCellFour.h"
#import "Common.h"
#import "NSString+Null.h"
#import "UIImageView+WebCache.h"

@interface GameDetailCollectionViewCellFour ()

@property (nonatomic, weak) IBOutlet UIImageView * icon;
@property (nonatomic, weak) IBOutlet UILabel * type;
@property (nonatomic, weak) IBOutlet UIImageView * result;
@property (nonatomic, weak) IBOutlet UIImageView * other;
@property (nonatomic, weak) IBOutlet UILabel * date;

@property (nonatomic, strong) CAShapeLayer * arrowLayer;
@property (nonatomic, strong) CAShapeLayer * lineLayer;

@end

@implementation GameDetailCollectionViewCellFour

- (void)awakeFromNib {
    
    self.lineLayer = [self getLineLayer];
    self.arrowLayer = [self getArrowLayer];
    
    [self.layer addSublayer:self.lineLayer];
    [self.layer addSublayer:self.arrowLayer];
    
    [self.icon.layer setBorderColor:[UIColor whiteColor].CGColor];
    [self.icon.layer setBorderWidth:2/[UIScreen mainScreen].scale];
    [self.icon.layer setMasksToBounds:YES];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.arrowLayer.path = [self getArrowPath];
    self.lineLayer.path = [self getLinePath];
    
    [self.icon.layer setCornerRadius:self.icon.bounds.size.width/2];
}

- (CAShapeLayer *)getLineLayer {
    CAShapeLayer * lineLayer = [CAShapeLayer layer];
    lineLayer.fillColor = [UIColor clearColor].CGColor;
    lineLayer.strokeColor = LgColor(102, 1).CGColor;
    lineLayer.lineWidth = 1/[UIScreen mainScreen].scale;
    
    return lineLayer;
}

- (CAShapeLayer *)getArrowLayer {
    CAShapeLayer * arrowLayer = [CAShapeLayer layer];
    arrowLayer.fillColor = [UIColor clearColor].CGColor;
    arrowLayer.strokeColor = LgColor(102, 1).CGColor;
    arrowLayer.lineWidth = 2/[UIScreen mainScreen].scale;
    
    return arrowLayer;
}

- (CGPathRef)getLinePath {
    
    CGMutablePathRef mutablePath = CGPathCreateMutable();
    CGPathMoveToPoint(mutablePath, NULL, 20, 0);
    CGPathAddLineToPoint(mutablePath, NULL, self.bounds.size.width - 20, 0);
    
    UIBezierPath * path = [UIBezierPath bezierPathWithCGPath:mutablePath];
    CGPathRelease(mutablePath);
    
    return path.CGPath;
}

- (CGPathRef)getArrowPath {
    CGMutablePathRef mutablePath = CGPathCreateMutable();
    CGPathMoveToPoint(mutablePath, NULL, self.bounds.size.width - 27, self.bounds.size.height/2 - 7);
    CGPathAddLineToPoint(mutablePath, NULL, self.bounds.size.width - 20, self.bounds.size.height/2);
    CGPathAddLineToPoint(mutablePath, NULL, self.bounds.size.width - 27, self.bounds.size.height/2 + 7);
    
    UIBezierPath * path = [UIBezierPath bezierPathWithCGPath:mutablePath];
    CGPathRelease(mutablePath);
    
    return path.CGPath;
}

- (void)setGameMatchInfo:(LGGameMatchInfo *)gameMatchInfo {
    if (_gameMatchInfo == gameMatchInfo) {
        return;
    }
    _gameMatchInfo = gameMatchInfo;
    

    NSURL * url = [NSURL URLWithString:_gameMatchInfo.img];
    [self.icon sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"user_default_head"]];
    
    [self.type setText:_gameMatchInfo.type];
    
    if ([_gameMatchInfo.rst isEqualToString:@"胜利"]) {
        [self.result setImage:[UIImage imageNamed:@"game_detail_win"]];
    } else if([_gameMatchInfo.rst isEqualToString:@"失败"]) {
        [self.result setImage:[UIImage imageNamed:@"game_detail_lose"]];
    } else {
        [self.result setImage:[UIImage imageNamed:@"game_detail_flee"]];
    }
    
    
//    if (self.indexPath.row % 2 == 0) {
//        [self.other setImage:[UIImage imageNamed:@"game_detail_kill.png"]];
//    } else {
//        [self.other setImage:[UIImage imageNamed:@"game_detail_mvp.png"]];
//    }
//    if (_gameMatchInfo.fiveKill) {
//        [self.other setImage:[UIImage imageNamed:@"game_detail_kill"]];
//    } else if(_gameMatchInfo.mvp) {
//        [self.other setImage:[UIImage imageNamed:@"game_detail_mvp"]];
//    } else {
//        [self.other setImage:nil];
//    }
    
    [self.date setText:_gameMatchInfo.time];
}
@end
