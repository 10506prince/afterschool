//
//  GameFightingListTableViewCellOne.h
//  AfterSchool
//
//  Created by lg on 15/11/13.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITableView+NibCell.h"
#import "LGGameMatchInfo.h"

#define GameMatchListTableViewCellOneHeight 86.0f

@interface GameMatchListTableViewCellOne : UITableViewCell

@property (nonatomic, strong) LGGameMatchInfo * gameMatchInfo;

@end
