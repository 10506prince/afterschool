//
//  GameFightingListTableViewCellOne.m
//  AfterSchool
//
//  Created by lg on 15/11/13.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "GameMatchListTableViewCellOne.h"
#import "NSString+Null.h"
#import "UIImageView+WebCache.h"
#import "Common.h"

@interface GameMatchListTableViewCellOne ()

@property (nonatomic, strong) CAShapeLayer * lineLayer;
@property (nonatomic, strong) CAShapeLayer * arrowLayer;

@property (nonatomic, weak) IBOutlet UIImageView * icon;
@property (nonatomic, weak) IBOutlet UILabel * type;
@property (nonatomic, weak) IBOutlet UIImageView * result;
@property (nonatomic, weak) IBOutlet UIImageView * other;
@property (nonatomic, weak) IBOutlet UILabel * date;

@end

@implementation GameMatchListTableViewCellOne

- (void)awakeFromNib {
    self.lineLayer = [self getLineLayer];
    self.arrowLayer = [self getArrowLayer];
    
    [self.layer addSublayer:self.lineLayer];
    [self.layer addSublayer:self.arrowLayer];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.lineLayer.path = [self getLinePath];
    self.arrowLayer.path = [self getArrowPath];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

- (void)setUp {
    [self.icon setImage:[UIImage imageNamed:@"user_default_head"]];
    [self.type setText:@"大乱斗"];
    [self.result setImage:[UIImage imageNamed:@"game_detail_win"]];
    [self.other setImage:[UIImage imageNamed:@"game_detail_kill"]];
    [self.date setText:@"11-02 13:25"];
}

- (CAShapeLayer *)getLineLayer {
    CAShapeLayer * lineLayer = [CAShapeLayer layer];
    lineLayer.fillColor = [UIColor clearColor].CGColor;
    lineLayer.strokeColor = LgColor(229, 1).CGColor;
    lineLayer.lineWidth = SINGLE_LINE_WIDTH;
    
    return lineLayer;
}

- (CAShapeLayer *)getArrowLayer {
    CAShapeLayer * arrowLayer = [CAShapeLayer layer];
    arrowLayer.fillColor = [UIColor clearColor].CGColor;
    arrowLayer.strokeColor = LgColor(229, 1).CGColor;
    arrowLayer.lineWidth = 2;
    
    return arrowLayer;
}

- (CGPathRef)getLinePath {
    
    CGMutablePathRef mutablePath = CGPathCreateMutable();
    CGPathMoveToPoint(mutablePath, NULL, 12, self.bounds.size.height);
    CGPathAddLineToPoint(mutablePath, NULL, self.bounds.size.width - 12, self.bounds.size.height);
    
    UIBezierPath * path = [UIBezierPath bezierPathWithCGPath:mutablePath];
    CGPathRelease(mutablePath);
    
    return path.CGPath;
}

- (CGPathRef)getArrowPath {
    CGMutablePathRef mutablePath = CGPathCreateMutable();
    CGPathMoveToPoint(mutablePath, NULL, self.bounds.size.width - 20, self.bounds.size.height/2 - 8);
    CGPathAddLineToPoint(mutablePath, NULL, self.bounds.size.width - 12, self.bounds.size.height/2);
    CGPathAddLineToPoint(mutablePath, NULL, self.bounds.size.width - 20, self.bounds.size.height/2 + 8);
    //CGPathCloseSubpath(mutablePath);
    
    UIBezierPath * path = [UIBezierPath bezierPathWithCGPath:mutablePath];
    CGPathRelease(mutablePath);
    
    return path.CGPath;
}

- (void)setGameMatchInfo:(LGGameMatchInfo *)gameMatchInfo {
    _gameMatchInfo = gameMatchInfo;
    
    if (_gameMatchInfo.img.length == 0) {
        [self.icon setImage:[UIImage imageNamed:@"user_default_head"]];
    } else {
        NSURL * url = [NSURL URLWithString:_gameMatchInfo.img];
        [self.icon sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"user_default_head"]];
    }
    
    [self.type setText:_gameMatchInfo.type];
    
    if ([_gameMatchInfo.rst isEqualToString:@"胜利"]) {
        [self.result setImage:[UIImage imageNamed:@"game_detail_win"]];
    } else if([_gameMatchInfo.rst isEqualToString:@"失败"]) {
        [self.result setImage:[UIImage imageNamed:@"game_detail_lose"]];
    } else {
        [self.result setImage:[UIImage imageNamed:@"game_detail_flee"]];
    }
    
    if (_gameMatchInfo.fiveKill) {
        [self.other setImage:[UIImage imageNamed:@"game_detail_kill"]];
    } else if(_gameMatchInfo.mvp) {
        [self.other setImage:[UIImage imageNamed:@"game_detail_mvp"]];
    } else {
        [self.other setImage:nil];
    }
    
    [self.date setText:_gameMatchInfo.time];
}
@end
