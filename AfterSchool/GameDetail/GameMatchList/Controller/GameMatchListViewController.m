//
//  GameFightingListViewController.m
//  AfterSchool
//
//  Created by lg on 15/11/12.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "GameMatchListViewController.h"
#import "Common.h"
#import "SVProgressHUD.h"
#import "LGDefineNetServer.h"
#import "TalkingData.h"

#import "GameMatchDetailViewController.h"

@interface GameMatchListViewController () <UITableViewDataSource,UITableViewDelegate> {
    
    LGGameHeroInfo * _gameHeroInfo;
    LGGamePlayerInfo * _gamePlayerInfo;
    
    NSMutableArray <LGGameMatchInfo *> * _dataSource;
    NSUInteger _currentPageNum;
}

@property (nonatomic, strong) NSMutableArray <LGGameMatchInfo *> * dataSource;
@property (nonatomic,) NSUInteger currentPageNum;

@property (nonatomic, strong) LGGameHeroInfo * gameHeroInfo;
@property (nonatomic, strong) LGGamePlayerInfo * gamePlayerInfo;

@end

@implementation GameMatchListViewController
- (void)dealloc
{
    NSLogSelfMethodName;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
    }
    return self;
}

- (instancetype)initWithGamePlayerInfo:(LGGamePlayerInfo *)gamePlayerInfo {
    self = [super init];
    if (self) {
        self.gamePlayerInfo = gamePlayerInfo;
    }
    return self;
}

- (instancetype)initWithGamePlayerInfo:(LGGamePlayerInfo *)gamePlayerInfo gameHero:(LGGameHeroInfo *)gameHeroInfo {
    self = [super init];
    if (self) {
        self.gamePlayerInfo = gamePlayerInfo;
        self.gameHeroInfo = gameHeroInfo;
    }
    return self;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [TalkingData trackPageBegin:selfControllerName];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [TalkingData trackPageEnd:selfControllerName];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initData];
    [self initUserInterface];
}

- (void)initData {
    _dataSource = [NSMutableArray array];
    _currentPageNum = 1;
}

- (void)initUserInterface {
    [self.view addSubview:self.rootView];
}

- (GameMatchListRootView *)rootView {
    if (!_rootView) {
        _rootView = [[GameMatchListRootView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        
        [_rootView.navigationBarView.leftButton addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
        
        [_rootView.tableView setDataSource:self];
        [_rootView.tableView setDelegate:self];
        
        __weak GameMatchListViewController * weakSelf = self;
        MJRefreshAutoNormalFooter * footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
            [weakSelf getGameMatchListLoadMore];
        }];
        MJRefreshNormalHeader * header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            [weakSelf getGameMatchListReload];
        }];
        
        [header setTintColor:LgColorRGB(120, 120, 120, 1)];
        
        [footer setTitle:@"没有更多数据了..." forState:MJRefreshStateNoMoreData];
        [footer setTintColor:LgColorRGB(120, 120, 120, 1)];
        
        _rootView.tableView.mj_footer = footer;
        _rootView.tableView.mj_header = header;
        
        [_rootView.tableView.mj_header beginRefreshing];
        
    }
    return _rootView;
}

#pragma mark - Action
- (IBAction)backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _dataSource.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    GameMatchListTableViewCellOne * cell = [tableView dequeueReusableCellWithClass:[GameMatchListTableViewCellOne class]];
    cell.gameMatchInfo = _dataSource[indexPath.row];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.1;
}
#pragma mark - UITableDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    LGGameMatchInfo * gameMatchInfo = _dataSource[indexPath.row];
    gameMatchInfo.playerName = _gamePlayerInfo.playerName;
    gameMatchInfo.serverName = _gamePlayerInfo.serverName;
    
    GameMatchDetailViewController * detailVC = [[GameMatchDetailViewController alloc] initWithGameMatchInfo:gameMatchInfo];
    [self.navigationController pushViewController:detailVC animated:YES];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return GameMatchListTableViewCellOneHeight;
}

#pragma mark - Networking
- (void)getGameMatchListLoadMore {
    _currentPageNum++;
    
    __weak typeof(self) weakSelf = self;

    [LGDefineNetServer getGameLOLGamePlayerMatchListWithGamePlayer:self.gamePlayerInfo gameHero:self.gameHeroInfo page:self.currentPageNum success:^(id result) {
        
        NSUInteger pageNum = [[result objectForKey:@"pageNum"] integerValue];
        NSArray * matches = [result objectForKey:@"matches"];
        
        NSArray <LGGameMatchInfo *> * gameMatchInfoArray = [LGGameMatchInfo objectArrayWithKeyValuesArray:matches];
        
        if (gameMatchInfoArray.count < pageNum) {
            [weakSelf.rootView.tableView.mj_footer endRefreshingWithNoMoreData];
        } else {
            [weakSelf.dataSource addObjectsFromArray:gameMatchInfoArray];
            [weakSelf.rootView.tableView reloadData];
            [weakSelf.rootView.tableView.mj_footer endRefreshing];
        }
        
        [weakSelf.rootView.tableView.mj_header endRefreshing];
    } failure:^(NSString *msg) {
        [SVProgressHUD showErrorWithStatus:msg];
        
        [weakSelf.rootView.tableView.mj_header endRefreshing];
        [weakSelf.rootView.tableView.mj_footer endRefreshing];
    }];
}

- (void)getGameMatchListReload {
    _currentPageNum = 1;
    __weak typeof(self) weakSelf = self;
    [self.rootView.tableView.mj_footer resetNoMoreData];
    
    [LGDefineNetServer getGameLOLGamePlayerMatchListWithGamePlayer:self.gamePlayerInfo gameHero:self.gameHeroInfo page:self.currentPageNum success:^(id result) {
        
        NSUInteger pageNum = [[result objectForKey:@"pageNum"] integerValue];
        NSArray * matches = [result objectForKey:@"matches"];

        [weakSelf.dataSource removeAllObjects];
        
        NSArray <LGGameMatchInfo *> * gameMatchInfoArray = [LGGameMatchInfo objectArrayWithKeyValuesArray:matches];
        
        if (gameMatchInfoArray.count < pageNum) {
            [weakSelf.rootView.tableView.mj_footer endRefreshingWithNoMoreData];
        } else {
            [weakSelf.dataSource addObjectsFromArray:gameMatchInfoArray];
            [weakSelf.rootView.tableView reloadData];
            [weakSelf.rootView.tableView.mj_footer endRefreshing];
        }
        
        [weakSelf.rootView.tableView.mj_header endRefreshing];
    } failure:^(NSString *msg) {
        [SVProgressHUD showErrorWithStatus:msg];
        
        [weakSelf.rootView.tableView.mj_header endRefreshing];
        [weakSelf.rootView.tableView.mj_footer endRefreshing];
        
    }];
}

@end
