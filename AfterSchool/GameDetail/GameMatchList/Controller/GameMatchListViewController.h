//
//  GameFightingListViewController.h
//  AfterSchool
//
//  Created by lg on 15/11/12.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGViewController.h"
#import "GameMatchListRootView.h"
#import "LGGameHeroInfo.h"
#import "LGGamePlayerInfo.h"

@interface GameMatchListViewController : LGViewController

@property (nonatomic, strong) GameMatchListRootView * rootView;

- (instancetype)init __deprecated_msg("Method deprecated. Use `initWithGameHero:` or `initWithGamePlayerInfo:`");

/**
 *  @brief  初始化为比赛列表（按时间排列）
 *
 *  @param gamePlayerInfo 游戏角色
 *
 *  @return 比赛列表控制器
 */
- (instancetype)initWithGamePlayerInfo:(LGGamePlayerInfo *)gamePlayerInfo;


/**
 *  @brief  初始化为单个英雄比赛列表
 *
 *  @param gameHeroInfo 英雄摘要
 *
 *  @return 比赛列表控制器
 */
- (instancetype)initWithGamePlayerInfo:(LGGamePlayerInfo *)gamePlayerInfo gameHero:(LGGameHeroInfo *)gameHeroInfo;

@end
