//
//  ForgetPasswordViewController.m
//  AfterSchool
//
//  Created by Teson Draw on 10/7/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "ForgetPasswordViewController.h"
#import "SetNewPasswordViewController.h"

#import "MacrosDefinition.h"
#import "Toast+UIView.h"
#import "NSString+PhoneNumber.h"
#import "TDNetworking.h"

#import "TDSingleton.h"

#import "Common.h"
#import "TalkingData.h"

@interface ForgetPasswordViewController () <UITextFieldDelegate>

@end

@implementation ForgetPasswordViewController
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [TalkingData trackPageBegin:selfControllerName];
    [_rootView.mobileNumberTextField becomeFirstResponder];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [TalkingData trackPageEnd:selfControllerName];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initData];
    [self initUserInterface];
}

- (void)initData {
    _model = [[RegisterModel alloc] init];
}

- (void)initUserInterface {
    [self.view addSubview:self.rootView];
}

- (ForgetPasswordRootView *)rootView {
    if (!_rootView) {
        _rootView = [[ForgetPasswordRootView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];

        [_rootView.navigationBarView.leftButton addTarget:self action:@selector(returnButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        [_rootView.navigationBarView.rightButton addTarget:self action:@selector(nextStepButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        [_rootView.nextStepButton addTarget:self action:@selector(nextStepButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        [_rootView.getVerificationCodeButton addTarget:self action:@selector(getVerificationCodeButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        
        _rootView.mobileNumberTextField.delegate = self;
        _rootView.verificationCodeTextField.delegate = self;
    }
    return _rootView;
}


- (void)returnButtonClicked {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)nextStepButtonClicked {
    if ([self verifyMobileNumber] && [self verifyVerificationCode]) {
        [self stopTimer];
        
        [self submitMobileNumber:_rootView.mobileNumberTextField.text verificationCode:_rootView.verificationCodeTextField.text];
    }
}

- (void)getVerificationCodeButtonClicked {
    _rootView.verificationCodeTextField.text = @"";
    [_rootView.verificationCodeTextField becomeFirstResponder];
    
    if ([self verifyMobileNumber]) {
        /**
         *  向服务器请求向特定手机发送验证码
         */
        [self requestVerificationCodeWihtMobileNumber:_rootView.mobileNumberTextField.text];
    }
}

- (BOOL)verifyMobileNumber {
    NSString *mobileNumber = [_rootView.mobileNumberTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if (mobileNumber.length == 0) {
        [self.view makeToast:@"请输入手机号" duration:2 position:[NSValue valueWithCGPoint:CGPointMake((SCREEN_WIDTH / 2), 110)]];
        [_rootView.mobileNumberTextField becomeFirstResponder];
        return NO;
    }
    
    if (![_rootView.mobileNumberTextField.text isNumber]) {
        [self.view makeToast:@"请检查手机号码" duration:2 position:[NSValue valueWithCGPoint:CGPointMake((SCREEN_WIDTH / 2), 110)]];
        [_rootView.mobileNumberTextField becomeFirstResponder];
        return NO;
    }
    
    if (mobileNumber.length < 7) {
        [self.view makeToast:@"请检查手机号码" duration:2 position:[NSValue valueWithCGPoint:CGPointMake((SCREEN_WIDTH / 2), 110)]];
        [_rootView.mobileNumberTextField becomeFirstResponder];
        return NO;
    }
    
    return YES;
}

- (BOOL)verifyVerificationCode {
    NSString *code = [_rootView.verificationCodeTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if (code.length == 0) {
        [self.view makeToast:@"请输入验证码" duration:2 position:[NSValue valueWithCGPoint:CGPointMake((SCREEN_WIDTH / 2), 110)]];
        [_rootView.verificationCodeTextField becomeFirstResponder];
        return NO;
    }
    
    if (code.length < 4 ) {
        [self.view makeToast:@"请检查验证码" duration:2 position:[NSValue valueWithCGPoint:CGPointMake((SCREEN_WIDTH / 2), 110)]];
        [_rootView.verificationCodeTextField becomeFirstResponder];
        return NO;
    }
    
    return YES;
}

- (BOOL)isVerificationCodeEqualToServer {
    if ([_rootView.verificationCodeTextField.text isEqualToString:_model.verificationCode]) {
        return YES;
    } else {
        [self.view makeToast:@"验证码不正确" duration:2 position:[NSValue valueWithCGPoint:CGPointMake((SCREEN_WIDTH / 2), 110)]];
    }
    
    return NO;
}

#pragma mark UITextField Protocol Method
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    switch (textField.tag) {
        case 100:
            [_rootView.verificationCodeTextField becomeFirstResponder];
            break;
            
        case 101:
        {
            [self nextStepButtonClicked];
        }
            break;
    }
    
    return NO;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    BOOL result = YES;
    switch (textField.tag) {
        case 100:
        {
            NSString * toBeString = [textField.text stringByReplacingCharactersInRange:range withString:string]; //得到输入框的内容
            
            //如果输入框内容大于11则弹出警告
            if ([toBeString length] > 11) {
                textField.text = [toBeString substringToIndex:11];
                return NO;
            }
        }
            break;
            
        case 101:
        {
            NSString * toBeString = [textField.text stringByReplacingCharactersInRange:range withString:string]; //得到输入框的内容
            
            if ([toBeString length] >= 4) {
                textField.text = [toBeString substringToIndex:4];
                
                return NO;
            }
        }
            break;
    }
    
    return result;
}

/**
 *  向服务器请求向特定手机发送验证码
 */
- (void)requestVerificationCodeWihtMobileNumber:(NSString *)MobileNumber {
    NSDictionary *parameters = @{@"action":@"step1",
                                 @"userName":MobileNumber};

    [TDNetworking requestWithURL:[TDSingleton instance].URL.setPasswordURL
                      parameters:parameters
                         success:^(id responseObject)
    {
        NSLog(@"result:%@", responseObject);
        if ([responseObject[@"result"] integerValue] == 1) {
//            _model.verificationCode = responseObject[@"code"];
            _rootView.getVerificationCodeButton.enabled = NO;
            
            NSThread *runTimerThread = [[NSThread alloc] initWithTarget:self
                                                               selector:@selector(initTimer)
                                                                 object:nil];
            [runTimerThread start];
        } else {
            [self.view makeToast:[NSString stringWithFormat:@"%@", responseObject[@"msg"]] duration:2 position:[NSValue valueWithCGPoint:CGPointMake((SCREEN_WIDTH / 2), 110)]];
        }
    } failure:^(NSError *error) {
        NSLog(@"error:%@", error);
        [self.view makeToast:@"连接出错，请检查网络" duration:2 position:[NSValue valueWithCGPoint:CGPointMake((SCREEN_WIDTH / 2), 110)]];
    }];
}

/**
 *  提交手机号和录入的验证码到服务器验证
 */
- (void)submitMobileNumber:(NSString *)mobileNumber verificationCode:(NSString *)verificationCode {
    NSDictionary *parameters = @{@"action":@"step2",
                                 @"userName":mobileNumber,
                                 @"code":verificationCode};
    
    [TDNetworking requestWithURL:[TDSingleton instance].URL.setPasswordURL
                      parameters:parameters
                         success:^(id responseObject)
    {
        NSLog(@"result:%@", responseObject);
        if ([responseObject[@"result"] integerValue] == 1) {
            [self enterSetNewPasswordViewController];
        } else {
            [self.view makeToast:[NSString stringWithFormat:@"%@", responseObject[@"msg"]] duration:2 position:[NSValue valueWithCGPoint:CGPointMake((SCREEN_WIDTH / 2), 110)]];
        }
    } failure:^(NSError *error) {
        NSLog(@"error:%@", error);
        [self.view makeToast:@"连接出错，请检查网络" duration:2 position:[NSValue valueWithCGPoint:CGPointMake((SCREEN_WIDTH / 2), 110)]];
    }];
}

- (void)initTimer {
    _model.secondsCountDown = 60;
    NSString *string = [NSString stringWithFormat:@"(%ld后)可重发", (long)_model.secondsCountDown];
    [_rootView.getVerificationCodeButton setTitle:string forState:UIControlStateNormal];
    
    _timer = [[NSTimer alloc] initWithFireDate:[NSDate dateWithTimeIntervalSinceNow:0] interval:1 target:self selector:@selector(calculateLeftTime) userInfo:nil repeats:YES];
    
    [[NSRunLoop currentRunLoop] addTimer:_timer forMode:NSDefaultRunLoopMode];
    
    //打开下面一行，该线程的runloop就会运行起来，timer才会起作用
    [[NSRunLoop currentRunLoop] run];
}

- (void)calculateLeftTime {
    _model.secondsCountDown -= 1;
    NSString *string = [NSString stringWithFormat:@"(%ld秒后)可重发", (long)_model.secondsCountDown];
    [_rootView.getVerificationCodeButton setTitle:string forState:UIControlStateNormal];
    
    if (_model.secondsCountDown == 0) {
        [self stopTimer];
    }
}

- (void)stopTimer {
    [_timer invalidate];
    _timer = nil;
    
    _rootView.getVerificationCodeButton.enabled = YES;
    [_rootView.getVerificationCodeButton setTitle:@"获取验证码" forState:UIControlStateNormal];
}

- (void)enterSetNewPasswordViewController {
    SetNewPasswordViewController *vc = [[SetNewPasswordViewController alloc] init];
    
    _model.mobileNumber = _rootView.mobileNumberTextField.text;
    _model.verificationCode = _rootView.verificationCodeTextField.text;
    vc.model = _model;
    [self.navigationController pushViewController:vc animated:YES];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

@end
