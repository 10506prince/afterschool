//
//  ForgetPasswordRootView.m
//  AfterSchool
//
//  Created by Teson Draw on 10/7/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "ForgetPasswordRootView.h"
#import "MacrosDefinition.h"
#import "UIButton+SetBackgroundColor.h"
#import "Common.h"

@implementation ForgetPasswordRootView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithRed:240/255.f green:239/255.f blue:244/255.f alpha:1];
        
        [self addSubview:self.navigationBarView];
        [self addSubview:self.mobileNumberTextField];
        [self addSubview:self.verificationCodeTextField];
        [self addSubview:self.getVerificationCodeButton];
        [self addSubview:self.nextStepButton];
    }
    return self;
}

- (NavigationBarView *)navigationBarView {
    if (!_navigationBarView) {
        _navigationBarView = [[NavigationBarView alloc] initWithTitle:@"忘记密码"
                                                           titleColor:nil
                                                      backgroundColor:nil
                                              leftButtonBgViewOriginX:0
                                                 leftButtonImageWidth:22
                                                 leftButtonTitleColor:nil
                                                       leftButtonName:@"返回"
                                                  leftButtonImageName:@"navigation_bar_return_button_left_arrow"
                                                      rightButtonName:nil
                                                 rightButtonImageName:nil];
//        _navigationBarView = [[NavigationBarView alloc] initWithTitle:@"忘记密码"
//                                                           titleColor:[UIColor whiteColor]
//                                                      backgroundColor:nil
//                                                       leftButtonName:nil
//                                                 leftButtonTitleColor:[UIColor whiteColor]
//                                                  leftButtonImageName:@"navigation_bar_return_button"
//                                                      rightButtonName:nil
//                                                 rightButtonImageName:nil];
    }
    return _navigationBarView;
}

- (CustomTextField *)mobileNumberTextField {
    if (!_mobileNumberTextField) {
        _mobileNumberTextField = [[CustomTextField alloc] initWithFrame:CGRectMake(20, 64 + 20, SCREEN_WIDTH - 40, 50)];
        _mobileNumberTextField.placeholder = @"请输入手机号";
        _mobileNumberTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
        _mobileNumberTextField.returnKeyType = UIReturnKeyNext;
        _mobileNumberTextField.tag = 100;
        _mobileNumberTextField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
        _mobileNumberTextField.autocorrectionType = UITextAutocorrectionTypeNo;
        _mobileNumberTextField.backgroundColor = [UIColor whiteColor];
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 26, 26)];
        imageView.image = [UIImage imageNamed:@"input_mobile_number_tip_pattern"];
        
        _mobileNumberTextField.leftView = imageView;
        _mobileNumberTextField.leftViewMode = UITextFieldViewModeAlways;
    }
    
    return _mobileNumberTextField;
}

- (TDTextField *)verificationCodeTextField {
    if (!_verificationCodeTextField) {
        _verificationCodeTextField = [[TDTextField alloc] initWithFrame:CGRectMake(20, CGRectGetMaxY(_mobileNumberTextField.frame) + 10, (SCREEN_WIDTH - 40)*34.0/67, 50)];
        _verificationCodeTextField.placeholder = @"输入验证码";
        _verificationCodeTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
        _verificationCodeTextField.returnKeyType = UIReturnKeySend;
        _verificationCodeTextField.tag = 101;
        _verificationCodeTextField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
        _verificationCodeTextField.autocorrectionType = UITextAutocorrectionTypeNo;
        _verificationCodeTextField.backgroundColor = [UIColor whiteColor];
    }
    
    return _verificationCodeTextField;
}

- (UIButton *)getVerificationCodeButton {
    if (!_getVerificationCodeButton) {
        _getVerificationCodeButton = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.verificationCodeTextField.frame), _verificationCodeTextField.frame.origin.y, (SCREEN_WIDTH - 40) - CGRectGetWidth(self.verificationCodeTextField.bounds), 50)];
        [_getVerificationCodeButton setTitle:@"获取验证码" forState:UIControlStateNormal];
        [_getVerificationCodeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_getVerificationCodeButton setBackgroundColor:[UIColor colorWithRed:253/255.f green:155/255.f blue:39/255.0 alpha:1]];
        _getVerificationCodeButton.layer.masksToBounds = YES;
        _getVerificationCodeButton.titleLabel.font = [UIFont systemFontOfSize:16];
    }
    return _getVerificationCodeButton;
}

- (UIButton *)nextStepButton {
    if (!_nextStepButton) {
        _nextStepButton = [[UIButton alloc] initWithFrame:CGRectMake(20, CGRectGetMaxY(_getVerificationCodeButton.frame) + 20, SCREEN_WIDTH - 40, 50)];
        [_nextStepButton setTitle:@"下一步" forState:UIControlStateNormal];
        [_nextStepButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_nextStepButton setBackgroundColor:APPLightColor];
        [_nextStepButton setBackgroundColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
        _nextStepButton.layer.cornerRadius = 5;
        _nextStepButton.layer.masksToBounds = YES;
        _nextStepButton.titleLabel.font = [UIFont systemFontOfSize:20];
    }
    return _nextStepButton;
}

@end
