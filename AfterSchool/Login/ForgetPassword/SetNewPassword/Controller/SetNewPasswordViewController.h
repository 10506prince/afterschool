//
//  SetNewPasswordViewController.h
//  AfterSchool
//
//  Created by Teson Draw on 10/7/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SetNewPasswordRootView.h"
#import "RegisterModel.h"

@interface SetNewPasswordViewController : UIViewController

@property (nonatomic, strong) SetNewPasswordRootView *rootView;
@property (nonatomic, strong) RegisterModel *model;

@end
