//
//  SetNewPasswordViewController.m
//  AfterSchool
//
//  Created by Teson Draw on 10/7/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "SetNewPasswordViewController.h"
#import "AppDelegate.h"

#import "MacrosDefinition.h"

#import "Toast+UIView.h"
#import "TDNetworking.h"
#import "TDSingleton.h"

#import "TDMixedCryptogram.h"

#import "Common.h"
#import "TalkingData.h"

@interface SetNewPasswordViewController () <UITextFieldDelegate>

@end

@implementation SetNewPasswordViewController
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [TalkingData trackPageBegin:selfControllerName];
    [_rootView.passwordTextField becomeFirstResponder];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [TalkingData trackPageEnd:selfControllerName];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initUserInterface];
}

- (void)initUserInterface {
    [self.view addSubview:self.rootView];
}

- (SetNewPasswordRootView *)rootView {
    if (!_rootView) {
        _rootView = [[SetNewPasswordRootView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
        
        [_rootView.navigationBarView.leftButton addTarget:self action:@selector(returnButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        [_rootView.completeButton addTarget:self action:@selector(completeButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        
        _rootView.passwordTextField.delegate = self;
        _rootView.confirmPasswordTextField.delegate = self;
    }
    return _rootView;
}

- (void)returnButtonClicked {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)completeButtonClicked {
    if ([self verifyPassword] && [self verifyConfirmPassword] && [self passwordEqualToConfirmPassword]) {
        NSString *password = [_rootView.passwordTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        [self submitPassword:password];
    }
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

#pragma mark UITextField Protocol Method
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    switch (textField.tag) {
        case 100:
            [_rootView.confirmPasswordTextField becomeFirstResponder];
            break;
            
        case 101:
        {
            [self completeButtonClicked];
        }
            break;
    }
    
    return NO;
}

- (BOOL)verifyPassword {
    NSString *password = [_rootView.passwordTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if (password.length == 0) {
        [self.view makeToast:@"请输入密码" duration:2 position:[NSValue valueWithCGPoint:CGPointMake((SCREEN_WIDTH / 2), 110)]];
        return NO;
    }
    
    if (password.length < 6) {
        [self.view makeToast:@"密码至少6位" duration:2 position:[NSValue valueWithCGPoint:CGPointMake((SCREEN_WIDTH / 2), 110)]];
        return NO;
    }
    
    return YES;
}

- (BOOL)verifyConfirmPassword {
    NSString *password = [_rootView.confirmPasswordTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if (password.length == 0) {
        [self.view makeToast:@"请输入密码" duration:2 position:[NSValue valueWithCGPoint:CGPointMake((SCREEN_WIDTH / 2), 110)]];
        return NO;
    }
    
    if (password.length < 6) {
        [self.view makeToast:@"密码至少6位" duration:2 position:[NSValue valueWithCGPoint:CGPointMake((SCREEN_WIDTH / 2), 110)]];
        return NO;
    }
    
    return YES;
}

- (BOOL)passwordEqualToConfirmPassword {
    NSString *password = [_rootView.passwordTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    NSString *confirmPassword = [_rootView.confirmPasswordTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if (![password isEqualToString:confirmPassword]) {
        [self.view makeToast:@"两次密码不一致" duration:2 position:[NSValue valueWithCGPoint:CGPointMake((SCREEN_WIDTH / 2), 110)]];
        return NO;
    }
    
    return YES;
}

/**
 *  提交手机号和录入的验证码到服务器验证
 */
- (void)submitPassword:(NSString *)password  {
    NSString *mixedCryptogram = [TDMixedCryptogram fromPassword:password];
    NSDictionary *parameters = @{@"action":@"step3",
                               @"userName":_model.mobileNumber,
                                   @"code":_model.verificationCode,
                                @"userPwd":mixedCryptogram};
    
    [TDNetworking requestWithURL:[TDSingleton instance].URL.setPasswordURL
                      parameters:parameters
                         success:^(id responseObject)
    {
        NSLog(@"step3: submitPassword result:%@", responseObject);
        if ([responseObject[@"result"] integerValue] == 1) {
            [self backToLoginWindow];
        } else {
            [self.view makeToast:[NSString stringWithFormat:@"%@", responseObject[@"msg"]] duration:2 position:[NSValue valueWithCGPoint:CGPointMake((SCREEN_WIDTH / 2), 110)]];
        }
    } failure:^(NSError *error) {
        NSLog(@"error:%@", error);
        [self.view makeToast:@"连接出错，请检查网络" duration:2 position:[NSValue valueWithCGPoint:CGPointMake((SCREEN_WIDTH / 2), 110)]];
    }];
}

- (void)backToLoginWindow {
    [self.navigationController popToViewController:self.navigationController.viewControllers[1] animated:YES];
}

@end
