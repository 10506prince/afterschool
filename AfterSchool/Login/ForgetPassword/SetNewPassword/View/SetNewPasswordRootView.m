//
//  SetNewPasswordRootView.m
//  AfterSchool
//
//  Created by Teson Draw on 10/7/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "SetNewPasswordRootView.h"
#import "MacrosDefinition.h"
#import "UIButton+SetBackgroundColor.h"
#import "Common.h"


@implementation SetNewPasswordRootView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithRed:240/255.f green:239/255.f blue:244/255.f alpha:1];
        
        [self addSubview:self.navigationBarView];
        [self addSubview:self.passwordTextField];
        [self addSubview:self.confirmPasswordTextField];
        [self addSubview:self.completeButton];
    }
    return self;
}

- (NavigationBarView *)navigationBarView {
    if (!_navigationBarView) {
        _navigationBarView = [[NavigationBarView alloc] initWithTitle:@"设置密码"
                                                           titleColor:nil
                                                      backgroundColor:nil
                                              leftButtonBgViewOriginX:0
                                                 leftButtonImageWidth:22
                                                 leftButtonTitleColor:nil
                                                       leftButtonName:@"返回"
                                                  leftButtonImageName:@"navigation_bar_return_button_left_arrow"
                                                      rightButtonName:nil
                                                 rightButtonImageName:nil];
//        _navigationBarView = [[NavigationBarView alloc] initWithTitle:@"设置密码" titleColor:[UIColor whiteColor] backgroundColor:[UIColor colorWithRed:56/255.f green:55/255.f blue:60/255.f alpha:0.90] leftButtonName:nil leftButtonTitleColor:[UIColor whiteColor] leftButtonImageName:@"navigation_bar_return_button" rightButtonName:nil rightButtonImageName:nil];
    }
    return _navigationBarView;
}

- (CustomTextField *)passwordTextField {
    if (!_passwordTextField) {
        _passwordTextField = [[CustomTextField alloc] initWithFrame:CGRectMake(20, 64 + 20, SCREEN_WIDTH - 40, 44)];
        _passwordTextField.placeholder = @"请输入密码";
        _passwordTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
        _passwordTextField.returnKeyType = UIReturnKeyNext;
        _passwordTextField.tag = 100;
        _passwordTextField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
        _passwordTextField.autocorrectionType = UITextAutocorrectionTypeNo;
//        _passwordTextField.layer.cornerRadius = 5;
        _passwordTextField.backgroundColor = [UIColor whiteColor];
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 26, 26)];
        imageView.image = [UIImage imageNamed:@"input_verification_code_tip_pattern"];
        
        _passwordTextField.leftView = imageView;
        _passwordTextField.leftViewMode = UITextFieldViewModeAlways;
    }
    
    return _passwordTextField;
}

- (CustomTextField *)confirmPasswordTextField {
    if (!_confirmPasswordTextField) {
        _confirmPasswordTextField = [[CustomTextField alloc] initWithFrame:CGRectMake(20, CGRectGetMaxY(_passwordTextField.frame) + 10, SCREEN_WIDTH - 40, 44)];
        _confirmPasswordTextField.placeholder = @"请再次输入密码";
        _confirmPasswordTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
        _confirmPasswordTextField.returnKeyType = UIReturnKeyNext;
        _confirmPasswordTextField.tag = 101;
        _confirmPasswordTextField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
        _confirmPasswordTextField.autocorrectionType = UITextAutocorrectionTypeNo;
//        _confirmPasswordTextField.layer.cornerRadius = 5;
        _confirmPasswordTextField.backgroundColor = [UIColor whiteColor];
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 26, 26)];
        imageView.image = [UIImage imageNamed:@"input_verification_code_tip_pattern"];
        
        _confirmPasswordTextField.leftView = imageView;
        _confirmPasswordTextField.leftViewMode = UITextFieldViewModeAlways;
    }
    
    return _confirmPasswordTextField;
}

- (UIButton *)completeButton {
    if (!_completeButton) {
        _completeButton = [[UIButton alloc] initWithFrame:CGRectMake(20, CGRectGetMaxY(_confirmPasswordTextField.frame) + 20, SCREEN_WIDTH - 40, 45)];
        [_completeButton setTitle:@"完成" forState:UIControlStateNormal];
        [_completeButton setBackgroundColor:APPLightColor];
        [_completeButton setBackgroundColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
        _completeButton.layer.cornerRadius = 5;
        _completeButton.layer.masksToBounds = YES;
        _completeButton.titleLabel.font = [UIFont systemFontOfSize:20];
    }
    return _completeButton;
}

@end
