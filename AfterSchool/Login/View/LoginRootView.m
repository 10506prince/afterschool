//
//  LoginRootView.m
//  AfterSchool
//
//  Created by Teson Draw on 9/30/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "LoginRootView.h"
#import "MacrosDefinition.h"
#import "UIButton+SetBackgroundColor.h"
#import "Common.h"

@implementation LoginRootView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithRed:240/255.f green:239/255.f blue:244/255.f alpha:1];
        [self addSubview:self.navigationBarView];
        [self addSubview:self.mobileNumberTextField];
        [self addSubview:self.passwordTextField];
        [self addSubview:self.forgetPasswordButton];
        [self addSubview:self.loginButton];
    }
    
    return self;
}

- (NavigationBarView *)navigationBarView {
    if (!_navigationBarView) {
        _navigationBarView = [[NavigationBarView alloc] initWithTitle:@"登录"
                                                           titleColor:nil
                                                      backgroundColor:nil
                                              leftButtonBgViewOriginX:0
                                                 leftButtonImageWidth:22
                                                 leftButtonTitleColor:nil
                                                       leftButtonName:@"返回"
                                                  leftButtonImageName:@"navigation_bar_return_button_left_arrow"
                                                      rightButtonName:nil
                                                 rightButtonImageName:nil];
    }
    return _navigationBarView;
}

- (CustomTextField *)mobileNumberTextField {
    if (!_mobileNumberTextField) {
        _mobileNumberTextField = [[CustomTextField alloc] initWithFrame:CGRectMake(20, 64 + 20, SCREEN_WIDTH - 40, 44)];
        _mobileNumberTextField.placeholder = @"请输入手机号";
        _mobileNumberTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
        _mobileNumberTextField.returnKeyType = UIReturnKeyNext;
        _mobileNumberTextField.tag = 100;
        _mobileNumberTextField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
        _mobileNumberTextField.autocorrectionType = UITextAutocorrectionTypeNo;
//        _mobileNumberTextField.layer.cornerRadius = BUTTON_CORNER_RADIUS;

        _mobileNumberTextField.backgroundColor = [UIColor whiteColor];
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 26, 26)];
        imageView.image = [UIImage imageNamed:@"input_mobile_number_tip_pattern"];
        _mobileNumberTextField.leftView = imageView;
        _mobileNumberTextField.leftViewMode = UITextFieldViewModeAlways;
    }
    
    return _mobileNumberTextField;
}

- (CustomTextField *)passwordTextField {
    if (!_passwordTextField) {
        _passwordTextField = [[CustomTextField alloc] initWithFrame:CGRectMake(20, CGRectGetMaxY(_mobileNumberTextField.frame) + 10, SCREEN_WIDTH - 40, 44)];
        _passwordTextField.placeholder = @"请输入密码";
        _passwordTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
        _passwordTextField.returnKeyType = UIReturnKeyNext;
        _passwordTextField.tag = 101;
        _passwordTextField.keyboardType = UIKeyboardTypeNamePhonePad;
        _passwordTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
        _passwordTextField.autocorrectionType = UITextAutocorrectionTypeNo;
        _passwordTextField.returnKeyType = UIReturnKeySend;
//        _passwordTextField.layer.cornerRadius = BUTTON_CORNER_RADIUS;
        
        _passwordTextField.secureTextEntry = YES;
        _passwordTextField.backgroundColor = [UIColor whiteColor];
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 26, 26)];
        imageView.image = [UIImage imageNamed:@"input_verification_code_tip_pattern"];
        _passwordTextField.leftView = imageView;
        _passwordTextField.leftViewMode = UITextFieldViewModeAlways;
    }
    
    return _passwordTextField;
}

- (UIUnderlinedButton *)forgetPasswordButton {
    if (!_forgetPasswordButton) {
        _forgetPasswordButton = [[UIUnderlinedButton alloc] initWithFrame:CGRectMake(20, CGRectGetMaxY(_passwordTextField.frame) + 20, 120, 44)];
        [_forgetPasswordButton setTitle:@"忘记密码？" forState:UIControlStateNormal];
        [_forgetPasswordButton setTitleColor:[UIColor colorWithRed:38/255.f green:166/255.f blue:154/255.f alpha:1] forState:UIControlStateNormal];
        [_forgetPasswordButton setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
    }
    return _forgetPasswordButton;
}

- (UIButton *)loginButton {
    if (!_loginButton) {
        _loginButton = [[UIButton alloc] initWithFrame:CGRectMake(20, CGRectGetMaxY(_forgetPasswordButton.frame) + 30, SCREEN_WIDTH - 40, 46)];
        [_loginButton setTitle:@"开始登录" forState:UIControlStateNormal];
        [_loginButton setBackgroundColor:APPLightColor];
        [_loginButton setBackgroundColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
        _loginButton.layer.cornerRadius = BUTTON_CORNER_RADIUS;
        _loginButton.layer.masksToBounds = YES;
        _loginButton.titleLabel.font = [UIFont systemFontOfSize:20];
    }
    return _loginButton;
}

@end
