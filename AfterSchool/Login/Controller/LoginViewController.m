//
//  LoginViewController.m
//  AfterSchool
//
//  Created by Teson Draw on 9/30/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "LoginViewController.h"
#import "MacrosDefinition.h"

#import "ForgetPasswordViewController.h"

#import "Toast+UIView.h"
#import "NSString+PhoneNumber.h"

#import "TDSingleton.h"
#import "SVProgressHUD.h"
#import "LoginGatewayServer.h"
#import "LGNavigationController.h"
#import "LGNavigationBar.h"
#import "LGToolBar.h"
#import "LGTabBarController.h"
#import "LGDefineNetServer.h"
#import "SettingViewController.h"
#import "TDMixedCryptogram.h"

#import "Common.h"
#import "TalkingData.h"

@interface LoginViewController () <UITextFieldDelegate>

@end

@implementation LoginViewController
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [TalkingData trackPageBegin:selfControllerName];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [TalkingData trackPageEnd:selfControllerName];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initUserInterface];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if ([TDSingleton instance].autoLogin == YES) {

        [self addTransitionImage];
        
        _rootView.mobileNumberTextField.text = [TDSingleton instance].account;
        _rootView.passwordTextField.text = [TDSingleton instance].password;
        [self loginButtonClicked];
    } else {
        [self removeTransitionImage];
        _rootView.passwordTextField.text = @"";//重设密码成功返回，清除原来输入的密码
    }
}

- (void)initUserInterface {
    [self.view addSubview:self.rootView];
}

- (LoginRootView *)rootView {
    if (!_rootView) {
        _rootView = [[LoginRootView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
        [_rootView.loginButton addTarget:self action:@selector(loginButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        [_rootView.navigationBarView.leftButton addTarget:self action:@selector(returnButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        [_rootView.forgetPasswordButton addTarget:self action:@selector(forgetPasswordButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        _rootView.mobileNumberTextField.delegate = self;
        _rootView.passwordTextField.delegate = self;
    }
    return _rootView;
}

- (void)returnButtonClicked {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)loginButtonClicked {
    
    [_rootView endEditing:YES];

    if ([self verifyMobileNumber] && [self verifyPassword]) {
        
        if ([TDSingleton instance].hasGetPushID) {
            [self verifyMobileNumberAndPasswordOnServer];
        } else {
            
            dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_async(queue, ^{
                
//                while (![TDSingleton instance].hasGetPushID) {
//                    [NSThread sleepForTimeInterval:0.01];
//                }
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self verifyMobileNumberAndPasswordOnServer];
                });
            });
        }
    }
}

- (void)forgetPasswordButtonClicked {
    ForgetPasswordViewController *vc = [[ForgetPasswordViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

- (BOOL)verifyMobileNumber {
    NSString *mobileNumber = [_rootView.mobileNumberTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if (mobileNumber.length == 0) {
        [self.view makeToast:@"请输入手机号" duration:2 position:[NSValue valueWithCGPoint:CGPointMake((SCREEN_WIDTH / 2), 110)]];
        [_rootView.mobileNumberTextField becomeFirstResponder];
        return NO;
    }
    
    if (![mobileNumber isNumber]) {
        [self.view makeToast:@"请检查手机号码" duration:2 position:[NSValue valueWithCGPoint:CGPointMake((SCREEN_WIDTH / 2), 110)]];
        [_rootView.mobileNumberTextField becomeFirstResponder];
        return NO;
    }
    
    if (mobileNumber.length < 7) {
        [self.view makeToast:@"请检查手机号码" duration:2 position:[NSValue valueWithCGPoint:CGPointMake((SCREEN_WIDTH / 2), 110)]];
        [_rootView.mobileNumberTextField becomeFirstResponder];
        return NO;
    }
    
    return YES;
}

- (BOOL)verifyPassword {
    NSString *password = [_rootView.passwordTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if (password.length == 0) {
        [self.view makeToast:@"请输入密码" duration:2 position:[NSValue valueWithCGPoint:CGPointMake((SCREEN_WIDTH / 2), 110)]];
        [_rootView.passwordTextField becomeFirstResponder];
        return NO;
    }
    
    if (password.length < 6) {
        [self.view makeToast:@"密码至少6位" duration:2 position:[NSValue valueWithCGPoint:CGPointMake((SCREEN_WIDTH / 2), 110)]];
        [_rootView.passwordTextField becomeFirstResponder];
        return NO;
    }

    return YES;
}

- (void)verifyMobileNumberAndPasswordOnServer {
    NSString *mobileNumber = [_rootView.mobileNumberTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *password = [_rootView.passwordTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    /**
     *  记录账号密码
     */
    [TDSingleton instance].account = mobileNumber;
    [TDSingleton instance].password = password;
    
    NSString *mixedCryptogram = [TDMixedCryptogram fromPassword:password];
    
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
    [LoginGatewayServer submitMobileNumber:mobileNumber
                                  password:mixedCryptogram
                                   success:^
     {
         [SVProgressHUD dismiss];
         [self enterMainWindow];
     } failure:^(NSString *message) {

//         [SVProgressHUD showErrorWithStatus:message duration:2];
//         [SVProgressHUD showWithStatus:message maskType:SVProgressHUDMaskTypeClear];

         [SVProgressHUD dismissWithError:message];
         
         [self removeTransitionImage];
         
         /**
          *  调试时，外网登录成功，切换到内网时，因为内网无该账号，不能自动登录，返回选择入口界面，因为是自动登录，又被推回到登录界面
          */
         [TDSingleton instance].autoLogin = NO;
     }];
}


- (void)enterMainWindow {
    
    SettingViewController *vc = [[SettingViewController alloc] init];
    [self.navigationController pushViewController:vc animated:NO];
    
    [self removeTransitionImage];
}

- (void)logout {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark UITextField Protocol Method
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    switch (textField.tag) {
        case 100:
            [_rootView.passwordTextField becomeFirstResponder];
            break;
            
        case 101:
        {
            [self loginButtonClicked];
        }
            break;
    }
    
    return NO;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [_rootView endEditing:YES];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)addTransitionImage {
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    imageView.tag = 200;
    
    imageView.image = [UIImage imageNamed:@"select_entrance_background_iPhone6Plus"];
    
    if (iPhone4s) {
        imageView.image = [UIImage imageNamed:@"select_entrance_background_iPhone4s"];
    }
    
    if (iPhone5s) {
        imageView.image = [UIImage imageNamed:@"select_entrance_background_iPhone5"];
    }
    
    if (iPhone6) {
        imageView.image = [UIImage imageNamed:@"select_entrance_background_iPhone6"];
    }
    
    if (iPhone6Plus) {
        imageView.image = [UIImage imageNamed:@"select_entrance_background_iPhone6Plus"];
    }
    
    [self.view addSubview:imageView];
}

- (void)removeTransitionImage {
    UIImageView *imageView = [self.view viewWithTag:200];
    
    if (imageView) {
        [imageView removeFromSuperview];
    }
}

//- (void)setUnreadMessagesCountWithTabBarController:(LGTabBarController *)tabBarController {
//    NSNumber *conversationTypeNumber = [NSNumber numberWithInteger:ConversationType_PRIVATE];
//    NSArray *conversationTypeArray = [[NSArray alloc] initWithObjects:conversationTypeNumber, nil];
//    
//    int count = [[RCIMClient sharedRCIMClient]getUnreadCount:conversationTypeArray];
//    
//    if (count > 0) {
//       [[[[tabBarController tabBar] items] objectAtIndex:1] setBadgeValue:[NSString stringWithFormat:@"%d", count]];
//    } else {
//        [[[[tabBarController tabBar] items] objectAtIndex:1] setBadgeValue:nil];
//    }
//}

@end
