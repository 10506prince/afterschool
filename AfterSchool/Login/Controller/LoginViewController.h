//
//  LoginViewController.h
//  AfterSchool
//
//  Created by Teson Draw on 9/30/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginRootView.h"

@interface LoginViewController : UIViewController

@property (nonatomic, strong) LoginRootView *rootView;

- (void)logout;

@end
