//
//  SettingViewController.m
//  AfterSchool
//
//  Created by Teson Draw on 12/29/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "SettingViewController.h"
#import "MacrosDefinition.h"
#import "TDSingleton.h"
#import "UIImageView+WebCache.h"

#import "PeopleNearbyViewController.h"
#import "RongCloudMessagesViewController.h"
#import "RelationshipsViewController.h"

#import "SettingWindowAnimation.h"

#import "OrdersViewController.h"
#import "AssetViewController.h"
#import "SetupViewController.h"
#import "DakaReviewViewController.h"

#import "SettingTableViewCell.h"

#import "UserCenterViewController.h"
#import "GameAccountViewController.h"
#import "CommonAddressViewController.h"
#import "NotificationName.h"
#import "LGDefineNetServer.h"

#import "Common.h"
#import "SVProgressHUD.h"
#import "TalkingData.h"
#import "SendMessageViewController.h"

#import "LoginViewController.h"

@interface SettingViewController () <UITableViewDataSource, UITableViewDelegate>

@end

@implementation SettingViewController

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [TalkingData trackPageBegin:selfControllerName];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [TalkingData trackPageEnd:selfControllerName];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initData];
    [self initUserInterface];
    [self addTabBarControllerView];
    
    [self addUserIconChangeNotification];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    _rootView.nameLabel.text = [TDSingleton instance].userInfoModel.nickName;
    _rootView.signatureLabel.text = [TDSingleton instance].userInfoModel.info;
}

- (void)initData {
    
//    NSInteger roleType = [TDSingleton instance].userInfoModel.type;
//    /**
//     *  0是普通用户，1是大咖
//     */
//    if (roleType > 0) {
//        _dataSource = @[@"资产", @"约单", @"游戏账号", @"地址管理", @"设置"];//@"大咖审核", @"展示台",
//        _imageNameArray = @[@"setting_asset", @"setting_order", @"setting_game_account", @"setting_address", @""];//@"setting_review", @"setting_game_character",
//    } else {
//        _dataSource = @[@"资产", @"约单", @"游戏账号", @"地址管理"];//, @"大咖审核", @"展示台"
//        _imageNameArray = @[@"setting_asset", @"setting_order", @"setting_game_account", @"setting_address"];//, @"setting_review", @"setting_game_character"
//    }
    _dataSource = @[@"资产", @"约单", @"游戏账号", @"地址管理", @"我要吐槽", @"大咖审核", @"设置"];
    _imageNameArray = @[@"setting_asset", @"setting_order", @"setting_game_account", @"setting_address", @"setting_free_talk", @"setting_review", @"setting_setup"];
}

- (void)initUserInterface {
    [self.view addSubview:self.rootView];
    _rootView.accountLabel.text = [NSString stringWithFormat:@"账号：%@", [TDSingleton instance].account];
}

- (SettingRootView *)rootView {
    if (!_rootView) {
        _rootView = [[SettingRootView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
        [_rootView.portraitImageView sd_setImageWithURL:[NSURL URLWithString:[TDSingleton instance].userInfoModel.portraitURL] placeholderImage:[UIImage imageNamed:@"user_default_head"]];
        [_rootView.portraitButton addTarget:self action:@selector(portraitButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        
        _rootView.tableView.dataSource = self;
        _rootView.tableView.delegate = self;
        
        
        [_rootView.swipeGesture addTarget:self action:@selector(swipeGestureAction)];
    }
    return _rootView;
}

- (void)addTabBarControllerView {
    UITabBarController *tabBarController = [[UITabBarController alloc] init];
    tabBarController.tabBar.tintColor = [UIColor colorWithRed:255/255.f green:145/255.f blue:0 alpha:1];
    
    tabBarController.tabBar.barTintColor = [UIColor colorWithRed:56/255.f green:56/255.f blue:56/255.f alpha:1];
    /**
     *  约玩
     */
    PeopleNearbyViewController * peopleNearbyVC = [[PeopleNearbyViewController alloc] init];
    
    UINavigationController *peopleNearbyNC = [[UINavigationController alloc] initWithRootViewController:peopleNearbyVC];
    
    peopleNearbyNC.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"约玩" image:[UIImage imageNamed:@"tabbar_about_play_normal"] tag:1];
    
    [peopleNearbyNC.navigationBar setTranslucent:NO];
    peopleNearbyNC.edgesForExtendedLayout = UIRectEdgeNone;
    
    /**
     *  消息
     */
    RongCloudMessagesViewController * messagesVC = [[RongCloudMessagesViewController alloc] init];
    UINavigationController *messagesNC = [[UINavigationController alloc] initWithRootViewController:messagesVC];
    
    messagesNC.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"消息" image:[UIImage imageNamed:@"tabbar_message_normal"] tag:2];
    
    [messagesNC.navigationBar setTranslucent:NO];
    messagesNC.edgesForExtendedLayout = UIRectEdgeNone;
    
    [messagesVC view];
    
    /**
     *  好友
     */
    RelationshipsViewController *relationshipVC = [[RelationshipsViewController alloc] init];
    UINavigationController *relationshipNC = [[UINavigationController alloc] initWithRootViewController:relationshipVC];
    
    relationshipNC.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"好友" image:[UIImage imageNamed:@"tabbar_contacts_normal"] tag:3];
    
    [relationshipNC.navigationBar setTranslucent:NO];
    relationshipNC.edgesForExtendedLayout = UIRectEdgeNone;
    
    tabBarController.viewControllers = [[NSArray alloc] initWithObjects:peopleNearbyNC, messagesNC, relationshipNC, nil];
    tabBarController.view.tag = 100;
    
    [self setUnreadMessagesCountWithTabBarController:tabBarController];
    
    [self addChildViewController:tabBarController];
    [self.view addSubview:tabBarController.view];
    
    [TDSingleton instance].isLogin = YES;
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)setUnreadMessagesCountWithTabBarController:(UITabBarController *)tabBarController {
    NSNumber *conversationTypeNumber = [NSNumber numberWithInteger:ConversationType_PRIVATE];
    NSArray *conversationTypeArray = [[NSArray alloc] initWithObjects:conversationTypeNumber, nil];
    
    int count = [[RCIMClient sharedRCIMClient]getUnreadCount:conversationTypeArray];
    
    if (count > 0) {
        [[[[tabBarController tabBar] items] objectAtIndex:1] setBadgeValue:[NSString stringWithFormat:@"%d", count]];
    } else {
        [[[[tabBarController tabBar] items] objectAtIndex:1] setBadgeValue:nil];
    }
}

- (void)dealloc {
    NSLog(@"BackgroundViewController dealloc");
    [[NSNotificationCenter defaultCenter] removeObserver:self name:LGSetUserIcon object:nil];
}

//- (void)logout {
//    NSLog(@"%@", self.navigationController.viewControllers);
//    [self.navigationController popViewControllerAnimated:YES];
//    
//    if (self.navigationController.viewControllers.count > 1) {
//        UIViewController *vc = self.navigationController.viewControllers[1];
//        
//        if ([vc isKindOfClass:[LoginViewController class]]) {
//            [self.navigationController popToViewController:vc animated:NO];
//        }
//    }
//}

- (void)portraitButtonClicked {
    UserCenterViewController *vc = [[UserCenterViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark UITableView delegate method
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 54;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *reuseIdentifier = @"reuseIdentfier";
    
    SettingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    
    if (!cell) {
        cell = [[SettingTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    }

    cell.titleLabel.text = _dataSource[indexPath.row];
    cell.markImageView.image = [UIImage imageNamed:_imageNameArray[indexPath.row]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.row) {
        case 0:
        {
            AssetViewController *vc = [[AssetViewController alloc] init];
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
            
        case 1:
        {
            OrdersViewController *vc = [[OrdersViewController alloc] init];
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
            
        case 2:
        {
            GameAccountViewController *vc = [[GameAccountViewController alloc] init];
            [self.navigationController pushViewController:vc animated:YES];
        }
            
            break;
            
        case 3:
        {
            CommonAddressViewController *vc = [[CommonAddressViewController alloc] init];
            [self.navigationController pushViewController:vc animated:YES];
        }
            
            break;
            
        case 4:
        {
            SendMessageViewController *vc = [[SendMessageViewController alloc]init];
            vc.conversationType = ConversationType_PRIVATE;
            vc.targetId = [NSString stringWithFormat:@"%@", [TDSingleton instance].customerService[@"id"]];
            vc.userName = [NSString stringWithFormat:@"%@", [TDSingleton instance].customerService[@"name"]];
            
            /**
             * 参数说明
             *
             * conversationType 会话类型，此处应该传 RCConversationType.ConversationType_APPSERVICE
             * targetId 已获得的客服 Id
             * userName 客服名称
             * title 客服会话界面 Title
             */
//            RCPublicServiceChatViewController *vc = [[RCPublicServiceChatViewController alloc] init];
//            vc.conversationType = ConversationType_CUSTOMERSERVICE;
//            vc.targetId = @"KEFU145327333218393";
//            vc.userName = [NSString stringWithFormat:@"%@", [TDSingleton instance].customerService[@"name"]];;
//            vc.title = [NSString stringWithFormat:@"%@-title", [TDSingleton instance].customerService[@"name"]];
            
            [self.navigationController pushViewController:vc animated:YES];

            NSLog(@"customer service:%@", [TDSingleton instance].customerService);
        }
            break;
            
        case 5:
        {
            if ([TDSingleton instance].userInfoModel.authentication == 2) {
                DakaReviewViewController * vc = [[DakaReviewViewController alloc] init];
                [self.navigationController pushViewController:vc animated:YES];
            } else {
                [SVProgressHUD showErrorWithStatus:@"请通过身份认证后再来尝试！"];
            }
        }
            
            break;
            
        case 6:
        {
            SetupViewController * vc = [[SetupViewController alloc] init];
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
            
        case 7:
            
            break;
            
        default:
            break;
    }
}

///添加定位服务通知
- (void)addUserIconChangeNotification {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateUserIcon:) name:LGSetUserIcon object:nil];
}

#pragma mark - 头像改变通知
- (void)didUpdateUserIcon:(NSNotification *)notifi {
    NSURL * url = [NSURL URLWithString:notifi.object];
    [self.rootView.portraitImageView sd_setImageWithURL:url];
}

#pragma mark - 滑动手势
- (void)swipeGestureAction {
    UIView *view = [self.view viewWithTag:100];
    
    UIView *maskView = [view viewWithTag:1000];
    
    for (UIGestureRecognizer *recognizer in [maskView gestureRecognizers]) {
        [maskView removeGestureRecognizer:recognizer];
    }
    
    [maskView removeFromSuperview];
    
    [SettingWindowAnimation hideWithView:view];
}

@end
