//
//  SettingViewController.h
//  AfterSchool
//
//  Created by Teson Draw on 12/29/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SettingRootView.h"

@interface SettingViewController : UIViewController

@property (nonatomic, strong) SettingRootView *rootView;

@property (nonatomic, strong) NSArray *dataSource;
@property (nonatomic, strong) NSArray *imageNameArray;

//- (void)logout;

@end
