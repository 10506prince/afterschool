//
//  OrderDetailRootView.m
//  AfterSchool
//
//  Created by Teson Draw on 11/26/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "OrderDetailRootView.h"
#import "MacrosDefinition.h"

@implementation OrderDetailRootView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithRed:235/255.f green:235/255.f blue:235/255.f alpha:1];
        
        [self addSubview:self.navigationBarView];
        [self addSubview:self.userInfoView];

        [self addSubview:self.scrollView];
        [_scrollView addSubview:self.verticalLineView];
        [_scrollView addSubview:self.acceptOrderDateView];
        [_scrollView addSubview:self.orderAcceptView];
        [_scrollView addSubview:self.waitToStartDateView];
        [_scrollView addSubview:self.orderWaitToStartView];
    }
    return self;
}

- (NavigationBarView *)navigationBarView {
    if (!_navigationBarView) {
        
        _navigationBarView = [[NavigationBarView alloc] initWithTitle:@"订单详情"
                                                           titleColor:nil
                                                      backgroundColor:nil
                                              leftButtonBgViewOriginX:0
                                                 leftButtonImageWidth:22
                                                 leftButtonTitleColor:[UIColor whiteColor]
                                                       leftButtonName:@"返回"
                                                  leftButtonImageName:@"navigation_bar_return_button_left_arrow" rightButtonName:nil
                                                 rightButtonImageName:nil];
    }
    return _navigationBarView;
}

- (TDUserBriefInfoView *)userInfoView {
    if (!_userInfoView) {
        _userInfoView = [[TDUserBriefInfoView alloc] initWithFrame:CGRectMake(0, 64, SCREEN_WIDTH, 110)];
    }
    return _userInfoView;
}


- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_userInfoView.frame) + 16, SCREEN_WIDTH, SCREEN_HEIGHT - CGRectGetMaxY(_userInfoView.frame) - 16)];
        _scrollView.backgroundColor = [UIColor colorWithRed:244/255.f green:244/255.f blue:244/255.f alpha:1];
        _scrollView.contentSize = CGSizeMake(SCREEN_WIDTH, SCREEN_HEIGHT - CGRectGetMaxY(_userInfoView.frame) - 16 + 20);
    }
    return _scrollView;
}


//- (UIView *)tableViewBackgroundView {
//    if (!_tableViewBackgroundView) {
//        _tableViewBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_userInfoView.frame) + 16, SCREEN_WIDTH, SCREEN_HEIGHT - CGRectGetMaxY(_userInfoView.frame) - 16)];
//        _tableViewBackgroundView.backgroundColor = [UIColor colorWithRed:244/255.f green:244/255.f blue:244/255.f alpha:1];
//    }
//    return _tableViewBackgroundView;
//}


- (UIView *)verticalLineView {
    if (!_verticalLineView) {
        _verticalLineView = [[UIView alloc] initWithFrame:CGRectMake((_scrollView.bounds.size.width - 2) / 2, 0, 2, _scrollView.bounds.size.height)];
        _verticalLineView.backgroundColor = [UIColor colorWithRed:170/255.f green:170/255.f blue:170/255.f alpha:1];
    }
    return _verticalLineView;
}

- (TDDateView *)acceptOrderDateView {
    if (!_acceptOrderDateView) {
        _acceptOrderDateView = [[TDDateView alloc] initWithOrigin:CGPointMake((_scrollView.bounds.size.width / 2) - 72 + 4, 20) lanyardPosition:LanyardPositonRight];
    }
    return _acceptOrderDateView;
}

- (TDOrderStepView *)orderAcceptView {
    if (!_orderAcceptView) {
        _orderAcceptView = [[TDOrderStepView alloc] initWithOrigin:CGPointMake((_scrollView.bounds.size.width - 200) / 2 - 45, CGRectGetMaxY(_acceptOrderDateView.frame) + 20) backgroundColor:nil];
        _orderAcceptView.imageView.image = [UIImage imageNamed:@"order_step_cancel"];
    }
    return _orderAcceptView;
}

- (TDDateView *)waitToStartDateView {
    if (!_waitToStartDateView) {
        _waitToStartDateView = [[TDDateView alloc] initWithOrigin:CGPointMake((_scrollView.bounds.size.width / 2) - 4, CGRectGetMaxY(_orderAcceptView.frame) + 20) lanyardPosition:LanyardPositonLeft];
    }
    return _waitToStartDateView;
}

- (TDOrderStepView *)orderWaitToStartView {
    if (!_orderWaitToStartView) {
        _orderWaitToStartView = [[TDOrderStepView alloc] initWithOrigin:CGPointMake((_scrollView.bounds.size.width - 200) / 2 + 45, CGRectGetMaxY(_waitToStartDateView.frame) + 20) backgroundColor:COLOR(193, 193, 193)];
    }
    return _orderWaitToStartView;
}

- (TDOrderStepView *)orderEndView {
    if (!_orderEndView) {
        _orderEndView = [[TDOrderStepView alloc] initWithOrigin:CGPointMake((_scrollView.bounds.size.width - 200) / 2 - 45, CGRectGetMaxY(_orderWaitToStartView.frame) + 20) backgroundColor:COLOR(193, 193, 193)];
        _orderEndView.titleLabel.text = @"等待结束";
    }
    return _orderEndView;
}

@end
