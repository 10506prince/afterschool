//
//  OrderDetailRootView.h
//  AfterSchool
//
//  Created by Teson Draw on 11/26/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavigationBarView.h"
#import "TDDateView.h"
#import "TDOrderStepView.h"
#import "TDUserBriefInfoView.h"

@interface OrderDetailRootView : UIView

@property (nonatomic, strong) NavigationBarView  * navigationBarView;

@property (nonatomic, strong) TDUserBriefInfoView *userInfoView;

@property (nonatomic, strong) UIView *verticalLineView;

@property (nonatomic, strong) UIScrollView *scrollView;

@property (nonatomic, strong) TDDateView *acceptOrderDateView;
@property (nonatomic, strong) TDDateView *waitToStartDateView;

@property (nonatomic, strong) TDOrderStepView *orderAcceptView;
@property (nonatomic, strong) TDOrderStepView *orderWaitToStartView;

@property (nonatomic, strong) TDOrderStepView *orderEndView;

@end
