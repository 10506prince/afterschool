//
//  OrderDetailViewController.h
//  AfterSchool
//
//  Created by Teson Draw on 11/26/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderDetailRootView.h"
#import "UserInfoModel.h"
#import "OrderDetailModel.h"
#import "LGDaKaInfo.h"
#import "LGDaKaInfoHelp.h"

@interface OrderDetailViewController : UIViewController

@property (nonatomic, strong) OrderDetailRootView *rootView;
@property (nonatomic, strong) NSDictionary *orderInfo;

//@property (nonatomic, strong) UserInfoModel *userInfoModel;
@property (nonatomic, strong) OrderDetailModel *orderModel;

@property (nonatomic, strong) LGDaKaInfo *bigShotInfo;
@property (nonatomic, assign) NSUInteger orderType;///<0表示进行中的订单，1表示已完成的订单

@end
