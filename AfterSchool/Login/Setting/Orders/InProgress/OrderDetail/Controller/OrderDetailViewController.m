//
//  OrderDetailViewController.m
//  AfterSchool
//
//  Created by Teson Draw on 11/26/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "OrderDetailViewController.h"
#import "PeopleGameDetailViewController.h"
#import "TDNetworkRequest.h"
#import "SVProgressHUD.h"
#import "TDNetworkRequest.h"
#import "UIImageView+WebCache.h"
#import "LGDefineNetServer.h"

@interface OrderDetailViewController () <UIAlertViewDelegate>

@end

@implementation OrderDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    
    [self initUserInterface];
    [self initData];
}

- (void)initData {
    
    _orderModel = [[OrderDetailModel alloc] initWithDictionary:_orderInfo];
    
    
    _rootView.acceptOrderDateView.dateLabel.text = _orderModel.createDate;
    
    _rootView.orderAcceptView.timeLabel.text = _orderModel.createTime;
    _rootView.orderAcceptView.detailLabel.text = [NSString stringWithFormat:@"请等待%@联系你", _orderModel.bigShotName];
    
    _rootView.waitToStartDateView.dateLabel.text = _orderModel.startDate;
    _rootView.orderWaitToStartView.timeLabel.text = _orderModel.startTime;
    
    if (_orderType == 0) {//进行中的订单
        /**
         *  订单已接受，未开始
         */
        if (_orderModel.state == 1) {
            _rootView.orderAcceptView.titleLabel.text = @"待确认";
            
            _rootView.orderAcceptView.imageView.image = [UIImage imageNamed:@"order_step_cancel"];
            
            _rootView.orderWaitToStartView.titleLabel.text = @"等待开始";
            
            long planStartTime = (long)([_orderInfo[@"planStartTime"] longLongValue] / 1000);
            long nowTimestamp = [[NSDate date] timeIntervalSince1970] * 1000;
            
            int hours = (int)((planStartTime - nowTimestamp) / 1000) / 3600;
            int minutes = (((planStartTime - nowTimestamp) / 1000) % 3600) / 60;
            
            
            if (hours < 0 || minutes < 0) {
                _rootView.orderWaitToStartView.detailLabel.text = @"距离开始时间还有0小时0分钟";
            } else {
                if (hours == 0) {
                    _rootView.orderWaitToStartView.detailLabel.text = [NSString stringWithFormat:@"距离开始时间还有%d分钟", minutes];
                } else {
                    _rootView.orderWaitToStartView.detailLabel.text = [NSString stringWithFormat:@"距离开始时间还有%d小时%d分钟", hours, minutes];
                }
            }
            
            [_rootView.orderAcceptView.button addTarget:self action:@selector(cancelOrderButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        }
        
        /**
         *  订单已开始，未结束
         */
        if (_orderModel.state == 3) {
            _rootView.orderAcceptView.titleLabel.text = @"已确认";
            _rootView.orderAcceptView.imageView.image = [UIImage imageNamed:@"order_step_confirm"];
            
            _rootView.orderWaitToStartView.titleLabel.text = @"进行中";
            _rootView.orderWaitToStartView.backgroundColor = [UIColor colorWithRed:180/255.f green:201/255.f blue:60/255.f alpha:1];
            _rootView.orderWaitToStartView.detailLabel.text = [NSString stringWithFormat:@"%@ 启动了订单", _orderModel.bigShotName];
            _rootView.orderWaitToStartView.imageView.image = [UIImage imageNamed:@"order_step_confirm"];
            
            
            [_rootView.scrollView addSubview:_rootView.orderEndView];
//            [_rootView.orderWaitToStartView.button addTarget:self action:@selector(cancelOrderButtonClicked) forControlEvents:UIControlEventTouchUpInside];
            
            long playTime = (long)[_orderInfo[@"planKeepTime"] integerValue] * 3600;
            
            long startTime = [_orderInfo[@"startTime"] longValue];
            
            long nowTimestamp = [[NSDate date] timeIntervalSince1970] * 1000;
            
            long leftTime = playTime - (nowTimestamp - startTime) / 1000;
            
            int hours = (int)(leftTime / 3600);
            int minutes = (leftTime % 3600) / 60;
            
            if (hours < 0 || minutes < 0) {
                _rootView.orderEndView.detailLabel.text = @"距离结束时间还有0小时0分钟";
            } else {
                if (hours == 0) {
                    _rootView.orderEndView.detailLabel.text = [NSString stringWithFormat:@"距离结束时间还有%d分钟", minutes];
                } else {
                    _rootView.orderEndView.detailLabel.text = [NSString stringWithFormat:@"距离结束时间还有%d小时%d分钟", hours, minutes];
                }
            }
        }
    } else {//已完成的订单
        /**
         *  已确认
         */
        _rootView.orderAcceptView.titleLabel.text = @"已确认";
        _rootView.orderAcceptView.imageView.image = [UIImage imageNamed:@"order_step_confirm"];
        
        
        /**
         *  进行中
         */
        _rootView.orderWaitToStartView.titleLabel.text = @"进行中";
        _rootView.orderWaitToStartView.backgroundColor = [UIColor colorWithRed:180/255.f green:201/255.f blue:60/255.f alpha:1];
        _rootView.orderWaitToStartView.imageView.image = [UIImage imageNamed:@"order_step_confirm"];
        _rootView.orderWaitToStartView.detailLabel.text = [NSString stringWithFormat:@"%@ 启动了订单", _orderModel.bigShotName];
        
        /**
         *  已结束
         */
        [_rootView.scrollView addSubview:_rootView.orderEndView];
        _rootView.orderEndView.titleLabel.text = @"已结束";
        _rootView.orderEndView.detailLabel.text = [NSString stringWithFormat:@"%@ 结束了订单", _orderModel.bigShotName];
        _rootView.orderEndView.imageView.image = [UIImage imageNamed:@"order_step_confirm"];
        _rootView.orderEndView.backgroundColor = [UIColor colorWithRed:180/255.f green:201/255.f blue:60/255.f alpha:1];
    }
    
    NSString *account = [NSString stringWithFormat:@"%@", _orderInfo[@"receiverUserName"]];

    [SVProgressHUD showWithStatus:@"拼命加载数据中..." maskType:SVProgressHUDMaskTypeGradient];
    
    [LGDefineNetServer getUsersInfoWithAccounts:@[account] success:^(id result) {
        [SVProgressHUD dismiss];
        
        if ([result isKindOfClass:[NSArray class]]) {
            NSArray *array = result;
            
            if (array.count > 0) {
                _bigShotInfo = [[LGDaKaInfo alloc] initWithDakaInfoHelp:result[0]];
                [self refreshUI];
            }
        }
    } failure:^(NSString *msg) {
        [SVProgressHUD dismissWithError:msg];
    }];
}

- (void)initUserInterface {
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.rootView];
}

- (void)refreshUI {
    [_rootView.userInfoView.portraitImageView sd_setImageWithURL:[NSURL URLWithString:_bigShotInfo.headImageUrl]];
    _rootView.userInfoView.nameLabel.text = _bigShotInfo.nickName;
    
    if (_bigShotInfo.sex == 0) {
        _rootView.userInfoView.sexImageView.image = [UIImage imageNamed:@"sex0"];
    } else {
        _rootView.userInfoView.sexImageView.image = [UIImage imageNamed:@"sex1"];
    }
    
    _rootView.userInfoView.ageLabel.text = [NSString stringWithFormat:@"%lu", (unsigned long)_bigShotInfo.age];
    
    _rootView.userInfoView.horoscopeLabel.text = _bigShotInfo.constellation;
    _rootView.userInfoView.signatureLabel.text = _bigShotInfo.signature.length == 0 ? @"这个家伙很懒，什么都没有留下" : _bigShotInfo.signature;
}

- (OrderDetailRootView *)rootView {
    if (!_rootView) {
        _rootView = [[OrderDetailRootView alloc] initWithFrame:self.view.bounds];
        [_rootView.navigationBarView.leftButton addTarget:self action:@selector(returnButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        [_rootView.userInfoView.button addTarget:self action:@selector(bigShotInfoButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return _rootView;
}

- (void)returnButtonClicked {
    [self.navigationController popViewControllerAnimated:YES];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)cancelOrderButtonClicked {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"注意" message:@"选择“继续”，系统将取消此订单且退款只有原消费总额的80%" delegate:self cancelButtonTitle:@"继续" otherButtonTitles:@"取消", nil];
    [alertView show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        [TDNetworkRequest cancelOrderWithOrderID:_orderModel.orderID timeout:@"0" success:^(NSString *message) {
            [SVProgressHUD dismissWithSuccess:@"取消订单成功！"];
        } failure:^(NSString *error) {
            [SVProgressHUD dismissWithSuccess:error];
        }];
    }
}

- (void)bigShotInfoButtonClicked {
    NSLog(@"bigShotInfoButton Clicked");
    PeopleGameDetailViewController * vc = [[PeopleGameDetailViewController alloc] initWithDakaInfo:_bigShotInfo];
    [self.navigationController pushViewController:vc animated:YES];
}

@end
