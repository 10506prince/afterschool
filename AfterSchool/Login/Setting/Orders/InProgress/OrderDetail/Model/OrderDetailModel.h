//
//  OrderDetailModel.h
//  AfterSchool
//
//  Created by Teson Draw on 11/27/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OrderDetailModel : NSObject

@property (nonatomic, assign) NSUInteger state;

@property (nonatomic, strong) NSString *orderID;
@property (nonatomic, strong) NSString *bigShotAccount;
@property (nonatomic, strong) NSString *bigShotName;

@property (nonatomic, strong) NSString *createDate;
@property (nonatomic, strong) NSString *createTime;

@property (nonatomic, strong) NSString *playDate;
@property (nonatomic, strong) NSString *playTime;

@property (nonatomic, strong) NSString *startDate;
@property (nonatomic, strong) NSString *startTime;


- (instancetype)initWithDictionary:(NSDictionary *)dic;

@end
