//
//  OrderDetailModel.m
//  AfterSchool
//
//  Created by Teson Draw on 11/27/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "OrderDetailModel.h"
#import "TDDateConversion.h"

@implementation OrderDetailModel


- (instancetype)initWithDictionary:(NSDictionary *)dic
{
    self = [super init];
    if (self) {
        _state = [dic[@"state"] integerValue];
        _bigShotAccount = [NSString stringWithFormat:@"%@", dic[@"receiverUserName"]];
        _bigShotName = [NSString stringWithFormat:@"%@", dic[@"receiverNickName"]];
        _orderID = [NSString stringWithFormat:@"%@", dic[@"id"]];
        
        long tempLongValue = (long)([dic[@"createTime"] longLongValue] / 1000);
        _createDate = [TDDateConversion dateFromNumber:tempLongValue dateFormat:@"MM月dd日"];
        _createTime = [TDDateConversion dateFromNumber:tempLongValue dateFormat:@"HH:mm"];
        
        tempLongValue = (long)([dic[@"startTime"] longLongValue] / 1000);
        _startDate = [TDDateConversion dateFromNumber:tempLongValue dateFormat:@"MM月dd日"];
        _startTime = [TDDateConversion dateFromNumber:tempLongValue dateFormat:@"HH:mm"];
    }
    return self;
}

@end
