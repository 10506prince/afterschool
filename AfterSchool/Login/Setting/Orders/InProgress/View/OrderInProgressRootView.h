//
//  OrderInProgressRootView.h
//  AfterSchool
//
//  Created by Teson Draw on 11/26/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderInProgressRootView : UIView

@property (nonatomic, strong) UITableView *tableView;

@end
