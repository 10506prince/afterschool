//
//  OrderTableViewCell.m
//  AfterSchool
//
//  Created by Teson Draw on 11/26/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "OrderTableViewCell.h"

@implementation OrderTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self addSubview:self.nameLabel];
        [self addSubview:self.statusImageView];
        [self addSubview:self.timeLabel];
        [self addSubview:self.statusLabel];
        [self addSubview:self.sumMoneyLabel];
    }
    return self;
}

- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(12, 12, self.bounds.size.width - 12 - 92 - 10, 18)];
        _nameLabel.font = [UIFont systemFontOfSize:16];
        _nameLabel.textColor = [UIColor blackColor];
    }
    return _nameLabel;
}

- (UIImageView *)statusImageView {
    if (!_statusImageView) {
        _statusImageView = [[UIImageView alloc] initWithFrame:CGRectMake(12, CGRectGetMaxY(_nameLabel.frame) + 12, 12, 12)];
    }
    return _statusImageView;
}

- (UILabel *)timeLabel {
    if (!_timeLabel) {
        _timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_statusImageView.frame) + 8, CGRectGetMaxY(_nameLabel.frame) + 12, 200, 12)];
        _timeLabel.font = [UIFont systemFontOfSize:12];
        _timeLabel.textColor = [UIColor colorWithRed:120/255.f green:120/255.f blue:120/255.f alpha:1];
    }
    return _timeLabel;
}

- (UILabel *)statusLabel {
    if (!_statusLabel) {
        _statusLabel = [[UILabel alloc] initWithFrame:CGRectMake((self.bounds.size.width - 92) , 12, 80, 20)];
        _statusLabel.font = [UIFont systemFontOfSize:12];
        _statusLabel.textColor = [UIColor whiteColor];
        _statusLabel.textAlignment = NSTextAlignmentCenter;
        _statusLabel.layer.cornerRadius = 4;
        _statusLabel.layer.masksToBounds = YES;
    }
    return _statusLabel;
}

- (UILabel *)sumMoneyLabel {
    if (!_sumMoneyLabel) {
        _sumMoneyLabel = [[UILabel alloc] initWithFrame:CGRectMake((self.bounds.size.width - 92), 64 - 24, 92, 18)];
        _sumMoneyLabel.font = [UIFont systemFontOfSize:12];
        _sumMoneyLabel.textColor = [UIColor colorWithRed:120/255.f green:120/255.f blue:120/255.f alpha:1];
    }
    return _sumMoneyLabel;
}

@end
