//
//  OrderInProgressViewController.h
//  AfterSchool
//
//  Created by Teson Draw on 11/26/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderInProgressRootView.h"

@interface OrderInProgressViewController : UIViewController

@property (nonatomic, strong) OrderInProgressRootView *rootView;
@property (nonatomic, strong) NSMutableArray *dataSource;
@property (nonatomic, assign) NSInteger pageNumber;

@end
