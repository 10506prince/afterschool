//
//  OrderFinishedViewController.h
//  AfterSchool
//
//  Created by Teson Draw on 11/26/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderFinishedRootView.h"

@interface OrderFinishedViewController : UIViewController

@property (nonatomic, strong) OrderFinishedRootView *rootView;
@property (nonatomic, strong) NSMutableArray *dataSource;
@property (nonatomic, assign) NSInteger pageNumber;

@property (nonatomic, strong) NSIndexPath *selectedIndexPath;

@end
