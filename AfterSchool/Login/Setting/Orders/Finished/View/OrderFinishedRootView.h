//
//  OrderFinishedRootView.h
//  AfterSchool
//
//  Created by Teson Draw on 11/26/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderFinishedRootView : UIView

@property (nonatomic, strong) UITableView *tableView;

@end
