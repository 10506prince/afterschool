//
//  AllOrderViewController.h
//  AfterSchool
//
//  Created by Teson Draw on 12/31/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AllOrderRootView.h"

@interface AllOrderViewController : UIViewController

@property (nonatomic, strong) AllOrderRootView *rootView;
@property (nonatomic, strong) NSMutableArray *dataSource;
@property (nonatomic, assign) NSInteger pageNumber;

@property (nonatomic, strong) NSIndexPath *selectedIndexPath;

@end
