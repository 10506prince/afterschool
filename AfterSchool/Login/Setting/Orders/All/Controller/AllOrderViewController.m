//
//  AllOrderViewController.m
//  AfterSchool
//
//  Created by Teson Draw on 12/31/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "AllOrderViewController.h"
#import "MacrosDefinition.h"
#import "TDNetworkRequest.h"

//#import "NewOrderTableViewCell.h"
#import "PortraitOrderTableViewCell.h"

#import "TDDateConversion.h"
#import "MJRefresh.h"

#import "Common.h"
#import "TalkingData.h"

#import "OrderBriefModel.h"

#import "TDSingleton.h"

#import "NotStartedOrderDetailViewController.h"

#import "OrderLayout.h"

#import "UIImageView+WebCache.h"

#import "NotificationName.h"
#import "SVProgressHUD.h"

@interface AllOrderViewController () <UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate>

@end

@implementation AllOrderViewController

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [TalkingData trackPageBegin:selfControllerName];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [TalkingData trackPageEnd:selfControllerName];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [_rootView.tableView.mj_header beginRefreshing];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initData];
    [self initUserInterface];
    [self initObserver];
}

- (void)initData {
    _dataSource = [[NSMutableArray alloc] init];
    _pageNumber = 1;
}

- (void)initUserInterface {
    self.view.frame = CGRectMake(0, 64 + BUTTON_HEIGHT, SCREEN_WIDTH, SCREEN_HEIGHT - 64 - BUTTON_HEIGHT);
    [self.view addSubview:self.rootView];
}

- (void)initObserver {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshData) name:TDBigShotGetOrder object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshData) name:TDUserCompleteOrderSuccess object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshData) name:TDUserCancelOrderSuccess object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshData) name:TDBigShotStartOrderSuccess object:nil];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (AllOrderRootView *)rootView {
    if (!_rootView) {
        _rootView = [[AllOrderRootView alloc] initWithFrame:self.view.bounds];
        _rootView.tableView.dataSource = self;
        _rootView.tableView.delegate = self;
        
        __weak AllOrderViewController * weakSelf = self;
        
        
        MJRefreshNormalHeader * header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            [weakSelf refreshData];
        }];
        
        [header setTintColor:[UIColor colorWithRed:120/255.f green:120/255.f blue:120/255.f alpha:1]];
        
        
        MJRefreshAutoNormalFooter * footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
            [weakSelf loadMoreData];
        }];
        [footer setTitle:@"没有更多数据了..." forState:MJRefreshStateNoMoreData];
        [footer setTintColor:[UIColor colorWithRed:120/255.f green:120/255.f blue:120/255.f alpha:1]];
        
        _rootView.tableView.mj_footer = footer;
        _rootView.tableView.mj_header = header;
    }
    return _rootView;
}

#pragma mark TableView Protocol Method
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _dataSource.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 148;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString * reuseIdentifier = @"reuseIdentifier";
    
    PortraitOrderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    if (!cell) {
        cell = [[PortraitOrderTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    }
    
    NSDictionary *orderDictionary = _dataSource[indexPath.row];
    
    OrderBriefModel *orderBriefModel = [[OrderBriefModel alloc] initWithDictionary:orderDictionary];
    
    cell.orderStatusLabel.text = orderBriefModel.orderStatus;
    
    if ([orderBriefModel.receiverAccount isEqualToString:[TDSingleton instance].account]) {
        cell.nameLabel.text = orderBriefModel.senderName;
        cell.orderTypeLabel.text = @"应约单";
        
        cell.orderTypeLabel.textColor = [UIColor colorWithRed:38/255.f green:166/255.f blue:154/255.f alpha:1];
//        cell.checkOrderLabel.textColor =  [UIColor colorWithRed:38/255.f green:166/255.f blue:154/255.f alpha:1];
        
//        cell.orderContentLabel.text = [NSString stringWithFormat:@"【%@】向【你】发起约玩", orderBriefModel.senderName];
        
        cell.orderTypeImageView.image = [UIImage imageNamed:@"order_type_be_invited"];
        
        cell.orderStatusLabel.textColor = [UIColor colorWithRed:38/255.f green:166/255.f blue:154/255.f alpha:1];
        
        [cell.orderTypeImageView sd_setImageWithURL:[NSURL URLWithString:orderBriefModel.senderIconURL] placeholderImage:[UIImage imageNamed:@"user_default_head"]];
    } else {
        cell.nameLabel.text = orderBriefModel.receiverName;
        cell.orderTypeLabel.text = @"邀约单";
        
        cell.orderTypeLabel.textColor = [UIColor colorWithRed:1.f green:145/255.f blue:0.f alpha:1];
//        cell.checkOrderLabel.textColor = [UIColor colorWithRed:1.f green:145/255.f blue:0.f alpha:1];
        
//        cell.orderContentLabel.text = [NSString stringWithFormat:@"【你】向【%@】发起约玩", orderBriefModel.receiverName];
        cell.orderTypeImageView.image = [UIImage imageNamed:@"order_type_invite"];
        
        cell.orderStatusLabel.textColor = [UIColor colorWithRed:1.f green:145/255.f blue:0.f alpha:1];

        [cell.orderTypeImageView sd_setImageWithURL:[NSURL URLWithString:orderBriefModel.receiverIconURL] placeholderImage:[UIImage imageNamed:@"user_default_head"]];
    }
    
    if (orderBriefModel.playType == 5) {
        cell.timeLabel.text = [NSString stringWithFormat:@"完成时间：%@", orderBriefModel.orderEndTime];
        cell.orderContentLabel.text = [NSString stringWithFormat:@"目标：%@", orderBriefModel.target];
    } else {
        cell.timeLabel.text = [NSString stringWithFormat:@"开始时间：%@", orderBriefModel.orderStartTime];
        cell.orderContentLabel.text = [NSString stringWithFormat:@"地点：%@", orderBriefModel.address];
    }
    
    cell.amountLabel.text = [NSString stringWithFormat:@"%ld元", (long)orderBriefModel.amount];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NotStartedOrderDetailViewController *vc = [[NotStartedOrderDetailViewController alloc] init];
    vc.orderInfo = _dataSource[indexPath.row];
//    vc.orderType = 1;
    
    [self.navigationController pushViewController:vc animated:YES];
}

- (NSArray<UITableViewRowAction *> *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *orderDictionary = _dataSource[indexPath.row];
    
    UITableViewRowAction *deleteRowAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive title:@"删除" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        
        NSString *orderId = orderDictionary[@"id"];
        
        _selectedIndexPath = indexPath;
        
        [self deleteOrderWithOrderId:orderId];
    }];
    
    NSArray *array = @[deleteRowAction];
    return array;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *orderDictionary = _dataSource[indexPath.row];
    NSInteger orderStatus = [orderDictionary[@"state"] integerValue];

    if (orderStatus == 0 || orderStatus == 2 || orderStatus == 7) {
        return YES;
    } else {
        return NO;
    }
}

- (void)deleteOrderWithOrderId:(NSString *)orderId {
    NSString *message = [NSString stringWithFormat:@"订单编号：%@", orderId];
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"删除订单"
                                                        message:message
                                                       delegate:self
                                              cancelButtonTitle:@"取消"
                                              otherButtonTitles:@"删除", nil];

    [alertView show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
        NSDictionary *orderDictionary = _dataSource[_selectedIndexPath.row];
        NSString *orderId = orderDictionary[@"id"];
        
        [TDNetworkRequest deleteOrderWithOrderId:orderId success:^(NSString *message) {
            [_dataSource removeObjectAtIndex:_selectedIndexPath.row];
            [_rootView.tableView deleteRowsAtIndexPaths:@[_selectedIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        } failure:^(NSString *error) {
            [SVProgressHUD showErrorWithStatus:error];
        }];
    }
}

- (void)refreshData {
//    [_rootView.tableView.mj_header beginRefreshing];
    _pageNumber = 1;
    [_dataSource removeAllObjects];
    [TDNetworkRequest getOrdersWithType:OrderAll
                                   page:_pageNumber
                                success:^(id responseObject)
     {
         NSLog(@"getOrdersWithType:Order All:%@", responseObject);
         NSArray *data = responseObject[@"orders"];
         
         [_dataSource addObjectsFromArray:data];
         
         [_rootView.tableView reloadData];
     }];
    
    [self performSelector:@selector(endRefreshData) withObject:self afterDelay:1];
}

- (void)endRefreshData {
    [_rootView.tableView.mj_header endRefreshing];
}

- (void)loadMoreData {
    _pageNumber++;
    
    [TDNetworkRequest getOrdersWithType:OrderAll
                                   page:_pageNumber
                                success:^(id responseObject)
     {
         NSLog(@"Order All:%@", responseObject);
         NSArray *data = responseObject[@"orders"];
         
         if (data.count > 0) {
             [_dataSource addObjectsFromArray:data];
             [_rootView.tableView reloadData];
             [self performSelector:@selector(endLoadMoreData) withObject:self afterDelay:1];
         } else {
             _pageNumber -= 1;
             [self.rootView.tableView.mj_footer endRefreshingWithNoMoreData];
         }
     }];
}

- (void)endLoadMoreData {
    [_rootView.tableView.mj_footer endRefreshing];
}

@end
