//
//  CancelledOrderViewController.h
//  AfterSchool
//
//  Created by Teson Draw on 12/31/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CancelledOrderRootView.h"

@interface CancelledOrderViewController : UIViewController

@property (nonatomic, strong) CancelledOrderRootView *rootView;
@property (nonatomic, strong) NSMutableArray *dataSource;
@property (nonatomic, assign) NSInteger pageNumber;

@property (nonatomic, strong) NSIndexPath *selectedIndexPath;

@end
