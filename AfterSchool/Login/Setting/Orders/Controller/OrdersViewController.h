//
//  OrdersViewController.h
//  AfterSchool
//
//  Created by lg on 15/10/22.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGViewController.h"
#import "OrdersRootView.h"

#import "NotStartedOrderViewController.h"
#import "OrderInProgressViewController.h"
#import "orderFinishedViewController.h"
#import "CancelledOrderViewController.h"
#import "AllOrderViewController.h"

@interface OrdersViewController : LGViewController

@property (nonatomic, strong) OrdersRootView * rootView;

@property (nonatomic, strong) AllOrderViewController        *allOrderVC;
@property (nonatomic, strong) NotStartedOrderViewController *notStartedVC;
@property (nonatomic, strong) OrderInProgressViewController *inProgressVC;
@property (nonatomic, strong) OrderFinishedViewController   *finishedVC;
@property (nonatomic, strong) CancelledOrderViewController  *cancelledVC;

@end
