//
//  OrdersViewController.m
//  AfterSchool
//
//  Created by lg on 15/10/22.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "OrdersViewController.h"
#import "Common.h"

#import "TalkingData.h"



@interface OrdersViewController () {
    NSArray * _dataSource;
}

@end

@implementation OrdersViewController

- (void)dealloc
{
    NSLogSelfMethodName;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [TalkingData trackPageBegin:selfControllerName];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [TalkingData trackPageEnd:selfControllerName];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initData];
    [self initUserInterface];
}

- (void)initData {
    
}

- (void)initUserInterface {
    [self.view addSubview:self.rootView];
    
    _allOrderVC   = [[AllOrderViewController alloc] init];
    _notStartedVC = [[NotStartedOrderViewController alloc] init];
    _inProgressVC = [[OrderInProgressViewController alloc] init];
    _finishedVC   = [[OrderFinishedViewController alloc] init];
    _cancelledVC  = [[CancelledOrderViewController alloc] init];
    
    [self addChildViewController:_allOrderVC];
    [self addChildViewController:_notStartedVC];
    [self addChildViewController:_finishedVC];
    [self addChildViewController:_inProgressVC];
    [self addChildViewController:_cancelledVC];
    
    [self.view addSubview:_cancelledVC.view];
    [self.view addSubview:_inProgressVC.view];
    [self.view addSubview:_finishedVC.view];
    [self.view addSubview:_notStartedVC.view];
    [self.view addSubview:_allOrderVC.view];
    
//    _rootView.notStartedButton.selected = YES;
}

- (OrdersRootView *)rootView {
    if (!_rootView) {
        _rootView = [[OrdersRootView alloc] initWithFrame:self.view.bounds];
        [_rootView.navigationBarView.leftButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        
//        [_rootView.notStartedButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
//        [_rootView.inProgressButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
//        [_rootView.finishedButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
//        [_rootView.canceledButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        for (int i = 0; i < _rootView.buttonNameArray.count; i++) {
            UIButton *button = [_rootView.scrollView viewWithTag:(100 + i)];
            [button addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        }
    }
    
    return _rootView;
}

- (void)backAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)buttonClicked:(UIButton *)sender {
    for (int i = 0; i < _rootView.buttonNameArray.count; i++) {
        UIButton *button = [_rootView.scrollView viewWithTag:(100 + i)];
        button.selected = NO;
    }
    
    sender.selected = YES;
    
    switch (sender.tag) {
            
        case 100:
            [self.view bringSubviewToFront:_allOrderVC.view];
            break;
            
        case 101:
            [self.view bringSubviewToFront:_notStartedVC.view];
            break;
            
        case 102:
            [self.view bringSubviewToFront:_inProgressVC.view];
            break;
            
        case 103:
            [self.view bringSubviewToFront:_finishedVC.view];
            break;
            
        case 104:
            [self.view bringSubviewToFront:_cancelledVC.view];
            break;
    }
}

@end
