//
//  BigShotOrderModel.m
//  AfterSchool
//
//  Created by Teson Draw on 11/7/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "TodayOrderModel.h"
#import "NSDate+StringFormatter.h"
//#import "NSDate"
#import "NSDate+constellation.h"
#import "TDDateConversion.h"

@implementation TodayOrderModel

- (instancetype)initWithDictionary:(NSDictionary *)dictionary {
    self = [super init];
    
    if (self) {
        _type = [dictionary[@"type"] integerValue];
        _orderID = [NSString stringWithFormat:@"%@", dictionary[@"orderId"]];
        _senderAccount = [NSString stringWithFormat:@"%@", dictionary[@"senderUserName"]];
        _senderName = [NSString stringWithFormat:@"%@", dictionary[@"senderNickName"]];
        _playTypeChinese = [NSString stringWithFormat:@"%@", dictionary[@"info"]];
        _playType = [dictionary[@"sceneId"] integerValue];
        
        if (_playType == 5) {
            _currentDan = [NSString stringWithFormat:@"%@", dictionary[@"sGameGrade"]];
            _targetDan = [NSString stringWithFormat:@"%@", dictionary[@"target"]];
        }
    
        /**
         *  零钱
         */
        if (dictionary[@"money"]) {
            _money = [dictionary[@"money"] integerValue];
        } else {
            _money = 0;
        }
        
        /**
         *  游戏币
         */
        if (dictionary[@"tokenMoney"]) {
            _gameCoin = [dictionary[@"tokenMoney"] integerValue];
        } else {
            _gameCoin = 0;
        }
        
        /**
         *  总金额
         */
        _amount = _money + _gameCoin;

        /**
         *  时长
         */
        long duration = [dictionary[@"planKeepTime"] integerValue];
        _duration = [NSString stringWithFormat:@"%ld小时", (long)duration / 60];
        
        /**
         *  订单开始时间
         */
        long orderStartTime = (long)([dictionary[@"planStartTime"] longLongValue] / 1000);
        if (dictionary[@"planStartTime"]) {
            
            _orderStartTime = [NSString stringWithFormat:@"%@", [TDDateConversion dateFromNumber:orderStartTime dateFormat:@"YYYY-MM-dd HH:mm"]];
        }
        
        /**
         *  订单结束时间
         */
        long endTime = orderStartTime + duration * 60000;
        _orderEndTime = [NSString stringWithFormat:@"%@", [TDDateConversion dateFromNumber:endTime dateFormat:@"YYYY-MM-dd HH:mm"]];
        
        /**
         *  地点
         */
        if (dictionary[@"address"]) {
            _address = [NSString stringWithFormat:@"%@", dictionary[@"address"]];
        }
        
        NSInteger online = [dictionary[@"online"] integerValue];
        if (online == 1) {
            _online = @"线上";
            _address = @"";
        } else {
            _online = @"线下";
        }
        
        if (dictionary[@"senderIcon"]) {
            _portraitURL = [NSString stringWithFormat:@"%@", dictionary[@"senderIcon"]];
        }
        
        if (dictionary[@"startTime"]) {
            _startTime = [dictionary[@"startTime"] longLongValue];
        }
        
        if (dictionary[@"endTime"]) {
            _endTime = [dictionary[@"endTime"] longLongValue];
            _formatEndTime = [[NSDate dateWithTimeIntervalSince1970:_endTime/1000] chineseDatetime];
        }
        
        if (dictionary[@"senderSex"]) {
            _sex = [dictionary[@"senderSex"] integerValue];
        }
        
        if (dictionary[@"timeOut"]) {
            _timeOut = [dictionary[@"timeOut"] integerValue];
        }
        
        if (dictionary[@"senderBirth"]) {
            NSTimeInterval timeInterval = [dictionary[@"senderBirth"] longLongValue];
            NSDate *birthday = [NSDate dateWithTimeIntervalSince1970:timeInterval/1000];
            _age = [NSDate getAgeFromBirthday:birthday];
        }
        
        if (dictionary[@"senderCreditLev"]) {
            _creditLevel = [dictionary[@"senderCreditLev"] integerValue];
        }
        
        _gameServer = dictionary[@"sGameServer"];
    }
    return self;
}

- (NSString *)description {
    NSString *result = [NSString stringWithFormat:@"\nAccount:%@\nName:%@\nOrderId:%@\n", _senderAccount, _senderName, _orderID];
    
    return result;
}

@end
