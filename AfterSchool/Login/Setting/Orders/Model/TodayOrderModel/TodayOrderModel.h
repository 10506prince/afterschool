//
//  BigShotOrderModel.h
//  AfterSchool
//
//  Created by Teson Draw on 11/7/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TodayOrderModel : NSObject

@property (nonatomic, assign) NSUInteger type;

@property (nonatomic, strong) NSString *orderID;

@property (nonatomic, strong) NSString *senderAccount;
@property (nonatomic, strong) NSString *senderName;

@property (nonatomic, strong) NSString *receiverAccount;
@property (nonatomic, strong) NSString *receiverName;

@property (nonatomic, strong) NSString *gameServer;

@property (nonatomic, assign) NSInteger money;
@property (nonatomic, assign) NSInteger gameCoin;
@property (nonatomic, assign) NSInteger reward;
@property (nonatomic, assign) NSInteger amount;

/**
 *  对应情景模式类型
 *
 *  1、无聊
 *  2、约妹纸
 *  3、约男神
 *  4、上积分
 *  5、保晋级
 */
@property (nonatomic, assign) NSInteger playType;
@property (nonatomic, strong) NSString *playTypeChinese;

@property (nonatomic, strong) NSString *currentDan;
@property (nonatomic, strong) NSString *targetDan;

//@property (nonatomic, assign) float planKeepTime;//unit：hour

@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *portraitURL;


@property (nonatomic, strong) NSString *orderStartTime;
@property (nonatomic, strong) NSString *orderEndTime;

@property (nonatomic, strong) NSString *duration;
@property (nonatomic, strong) NSString *online;

@property (nonatomic, assign) long long startTime;
@property (nonatomic, assign) long long endTime;
@property (nonatomic, assign) long long planStartTime;

@property (nonatomic, strong) NSString *formatEndTime;

@property (nonatomic, assign) NSUInteger sex;
@property (nonatomic, assign) NSUInteger age;
@property (nonatomic, assign) NSUInteger creditLevel;
@property (nonatomic, assign) NSUInteger timeOut;

@property (nonatomic, assign) float moneyFromBigShotAccount;

- (instancetype)initWithDictionary:(NSDictionary *)dictionary;

@end
