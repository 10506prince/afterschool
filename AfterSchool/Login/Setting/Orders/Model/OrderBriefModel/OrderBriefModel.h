//
//  OrderBriefModel.h
//  AfterSchool
//
//  Created by Teson Draw on 1/7/16.
//  Copyright © 2016 Teson Draw. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OrderBriefModel : NSObject

@property (nonatomic, strong) NSString *orderID;

@property (nonatomic, strong) NSString *senderAccount;
@property (nonatomic, strong) NSString *senderName;

@property (nonatomic, strong) NSString *receiverAccount;
@property (nonatomic, strong) NSString *receiverName;

@property (nonatomic, assign) NSInteger money;
@property (nonatomic, assign) NSInteger gameCoin;
@property (nonatomic, assign) NSInteger amount;
@property (nonatomic, assign) NSInteger rating;

@property (nonatomic, strong) NSString *orderStartTime;
@property (nonatomic, strong) NSString *orderEndTime;

@property (nonatomic, strong) NSString *duration;

@property (nonatomic, strong) NSString *orderStatus;
@property (nonatomic, assign) NSInteger status;

@property (nonatomic, strong) NSString *address;

@property (nonatomic, assign) NSInteger playType;

@property (nonatomic, assign) NSInteger online;

@property (nonatomic, strong) NSString *target;

@property (nonatomic, strong) NSString *senderIconURL;
@property (nonatomic, strong) NSString *receiverIconURL;


- (instancetype)initWithDictionary:(NSDictionary *)dictionary;

@end
