//
//  OrderBriefInfoModel.m
//  AfterSchool
//
//  Created by Teson Draw on 1/7/16.
//  Copyright © 2016 Teson Draw. All rights reserved.
//

#import "OrderBriefModel.h"
#import "TDDateConversion.h"

@implementation OrderBriefModel

- (instancetype)initWithDictionary:(NSDictionary *)dictionary {
    self = [super init];
    
    if (self) {
        _orderID = [NSString stringWithFormat:@"%@", dictionary[@"id"]];
        
        _senderAccount = [NSString stringWithFormat:@"%@", dictionary[@"senderUserName"]];
        _senderName = [NSString stringWithFormat:@"%@", dictionary[@"senderNickName"]];

        _receiverAccount = [NSString stringWithFormat:@"%@", dictionary[@"receiverUserName"]];
        _receiverName = [NSString stringWithFormat:@"%@", dictionary[@"receiverNickName"]];
        
        /**
         *  零钱
         */
        if (dictionary[@"money"]) {
            _money = [dictionary[@"money"] integerValue];
        } else {
            _money = 0;
        }
        
        /**
         *  游戏币
         */
        if (dictionary[@"tokenMoney"]) {
            _gameCoin = [dictionary[@"tokenMoney"] integerValue];
        } else {
            _gameCoin = 0;
        }
        
        /**
         *  总金额
         */
        _amount = _money + _gameCoin;

        /**
         *  时长
         */
        long duration = [dictionary[@"planKeepTime"] integerValue];
        _duration = [NSString stringWithFormat:@"%ld小时", (long)duration / 60];
        
        /**
         *  约玩类型
         */
        _playType = [dictionary[@"sceneId"] integerValue];
        
        if (_playType == 5) {
            _target = [NSString stringWithFormat:@"%@", dictionary[@"target"]];
        }
        
        /**
         *  订单开始时间
         */
        long orderStartTime = (long)([dictionary[@"planStartTime"] longLongValue] / 1000);
        if (dictionary[@"planStartTime"]) {

            _orderStartTime = [NSString stringWithFormat:@"%@", [TDDateConversion dateFromNumber:orderStartTime dateFormat:@"YYYY-MM-dd HH:mm"]];
        }
        
        /**
         *  订单结束时间
         */
        long endTime = orderStartTime + duration * 60000;
        _orderEndTime = [NSString stringWithFormat:@"%@", [TDDateConversion dateFromNumber:endTime dateFormat:@"YYYY-MM-dd HH:mm"]];
        
        /**
         *  订单状态
         */
        _status = [dictionary[@"state"] integerValue];
        
        switch (_status) {
            case 0:
                _orderStatus = @"未接单";
                break;
                
            case 1:
                _orderStatus = @"已接单";
                break;
                
            case 2:
                _orderStatus = @"已取消";
                break;
                
            case 3:
                _orderStatus = @"进行中";
                break;
                
            case 4:
                _orderStatus = @"已冻结";
                break;
                
            case 5:
                _orderStatus = @"待确认";
                break;
                
            case 6:
                _orderStatus = @"已完成";
                break;
                
            case 7:
                _orderStatus = @"已完成";
                break;
                
            default:
                break;
        }
        
        /**
         *  信用评级
         */
        _rating = [dictionary[@"rating"] integerValue];
        
        
        /**
         *  线上线下和约玩地点
         */
        _online = [dictionary[@"online"] integerValue];
        
        if (_online == 1) {
            _address = @"线上";
        } else {
            _address = dictionary[@"address"];
        }
        
        /**
         *  sender 头像
         */
        if (dictionary[@"senderIcon"]) {
            _senderIconURL = [NSString stringWithFormat:@"%@", dictionary[@"senderIcon"]];
        } else {
            _senderIconURL = @"";
        }
        
        /**
         *  receiver 头像
         */
        if (dictionary[@"receiverIcon"]) {
            _receiverIconURL = [NSString stringWithFormat:@"%@", dictionary[@"receiverIcon"]];
        } else {
            _receiverIconURL = @"";
        }
        
    }
    return self;
}

@end
