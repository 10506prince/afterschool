//
//  OrderStepTableViewCell.m
//  AfterSchool
//
//  Created by Teson Draw on 1/4/16.
//  Copyright © 2016 Teson Draw. All rights reserved.
//

#import "OrderStepTableViewCell.h"
#import "MacrosDefinition.h"
#import "TDPathRef.h"

//cell 高度75
@implementation OrderStepTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = COLOR(230, 230, 230);
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [self addSubview:self.topVerticalLineView];
        [self addSubview:self.bottomVerticalLineView];
        
        [self addSubview:self.colorCircleView];
        [_colorCircleView addSubview:self.centerCircleView];
        
        [self addSubview:self.customBackgroundView];
        [_customBackgroundView addSubview:self.stepInfoLabel];
        [_customBackgroundView addSubview:self.timeLabel];
        
        [self.layer addSublayer:self.leftTriangleLayer];
    }
    return self;
}

- (UIView *)colorCircleView {
    if (!_colorCircleView) {
        _colorCircleView = [[UIView alloc] initWithFrame:CGRectMake(20, 18, 15, 15)];
        _colorCircleView.backgroundColor = [UIColor colorWithRed:153/255.f green:153/255.f blue:153/255.f alpha:1];
        _colorCircleView.layer.cornerRadius = 7.5;
    }
    return _colorCircleView;
}

- (UIView *)centerCircleView {
    if (!_centerCircleView) {
        _centerCircleView = [[UIView alloc] initWithFrame:CGRectMake(2.5, 2.5, 10, 10)];
        _centerCircleView.backgroundColor = [UIColor colorWithRed:230/255.f green:230/255.f blue:230/255.f alpha:1];
        _centerCircleView.layer.cornerRadius = 5;
    }
    return _centerCircleView;
}

- (UIView *)topVerticalLineView {
    if (!_topVerticalLineView) {
        _topVerticalLineView = [[UIView alloc] initWithFrame:CGRectMake(20 + 6.5, 0, 2, 31.5)];
        _topVerticalLineView.backgroundColor = [UIColor colorWithRed:152/255.f green:152/255.f blue:152/255.f alpha:1];
    }
    return _topVerticalLineView;
}

- (UIView *)bottomVerticalLineView {
    if (!_bottomVerticalLineView) {
        _bottomVerticalLineView = [[UIView alloc] initWithFrame:CGRectMake(20 + 6.5, 31.5, 2, 50)];
        _bottomVerticalLineView.backgroundColor = [UIColor colorWithRed:152/255.f green:152/255.f blue:152/255.f alpha:1];
    }
    return _bottomVerticalLineView;
}

- (UIView *)customBackgroundView {
    if (!_customBackgroundView) {
        _customBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_colorCircleView.frame) + 10 + 6, 5, SCREEN_WIDTH - CGRectGetMaxX(_colorCircleView.frame) - 36, 55)];
        _customBackgroundView.backgroundColor = [UIColor whiteColor];
        _customBackgroundView.layer.cornerRadius = 5;
    }
    return _customBackgroundView;
}

- (UILabel *)stepInfoLabel {
    if (!_stepInfoLabel) {
        _stepInfoLabel = [[UILabel alloc] initWithFrame:CGRectMake(13, 14, _customBackgroundView.bounds.size.width - 13, 14)];
        _stepInfoLabel.textColor = [UIColor colorWithRed:56/255.f green:56/255.f blue:56/255.f alpha:1];
        _stepInfoLabel.font = [UIFont systemFontOfSize:14];
    }
    return _stepInfoLabel;
}

- (UILabel *)timeLabel {
    if (!_timeLabel) {
        _timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(13, CGRectGetMaxY(_stepInfoLabel.frame) + 8, 240, 12)];
        _timeLabel.textColor = [UIColor colorWithRed:153/255.f green:153/255.f blue:153/255.f alpha:1];
        _timeLabel.font = [UIFont systemFontOfSize:12];
    }
    return _timeLabel;
}

- (CAShapeLayer *)leftTriangleLayer {
    if (!_leftTriangleLayer) {
        _leftTriangleLayer = [CAShapeLayer layer];
        _leftTriangleLayer.lineWidth = 1;
        _leftTriangleLayer.fillColor = [UIColor whiteColor].CGColor;
        _leftTriangleLayer.strokeColor = [UIColor whiteColor].CGColor;
        
        _leftTriangleLayer.path = [TDPathRef leftTriangleWithStart:CGPointMake(_customBackgroundView.frame.origin.x, _customBackgroundView.frame.origin.y + 15)];
    }
    
    return _leftTriangleLayer;
}

@end
