//
//  NewOrderTableViewCell.h
//  AfterSchool
//
//  Created by Teson Draw on 1/4/16.
//  Copyright © 2016 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PortraitOrderTableViewCell : UITableViewCell

@property (nonatomic, strong) UIImageView *orderTypeImageView;

@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *orderTypeLabel;
@property (nonatomic, strong) UILabel *orderStatusLabel;
@property (nonatomic, strong) UILabel *orderContentLabel;
//@property (nonatomic, strong) UILabel *checkOrderLabel;
//@property (nonatomic, strong) UILabel *startTimeLabel;
@property (nonatomic, strong) UILabel *timeLabel;
@property (nonatomic, strong) UILabel *amountLabel;

@property (nonatomic, strong) UIView *customBackgroundView;
@property (nonatomic, strong) UIView *topDividingLineView;
//@property (nonatomic, strong) UIView *bottomDividingLineView;

//@property (nonatomic, strong) CAShapeLayer *rightArrowLayer;

@end


