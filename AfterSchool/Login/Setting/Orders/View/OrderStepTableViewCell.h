//
//  OrderStepTableViewCell.h
//  AfterSchool
//
//  Created by Teson Draw on 1/4/16.
//  Copyright © 2016 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderStepTableViewCell : UITableViewCell

@property (nonatomic, strong) UIView *customBackgroundView;

@property (nonatomic, strong) UIView *colorCircleView;
@property (nonatomic, strong) UIView *centerCircleView;

@property (nonatomic, strong) UIView *topVerticalLineView;
@property (nonatomic, strong) UIView *bottomVerticalLineView;

@property (nonatomic, strong) UILabel *stepInfoLabel;
@property (nonatomic, strong) UILabel *timeLabel;

@property (nonatomic, strong) CAShapeLayer *leftTriangleLayer;

@end
