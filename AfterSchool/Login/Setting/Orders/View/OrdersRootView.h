//
//  OrdersRootView.h
//  AfterSchool
//
//  Created by lg on 15/10/22.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavigationBarView.h"

#import "TDSwitchButtonView.h"

@interface OrdersRootView : UIView

@property (nonatomic, strong) NavigationBarView * navigationBarView;

//@property (nonatomic, strong) UITableView * tableView;

//@property (nonatomic, strong) UIView *optionMenuView;
//@property (nonatomic, strong) TDSwitchButtonView *switchButtonView;
@property (nonatomic, strong) UIScrollView *scrollView;

@property (nonatomic, strong) UIButton *allButton;
@property (nonatomic, strong) UIButton *notStartedButton;
@property (nonatomic, strong) UIButton *inProgressButton;
@property (nonatomic, strong) UIButton *finishedButton;
@property (nonatomic, strong) UIButton *canceledButton;

@property (nonatomic, strong) NSArray *buttonNameArray;

@end
