//
//  OrdersRootView.m
//  AfterSchool
//
//  Created by lg on 15/10/22.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "OrdersRootView.h"
#import "MacrosDefinition.h"
#import "UIButton+SetBackgroundColor.h"

#import "UIButton+AddLineAtLocation.h"

#import "OrderLayout.h"

@implementation OrdersRootView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = COLOR(230, 230, 230);
        
        [self addSubview:self.navigationBarView];
        [self addSubview:self.scrollView];
        
        _buttonNameArray = @[@"全部", @"新单", @"进行中", @"完成", @"废单"];
        for (int i = 0; i < _buttonNameArray.count; i++) {
            UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(BUTTON_WIDTH * i, 0, BUTTON_WIDTH, BUTTON_HEIGHT)];
            [button setTitle:_buttonNameArray[i] forState:UIControlStateNormal];
            [button setTitleColor:COLOR(56, 56, 56) forState:UIControlStateNormal];
            [button setTitleColor:COLOR(255, 255, 255) forState:UIControlStateSelected];
            [button setBackgroundColor:[UIColor whiteColor]];
            [button setBackgroundColor:[UIColor colorWithRed:251/255.f green:144/255.f blue:0/255.0 alpha:1] forState:UIControlStateSelected];
            button.titleLabel.font = [UIFont systemFontOfSize:BUTTON_FONT_SIZE];
            button.tag = 100 + i;
            
            if (i < (_buttonNameArray.count - 1)) {
                [button addLineAtLocation:LocationRight color:nil];
            }
            
            if (i == 0) {
                button.selected = YES;
            }
            
            [_scrollView addSubview:button];
        }
//        [self addSubview:self.optionMenuView];
//        [_optionMenuView addSubview:self.tableView];
//        [self.optionMenuView addSubview:self.notStartedButton];
//        [self.optionMenuView addSubview:self.inProgressButton];
//        [self.optionMenuView addSubview:self.finishedButton];
//        [self.optionMenuView addSubview:self.canceledButton];
    }
    return self;
}

- (NavigationBarView *)navigationBarView {
    if (!_navigationBarView) {
   
        _navigationBarView = [[NavigationBarView alloc] initWithTitle:@"约单列表"
                                                           titleColor:nil
                                                      backgroundColor:nil
                                              leftButtonBgViewOriginX:0
                                                 leftButtonImageWidth:22
                                                 leftButtonTitleColor:[UIColor whiteColor]
                                                       leftButtonName:@"我"
                                                  leftButtonImageName:@"navigation_bar_return_button_left_arrow" rightButtonName:nil
                                                 rightButtonImageName:nil];
    }
    return _navigationBarView;
}

//- (UIView *)optionMenuView {
//    if (!_optionMenuView) {
//        _optionMenuView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_navigationBarView.frame), SCREEN_WIDTH, 44)];
//        _optionMenuView.backgroundColor = [UIColor whiteColor];
//    }
//    return _optionMenuView;
//}

//- (UITableView *)tableView {
//    if (!_tableView) {
//        _tableView = [[UITableView alloc] initWithFrame:_optionMenuView.bounds];
//        _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
////        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
////        _tableView.backgroundColor = [UIColor colorWithRed:230/255.f green:230/255.f blue:230/255.f alpha:1];
//    }
//    return _tableView;
//}

- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 64, SCREEN_WIDTH, BUTTON_HEIGHT)];
        _scrollView.contentSize = CGSizeMake(5 * BUTTON_WIDTH, BUTTON_HEIGHT);
        _scrollView.showsHorizontalScrollIndicator = NO;
    }
    return _scrollView;
}

- (UIButton *)notStartedButton {
    if (!_notStartedButton) {
        _notStartedButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, BUTTON_WIDTH, 44)];
        [_notStartedButton setTitle:@"新单" forState:UIControlStateNormal];
        [_notStartedButton setTitleColor:COLOR(56, 56, 56) forState:UIControlStateNormal];
        [_notStartedButton setTitleColor:COLOR(255, 255, 255) forState:UIControlStateSelected];
        [_notStartedButton setBackgroundColor:[UIColor whiteColor]];
        [_notStartedButton setBackgroundColor:[UIColor colorWithRed:251/255.f green:144/255.f blue:0/255.0 alpha:1] forState:UIControlStateSelected];
        _notStartedButton.titleLabel.font = [UIFont systemFontOfSize:BUTTON_FONT_SIZE];
        _notStartedButton.tag = 100;
        
        [_notStartedButton addLineAtLocation:LocationRight color:nil];
        
//        _notStartedButton.layer.borderColor = COLOR(230, 230, 230).CGColor;
//        _notStartedButton.layer.borderWidth = 1;
    }
    
    return _notStartedButton;
}

- (UIButton *)allButton {
    if (!_allButton) {
        _allButton = [[UIButton alloc] initWithFrame:CGRectMake(-1, 0, BUTTON_WIDTH + 2, 44)];
        [_allButton setTitle:@"全部" forState:UIControlStateNormal];
        [_allButton setTitleColor:COLOR(56, 56, 56) forState:UIControlStateNormal];
        [_allButton setTitleColor:COLOR(255, 255, 255) forState:UIControlStateSelected];
        [_allButton setBackgroundColor:[UIColor whiteColor]];
        [_allButton setBackgroundColor:[UIColor colorWithRed:251/255.f green:144/255.f blue:0/255.0 alpha:1] forState:UIControlStateSelected];
        _allButton.titleLabel.font = [UIFont systemFontOfSize:BUTTON_FONT_SIZE];
        _allButton.tag = 100;
        
        
//        _allButton.layer.borderColor = COLOR(230, 230, 230).CGColor;
//        _allButton.layer.borderWidth = 1;
    }
    
    return _allButton;
}

- (UIButton *)inProgressButton {
    if (!_inProgressButton) {
        _inProgressButton = [[UIButton alloc] initWithFrame:CGRectMake(BUTTON_WIDTH, 0, BUTTON_WIDTH, 44)];
        [_inProgressButton setTitle:@"进行中" forState:UIControlStateNormal];
        [_inProgressButton setTitleColor:COLOR(56, 56, 56) forState:UIControlStateNormal];
        [_inProgressButton setTitleColor:COLOR(255, 255, 255) forState:UIControlStateSelected];
        [_inProgressButton setBackgroundColor:[UIColor whiteColor]];
        [_inProgressButton setBackgroundColor:[UIColor colorWithRed:251/255.f green:144/255.f blue:0/255.0 alpha:1] forState:UIControlStateSelected];
        _inProgressButton.titleLabel.font = [UIFont systemFontOfSize:BUTTON_FONT_SIZE];
        _inProgressButton.tag = 101;
        
        [_inProgressButton addLineAtLocation:LocationRight color:nil];
//        _inProgressButton.layer.borderColor = COLOR(230, 230, 230).CGColor;
//        _inProgressButton.layer.borderWidth = 1;
    }
    
    return _inProgressButton;
}

- (UIButton *)finishedButton {
    if (!_finishedButton) {
        _finishedButton = [[UIButton alloc] initWithFrame:CGRectMake(BUTTON_WIDTH * 2, 0, BUTTON_WIDTH, 44)];
        [_finishedButton setTitle:@"完成" forState:UIControlStateNormal];
        [_finishedButton setTitleColor:COLOR(56, 56, 56) forState:UIControlStateNormal];
        [_finishedButton setTitleColor:COLOR(255, 255, 255) forState:UIControlStateSelected];
        [_finishedButton setBackgroundColor:[UIColor whiteColor]];
        [_finishedButton setBackgroundColor:[UIColor colorWithRed:251/255.f green:144/255.f blue:0/255.0 alpha:1] forState:UIControlStateSelected];
        _finishedButton.titleLabel.font = [UIFont systemFontOfSize:BUTTON_FONT_SIZE];
        _finishedButton.tag = 102;
        
        [_finishedButton addLineAtLocation:LocationRight color:nil];
//        _finishedButton.layer.borderColor = COLOR(230, 230, 230).CGColor;
//        _finishedButton.layer.borderWidth = 1;
    }
    
    return _finishedButton;
}

- (UIButton *)canceledButton {
    if (!_canceledButton) {
        _canceledButton = [[UIButton alloc] initWithFrame:CGRectMake(BUTTON_WIDTH * 3, 0, BUTTON_WIDTH, 44)];
        [_canceledButton setTitle:@"废单" forState:UIControlStateNormal];
        [_canceledButton setTitleColor:COLOR(56, 56, 56) forState:UIControlStateNormal];
        [_canceledButton setTitleColor:COLOR(255, 255, 255) forState:UIControlStateSelected];
        [_canceledButton setBackgroundColor:[UIColor whiteColor]];
        [_canceledButton setBackgroundColor:[UIColor colorWithRed:251/255.f green:144/255.f blue:0/255.0 alpha:1] forState:UIControlStateSelected];
        _canceledButton.titleLabel.font = [UIFont systemFontOfSize:BUTTON_FONT_SIZE];
        _canceledButton.tag = 103;
        
//        _canceledButton.layer.borderColor = COLOR(230, 230, 230).CGColor;
//        _canceledButton.layer.borderWidth = 1;
    }
    
    return _canceledButton;
}

@end
