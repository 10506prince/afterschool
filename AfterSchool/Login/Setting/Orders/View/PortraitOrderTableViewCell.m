//
//  NewOrderTableViewCell.m
//  AfterSchool
//
//  Created by Teson Draw on 1/4/16.
//  Copyright © 2016 Teson Draw. All rights reserved.
//

#import "PortraitOrderTableViewCell.h"
#import "MacrosDefinition.h"
#import "TDPathRef.h"

@implementation PortraitOrderTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = COLOR(230, 230, 230);
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [self addSubview:self.customBackgroundView];
        [_customBackgroundView addSubview:self.orderTypeImageView];
        [_customBackgroundView addSubview:self.nameLabel];
        
        [_customBackgroundView addSubview:self.orderTypeLabel];
        
        [_customBackgroundView addSubview:self.topDividingLineView];
        [_customBackgroundView addSubview:self.timeLabel];
        [_customBackgroundView addSubview:self.orderContentLabel];
        
//        [_customBackgroundView addSubview:self.bottomDividingLineView];
//        [_customBackgroundView addSubview:self.checkOrderLabel];
        
        [_customBackgroundView addSubview:self.amountLabel];
        [_customBackgroundView addSubview:self.orderStatusLabel];
        
//        [_customBackgroundView.layer addSublayer:self.rightArrowLayer];
    }
    return self;
}

- (UIView *)customBackgroundView {
    if (!_customBackgroundView) {
        _customBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 5, SCREEN_WIDTH, 138)];
        _customBackgroundView.backgroundColor = [UIColor whiteColor];
    }
    return _customBackgroundView;
}

- (UIImageView *)orderTypeImageView {
    if (!_orderTypeImageView) {
        _orderTypeImageView = [[UIImageView alloc] initWithFrame:CGRectMake(20, 10, 33, 33)];
        _orderTypeImageView.layer.cornerRadius = 5;
    }
    return _orderTypeImageView;
}

- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_orderTypeImageView.frame) + 10, 16, 120, 19)];
        _nameLabel.textColor = [UIColor colorWithRed:56/255.f green:56/255.f blue:56/255.f alpha:1];
        _nameLabel.font = [UIFont systemFontOfSize:16];
    }
    return _nameLabel;
}

- (UILabel *)orderTypeLabel {
    if (!_orderTypeLabel) {
        _orderTypeLabel = [[UILabel alloc] initWithFrame:CGRectMake(_customBackgroundView.bounds.size.width - 20 - 80, 17, 80, 14)];
        _orderTypeLabel.font = [UIFont systemFontOfSize:14];
        _orderTypeLabel.textAlignment = NSTextAlignmentRight;
    }
    return _orderTypeLabel;
}

- (UILabel *)orderStatusLabel {
    if (!_orderStatusLabel) {
        _orderStatusLabel = [[UILabel alloc] initWithFrame:CGRectMake(_customBackgroundView.bounds.size.width - 20 - 100, CGRectGetMaxY(_amountLabel.frame) + 11, 100, 14)];
        _orderStatusLabel.font = [UIFont systemFontOfSize:14];
        _orderStatusLabel.textAlignment = NSTextAlignmentRight;
        _orderStatusLabel.textColor = [UIColor colorWithRed:1.f green:145/255.f blue:0.f alpha:1];
    }
    return _orderStatusLabel;
}

- (UIView *)topDividingLineView {
    if (!_topDividingLineView) {
        _topDividingLineView = [[UIView alloc] initWithFrame:CGRectMake(20, CGRectGetMaxY(_orderTypeImageView.frame) + 10, _customBackgroundView.bounds.size.width - 40, 0.5)];
        _topDividingLineView.backgroundColor = COLOR(230, 230, 230);
    }
    return _topDividingLineView;
}

- (UILabel *)timeLabel {
    if (!_timeLabel) {
        _timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, CGRectGetMaxY(_topDividingLineView.frame) + 20, _customBackgroundView.bounds.size.width - 80, 16)];
        _timeLabel.textColor = COLOR(153, 153, 153);
        _timeLabel.font = [UIFont systemFontOfSize:14];
    }
    return _timeLabel;
}

//- (UILabel *)startTimeLabel {
//    if (!_startTimeLabel) {
//        _startTimeLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, CGRectGetMaxY(_orderContentLabel.frame) + 10, _customBackgroundView.bounds.size.width - 40, 16)];
//        _startTimeLabel.textColor = COLOR(153, 153, 153);
//        _startTimeLabel.font = [UIFont systemFontOfSize:14];
//    }
//    return _startTimeLabel;
//}

- (UILabel *)orderContentLabel {
    if (!_orderContentLabel) {
        _orderContentLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, CGRectGetMaxY(_timeLabel.frame) + 11, _customBackgroundView.bounds.size.width - 80, 16)];
        _orderContentLabel.textColor = COLOR(153, 153, 153);
        _orderContentLabel.font = [UIFont systemFontOfSize:14];
    }
    return _orderContentLabel;
}

- (UILabel *)amountLabel {
    if (!_amountLabel) {
        _amountLabel = [[UILabel alloc] initWithFrame:CGRectMake(_customBackgroundView.bounds.size.width - 20 - 100, CGRectGetMaxY(_topDividingLineView.frame) + 20, 100, 16)];
        _amountLabel.textColor = COLOR(56, 56, 56);
        _amountLabel.font = [UIFont systemFontOfSize:14];
        _amountLabel.textAlignment = NSTextAlignmentRight;
    }
    return _amountLabel;
}

//- (UIView *)bottomDividingLineView {
//    if (!_bottomDividingLineView) {
//        _bottomDividingLineView = [[UIView alloc] initWithFrame:CGRectMake(20, _customBackgroundView.bounds.size.height - 38, _customBackgroundView.bounds.size.width - 40, 0.5)];
//        _bottomDividingLineView.backgroundColor = COLOR(230, 230, 230);
//    }
//    return _bottomDividingLineView;
//}

//- (UILabel *)checkOrderLabel {
//    if (!_checkOrderLabel) {
//        _checkOrderLabel = [[UILabel alloc] initWithFrame:CGRectMake(_customBackgroundView.bounds.size.width - 104, CGRectGetMaxY(_bottomDividingLineView.frame), 72, _customBackgroundView.bounds.size.height - CGRectGetMaxY(_bottomDividingLineView.frame))];
//        _checkOrderLabel.font = [UIFont systemFontOfSize:16];
//        _checkOrderLabel.text = @"查看约单";
//        _checkOrderLabel.textAlignment = NSTextAlignmentRight;
////        _checkOrderLabel.layer.borderColor = [UIColor blackColor].CGColor;
////        _checkOrderLabel.layer.borderWidth = 1;
//    }
//    return _checkOrderLabel;
//}
//
//- (CAShapeLayer *)rightArrowLayer {
//    if (!_rightArrowLayer) {
//        _rightArrowLayer = [CAShapeLayer layer];
//        _rightArrowLayer.lineWidth = 2;
//        _rightArrowLayer.fillColor = [UIColor clearColor].CGColor;
//        _rightArrowLayer.strokeColor = [UIColor lightGrayColor].CGColor;
//        
//        CGFloat centerY = (_customBackgroundView.bounds.size.height - CGRectGetMaxY(_bottomDividingLineView.frame)) / 2;
//        CGFloat pointY = _customBackgroundView.bounds.size.height - centerY - 7;
//        
//        _rightArrowLayer.path = [TDPathRef rightArrowWithStart:CGPointMake(_customBackgroundView.bounds.size.width - 15 - 7, pointY)];
//    }
//    
//    return _rightArrowLayer;
//}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
