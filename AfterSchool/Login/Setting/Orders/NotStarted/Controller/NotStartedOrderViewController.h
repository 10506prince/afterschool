//
//  NotStartedOrderViewController.h
//  AfterSchool
//
//  Created by Teson Draw on 12/31/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NotStartedOrderRootView.h"

@interface NotStartedOrderViewController : UIViewController

@property (nonatomic, strong) NotStartedOrderRootView *rootView;

@property (nonatomic, strong) NSMutableArray *dataSource;
@property (nonatomic, assign) NSInteger pageNumber;

@end
