//
//  NotStartedOrderRootView.h
//  AfterSchool
//
//  Created by Teson Draw on 12/31/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotStartedOrderRootView : UIView

@property (nonatomic, strong) UITableView *tableView;

@end
