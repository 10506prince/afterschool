//
//  NotStartedOrderDetailViewController.h
//  AfterSchool
//
//  Created by Teson Draw on 1/4/16.
//  Copyright © 2016 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NotStartedOrderDetailRootView.h"
#import "OrderBriefModel.h"

@interface NotStartedOrderDetailViewController : UIViewController

@property (nonatomic, strong) NotStartedOrderDetailRootView *rootView;

@property (nonatomic, strong) NSDictionary *orderInfo;

@property (nonatomic, strong) NSArray *dataSource;
@property (nonatomic, strong) NSMutableArray *orderInfoArray;

@property (nonatomic, strong) OrderBriefModel *orderBriefModel;

@property (nonatomic, assign) BOOL otherPeopleOrder;

/**
 *  订单类型
 *  1、新单
 *  2、进行中的订单
 *  3、完成的订单
 *  4、废单
 */
@property (nonatomic, assign) NSInteger orderType;

@end
