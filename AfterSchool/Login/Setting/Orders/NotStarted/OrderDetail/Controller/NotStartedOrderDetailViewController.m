//
//  NotStartedOrderViewController.m
//  AfterSchool
//
//  Created by Teson Draw on 1/4/16.
//  Copyright © 2016 Teson Draw. All rights reserved.
//

#import "NotStartedOrderDetailViewController.h"
#import "MacrosDefinition.h"

#import "MJRefresh.h"
#import "TDNetworkRequest.h"
#import "TDSingleton.h"
#import "TDNetworking.h"
#import "SVProgressHUD.h"

#import "OrderStepTableViewCell.h"
#import "ComplaintViewController.h"

#import "NotificationName.h"
#import "SendMessageViewController.h"

#import "TDAttributedString.h"

#import "Common.h"
#import "TalkingData.h"
#import "UIImageView+WebCache.h"

#import "LGDaKaInfo.h"

#import "PeopleGameDetailViewController.h"

@interface NotStartedOrderDetailViewController () <UITableViewDataSource, UITableViewDelegate, RatingBarDelegate, UIAlertViewDelegate>

@end

@implementation NotStartedOrderDetailViewController

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [TalkingData trackPageBegin:selfControllerName];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [TalkingData trackPageEnd:selfControllerName];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initData];
    [self initUserInterface];
}

- (void)initData {
    _orderBriefModel = [[OrderBriefModel alloc] initWithDictionary:_orderInfo];
    _orderInfoArray = [[NSMutableArray alloc] init];
    
    _otherPeopleOrder = NO;
    
    /**
     *  内容
     */
    NSString *content;
    if ([_orderBriefModel.receiverAccount isEqualToString:[TDSingleton instance].account]) {
        
        content = [NSString stringWithFormat:@"【%@】向【你】发起约玩", _orderBriefModel.senderName];
        
    } else if ([_orderBriefModel.senderAccount isEqualToString:[TDSingleton instance].account]) {
        
        content = [NSString stringWithFormat:@"【你】向【%@】发起约玩", _orderBriefModel.receiverName];
        
    } else {
        _otherPeopleOrder = YES;
        content = [NSString stringWithFormat:@"【%@】向【%@】发起约玩", _orderBriefModel.senderName, _orderBriefModel.receiverName];
    }

    [_orderInfoArray addObject:[TDAttributedString fromForePartString:@"内容：" afterPartString:content]];
    
    if (_orderBriefModel.playType == 5) {

        /**
         *  开始时间
         */
        [_orderInfoArray addObject:[TDAttributedString fromString:[NSString stringWithFormat:@"开始时间：%@", _orderBriefModel.orderStartTime] indexString:@"："]];
        
        /**
         *  完成时间
         */
        [_orderInfoArray addObject:[TDAttributedString fromString:[NSString stringWithFormat:@"完成时间：%@", _orderBriefModel.orderEndTime] indexString:@"："]];
        
        /**
         *  保级目标
         */
        [_orderInfoArray addObject:[TDAttributedString fromString:[NSString stringWithFormat:@"保级目标：%@", _orderBriefModel.target] indexString:@"："]];
        
        /**
         *  赏金
         */
        [_orderInfoArray addObject:[TDAttributedString fromString:[NSString stringWithFormat:@"赏金：%ld.00", (long)_orderBriefModel.amount]  indexString:@"："]];

        /**
         *  地点
         */
        [_orderInfoArray addObject:[TDAttributedString fromString:[NSString stringWithFormat:@"地点：%@", _orderBriefModel.address] indexString:@"："]];
        
        /**
         *  状态
         */
        [_orderInfoArray addObject:[TDAttributedString fromString:[NSString stringWithFormat:@"状态：%@", _orderBriefModel.orderStatus] indexString:@"："]];
        
    } else {
        /**
         *  时间
         */
        [_orderInfoArray addObject:[TDAttributedString fromString:[NSString stringWithFormat:@"时间：%@", _orderBriefModel.orderStartTime] indexString:@"："]];
        
        /**
         *  时长
         */
        [_orderInfoArray addObject:[TDAttributedString fromString:[NSString stringWithFormat:@"时长：%@", _orderBriefModel.duration] indexString:@"："]];
        
        /**
         *  赏金
         */
        [_orderInfoArray addObject:[TDAttributedString fromString:[NSString stringWithFormat:@"赏金：%ld.00", (long)_orderBriefModel.amount] indexString:@"："]];
        
        /**
         *  地点
         */
        [_orderInfoArray addObject:[TDAttributedString fromString:[NSString stringWithFormat:@"地点：%@", _orderBriefModel.address] indexString:@"："]];
        
        /**
         *  状态
         */
        [_orderInfoArray addObject:[TDAttributedString fromString:[NSString stringWithFormat:@"状态：%@", _orderBriefModel.orderStatus] indexString:@"："]];
    }
}

- (void)initUserInterface {
    [self.view addSubview:self.rootView];

    /**
     *  应约单
     */
    if ([_orderBriefModel.receiverAccount isEqualToString:[TDSingleton instance].account]) {
        _rootView.nameLabel.text = _orderBriefModel.senderName;
        
        [_rootView.portraitImageView sd_setImageWithURL:[NSURL URLWithString:_orderBriefModel.senderIconURL] placeholderImage:[UIImage imageNamed:@"user_default_head"]];
        
        switch (_orderBriefModel.status) {
            case 1://已接单
                [_rootView.detailBackgroundView addSubview:_rootView.bigShotStartOrderButton];
                [_rootView.detailBackgroundView addSubview:_rootView.bigShotCancelOrderButton];

                [_rootView.bigShotStartOrderButton addTarget:self action:@selector(bigShotStartOrderButtonClicked) forControlEvents:UIControlEventTouchUpInside];

                [_rootView.bigShotCancelOrderButton addTarget:self action:@selector(bigShotCancelOrderButtonClicked) forControlEvents:UIControlEventTouchUpInside];
                break;
                
            case 2:
                
                break;
                
            case 3:
                [_rootView.detailBackgroundView addSubview:_rootView.bigShotCompleteOrderButton];
                [_rootView.bigShotCompleteOrderButton addTarget:self action:@selector(bigShotCompleteOrderButtonClicked) forControlEvents:UIControlEventTouchUpInside];
                break;
                
            case 4:
                
                break;
                
            case 5:
                
                break;
                
            case 6:
                
                break;
                
            case 7:
                
                break;
        }
    } else if (![_orderBriefModel.receiverAccount isEqualToString:[TDSingleton instance].account]) {
        /**
         *  邀约单
         */
        _rootView.nameLabel.text = _orderBriefModel.receiverName;
        
        [_rootView.portraitImageView sd_setImageWithURL:[NSURL URLWithString:_orderBriefModel.receiverIconURL] placeholderImage:[UIImage imageNamed:@"user_default_head"]];
        
        switch (_orderBriefModel.status) {
            case 1:
                [_rootView.detailBackgroundView addSubview:_rootView.userCancelOrderButton];
                [_rootView.userCancelOrderButton addTarget:self action:@selector(userCancelOrderButtonClicked) forControlEvents:UIControlEventTouchUpInside];
                break;
                
            case 2:
                
                break;
                
            case 3:
                
                break;
                
            case 4:
                
                break;
                
            case 5:
                [_rootView.detailBackgroundView addSubview:_rootView.userCompleteOrderButton];
                [_rootView.userCompleteOrderButton addTarget:self action:@selector(userCompleteOrderButtonClicked) forControlEvents:UIControlEventTouchUpInside];
                break;
                
            case 6:
                [_rootView.detailBackgroundView addSubview:_rootView.ratingTipLabel];
                [_rootView.detailBackgroundView addSubview:_rootView.ratingBar];
                _rootView.ratingBar.delegate = self;

                if (_orderBriefModel.rating > 0) {
                    [_rootView.ratingBar displayRating:_orderBriefModel.rating];
                    _rootView.ratingBar.userInteractionEnabled = NO;
                    _rootView.ratingTipLabel.text = @"信用等级";
                }

                [_rootView.navigationBarView addSubview:_rootView.navigationBarView.rightButton];
                [_rootView.navigationBarView.rightButton setTitle:@"投诉" forState:UIControlStateNormal];
                [_rootView.navigationBarView.rightButton addTarget:self action:@selector(complaintButtonClicked) forControlEvents:UIControlEventTouchUpInside];
                break;
                
            case 7:
                
                break;
        }
    } else {
        /**
         *  其他人的订单
         */
        _rootView.nameLabel.text = _orderBriefModel.receiverName;
        
        [_rootView.portraitImageView sd_setImageWithURL:[NSURL URLWithString:_orderBriefModel.receiverIconURL] placeholderImage:[UIImage imageNamed:@"user_default_head"]];
    }

    if (_orderBriefModel.status == 0 || _otherPeopleOrder == YES) {
        [_rootView.dialPhoneButton removeFromSuperview];
        [_rootView.sendMessageButton removeFromSuperview];
    }
    
    [_rootView.orderInfoTableView reloadData];
    
    
    if (!_otherPeopleOrder) {
        [self requestOrderStepInfo];
    }
}

- (void)refreshUI {
    _orderBriefModel = [[OrderBriefModel alloc] initWithDictionary:_orderInfo];
    [_orderInfoArray removeAllObjects];
    
    /**
     *  内容
     */
    NSString *content;
    if ([_orderBriefModel.receiverAccount isEqualToString:[TDSingleton instance].account]) {
        
        content = [NSString stringWithFormat:@"【%@】向【你】发起约玩", _orderBriefModel.senderName];
    } else {
        content = [NSString stringWithFormat:@"【你】向【%@】发起约玩", _orderBriefModel.receiverName];
    }
    
    [_orderInfoArray addObject:[TDAttributedString fromForePartString:@"内容：" afterPartString:content]];
    
    if (_orderBriefModel.playType == 5) {
        
        /**
         *  开始时间
         */
        [_orderInfoArray addObject:[TDAttributedString fromString:[NSString stringWithFormat:@"开始时间：%@", _orderBriefModel.orderStartTime] indexString:@"："]];
        
        /**
         *  完成时间
         */
        [_orderInfoArray addObject:[TDAttributedString fromString:[NSString stringWithFormat:@"完成时间：%@", _orderBriefModel.orderEndTime] indexString:@"："]];
        
        /**
         *  保级目标
         */
        [_orderInfoArray addObject:[TDAttributedString fromString:[NSString stringWithFormat:@"保级目标：%@", _orderBriefModel.target] indexString:@"："]];
        
        /**
         *  赏金
         */
        [_orderInfoArray addObject:[TDAttributedString fromString:[NSString stringWithFormat:@"赏金：%ld.00", (long)_orderBriefModel.amount]  indexString:@"："]];
        
        /**
         *  地点
         */
        [_orderInfoArray addObject:[TDAttributedString fromString:[NSString stringWithFormat:@"地点：%@", _orderBriefModel.address] indexString:@"："]];
        
        /**
         *  状态
         */
        [_orderInfoArray addObject:[TDAttributedString fromString:[NSString stringWithFormat:@"状态：%@", _orderBriefModel.orderStatus] indexString:@"："]];
        
    } else {
        /**
         *  时间
         */
        [_orderInfoArray addObject:[TDAttributedString fromString:[NSString stringWithFormat:@"时间：%@", _orderBriefModel.orderStartTime] indexString:@"："]];
        
        /**
         *  时长
         */
        [_orderInfoArray addObject:[TDAttributedString fromString:[NSString stringWithFormat:@"时长：%@", _orderBriefModel.duration] indexString:@"："]];
        
        /**
         *  赏金
         */
        [_orderInfoArray addObject:[TDAttributedString fromString:[NSString stringWithFormat:@"赏金：%ld.00", (long)_orderBriefModel.amount] indexString:@"："]];
        
        /**
         *  地点
         */
        [_orderInfoArray addObject:[TDAttributedString fromString:[NSString stringWithFormat:@"地点：%@", _orderBriefModel.address] indexString:@"："]];
        
        /**
         *  状态
         */
        [_orderInfoArray addObject:[TDAttributedString fromString:[NSString stringWithFormat:@"状态：%@", _orderBriefModel.orderStatus] indexString:@"："]];
    }
    
    [_rootView.orderInfoTableView reloadData];
    [_rootView.orderStepTableView reloadData];
}

- (NotStartedOrderDetailRootView *)rootView {
    if (!_rootView) {
        _rootView = [[NotStartedOrderDetailRootView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
        
        [_rootView.navigationBarView.leftButton addTarget:self action:@selector(returnButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        
        [_rootView.portraitButton addTarget:self action:@selector(portraitButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        
        _rootView.orderStepTableView.dataSource = self;
        _rootView.orderStepTableView.delegate = self;
        
        _rootView.orderInfoTableView.dataSource = self;
        _rootView.orderInfoTableView.delegate = self;
        
        [_rootView.dialPhoneButton addTarget:self action:@selector(dialPhoneButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        [_rootView.sendMessageButton addTarget:self action:@selector(sendMessageButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    }
    return _rootView;
}

- (void)returnButtonClicked {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)complaintButtonClicked {
    NSLog(@"complaintButtonClicked");
    ComplaintViewController *vc = [[ComplaintViewController alloc] init];
    vc.orderInfo = _orderInfo;
    
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)portraitButtonClicked {
    LGDaKaInfo *dakaInfo = [[LGDaKaInfo alloc] init];
    
    if ([_orderBriefModel.receiverAccount isEqualToString:[TDSingleton instance].account]) {
        dakaInfo.account = _orderBriefModel.senderAccount;
        dakaInfo.nickName = _orderBriefModel.senderName;
    } else {
        dakaInfo.account = _orderBriefModel.receiverAccount;
        dakaInfo.nickName = _orderBriefModel.receiverName;
    }
    
    PeopleGameDetailViewController * vc = [[PeopleGameDetailViewController alloc] initWithDakaInfo:dakaInfo];
    
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)dialPhoneButtonClicked {
    
    NSString *phoneNumber;
    
    if ([_orderBriefModel.receiverAccount isEqualToString:[TDSingleton instance].account]) {
        phoneNumber = _orderBriefModel.senderAccount;
    } else {
        phoneNumber = _orderBriefModel.receiverAccount;
    }
    
    NSString *message = [NSString stringWithFormat:@"%@", phoneNumber];
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:message message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"拨打", nil];
    alertView.tag = 1000;
    [alertView show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch (alertView.tag) {
        case 1000:
            if (buttonIndex == 1) {
                if ([_orderBriefModel.receiverAccount isEqualToString:[TDSingleton instance].account]) {
                    NSURL * phoneUrl = [NSURL URLWithString:[NSString stringWithFormat:@"tel://%@", _orderBriefModel.senderAccount]];
                    [[UIApplication sharedApplication] openURL:phoneUrl];
                    
                } else {
                    NSURL * phoneUrl = [NSURL URLWithString:[NSString stringWithFormat:@"tel://%@", _orderBriefModel.receiverAccount]];
                    
                    [[UIApplication sharedApplication] openURL:phoneUrl];
                }
            }
            break;
            
        case 1001:
            if (buttonIndex == 1) {
                [TDNetworkRequest cancelOrderWithOrderID:_orderBriefModel.orderID timeout:@"0" success:^(NSString *message) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:TDUserCancelOrderSuccess object:nil];
                    [SVProgressHUD showSuccessWithStatus:@"取消订单成功！"];
                    [self removeUserCancelOrderButton];
                    [self requestOrderInfo];
                } failure:^(NSString *error) {
                    [SVProgressHUD showErrorWithStatus:error];
                }];
            }
            break;
    }
    
}

- (void)sendMessageButtonClicked {

    NSString *targetId;
    NSString *userName;
    
    if ([_orderBriefModel.receiverAccount isEqualToString:[TDSingleton instance].account]) {
        targetId = _orderBriefModel.senderAccount;
        userName = _orderBriefModel.senderName;
    } else {
        targetId = _orderBriefModel.receiverAccount;
        userName = _orderBriefModel.receiverName;
    }
    
    [self enterChatWindowWithTargetId:targetId userName:userName];
}

- (void)enterChatWindowWithTargetId:(NSString *)targetId userName:(NSString *)userName {
    SendMessageViewController *vc = [[SendMessageViewController alloc]init];
    vc.conversationType = ConversationType_PRIVATE;
    vc.targetId = targetId;
    vc.userName = userName;
    
    vc.entryMode = 2;
    
    [self.navigationController pushViewController:vc animated:YES];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

#pragma mark TableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (tableView.tag == 200) {
        return _orderInfoArray.count;
    } else {
        return _dataSource.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *orderInfoReuseIdentifier = @"orderInfoReuseIdentifier";
    static NSString *orderStepReuseIdentifier = @"orderStepReuseIdentifier";
    
    OrderStepTableViewCell *stepCell;
    UITableViewCell *infoCell;
    
    
    if (tableView.tag == 200) {
        infoCell = [tableView dequeueReusableCellWithIdentifier:orderInfoReuseIdentifier];
        if (!infoCell) {
            infoCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:orderInfoReuseIdentifier];
        }
        
//        NSLog(@"cellForRowAtIndexPath:%@", _orderInfoArray[indexPath.row]);
        [infoCell.textLabel setAttributedText:_orderInfoArray[indexPath.row]];
        infoCell.textLabel.numberOfLines = 0;
        infoCell.selectionStyle = UITableViewCellSelectionStyleNone;
        
//        infoCell.layer.borderColor = [UIColor blackColor].CGColor;
//        infoCell.layer.borderWidth = 1;
        
        return infoCell;
    
    } else {
        stepCell = [tableView dequeueReusableCellWithIdentifier:orderStepReuseIdentifier];
        if (!stepCell) {
            stepCell = [[OrderStepTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:orderStepReuseIdentifier];
        }
        
        if (indexPath.row == 0) {
            stepCell.colorCircleView.backgroundColor = [UIColor colorWithRed:38/255.f green:166/255.f blue:154/255.f alpha:1];
            
            if (_dataSource.count == 1) {
                [stepCell.topVerticalLineView removeFromSuperview];
                [stepCell.bottomVerticalLineView removeFromSuperview];
            } else {
                [stepCell.topVerticalLineView removeFromSuperview];
            }
        } else {
            stepCell.colorCircleView.backgroundColor = [UIColor colorWithRed:153/255.f green:153/255.f blue:153/255.f alpha:1];
            
            if (indexPath.row == (_dataSource.count - 1)) {
                [stepCell.bottomVerticalLineView removeFromSuperview];
            }
        }
        
        stepCell.stepInfoLabel.text = _dataSource[indexPath.row][@"text"];
        stepCell.timeLabel.text = _dataSource[indexPath.row][@"time"];
        
        return stepCell;
    }

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {

    if (tableView.tag == 200) {
//        return 24;
//        [infoCell.textLabel setAttributedText:_orderInfoArray[indexPath.row]];
//        NSString *content = _orderInfoArray[indexPath.row];
        
        // 列寬
        CGFloat contentWidth = tableView.frame.size.width;
        // 用何種字體進行顯示
        UIFont *font = [UIFont systemFontOfSize:14];
        // 該行要顯示的內容
        NSString *content = [_orderInfoArray[indexPath.row] string];
        // 計算出顯示完內容需要的最小尺寸
        CGSize size = [content sizeWithFont:font constrainedToSize:CGSizeMake(contentWidth, 1000.0f) lineBreakMode:UILineBreakModeWordWrap];
        // 這裏返回需要的高度
        return size.height + 20;
        
    } else {
        return 75;
    }
}

- (void)requestOrderStepInfo {
    NSDictionary *parameters = @{@"action":@"getOrderById",
                                 @"sessionKey":[TDSingleton instance].sessionKey,
                                 @"orderId":_orderBriefModel.orderID,
                                 @"needProcess":@"1"};
    
    [TDNetworking requestWithURL:[TDSingleton instance].gatewayServerBusinessURL
                      parameters:parameters
                         success:^(id responseObject)
     {
         NSLog(@"getOrderById:%@", responseObject);
         if ([responseObject[@"result"] integerValue] == 1) {
             _dataSource = responseObject[@"orderProcess"];
             [_rootView.orderStepTableView reloadData];
         } else {
             [SVProgressHUD dismissWithError:[NSString stringWithFormat:@"%@", responseObject[@"msg"]]];
         }
     } failure:^(NSError *error) {
         NSLog(@"error:%@", error);
     }];
}

- (void)requestOrderInfo {
    NSDictionary *parameters = @{@"action":@"getOrderById",
                                 @"sessionKey":[TDSingleton instance].sessionKey,
                                 @"orderId":_orderBriefModel.orderID,
                                 @"needProcess":@"1"};
    
    [TDNetworking requestWithURL:[TDSingleton instance].gatewayServerBusinessURL
                      parameters:parameters
                         success:^(id responseObject)
     {
         NSLog(@"getOrderById:%@", responseObject);
         if ([responseObject[@"result"] integerValue] == 1) {
             _dataSource = responseObject[@"orderProcess"];
             _orderInfo = responseObject[@"order"];
             
//             [_rootView.orderStepTableView reloadData];
             [self refreshUI];
         } else {
             [SVProgressHUD dismissWithError:[NSString stringWithFormat:@"%@", responseObject[@"msg"]]];
         }
     } failure:^(NSError *error) {
         NSLog(@"error:%@", error);
     }];
}

#pragma mark 按钮事件
- (void)bigShotStartOrderButtonClicked {

    [TDNetworkRequest startOrderWithOrderID:_orderBriefModel.orderID
                                    success:^(NSString *message)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:TDBigShotStartOrderSuccess object:nil];
        [SVProgressHUD showSuccessWithStatus:@"开始订单成功！"];
        [self removeBigShotStartOrderCancelOrderButton];
        [self addBighShotCompleteOrderButton];
        [self requestOrderInfo];
    } failure:^(NSString *error) {
        [SVProgressHUD showErrorWithStatus:error];
    }];
}

- (void)bigShotCancelOrderButtonClicked {

    [TDNetworkRequest cancelOrderWithOrderID:_orderBriefModel.orderID
                                     timeout:@"0"
                                     success:^(NSString *message)
    {
        [self requestOrderInfo];
        [SVProgressHUD showSuccessWithStatus:@"订单取消成功！"];
        [self removeBigShotStartOrderCancelOrderButton];
        
    } failure:^(NSString *error) {
       [SVProgressHUD showErrorWithStatus:error];
    }];
}

- (void)removeBigShotStartOrderCancelOrderButton {
    [_rootView.bigShotStartOrderButton removeFromSuperview];
    [_rootView.bigShotCancelOrderButton removeFromSuperview];
}

- (void)addBighShotCompleteOrderButton {
    [_rootView.detailBackgroundView addSubview:_rootView.bigShotCompleteOrderButton];
    
    [_rootView.bigShotCompleteOrderButton addTarget:self action:@selector(bigShotCompleteOrderButtonClicked) forControlEvents:UIControlEventTouchUpInside];
}

- (void)bigShotCompleteOrderButtonClicked {
    [TDNetworkRequest bigShotCompleteOrderWithOrderID:_orderBriefModel.orderID
                                              success:^(NSString *message)
    {
        [SVProgressHUD showSuccessWithStatus:@"结束订单成功！"];
        [self requestOrderInfo];
        [self removeBigShotCompleteOrderButton];
    } failure:^(NSString *error) {
        [SVProgressHUD showErrorWithStatus:error];
    }];
}

- (void)removeBigShotCompleteOrderButton {
    [_rootView.bigShotCompleteOrderButton removeFromSuperview];
}

- (void)userCancelOrderButtonClicked {
    [SVProgressHUD showWithStatus:@""];
    
    [TDNetworkRequest getPenaltyWithOrderID:_orderBriefModel.orderID success:^(float penalty) {
        NSLog(@"违约金：%.2f", penalty);
        [SVProgressHUD dismiss];
        [self confirmPenalty:penalty];
        
    } failure:^(NSString *error) {
        [SVProgressHUD dismissWithError:error];
    }];
}

- (void)confirmPenalty:(float)penalty {

    NSString *message;
    
    if (penalty > 1) {
        message = [NSString stringWithFormat:@"现在取消，将扣除违约金%.2f元。您确定要取消订单吗？", penalty];
    } else {
        message = @"您确定要取消订单吗？";
    }

    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:message
                                                        message:nil
                                                       delegate:self
                                              cancelButtonTitle:@"取消"
                                              otherButtonTitles:@"确定", nil];
    alertView.tag = 1001;
    [alertView show];
}

- (void)removeUserCancelOrderButton {
    [_rootView.userCancelOrderButton removeFromSuperview];
}

- (void)userCompleteOrderButtonClicked {
    [TDNetworkRequest userCompleteOrderWithOrderID:_orderBriefModel.orderID success:^(NSString *message) {
        [SVProgressHUD showSuccessWithStatus:@"结束订单成功！"];
        [self removeUserCompleteOrderButton];
        [self requestOrderInfo];
    } failure:^(NSString *error) {
        [SVProgressHUD showErrorWithStatus:error];
    }];
}

- (void)removeUserCompleteOrderButton {
    [_rootView.userCompleteOrderButton removeFromSuperview];
}

#pragma mark - RatingBar delegate
- (void)ratingChanged:(float)newRating {
    NSLog(@"rating:%.0f", newRating);
    NSInteger rating = (NSInteger)newRating;
    
    if (rating > 0) {
        [self submitOrderRating:rating];
    }
}

- (void)submitOrderRating:(NSInteger)rating {
    
    [SVProgressHUD showWithStatus:@"上传评级中......"];
    NSDictionary *parameters = @{@"action":@"rateOrder",
                                 @"sessionKey":[TDSingleton instance].sessionKey,
                                 @"orderId":_orderBriefModel.orderID,
                                 @"star":[NSString stringWithFormat:@"%ld", (long)rating]};
    
    NSLog(@"submitOrderEvaluation parameters:%@", parameters);
    
    [TDNetworking requestWithURL:[TDSingleton instance].gatewayServerBusinessURL
                      parameters:parameters
                         success:^(id responseObject)
     {
         NSLog(@"submitOrderEvaluation result:%@", responseObject);
         if ([responseObject[@"result"] integerValue] == 1) {
             [SVProgressHUD dismissWithSuccess:@"评价成功!\n"];
             
             _rootView.ratingBar.userInteractionEnabled = NO;
             
             [[NSNotificationCenter defaultCenter] postNotificationName:TDEvaluatedFinishedOrder object:nil];
             
         } else {
             [SVProgressHUD dismissWithError:[NSString stringWithFormat:@"%@", responseObject[@"msg"]]];
         }
     } failure:^(NSError *error) {
         [SVProgressHUD dismissWithError:[NSString stringWithFormat:@"%@", error]];
         NSLog(@"requestUserInfoWithIdentifier error:%@", error);
     }];
}

@end
