//
//  NotStartedOrderDetailRootView.m
//  AfterSchool
//
//  Created by Teson Draw on 1/4/16.
//  Copyright © 2016 Teson Draw. All rights reserved.
//

#import "NotStartedOrderDetailRootView.h"
#import "UIButton+SetBackgroundColor.h"

@implementation NotStartedOrderDetailRootView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithRed:230/255.f green:230/255.f blue:230/255.f alpha:1];
        
        [self addSubview:self.navigationBarView];
        [self addSubview:self.detailBackgroundView];
        
//        [_detailBackgroundView addSubview:self.orderTypeImageView];
        [_detailBackgroundView addSubview:self.portraitImageView];
        [_detailBackgroundView addSubview:self.portraitButton];
        
        [_detailBackgroundView addSubview:self.nameLabel];
        [_detailBackgroundView addSubview:self.topDividingLineView];
        
//        [_detailBackgroundView addSubview:self.contentTitleLabel];
//        [_detailBackgroundView addSubview:self.contentLabel];
//        
//        [_detailBackgroundView addSubview:self.startTimeTitleLabel];
//        [_detailBackgroundView addSubview:self.startTimeLabel];
//        
//        [_detailBackgroundView addSubview:self.durationTitleLabel];
//        [_detailBackgroundView addSubview:self.durationLabel];
//        
//        [_detailBackgroundView addSubview:self.rewardTitleLabel];
//        [_detailBackgroundView addSubview:self.rewardLabel];
//        
//        [_detailBackgroundView addSubview:self.addressTitleLabel];
//        [_detailBackgroundView addSubview:self.addressLabel];
//        
//        [_detailBackgroundView addSubview:self.statusTitleLabel];
//        [_detailBackgroundView addSubview:self.statusLabel];
        [_detailBackgroundView addSubview:self.orderInfoTableView];
        
        [_detailBackgroundView addSubview:self.bottomDividingLineView];
        
        [_detailBackgroundView addSubview:self.dialPhoneButton];
        [_detailBackgroundView addSubview:self.sendMessageButton];
        
        [self addSubview:self.orderStepTableView];
    }
    return self;
}

- (NavigationBarView *)navigationBarView {
    if (!_navigationBarView) {
        _navigationBarView = [[NavigationBarView alloc] initWithTitle:@"约单"
                                                           titleColor:nil
                                                      backgroundColor:nil
                                              leftButtonBgViewOriginX:0
                                                 leftButtonImageWidth:22
                                                 leftButtonTitleColor:[UIColor whiteColor]
                                                       leftButtonName:@"约单列表"
                                                  leftButtonImageName:@"navigation_bar_return_button_left_arrow"
                                                      rightButtonName:nil
                                                 rightButtonImageName:nil];
    }
    return _navigationBarView;
}

- (UIView *)detailBackgroundView {
    if (!_detailBackgroundView) {
        _detailBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 64, self.bounds.size.width, 344)];
        _detailBackgroundView.backgroundColor = [UIColor whiteColor];
    }
    return _detailBackgroundView;
}

//- (UIImageView *)orderTypeImageView {
//    if (!_orderTypeImageView) {
//        _orderTypeImageView = [[UIImageView alloc] initWithFrame:CGRectMake(20, 20, 30, 30)];
//        _orderTypeImageView.image = [UIImage imageNamed:@"order_type_be_invited"];
//    }
//    return _orderTypeImageView;
//}

- (UIImageView *)portraitImageView {
    if (!_portraitImageView) {
        _portraitImageView = [[UIImageView alloc] initWithFrame:CGRectMake(20, 20, 30, 30)];
        _portraitImageView.layer.cornerRadius = 5;
    }
    return _portraitImageView;
}

- (UIButton *)portraitButton {
    if (!_portraitButton) {
        _portraitButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
        _portraitButton.center = _portraitImageView.center;
    }
    
    return _portraitButton;
}

//- (UILabel *)orderIDLabel {
//    if (!_orderIDLabel) {
//        _orderIDLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_orderTypeImageView.frame) + 10, 28, self.bounds.size.width - CGRectGetMaxX(_orderTypeImageView.frame) - 30, 19)];
//        _orderIDLabel.textColor = [UIColor colorWithRed:38/255.f green:166/255.f blue:154/255.f alpha:1];
//        _orderIDLabel.font = [UIFont systemFontOfSize:19];
//        _orderIDLabel.text = @"单号：00000000000000";
//    }
//    return _orderIDLabel;
//}

- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_portraitImageView.frame) + 10, 28, self.bounds.size.width - CGRectGetMaxX(_portraitImageView.frame) - 90, 21)];
        _nameLabel.textColor = [UIColor colorWithRed:38/255.f green:166/255.f blue:154/255.f alpha:1];
        _nameLabel.font = [UIFont systemFontOfSize:19];
    }
    return _nameLabel;
}

- (UIView *)topDividingLineView {
    if (!_topDividingLineView) {
        _topDividingLineView = [[UIView alloc] initWithFrame:CGRectMake(20, CGRectGetMaxY(_portraitImageView.frame) + 10, self.bounds.size.width - 40, 1)];
        _topDividingLineView.backgroundColor = [UIColor colorWithRed:204/255.f green:204/255.f blue:204/255.f alpha:1];
    }
    return _topDividingLineView;
}

- (UILabel *)contentTitleLabel {
    if (!_contentTitleLabel) {
        _contentTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, CGRectGetMaxY(_topDividingLineView.frame) + 20, 42, 14)];
        _contentTitleLabel.textColor = [UIColor colorWithRed:56/255.f green:56/255.f blue:56/255.f alpha:1];
        _contentTitleLabel.font = [UIFont systemFontOfSize:14];
        _contentTitleLabel.text = @"内容：";
    }
    return _contentTitleLabel;
}

- (UILabel *)contentLabel {
    if (!_contentLabel) {
        _contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_contentTitleLabel.frame), CGRectGetMaxY(_topDividingLineView.frame) + 20, self.bounds.size.width - 82, 14)];
        _contentLabel.textColor = [UIColor colorWithRed:38/255.f green:166/255.f blue:154/255.f alpha:1];
        _contentLabel.font = [UIFont systemFontOfSize:14];
        _contentLabel.text = @"";
    }
    return _contentLabel;
}

- (UILabel *)startTimeTitleLabel {
    if (!_startTimeTitleLabel) {
        _startTimeTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, CGRectGetMaxY(_contentLabel.frame) + 10, 42, 14)];
        _startTimeTitleLabel.textColor = [UIColor colorWithRed:56/255.f green:56/255.f blue:56/255.f alpha:1];
        _startTimeTitleLabel.font = [UIFont systemFontOfSize:14];
        _startTimeTitleLabel.text = @"开始：";
    }
    return _startTimeTitleLabel;
}

- (UILabel *)startTimeLabel {
    if (!_startTimeLabel) {
        _startTimeLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_startTimeTitleLabel.frame), CGRectGetMaxY(_contentLabel.frame) + 10, self.bounds.size.width - 82, 14)];
        _startTimeLabel.textColor = [UIColor colorWithRed:38/255.f green:166/255.f blue:154/255.f alpha:1];
        _startTimeLabel.font = [UIFont systemFontOfSize:14];
        _startTimeLabel.text = @"";
    }
    return _startTimeLabel;
}

- (UILabel *)durationTitleLabel {
    if (!_durationTitleLabel) {
        _durationTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, CGRectGetMaxY(_startTimeLabel.frame) + 10, 42, 14)];
        _durationTitleLabel.textColor = [UIColor colorWithRed:56/255.f green:56/255.f blue:56/255.f alpha:1];
        _durationTitleLabel.font = [UIFont systemFontOfSize:14];
        _durationTitleLabel.text = @"时长：";
    }
    return _durationTitleLabel;
}

- (UILabel *)durationLabel {
    if (!_durationLabel) {
        _durationLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_durationTitleLabel.frame), CGRectGetMaxY(_startTimeLabel.frame) + 10, self.bounds.size.width - 40, 14)];
        _durationLabel.textColor = [UIColor colorWithRed:38/255.f green:166/255.f blue:154/255.f alpha:1];
        _durationLabel.font = [UIFont systemFontOfSize:14];
        _durationLabel.text = @"1小时";
    }
    return _durationLabel;
}

- (UILabel *)rewardTitleLabel {
    if (!_rewardTitleLabel) {
        _rewardTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, CGRectGetMaxY(_durationLabel.frame) + 10, 42, 14)];
        _rewardTitleLabel.textColor = [UIColor colorWithRed:56/255.f green:56/255.f blue:56/255.f alpha:1];
        _rewardTitleLabel.font = [UIFont systemFontOfSize:14];
        _rewardTitleLabel.text = @"赏金：";
    }
    return _rewardTitleLabel;
}

- (UILabel *)rewardLabel {
    if (!_rewardLabel) {
        _rewardLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_rewardTitleLabel.frame), CGRectGetMaxY(_durationLabel.frame) + 10, self.bounds.size.width - 40, 14)];
        _rewardLabel.textColor = [UIColor colorWithRed:38/255.f green:166/255.f blue:154/255.f alpha:1];
        _rewardLabel.font = [UIFont systemFontOfSize:14];
        _rewardLabel.text = @"38.00";
    }
    return _rewardLabel;
}

- (UILabel *)addressTitleLabel {
    if (!_addressTitleLabel) {
        _addressTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, CGRectGetMaxY(_rewardLabel.frame) + 10, 42, 14)];
        _addressTitleLabel.textColor = [UIColor colorWithRed:56/255.f green:56/255.f blue:56/255.f alpha:1];
        _addressTitleLabel.font = [UIFont systemFontOfSize:14];
        _addressTitleLabel.text = @"地点：";
    }
    return _addressTitleLabel;
}

- (UILabel *)addressLabel {
    if (!_addressLabel) {
        _addressLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_addressTitleLabel.frame), CGRectGetMaxY(_rewardLabel.frame) + 10, self.bounds.size.width - 40, 14)];
        _addressLabel.textColor = [UIColor colorWithRed:38/255.f green:166/255.f blue:154/255.f alpha:1];
        _addressLabel.font = [UIFont systemFontOfSize:14];
        _addressLabel.text = @"窦团山";
    }
    return _addressLabel;
}

- (UILabel *)statusTitleLabel {
    if (!_statusTitleLabel) {
        _statusTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, CGRectGetMaxY(_addressTitleLabel.frame) + 10, 42, 14)];
        _statusTitleLabel.textColor = [UIColor colorWithRed:56/255.f green:56/255.f blue:56/255.f alpha:1];
        _statusTitleLabel.font = [UIFont systemFontOfSize:14];
        _statusTitleLabel.text = @"状态：";
    }
    return _statusTitleLabel;
}

- (UILabel *)statusLabel {
    if (!_statusLabel) {
        _statusLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_statusTitleLabel.frame), CGRectGetMaxY(_addressLabel.frame) + 10, self.bounds.size.width - 40, 14)];
        _statusLabel.textColor = [UIColor colorWithRed:38/255.f green:166/255.f blue:154/255.f alpha:1];
        _statusLabel.font = [UIFont systemFontOfSize:14];
        _statusLabel.text = @"待处理";
    }
    return _statusLabel;
}



- (UIView *)bottomDividingLineView {
    if (!_bottomDividingLineView) {
        _bottomDividingLineView = [[UIView alloc] initWithFrame:CGRectMake(20, _detailBackgroundView.bounds.size.height - 85, self.bounds.size.width - 40, 1)];
        _bottomDividingLineView.backgroundColor = [UIColor colorWithRed:204/255.f green:204/255.f blue:204/255.f alpha:1];
    }
    return _bottomDividingLineView;
}

- (UIButton *)bigShotStartOrderButton {
    if (!_bigShotStartOrderButton) {
        _bigShotStartOrderButton = [[UIButton alloc] initWithFrame:CGRectMake(20, CGRectGetMaxY(_bottomDividingLineView.frame) + 20, 180, 45)];
        [_bigShotStartOrderButton setTitle:@"开始" forState:UIControlStateNormal];
        [_bigShotStartOrderButton setBackgroundColor:[UIColor colorWithRed:38/255.f green:166/255.f blue:154/255.0 alpha:1]];
        [_bigShotStartOrderButton setBackgroundColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
        _bigShotStartOrderButton.layer.cornerRadius = 5;
        _bigShotStartOrderButton.layer.masksToBounds = YES;
        _bigShotStartOrderButton.titleLabel.font = [UIFont systemFontOfSize:20];
        _bigShotStartOrderButton.tag = 100;
    }
    
    return _bigShotStartOrderButton;
}

- (UIButton *)bigShotCancelOrderButton {
    if (!_bigShotCancelOrderButton) {
        _bigShotCancelOrderButton = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_bigShotStartOrderButton.frame) + 10, CGRectGetMaxY(_bottomDividingLineView.frame) + 20, self.bounds.size.width - CGRectGetMaxX(_bigShotStartOrderButton.frame) - 10 - 20, 45)];
        [_bigShotCancelOrderButton setTitle:@"取消" forState:UIControlStateNormal];
        [_bigShotCancelOrderButton setBackgroundColor:[UIColor colorWithRed:255/255.f green:145/255.f blue:0 alpha:1]];
        [_bigShotCancelOrderButton setBackgroundColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
        _bigShotCancelOrderButton.layer.cornerRadius = 5;
        _bigShotCancelOrderButton.layer.masksToBounds = YES;
        _bigShotCancelOrderButton.titleLabel.font = [UIFont systemFontOfSize:20];
        _bigShotCancelOrderButton.tag = 101;
    }
    
    return _bigShotCancelOrderButton;
}

//- (UIButton *)userStartOrderButton {
//    if (!_userStartOrderButton) {
//        _userStartOrderButton = [[UIButton alloc] initWithFrame:CGRectMake(20, CGRectGetMaxY(_bottomDividingLineView.frame) + 20, 180, 45)];
//        [_userStartOrderButton setTitle:@"开始" forState:UIControlStateNormal];
//        [_userStartOrderButton setBackgroundColor:[UIColor colorWithRed:38/255.f green:166/255.f blue:154/255.0 alpha:1]];
//        [_userStartOrderButton setBackgroundColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
//        _userStartOrderButton.layer.cornerRadius = 5;
//        _userStartOrderButton.layer.masksToBounds = YES;
//        _userStartOrderButton.titleLabel.font = [UIFont systemFontOfSize:20];
//        _userStartOrderButton.tag = 100;
//    }
//    
//    return _userStartOrderButton;
//}

- (UIButton *)userCancelOrderButton {
    if (!_userCancelOrderButton) {
        _userCancelOrderButton = [[UIButton alloc] initWithFrame:CGRectMake(20, CGRectGetMaxY(_bottomDividingLineView.frame) + 20, self.bounds.size.width - 40, 45)];
        [_userCancelOrderButton setTitle:@"取消" forState:UIControlStateNormal];
        [_userCancelOrderButton setBackgroundColor:[UIColor colorWithRed:255/255.f green:145/255.f blue:0 alpha:1]];
        [_userCancelOrderButton setBackgroundColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
        _userCancelOrderButton.layer.cornerRadius = 5;
        _userCancelOrderButton.layer.masksToBounds = YES;
        _userCancelOrderButton.titleLabel.font = [UIFont systemFontOfSize:20];
        _userCancelOrderButton.tag = 101;
    }
    
    return _userCancelOrderButton;
}

- (UIButton *)bigShotCompleteOrderButton {
    if (!_bigShotCompleteOrderButton) {
        _bigShotCompleteOrderButton = [[UIButton alloc] initWithFrame:CGRectMake(20, CGRectGetMaxY(_bottomDividingLineView.frame) + 20, self.bounds.size.width - 40, 45)];
        [_bigShotCompleteOrderButton setTitle:@"结束" forState:UIControlStateNormal];
        [_bigShotCompleteOrderButton setBackgroundColor:[UIColor colorWithRed:255/255.f green:145/255.f blue:0 alpha:1]];
        [_bigShotCompleteOrderButton setBackgroundColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
        _bigShotCompleteOrderButton.layer.cornerRadius = 5;
        _bigShotCompleteOrderButton.layer.masksToBounds = YES;
        _bigShotCompleteOrderButton.titleLabel.font = [UIFont systemFontOfSize:20];
        _bigShotCompleteOrderButton.tag = 102;
    }
    
    return _bigShotCompleteOrderButton;
}

- (UIButton *)userCompleteOrderButton {
    if (!_userCompleteOrderButton) {
        _userCompleteOrderButton = [[UIButton alloc] initWithFrame:CGRectMake(20, CGRectGetMaxY(_bottomDividingLineView.frame) + 20, self.bounds.size.width - 40, 45)];
        [_userCompleteOrderButton setTitle:@"确认完成" forState:UIControlStateNormal];
        [_userCompleteOrderButton setBackgroundColor:[UIColor colorWithRed:255/255.f green:145/255.f blue:0 alpha:1]];
        [_userCompleteOrderButton setBackgroundColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
        _userCompleteOrderButton.layer.cornerRadius = 5;
        _userCompleteOrderButton.layer.masksToBounds = YES;
        _userCompleteOrderButton.titleLabel.font = [UIFont systemFontOfSize:20];
        _userCompleteOrderButton.tag = 103;
    }
    
    return _userCompleteOrderButton;
}

//- (UIButton *)freezeOrderButton {
//    if (!_freezeOrderButton) {
//        _freezeOrderButton = [[UIButton alloc] initWithFrame:CGRectMake(20, CGRectGetMaxY(_bottomDividingLineView.frame) + 20, self.bounds.size.width - 40, 45)];
//        [_freezeOrderButton setTitle:@"冻结" forState:UIControlStateNormal];
//        [_freezeOrderButton setBackgroundColor:[UIColor colorWithRed:255/255.f green:145/255.f blue:0 alpha:1]];
//        [_freezeOrderButton setBackgroundColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
//        _freezeOrderButton.layer.cornerRadius = 5;
//        _freezeOrderButton.layer.masksToBounds = YES;
//        _freezeOrderButton.titleLabel.font = [UIFont systemFontOfSize:20];
//        _freezeOrderButton.tag = 103;
//    }
//    
//    return _completeButton;
//}

- (UITableView *)orderInfoTableView {
    if (!_orderInfoTableView) {
        _orderInfoTableView = [[UITableView alloc] initWithFrame:CGRectMake(4, CGRectGetMaxY(_topDividingLineView.frame), self.bounds.size.width - 24, 24 * 8)];
        _orderInfoTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        _orderInfoTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _orderInfoTableView.backgroundColor = [UIColor whiteColor];
        _orderInfoTableView.tag = 200;
        
        _orderInfoTableView.scrollEnabled = NO;
        UIView *tableHeadView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, 15)];
        _orderInfoTableView.tableHeaderView = tableHeadView;
    }
    return _orderInfoTableView;
}

- (UITableView *)orderStepTableView {
    if (!_orderStepTableView) {
        _orderStepTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_detailBackgroundView.frame), self.bounds.size.width, self.bounds.size.height - CGRectGetMaxY(_detailBackgroundView.frame))];
        _orderStepTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        _orderStepTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _orderStepTableView.backgroundColor = [UIColor colorWithRed:230/255.f green:230/255.f blue:230/255.f alpha:1];
        _orderStepTableView.tag = 201;
        
        UIView *tableHeadView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, 10)];
        _orderStepTableView.tableHeaderView = tableHeadView;
    }
    return _orderStepTableView;
}

- (UILabel *)ratingTipLabel {
    if (!_ratingTipLabel) {
        _ratingTipLabel = [[UILabel alloc] initWithFrame:CGRectMake((_detailBackgroundView.bounds.size.width - 100) / 2, CGRectGetMaxY(_bottomDividingLineView.frame) + 10, 100, 14)];
        _ratingTipLabel.textColor = [UIColor colorWithRed:153/255.f green:153/255.f blue:153/255.f alpha:1];
        _ratingTipLabel.font = [UIFont systemFontOfSize:12];
        _ratingTipLabel.text = @"给个评级吧";
        _ratingTipLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _ratingTipLabel;
}

- (RatingBar *)ratingBar {
    if (!_ratingBar) {
        
        _ratingBar = [[RatingBar alloc] initWithFrame:CGRectMake((_detailBackgroundView.bounds.size.width - 200) / 2, CGRectGetMaxY(_bottomDividingLineView.frame) + 10, 200, 76) deselectedImage:@"order_evaluation_gray_star" selectedImage:@"order_evaluation_solid_star"];
    }
    
    return _ratingBar;
}

- (UIButton *)dialPhoneButton {
    if (!_dialPhoneButton) {
        _dialPhoneButton = [[UIButton alloc] initWithFrame:CGRectMake(self.bounds.size.width - 20 - 44 - 44 - 10, _topDividingLineView.frame.origin.y - 44, 44, 44)];
        [_dialPhoneButton setImage:[UIImage imageNamed:@"order_make_call"] forState:UIControlStateNormal];
        
//        _dialPhoneButton.layer.borderColor = [UIColor blackColor].CGColor;
//        _dialPhoneButton.layer.borderWidth = 1;
    }
    
    return _dialPhoneButton;
}

- (UIButton *)sendMessageButton {
    if (!_sendMessageButton) {
        _sendMessageButton = [[UIButton alloc] initWithFrame:CGRectMake(self.bounds.size.width - 20 - 44, _topDividingLineView.frame.origin.y - 44, 44, 44)];
        [_sendMessageButton setImage:[UIImage imageNamed:@"order_send_message"] forState:UIControlStateNormal];
        
//        _sendMessageButton.layer.borderColor = [UIColor blackColor].CGColor;
//        _sendMessageButton.layer.borderWidth = 1;
    }
    
    return _sendMessageButton;
}

@end
