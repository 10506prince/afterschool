//
//  NotStartedOrderDetailRootView.h
//  AfterSchool
//
//  Created by Teson Draw on 1/4/16.
//  Copyright © 2016 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavigationBarView.h"
#import "RatingBar.h"

@interface NotStartedOrderDetailRootView : UIView

@property (nonatomic, strong) NavigationBarView *navigationBarView;

@property (nonatomic, strong) UIImageView *portraitImageView;

@property (nonatomic, strong) UIView *detailBackgroundView;
@property (nonatomic, strong) UIView *topDividingLineView;
@property (nonatomic, strong) UIView *bottomDividingLineView;

@property (nonatomic, strong) UILabel *orderIDLabel;
@property (nonatomic, strong) UILabel *nameLabel;

@property (nonatomic, strong) UILabel *contentTitleLabel;
@property (nonatomic, strong) UILabel *contentLabel;

@property (nonatomic, strong) UILabel *startTimeTitleLabel;
@property (nonatomic, strong) UILabel *startTimeLabel;

@property (nonatomic, strong) UILabel *durationTitleLabel;
@property (nonatomic, strong) UILabel *durationLabel;

@property (nonatomic, strong) UILabel *rewardTitleLabel;
@property (nonatomic, strong) UILabel *rewardLabel;

@property (nonatomic, strong) UILabel *addressTitleLabel;
@property (nonatomic, strong) UILabel *addressLabel;

@property (nonatomic, strong) UILabel *statusTitleLabel;
@property (nonatomic, strong) UILabel *statusLabel;

@property (nonatomic, strong) UILabel *ratingTipLabel;


@property (nonatomic, strong) UIButton *portraitButton;

@property (nonatomic, strong) UIButton *bigShotStartOrderButton;
@property (nonatomic, strong) UIButton *bigShotCancelOrderButton;

@property (nonatomic, strong) UIButton *userCancelOrderButton;

@property (nonatomic, strong) UIButton *bigShotCompleteOrderButton;
@property (nonatomic, strong) UIButton *userCompleteOrderButton;

@property (nonatomic, strong) UIButton *dialPhoneButton;
@property (nonatomic, strong) UIButton *sendMessageButton;

@property (nonatomic, strong) UITableView *orderInfoTableView;
@property (nonatomic, strong) UITableView *orderStepTableView;

@property (nonatomic, strong) RatingBar *ratingBar;

@end
