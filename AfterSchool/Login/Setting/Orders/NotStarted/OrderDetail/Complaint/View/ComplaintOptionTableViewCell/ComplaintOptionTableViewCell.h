//
//  ComplaintOptionTableViewCell.h
//  AfterSchool
//
//  Created by Teson Draw on 12/25/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ComplaintOptionTableViewCell : UITableViewCell

@property (nonatomic, strong) UIView *textBackgroundView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIImageView *selectedImageView;

@end
