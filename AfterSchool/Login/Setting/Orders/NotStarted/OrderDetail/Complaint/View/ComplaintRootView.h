//
//  ComplaintRootView.h
//  AfterSchool
//
//  Created by Teson Draw on 12/24/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavigationBarView.h"

@interface ComplaintRootView : UIView

@property (nonatomic, strong) NavigationBarView *navigationBarView;
@property (nonatomic, strong) UITableView       *tableView;

@property (nonatomic, strong) UIView *tableHeaderView;
@property (nonatomic, strong) UILabel *tipLabel;

@property (nonatomic, strong) UILabel *customerServiceLabel;
@property (nonatomic, strong) UIImageView *customerServiceImageView;
@property (nonatomic, strong) UIButton *callCustomerServiceButton;

@end
