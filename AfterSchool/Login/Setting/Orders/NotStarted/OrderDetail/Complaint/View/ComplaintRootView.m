//
//  ComplaintRootView.m
//  AfterSchool
//
//  Created by Teson Draw on 12/24/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "ComplaintRootView.h"
#import "UIButton+SetBackgroundColor.h"

@implementation ComplaintRootView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithRed:230/255.f green:230/255.f blue:230/255.f alpha:1];
        
        
        [self addSubview:self.tableView];
        [self addSubview:self.navigationBarView];
        _tableView.tableHeaderView = self.tableHeaderView;
        
        [_tableHeaderView addSubview:self.tipLabel];
        [_tableHeaderView addSubview:self.customerServiceLabel];
        [_tableHeaderView addSubview:self.customerServiceImageView];
        [_tableHeaderView addSubview:self.callCustomerServiceButton];
    }
    return self;
}

- (NavigationBarView *)navigationBarView {
    if (!_navigationBarView) {
        
        _navigationBarView = [[NavigationBarView alloc] initWithTitle:@"投诉"
                                                           titleColor:nil
                                                      backgroundColor:nil
                                              leftButtonBgViewOriginX:0
                                                 leftButtonImageWidth:22
                                                 leftButtonTitleColor:[UIColor whiteColor]
                                                       leftButtonName:@"返回"
                                                  leftButtonImageName:@"navigation_bar_return_button_left_arrow"
                                                      rightButtonName:nil
                                                 rightButtonImageName:nil];
    }
    return _navigationBarView;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height)];
        _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = [UIColor colorWithRed:230/255.f green:230/255.f blue:230/255.f alpha:1];
    }
    return _tableView;
}

- (UIView *)tableHeaderView {
    if (!_tableHeaderView) {
        _tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, 100)];
        _tableHeaderView.backgroundColor = [UIColor colorWithRed:230/255.f green:230/255.f blue:230/255.f alpha:1];

//        _tableHeaderView.layer.borderColor = [UIColor redColor].CGColor;
//        _tableHeaderView.layer.borderWidth = 1;
    }
    return _tableHeaderView;
}

- (UILabel *)tipLabel {
    if (!_tipLabel) {
        _tipLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 74, 160, 14)];
        _tipLabel.textColor = [UIColor colorWithRed:204/255.f green:204/255.f blue:204/255.f alpha:1];
        _tipLabel.font = [UIFont systemFontOfSize:14];
        _tipLabel.text = @"有什么要吐槽TA的？";
//        _tipLabel.layer.borderColor = [UIColor blueColor].CGColor;
//        _tipLabel.layer.borderWidth = 1;
    }
    return _tipLabel;
}

- (UIImageView *)customerServiceImageView {
    if (!_customerServiceImageView) {
        _customerServiceImageView = [[UIImageView alloc] initWithFrame:CGRectMake(_tableHeaderView.bounds.size.width - 56 - 40, 75, 15, 15)];
        _customerServiceImageView.image = [UIImage imageNamed:@"call_customer_service"];
    }
    return _customerServiceImageView;
}

- (UILabel *)customerServiceLabel {
    if (!_customerServiceLabel) {
        _customerServiceLabel = [[UILabel alloc] initWithFrame:CGRectMake(_tableHeaderView.bounds.size.width - 20 - 56, 64, 56, 36)];
        _customerServiceLabel.textColor = [UIColor lightGrayColor];
        _customerServiceLabel.font = [UIFont systemFontOfSize:14];
        _customerServiceLabel.text = @"联系客服";
        
//        _customerServiceLabel.layer.borderColor = [UIColor blueColor].CGColor;
//        _customerServiceLabel.layer.borderWidth = 1;
    }
    return _customerServiceLabel;
}

- (UIButton *)callCustomerServiceButton {
    if (!_callCustomerServiceButton) {
        _callCustomerServiceButton = [[UIButton alloc] initWithFrame:CGRectMake((self.bounds.size.width - 120), 64, 120, 29)];
//        [_callCustomerServiceButton setTitle:@"客服电话" forState:UIControlStateNormal];
//        [_callCustomerServiceButton setBackgroundColor:[UIColor colorWithRed:37/255.f green:183/255.f blue:237/255.0 alpha:1]];
//        [_callCustomerServiceButton setBackgroundColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
//        _callCustomerServiceButton.layer.cornerRadius = 5;
//        _callCustomerServiceButton.layer.masksToBounds = YES;
//        _callCustomerServiceButton.titleLabel.font = [UIFont systemFontOfSize:20];
//        _callCustomerServiceButton.layer.borderColor = [UIColor blackColor].CGColor;
//        _callCustomerServiceButton.layer.borderWidth = 1;
    }
    
    return _callCustomerServiceButton;
}

@end
