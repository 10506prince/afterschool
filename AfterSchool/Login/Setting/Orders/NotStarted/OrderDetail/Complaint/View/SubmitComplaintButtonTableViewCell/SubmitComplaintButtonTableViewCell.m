//
//  SubmitComplaintButtonTableViewCell.m
//  AfterSchool
//
//  Created by Teson Draw on 12/25/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "SubmitComplaintButtonTableViewCell.h"
#import "UIButton+SetBackgroundColor.h"

@implementation SubmitComplaintButtonTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor colorWithRed:230/255.f green:230/255.f blue:230/255.f alpha:1];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [self addSubview:self.button];
    }
    return self;
}

- (UIButton *)button {
    if (!_button) {
        _button = [[UIButton alloc] initWithFrame:CGRectMake(20, 4, self.bounds.size.width - 40, 45)];
        [_button setTitle:@"提交" forState:UIControlStateNormal];
        [_button setBackgroundColor:[UIColor colorWithRed:38/255.f green:166/255.f blue:154/255.0 alpha:1]];
        [_button setBackgroundColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
        [_button setBackgroundColor:[UIColor colorWithRed:228/255.f green:228/255.f blue:228/255.f alpha:1] forState:UIControlStateDisabled];
        _button.layer.cornerRadius = 5;
        _button.layer.masksToBounds = YES;
        _button.titleLabel.font = [UIFont systemFontOfSize:19];
        _button.enabled = NO;
    }
    
    return _button;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
