//
//  InputComplaintInfoTableViewCell.h
//  AfterSchool
//
//  Created by Teson Draw on 12/25/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InputComplaintInfoTableViewCell : UITableViewCell

@property (nonatomic, strong) UITextView *textView;

@property (nonatomic, strong) UILabel *placeholderLabel;
@property (nonatomic, strong) UILabel *characterNumberLabel;

@end
