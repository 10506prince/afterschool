//
//  InputComplaintInfoTableViewCell.m
//  AfterSchool
//
//  Created by Teson Draw on 12/25/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "InputComplaintInfoTableViewCell.h"

@implementation InputComplaintInfoTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor colorWithRed:230/255.f green:230/255.f blue:230/255.f alpha:1];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [self addSubview:self.textView];
        [_textView addSubview:self.placeholderLabel];
        [_textView addSubview:self.characterNumberLabel];
    }
    return self;
}

- (UITextView *)textView {
    if (!_textView) {
        _textView = [[UITextView alloc] initWithFrame:CGRectMake(20, 4, self.bounds.size.width - 40, 56)];
        _textView.layer.cornerRadius = 4;
    }
    
    return _textView;
}

- (UILabel *)placeholderLabel {
    if (!_placeholderLabel) {
        _placeholderLabel = [[UILabel alloc] initWithFrame:CGRectMake(12, 6, 100, 12)];
        _placeholderLabel.text = @"我还有其他想说的";
        _placeholderLabel.textColor = [UIColor colorWithRed:200/255.f green:200/255.f blue:200/255.f alpha:1];
        _placeholderLabel.font = [UIFont systemFontOfSize:12];
    }
    return _placeholderLabel;
}

- (UILabel *)characterNumberLabel {
    if (!_characterNumberLabel) {
        _characterNumberLabel = [[UILabel alloc] initWithFrame:CGRectMake(_textView.bounds.size.width - 20, _textView.bounds.size.height - 10, 20, 10)];
        _characterNumberLabel.text = @"100";
        _characterNumberLabel.font = [UIFont systemFontOfSize:10];
        _characterNumberLabel.textAlignment = NSTextAlignmentRight;
        _characterNumberLabel.textColor = [UIColor colorWithRed:106/255.f green:151/255.f blue:52/255.f alpha:1];
    }
    return _characterNumberLabel;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
