//
//  ComplaintOptionTableViewCell.m
//  AfterSchool
//
//  Created by Teson Draw on 12/25/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "ComplaintOptionTableViewCell.h"

@implementation ComplaintOptionTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor colorWithRed:230/255.f green:230/255.f blue:230/255.f alpha:1];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [self addSubview:self.textBackgroundView];
        [_textBackgroundView addSubview:self.titleLabel];
    }
    return self;
}

- (UIView *)textBackgroundView {
    if (!_textBackgroundView) {
        _textBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(20, 4, self.bounds.size.width - 40, 44)];
        _textBackgroundView.backgroundColor = [UIColor whiteColor];
//        _textBackgroundView.layer.cornerRadius = 4;
    }
    return _textBackgroundView;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(12, 0, self.bounds.size.width - 24 - 28 - 20, 42)];
        _titleLabel.textColor = [UIColor colorWithRed:153/255.f green:153/255.f blue:153/255.f alpha:1];
        _titleLabel.font = [UIFont systemFontOfSize:16];
//        _titleLabel.layer.borderColor = [UIColor blackColor].CGColor;
//        _titleLabel.layer.borderWidth = 1;
    }
    return _titleLabel;
}

- (UIImageView *)selectedImageView {
    if (!_selectedImageView) {
        _selectedImageView = [[UIImageView alloc] initWithFrame:CGRectMake(_textBackgroundView.bounds.size.width - 14 - 22, 10, 22, 22)];
        _selectedImageView.image = [UIImage imageNamed:@"complaint_option_selected"];
    }
    return _selectedImageView;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    if (selected) {
        self.textBackgroundView.layer.borderColor = [UIColor colorWithRed:38/255.f green:166/255.f blue:154/255.f alpha:1].CGColor;
        self.textBackgroundView.layer.borderWidth = 0.5;
        self.titleLabel.textColor = [UIColor colorWithRed:38/255.f green:166/255.f blue:154/255.f alpha:1];
        [self.textBackgroundView addSubview:self.selectedImageView];
    } else {
        self.textBackgroundView.layer.borderColor = [UIColor clearColor].CGColor;
        self.textBackgroundView.layer.borderWidth = 0;
        self.titleLabel.textColor = [UIColor colorWithRed:153/255.f green:153/255.f blue:153/255.f alpha:1];
        
        if (_selectedImageView) {
            [_selectedImageView removeFromSuperview];
        }
    }
}

@end
