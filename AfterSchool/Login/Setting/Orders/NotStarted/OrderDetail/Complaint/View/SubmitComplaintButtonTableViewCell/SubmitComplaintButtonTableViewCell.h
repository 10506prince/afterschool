//
//  SubmitComplaintButtonTableViewCell.h
//  AfterSchool
//
//  Created by Teson Draw on 12/25/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SubmitComplaintButtonTableViewCell : UITableViewCell

@property (nonatomic, strong) UIButton *button;

@end
