//
//  ComplaintViewController.h
//  AfterSchool
//
//  Created by Teson Draw on 12/24/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ComplaintRootView.h"

@interface ComplaintViewController : UIViewController

@property (nonatomic, strong) ComplaintRootView *rootView;
@property (nonatomic, strong) NSArray *dataSource;

@property (nonatomic, strong) NSDictionary *orderInfo;

@property (nonatomic, assign) NSInteger rowNumber;
@property (nonatomic, assign) NSInteger initialRowNumber;

@property (nonatomic, assign) NSInteger complaintType;
@property (nonatomic, assign) CGRect tableViewFrame;

@property (nonatomic, assign) CGRect keyboardFrame;


@end
