//
//  ComplaintViewController.m
//  AfterSchool
//
//  Created by Teson Draw on 12/24/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "ComplaintViewController.h"
#import "TDNetworking.h"
#import "TDSingleton.h"
#import "SVProgressHUD.h"

#import "ComplaintOptionTableViewCell.h"
#import "SubmitComplaintButtonTableViewCell.h"
#import "InputComplaintInfoTableViewCell.h"

#import "MacrosDefinition.h"

#import "Common.h"
#import "TalkingData.h"

@interface ComplaintViewController () <UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate, UITextViewDelegate>

@end

@implementation ComplaintViewController
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [TalkingData trackPageBegin:selfControllerName];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [TalkingData trackPageEnd:selfControllerName];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initData];
    [self initUserInterface];
    
    NSLog(@"ComplaintViewController frame:%@", NSStringFromCGRect(self.view.frame));
}

- (void)initData {
    _dataSource = @[@"没有陪玩", @"技术太差", @"服务态度不好", @"费用问题", @"其他问题"];
    _rowNumber = _dataSource.count + 1;
    _initialRowNumber = _rowNumber;
    
    _complaintType = -1;
    
    //注册通知,监听键盘弹出事件
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    
    //注册通知,监听键盘消失事件
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide) name:UIKeyboardWillHideNotification object:nil];
    
//    //注册通知,键盘形状发生变化
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keboardDidChangeFrameNotification:) name:UIKeyboardDidChangeFrameNotification object:nil];
}

- (void)initUserInterface {
    [self.view addSubview:self.rootView];
    self.automaticallyAdjustsScrollViewInsets = NO;
}

- (ComplaintRootView *)rootView {
    if (!_rootView) {
        _rootView = [[ComplaintRootView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
        [_rootView.navigationBarView.leftButton addTarget:self action:@selector(returnButtonClicked) forControlEvents:UIControlEventTouchUpInside];

        [_rootView.callCustomerServiceButton addTarget:self action:@selector(callCustomerServiceButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        
        _rootView.tableView.dataSource = self;
        _rootView.tableView.delegate = self;
    }
    
    return _rootView;
}

- (void)returnButtonClicked {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)callCustomerServiceButtonClicked {
    NSLog(@"callCustomerServiceButtonClicked");
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"400-888-8888" message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"拨打", nil];
    [alertView show];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)keyboardDidShow:(NSNotification *)notification {
    //获取键盘高度
    NSValue *value = [[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey];
    
    CGRect keyboardFrame = [value CGRectValue];
    
    NSLog(@"DidShow keboard frame:%@", NSStringFromCGRect(keyboardFrame));
    
    if (keyboardFrame.size.height >= 216) {
        _keyboardFrame = keyboardFrame;
        NSLog(@"DidShow keboard frame > 217:%@", NSStringFromCGRect(_keyboardFrame));
        [self moveUpTableView];
    }
}

- (void)keyboardWillHide {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    
    _rootView.tableView.frame = CGRectMake(0, 0, _rootView.tableView.frame.size.width, _rootView.tableView.frame.size.height);
    
    [UIView commitAnimations];
}

- (void)moveUpTableView {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    
    _rootView.tableView.frame = CGRectMake(0, -(_keyboardFrame.size.height), _rootView.tableView.frame.size.width, _rootView.tableView.frame.size.height);
    
    [UIView commitAnimations];
}

#pragma mark UITableView delegate method
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _rowNumber;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *reuseIdentifierOption    = @"reuseIdentifierOption";
    static NSString *reuseIdentifierButton    = @"reuseIdentifierButton";
    static NSString *reuseIdentifierTextField = @"reuseIdentifierTextField";
    
    /**
     *  未加入投诉类型输入框
     */
    if (_rowNumber == _initialRowNumber) {
        if (indexPath.row == (_rowNumber - 1)) {
            SubmitComplaintButtonTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifierButton];
            
            if (!cell) {
                cell = [[SubmitComplaintButtonTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifierButton];
            }
            
            [cell.button addTarget:self action:@selector(submitComplaintButtonClicked) forControlEvents:UIControlEventTouchUpInside];
            
            return cell;
            
        } else {
            ComplaintOptionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifierOption];
            
            if (!cell) {
                cell = [[ComplaintOptionTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifierOption];
            }
            
            cell.titleLabel.text = _dataSource[indexPath.row];
            
            return cell;
        }
    /**
     *  加入投诉类型输入框
     */
    } else {
        if (indexPath.row == (_rowNumber - 1)) {
            SubmitComplaintButtonTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifierButton];
            
            if (!cell) {
                cell = [[SubmitComplaintButtonTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifierButton];
            }
            
            [cell.button addTarget:self action:@selector(submitComplaintButtonClicked) forControlEvents:UIControlEventTouchUpInside];
            
            return cell;
            
        } else if (indexPath.row == (_rowNumber - 2)) {
            InputComplaintInfoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifierTextField];
            
            if (!cell) {
                cell = [[InputComplaintInfoTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifierTextField];
            }
            
            cell.textView.delegate = self;
            
            return cell;
            
        } else {
            ComplaintOptionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifierOption];
            
            if (!cell) {
                cell = [[ComplaintOptionTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifierOption];
            }
            
            cell.titleLabel.text = _dataSource[indexPath.row];
            
            return cell;
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat height = 56;

    if (_rowNumber == _initialRowNumber) {//如果没有添加投诉内容输入框cell
        if (indexPath.row == (_rowNumber - 1)) {//提交按钮
            return 54;
        } else {
            return 50;
        }
    } else {//添加了投诉内容输入框
        if (indexPath.row == (_rowNumber - 1)) {//提交按钮
            return 54;
        } else if (indexPath.row == (_rowNumber - 2)) {//投诉内容输入框
            return 64;
        } else {//投诉类型
            return 50;
        }
    }

    return height;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    if ([cell isKindOfClass:[ComplaintOptionTableViewCell class]]) {
        /**
         *  投诉类型赋值
         */
        _complaintType = indexPath.row + 1;
        
        /**
         *  使提交按钮可用
         */
        NSIndexPath *buttonIndexPath = [NSIndexPath indexPathForRow:(_rowNumber - 1) inSection:0];
        SubmitComplaintButtonTableViewCell *cell = [tableView cellForRowAtIndexPath:buttonIndexPath];
        cell.button.enabled = YES;
        
        /**
         *  插入投诉内容输入框
         */
        if (_rowNumber == _initialRowNumber) {
            _rowNumber += 1;
            
            NSMutableArray * mutableArray = [[NSMutableArray alloc] init];
            NSIndexPath *inputInfoCellIndexPath = [NSIndexPath indexPathForRow:(_rowNumber - 2) inSection:0];
            [mutableArray addObject:inputInfoCellIndexPath];

            [tableView insertRowsAtIndexPaths:mutableArray withRowAnimation:UITableViewRowAnimationTop];
        }
    }
    
//    if (indexPath.row < (_rowNumber - 1)) {
//        NSIndexPath *buttonIndexPath = [NSIndexPath indexPathForRow:(_rowNumber - 1) inSection:0];
//        SubmitComplaintButtonTableViewCell *cell = [tableView cellForRowAtIndexPath:buttonIndexPath];
//        cell.button.enabled = YES;
//    }
//    
//    if (indexPath.row < (_rowNumber - 2)) {
//        _complaintType = indexPath.row + 1;
//    }
}

- (void)submitComplaintWithInfo:(NSString *)info type:(NSInteger)type {
    NSString *typeString = [NSString stringWithFormat:@"%ld", (long)type];
    [SVProgressHUD showWithStatus:@"数据上传中..."];
    NSString *orderID = [NSString stringWithFormat:@"%@", _orderInfo[@"id"]];
    NSDictionary *parameters = @{@"action":@"complainOrder",
                                 @"sessionKey":[TDSingleton instance].sessionKey,
                                 @"orderId":orderID,
                                 @"type":typeString,
                                 @"info":info};
    
    NSLog(@"parameters:%@", parameters);
    
    [TDNetworking requestWithURL:[TDSingleton instance].gatewayServerBusinessURL
                      parameters:parameters
                         success:^(id responseObject)
     {
         NSLog(@"submitComplaint:%@", responseObject);
         if ([responseObject[@"result"] integerValue] == 1) {
             [SVProgressHUD dismissWithSuccess:@"提交成功！"];
         } else {
             [SVProgressHUD dismissWithError:[NSString stringWithFormat:@"%@", responseObject[@"msg"]]];
         }
     } failure:^(NSError *error) {
         NSLog(@"error:%@", error);
     }];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
        NSURL * phoneUrl = [NSURL URLWithString:[NSString stringWithFormat:@"tel://%@", @"4008888888"]];
        [[UIApplication sharedApplication] openURL:phoneUrl];
    }
}

- (void)submitComplaintButtonClicked {
    [_rootView endEditing:YES];
    
    NSIndexPath *textFieldIndexPath = [NSIndexPath indexPathForRow:(_rowNumber - 2) inSection:0];
    
    UITableViewCell *cell = [_rootView.tableView cellForRowAtIndexPath:textFieldIndexPath];
    
    if ([cell isKindOfClass:[InputComplaintInfoTableViewCell class]]) {
        InputComplaintInfoTableViewCell *inputInfoCell = (InputComplaintInfoTableViewCell *)cell;
        NSString *complaintInfo = [inputInfoCell.textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
        [self submitComplaintWithInfo:complaintInfo type:_complaintType];
        
    } else {
        [self submitComplaintWithInfo:@"" type:_complaintType];
    }
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [_rootView endEditing:YES];
}

#pragma mark UITextView Delegate
- (void)textViewDidChange:(UITextView *)textView {
    if (textView.text.length > 0) {
        UIView *view = textView.superview;
        if ([view isKindOfClass:[InputComplaintInfoTableViewCell class]]) {
            InputComplaintInfoTableViewCell *cell = (InputComplaintInfoTableViewCell *)view;
            cell.placeholderLabel.text = @"";
            
            NSInteger characterNumber = 100 - textView.text.length;
            cell.characterNumberLabel.text = [NSString stringWithFormat:@"%ld", (long)characterNumber];
            
            if (characterNumber > 0) {
                cell.characterNumberLabel.textColor = [UIColor colorWithRed:106/255.f green:151/255.f blue:52/255.f alpha:1];
            } else {
                cell.characterNumberLabel.textColor = [UIColor redColor];
            }
            
            cell.characterNumberLabel.frame = CGRectMake(cell.textView.bounds.size.width - 20, cell.textView.bounds.size.height - 10, 20, 10);
        }
    } else {
        UIView *view = textView.superview;
        if ([view isKindOfClass:[InputComplaintInfoTableViewCell class]]) {
            InputComplaintInfoTableViewCell *cell = (InputComplaintInfoTableViewCell *)view;
            cell.placeholderLabel.text = @"我还有其他想说的";
            cell.characterNumberLabel.text = @"100";
            cell.characterNumberLabel.textColor = [UIColor colorWithRed:106/255.f green:151/255.f blue:52/255.f alpha:1];
        }
    }
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
