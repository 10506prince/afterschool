//
//  DakaReviewRootView.h
//  AfterSchool
//
//  Created by lg on 16/1/15.
//  Copyright © 2016年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavigationBarView.h"

@interface DakaReviewRootView : UIView

@property (nonatomic, strong) NavigationBarView * navigationBarView;

@end
