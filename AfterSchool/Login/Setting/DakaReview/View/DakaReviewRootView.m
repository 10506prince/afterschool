//
//  DakaReviewRootView.m
//  AfterSchool
//
//  Created by lg on 16/1/15.
//  Copyright © 2016年 Teson Draw. All rights reserved.
//

#import "DakaReviewRootView.h"

@implementation DakaReviewRootView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubview:self.navigationBarView];
    }
    return self;
}

- (NavigationBarView *)navigationBarView {
    if (!_navigationBarView) {
        
        _navigationBarView = [[NavigationBarView alloc] initWithTitle:@"大咖审核"
                                                           titleColor:nil
                                                      backgroundColor:nil
                                              leftButtonBgViewOriginX:0
                                                 leftButtonImageWidth:22
                                                 leftButtonTitleColor:[UIColor whiteColor]
                                                       leftButtonName:@"返回"
                                                  leftButtonImageName:@"navigation_bar_return_button_left_arrow"
                                                      rightButtonName:nil
                                                 rightButtonImageName:nil];
    }
    return _navigationBarView;
}

@end
