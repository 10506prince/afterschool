//
//  DakaReviewStepOneViewController.h
//  AfterSchool
//
//  Created by lg on 16/1/16.
//  Copyright © 2016年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LGDakaCertificate.h"

@interface DakaReviewStepOneViewController : UIViewController

@property (nonatomic, strong) LGDakaCertificate * dakaCertificate;

@property (nonatomic, weak) IBOutlet UITableView * tableView;
@property (nonatomic, strong) UIButton * reviewBtn;


@end
