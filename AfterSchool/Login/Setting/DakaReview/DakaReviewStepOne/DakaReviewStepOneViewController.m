//
//  DakaReviewStepOneViewController.m
//  AfterSchool
//
//  Created by lg on 16/1/16.
//  Copyright © 2016年 Teson Draw. All rights reserved.
//

#import "DakaReviewStepOneViewController.h"
#import "LGPickerSelectViewController.h"
#import "CityViewController.h"
#import "Common.h"
#import "LGUIImageComponent.h"
#import "DakaReviewTableViewCellOne.h"

@interface OptionModel : NSObject

@property (nonatomic, copy) NSString * name;
@property (nonatomic, copy) NSString * detail;

@end

@implementation OptionModel
- (instancetype)initWithName:(NSString *)name detail:(NSString *)detail
{
    self = [super init];
    if (self) {
        self.name = name;
        self.detail = detail;
    }
    return self;
}
@end


@interface DakaReviewStepOneViewController () <UITableViewDataSource,UITableViewDelegate>

@property (nonatomic, strong) NSArray <NSArray<OptionModel *> *> * options;

@property (nonatomic, strong) UIView * footerView;

@property (nonatomic, strong) LGPickerSelectViewController * pickerSelectVC;

@end

@implementation DakaReviewStepOneViewController

- (void)dealloc
{
    NSLogSelfMethodName;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initData];
    [self initUserInterface];
}

- (void)initData {
    
    NSArray * names = @[@"游戏",@"陪玩城市",@"陪玩每小时身价（元）"];
    NSArray * details = @[@"英雄联盟",@"成都",@"38.00"];
    
//    NSArray * names = @[@"游戏",@"陪玩城市",@"陪玩每小时身价（元）",@"标签（选填）",@"联系方式",@"显示号码"];
//    NSArray * details = @[@"英雄联盟",@"成都",@"38.00",@"萌妹子、萝莉",@"--",@"--"];
    NSMutableArray <OptionModel *> * array = [NSMutableArray array];
    for (int i = 0; i < names.count; i++) {
        OptionModel * option = [[OptionModel alloc] initWithName:names[i] detail:details[i]];
        [array addObject:option];
    }
    
    _options = @[array];
    
    self.dakaCertificate = [[LGDakaCertificate alloc] init];
    self.dakaCertificate.gameId = 0;
    self.dakaCertificate.city = details[1];
    self.dakaCertificate.priceOnline = [details[2] floatValue];
    self.dakaCertificate.priceOffline = [details[2] floatValue];
    self.dakaCertificate.type = 1;
}

- (void)initUserInterface {
    [self initTableView];
    
    self.pickerSelectVC = [[LGPickerSelectViewController alloc] initWithNibName:@"LGPickerSelectViewController" bundle:nil];
    [self.pickerSelectVC.view setFrame:[UIScreen mainScreen].bounds];
    [self addChildViewController:self.pickerSelectVC];
}

- (void)initTableView {

    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    [self.tableView registerNibCellWithClass:[DakaReviewTableViewCellOne class]];
    
    self.tableView.tableFooterView = self.footerView;
}

- (UIView *)footerView {
    if (!_footerView) {
        _footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, LgScreenWidth, 85)];
        
        [_footerView addSubview:self.reviewBtn];
    }
    return _footerView;
}

- (UIButton *)reviewBtn {
    if (!_reviewBtn) {
        _reviewBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_reviewBtn setFrame:CGRectMake(20, 20, self.footerView.bounds.size.width - 40, 45)];
        
        [_reviewBtn setTitle:@"提交审核" forState:UIControlStateNormal];
        [_reviewBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        [_reviewBtn setBackgroundImage:[UIImage imageWithColor:APPLightColor] forState:UIControlStateNormal];
        [_reviewBtn setBackgroundImage:[UIImage imageWithColor:APPDarkColor] forState:UIControlStateHighlighted];
        
        [_reviewBtn.layer setCornerRadius:3];
        [_reviewBtn.layer setMasksToBounds:YES];
    }
    return _reviewBtn;
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _options.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSArray <OptionModel *> * array = [_options objectAtIndex:section];
    return array.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    switch (indexPath.section) {
            
        case 0:
        {
            switch (indexPath.row) {
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                {
                    OptionModel * option = self.options[indexPath.section][indexPath.row];
                    
                    DakaReviewTableViewCellOne * cell = [tableView dequeueReusableCellWithClass:[DakaReviewTableViewCellOne class]];
                    cell.name = option.name;
                    cell.detail = option.detail;
                    
                    return cell;
                }
                    break;
                    
                default:
                    return [UITableViewCell new];
                    break;
            }
            
        }
            break;
        default:
            return [UITableViewCell new];
            break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return DakaReviewTableViewCellOneHeight;
}

//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
//    return 22.0f;
//}
//
//- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
//    switch (section) {
//        case 1:
//            return LGSetupTableViewFooterViewHeight;
//            break;
//
//        default:
//            return SINGLE_LINE_WIDTH;
//            break;
//    }
//}
#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //NSUInteger index = [self getIndexFromIndexPath:indexPath];
    switch (indexPath.section) {
        case 0:
        {
            switch (indexPath.row) {
                case 0:
                {
                    [self selectGameWithIndexPath:indexPath];
                }
                    break;
                case 1:
                {
                    [self selectCityWithIndexPath:indexPath];
                }
                    break;
                case 2:
                {
//                    [self selectAddressWithIndexPath:indexPath];
                    [self selectPriceWithIndexPath:indexPath];
                }
                    break;
//                case 3:
//                {
//                    [self selectPriceWithIndexPath:indexPath];
//                }
//                    break;
                    
                default:
                    break;
            }
        }
            break;
            
        default:
            break;
    }
}

- (NSUInteger)getIndexFromIndexPath:(NSIndexPath *)indexPath {
    NSUInteger index = 0;
    for (int i = 0; i < indexPath.section; i++) {
        NSUInteger row = [self tableView:self.tableView numberOfRowsInSection:i];
        index += row;
    }
    index += indexPath.row;
    
    return index;
}


#pragma mark - method
///选择游戏
- (void)selectGameWithIndexPath:(NSIndexPath *)indexPath {
    
    OptionModel * option = self.options[indexPath.section][indexPath.row];
    
    __weak typeof(self) weakSelf = self;
    
    NSArray <LGDataSource *> * array = @[
                                         [[LGDataSource alloc] initWithName:@"英雄联盟" value:@"英雄联盟"]
                                         ];
    self.pickerSelectVC.dataSource = array;
    [self.pickerSelectVC addConfirmClickBlock:^(LGDataSource *data) {
        
        option.detail = data.name;
        weakSelf.dakaCertificate.gameId = 0;
        
        [weakSelf.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        [weakSelf.pickerSelectVC.view removeFromSuperview];
    }];
    [self.pickerSelectVC addCancelClickBlock:^(LGDataSource *data) {
    }];
    [self.pickerSelectVC showInView:self.view];
}

///选择陪玩身价
- (void)selectPriceWithIndexPath:(NSIndexPath *)indexPath {
    
    OptionModel * option = self.options[indexPath.section][indexPath.row];
    
    __weak typeof(self) weakSelf = self;
    
    NSArray <LGDataSource *> * array = @[
                                         [[LGDataSource alloc] initWithName:@"38.00" value:@(38.00)],
                                         [[LGDataSource alloc] initWithName:@"48.00" value:@(48.00)],
                                         [[LGDataSource alloc] initWithName:@"58.00" value:@(58.00)],
                                         [[LGDataSource alloc] initWithName:@"68.00" value:@(68.00)],
                                         ];
    self.pickerSelectVC.dataSource = array;
    [self.pickerSelectVC addConfirmClickBlock:^(LGDataSource *data) {
        
        option.detail = data.name;
        weakSelf.dakaCertificate.priceOffline = [data.value floatValue];
        weakSelf.dakaCertificate.priceOnline = [data.value floatValue];
        
        [weakSelf.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        [weakSelf.pickerSelectVC.view removeFromSuperview];
    }];
    [self.pickerSelectVC addCancelClickBlock:^(LGDataSource *data) {
    }];
    [self.pickerSelectVC showInView:self.view];
}

///选择陪玩城市
- (void)selectCityWithIndexPath:(NSIndexPath *)indexPath {
    
    OptionModel * option = self.options[indexPath.section][indexPath.row];

    __weak typeof(self) weakSelf = self;

//    NSArray <LGDataSource *> * array = @[
//                                         [[LGDataSource alloc] initWithName:@"成都" value:@"成都"]
//                                         ];
//    self.pickerSelectVC.dataSource = array;
//    [self.pickerSelectVC addConfirmClickBlock:^(LGDataSource *data) {
//        
//        option.detail = data.value;
//        weakSelf.dakaCertificate.city = data.value;
//        
//        [weakSelf.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
//        [weakSelf.pickerSelectVC.view removeFromSuperview];
//    }];
//    [self.pickerSelectVC addCancelClickBlock:^(LGDataSource *data) {
//    }];
//    [self.pickerSelectVC showInView:self.view];
    
    CityViewController * selectCityVC = [[CityViewController alloc] init];
    [selectCityVC addFinishBlock:^(NSString *city) {
        option.detail = city;
        weakSelf.dakaCertificate.city = city;

        [weakSelf.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        [weakSelf.pickerSelectVC.view removeFromSuperview];
    }];
    [self.navigationController pushViewController:selectCityVC animated:YES];
}

///选择默认地点
- (void)selectAddressWithIndexPath:(NSIndexPath *)indexPath {
    
    OptionModel * option = self.options[indexPath.section][indexPath.row];
    
    __weak typeof(self) weakSelf = self;
    
    NSArray <LGDataSource *> * array = @[
                                         [[LGDataSource alloc] initWithName:@"软件园" value:@"软件园"]
                                         ];
    self.pickerSelectVC.dataSource = array;
    [self.pickerSelectVC addConfirmClickBlock:^(LGDataSource *data) {
        option.detail = data.value;
        [weakSelf.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        [weakSelf.pickerSelectVC.view removeFromSuperview];
    }];
    [self.pickerSelectVC addCancelClickBlock:^(LGDataSource *data) {
    }];
    [self.pickerSelectVC showInView:self.view];
}

///选择标签
///选择联系方式
- (void)selectWithIndexPath:(NSIndexPath *)indexPath {
    
    OptionModel * option = self.options[indexPath.section][indexPath.row];
    
    __weak typeof(self) weakSelf = self;
    
    NSArray <LGDataSource *> * array = @[
                                         [[LGDataSource alloc] initWithName:@"软件园" value:@"软件园"]
                                         ];
    self.pickerSelectVC.dataSource = array;
    [self.pickerSelectVC addConfirmClickBlock:^(LGDataSource *data) {
        option.detail = data.value;
        [weakSelf.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        [weakSelf.pickerSelectVC.view removeFromSuperview];
    }];
    [self.pickerSelectVC addCancelClickBlock:^(LGDataSource *data) {
    }];
    [self.pickerSelectVC showInView:self.view];
}
///选择显示号码

@end
