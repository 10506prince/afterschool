//
//  DakaReviewTableViewCellOne.h
//  AfterSchool
//
//  Created by lg on 16/1/15.
//  Copyright © 2016年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITableView+NibCell.h"

#define DakaReviewTableViewCellOneHeight 44

@interface DakaReviewTableViewCellOne : UITableViewCell

@property (nonatomic, copy) NSString * name;
@property (nonatomic, copy) NSString * detail;

@end
