//
//  DakaReviewTableViewCellOne.m
//  AfterSchool
//
//  Created by lg on 16/1/15.
//  Copyright © 2016年 Teson Draw. All rights reserved.
//

#import "DakaReviewTableViewCellOne.h"
#import "CAShapeLayer+CustomPath.h"

@interface DakaReviewTableViewCellOne ()

@property (nonatomic, weak) IBOutlet UILabel * nameLabel;
@property (nonatomic, weak) IBOutlet UILabel * detailLabel;

@property (nonatomic, strong) CAShapeLayer * lineLayer;
@property (nonatomic, strong) CAShapeLayer * arrowLayer;

@end

@implementation DakaReviewTableViewCellOne

- (void)awakeFromNib {
    self.lineLayer = [CAShapeLayer getLineLayer];
    self.arrowLayer = [CAShapeLayer getArrowLayer];
    
    [self.layer addSublayer:self.lineLayer];
    [self.layer addSublayer:self.arrowLayer];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.lineLayer.path = [CAShapeLayer linePathWithRect:self.bounds edgeInsets:UIEdgeInsetsMake(0, 20, 0, 20)];
    self.arrowLayer.path = [CAShapeLayer arrowPathWithRect:self.bounds edgeInsets:UIEdgeInsetsMake(0, 0, 0, 20)];
}
#pragma mark - setValue
- (void)setName:(NSString *)name {
    _name = [name copy];
    
    [self.nameLabel setText:_name];
}
- (void)setDetail:(NSString *)detail {
    _detail = [detail copy];
    
    [self.detailLabel setText:_detail];
}

@end
