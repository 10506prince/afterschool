//
//  DakaReviewViewController.m
//  AfterSchool
//
//  Created by lg on 16/1/15.
//  Copyright © 2016年 Teson Draw. All rights reserved.
//

#import "DakaReviewViewController.h"
#import "DakaReviewRootView.h"
#import "Common.h"
#import "LGDefineNetServer.h"
#import "SVProgressHUD.h"
#import "TDSingleton.h"

#import "DakaReviewStepOneViewController.h"
#import "DakaReviewStepTwoViewController.h"
#import "DakaReviewStepThreeViewController.h"

@interface DakaReviewViewController () {
    LGDakaCertificate * _currentDakaCertificate;
}

@property (nonatomic, assign) NSUInteger reviewState;
@property (nonatomic, strong) DakaReviewRootView * rootView;

@property (nonatomic, strong) DakaReviewStepOneViewController * oneVC;
@property (nonatomic, strong) DakaReviewStepTwoViewController * twoVC;
@property (nonatomic, strong) DakaReviewStepThreeViewController * threeVC;

@end

@implementation DakaReviewViewController

- (void)dealloc
{
    NSLogSelfMethodName;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initData];
    [self initUserInterface];
}

- (void)initData {
    LGDakaCertificate * dakaCertificate = [[TDSingleton instance].userInfoModel.subUsersSimple firstObject];
    self.reviewState = dakaCertificate.certifyState;
}

- (void)initUserInterface {
    [self.view addSubview:self.rootView];
    
    switch (self.reviewState) {
        case 0:
        {
            self.oneVC = [[DakaReviewStepOneViewController alloc] initWithNibName:@"DakaReviewStepOneViewController" bundle:nil];
            [self addChildViewController:self.oneVC];
            [self.oneVC.view setFrame:[UIScreen mainScreen].bounds];
            
            UIEdgeInsets ed = UIEdgeInsetsMake(64, 0, 0, 0);
            [self.oneVC.tableView setContentInset:ed];
            [self.oneVC.tableView setScrollIndicatorInsets:ed];
            [self.oneVC.reviewBtn addTarget:self action:@selector(reviewAction:) forControlEvents:UIControlEventTouchUpInside];
            
            
            [self.rootView insertSubview:self.oneVC.view belowSubview:self.rootView.navigationBarView];
        }
            break;
        case 1:
        {
            [self showStepTwo];
//            self.twoVC = [[DakaReviewStepTwoViewController alloc] initWithNibName:@"DakaReviewStepTwoViewController" bundle:nil];
//            [self addChildViewController:self.twoVC];
//            [self.twoVC.view setFrame:[UIScreen mainScreen].bounds];
//            
//            [self.rootView insertSubview:self.twoVC.view belowSubview:self.rootView.navigationBarView];
        }
            break;
        case 2:
        {
            [self showStepThree];
//            self.threeVC = [[DakaReviewStepThreeViewController alloc] initWithNibName:@"DakaReviewStepThreeViewController" bundle:nil];
//            [self addChildViewController:self.threeVC];
//            [self.threeVC.view setFrame:[UIScreen mainScreen].bounds];
//            [self.rootView insertSubview:self.threeVC.view belowSubview:self.rootView.navigationBarView];
        }
            break;
            
        default:
            break;
    }
}
- (DakaReviewRootView *)rootView {
    if (!_rootView) {
        _rootView = [[DakaReviewRootView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        
        [_rootView.navigationBarView.leftButton addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _rootView;
}

#pragma mark - 界面切换

- (void)showStepTwo {
    if (!self.twoVC) {
        self.twoVC = [[DakaReviewStepTwoViewController alloc] initWithNibName:@"DakaReviewStepTwoViewController" bundle:nil];
        [self.twoVC.view setFrame:[UIScreen mainScreen].bounds];
        [self addChildViewController:self.twoVC];
    }
    
    [self.rootView insertSubview:self.twoVC.view belowSubview:self.rootView.navigationBarView];
}

- (void)showStepThree {
    if (!self.threeVC) {
        self.threeVC = [[DakaReviewStepThreeViewController alloc] initWithNibName:@"DakaReviewStepThreeViewController" bundle:nil];
        [self addChildViewController:self.threeVC];
        [self.threeVC.view setFrame:[UIScreen mainScreen].bounds];
    }
    
    [self.rootView insertSubview:self.threeVC.view belowSubview:self.rootView.navigationBarView];

}

#pragma mark - Action
- (IBAction)backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)reviewAction:(id)sender {
    NSLogSelfMethodName;
    
    __weak typeof(self) weakSelf = self;
    
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
    [LGDefineNetServer certificateDaka:self.oneVC.dakaCertificate success:^(NSInteger state) {
        [SVProgressHUD dismiss];
        
        LGDakaCertificate * certificate = [[TDSingleton instance].userInfoModel.subUsersSimple firstObject];
        certificate.certifyState = state;
        [weakSelf showStepTwo];
    } failure:^(NSString *msg) {
        [SVProgressHUD dismissWithError:msg];
    }];
}
@end
