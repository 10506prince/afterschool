//
//  UserCenterRootView.m
//  AfterSchool
//
//  Created by lg on 15/10/22.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "UserCenterRootView.h"
#import "MacrosDefinition.h"
#import "TDDateComponents.h"
#import "UIButton+SetBackgroundColor.h"
#import "Common.h"

@implementation UserCenterRootView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self addSubview:self.tableView];
        [self addSubview:self.navigationBarView];
    }
    return self;
}

- (NavigationBarView *)navigationBarView {
    if (!_navigationBarView) {
        _navigationBarView = [[NavigationBarView alloc] initWithTitle:@"我的资料"
                                                           titleColor:nil
                                                      backgroundColor:nil
                                              leftButtonBgViewOriginX:0
                                                 leftButtonImageWidth:22
                                                 leftButtonTitleColor:[UIColor whiteColor]
                                                       leftButtonName:@"返回"
                                                  leftButtonImageName:@"navigation_bar_return_button_left_arrow" rightButtonName:nil
                                                 rightButtonImageName:nil];
    }
    return _navigationBarView;
}

- (LGTableView *)tableView {
    if (!_tableView) {
        _tableView = [[LGTableView alloc] initWithFrame:self.bounds style:UITableViewStylePlain];
        
        [_tableView setBackgroundColor:LgColor(230, 1)];
        [_tableView setTableFooterView:self.tableFooterView];
        
        [_tableView registerNibCellWithClass:[LGUserCenterTableViewCellOne class]];
        [_tableView registerNibCellWithClass:[LGUserCenterTableViewCellTwo class]];
        [_tableView registerNibCellWithClass:[LGUserCenterTableViewCellThree class]];
        
        _tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectZero];
    }
    return _tableView;
}

- (UIView *)tableFooterView {
    if (!_tableFooterView) {
        _tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, 85)];
        
        [_tableFooterView addSubview:self.exitButton];
    }
    return _tableFooterView;
}

- (UIButton *)exitButton {
    if (!_exitButton) {
        _exitButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_exitButton setFrame:CGRectMake(20, 20, self.tableFooterView.bounds.size.width - 40, 45)];
        
        [_exitButton setTitle:@"退出账号" forState:UIControlStateNormal];
        [_exitButton setBackgroundColor:APPOrangeLightColor forState:UIControlStateNormal];
        [_exitButton setBackgroundColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
        
        _exitButton.layer.cornerRadius = 3;
        _exitButton.layer.masksToBounds = YES;
        _exitButton.titleLabel.font = [UIFont systemFontOfSize:19];
    }
    
    return _exitButton;
}

- (TDInputTextView *)changeNameInputTextView {
    if (!_changeNameInputTextView) {
        _changeNameInputTextView = [[TDInputTextView alloc] initWithFrame:CGRectMake((SCREEN_WIDTH - 300) / 2, -144, 300, 144) targetPosition:CGPointMake((SCREEN_WIDTH - 300) / 2, 64)];
        _changeNameInputTextView.titleLabel.text = @"编辑名字";
        _changeNameInputTextView.cancelButton.tag = 100;
        _changeNameInputTextView.confirmButton.tag = 102;
    }
    return _changeNameInputTextView;
}

- (TDInputTextView *)changeSignatureInputTextView {
    if (!_changeSignatureInputTextView) {
        _changeSignatureInputTextView = [[TDInputTextView alloc] initWithFrame:CGRectMake((SCREEN_WIDTH - 300) / 2, -144, 300, 144) targetPosition:CGPointMake((SCREEN_WIDTH - 300) / 2, 64)];
        _changeSignatureInputTextView.titleLabel.text = @"编辑个性签名";
        _changeSignatureInputTextView.cancelButton.tag = 101;
        _changeSignatureInputTextView.confirmButton.tag = 103;
    }
    return _changeSignatureInputTextView;
}

- (TDDatePickerView *)datePickerView {
    if (!_datePickerView) {
        _datePickerView = [[TDDatePickerView alloc] initWithFrame:CGRectMake((SCREEN_WIDTH - 300) / 2, SCREEN_HEIGHT, 300, 244) dateComponents:[TDDateComponents dateComponentsWithYear:2010 month:11 day:12] targetPosition:CGPointMake((SCREEN_WIDTH - 300) / 2, SCREEN_HEIGHT - 300)];
    }
    return _datePickerView;
}

@end
