//
//  UserCenterRootView.h
//  AfterSchool
//
//  Created by lg on 15/10/22.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavigationBarView.h"
#import "LGTableView.h"

#import "LGUserCenterTableViewCellOne.h"
#import "LGUserCenterTableViewCellTwo.h"
#import "LGUserCenterTableViewCellThree.h"

#import "TDInputTextView.h"
#import "TDDatePickerView.h"

@interface UserCenterRootView : UIView

@property (nonatomic, strong) NavigationBarView * navigationBarView;
@property (nonatomic, strong) LGTableView * tableView;

@property (nonatomic, strong) UIView *tableFooterView;
@property (nonatomic, strong) UIButton *exitButton;



@property (nonatomic, strong) TDInputTextView *changeNameInputTextView;
@property (nonatomic, strong) TDInputTextView *changeSignatureInputTextView;

@property (nonatomic, strong) TDDatePickerView *datePickerView;

@end
