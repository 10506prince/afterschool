//
//  LGUserCenterTableViewCellThree.h
//  AfterSchool
//
//  Created by lg on 15/10/22.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#define LGUserCenterTableViewCellThreeHeight 44.0f

@interface LGUserCenterTableViewCellThree : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel * name;
@property (nonatomic, strong) IBOutlet UIImageView * img;

@end
