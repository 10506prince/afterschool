//
//  LGUserCenterTableViewCell.m
//  AfterSchool
//
//  Created by lg on 15/10/22.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGUserCenterTableViewCellOne.h"
#import "Common.h"
#import "CAShapeLayer+CustomPath.h"

@interface LGUserCenterTableViewCellOne ()

@property (nonatomic, strong) CAShapeLayer * arrow;

@end

@implementation LGUserCenterTableViewCellOne

- (void)awakeFromNib {
    [self setUp];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)setUp {
    self.arrow = [CAShapeLayer getArrowLayer];
    [self.layer addSublayer:self.arrow];
    
    [self.img.layer setCornerRadius:3];
    [self.img.layer setBorderColor:[UIColor whiteColor].CGColor];
    [self.img.layer setBorderWidth:5/[UIScreen mainScreen].scale];
    
    [self.img.layer setMasksToBounds:YES];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.arrow.path = [CAShapeLayer arrowPathWithRect:self.bounds edgeInsets:UIEdgeInsetsMake(0, 0, 0, 20)];
}
@end
