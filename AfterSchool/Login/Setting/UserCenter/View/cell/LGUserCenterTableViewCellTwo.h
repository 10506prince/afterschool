//
//  LGUserCenterTableViewCellTwo.h
//  AfterSchool
//
//  Created by lg on 15/10/22.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>

#define LGUserCenterTableViewCellTwoHeight 44.0f

@interface LGUserCenterTableViewCellTwo : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel * name;
@property (nonatomic, weak) IBOutlet UILabel * detail;

@property (nonatomic) BOOL arrowHide;

@end
