//
//  LGUserCenterTableViewCell.h
//  AfterSchool
//
//  Created by lg on 15/10/22.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>

#define LGUserCenterTableViewCellOneHeight 84.0f

@interface LGUserCenterTableViewCellOne : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel * name;
@property (nonatomic, weak) IBOutlet UIImageView * img;

@end
