//
//  LGUserCenterTableViewCellTwo.m
//  AfterSchool
//
//  Created by lg on 15/10/22.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGUserCenterTableViewCellTwo.h"
#import "Common.h"
#import "CAShapeLayer+CustomPath.h"

@interface LGUserCenterTableViewCellTwo ()

@property (nonatomic, strong) CAShapeLayer * arrow;

@end

@implementation LGUserCenterTableViewCellTwo

- (void)awakeFromNib {
    self.arrow = [CAShapeLayer getArrowLayer];
    [self.layer addSublayer:self.arrow];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.arrow.path = [CAShapeLayer arrowPathWithRect:self.bounds edgeInsets:UIEdgeInsetsMake(0, 0, 0, 20)];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

- (void)setArrowHide:(BOOL)arrowHide {
    _arrowHide = arrowHide;
    
    [self.arrow setHidden:_arrowHide];
}

@end
