//
//  SelectServerRootView.m
//  AfterSchool
//
//  Created by lg on 15/11/27.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "SelectServerRootView.h"
#import "Common.h"
#import "UIImage+ImageWithColor.h"

@implementation SelectServerRootView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self addSubview:self.tableView];
        
        [self addSubview:self.navigationBarView];
    }
    return self;
}

- (NavigationBarView *)navigationBarView {
    if (!_navigationBarView) {
        
        _navigationBarView = [[NavigationBarView alloc] initWithTitle:@"服务器"
                                                           titleColor:nil
                                                      backgroundColor:nil
                                              leftButtonBgViewOriginX:0
                                                 leftButtonImageWidth:22
                                                 leftButtonTitleColor:[UIColor whiteColor]
                                                       leftButtonName:@"返回"
                                                  leftButtonImageName:@"navigation_bar_return_button_left_arrow" rightButtonName:nil
                                                 rightButtonImageName:nil];
    }
    return _navigationBarView;
}

- (LGTableView *)tableView {
    if (!_tableView) {
        _tableView = [[LGTableView alloc] initWithFrame:self.bounds style:UITableViewStylePlain];
        
        [_tableView registerNibCellWithClass:[SelectServerTableViewCellOne class]];
        [_tableView registerClass:[SelectServerHeaderView class] forHeaderFooterViewReuseIdentifier:NSStringFromClass([SelectServerHeaderView class])];
        
        _tableView.tableHeaderView = self.searchBar;
        _tableView.sectionIndexColor = LgColor(120, 1);
        //_tableView.sectionIndexMinimumDisplayRowCount = 0;
        //_tableView.sectionIndexBackgroundColor = [UIColor clearColor];
        //_tableView.sectionIndexTrackingBackgroundColor = [UIColor redColor];
    }
    return _tableView;
}

- (UISearchBar *)searchBar {
    if (!_searchBar) {
        _searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, 44)];
        [_searchBar setPlaceholder:@"搜索"];
        
        [_searchBar setBackgroundImage:nil];
        //[_searchBar setBarTintColor:[UIColor getColor:@"ebebeb"]];
        //[_searchBar setBackgroundColor:[UIColor getColor:@"ebebeb"]];
        
        [_searchBar setBackgroundImage:[UIImage imageWithColor:[UIColor groupTableViewBackgroundColor]]];
    }
    return _searchBar;
}

@end
