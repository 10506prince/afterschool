//
//  SelectServerHeaderView.h
//  AfterSchool
//
//  Created by lg on 15/11/27.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>

#define SelectServerHeaderViewHeight 22.0f

@interface SelectServerHeaderView : UITableViewHeaderFooterView

@property (nonatomic, copy) NSString * title;

@end
