//
//  SelectServerTableViewCellOne.m
//  AfterSchool
//
//  Created by lg on 15/11/27.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "SelectServerTableViewCellOne.h"

@interface SelectServerTableViewCellOne ()

@property (nonatomic, weak) IBOutlet UILabel * serverLabel;

@end
@implementation SelectServerTableViewCellOne

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)setServer:(NSString *)server {
    [self.serverLabel setText:server];
}

- (NSString *)server {
    return self.serverLabel.text;
}

@end
