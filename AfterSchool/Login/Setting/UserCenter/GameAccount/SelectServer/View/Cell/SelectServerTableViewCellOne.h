//
//  SelectServerTableViewCellOne.h
//  AfterSchool
//
//  Created by lg on 15/11/27.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>

#define SelectServerTableViewCellOneHeight 50.0f

@interface SelectServerTableViewCellOne : UITableViewCell

@property (nonatomic, copy) NSString * server;

@end
