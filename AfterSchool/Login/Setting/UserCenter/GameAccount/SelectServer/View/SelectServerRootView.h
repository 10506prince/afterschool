//
//  SelectServerRootView.h
//  AfterSchool
//
//  Created by lg on 15/11/27.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavigationBarView.h"
#import "LGTableView.h"
#import "SelectServerTableViewCellOne.h"
#import "SelectServerHeaderView.h"

@interface SelectServerRootView : UIView

@property (nonatomic, strong) NavigationBarView * navigationBarView;
@property (nonatomic, strong) LGTableView * tableView;
@property (nonatomic, strong) UISearchBar * searchBar;

@end
