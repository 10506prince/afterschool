//
//  SelectServerViewController.m
//  AfterSchool
//
//  Created by lg on 15/11/27.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "SelectServerViewController.h"
#import "Common.h"
#import "ZYPinYinSearch.h"
#import "PinYinForObjc.h"
#import "TDSingleton.h"
#import "LGDefineNetServer.h"

#import "TalkingData.h"


@interface SelectServerViewController () <UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate> {
    
    NSArray * _dataSource;
    
    UIImageView   *_bgImageView;
    UIView        *_tipsView;
    UILabel       *_tipsLab;
    NSTimer       *_timer;
}

@property (strong, nonatomic) NSMutableDictionary *searchResultDic;

@property (strong, nonatomic) NSMutableDictionary *servers;///<服务器数据(从上一级传过来的)
@property (strong, nonatomic) NSMutableArray *keys; ///<城市首字母

@end

@implementation SelectServerViewController

- (void)dealloc
{
    NSLogSelfMethodName;
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [TalkingData trackPageBegin:selfControllerName];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [TalkingData trackPageEnd:selfControllerName];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initData];
    [self initUserInterface];
}

- (void)initData {
    _servers = [NSMutableDictionary dictionary];
    _keys = [NSMutableArray array];
    
    [self getServers];
}

- (void)initUserInterface {
    [self.view addSubview:self.rootView];
}


- (SelectServerRootView *)rootView {
    if (!_rootView) {
        _rootView = [[SelectServerRootView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        [_rootView.navigationBarView.leftButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        
        _rootView.tableView.dataSource = self;
        _rootView.tableView.delegate = self;
        _rootView.searchBar.delegate = self;
    }
    
    return _rootView;
}

- (void)backAction {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableViewDataSource 列表数据代理
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _keys.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSString *key = [_keys objectAtIndex:section];
    NSArray *serverSection = [_servers objectForKey:key];
    
    return [serverSection count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString * key = [_keys objectAtIndex:indexPath.section];
    
    SelectServerTableViewCellOne * cell = [tableView dequeueReusableCellWithClass:[SelectServerTableViewCellOne class]];
    
    NSArray * servers = [_servers objectForKey:key];
    cell.server = servers[indexPath.row];
    
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    NSString * key = _keys[section];
    SelectServerHeaderView * header = [tableView dequeueReusableHeaderFooterViewWithClass:[SelectServerHeaderView class]];
    header.title = key;
    return header;
}

- (NSArray<NSString *> *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    return [NSArray arrayWithArray:_keys];
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index {
    [self showTipsWithTitle:title];
    return index;
}
#pragma mark - UITableViewDelegate 列表代理
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString * key = _keys[indexPath.section];
    NSArray * servers = [_servers objectForKey:key];
    
    [self.lastVc setServer:servers[indexPath.row]];
    [self performSelector:@selector(backAction) withObject:nil afterDelay:0.3];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return SelectServerHeaderViewHeight;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return SelectServerTableViewCellOneHeight;
}

- (void)showTipsWithTitle:(NSString*)title
{
    //获取当前屏幕window
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    //添加黑色透明背景
    //    if (!_bgImageView) {
    //        _bgImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, window.frame.size.width, window.frame.size.height)];
    //        _bgImageView.backgroundColor = [UIColor blackColor];
    //        _bgImageView.alpha = 0.1;
    //        [window addSubview:_bgImageView];
    //    }
    if (!_tipsView) {
        //添加字母提示框
        _tipsView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 80, 80)];
        _tipsView.center = window.center;
        _tipsView.backgroundColor = [UIColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:0.8];
        //设置提示框圆角
        _tipsView.layer.masksToBounds = YES;
        _tipsView.layer.cornerRadius  = _tipsView.frame.size.width/20;
        _tipsView.layer.borderColor   = [UIColor whiteColor].CGColor;
        _tipsView.layer.borderWidth   = 2;
        [window addSubview:_tipsView];
    }
    if (!_tipsLab) {
        //添加提示字母lable
        _tipsLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, _tipsView.frame.size.width, _tipsView.frame.size.height)];
        //设置背景为透明
        _tipsLab.backgroundColor = [UIColor clearColor];
        _tipsLab.font = [UIFont boldSystemFontOfSize:50];
        _tipsLab.textAlignment = NSTextAlignmentCenter;
        
        [_tipsView addSubview:_tipsLab];
    }
    _tipsLab.text = title;//设置当前显示字母
    
    //[self performSelector:@selector(hiddenTipsView) withObject:nil afterDelay:0.3];

//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        [self hiddenTipsView];
//    });
    
    
    _timer = [NSTimer timerWithTimeInterval:1 target:self selector:@selector(hiddenTipsView) userInfo:nil repeats:NO];
    [[NSRunLoop mainRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
    
}

- (void)hiddenTipsView
{
    [UIView animateWithDuration:0.2 animations:^{
        _bgImageView.alpha = 0;
        _tipsView.alpha = 0;
    } completion:^(BOOL finished) {
        [_bgImageView removeFromSuperview];
        [_tipsView removeFromSuperview];
        _bgImageView = nil;
        _tipsLab     = nil;
        _tipsView    = nil;
    }];
}

#pragma mark - UISearchBarDelegate 搜索代理
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    [self filterContentForSearchText:searchText];
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    
}

/**
 *  通过搜索条件过滤得到搜索结果
 *
 *  @param searchText 关键词
 *  @param scope      范围
 */
- (void)filterContentForSearchText:(NSString*)searchText {
    
    if (searchText.length > 0) {
        _searchResultDic = nil;
        _searchResultDic = [[NSMutableDictionary alloc]init];
        
        //搜索数组中是否含有关键字
        NSArray *resultAry  = [ZYPinYinSearch searchWithOriginalArray:_serverList andSearchText:searchText andSearchByPropertyName:@""];
        NSLog(@"搜索结果:%@",resultAry) ;
        
        for (NSString*city in resultAry) {
            //获取字符串拼音首字母并转为大写
            NSString *pinYinHead = [PinYinForObjc chineseConvertToPinYinHead:city].uppercaseString;
            NSString *firstHeadPinYin = [pinYinHead substringToIndex:1]; //拿到字符串第一个字的首字母
            //        NSLog(@"pinYin = %@",firstHeadPinYin);
            
            
            NSMutableArray *cityAry = [NSMutableArray arrayWithArray:[_searchResultDic objectForKey:firstHeadPinYin]]; //取出首字母数组
            
            if (cityAry != nil) {
                
                [cityAry addObject:city];
                
                NSArray *sortCityArr = [cityAry sortedArrayUsingFunction:serverNameSort context:NULL];
                [_searchResultDic setObject:sortCityArr forKey:firstHeadPinYin];
                
            }else
            {
                cityAry= [[NSMutableArray alloc]init];
                [cityAry addObject:city];
                NSArray *sortCityArr = [cityAry sortedArrayUsingFunction:serverNameSort context:NULL];
                [_searchResultDic setObject:sortCityArr forKey:firstHeadPinYin];
            }
        }

        _servers = nil;
        _servers = _searchResultDic;
        [_keys removeAllObjects];
        //按字母升序排列
        [_keys addObjectsFromArray:[[_servers allKeys] sortedArrayUsingSelector:@selector(compare:)]] ;
        [self.rootView.tableView reloadData];
    }else
    {
        //当字符串清空时 回到初始状态
        _servers = nil;
        [_keys removeAllObjects];
        [self getServers];
        [self.rootView.tableView reloadData];
    }
}

- (void)getServers {
    for (NSString * server in _serverList) {
        //获取字符串拼音首字母并转为大写
        NSString *pinYinHead = [PinYinForObjc chineseConvertToPinYinHead:server].uppercaseString;
        NSString *firstHeadPinYin = [pinYinHead substringToIndex:1]; //拿到字符串第一个字的首字母
        
        NSMutableArray *serverAry = [_servers objectForKey:firstHeadPinYin]; //取出首字母数组
        
        if (serverAry != nil) {
            [serverAry addObject:server];
            [serverAry sortedArrayUsingFunction:serverNameSort context:NULL];
            [_servers setObject:serverAry forKey:firstHeadPinYin];
        } else {
            serverAry= [NSMutableArray array];
            [serverAry addObject:server];
            [_servers setObject:serverAry forKey:firstHeadPinYin];
        }
    }
    [_keys removeAllObjects];
    //按字母升序排列
    [_keys addObjectsFromArray:[[_servers allKeys] sortedArrayUsingSelector:@selector(compare:)]];
}

NSInteger serverNameSort(id str1, id str2, void *context)
{
    NSString *string1 = (NSString*)str1;
    NSString *string2 = (NSString*)str2;
    
    return  [string1 localizedCompare:string2];
}

//- (void)setCityWithStr:(NSString *)cityStr {
//    NSLog(@"设置城市为：%@", cityStr);
//    
//    [LGDefineNetServer geocodeWithCity:cityStr address:cityStr success:^(id result) {
//        if ([result isKindOfClass:[LGGeoCodeResult class]]) {
//            
//            [TDSingleton instance].cityName = cityStr;
//            [TDSingleton instance].cityPt = ((LGGeoCodeResult *)result).locationValue;
//            NSLog(@"设置城市成功");
//            
//            [[NSNotificationCenter defaultCenter] postNotificationName:setCityName object:nil];
//            [self backAction];
//        }
//    } failure:^{
//        NSLog(@"设置城市失败");
//    }];
//}

@end
