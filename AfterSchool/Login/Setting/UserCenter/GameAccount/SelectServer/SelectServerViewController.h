//
//  SelectServerViewController.h
//  AfterSchool
//
//  Created by lg on 15/11/27.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGViewController.h"
#import "SelectServerRootView.h"
#import "AddGameAccountViewController.h"

@interface SelectServerViewController : LGViewController

@property (nonatomic, strong) AddGameAccountViewController * lastVc;
@property (nonatomic, strong) NSArray * serverList;

@property (nonatomic, strong) SelectServerRootView * rootView;

@end
