//
//  FXHPlayerTextField.m
//  AfterSchool
//
//  Created by lg on 15/11/27.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "FXHPlayerTextField.h"

@implementation FXHPlayerTextField

- (CGRect)borderRectForBounds:(CGRect)bounds {//感觉基本上不会执行
    CGRect borderRect = [super borderRectForBounds:bounds];
    UIEdgeInsets ed = UIEdgeInsetsMake(0, 12, 0, 12);
    borderRect.origin.x += ed.left;
    borderRect.origin.y += ed.top;
    borderRect.size.width -= ed.left + ed.right;
    borderRect.size.height -= ed.top + ed.bottom;
    return borderRect;
}
- (CGRect)textRectForBounds:(CGRect)bounds {//输入文本偏移
    CGRect textRect = [super textRectForBounds:bounds];
    UIEdgeInsets ed = UIEdgeInsetsMake(0, 0, 0, 0);
    textRect.origin.x += ed.left;
    textRect.origin.y += ed.top;
    textRect.size.width -= ed.left + ed.right;
    textRect.size.height -= ed.top + ed.bottom;
    return textRect;
}
- (CGRect)placeholderRectForBounds:(CGRect)bounds {//testRect偏移之后，这个也跟着偏移了
    CGRect placeholderRect = [super placeholderRectForBounds:bounds];
    UIEdgeInsets ed = UIEdgeInsetsMake(0, 0, 0, 0);
    placeholderRect.origin.x += ed.left;
    placeholderRect.origin.y += ed.top;
    placeholderRect.size.width -= ed.left + ed.right;
    placeholderRect.size.height -= ed.top + ed.bottom;
    return placeholderRect;
}
- (CGRect)editingRectForBounds:(CGRect)bounds {//编辑文本偏移
    CGRect editingRect = [super editingRectForBounds:bounds];
    UIEdgeInsets ed = UIEdgeInsetsMake(0, 0, 0, 0);
    editingRect.origin.x += ed.left;
    editingRect.origin.y += ed.top;
    editingRect.size.width -= ed.left + ed.right;
    editingRect.size.height -= ed.top + ed.bottom;
    return editingRect;
}
- (CGRect)clearButtonRectForBounds:(CGRect)bounds {
    CGRect clearButtonRect = [super clearButtonRectForBounds:bounds];
    UIEdgeInsets ed = UIEdgeInsetsMake(0, 0, 0, 0);
    clearButtonRect.origin.x += ed.left;
    clearButtonRect.origin.y += ed.top;
    clearButtonRect.size.width -= ed.left + ed.right;
    clearButtonRect.size.height -= ed.top + ed.bottom;
    return clearButtonRect;
}
- (CGRect)leftViewRectForBounds:(CGRect)bounds {
    CGRect leftViewRect = [super leftViewRectForBounds:bounds];
    UIEdgeInsets ed = UIEdgeInsetsMake(0, 12, 0, -12);
    leftViewRect.origin.x += ed.left;
    leftViewRect.origin.y += ed.top;
    //leftViewRect.size.width -= ed.left + ed.right;
    //leftViewRect.size.height -= ed.top + ed.bottom;
    return leftViewRect;
}
- (CGRect)rightViewRectForBounds:(CGRect)bounds {
    CGRect rightViewRect = [super rightViewRectForBounds:bounds];
    UIEdgeInsets ed = UIEdgeInsetsMake(0, 12, 0, -12);
    rightViewRect.origin.x += ed.left;
    rightViewRect.origin.y += ed.top;
    //rightViewRect.size.width -= ed.left + ed.right;
    //rightViewRect.size.height -= ed.top + ed.bottom;
    return rightViewRect;
}

@end
