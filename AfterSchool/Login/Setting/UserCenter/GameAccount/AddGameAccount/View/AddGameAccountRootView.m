//
//  AddGameAccountRootView.m
//  AfterSchool
//
//  Created by lg on 15/11/7.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "AddGameAccountRootView.h"
#import "Common.h"
#import "UIButton+SetBackgroundColor.h"

@implementation AddGameAccountRootView

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setUp];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self setUp];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUp];
    }
    return self;
}

- (void)setUp {
    [self addSubview:self.scrollView];
    [self addSubview:self.navigationView];
}

- (NavigationBarView *)navigationView {
    if (!_navigationView) {
        _navigationView = [[NavigationBarView alloc] initWithTitle:@"添加游戏账号"
                                                           titleColor:nil
                                                      backgroundColor:nil
                                              leftButtonBgViewOriginX:0
                                                 leftButtonImageWidth:22
                                                 leftButtonTitleColor:[UIColor whiteColor]
                                                       leftButtonName:@"返回"
                                                  leftButtonImageName:@"navigation_bar_return_button_left_arrow" rightButtonName:nil
                                                 rightButtonImageName:nil];
    }
    return _navigationView;
}

- (LGScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[LGScrollView alloc] initWithFrame:self.bounds];
        
        [_scrollView setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
        [_scrollView setDelaysContentTouches:YES];
        
        [_scrollView addSubview:self.backBtn];
        
        [_scrollView addSubview:self.playerName];
        [_scrollView addSubview:self.serverName];
        [_scrollView addSubview:self.done];
    }
    return _scrollView;
}

- (UIButton *)backBtn {
    if (!_backBtn) {
        _backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_backBtn setBackgroundColor:[UIColor clearColor]];
        [_backBtn setFrame:self.scrollView.bounds];
    }
    return _backBtn;
}

- (FXHPlayerTextField *)playerName {
    if (!_playerName) {
        _playerName = [[FXHPlayerTextField alloc] initWithFrame:CGRectMake(0, 70, self.bounds.size.width, 50)];
        [_playerName setPlaceholder:@"召唤师名字"];
        
        UIImageView * imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 19, 19)];
        [imageView setImage:[UIImage imageNamed:@"tabar_me_normal"]];
        
        _playerName.leftView = imageView;
        _playerName.leftViewMode = UITextFieldViewModeAlways;
        
        [_playerName setContentMode:UIViewContentModeCenter];
        
        [_playerName setBackgroundColor:[UIColor whiteColor]];
        [_playerName setOpaque:YES];
    }
    return _playerName;
}

- (SelectServerBtn *)serverName {
    if (!_serverName) {
        _serverName = [[SelectServerBtn alloc] init];
        [_serverName setFrame:CGRectMake(0, 22, self.bounds.size.width, 50)];
        [_serverName setPlaceholder:@"选择服务器"];
        [_serverName setImg:[UIImage imageNamed:@"tabar_nearby_normal"]];
        
        [_serverName setBackgroundColor:[UIColor whiteColor]];
        [_serverName setOpaque:YES];
    }
    return _serverName;
}

- (UIButton *)done {
    if (!_done) {
        _done = [UIButton buttonWithType:UIButtonTypeSystem];
        [_done setFrame:CGRectMake(12, 22 + 100 + 74, self.bounds.size.width - 24, 46)];
        
        [_done setTitle:@"开始关联" forState:UIControlStateNormal];
        [_done setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_done.titleLabel setFont:[UIFont systemFontOfSize:18]];
        
        [_done setBackgroundColor:APPLightColor forState:UIControlStateNormal];
        [_done setBackgroundColor:[UIColor lightGrayColor] forState:UIControlStateDisabled];
        [_done.layer setCornerRadius:3];
        [_done.layer setMasksToBounds:YES];
        [_done setOpaque:YES];
        
        [_done setEnabled:NO];
    }
    return _done;
}

@end
