//
//  SelectServerBtn.m
//  AfterSchool
//
//  Created by lg on 15/11/27.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "SelectServerBtn.h"
#import "Common.h"

@interface SelectServerBtn () {
    UILabel * _placeholderLabel;
    UILabel * _titleLabel;
    UIImageView * _imageView;
    
    CAShapeLayer * _lineLayer;
    CAShapeLayer * _arrowLayer;
}
@end

@implementation SelectServerBtn

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setUp];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self setUp];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUp];
    }
    return self;
}

- (void)setUp {
    _imageView = [[UIImageView alloc] initWithFrame:CGRectZero];
    
    _placeholderLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    [_placeholderLabel setFont:[UIFont systemFontOfSize:16]];
    [_placeholderLabel setTextColor:LgColor(202, 1)];
    
    _titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    [_titleLabel setFont:[UIFont systemFontOfSize:16]];
    [_titleLabel setTextColor:[UIColor blackColor]];
    
    _lineLayer = [self getLineLayer];
    _arrowLayer = [self getArrowLayer];
    
    [self addSubview:_imageView];
    [self addSubview:_placeholderLabel];
    [self addSubview:_titleLabel];
    
    [self.layer addSublayer:_lineLayer];
    [self.layer addSublayer:_arrowLayer];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    //CGFloat width = self.bounds.size.width;
    CGFloat height = self.bounds.size.height;
    
    [_imageView setFrame:CGRectMake(0, 0, 20, 20)];
    [_imageView setCenter:CGPointMake(22, height/2)];
    

    [_titleLabel sizeToFit];
    [_titleLabel setFrame:CGRectMake(32, height/2 - _titleLabel.bounds.size.height/2, _titleLabel.bounds.size.width, _titleLabel.bounds.size.height)];
    
    
    [_placeholderLabel sizeToFit];
    [_placeholderLabel setFrame:CGRectMake(32, height/2 - _placeholderLabel.bounds.size.height/2, _placeholderLabel.bounds.size.width, _placeholderLabel.bounds.size.height)];
    
    _lineLayer.path = [self getLinePathWithOffset:CGSizeZero];
    _arrowLayer.path = [self getArrowPathWithOffset:CGSizeZero];
}

- (void)setTitle:(NSString *)title {

    [_placeholderLabel setHidden:(title.length > 0)];
    [_titleLabel setText:title];
    [self setNeedsLayout];
}

- (NSString *)title {
    return _titleLabel.text;
}

- (void)setPlaceholder:(NSString *)placeholder {
    [_placeholderLabel setText:placeholder];
    [self setNeedsLayout];
}

- (NSString *)placeholder {
    return _placeholderLabel.text;
}

- (void)setImg:(UIImage *)img {
    _imageView.image = img;
}
- (UIImage *)img {
    return _imageView.image;
}

#pragma mark - Init
- (CAShapeLayer *)getLineLayer {
    CAShapeLayer * line = [CAShapeLayer layer];
    line.lineWidth = 1 / [UIScreen mainScreen].scale;
    line.fillColor = [UIColor clearColor].CGColor;
    line.strokeColor = LgColor(202, 1).CGColor;
    
    return line;
}
- (CAShapeLayer *)getArrowLayer {
    CAShapeLayer * arrow = [CAShapeLayer layer];
    arrow.lineWidth = 2;
    arrow.fillColor = [UIColor clearColor].CGColor;
    arrow.strokeColor = LgColor(202, 1).CGColor;
    
    return arrow;
}

- (CGPathRef)getLinePathWithOffset:(CGSize)offset {
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathMoveToPoint(path, NULL, 15, self.bounds.size.height-1);
    CGPathAddLineToPoint(path, NULL, self.bounds.size.width - 15, self.bounds.size.height - 1);
    
    UIBezierPath * linePath = [UIBezierPath bezierPathWithCGPath:path];
    CGPathRelease(path);
    
    return linePath.CGPath;
}
- (CGPathRef)getArrowPathWithOffset:(CGSize)offset {
    CGMutablePathRef path = CGPathCreateMutable();
    
    CGPoint start = CGPointMake(self.bounds.size.width - 25, self.bounds.size.height/2 + 8);
    CGPoint mid = CGPointMake(self.bounds.size.width - 15, self.bounds.size.height/2);
    CGPoint end = CGPointMake(self.bounds.size.width - 25, self.bounds.size.height/2 - 8);
    
    CGPathMoveToPoint(path, NULL, start.x, start.y);
    CGPathAddLineToPoint(path, NULL, mid.x, mid.y);
    CGPathAddLineToPoint(path, NULL, end.x, end.y);
    
    UIBezierPath * linePath = [UIBezierPath bezierPathWithCGPath:path];
    CGPathRelease(path);
    
    return linePath.CGPath;
}

@end
