//
//  SelectServerBtn.h
//  AfterSchool
//
//  Created by lg on 15/11/27.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectServerBtn : UIControl

@property (nonatomic, copy) NSString * title;
@property (nonatomic, copy) NSString * placeholder;
@property (nonatomic, strong) UIImage * img;

@end
