//
//  AddGameAccountRootView.h
//  AfterSchool
//
//  Created by lg on 15/11/7.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavigationBarView.h"
#import "LGScrollView.h"
#import "FXHPlayerTextField.h"
#import "SelectServerBtn.h"

@interface AddGameAccountRootView : UIView

@property (nonatomic, strong) NavigationBarView * navigationView;
@property (nonatomic, strong) LGScrollView * scrollView;

@property (nonatomic, strong) FXHPlayerTextField * playerName;
@property (nonatomic, strong) SelectServerBtn * serverName;

@property (nonatomic, strong) UIButton * done;

@property (nonatomic, strong) UIButton * backBtn;

@end
