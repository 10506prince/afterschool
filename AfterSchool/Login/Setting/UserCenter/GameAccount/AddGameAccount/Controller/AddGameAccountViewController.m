//
//  AddGameAccountViewController.m
//  AfterSchool
//
//  Created by lg on 15/11/7.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "AddGameAccountViewController.h"
#import "Common.h"
#import "SVProgressHUD.h"
#import "LGDefineNetServer.h"
#import "SelectServerViewController.h"
#import "TDSingleton.h"

#import "TalkingData.h"

@interface AddGameAccountViewController () <UIScrollViewDelegate> {
    NSMutableArray * _serverList;
    
    LGGamePlayerInfo * _addGamePlayer;
}

@property (nonatomic, strong) LGGamePlayerInfo * addGamePlayer;
@property (nonatomic, copy) AddGameAccountViewControllerFinishBlock finishBlock;

@end

@implementation AddGameAccountViewController
- (void)dealloc
{
    NSLogSelfMethodName;
    NSArray * keys = [LGGamePlayerInfo propertys];
    
    for (NSString * key in keys) {
        [_addGamePlayer removeObserver:self forKeyPath:key];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [TalkingData trackPageBegin:selfControllerName];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [TalkingData trackPageEnd:selfControllerName];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initData];
    [self initUserInterface];
}

- (void)initData {
    _serverList = [NSMutableArray array];
    _addGamePlayer = [[LGGamePlayerInfo alloc] init];
    NSArray * keys = [LGGamePlayerInfo propertys];
    
    for (NSString * key in keys) {
        [_addGamePlayer addObserver:self forKeyPath:key options:NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew context:nil];
    }
    
    [self getServerList];
}

- (void)initUserInterface {
    [self.view addSubview:self.rootView];
}

- (AddGameAccountRootView *)rootView {
    if (!_rootView) {
        _rootView = [[AddGameAccountRootView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        [_rootView.navigationView.leftButton addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
        
        [_rootView.backBtn addTarget:self action:@selector(tapAction:) forControlEvents:UIControlEventTouchDown];
        
        [_rootView.serverName addTarget:self action:@selector(selectServerAction:) forControlEvents:UIControlEventTouchUpInside];
        [_rootView.done addTarget:self action:@selector(enterAddAction:) forControlEvents:UIControlEventTouchUpInside];
        
        [_rootView.scrollView setDelegate:self];
        
        [_rootView.playerName addTarget:self action:@selector(textFieldChanged:) forControlEvents:UIControlEventEditingChanged];
    }
    return _rootView;
}

#pragma mark - Network
- (void)getServerList {
    [LGDefineNetServer getGameLOLServerListSuccess:^(id result) {
//        [SVProgressHUD dismiss];
        if ([result isKindOfClass:[NSArray class]]) {
            for (id item in result) {
                if ([item isKindOfClass:[NSString class]]) {
                    //NSDictionary * dic = @{@"name":item,@"value":@""};
                    [_serverList addObject:item];
                }
            }
        }
    } failure:^(NSString *msg) {
//        [SVProgressHUD dismissWithError:msg];
        [SVProgressHUD showErrorWithStatus:msg];
    }];
}

#pragma mark - Action
///点击空白处操作
- (IBAction)tapAction:(id)sender {
    [self.rootView.playerName resignFirstResponder];
}

- (IBAction)backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)selectServerAction:(id)sender {
    
    SelectServerViewController * nextVC = [[SelectServerViewController alloc] init];
    nextVC.lastVc = self;
    nextVC.serverList = _serverList;
    [self.navigationController pushViewController:nextVC animated:YES];
}

- (IBAction)enterAddAction:(id)sender {
    [self.rootView.playerName resignFirstResponder];
    
    NSParameterAssert(_addGamePlayer.playerName);
    NSParameterAssert(_addGamePlayer.serverName);

    [SVProgressHUD showWithStatus:@"处理中..."];
    [LGDefineNetServer addRoleBinding:_addGamePlayer success:^(LGGamePlayerInfo *gamePlayer, LGGamePlayer *defaultGamePlayer) {
        [SVProgressHUD dismissWithSuccess:@"绑定成功"];
        
        [TDSingleton instance].userInfoModel.defaultPlayer = defaultGamePlayer;
        
        AddGameAccountViewControllerFinishBlock block = self.finishBlock;
        block ? block(YES, gamePlayer) : nil;
    
        [self performSelector:@selector(backAction:) withObject:nil afterDelay:1];
    } failure:^(NSString *msg) {
        [SVProgressHUD dismissWithError:msg];
    }];
}

- (void)setServer:(NSString *)serverName {

    [self.rootView.serverName setTitle:serverName];
    self.addGamePlayer.serverName = serverName;
}

#pragma mark - KVO
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
//    NSLog(@"%@",object);
    if ([object isKindOfClass:[LGGamePlayerInfo class]]) {
        if (self.addGamePlayer.playerName.length > 0 && self.addGamePlayer.serverName.length > 0) {
            [_rootView.done setEnabled:YES];
        } else {
            [_rootView.done setEnabled:NO];
        }
    }
}


#pragma mark - UIScrollViewDelegate 

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {

    [self.rootView.playerName resignFirstResponder];
}

#pragma mark - Action

- (void)textFieldChanged:(UITextField *)textField {
    NSLogSelfMethodName;
    
    NSLog(@"%@", textField.text);
    self.addGamePlayer.playerName = textField.text;
}

#pragma mark - Block
- (void)addFinishBlock:(AddGameAccountViewControllerFinishBlock)finishBlcok {
    self.finishBlock = finishBlcok;
}

@end
