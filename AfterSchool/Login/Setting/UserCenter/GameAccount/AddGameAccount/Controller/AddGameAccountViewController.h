//
//  AddGameAccountViewController.h
//  AfterSchool
//
//  Created by lg on 15/11/7.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGViewController.h"
#import "AddGameAccountRootView.h"
#import "GameAccountViewController.h"

typedef void(^AddGameAccountViewControllerFinishBlock)(BOOL success, LGGamePlayerInfo * addGameAccount);
@interface AddGameAccountViewController : LGViewController

@property (nonatomic, strong) AddGameAccountRootView * rootView;

- (void)setServer:(NSString *)serverName;

- (void)addFinishBlock:(AddGameAccountViewControllerFinishBlock)finishBlcok;

@end
