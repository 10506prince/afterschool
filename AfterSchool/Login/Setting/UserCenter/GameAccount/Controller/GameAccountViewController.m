//
//  OrdersViewController.m
//  AfterSchool
//
//  Created by lg on 15/10/22.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "GameAccountViewController.h"
#import "Common.h"
#import "SVProgressHUD.h"
#import "LGDefineNetServer.h"
#import "AddGameAccountViewController.h"
#import "GameDetailViewController.h"
#import "TDSingleton.h"

#import "TalkingData.h"

@interface GameAccountViewController () <UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate> {
    NSMutableArray <LGGamePlayerInfo *> * _gamePlayers;
}

@property (nonatomic, strong) NSMutableArray <LGGamePlayerInfo *> * gamePlayers;

@end

@implementation GameAccountViewController
- (void)dealloc
{
    NSLogSelfMethodName;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [TalkingData trackPageBegin:selfControllerName];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [TalkingData trackPageEnd:selfControllerName];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initData];
    [self initUserInterface];
    [self updateData];
//    [self getGameList];
}

- (void)initData {
    _gamePlayers = [NSMutableArray array];
}
- (void)initUserInterface {
    [self.view addSubview:self.rootView];
}
- (void)updateData {
    if ([TDSingleton instance].userInfoModel.gamePlayers.count == 0) {
        
        __weak typeof(self) weakSelf = self;
        
        [LGDefineNetServer getRoleBindingsSuccess:^(NSArray<LGGamePlayerInfo *> *result, long long defaultAccountId) {
            
            [TDSingleton instance].userInfoModel.gamePlayers = result;
            
            [weakSelf.gamePlayers addObjectsFromArray:result];
            [weakSelf.rootView.tableView reloadData];
            
        } failure:^(NSString *msg) {
            [SVProgressHUD showErrorWithStatus:msg];
        }];
    } else {
        [self.gamePlayers addObjectsFromArray:[TDSingleton instance].userInfoModel.gamePlayers];
        [self.rootView.tableView reloadData];
    }
}

- (GameAccountRootView *)rootView {
    if (!_rootView) {
        _rootView = [[GameAccountRootView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        [_rootView.navigationBarView.leftButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        
        [_rootView.addGameAccountBtn addTarget:self action:@selector(addNewGameAccount:) forControlEvents:UIControlEventTouchUpInside];
        
        [_rootView.tableView setDataSource:self];
        [_rootView.tableView setDelegate:self];
    }
    
    return _rootView;
}

#pragma mark - Action
- (void)backAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)addNewGameAccount:(id)sender {
    NSLogSelfMethodName;
    AddGameAccountViewController * addGameAccountVC = [[AddGameAccountViewController alloc] init];
    
    __weak typeof(self) weakSelf = self;
    [addGameAccountVC addFinishBlock:^(BOOL success, LGGamePlayerInfo *addGameAccount) {
        [weakSelf.gamePlayers addObject:addGameAccount];
        [TDSingleton instance].userInfoModel.gamePlayers = weakSelf.gamePlayers;
        [weakSelf.rootView.tableView reloadData];
    }];

    [self.navigationController pushViewController:addGameAccountVC animated:YES];
}

#pragma mark - UITableViewDataSource 列表数据代理
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return _gamePlayers.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSUInteger index = [self getIndexFromIndexPath:indexPath];
    LGGamePlayerInfo * gamePlayer = _gamePlayers[index];
    
    LGGameAccountTableViewCellOne * cell = [tableView dequeueReusableCellWithClass:[LGGameAccountTableViewCellOne class]];
    cell.gamePlayer = gamePlayer;
    cell.defaultAccount = [TDSingleton instance].userInfoModel.defaultPlayer.id == gamePlayer.id;
    
    [cell setSeparatorInset:UIEdgeInsetsMake(0, 20, 0, 20)];
    return cell;
}
#pragma mark - UITableViewDelegate 列表代理
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    LGGamePlayerInfo * gamePlayerInfo = [_gamePlayers objectAtIndex:indexPath.row];
    
    GameDetailViewController * detailVC = [[GameDetailViewController alloc] initWithGamePlayerInfo:gamePlayerInfo];
    [self.navigationController pushViewController:detailVC animated:YES];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

//- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath {
//    return @"删除";
//}
- (NSArray<UITableViewRowAction *> *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSMutableArray <UITableViewRowAction *> * array = [NSMutableArray array];
    
    __weak typeof(self) weakSelf = self;
    
    UITableViewRowAction * action = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:nil handler:^(UITableViewRowAction * _Nonnull action, NSIndexPath * _Nonnull indexPath) {
    }];
    
    UITableViewRowAction * deleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"删除" handler:^(UITableViewRowAction * _Nonnull action, NSIndexPath * _Nonnull indexPath) {
        NSLog(@"删除");
        
        LGGamePlayerInfo * gamePlayer = weakSelf.gamePlayers[indexPath.row];
        [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
        [LGDefineNetServer removeRoleBinding:gamePlayer success:^{
            [SVProgressHUD dismiss];
            [weakSelf.gamePlayers removeObjectAtIndex:indexPath.row];
            [TDSingleton instance].userInfoModel.gamePlayers = weakSelf.gamePlayers;
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationRight];
        } failure:^(NSString *msg) {
            [tableView setEditing:NO animated:YES];
            [SVProgressHUD dismissWithError:msg];
        }];
    }];
    
    UITableViewRowAction * setDefaultAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"设为默认" handler:^(UITableViewRowAction * _Nonnull action, NSIndexPath * _Nonnull indexPath) {
        
        NSLog(@"设为默认");

        LGGamePlayerInfo * gamePlayer = weakSelf.gamePlayers[indexPath.row];
        [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
        [LGDefineNetServer updateDefaultRoleBinding:gamePlayer success:^(LGGamePlayer *defaultGamePlayer) {
            [SVProgressHUD dismiss];
            [TDSingleton instance].userInfoModel.defaultPlayer = defaultGamePlayer;
            
//            [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            [tableView reloadData];
        } failure:^(NSString *msg) {
//            [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            [tableView setEditing:NO animated:YES];
            [SVProgressHUD dismissWithError:msg];
        }];
    }];
    
    
    LGGamePlayerInfo * gameAccount = self.gamePlayers[indexPath.row];
    
    if (gameAccount.id != [TDSingleton instance].userInfoModel.defaultPlayer.id) {
        [array addObject:deleteAction];
        [array addObject:setDefaultAction];
    }
    [array addObject:action];
    
    return array;
}

//- (void)tableView:(UITableView *)tableView didEndEditingRowAtIndexPath:(NSIndexPath *)indexPath {
//    
//}

//- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
//    switch (editingStyle) {
//        case UITableViewCellEditingStyleDelete:
//        {
//            LGGamePlayerInfo * gamePlayer = _dataSource[indexPath.row];
//            [LGDefineNetServer removeRoleBinding:gamePlayer success:^{
//                [_dataSource removeObjectAtIndex:indexPath.row];
//                [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationRight];
//            } failure:^{
//            }];
//        }
//            break;
//        case UITableViewCellEditingStyleInsert:
//        {
//            
//        }
//            break;
//        case UITableViewCellEditingStyleNone:
//        {
//            
//        }
//            break;
//            
//        default:
//            break;
//    }
//}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return LGGameAccountTableViewCellOneHeight;
}

//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
//    return 0.1f;
//}
//
//- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
//    return 0.1f;
//}

- (NSUInteger)getIndexFromIndexPath:(NSIndexPath *)indexPath {
    NSUInteger index = 0;
    for (int i = 0; i < indexPath.section; i++) {
        NSUInteger row = [self tableView:self.rootView.tableView numberOfRowsInSection:i];
        index += row;
    }
    index += indexPath.row;
    
    return index;
}


@end
