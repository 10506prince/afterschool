//
//  OrdersViewController.h
//  AfterSchool
//
//  Created by lg on 15/10/22.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGViewController.h"
#import "GameAccountRootView.h"

@interface GameAccountViewController : LGViewController

@property (nonatomic, strong) GameAccountRootView * rootView;

@property (nonatomic, assign) BOOL needReloadData;

//- (void)addGamePlayer:(LGGamePlayer *)gamePlayer;

@end
