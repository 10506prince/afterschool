//
//  OrdersRootView.h
//  AfterSchool
//
//  Created by lg on 15/10/22.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavigationBarView.h"
#import "LGTableView.h"
#import "LGGameAccountTableViewCellOne.h"

@interface GameAccountRootView : UIView

@property (nonatomic, strong) NavigationBarView * navigationBarView;
@property (nonatomic, strong) LGTableView * tableView;
@property (nonatomic, strong) UIView * tableViewFooterView;
@property (nonatomic, strong) UIButton * addGameAccountBtn;

@end
