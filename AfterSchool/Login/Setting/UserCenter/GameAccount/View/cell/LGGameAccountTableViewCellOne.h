//
//  LGAddGameAccountTableViewCellOne.h
//  AfterSchool
//
//  Created by lg on 15/10/22.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LGGamePlayer.h"

#define LGGameAccountTableViewCellOneHeight 85.0f

@interface LGGameAccountTableViewCellOne : UITableViewCell

@property (nonatomic, strong) LGGamePlayerInfo * gamePlayer;
@property (nonatomic) BOOL defaultAccount;

@end
