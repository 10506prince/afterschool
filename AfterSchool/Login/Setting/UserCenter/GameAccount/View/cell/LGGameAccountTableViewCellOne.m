//
//  LGAddGameAccountTableViewCellOne.m
//  AfterSchool
//
//  Created by lg on 15/10/22.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGGameAccountTableViewCellOne.h"
#import "UIImageView+WebCache.h"
#import "CAShapeLayer+CustomPath.h"

@interface LGGameAccountTableViewCellOne ()

@property (nonatomic, weak) IBOutlet UIImageView * headImage;
@property (nonatomic, weak) IBOutlet UILabel * name;
@property (nonatomic, weak) IBOutlet UILabel * gameDetail;
@property (nonatomic, weak) IBOutlet UILabel * gameRank;
@property (nonatomic, weak) IBOutlet UILabel * defaultLabel;

@property (nonatomic, strong) CAShapeLayer * arrowLayer;

@end

@implementation LGGameAccountTableViewCellOne

- (void)awakeFromNib {

    self.arrowLayer = [CAShapeLayer getArrowLayer];
    [self.layer addSublayer:self.arrowLayer];
//    [self.headImage.layer setCornerRadius:3];
//    [self.headImage.layer setBorderColor:[UIColor whiteColor].CGColor];
//    [self.headImage.layer setBorderWidth:2];
//    [self.headImage.layer setShadowColor:[UIColor blackColor].CGColor];
//    [self.headImage.layer setShadowOpacity:0.5];
//    [self.headImage.layer setShadowOffset:CGSizeZero];
//    [self.headImage.layer setShadowRadius:3];
//    [self.headImage.layer setMasksToBounds:YES];
//    [self.headImage setClipsToBounds:YES];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.arrowLayer.path = [CAShapeLayer arrowPathWithRect:self.bounds edgeInsets:UIEdgeInsetsMake(0, 0, 0, 20)];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)setGamePlayer:(LGGamePlayerInfo *)gamePlayer {
    _gamePlayer = gamePlayer;
    
    //头像
    NSURL * imageUrl = [NSURL URLWithString:gamePlayer.icon];
    [self.headImage sd_setImageWithURL:imageUrl placeholderImage:[UIImage imageNamed:@"user_default_head"]];
    
    //名称
    [self.name setText:gamePlayer.playerName];
    
    //服务器
    [self.gameDetail setText:gamePlayer.serverName];
    
    //段位
    if (_gamePlayer.grade.length == 0) {
        [self.gameRank setText:@"无段位"];
    } else {
        [self.gameRank setText:_gamePlayer.grade];
    }
}

- (void)setDefaultAccount:(BOOL)defaultAccount {
    
    _defaultAccount = defaultAccount;
    [self.defaultLabel setHidden:!_defaultAccount];
}
@end
