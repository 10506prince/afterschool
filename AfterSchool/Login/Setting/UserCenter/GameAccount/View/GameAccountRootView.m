//
//  OrdersRootView.m
//  AfterSchool
//
//  Created by lg on 15/10/22.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "GameAccountRootView.h"
#import "Common.h"

@implementation GameAccountRootView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self addSubview:self.tableView];
        
        [self addSubview:self.navigationBarView];
    }
    return self;
}

- (NavigationBarView *)navigationBarView {
    if (!_navigationBarView) {
        _navigationBarView = [[NavigationBarView alloc] initWithTitle:@"游戏账号"
                                                           titleColor:nil
                                                      backgroundColor:nil
                                              leftButtonBgViewOriginX:0
                                                 leftButtonImageWidth:22
                                                 leftButtonTitleColor:[UIColor whiteColor]
                                                       leftButtonName:@"返回"
                                                  leftButtonImageName:@"navigation_bar_return_button_left_arrow"
                                                      rightButtonName:nil
                                                 rightButtonImageName:nil];
    }
    return _navigationBarView;
}

- (LGTableView *)tableView {
    if (!_tableView) {
        _tableView = [[LGTableView alloc] initWithFrame:self.bounds style:UITableViewStylePlain];
        
        [_tableView setSeparatorInset:UIEdgeInsetsMake(0, 20, 0, 20)];
        [_tableView setTableFooterView:self.tableViewFooterView];
        [_tableView registerNibCellWithClass:[LGGameAccountTableViewCellOne class]];
    }
    return _tableView;
}

- (UIView *)tableViewFooterView {
    if (!_tableViewFooterView) {
        _tableViewFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, 85)];
        
        [_tableViewFooterView addSubview:self.addGameAccountBtn];
    }
    return _tableViewFooterView;
}

- (UIButton *)addGameAccountBtn {
    if (!_addGameAccountBtn) {
        _addGameAccountBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_addGameAccountBtn setFrame:CGRectMake(20, 20, self.tableViewFooterView.bounds.size.width - 40, 45)];
        
        [_addGameAccountBtn setTitle:@"关联游戏账号" forState:UIControlStateNormal];
        [_addGameAccountBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_addGameAccountBtn setBackgroundColor:APPLightColor];
        [_addGameAccountBtn.layer setCornerRadius:3.0];
        //[btn.layer setBorderColor:[UIColor lightGrayColor].CGColor];
        //[btn.layer setBorderWidth:SINGLE_LINE_WIDTH];
    }
    return _addGameAccountBtn;
}

@end
