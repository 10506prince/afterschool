//
//  UserCenterViewController.m
//  AfterSchool
//
//  Created by lg on 15/10/22.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "UserCenterViewController.h"
#import "Common.h"
#import "UIImageView+WebCache.h"
#import "UIImage+Scale.h"
#import "TDSingleton.h"
#import "NSDate+StringFormatter.h"
#import "LGDefineNetServer.h"
#import "SVProgressHUD.h"
#import "NotificationName.h"
#import "ImageSize.h"

#import "GameAccountViewController.h"
#import "TDNetworking.h"
#import "TDDateComponents.h"

#import "AppDelegate.h"

#import "LGHeartBeatService.h"

#import "TalkingData.h"

@interface UserCenterViewController () <UITableViewDataSource,UITableViewDelegate,UIActionSheetDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate, TDInputTextViewDelegate, TDDatePickerViewDelegate> {
    
    NSArray <NSArray<NSString *> *> * _dataSource;
}

@property (nonatomic, strong) NSArray <NSArray<NSString *> *> * dataSource;
@end

@implementation UserCenterViewController

- (void)dealloc
{
    NSLogSelfMethodName;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [TalkingData trackPageBegin:selfControllerName];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [TalkingData trackPageEnd:selfControllerName];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initData];
    [self initUserInterface];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (_needReloadData) {
        _needReloadData = NO;
        [self.rootView.tableView reloadData];
    }
}

- (void)initData {
    self.dataSource = @[@[
                        @"头像",
                        ],
                    @[
                        @"名字",
                        @"性别",
                        @"生日",
                        ],
                    @[
                        @"信用等级",
                        @"个性签名"
                        ],
                    ];
}
- (void)initUserInterface {
    [self.view addSubview:self.rootView];
}

- (UserCenterRootView *)rootView {
    if (!_rootView) {
        _rootView = [[UserCenterRootView alloc] initWithFrame:self.view.bounds];
        
        ///Action 按键响应
        [_rootView.navigationBarView.leftButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        [_rootView.exitButton addTarget:self action:@selector(exitButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        
        ///Delegate 设置代理
        [_rootView.tableView setDelegate:self];
        [_rootView.tableView setDataSource:self];
        
    }
    
    return _rootView;
}

#pragma mark - Action
- (void)backAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)exitButtonClicked {
    NSLogSelfMethodName;
    [self.navigationController popViewControllerAnimated:NO];
    
    UserInfoModel * userInfoModel = [TDSingleton instance].userInfoModel;
    if (userInfoModel.account.length != 0) {
        [LGDefineNetServer setOnlineState:NO account:userInfoModel.account success:^{
        } failure:^{
        }];
    }
    
    [[LGHeartBeatService sharedManager] stop];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate logout];
}

- (IBAction)SelectHeadImageAction:(id)sender {
    UIActionSheet* actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:nil
                                  delegate:self
                                  cancelButtonTitle:@"取消"
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:@"照相机", @"图库", nil];
    
    [actionSheet showInView:self.view];
}
#pragma mark - UITableViewDataSource 列表数据代理
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.dataSource.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    NSArray<NSString *> * rows = [self.dataSource objectAtIndex:section];
    
    return rows.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSUInteger index = [self getIndexFromIndexPath:indexPath];
    NSString * name = [_dataSource objectAtIndex:indexPath.section][indexPath.row];
    
    if (index == 0) {//头像
        LGUserCenterTableViewCellOne * cell = [tableView dequeueReusableCellWithClass:[LGUserCenterTableViewCellOne class]];
        [cell.name setText:name];

        NSString * headImg = [TDSingleton instance].userInfoModel.portraitURL;
        NSURL * headUrl = [NSURL URLWithString:headImg];
        
        //NSParameterAssert(headUrl);
        
        [cell.img sd_setImageWithURL:headUrl placeholderImage:[UIImage imageNamed:@"user_default_head"]];
        
        return cell;
    } else if(index == 1) {//名字
        LGUserCenterTableViewCellTwo * cell = [tableView dequeueReusableCellWithClass:[LGUserCenterTableViewCellTwo class]];
        [cell.name setText:name];
        [cell setArrowHide:NO];
        
        NSString * userName = [TDSingleton instance].userInfoModel.nickName;
        [cell.detail setText:userName];
        
        return cell;
    } else if(index == 2) {//性别
        LGUserCenterTableViewCellTwo * cell = [tableView dequeueReusableCellWithClass:[LGUserCenterTableViewCellTwo class]];
        [cell.name setText:name];
        [cell setArrowHide:YES];
        
        NSUInteger sex = [[TDSingleton instance].userInfoModel.sex integerValue];
        
        [cell.detail setText:(sex == 0 ? @"女" : @"男")];
        
        return cell;
    } else if(index == 4) {//信用等级
        LGUserCenterTableViewCellThree * cell = [tableView dequeueReusableCellWithClass:[LGUserCenterTableViewCellThree class]];
        [cell.name setText:name];

        NSUInteger creditLev = [TDSingleton instance].userInfoModel.creditLev;
        
        if (creditLev == 0) {
            [cell.img setImage:[UIImage imageNamed:@"creditRating0"]];
        } else {
            [cell.img setImage:[UIImage imageNamed:[NSString stringWithFormat:@"creditRating%ld", (unsigned long)creditLev - 1]]];
        }
        
        return cell;
    } else if (index == 3) {//生日
        LGUserCenterTableViewCellTwo * cell = [tableView dequeueReusableCellWithClass:[LGUserCenterTableViewCellTwo class]];
        [cell.name setText:name];
        [cell setArrowHide:NO];
        [cell.detail setText:[TDSingleton instance].userInfoModel.birthdayDateFormat];
        
        return cell;
    } else if (index == 5) {//个性签名
        LGUserCenterTableViewCellTwo * cell = [tableView dequeueReusableCellWithClass:[LGUserCenterTableViewCellTwo class]];
        [cell.name setText:name];
        [cell setArrowHide:NO];
        
        if ([TDSingleton instance].userInfoModel.info.length == 0) {
            [cell.detail setText:@"未设置"];
        } else {
            [cell.detail setText:[TDSingleton instance].userInfoModel.info];
        }
        
        return cell;
    } else if (index == 6) {//游戏账号
        LGUserCenterTableViewCellTwo * cell = [tableView dequeueReusableCellWithClass:[LGUserCenterTableViewCellTwo class]];
        [cell setArrowHide:NO];
        [cell.name setText:name];
        [cell.detail setText:@""];
        
        return cell;
    } else {
        LGUserCenterTableViewCellTwo * cell = [tableView dequeueReusableCellWithClass:[LGUserCenterTableViewCellTwo class]];
        [cell.name setText:name];
        [cell.detail setText:[NSString stringWithFormat:@"%d", (int)index]];
        
        
        return cell;
    }
    return nil;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsMake(0, 20, 0, 20)];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsMake(0, 20, 0, 20)];
    }
}
#pragma mark - UITableViewDelegate 列表代理
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSUInteger index = [self getIndexFromIndexPath:indexPath];
    
    switch (index) {
        case 0:
            [self SelectHeadImageAction:nil];
            break;
        
        case 1://名字
        {
            [self.view addSubview:_rootView.changeNameInputTextView];
            _rootView.changeNameInputTextView.inputTextField.text = [TDSingleton instance].userInfoModel.nickName;
            [_rootView.changeNameInputTextView.inputTextField becomeFirstResponder];
            _rootView.changeNameInputTextView.delegate = self;
            [_rootView.changeNameInputTextView moveToTargetPositon];
        }
            break;
        
        case 2://性别
        {
            
        }
            break;
        
        case 3://生日
        {
            [self.view addSubview:_rootView.datePickerView];
            [_rootView.datePickerView initDataWithDateComponents:[TDDateComponents dateComponentsFromString:[TDSingleton instance].userInfoModel.birthdayDateFormat]];
            [_rootView.datePickerView refreshDate];
            _rootView.datePickerView.delegate = self;
            [_rootView.datePickerView moveToTargetPositon];
            NSLog(@"birthday ：%@", [TDSingleton instance].userInfoModel.birthdayDateFormat);
        }
            break;
            
        case 4:
            
            break;
            
        case 5://个性签名
        {
            [self.view addSubview:_rootView.changeSignatureInputTextView];
            _rootView.changeSignatureInputTextView.inputTextField.text = [TDSingleton instance].userInfoModel.info;
            
            [_rootView.changeSignatureInputTextView.inputTextField becomeFirstResponder];
            _rootView.changeSignatureInputTextView.delegate = self;
            [_rootView.changeSignatureInputTextView moveToTargetPositon];
        }
            break;
            
        case 6:
            [self toGameAccount];
            break;
            
        default:
            break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSUInteger index = [self getIndexFromIndexPath:indexPath];
    switch (index) {
        case 0:
            return LGUserCenterTableViewCellOneHeight;
            break;
        case 1:
        case 2:
        case 3:
        case 5:
        case 6:
            return LGUserCenterTableViewCellTwoHeight;
            break;
        case 4:
            return LGUserCenterTableViewCellThreeHeight;
            
        default:
            return 0;
    }
}

- (NSUInteger)getIndexFromIndexPath:(NSIndexPath *)indexPath {
    NSUInteger index = 0;
    for (int i = 0; i < indexPath.section; i++) {
        NSUInteger row = [self tableView:self.rootView.tableView numberOfRowsInSection:i];
        index += row;
    }
    index += indexPath.row;
    
    return index;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return section > 0 ? 10 : 0;
}

//- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
//    CGFloat height = section == _dataSource.count - 1 ? 1.0 : 1.0;
//    return height;
//}

#pragma mark - UIActionSheetDelegate
//选择图片来源
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSLog(@"%ld", (long)buttonIndex);
    switch (buttonIndex) {
        case 0:
        {
            //状态栏字体颜色改为黑色
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
            
            UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
            imagePicker.delegate = self;
            imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;//照相机
            imagePicker.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
            imagePicker.allowsEditing = YES;
            
            [self presentViewController:imagePicker animated:YES completion:nil];
        }
            break;
            
        case 1:
        {
            //状态栏字体颜色改为黑色
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
            
            UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
            imagePicker.delegate = self;
            imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;//图库
            imagePicker.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
            imagePicker.allowsEditing = YES;
            
            [self presentViewController:imagePicker animated:YES completion:nil];
        }
            break;
    }
}


#pragma mark - UIImagePickerControllerDelegate
//完成选取
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    NSLog(@"Info:%@", info);
    
    //状态栏改为白色
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    if ([info[@"UIImagePickerControllerMediaType"] isEqualToString:@"public.image"]) {
        
        UIImage * image = [info objectForKey:@"UIImagePickerControllerEditedImage"];
        
        UIImage * headImage = [UIImage imageScaleWithOriginImage:image toSize:USER_ICON_SIZE];
        
        [SVProgressHUD showWithStatus:@"正在修改头像..." maskType:SVProgressHUDMaskTypeBlack];
        
        __weak typeof(self) weakSelf = self;
        [LGDefineNetServer updateUserIcon:headImage success:^(NSString *iconUrl) {
            [weakSelf.rootView.tableView reloadData];
            [SVProgressHUD dismiss];
            
            /**
             *  更新本地头像信息
             */
            [self updateLocalPortraitURL:iconUrl];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:LGSetUserIcon object:iconUrl];
            
        } failure:^(NSString *msg) {
            [SVProgressHUD dismissWithError:msg];
        } progress:^(NSString *progressMsg) {
            [SVProgressHUD showWithStatus:progressMsg maskType:SVProgressHUDMaskTypeBlack];
        }];
        
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)updateLocalPortraitURL:(NSString *)URL {
    [TDSingleton instance].userInfoModel.portraitURL = URL;
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSDictionary *userInfoDic = [userDefault dictionaryForKey:@"UserInfo"];
    
    /**
     *  全部用户信息
     */
    NSMutableDictionary * userInfoAllDic = [[NSMutableDictionary alloc] initWithDictionary:userInfoDic];
    
    
    /**
     *  获取当前账号对应的字典
     */
    NSDictionary *dic = userInfoAllDic[[TDSingleton instance].account];
    NSMutableDictionary *currentUserDic = [[NSMutableDictionary alloc] initWithDictionary:dic];
    [currentUserDic setObject:[TDSingleton instance].userInfoModel.portraitURL forKey:@"PortraitURL"];
    
    [userInfoAllDic setObject:currentUserDic forKey:[TDSingleton instance].account];
    
    [userDefault setObject:userInfoAllDic forKey:@"UserInfo"];
}

// 取消选取
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    //状态栏改为白色
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    [picker dismissViewControllerAnimated:YES completion:^{
        NSLog(@"did cancel imagePickerController");
    }];
}

#pragma mark - 页面转换
- (void)toGameAccount {
    GameAccountViewController * nextPage = [[GameAccountViewController alloc] init];
    [self.navigationController pushViewController:nextPage animated:YES];
}

- (void)inputTextViewCancelButtonClicked:(UIButton *)sender {
    switch (sender.tag) {
        case 100:
            [_rootView.changeNameInputTextView.inputTextField resignFirstResponder];
            break;
            
        case 101:
            [_rootView.changeSignatureInputTextView.inputTextField resignFirstResponder];
        default:
            break;
    }
}

- (void)inputTextViewConfirmButtonClickedWithData:(NSDictionary *)data {

    NSString *result = data[@"Result"];
    UIButton *sender = data[@"Button"];
    
    switch (sender.tag) {
        case 102:
            [_rootView.changeNameInputTextView.inputTextField resignFirstResponder];
            [self submitToServerWithName:result];
            break;
            
        case 103:
            [_rootView.changeSignatureInputTextView.inputTextField resignFirstResponder];
            [self submitToServerWithSignature:result];
            break;
            
        default:
            break;
    }
}

- (void)submitToServerWithName:(NSString *)name {
    [SVProgressHUD showWithStatus:@"拼命上传数据中..." maskType:2];
    NSDictionary *parameters = @{@"action":@"updateNickName",
                                 @"sessionKey":[TDSingleton instance].sessionKey,
                                 @"nickName":name};
    
    [TDNetworking requestWithURL:[TDSingleton instance].gatewayServerBusinessURL
                      parameters:parameters
                         success:^(id responseObject)
    {
        NSLog(@"responseObject:%@", responseObject);
        if ([responseObject[@"result"] integerValue] == 1) {
            [SVProgressHUD dismissWithSuccess:@"上传成功"];
            [self updateLocalName:name];
            [self refreshName:name];
        } else {
            [SVProgressHUD dismissWithError:[NSString stringWithFormat:@"%@", responseObject[@"msg"]]];
        }
        
    } failure:^(NSError *error) {
        NSLog(@"error:%@", error);
        [SVProgressHUD dismissWithError:[NSString stringWithFormat:@"%@", error]];
    }];
}

- (void)updateLocalName:(NSString *)name {
    [TDSingleton instance].userInfoModel.nickName = name;
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSDictionary *userInfoDic = [userDefault dictionaryForKey:@"UserInfo"];
    
    /**
     *  全部用户信息
     */
    NSMutableDictionary * userInfoAllDic = [[NSMutableDictionary alloc] initWithDictionary:userInfoDic];
    
    
    /**
     *  获取当前账号对应的字典
     */
    NSDictionary *dic = userInfoAllDic[[TDSingleton instance].account];
    NSMutableDictionary *currentUserDic = [[NSMutableDictionary alloc] initWithDictionary:dic];
        [currentUserDic setObject:[TDSingleton instance].userInfoModel.nickName forKey:@"Name"];
    
    [userInfoAllDic setObject:currentUserDic forKey:[TDSingleton instance].account];
    
    [userDefault setObject:userInfoAllDic forKey:@"UserInfo"];
}

- (void)refreshName:(NSString *)name {
    [TDSingleton instance].userInfoModel.nickName = name;
    [self.rootView.tableView reloadData];
}

- (void)submitToServerWithSignature:(NSString *)signature {
    [SVProgressHUD showWithStatus:@"拼命上传数据中..." maskType:2];
    NSDictionary *parameters = @{@"action":@"updateSignature",
                                 @"sessionKey":[TDSingleton instance].sessionKey,
                                 @"info":signature};
    
    [TDNetworking requestWithURL:[TDSingleton instance].gatewayServerBusinessURL
                      parameters:parameters
                         success:^(id responseObject)
     {
         NSLog(@"submitToServerWithSignature responseObject:%@", responseObject);
         if ([responseObject[@"result"] integerValue] == 1) {
             [SVProgressHUD dismissWithSuccess:@"上传成功"];
             [self refreshSignature:signature];
         } else {
             [SVProgressHUD dismissWithError:[NSString stringWithFormat:@"%@", responseObject[@"msg"]]];
         }
         
     } failure:^(NSError *error) {
         NSLog(@"error:%@", error);
         [SVProgressHUD dismissWithError:[NSString stringWithFormat:@"%@", error]];
     }];
}

- (void)refreshSignature:(NSString *)signature {
    [TDSingleton instance].userInfoModel.info = signature;
    [self.rootView.tableView reloadData];
}

- (void)datePickerViewConfirmButtonClicked:(NSString *)date {
    NSLog(@"datePickerViewConfirmButtonClicked date:%@", date);
    
    [self submitToServerWithBirthday:date];
}

- (void)submitToServerWithBirthday:(NSString *)birthday {
    [SVProgressHUD showWithStatus:@"拼命上传数据中..." maskType:2];
    NSDictionary *parameters = @{@"action":@"updateBirth",
                                 @"sessionKey":[TDSingleton instance].sessionKey,
                                 @"birth":birthday};
    
    [TDNetworking requestWithURL:[TDSingleton instance].gatewayServerBusinessURL
                      parameters:parameters
                         success:^(id responseObject)
     {
         NSLog(@"submitToServerWithSignature responseObject:%@", responseObject);
         if ([responseObject[@"result"] integerValue] == 1) {
             [SVProgressHUD dismissWithSuccess:@"上传成功"];
             [self refreshBirthday:birthday];
         } else {
             [SVProgressHUD dismissWithError:[NSString stringWithFormat:@"%@", responseObject[@"msg"]]];
         }
         
     } failure:^(NSError *error) {
         NSLog(@"error:%@", error);
         [SVProgressHUD dismissWithError:[NSString stringWithFormat:@"%@", error]];
     }];
}

- (void)refreshBirthday:(NSString *)birthday {
    NSString *formatDateString = [birthday stringByReplacingOccurrencesOfString:@"-" withString:@"/"];
    [TDSingleton instance].userInfoModel.birthdayDateFormat = formatDateString;
    [self.rootView.tableView reloadData];
}

@end
