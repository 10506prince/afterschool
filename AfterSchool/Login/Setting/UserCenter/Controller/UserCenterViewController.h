//
//  UserCenterViewController.h
//  AfterSchool
//
//  Created by lg on 15/10/22.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGViewController.h"
#import "UserCenterRootView.h"

@interface UserCenterViewController : LGViewController

@property (nonatomic, strong) UserCenterRootView * rootView;

@property (nonatomic, assign) BOOL needReloadData;

@end
