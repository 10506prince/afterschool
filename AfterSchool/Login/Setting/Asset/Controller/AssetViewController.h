//
//  AssetViewController.h
//  AfterSchool
//
//  Created by Teson Draw on 12/30/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AssetRootView.h"

@interface AssetViewController : UIViewController

@property (nonatomic, strong) AssetRootView *rootView;

@property (nonatomic, assign) CGFloat moneyAmount;
@property (nonatomic, assign) NSInteger gameCoinAmount;

@end
