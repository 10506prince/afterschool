//
//  AssetViewController.m
//  AfterSchool
//
//  Created by Teson Draw on 12/30/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "AssetViewController.h"
#import "MacrosDefinition.h"
#import "LGDefineNetServer.h"
#import "SVProgressHUD.h"

#import "WithdrawCashViewController.h"
#import "RechargeViewController.h"

#import "NotificationName.h"

#import "Common.h"
#import "TalkingData.h"

@interface AssetViewController ()

@end

@implementation AssetViewController

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [TalkingData trackPageBegin:selfControllerName];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [TalkingData trackPageEnd:selfControllerName];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initUserInterface];
    [self getAsset];
}

- (void)initUserInterface {
    [self.view addSubview:self.rootView];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getAsset) name:TDWithdrawCashSuccess object:nil];
}

- (AssetRootView *)rootView {
    if (!_rootView) {
        _rootView = [[AssetRootView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
       
        [_rootView.navigationBarView.leftButton addTarget:self action:@selector(returnButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        [_rootView.withdrawCashButton addTarget:self action:@selector(withdrawCashButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        [_rootView.chargeButton addTarget:self action:@selector(chargeButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    }
    return _rootView;
}

- (void)returnButtonClicked {
    [self.navigationController popViewControllerAnimated:YES];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)getAsset {
//    [SVProgressHUD showWithStatus:@"拼命加载数据..." maskType:SVProgressHUDMaskTypeClear];
    [LGDefineNetServer getWalletInfoSuccess:^(id result) {
//        [SVProgressHUD dismiss];
        NSLog(@"getMoney info:%@", result);
        
        _moneyAmount = [result[@"money"] floatValue];
        _rootView.moneyAmountLabel.text = [NSString stringWithFormat:@"%.2f", _moneyAmount];
        
        _gameCoinAmount = [result[@"tokenMoney"] integerValue];
        _rootView.gameCoinAmountLabel.text = [NSString stringWithFormat:@"%ld", (long)_gameCoinAmount];
    } failure:^(NSString *msg) {
        [SVProgressHUD showErrorWithStatus:msg];
    }];
}

- (void)withdrawCashButtonClicked {
    WithdrawCashViewController *vc = [[WithdrawCashViewController alloc] init];
    vc.amount = _moneyAmount;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)chargeButtonClicked {
    __weak typeof(self) weakSelf = self;
    
    RechargeViewController * vc = [RechargeViewController rechargeViewControllerWithFinishBlock:^(BOOL success) {
        if (success) {
            [weakSelf chargeSuccess];
            [weakSelf.navigationController popViewControllerAnimated:YES];
        }
    }];
    
    
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)chargeSuccess {
    [LGDefineNetServer getWalletInfoSuccess:^(id result) {
        NSLog(@"getMoney info:%@", result);
        _rootView.moneyAmountLabel.text = [NSString stringWithFormat:@"%.2f", [result[@"money"] floatValue]];
    } failure:^(NSString *msg) {
        [SVProgressHUD dismissWithError:msg];
    }];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
