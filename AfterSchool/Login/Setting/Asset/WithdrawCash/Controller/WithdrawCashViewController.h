//
//  WithdrawCashViewController.h
//  AfterSchool
//
//  Created by Teson Draw on 12/18/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WithdrawCashRootView.h"

@interface WithdrawCashViewController : UIViewController 

@property (nonatomic, strong) WithdrawCashRootView *rootView;
@property (nonatomic, assign) CGFloat amount;
@property (nonatomic, assign) CGFloat withdrawAmount;

@property (nonatomic, strong) NSString *password;
@property (nonatomic, strong) NSString *money;
@property (nonatomic, strong) NSString *code;

@end
