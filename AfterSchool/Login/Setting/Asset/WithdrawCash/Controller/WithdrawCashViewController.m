//
//  WithdrawCashViewController.m
//  AfterSchool
//
//  Created by Teson Draw on 12/18/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "WithdrawCashViewController.h"
#import "MacrosDefinition.h"
#import "NSString+PhoneNumber.h"
#import "SVProgressHUD.h"
#import "TDSingleton.h"
#import "TDNetworking.h"

#import "WXApiManager.h"

#import "NotificationName.h"

#import "Common.h"
#import "TalkingData.h"

#import "TDMixedCryptogram.h"

@interface WithdrawCashViewController () <TDInputTextViewDelegate, WXApiManagerDelegate>

@property (nonatomic) BOOL monday;

@end

@implementation WithdrawCashViewController
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [TalkingData trackPageBegin:selfControllerName];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [TalkingData trackPageEnd:selfControllerName];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    NSDate * today = [NSDate date];
    NSCalendar *currentCalendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [currentCalendar componentsInTimeZone:[NSTimeZone systemTimeZone] fromDate:today];
    self.monday = components.weekday == 2;
    
    [self initUserInterface];
}

- (void)initUserInterface {
    [self.view addSubview:self.rootView];
    
    [WXApiManager sharedManager].delegate = self;
    
    if (_amount > 0) {
        _rootView.amountLabel.text = [NSString stringWithFormat:@"%.2f元", _amount];
    } else  {
        _rootView.amountLabel.text = @"0.00元";
    }
    
    if (self.monday) {
        [_rootView.amountTextField becomeFirstResponder];
    }
}

- (WithdrawCashRootView *)rootView {
    if (!_rootView) {
        _rootView = [[WithdrawCashRootView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
        [_rootView.navigationBarView.leftButton addTarget:self action:@selector(returnButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        
        [_rootView.weixinOptionView.optionButton addTarget:self action:@selector(withdrawCashButtonClicked:) forControlEvents:UIControlEventTouchUpInside];

    }
    return _rootView;
}

- (void)returnButtonClicked {
    [self.navigationController popViewControllerAnimated:YES];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [_rootView.amountTextField resignFirstResponder];
}

- (void)withdrawCashButtonClicked:(UIButton *)sender {
    /**
     *  关闭键盘
     */
    [_rootView.amountTextField resignFirstResponder];
    
    /**
     *  判断是否是数字
     */
    NSString *amountText = [_rootView.amountTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if (![amountText isNumber] && ![amountText isFloat]) {
        [SVProgressHUD showErrorWithStatus:@"请输入数字" duration:2];
        return;
    }
    
    _withdrawAmount = [_rootView.amountTextField.text floatValue];
    
    if (_withdrawAmount < 1.01) {
        [SVProgressHUD showErrorWithStatus:@"提现金额必须大于1元" duration:2];
//        [self performSelector:@selector(inputPassword) withObject:nil afterDelay:0.3];
        return;
    }
    
    [self weixinAuthoriztion];
//    [self inputPassword];
}

- (void)inputPassword {
    [self.view addSubview:_rootView.inputPasswordView];
//    _rootView.inputPasswordView.inputTextField.text = @"";
    [_rootView.inputPasswordView.inputTextField becomeFirstResponder];
    _rootView.inputPasswordView.delegate = self;
    [_rootView.inputPasswordView moveToTargetPositon];
}

- (void)withdrawCashWithAmount:(CGFloat)amount {
    
    NSInteger amountInteger = (amount * 100);
    NSString *amountString = [NSString stringWithFormat:@"%ld", (long)amountInteger];
    NSString *description = [NSString stringWithFormat:@"提现金额：%.2f元", amount];
    
    [SVProgressHUD showErrorWithStatus:@"数据拼命处理中......"];
    
    NSString *mixedCryptogram = [TDMixedCryptogram fromPassword:_password];
    
    NSDictionary *parameters = @{@"action":@"transfer",
                                 @"sessionKey":[TDSingleton instance].sessionKey,
                                 @"amount":amountString,
                                 @"recipient":_code,
                                 @"description":description,
                                 @"channel":@"wx",
                                 @"pwd":mixedCryptogram};
    
    NSLog(@"withdrawCashWithAmount parameters:%@", parameters);
    
    [TDNetworking requestWithURL:[TDSingleton instance].gatewayServerBusinessURL
                      parameters:parameters
                         success:^(id responseObject)
     {
         NSLog(@"withdrawCash:%@", responseObject);
         if ([responseObject[@"result"] integerValue] == 1) {
             [[NSNotificationCenter defaultCenter] postNotificationName:TDWithdrawCashSuccess object:nil];
             [SVProgressHUD dismissWithSuccess:@"提现成功" afterDelay:3];
             
             [self.navigationController popViewControllerAnimated:YES];
            
         } else {
             
             [SVProgressHUD dismissWithError:[NSString stringWithFormat:@"%@", responseObject[@"msg"]] afterDelay:2];
             
             if (([responseObject[@"result"] integerValue] == 13)) {
                 [self performSelector:@selector(inputPassword) withObject:nil afterDelay:2];
             }
         }
     } failure:^(NSError *error) {
         NSLog(@"error:%@", error);
     }];
}

#pragma mark 输入密码回调

- (void)inputTextViewCancelButtonClicked:(UIButton *)sender {
    [_rootView.inputPasswordView.inputTextField resignFirstResponder];
}

- (void)inputTextViewConfirmButtonClickedWithData:(NSDictionary *)data {
    
    NSString *result = data[@"Result"];
//    UIButton *sender = data[@"Button"];
    
    [_rootView.inputPasswordView.inputTextField resignFirstResponder];

    if (result.length < 6) {
//        [SVProgressHUD showErrorWithStatus:@"密码不能低于6位" ];
        [SVProgressHUD showErrorWithStatus:@"密码不能低于6位" duration:3];
//        [self inputPassword];
        [self performSelector:@selector(inputPassword) withObject:nil afterDelay:0.3];

        return;
    }
    
    _password = result;
    
    [self withdrawCashWithAmount:_withdrawAmount];
//    [self verifyPassword:result];
}

//- (void)verifyPassword:(NSString *)password {
//    [SVProgressHUD showWithStatus:@"数据拼命处理中......"];
//    NSDictionary *parameters = @{@"userName":[TDSingleton instance].account,
//                                 @"userPwd":password};
//    
//    NSLog(@"LoginVC submitMobileNumber parameter:%@", parameters);
//    
//    [TDNetworking requestWithURL:[TDSingleton instance].URL.loginServerLoginURL
//                      parameters:parameters
//                         success:^(id responseObject)
//     {
//         NSLog(@"submitMobileNumber result:%@", responseObject);
//         if ([responseObject[@"result"] integerValue] == 1) {
//             
//             _password = password;
//             
//             [SVProgressHUD dismiss];
//             
//             [self withdrawCashWithAmount:_withdrawAmount];
////             [self weixinAuthoriztion];
//
//         } else {
//             NSString *msg = [NSString stringWithFormat:@"%@", responseObject[@"msg"]];
//             [SVProgressHUD showErrorWithStatus:msg];
//         }
//     } failure:^(NSError *error) {
//         NSString *message = [NSString stringWithFormat:@"%@", error.description];
//         [SVProgressHUD showErrorWithStatus:message];
//     }];
//}

- (void)weixinAuthoriztion {
    NSString *kAuthScope = @"snsapi_userinfo";
    NSString *kAuthState = @"withdrawCash";
    NSString *kAuthOpenID = @"";
    
    SendAuthReq* req = [[SendAuthReq alloc] init];
    req.scope = kAuthScope;
    req.state = kAuthState;
    req.openID = kAuthOpenID;
    
    __weak typeof(self) weakSelf = self;
    
    [WXApi sendAuthReq:req viewController:weakSelf delegate:[WXApiManager sharedManager]];
}

- (void)managerDidRecvAuthResponse:(SendAuthResp *)response {
    if (response.errCode == 0) {

        _code = response.code;
        
        [self inputPassword];
//        [self withdrawCashWithAmount:_withdrawAmount];
    } else {
        [SVProgressHUD showErrorWithStatus:response.errStr];
    }
}

@end
