//
//  WithdrawCashRootView.h
//  AfterSchool
//
//  Created by Teson Draw on 12/18/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavigationBarView.h"
#import "TDTextField.h"
#import "OptionView.h"

#import "TDInputTextView.h"

@interface WithdrawCashRootView : UIView

@property (nonatomic, strong) NavigationBarView *navigationBarView;

@property (nonatomic, strong) UILabel *amountTitleLabel;
@property (nonatomic, strong) UILabel *amountLabel;

//@property (nonatomic, strong) UILabel *itemNameLabel;
//@property (nonatomic, strong) UILabel *unitLabel;

@property (nonatomic, strong) TDTextField *amountTextField;

@property (nonatomic, strong) OptionView *weixinOptionView;

@property (nonatomic, strong) TDInputTextView *inputPasswordView;

@property (nonatomic, strong) UILabel * notWithdraw;

//@property (nonatomic, strong) UIButton *alipayButton;
//@property (nonatomic, strong) UIButton *weixinButton;

//- (TDInputTextView *)changeNameInputTextView {
//    if (!_changeNameInputTextView) {
//        _changeNameInputTextView = [[TDInputTextView alloc] initWithFrame:CGRectMake((SCREEN_WIDTH - 300) / 2, -144, 300, 144) targetPosition:CGPointMake((SCREEN_WIDTH - 300) / 2, 64)];
//        _changeNameInputTextView.titleLabel.text = @"编辑名字";
//        _changeNameInputTextView.cancelButton.tag = 100;
//        _changeNameInputTextView.confirmButton.tag = 102;
//    }
//    return _changeNameInputTextView;
//}


@end
