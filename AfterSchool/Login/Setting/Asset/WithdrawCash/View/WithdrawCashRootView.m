//
//  WithdrawCashRootView.m
//  AfterSchool
//
//  Created by Teson Draw on 12/18/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "WithdrawCashRootView.h"
#import "UIButton+SetBackgroundColor.h"
#import "Common.h"

@interface WithdrawCashRootView ()

@property (nonatomic) BOOL monday;

@end

@implementation WithdrawCashRootView


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithRed:230/255.f green:230/255.f blue:230/255.f alpha:1];
        
        NSDate * today = [NSDate date];
        NSCalendar *currentCalendar = [NSCalendar currentCalendar];
        NSDateComponents *components = [currentCalendar componentsInTimeZone:[NSTimeZone systemTimeZone] fromDate:today];
        self.monday = components.weekday == 2;
        
        
        [self addSubview:self.amountTitleLabel];
        [self addSubview:self.amountLabel];
        
        [self addSubview:self.amountTextField];
        [self addSubview:self.weixinOptionView];
        
        
        if (self.monday) {
        } else {
            [self.amountTextField setHidden:YES];
            [self.weixinOptionView setHidden:YES];
            [self addSubview:self.notWithdraw];
        }
        
        [self addSubview:self.navigationBarView];
    }
    return self;
}

- (NavigationBarView *)navigationBarView {
    if (!_navigationBarView) {

        _navigationBarView = [[NavigationBarView alloc] initWithTitle:@"提现"
                                                           titleColor:nil
                                                      backgroundColor:nil
                                              leftButtonBgViewOriginX:0
                                                 leftButtonImageWidth:22
                                                 leftButtonTitleColor:[UIColor whiteColor]
                                                       leftButtonName:@"资产"
                                                  leftButtonImageName:@"navigation_bar_return_button_left_arrow"
                                                      rightButtonName:nil
                                                 rightButtonImageName:nil];
    }
    return _navigationBarView;
}



- (UILabel *)amountTitleLabel {
    if (!_amountTitleLabel) {
        _amountTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 64 + 20, 84, 14)];
        _amountTitleLabel.textColor = [UIColor colorWithRed:56/255.f green:56/255.f blue:56/255.f alpha:1];
        _amountTitleLabel.font = [UIFont systemFontOfSize:14];
        _amountTitleLabel.textAlignment = NSTextAlignmentLeft;
        _amountTitleLabel.text = @"可提现余额：";
    }
    return _amountTitleLabel;
}

- (UILabel *)amountLabel {
    if (!_amountLabel) {
        _amountLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_amountTitleLabel.frame), _amountTitleLabel.frame.origin.y, 180, 14)];
        _amountLabel.textColor = APPOrangeLightColor;
        _amountLabel.font = [UIFont systemFontOfSize:14];
        _amountLabel.textAlignment = NSTextAlignmentLeft;
        _amountLabel.text = @"0元";
    }
    return _amountLabel;
}

//- (UILabel *)itemNameLabel {
//    if (!_itemNameLabel) {
//        _itemNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 80, 60, 44)];
//        _itemNameLabel.textColor = [UIColor blackColor];
//        _itemNameLabel.font = [UIFont systemFontOfSize:16];
//        _itemNameLabel.textAlignment = NSTextAlignmentLeft;
//        _itemNameLabel.text = @"金额：";
//    }
//    return _itemNameLabel;
//}

- (TDTextField *)amountTextField {
    if (!_amountTextField) {
        _amountTextField = [[TDTextField alloc] initWithFrame:CGRectMake(20, CGRectGetMaxY(_amountTitleLabel.frame) + 20, self.bounds.size.width - 40, 44)];
        _amountTextField.keyboardType = UIKeyboardTypeNumberPad;
        _amountTextField.placeholder = @"填写金额";
        _amountTextField.backgroundColor = [UIColor whiteColor];
        _amountTextField.layer.cornerRadius = 5;
    }
    return _amountTextField;
}

//- (UILabel *)unitLabel {
//    if (!_unitLabel) {
//        _unitLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_amountTextField.frame) + 4, _amountTextField.frame.origin.y, 20, 44)];
//        _unitLabel.textColor = [UIColor blackColor];
//        _unitLabel.font = [UIFont systemFontOfSize:16];
//        _unitLabel.text = @"元";
//    }
//    return _unitLabel;
//}
//
//- (UIButton *)alipayButton {
//    if (!_alipayButton) {
//        _alipayButton = [[UIButton alloc] initWithFrame:CGRectMake((self.bounds.size.width - 180) / 2, CGRectGetMaxY(_amountTextField.frame) + 20,  180, 44)];
//        [_alipayButton setTitle:@"支付宝" forState:UIControlStateNormal];
//        [_alipayButton setBackgroundColor:[UIColor colorWithRed:37/255.f green:183/255.f blue:237/255.0 alpha:1]];
//        [_alipayButton setBackgroundColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
//        _alipayButton.layer.cornerRadius = 5;
//        _alipayButton.layer.masksToBounds = YES;
//        _alipayButton.titleLabel.font = [UIFont systemFontOfSize:20];
//        _alipayButton.tag = 100;
//    }
//    
//    return _alipayButton;
//}

//- (UIButton *)weixinButton {
//    if (!_weixinButton) {
//        _weixinButton = [[UIButton alloc] initWithFrame:CGRectMake((self.bounds.size.width - 180) / 2, CGRectGetMaxY(_amountTextField.frame) + 20,  180, 44)];
//        [_weixinButton setTitle:@"微信提现" forState:UIControlStateNormal];
//        [_weixinButton setBackgroundColor:[UIColor colorWithRed:37/255.f green:183/255.f blue:237/255.0 alpha:1]];
//        [_weixinButton setBackgroundColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
//        _weixinButton.layer.cornerRadius = 5;
//        _weixinButton.layer.masksToBounds = YES;
//        _weixinButton.titleLabel.font = [UIFont systemFontOfSize:20];
//        _weixinButton.tag = 101;
//    }
//    
//    return _weixinButton;
//}

- (OptionView *)weixinOptionView {
    if (!_weixinOptionView) {
        
        _weixinOptionView = [[OptionView alloc] initWithFrame:CGRectMake(20, CGRectGetMaxY(_amountTextField.frame) + 10, self.bounds.size.width - 40, 44)
                                              backgroundColor:nil
                                                    imageName:@"withdraw_weixin"
                                                 imageOriginX:15
                                                    imageSize:CGSizeMake(30, 30)
                                                         text:@"提现到微信"
                                                  textOriginX:55
                                                    textColor:[UIColor colorWithRed:56/255.f green:56/255.f blue:56/255.f alpha:1]
                                                     fontSize:16
                                               showRightArrow:YES];
        
        _weixinOptionView.layer.cornerRadius = 5;
    }
    return _weixinOptionView;
}


- (TDInputTextView *)inputPasswordView {
    if (!_inputPasswordView) {
        _inputPasswordView = [[TDInputTextView alloc] initWithFrame:CGRectMake((self.bounds.size.width - 300) / 2, -144, 300, 144) targetPosition:CGPointMake((self.bounds.size.width - 300) / 2, 64)];
        _inputPasswordView.titleLabel.text = @"输入登录密码";
        _inputPasswordView.cancelButton.tag = 100;
        _inputPasswordView.confirmButton.tag = 101;
    }
    return _inputPasswordView;
}

- (UILabel *)notWithdraw {
    if (!_notWithdraw) {
        _notWithdraw = [[UILabel alloc] initWithFrame:CGRectMake(20, 64 + 54, self.bounds.size.width - 40, 44)];
        [_notWithdraw setBackgroundColor:[UIColor whiteColor]];
        [_notWithdraw setText:@"每周一提现"];
        [_notWithdraw setTextAlignment:NSTextAlignmentCenter];
        [_notWithdraw setTextColor:LgColor(153, 1)];
    }
    return _notWithdraw;
}
@end
