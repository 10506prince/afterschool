//
//  AssetRootView.h
//  AfterSchool
//
//  Created by Teson Draw on 12/30/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavigationBarView.h"

@interface AssetRootView : UIView

@property (nonatomic, strong) NavigationBarView *navigationBarView;

@property (nonatomic, strong) UIScrollView *scrollView;

@property (nonatomic, strong) UILabel *moneyAnnotationLabel;
@property (nonatomic, strong) UILabel *gameCoinAnnotationLabel;
@property (nonatomic, strong) UILabel *moneyAmountLabel;
@property (nonatomic, strong) UILabel *gameCoinAmountLabel;
@property (nonatomic, strong) UILabel *gameCoinTipLabel;

@property (nonatomic, strong) UIButton *withdrawCashButton;
@property (nonatomic, strong) UIButton *chargeButton;

@property (nonatomic, strong) UIImageView *moneyMarkImageView;
@property (nonatomic, strong) UIImageView *gameCoinMarkImageView;

@property (nonatomic, strong) UIView *moneyBackgroundView;
@property (nonatomic, strong) UIView *gameCoinBackgroundView;

@property (nonatomic, strong) UIView *moneySeparatorLineView;
@property (nonatomic, strong) UIView *gameCoinSeparatorLineView;

@end
