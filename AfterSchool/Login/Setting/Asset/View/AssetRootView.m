//
//  AssetRootView.m
//  AfterSchool
//
//  Created by Teson Draw on 12/30/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "AssetRootView.h"
#import "UIButton+SetBackgroundColor.h"
#import "MacrosDefinition.h"

@implementation AssetRootView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithRed:230/255.f green:230/255.f blue:230/255.f alpha:1];
        
        [self addSubview:self.navigationBarView];
        
        [self addSubview:self.scrollView];
        
        [_scrollView addSubview:self.moneyBackgroundView];
        
        [_moneyBackgroundView addSubview:self.moneyMarkImageView];
        [_moneyBackgroundView addSubview:self.moneyAnnotationLabel];
        [_moneyBackgroundView addSubview:self.moneySeparatorLineView];
        [_moneyBackgroundView addSubview:self.moneyAmountLabel];
        [_moneyBackgroundView addSubview:self.withdrawCashButton];
        [_moneyBackgroundView addSubview:self.chargeButton];

        [_scrollView addSubview:self.gameCoinBackgroundView];
        
        [_gameCoinBackgroundView addSubview:self.gameCoinMarkImageView];
        [_gameCoinBackgroundView addSubview:self.gameCoinAnnotationLabel];
        [_gameCoinBackgroundView addSubview:self.gameCoinSeparatorLineView];
        [_gameCoinBackgroundView addSubview:self.gameCoinAmountLabel];
        [_gameCoinBackgroundView addSubview:self.gameCoinTipLabel];
    }
    
    return self;
}

- (NavigationBarView *)navigationBarView {
    if (!_navigationBarView) {

        _navigationBarView = [[NavigationBarView alloc] initWithTitle:@"资产"
                                                           titleColor:nil
                                                      backgroundColor:nil
                                              leftButtonBgViewOriginX:0
                                                 leftButtonImageWidth:22
                                                 leftButtonTitleColor:[UIColor whiteColor]
                                                       leftButtonName:@"我"
                                                  leftButtonImageName:@"navigation_bar_return_button_left_arrow"
                                                      rightButtonName:nil
                                                 rightButtonImageName:nil];
        
    }
    return _navigationBarView;
}

- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 64, SCREEN_WIDTH, SCREEN_HEIGHT - 64)];
        _scrollView.contentSize = CGSizeMake(SCREEN_WIDTH, 603);
        _scrollView.showsVerticalScrollIndicator = NO;
    }
    return _scrollView;
}

- (UIView *)moneyBackgroundView {
    if (!_moneyBackgroundView) {
        _moneyBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 300)];
        _moneyBackgroundView.backgroundColor = [UIColor whiteColor];
    }
    return _moneyBackgroundView;
}

- (UIImageView *)moneyMarkImageView {
    if (!_moneyMarkImageView) {
        _moneyMarkImageView = [[UIImageView alloc] initWithFrame:CGRectMake(45, 20, 25, 25)];
        _moneyMarkImageView.image = [UIImage imageNamed:@"asset_money_mark"];
    }
    return _moneyMarkImageView;
}

- (UILabel *)moneyAnnotationLabel {
    if (!_moneyAnnotationLabel) {
        _moneyAnnotationLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_moneyMarkImageView.frame) + 15, 25, 80, 16)];
        _moneyAnnotationLabel.textColor = [UIColor colorWithRed:56/255.f green:56/255.f blue:56/255.f alpha:1];
        _moneyAnnotationLabel.font = [UIFont systemFontOfSize:16];
        _moneyAnnotationLabel.text = @"余额（元）";
        
    }
    return _moneyAnnotationLabel;
}

- (UIView *)moneySeparatorLineView {
    if (!_moneySeparatorLineView) {
        _moneySeparatorLineView = [[UIView alloc] initWithFrame:CGRectMake(45, CGRectGetMaxY(_moneyMarkImageView.frame) + 20, self.bounds.size.width - 90, 1)];
        _moneySeparatorLineView.backgroundColor = [UIColor colorWithRed:204/255.f green:204/255.f blue:204/255.f alpha:1];
    }
    return _moneySeparatorLineView;
}

- (UILabel *)moneyAmountLabel {
    if (!_moneyAmountLabel) {
        _moneyAmountLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_moneySeparatorLineView.frame) + 60, self.bounds.size.width, 60)];
        _moneyAmountLabel.textColor = [UIColor colorWithRed:38/255.f green:166/255.f blue:154/255.f alpha:1];
        _moneyAmountLabel.font = [UIFont systemFontOfSize:60];
        _moneyAmountLabel.textAlignment = NSTextAlignmentCenter;
        _moneyAmountLabel.text = @"0.00";
    }
    return _moneyAmountLabel;
}

- (UIButton *)withdrawCashButton {
    if (!_withdrawCashButton) {
        _withdrawCashButton = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_WIDTH / 2 - 65 - 5, CGRectGetMaxY(_moneyAmountLabel.frame) + 20, 65, 44)];
        [_withdrawCashButton setTitle:@"提现" forState:UIControlStateNormal];
        [_withdrawCashButton setBackgroundColor:[UIColor colorWithRed:38/255.f green:166/255.f blue:154/255.0 alpha:1]];
        [_withdrawCashButton setBackgroundColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
        _withdrawCashButton.layer.cornerRadius = 5;
        _withdrawCashButton.layer.masksToBounds = YES;
        _withdrawCashButton.titleLabel.font = [UIFont systemFontOfSize:18];
    }
    
    return _withdrawCashButton;
}

- (UIButton *)chargeButton {
    if (!_chargeButton) {
        _chargeButton = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_withdrawCashButton.frame) + 10, _withdrawCashButton.frame.origin.y, 65, 44)];
        [_chargeButton setTitle:@"充值" forState:UIControlStateNormal];
        [_chargeButton setBackgroundColor:[UIColor colorWithRed:255/255.f green:145/255.f blue:0 alpha:1]];
        [_chargeButton setBackgroundColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
        _chargeButton.layer.cornerRadius = 5;
        _chargeButton.layer.masksToBounds = YES;
        _chargeButton.titleLabel.font = [UIFont systemFontOfSize:18];
    }
    
    return _chargeButton;
}

- (UIView *)gameCoinBackgroundView {
    if (!_gameCoinBackgroundView) {
        _gameCoinBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_moneyBackgroundView.frame) + 3, SCREEN_WIDTH, 300)];
        _gameCoinBackgroundView.backgroundColor = [UIColor whiteColor];
    }
    return _gameCoinBackgroundView;
}

- (UIImageView *)gameCoinMarkImageView {
    if (!_gameCoinMarkImageView) {
        _gameCoinMarkImageView = [[UIImageView alloc] initWithFrame:CGRectMake(45, 20, 25, 25)];
        _gameCoinMarkImageView.image = [UIImage imageNamed:@"asset_game_coin"];
    }
    return _gameCoinMarkImageView;
}

- (UILabel *)gameCoinAnnotationLabel {
    if (!_gameCoinAnnotationLabel) {
        _gameCoinAnnotationLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_gameCoinMarkImageView.frame) + 15, 25, 100, 16)];
        _gameCoinAnnotationLabel.textColor = [UIColor colorWithRed:56/255.f green:56/255.f blue:56/255.f alpha:1];
        _gameCoinAnnotationLabel.font = [UIFont systemFontOfSize:16];
        _gameCoinAnnotationLabel.text = @"游戏币（个）";
        
    }
    return _gameCoinAnnotationLabel;
}

- (UIView *)gameCoinSeparatorLineView {
    if (!_gameCoinSeparatorLineView) {
        _gameCoinSeparatorLineView = [[UIView alloc] initWithFrame:CGRectMake(45, CGRectGetMaxY(_gameCoinMarkImageView.frame) + 20, self.bounds.size.width - 90, 1)];
        _gameCoinSeparatorLineView.backgroundColor = [UIColor colorWithRed:204/255.f green:204/255.f blue:204/255.f alpha:1];
    }
    return _gameCoinSeparatorLineView;
}

- (UILabel *)gameCoinAmountLabel {
    if (!_gameCoinAmountLabel) {
        _gameCoinAmountLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_gameCoinSeparatorLineView.frame) + 60, self.bounds.size.width, 60)];
        _gameCoinAmountLabel.textColor = [UIColor colorWithRed:38/255.f green:166/255.f blue:154/255.f alpha:1];
        _gameCoinAmountLabel.font = [UIFont systemFontOfSize:60];
        _gameCoinAmountLabel.textAlignment = NSTextAlignmentCenter;
        _gameCoinAmountLabel.text = @"0";
    }
    return _gameCoinAmountLabel;
}

- (UILabel *)gameCoinTipLabel {
    if (!_gameCoinTipLabel) {
        _gameCoinTipLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_gameCoinAmountLabel.frame) + 90, SCREEN_WIDTH, 14)];
        _gameCoinTipLabel.textColor = [UIColor colorWithRed:255/255.f green:145/255.f blue:0.f alpha:1];
        _gameCoinTipLabel.font = [UIFont systemFontOfSize:12];
        _gameCoinTipLabel.text = @"注：游戏币不能提现，约单将优先扣除";
        _gameCoinTipLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _gameCoinTipLabel;
}

@end
