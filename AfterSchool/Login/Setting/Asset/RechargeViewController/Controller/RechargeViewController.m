//
//  RechargeViewController.m
//  AfterSchool
//
//  Created by lg on 15/12/17.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "RechargeViewController.h"
#import "Pingpp.h"
#import "TDNetworking.h"
#import "TDSingleton.h"
#import "NSString+PhoneNumber.h"
#import "SVProgressHUD.h"

#import "RechargeRootView.h"

#import "Common.h"
#import "TalkingData.h"

typedef NS_ENUM(NSUInteger, PayType) {
    PayTypeAliPay = 100,
    PayTypeWeiXin = 101,
};

@interface RechargeViewController ()

@property (nonatomic, weak) IBOutlet RechargeRootView * rootView;
@property (nonatomic, weak) IBOutlet UITextField * moneyTextField;
@property (nonatomic, weak) IBOutlet UIButton * alipayBtn;
@property (nonatomic, weak) IBOutlet UIButton * weiXinBtn;

@property (copy) RechargeViewControllerFinishBlock finishBlock;

@end

@implementation RechargeViewController

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [TalkingData trackPageBegin:selfControllerName];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [TalkingData trackPageEnd:selfControllerName];
}

- (instancetype)init
{
    return nil;
}

+ (instancetype)rechargeViewControllerWithFinishBlock:(RechargeViewControllerFinishBlock)finishBlock {
    RechargeViewController * selfVC = [[RechargeViewController alloc] initWithNibName:@"RechargeViewController" bundle:nil];
    selfVC.finishBlock = finishBlock;
    return selfVC;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initData];
    [self initUserInterface];
}

- (void)initData {
    
}

- (void)initUserInterface {
    self.alipayBtn.tag = PayTypeAliPay;
    self.weiXinBtn.tag = PayTypeWeiXin;
    
    [self.rootView.navigationBarView.leftButton addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.moneyTextField becomeFirstResponder];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];
    [self.view endEditing:YES];
}

#pragma mark - Action
- (IBAction)backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)rechargeAction:(UIButton *)sender {
    
    /**
     *  关闭键盘
     */
    [self.moneyTextField resignFirstResponder];
    
    /**
     *  判断是否是数字
     */
    NSString *amountText = [self.moneyTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if (![amountText isNumber] && ![amountText isFloat]) {
        [SVProgressHUD showErrorWithStatus:@"请输入数字" duration:2];
        return;
    }
    
    CGFloat amount = [self.moneyTextField.text floatValue];
    
//    NSString * channel = nil;
    PayType type = sender.tag;
    switch (type) {
        case PayTypeAliPay:
            _channel = @"alipay";
            break;
            
        case PayTypeWeiXin:
            _channel = @"wx";
            break;
    }
    
    NSString *amountString = [NSString stringWithFormat:@"%.0f", amount * 100];
    NSString *body = [NSString stringWithFormat:@"充值%.2f元", amount];
    
    [SVProgressHUD showWithStatus:@"拼命加载中..."];
    
    NSDictionary *parameters = @{@"action":@"getCharge",
                                 @"amount":amountString,
                                 @"subject":@"充值",
                                 @"body":body,
                                 @"channel":_channel,
                                 @"sessionKey":[TDSingleton instance].sessionKey};
    
    NSLog(@"charge parameters:%@", parameters);
    
    
    __weak typeof(self) weakSelf = self;
    
    [TDNetworking requestWithURL:[TDSingleton instance].gatewayServerBusinessURL
                      parameters:parameters
                         success:^(id responseObject)
     {
         RechargeViewControllerFinishBlock block = weakSelf.finishBlock;
         
         NSLog(@"getCharge:%@", responseObject);
         if ([responseObject[@"result"] integerValue] == 1) {
             [SVProgressHUD dismiss];
//             if (block) {
//                 block(YES);
//             }
             NSDictionary *charge = responseObject[@"charge"];
             [weakSelf createPaymentWithCharge:charge];
         } else {
             if (block) {
                 block(NO);
             }
             [SVProgressHUD dismissWithError:[NSString stringWithFormat:@"%@", responseObject[@"msg"]]];
         }
     } failure:^(NSError *error) {
         RechargeViewControllerFinishBlock block = weakSelf.finishBlock;
         if (block) {
             block(NO);
         }
         NSLog(@"didReceiveMessageNotification error:%@", error);
         [SVProgressHUD dismissWithError:[NSString stringWithFormat:@"%@", error.description]];
     }];
}

- (void)createPaymentWithCharge:(NSDictionary *)charge {
    NSString *appURLScheme;
    
    if ([_channel isEqualToString:@"alipay"]) {
        appURLScheme = @"alipay19810104wl";
    } else if([_channel isEqualToString:@"wx"]) {
        appURLScheme = @"wx9fdbe0ccbadfe82c";
    }
    
    __weak typeof(self) weakSelf = self;
    
    [Pingpp createPayment:charge
           viewController:self
             appURLScheme:appURLScheme
           withCompletion:^(NSString *result, PingppError *error)
     {
         RechargeViewControllerFinishBlock block = weakSelf.finishBlock;
         
         if ([result isEqualToString:@"success"]) {
             if (block) {
                 block(YES);
             }
             [SVProgressHUD dismissWithSuccess:@"充值成功！"];
             
         } else {
             if (block) {
                 block(NO);
             }
             NSLog(@"Error: code=%lu msg=%@", (unsigned long)error.code, [error getMsg]);
             [SVProgressHUD dismissWithError:@"充值失败！"];
         }
     }];
}

@end
