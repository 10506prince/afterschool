//
//  RechargeViewController.h
//  AfterSchool
//
//  Created by lg on 15/12/17.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGViewController.h"

typedef void(^RechargeViewControllerFinishBlock)(BOOL success);

@interface RechargeViewController : LGViewController

- (instancetype)init __deprecated_msg("使用“rechargeViewControllerWithFinishBlock:”代替");

+ (instancetype)rechargeViewControllerWithFinishBlock:(RechargeViewControllerFinishBlock)finishBlock;

@property (nonatomic, strong) NSString *channel;

@end
