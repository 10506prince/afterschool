//
//  RechargeRootView.m
//  AfterSchool
//
//  Created by lg on 15/12/17.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "RechargeRootView.h"

IB_DESIGNABLE
@implementation RechargeRootView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setUp];
    }
    return self;
}
- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self setUp];
    }
    return self;
}

- (void)setUp {
    
    
    [self addSubview:self.navigationBarView];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self.navigationBarView setFrame:CGRectMake(0, 0, self.bounds.size.width, 64)];
}

- (NavigationBarView *)navigationBarView {
    if (!_navigationBarView) {
        _navigationBarView = [[NavigationBarView alloc] initWithTitle:@"充值"
                                                           titleColor:nil
                                                      backgroundColor:nil
                                              leftButtonBgViewOriginX:0
                                                 leftButtonImageWidth:22
                                                 leftButtonTitleColor:nil
                                                       leftButtonName:@"返回"
                                                  leftButtonImageName:@"navigation_bar_return_button_left_arrow"
                                                      rightButtonName:nil
                                                 rightButtonImageName:nil];
    }
    return _navigationBarView;
}

@end
