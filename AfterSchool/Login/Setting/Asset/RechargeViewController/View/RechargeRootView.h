//
//  RechargeRootView.h
//  AfterSchool
//
//  Created by lg on 15/12/17.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavigationBarView.h"

@interface RechargeRootView : UIView

@property (nonatomic, strong) NavigationBarView * navigationBarView;


@end
