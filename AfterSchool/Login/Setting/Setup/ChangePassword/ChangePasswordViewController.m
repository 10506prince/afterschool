//
//  ChangePasswordViewController.m
//  AfterSchool
//
//  Created by lg on 16/1/14.
//  Copyright © 2016年 Teson Draw. All rights reserved.
//

#import "ChangePasswordViewController.h"
#import "NavigationBarView.h"
#import "SVProgressHUD.h"
#import "LGDefineNetServer.h"
#import "TDSingleton.h"
#import "TDMixedCryptogram.h"

@interface ChangePasswordViewController () <UIScrollViewDelegate>

@property (nonatomic, strong) NavigationBarView * navigationBarView;

@property (nonatomic, weak) IBOutlet UIScrollView * scrollView;
@property (nonatomic, weak) IBOutlet UITextField * oldPasswordTextField;
@property (nonatomic, weak) IBOutlet UITextField * nnewPasswordTextField;
@property (nonatomic, weak) IBOutlet UITextField * repeatPasswordTextField;
@property (nonatomic, weak) IBOutlet UIButton * changeBtn;
@end

@implementation ChangePasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initData];
    [self initUserInterface];
}

- (void)initData {
    
}
- (void)initUserInterface {
    [self.view addSubview:self.navigationBarView];
    
    UIEdgeInsets ed = UIEdgeInsetsMake(64, 0, 0, 0);
    [self.scrollView setScrollIndicatorInsets:ed];
    [self.scrollView setContentInset:ed];
}

- (NavigationBarView *)navigationBarView {
    if (!_navigationBarView) {
        _navigationBarView = [[NavigationBarView alloc] initWithTitle:@"修改密码"
                                                           titleColor:nil
                                                      backgroundColor:nil
                                              leftButtonBgViewOriginX:0
                                                 leftButtonImageWidth:22
                                                 leftButtonTitleColor:[UIColor whiteColor]
                                                       leftButtonName:@"返回"
                                                  leftButtonImageName:@"navigation_bar_return_button_left_arrow"
                                                      rightButtonName:nil
                                                 rightButtonImageName:nil];
        
        [_navigationBarView.leftButton addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _navigationBarView;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];
    
    UITouch * touch = [[touches allObjects] firstObject];
    NSLog(@"%@", [touches allObjects]);
    NSLog(@"%@", NSStringFromCGPoint([touch locationInView:self.view]));
    NSLog(@"%@", event);
}

#pragma makr - Action
- (IBAction)backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)changeAction:(id)sender {
    [self.view endEditing:YES];
    
    NSString * old = self.oldPasswordTextField.text;
    NSString * new = self.nnewPasswordTextField.text;
    NSString * repeat = self.repeatPasswordTextField.text;
    
    if (old.length <= 0 || new.length <= 0 || repeat.length <= 0) {
        [SVProgressHUD showErrorWithStatus:@"参数错误"];
        return;
    }
    
    if ([new isEqualToString:repeat]) {
        
        [LGDefineNetServer modifyUserPasswordWithUseerId:[TDSingleton instance].userInfoModel.account oldPswd:[TDMixedCryptogram fromPassword:old] newPswd:[TDMixedCryptogram fromPassword:new] success:^{
            [SVProgressHUD showSuccessWithStatus:@"修改成功"];
        } failure:^(NSString *msg) {
            [SVProgressHUD showErrorWithStatus:msg];
        }];
    } else {
        [SVProgressHUD showErrorWithStatus:@"两次输入密码不同！"];
    }
}

#pragma mark - UIScrollViewDelegate

@end
