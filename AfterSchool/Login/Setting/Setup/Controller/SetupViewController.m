//
//  SetupViewController.m
//  AfterSchool
//
//  Created by lg on 15/10/21.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "SetupViewController.h"
#import "Common.h"
#import "SVProgressHUD.h"

#import "SelectEntranceViewController.h"
#import "SetupRootView.h"
#import "TDSingleton.h"
#import "LoginViewController.h"
#import "TDNetworking.h"

#import <RongIMKit/RongIMKit.h>
#import "AppDelegate.h"
#import "LGDefineNetServer.h"
#import "LGHeartBeatService.h"

#import "MultiLanguageViewController.h"
#import "SelectBackgroundImageViewController.h"
#import "CommonAddressViewController.h"
#import "SetFontViewController.h"
#import "ChangePasswordViewController.h"

#import "TalkingData.h"

#import "IdentityAuthenticationViewController.h"
#import "NotificationName.h"

@interface SetupViewController () <UITableViewDataSource,UITableViewDelegate> {
    NSArray <NSArray <NSString *>*> * _dataSource;
}

@property (nonatomic, strong) NSArray <NSArray <NSString *>*> * dataSource;
@property (nonatomic, strong) SetupRootView * rootView;

@end

@implementation SetupViewController

- (void)dealloc
{
    NSLogSelfMethodName;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [TalkingData trackPageBegin:selfControllerName];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [TalkingData trackPageEnd:selfControllerName];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initData];
    [self initUserInterface];
    [self initObserver];
}

-(void)initData {
    //_dataSource = @[@[@"多语言",@"聊天背景"],@[@"字体大小",@"常用地点"],@[@"清除所有聊天记录"]];
//    _dataSource = @[@[@"多语言"],@[@"常用地点"]];
    if ([TDSingleton instance].userInfoModel.type == 0) {
        _dataSource = @[@[@"修改密码"], @[@"身份认证"], @[@"版本号"]];
    } else {
        _dataSource = @[@[@"修改密码"], @[@"身份认证"], @[@"版本号"], @[@"接收订单"]];
    }
}

- (void)initUserInterface {
    [self.view addSubview:self.rootView];
}

- (void)initObserver {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tableviewReloadData) name:TDIdentifierAuthenticationSuccess object:nil];
}

- (void)tableviewReloadData {
    [_rootView.tableView reloadData];
}

- (SetupRootView *)rootView {
    if (!_rootView) {
        _rootView = [[SetupRootView alloc] initWithFrame:self.view.bounds];
        
        [_rootView.navigationBarView.leftButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        
        [_rootView.tableView setDelegate:self];
        [_rootView.tableView setDataSource:self];
    }
    
    return _rootView;
}

- (void)backAction {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _dataSource.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (_dataSource.count == 0 || _dataSource.count < section + 1) {
        return 0;
    }
    
    NSArray * array = [_dataSource objectAtIndex:section];
    return array.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    //NSString * str = [_dataSource objectAtIndex:indexPath.section][indexPath.row];
    switch (indexPath.section) {
        
        case 0:
        {
            switch (indexPath.row) {
                case 0:
                {
                    LGSetUpTableViewCellOne * cell = [tableView dequeueReusableCellWithClass:[LGSetUpTableViewCellOne class]];
                    cell.name = _dataSource[indexPath.section][indexPath.row];
                    cell.detail = @"";
                    cell.hideArrow = NO;
                    return cell;
                }
                    break;
                    
                default:
                    return [UITableViewCell new];
                    break;
            }

        }
            break;
        
        case 1:
            switch (indexPath.row) {
                case 0:
                {
                    LGSetUpTableViewCellOne * cell = [tableView dequeueReusableCellWithClass:[LGSetUpTableViewCellOne class]];
                    cell.name = _dataSource[indexPath.section][indexPath.row];
                    cell.hideArrow = NO;
                    
                    switch ([TDSingleton instance].userInfoModel.authentication) {
                        case 0:
                            cell.detail = @"未认证";
                            break;
                            
                        case 1:
                            cell.detail = @"认证中";
                            break;
                            
                        case 2:
                            cell.detail = @"已认证";
                            break;
                            
                        default:
                            break;
                    }

                    return cell;
                }
                    break;
                    
                default:
                    return [UITableViewCell new];
                    break;
            }
            break;
            
        case 2:
            switch (indexPath.row) {
                case 0:
                {
                    LGSetUpTableViewCellOne * cell = [tableView dequeueReusableCellWithClass:[LGSetUpTableViewCellOne class]];
                    cell.name = _dataSource[indexPath.section][indexPath.row];
                    cell.detail = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
                    cell.hideArrow = YES;
                    
                    return cell;
                }
                    break;
                    
                default:
                    return [UITableViewCell new];
                    break;
            }
            break;
            
        case 3:
        {
            switch (indexPath.row) {
                case 0:
                {
                    LGSetUpTableViewCellSwitch * cell = [tableView dequeueReusableCellWithClass:[LGSetUpTableViewCellSwitch class]];
                    cell.name = _dataSource[indexPath.section][indexPath.row];
                    cell.switchState = ![TDSingleton instance].userInfoModel.unReceiveOrderState;
                    [cell addSwitchActionBlock:^(BOOL state) {
                        
                        [LGDefineNetServer changeUserInfoReceiveOrderState:!state success:^(NSString *msg) {
                            NSString * str = state ? @"开启订单通知" : @"关闭订单通知";
                            [SVProgressHUD showSuccessWithStatus:str];
                            [TDSingleton instance].userInfoModel.unReceiveOrderState = !state;
                        } failure:^(NSString *msg) {
                            [SVProgressHUD showErrorWithStatus:msg];
                        }];
                    }];
                    return cell;
                }
                    break;
                    
                default:
                    return [UITableViewCell new];
                    break;
            }
        }
            break;
 
        default:
            return [UITableViewCell new];
            break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return LGSetUpTableViewCellOneHeight;
}

//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
//    return 22.0f;
//}
//
//- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
//    switch (section) {
//        case 1:
//            return LGSetupTableViewFooterViewHeight;
//            break;
//            
//        default:
//            return SINGLE_LINE_WIDTH;
//            break;
//    }
//}
#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //NSUInteger index = [self getIndexFromIndexPath:indexPath];
    switch (indexPath.section) {
        case 0:
        {
            switch (indexPath.row) {
                case 0:
                {
                    ChangePasswordViewController * vc = [[ChangePasswordViewController alloc] initWithNibName:@"ChangePasswordViewController" bundle:nil];
                    [self.navigationController pushViewController:vc animated:YES];
                }
                    break;
                    
                default:
                    break;
            }
        }
            break;
            
        case 1:
        {
            if ([TDSingleton instance].userInfoModel.authentication == 0) {
                IdentityAuthenticationViewController *vc = [[IdentityAuthenticationViewController alloc] init];
                [self.navigationController pushViewController:vc animated:YES];
            }
        }
            break;
            
        default:
            break;
    }
}

- (NSUInteger)getIndexFromIndexPath:(NSIndexPath *)indexPath {
    NSUInteger index = 0;
    for (int i = 0; i < indexPath.section; i++) {
        NSUInteger row = [self tableView:self.rootView.tableView numberOfRowsInSection:i];
        index += row;
    }
    index += indexPath.row;
    
    return index;
}

- (IBAction)clearSpeakCache:(id)sender {
    
    NSLogSelfMethodName;
    
    [[RCIM sharedRCIM] clearUserInfoCache];
}

@end
