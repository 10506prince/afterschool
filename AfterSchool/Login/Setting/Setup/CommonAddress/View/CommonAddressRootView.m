//
//  CommonAddressRootView.m
//  AfterSchool
//
//  Created by lg on 15/12/2.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "CommonAddressRootView.h"
#import "Common.h"

@implementation CommonAddressRootView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self addSubview:self.tableView];
        
        [self addSubview:self.navigationBarView];
    }
    return self;
}

- (NavigationBarView *)navigationBarView {
    if (!_navigationBarView) {
        _navigationBarView = [[NavigationBarView alloc] initWithTitle:@"常用地址"
                                                           titleColor:nil
                                                      backgroundColor:nil
                                              leftButtonBgViewOriginX:0
                                                 leftButtonImageWidth:22
                                                 leftButtonTitleColor:[UIColor whiteColor]
                                                       leftButtonName:@"返回"
                                                  leftButtonImageName:@"navigation_bar_return_button_left_arrow"
                                                      rightButtonName:nil
                                                 rightButtonImageName:nil];
    }
    return _navigationBarView;
}

- (LGTableView *)tableView {
    if (!_tableView) {
        _tableView = [[LGTableView alloc] initWithFrame:self.bounds style:UITableViewStylePlain];
        
        _tableView.tableFooterView = self.tableViewFooterView;
        
        [_tableView registerNibCellWithClass:[CommonAddressTableViewCellOne class]];
    }
    return _tableView;
}

- (UIView *)tableViewFooterView {
    if (!_tableViewFooterView) {
        _tableViewFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, 85)];
        
        [_tableViewFooterView addSubview:self.addAddressBtn];
    }
    return _tableViewFooterView;
}

- (UIButton *)addAddressBtn {
    if (!_addAddressBtn) {
        _addAddressBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_addAddressBtn setFrame:CGRectMake(20, 20, self.tableViewFooterView.bounds.size.width - 40, 45)];
        
        [_addAddressBtn setTitle:@"新增地址" forState:UIControlStateNormal];
        [_addAddressBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_addAddressBtn setBackgroundColor:APPLightColor];
        [_addAddressBtn.layer setCornerRadius:3.0];
        //[btn.layer setBorderColor:[UIColor lightGrayColor].CGColor];
        //[btn.layer setBorderWidth:SINGLE_LINE_WIDTH];
    }
    return _addAddressBtn;
}
@end
