//
//  CommonAddressTableViewCellOne.m
//  AfterSchool
//
//  Created by lg on 15/12/2.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "CommonAddressTableViewCellOne.h"
#import "CAShapeLayer+CustomPath.h"

@interface CommonAddressTableViewCellOne ()

@property (nonatomic, weak) IBOutlet UILabel * address;
@property (nonatomic, strong) CAShapeLayer * arrowLayer;

@end

@implementation CommonAddressTableViewCellOne

- (void)awakeFromNib {
    // Initialization code
    self.arrowLayer = [CAShapeLayer getArrowLayer];
    [self.layer addSublayer:self.arrowLayer];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.arrowLayer.path = [CAShapeLayer arrowPathWithRect:self.bounds edgeInsets:UIEdgeInsetsMake(0, 0, 0, 20)];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setCommonAddress:(LGCommonAddress *)commonAddress {
    if (_commonAddress != commonAddress) {
        _commonAddress = commonAddress;
        
        [_address setText:_commonAddress.address];
    }
}
@end
