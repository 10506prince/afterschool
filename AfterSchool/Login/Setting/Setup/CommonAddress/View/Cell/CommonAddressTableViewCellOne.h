//
//  CommonAddressTableViewCellOne.h
//  AfterSchool
//
//  Created by lg on 15/12/2.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LGCommonAddress.h"
#import "UITableView+NibCell.h"

#define CommonAddressTableViewCellOneHeight 55.0f

@interface CommonAddressTableViewCellOne : UITableViewCell

@property (nonatomic, strong) LGCommonAddress * commonAddress;

@end
