//
//  CommonAddressModifyRootView.h
//  AfterSchool
//
//  Created by lg on 15/12/13.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavigationBarView.h"

@interface CommonAddressModifyRootView : UIView

@property (nonatomic, strong) NavigationBarView * navigationBarView;
@property (nonatomic, strong) UITextView * textView;

@end
