//
//  CommonAddressModifyRootView.m
//  AfterSchool
//
//  Created by lg on 15/12/13.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "CommonAddressModifyRootView.h"

@implementation CommonAddressModifyRootView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubview:self.textView];
        
        [self addSubview:self.navigationBarView];
    }
    return self;
}

- (NavigationBarView *)navigationBarView {
    if (!_navigationBarView) {
        _navigationBarView = [[NavigationBarView alloc] initWithTitle:@"修改地址"
                                                           titleColor:nil
                                                      backgroundColor:nil
                                              leftButtonBgViewOriginX:0
                                                 leftButtonImageWidth:22
                                                 leftButtonTitleColor:[UIColor whiteColor]
                                                       leftButtonName:@"返回"
                                                  leftButtonImageName:@"navigation_bar_return_button_left_arrow"
                                                      rightButtonName:@"保存"
                                                 rightButtonImageName:nil];
    }
    return _navigationBarView;
}

- (UITextView *)textView {
    if (!_textView) {
        
//        NSTextContainer * textContainer = [[NSTextContainer alloc] initWithSize:CGSizeMake(self.bounds.size.width , 300)];
//        _textView = [[UITextView alloc] initWithFrame:self.bounds textContainer:textContainer];
        _textView = [[UITextView alloc] initWithFrame:self.bounds];
        [_textView setTextColor:[UIColor blackColor]];
        [_textView setFont:[UIFont systemFontOfSize:16]];
        
        [_textView setContentInset:UIEdgeInsetsMake(64, 0, 0, 0)];
        //[_textView setTextContainerInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    return _textView;
}

@end
