//
//  CommonAddressModifyViewController.h
//  AfterSchool
//
//  Created by lg on 15/12/13.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGViewController.h"

@class LGCommonAddress;

typedef void(^CommonAddressModifyViewControllerSaveBlock)(LGCommonAddress * modifyAddress);

@interface CommonAddressModifyViewController : LGViewController

- (instancetype)initWithCommonAddress:(LGCommonAddress *)commonAddress;

@property (nonatomic, copy) CommonAddressModifyViewControllerSaveBlock saveBlock;

- (void)addSaveBlock:(CommonAddressModifyViewControllerSaveBlock)saveBlock;

@end
