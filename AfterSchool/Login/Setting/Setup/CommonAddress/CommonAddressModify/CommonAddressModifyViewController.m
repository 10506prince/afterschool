//
//  CommonAddressModifyViewController.m
//  AfterSchool
//
//  Created by lg on 15/12/13.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "CommonAddressModifyViewController.h"
#import "CommonAddressModifyRootView.h"
#import "Common.h"
#import "SVProgressHUD.h"
#import "LGCommonAddress.h"
#import "LGDefineNetServer.h"

@interface CommonAddressModifyViewController () <UITextViewDelegate>

@property (nonatomic, strong) LGCommonAddress * commonAddress;
@property (nonatomic, strong) CommonAddressModifyRootView * rootView;

@property (nonatomic, strong) LGCommonAddress * modifyAddress;

@end

@implementation CommonAddressModifyViewController

- (void)dealloc
{
    NSLogSelfMethodName;
}

- (instancetype)initWithCommonAddress:(LGCommonAddress *)commonAddress
{
    self = [super init];
    if (self) {
        self.commonAddress = commonAddress;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initData];
    [self initUserInterface];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)initData {
    self.modifyAddress = [[LGCommonAddress alloc] initWithCommonAddress:self.commonAddress];
}
- (void)initUserInterface {
    [self.view addSubview:self.rootView];
    
    [self.rootView.textView setText:self.modifyAddress.address];
}

- (CommonAddressModifyRootView *)rootView {
    if (!_rootView) {
        _rootView = [[CommonAddressModifyRootView alloc] initWithFrame:self.view.bounds];
        [_rootView.navigationBarView.leftButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        [_rootView.navigationBarView.rightButton addTarget:self action:@selector(saveAction:) forControlEvents:UIControlEventTouchUpInside];
        
        [_rootView.textView setDelegate:self];

    }
    
    return _rootView;
}

#pragma mark - Network

#pragma mark - Block
- (void)addSaveBlock:(CommonAddressModifyViewControllerSaveBlock)saveBlock {
    self.saveBlock = saveBlock;
}

#pragma mark - Action
- (void)backAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)saveAction:(id)sender {
    [self.rootView.textView resignFirstResponder];
    self.modifyAddress.address = self.rootView.textView.text;
    
    if (self.modifyAddress.address.length == 0) {
        [SVProgressHUD showErrorWithStatus:@"地址不能为空"];
        return;
    }
    
    LGCommonAddress * address = self.modifyAddress;
    
    [LGDefineNetServer updateCommonAddress:address success:^(NSString *msg) {
        CommonAddressModifyViewControllerSaveBlock block = self.saveBlock;
        
        if (block) {
            block(address);
        }
        [self backAction];
    } failure:^(NSString *msg) {
        
    }];
}

#pragma mark - UITextViewDelegate

- (void)textViewDidBeginEditing:(UITextView *)textView {
    
}
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    
    return YES;
}

@end
