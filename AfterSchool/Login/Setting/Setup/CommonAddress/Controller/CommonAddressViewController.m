//
//  CommonAddressViewController.m
//  AfterSchool
//
//  Created by lg on 15/12/2.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "CommonAddressViewController.h"
#import "Common.h"
#import "SVProgressHUD.h"
#import "LGDefineNetServer.h"
#import "LGSelectAddressViewController.h"
#import "CommonAddressModifyViewController.h"
#import "TDSingleton.h"

#import "TalkingData.h"

@interface CommonAddressViewController () <UITableViewDataSource,UITableViewDelegate> {
    NSMutableArray <LGCommonAddress *> * _commonAddress;
}
@property (nonatomic, strong) NSMutableArray <LGCommonAddress *> * commonAddress;

@end

@implementation CommonAddressViewController
- (void)dealloc
{
    NSLogSelfMethodName;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [TalkingData trackPageBegin:selfControllerName];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [TalkingData trackPageEnd:selfControllerName];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initData];
    [self initUserInterface];
    [self updateData];
}
- (void)initData {
    self.commonAddress = [NSMutableArray array];
}
- (void)initUserInterface {
    [self.view addSubview:self.rootView];
}
- (void)updateData {
    if ([TDSingleton instance].userInfoModel.commonAddresses.count == 0) {
        
        __weak typeof(self) weakSelf = self;
        
        [LGDefineNetServer getCommonAddressSuccess:^(NSArray<LGCommonAddress *> *addresses) {
            [TDSingleton instance].userInfoModel.commonAddresses = addresses;
            
            [weakSelf.commonAddress addObjectsFromArray:addresses];
            [weakSelf.rootView.tableView reloadData];
            
        } failure:^(NSString *msg) {
            [SVProgressHUD showErrorWithStatus:msg];
        }];
    } else {
        [self.commonAddress addObjectsFromArray:[TDSingleton instance].userInfoModel.commonAddresses];
        [self.rootView.tableView reloadData];
    }
}

- (CommonAddressRootView *)rootView {
    if (!_rootView) {
        _rootView = [[CommonAddressRootView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        
        [_rootView.navigationBarView.leftButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        [_rootView.addAddressBtn addTarget:self action:@selector(addNewCommonAddress:) forControlEvents:UIControlEventTouchUpInside];
        
        [_rootView.tableView setDataSource:self];
        [_rootView.tableView setDelegate:self];
    }
    
    return _rootView;
}

#pragma mark - Action
- (void)backAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)addNewCommonAddress:(id)sender {
    NSLogSelfMethodName;
    
    if (_commonAddress.count >= MAX_COMMON_ADDRESS_NUMBER) {
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"常用地址已经满%d个，不能继续添加，请修改现有的或者删除后重新添加", MAX_COMMON_ADDRESS_NUMBER] duration:2.5];
        return;
    }
    
    LGSelectAddressViewController *addGameAccountVC = [[LGSelectAddressViewController alloc] init];
    
    __weak typeof(self) weakSelf = self;
    
    [addGameAccountVC setClickEnterActionBlock:^(LGSelectAddressResult * address) {

        NSString * position = [NSString stringWithFormat:@"%f,%f", address.pt.longitude, address.pt.latitude];
        
        if (address.name.length > 0 && address.address.length > 0) {
            
            LGCommonAddress * newAddress = [[LGCommonAddress alloc] init];
            newAddress.address = address.address;
            newAddress.name = address.name;
            newAddress.position = position;
            
            [LGDefineNetServer addCommonAddress:newAddress success:^{
                [weakSelf.commonAddress insertObject:newAddress atIndex:0];
                [TDSingleton instance].userInfoModel.commonAddresses = weakSelf.commonAddress;
                
                NSIndexPath * indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
                [weakSelf.rootView.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationTop];
            } failure:^(NSString *msg) {
                [SVProgressHUD dismissWithError:msg];
            }];
        }
    }];
    
    [self.navigationController pushViewController:addGameAccountVC animated:YES];
}

#pragma mark - UITableViewDataSource 列表数据代理
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return _commonAddress.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSUInteger index = [self getIndexFromIndexPath:indexPath];
    
    CommonAddressTableViewCellOne * cell = [tableView dequeueReusableCellWithClass:[CommonAddressTableViewCellOne class]];
    cell.commonAddress = _commonAddress[index];
    
    return cell;
}
#pragma mark - UITableViewDelegate 列表代理
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
//    LGCommonAddress * address = [_commonAddress objectAtIndex:indexPath.row];
//    
//    CommonAddressModifyViewController * modifyVC = [[CommonAddressModifyViewController alloc] initWithCommonAddress:address];
//    [self.navigationController pushViewController:modifyVC animated:YES];
//    
//    //__weak typeof(self) weakSelf = self;
//    [modifyVC addSaveBlock:^(LGCommonAddress *modifyAddress) {
//        
//        address.address = modifyAddress.address;
//        
//        [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
//    }];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath {
    return @"删除";
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    __weak typeof(self) weakSelf = self;
    
    switch (editingStyle) {
        case UITableViewCellEditingStyleDelete:
        {
            LGCommonAddress * commonAddress = _commonAddress[indexPath.row];
            [LGDefineNetServer removeCommonAddress:commonAddress success:^{
                [weakSelf.commonAddress removeObjectAtIndex:indexPath.row];
                [TDSingleton instance].userInfoModel.commonAddresses = weakSelf.commonAddress;
                [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationRight];
            } failure:^(NSString *msg) {
                [SVProgressHUD dismissWithError:msg];
            }];
        }
            break;
        case UITableViewCellEditingStyleInsert:
        {
        }
            break;
        case UITableViewCellEditingStyleNone:
        {
        }
            break;
        default:
            break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return CommonAddressTableViewCellOneHeight;
}

//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
//    return 0.1f;
//}
//
//- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
//    return 0.1f;
//}

- (NSUInteger)getIndexFromIndexPath:(NSIndexPath *)indexPath {
    NSUInteger index = 0;
    for (int i = 0; i < indexPath.section; i++) {
        NSUInteger row = [self tableView:self.rootView.tableView numberOfRowsInSection:i];
        index += row;
    }
    index += indexPath.row;
    
    return index;
}




@end
