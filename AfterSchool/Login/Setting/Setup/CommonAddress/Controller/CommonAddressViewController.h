//
//  CommonAddressViewController.h
//  AfterSchool
//
//  Created by lg on 15/12/2.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGViewController.h"
#import "CommonAddressRootView.h"

@interface CommonAddressViewController : LGViewController

@property (nonatomic, strong) CommonAddressRootView * rootView;

@end
