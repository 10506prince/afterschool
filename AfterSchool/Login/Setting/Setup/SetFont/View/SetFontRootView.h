//
//  SetFontRootView.h
//  AfterSchool
//
//  Created by Teson Draw on 12/3/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavigationBarView.h"
#import "FontSizeType.h"

@interface SetFontRootView : UIView

@property (nonatomic, strong) NavigationBarView * navigationBarView;

@property (nonatomic, strong) UIView *scaleBackgroundView;
@property (nonatomic, strong) CAShapeLayer *scaleLayer;

@property (nonatomic, strong) UISlider *slider;

@property (nonatomic, assign) FontSizeType fontSizeType;

@property (nonatomic, strong) UITextView *descriptionTextView;

@property (nonatomic, strong) UILabel *smallALabel;
@property (nonatomic, strong) UILabel *bigALabel;

@end
