//
//  SetFontRootView.m
//  AfterSchool
//
//  Created by Teson Draw on 12/3/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "SetFontRootView.h"
#import "TDShapeLayer.h"

@implementation SetFontRootView


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithRed:234/255.f green:234/255.f blue:234/255.f alpha:1];
        [self addSubview:self.navigationBarView];
        [self addSubview:self.descriptionTextView];
        [self addSubview:self.scaleBackgroundView];
        [_scaleBackgroundView.layer addSublayer:[TDShapeLayer scaleLayerWithSuperviewWidth:_scaleBackgroundView.bounds.size.width height:_scaleBackgroundView.bounds.size.height]];
        [_scaleBackgroundView addSubview:self.slider];
        [_scaleBackgroundView addSubview:self.smallALabel];
        [_scaleBackgroundView addSubview:self.bigALabel];
    }
    return self;
}

- (NavigationBarView *)navigationBarView {
    if (!_navigationBarView) {
        _navigationBarView = [[NavigationBarView alloc] initWithTitle:@"文字大小"
                                                           titleColor:nil
                                                      backgroundColor:nil
                                              leftButtonBgViewOriginX:0
                                                 leftButtonImageWidth:22
                                                 leftButtonTitleColor:[UIColor whiteColor]
                                                       leftButtonName:@"返回"
                                                  leftButtonImageName:@"navigation_bar_return_button_left_arrow"
                                                      rightButtonName:nil
                                                 rightButtonImageName:nil];
    }
    return _navigationBarView;
}

- (UIView *)scaleBackgroundView {
    if (!_scaleBackgroundView) {
        _scaleBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_descriptionTextView.frame), self.bounds.size.width, 120)];
        _scaleBackgroundView.backgroundColor = [UIColor whiteColor];
    }
    return _scaleBackgroundView;
}

- (UILabel *)smallALabel {
    if (!_smallALabel) {
        NSInteger leftMarginLength = (_scaleBackgroundView.bounds.size.width - 250) / 2;
        NSInteger topMarginLength = _scaleBackgroundView.bounds.size.height * 2 / 3 - 20 - 40;
        _smallALabel = [[UILabel alloc] initWithFrame:CGRectMake(leftMarginLength - 7, topMarginLength, 14, 14)];
        _smallALabel.textColor = [UIColor blackColor];
        _smallALabel.font = [UIFont systemFontOfSize:14];
        _smallALabel.text = @"A";
        _smallALabel.textAlignment = NSTextAlignmentCenter;
    }
    return _smallALabel;
}

- (UILabel *)bigALabel {
    if (!_bigALabel) {
        NSInteger leftMarginLength = (_scaleBackgroundView.bounds.size.width - 250) / 2;
        NSInteger topMarginLength = _scaleBackgroundView.bounds.size.height * 2 / 3 - 20 - 40;
        
        _bigALabel = [[UILabel alloc] initWithFrame:CGRectMake(_scaleBackgroundView.bounds.size.width - leftMarginLength - 7, topMarginLength, 14, 14)];
        _bigALabel.textColor = [UIColor blackColor];
        _bigALabel.font = [UIFont systemFontOfSize:22];
        _bigALabel.text = @"A";
        _bigALabel.textAlignment = NSTextAlignmentCenter;
    }
    return _bigALabel;
}

- (CAShapeLayer *)scaleLayer {
    if (!_scaleLayer) {
        _scaleLayer = [CAShapeLayer init];
        _scaleLayer.fillColor = [UIColor clearColor].CGColor;
        _scaleLayer.strokeColor = [UIColor lightGrayColor].CGColor;
    }
    return _scaleLayer;
}

- (UISlider *)slider {
    if (!_slider) {
        NSInteger leftMarginLength = (_scaleBackgroundView.bounds.size.width - 250) / 2;
        NSInteger topMarginLength = _scaleBackgroundView.bounds.size.height * 2 / 3 - 20;
        _slider = [[UISlider alloc] initWithFrame:CGRectMake(leftMarginLength - 15.5, topMarginLength - 30, 250 + 31, 60)];
        _slider.minimumTrackTintColor = [UIColor clearColor];
        //设置未滑过一端滑动条颜色
        _slider.maximumTrackTintColor = [UIColor clearColor];
        
        _slider.minimumValue = 0;
        //设置最大值
        _slider.maximumValue = 250;
    }
    return _slider;
}

- (UITextView *)descriptionTextView {
    if (!_descriptionTextView) {
        _descriptionTextView = [[UITextView alloc] initWithFrame:CGRectMake(30, 64 + 20, self.bounds.size.width - 60, 160)];
        _descriptionTextView.text = @"拖动下面的滑块，可设置聊天字体的大小";
        _descriptionTextView.backgroundColor = [UIColor colorWithRed:234/255.f green:234/255.f blue:234/255.f alpha:1];
        _descriptionTextView.textAlignment = NSTextAlignmentCenter;
//        _descriptionTextView.layer.borderColor = [UIColor blackColor].CGColor;
//        _descriptionTextView.layer.borderWidth = 1;
        
    }
    return _descriptionTextView;
}


@end
