//
//  SetFontViewController.m
//  AfterSchool
//
//  Created by Teson Draw on 12/3/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "SetFontViewController.h"
#import "TDSingleton.h"

#define FONT_SMALL      12
#define FONT_STANDARD   14
#define FONT_BIG        16
#define FONT_BIGGER     18
#define FONT_LARGE      20
#define FONT_VERY_LARGE 22

@implementation SetFontViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initData];
    [self initUserInterface];
}

-(void)initData {
//    _fontSizeType = [TDSingleton instance].fontSizeType;
}

- (void)initUserInterface {
    [self.view addSubview:self.rootView];
    
    switch ([TDSingleton instance].fontSizeType) {
        case FontSizeSmall:
            _rootView.slider.value = 0;
            _rootView.descriptionTextView.font = [UIFont systemFontOfSize:FONT_SMALL];
            break;
            
        case FontSizeStandard:
            _rootView.slider.value = 50;
            _rootView.descriptionTextView.font = [UIFont systemFontOfSize:FONT_STANDARD];
            break;
            
        case FontSizeBig:
            _rootView.slider.value = 100;
            _rootView.descriptionTextView.font = [UIFont systemFontOfSize:FONT_BIG];
            break;
            
        case FontSizeBigger:
            _rootView.slider.value = 150;
            _rootView.descriptionTextView.font = [UIFont systemFontOfSize:FONT_BIGGER];
            break;
            
        case FontSizeLarge:
            _rootView.slider.value = 200;
            _rootView.descriptionTextView.font = [UIFont systemFontOfSize:FONT_LARGE];
            break;
            
        case FontSizeVeryLarge:
            _rootView.slider.value = 250;
            _rootView.descriptionTextView.font = [UIFont systemFontOfSize:FONT_VERY_LARGE];
            break;
    }
}

- (SetFontRootView *)rootView {
    if (!_rootView) {
        _rootView = [[SetFontRootView alloc] initWithFrame:self.view.bounds];
        
        [_rootView.navigationBarView.leftButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        
        [_rootView.slider addTarget:self action:@selector(sliderAction:) forControlEvents:UIControlEventTouchDragInside];
        [_rootView.slider addTarget:self action:@selector(sliderAction:) forControlEvents:UIControlEventTouchDragOutside];
        [_rootView.slider addTarget:self action:@selector(sliderAction:) forControlEvents:UIControlEventTouchDown];
        [_rootView.slider addTarget:self action:@selector(sliderAction:) forControlEvents:UIControlEventTouchUpInside];
        [_rootView.slider addTarget:self action:@selector(sliderAction:) forControlEvents:UIControlEventTouchUpOutside];
    }
    
    return _rootView;
}

- (void)backAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    if (self.returnTextBlock != nil) {
//        self.returnTextBlock(self.inputTF.text);
    }
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)returnText:(ReturnTextBlock)block {
    self.returnTextBlock = block;
}

- (void)sliderAction:(UISlider *)sender {
    if (sender.value < 25) {
        sender.value = 0;
        [TDSingleton instance].fontSizeType = FontSizeSmall;
        _rootView.descriptionTextView.font = [UIFont systemFontOfSize:FONT_SMALL];
    } else if (sender.value >= 25 && sender.value < 75) {
        sender.value = 50;
        [TDSingleton instance].fontSizeType = FontSizeStandard;
        _rootView.descriptionTextView.font = [UIFont systemFontOfSize:FONT_STANDARD];
    } else if (sender.value >= 75 && sender.value < 125) {
        sender.value = 100;
        [TDSingleton instance].fontSizeType = FontSizeBig;
        _rootView.descriptionTextView.font = [UIFont systemFontOfSize:FONT_BIG];
    } else if (sender.value >= 125 && sender.value < 175) {
        sender.value = 150;
        [TDSingleton instance].fontSizeType = FontSizeBigger;
        _rootView.descriptionTextView.font = [UIFont systemFontOfSize:FONT_BIGGER];
    } else if (sender.value >= 175 && sender.value < 225) {
        
        sender.value = 200;
        [TDSingleton instance].fontSizeType = FontSizeLarge;
        _rootView.descriptionTextView.font = [UIFont systemFontOfSize:FONT_LARGE];
    } else if (sender.value >= 225) {
        sender.value = 250;
        [TDSingleton instance].fontSizeType = FontSizeVeryLarge;
        _rootView.descriptionTextView.font = [UIFont systemFontOfSize:FONT_VERY_LARGE];
    }
    
    [[TDSingleton instance].settingDictionary setValue:[NSNumber numberWithInt:[TDSingleton instance].fontSizeType] forKey:@"FontSizeType"];
    [[TDSingleton instance].settingDictionary writeToFile:[TDSingleton instance].settingPlistFileName atomically:YES];
}

@end
