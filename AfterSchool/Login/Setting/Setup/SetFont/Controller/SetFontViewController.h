//
//  SetFontViewController.h
//  AfterSchool
//
//  Created by Teson Draw on 12/3/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SetFontRootView.h"

typedef void (^ReturnTextBlock)(NSString *showText);

@interface SetFontViewController : UIViewController

@property (nonatomic, strong) SetFontRootView *rootView;

@property (copy) void(^backBlock)();

@property (nonatomic, copy) ReturnTextBlock returnTextBlock;

@property (nonatomic, assign) FontSizeType fontSizeType;

- (void)returnText:(ReturnTextBlock)block;

@end
