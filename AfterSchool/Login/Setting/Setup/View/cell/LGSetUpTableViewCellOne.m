//
//  LGSetUpTableViewCell.m
//  AfterSchool
//
//  Created by lg on 15/10/24.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGSetUpTableViewCellOne.h"
#import "CAShapeLayer+CustomPath.h"

@interface LGSetUpTableViewCellOne ()

@property (nonatomic, strong) IBOutlet UILabel * nameLabel;
@property (nonatomic, strong) IBOutlet UILabel * detailLabel;

@property (nonatomic, strong) CAShapeLayer * line;
@property (nonatomic, strong) CAShapeLayer * arrow;

@end

@implementation LGSetUpTableViewCellOne

- (void)awakeFromNib {
    self.line = [CAShapeLayer getLineLayer];
    self.arrow = [CAShapeLayer getArrowLayer];
    
    [self.layer addSublayer:self.line];
    [self.layer addSublayer:self.arrow];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.line.path = [CAShapeLayer linePathWithRect:self.bounds edgeInsets:UIEdgeInsetsMake(0, 20, 0, 20)];
    self.arrow.path = [CAShapeLayer arrowPathWithRect:self.bounds edgeInsets:UIEdgeInsetsMake(0, 0, 0, 20)];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setName:(NSString *)name {
    _name = [name copy];
    
    [self.nameLabel setText:_name];
}
- (void)setDetail:(NSString *)detail {
    _detail = [detail copy];
    
    [self.detailLabel setText:_detail];
}

- (void)setHideArrow:(BOOL)hideArrow {
    _hideArrow = hideArrow;
    
    [self.arrow setHidden:_hideArrow];
}

@end
