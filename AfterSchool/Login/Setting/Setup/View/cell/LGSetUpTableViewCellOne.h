//
//  LGSetUpTableViewCell.h
//  AfterSchool
//
//  Created by lg on 15/10/24.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>

#define LGSetUpTableViewCellOneHeight 50.0f

@interface LGSetUpTableViewCellOne : UITableViewCell

@property (nonatomic, copy) NSString * name;
@property (nonatomic, copy) NSString * detail;
@property (nonatomic, assign) BOOL hideArrow;

@end
