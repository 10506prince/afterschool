//
//  LGSetupTableViewHeaderFooterView.m
//  AfterSchool
//
//  Created by lg on 15/10/21.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGSetupTableViewHeaderFooterView.h"
#import "UIColor+RGB.h"
#import "Common.h"

@implementation LGSetupTableViewFooterView

- (void)awakeFromNib {
    [self setUp];
}

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if (self) {
        [self setUp];
    }
    
    return self;
}

- (void)setUp {
    
    [_logoutBtn.layer setCornerRadius:3];
    
//    _logoutBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    
//    [_logoutBtn setTitle:@"退出登录" forState:UIControlStateNormal];
//    [_logoutBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    [_logoutBtn setBackgroundColor:[UIColor getColor:@"f51b6b"]];
//    
//    [_logoutBtn setFrame:CGRectMake(12, 150, LgScreenWidth - 24, 44)];
//    
//    [self addSubview:_logoutBtn];
}

@end
