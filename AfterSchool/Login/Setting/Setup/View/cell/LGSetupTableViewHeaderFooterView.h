//
//  LGSetupTableViewHeaderFooterView.h
//  AfterSchool
//
//  Created by lg on 15/10/21.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>

#define LGSetupTableViewFooterViewHeight 120.0f


@interface LGSetupTableViewFooterView : UITableViewHeaderFooterView

@property (nonatomic, strong) IBOutlet UIButton * logoutBtn;

@end
