//
//  LGSetUpTableViewCellSwitch.m
//  AfterSchool
//
//  Created by lg on 16/1/14.
//  Copyright © 2016年 Teson Draw. All rights reserved.
//

#import "LGSetUpTableViewCellSwitch.h"
#import "CAShapeLayer+CustomPath.h"

@interface LGSetUpTableViewCellSwitch ()

@property (nonatomic, strong) IBOutlet UILabel * nameLabel;
@property (nonatomic, strong) IBOutlet UISwitch * functionSwitch;

@property (nonatomic, strong) CAShapeLayer * line;

@property (nonatomic, copy) LGSetUpTableViewCellSwitchBlock switchActionBlock;

@end

@implementation LGSetUpTableViewCellSwitch

- (void)awakeFromNib {
    self.line = [CAShapeLayer getLineLayer];
    
    [self.layer addSublayer:self.line];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.line.path = [CAShapeLayer linePathWithRect:self.bounds edgeInsets:UIEdgeInsetsMake(0, 20, 0, 20)];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)setName:(NSString *)name {
    _name = [name copy];
    
    [self.nameLabel setText:_name];
}



- (void)setSwitchState:(BOOL)switchState {
    [self.functionSwitch setOn:switchState animated:YES];
}
- (BOOL)isSwitchOn {
    return self.functionSwitch.on;
}

- (IBAction)switchAction:(UISwitch *)sender {
    if (self.switchActionBlock) {
        self.switchActionBlock(sender.on);
    }
}

- (void)addSwitchActionBlock:(LGSetUpTableViewCellSwitchBlock)switchActionBlock {
    self.switchActionBlock = switchActionBlock;
}
@end
