//
//  LGSetUpTableViewCellSwitch.h
//  AfterSchool
//
//  Created by lg on 16/1/14.
//  Copyright © 2016年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^LGSetUpTableViewCellSwitchBlock)(BOOL state);

@interface LGSetUpTableViewCellSwitch : UITableViewCell

@property (nonatomic, copy) NSString * name;
@property (nonatomic, assign, getter=isSwitchOn) BOOL switchState;

- (void)addSwitchActionBlock:(LGSetUpTableViewCellSwitchBlock)switchActionBlock;

@end
