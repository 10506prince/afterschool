//
//  SetupRootView.m
//  AfterSchool
//
//  Created by lg on 15/10/21.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "SetupRootView.h"

@implementation SetupRootView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubview:self.tableView];
        
        [self addSubview:self.navigationBarView];
    }
    return self;
}

- (NavigationBarView *)navigationBarView {
    if (!_navigationBarView) {
        _navigationBarView = [[NavigationBarView alloc] initWithTitle:@"设置"
                                                           titleColor:nil
                                                      backgroundColor:nil
                                              leftButtonBgViewOriginX:0
                                                 leftButtonImageWidth:22
                                                 leftButtonTitleColor:[UIColor whiteColor]
                                                       leftButtonName:@"返回"
                                                  leftButtonImageName:@"navigation_bar_return_button_left_arrow" rightButtonName:nil
                                                 rightButtonImageName:nil];
    }
    return _navigationBarView;
}

- (LGTableView *)tableView {
    
    if (!_tableView) {
        _tableView = [[LGTableView alloc] initWithFrame:self.bounds style:UITableViewStylePlain];
        
        [_tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        
        [_tableView registerNibCellWithClass:[LGSetUpTableViewCellOne class]];
        [_tableView registerNibCellWithClass:[LGSetUpTableViewCellTwo class]];
        [_tableView registerNibCellWithClass:[LGSetUpTableViewCellSwitch class]];
        
        [_tableView registerNibHeaderFooterViewWithClass:[LGSetupTableViewFooterView class]];
    }
    return _tableView;
}

@end
