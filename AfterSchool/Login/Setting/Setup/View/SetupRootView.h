//
//  SetupRootView.h
//  AfterSchool
//
//  Created by lg on 15/10/21.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LGTableView.h"
#import "NavigationBarView.h"

#import "LGSetUpTableViewCellOne.h"
#import "LGSetUpTableViewCellTwo.h"
#import "LGSetUpTableViewCellSwitch.h"

#import "LGSetupTableViewHeaderFooterView.h"

#import "UITableView+NibCell.h"

@interface SetupRootView : UIView

@property (nonatomic, strong) NavigationBarView * navigationBarView;
@property (nonatomic, strong) LGTableView * tableView;

@end
