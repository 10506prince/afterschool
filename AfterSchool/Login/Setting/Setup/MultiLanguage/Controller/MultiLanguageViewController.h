//
//  MultiLanguageViewController.h
//  AfterSchool
//
//  Created by Teson Draw on 12/2/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MultiLanguageRootView.h"


@interface MultiLanguageViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) MultiLanguageRootView *rootView;
@property (nonatomic, strong) NSArray *dataSource;

@end
