//
//  MultiLanguageViewController.m
//  AfterSchool
//
//  Created by Teson Draw on 12/2/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "MultiLanguageViewController.h"


@implementation MultiLanguageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initData];
    [self initUserInterface];
}

-(void)initData {
    _dataSource = @[@"中文"];
}

- (void)initUserInterface {
    [self.view addSubview:self.rootView];
}

- (MultiLanguageRootView *)rootView {
    if (!_rootView) {
        _rootView = [[MultiLanguageRootView alloc] initWithFrame:self.view.bounds];
        
        [_rootView.navigationBarView.leftButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        
        [_rootView.tableView setDelegate:self];
        [_rootView.tableView setDataSource:self];
    }
    
    return _rootView;
}

- (void)backAction {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

#pragma mark UITableView Delegate Method
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString * reuseIdentifier = @"reuseIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    }
    
    cell.textLabel.text = _dataSource[indexPath.row];
    cell.textLabel.font = [UIFont boldSystemFontOfSize:16];
    cell.accessoryType = UITableViewCellAccessoryCheckmark;
    
    return cell;
}

//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
//    cell.accessoryType = UITableViewCellAccessoryCheckmark;
//}

@end
