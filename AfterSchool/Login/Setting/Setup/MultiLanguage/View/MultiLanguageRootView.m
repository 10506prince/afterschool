//
//  MultiLanguageRootView.m
//  AfterSchool
//
//  Created by Teson Draw on 12/2/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "MultiLanguageRootView.h"

@implementation MultiLanguageRootView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self addSubview:self.navigationBarView];
        [self addSubview:self.tableView];
    }
    return self;
}

- (NavigationBarView *)navigationBarView {
    if (!_navigationBarView) {
        _navigationBarView = [[NavigationBarView alloc] initWithTitle:@"多语言"
                                                           titleColor:nil
                                                      backgroundColor:nil
                                              leftButtonBgViewOriginX:0
                                                 leftButtonImageWidth:22
                                                 leftButtonTitleColor:[UIColor whiteColor]
                                                       leftButtonName:@"取消"
                                                  leftButtonImageName:nil
                                                      rightButtonName:@"保存"
                                                 rightButtonImageName:nil];
        [_navigationBarView.rightButton setEnabled:NO];
        [_navigationBarView.rightButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateDisabled];
    }
    return _navigationBarView;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 64, self.bounds.size.width, self.bounds.size.height - 64) style:UITableViewStylePlain];
    }
    return _tableView;
}

@end
