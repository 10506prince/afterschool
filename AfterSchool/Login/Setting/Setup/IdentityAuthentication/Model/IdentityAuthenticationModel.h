//
//  IdentityAuthenticationModel.h
//  AfterSchool
//
//  Created by Teson Draw on 1/15/16.
//  Copyright © 2016 Teson Draw. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface IdentityAuthenticationModel : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *identifier;

@property (nonatomic, assign) NSString *sex;

//@property (nonatomic, strong) NSString *idCardImageURL;
//@property (nonatomic, strong) NSString *personalPortraitURL;

@property (nonatomic, strong) UIImage *idCardImage;
@property (nonatomic, strong) UIImage *personalPortraitImage;

@property (nonatomic, strong) NSString *idCardImageObjectKey;
@property (nonatomic, strong) NSString *personalPortraitImageObjectKey;

/**
 *  0表示选择身份证
 *  1表示选择的个人图片 
 */
@property (nonatomic, assign) NSInteger selectedImage;

//@property (nonatomic, strong) NSURL *idCardImageURL;
//@property (nonatomic, strong) NSURL *personalPortraitURL;

@end
