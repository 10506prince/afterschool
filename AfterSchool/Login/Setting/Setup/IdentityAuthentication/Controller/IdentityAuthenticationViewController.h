//
//  IdentityAuthenticationViewController.h
//  AfterSchool
//
//  Created by Teson Draw on 1/15/16.
//  Copyright © 2016 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IdentityAuthenticationRootView.h"
#import "IdentityAuthenticationModel.h"

@interface IdentityAuthenticationViewController : UIViewController

@property (nonatomic, strong) IdentityAuthenticationRootView *rootView;

@property (nonatomic, strong) NSArray *dataSource;

@property (nonatomic, strong) IdentityAuthenticationModel *model;

@property (nonatomic, assign) BOOL uploadIDCardSuccess;
@property (nonatomic, assign) BOOL uploadPersonalImageSuccess;


@end
