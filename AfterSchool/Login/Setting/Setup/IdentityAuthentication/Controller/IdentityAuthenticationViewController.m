//
//  IdentityAuthenticationViewController.m
//  AfterSchool
//
//  Created by Teson Draw on 1/15/16.
//  Copyright © 2016 Teson Draw. All rights reserved.
//

#import "IdentityAuthenticationViewController.h"

#import "MacrosDefinition.h"

#import "Common.h"
#import "TalkingData.h"

#import "TextDetailRightArrowTableViewCell.h"
#import "ButtonTableViewCell.h"
#import "UploadIDCardTableViewCell.h"
#import "TDNetworking.h"
#import "TDSingleton.h"
#import "SVProgressHUD.h"
#import "LGDefineNetServer.h"

#import "NotificationName.h"
#import "NSString+MD5String.h"

@interface IdentityAuthenticationViewController () <UITableViewDataSource, UITableViewDelegate, TDInputTextViewDelegate, UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, TDSexPickerViewDelegate>

@end

@implementation IdentityAuthenticationViewController

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [TalkingData trackPageBegin:selfControllerName];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [TalkingData trackPageEnd:selfControllerName];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initData];
    [self initUserInterface];
}

- (void)initData {
    _model = [[IdentityAuthenticationModel alloc] init];
    _dataSource = @[@[@"姓名", @"性别", @"身份证"], @[@"图片"], @[@"提交"]];
    
    _uploadIDCardSuccess        = NO;
    _uploadPersonalImageSuccess = NO;
}

- (void)initUserInterface {
    [self.view addSubview:self.rootView];
}

- (IdentityAuthenticationRootView *)rootView {
    if (!_rootView) {
        _rootView = [[IdentityAuthenticationRootView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
        [_rootView.navigationBarView.leftButton addTarget:self action:@selector(returnButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        
        _rootView.tableView.dataSource = self;
        _rootView.tableView.delegate = self;
    }
    return _rootView;
}

- (void)returnButtonClicked {
    [self.navigationController popViewControllerAnimated:YES];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _dataSource.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return ((NSArray *)_dataSource[section]).count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *reuseIdentifier = @"reuseIdentifier";
    static NSString *buttonReuseIdentifier = @"buttonReuseIdentifier";
    static NSString *uploadIDCardReuseIdentifier = @"uploadIDCardReuseIdentifier";
    
    
    switch (indexPath.section) {
        case 0:
        {
            TextDetailRightArrowTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
            
            if (!cell) {
                cell = [[TextDetailRightArrowTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier cellHeight:68 textColor:[UIColor colorWithRed:56/255.f green:56/255.f blue:56/255.f alpha:1] textFontSize:0 detailColor:nil detailFontSize:0 showDividingLine:YES];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }

            switch (indexPath.row) {
                case 0:
                    cell.itemLabel.text = _dataSource[indexPath.section][indexPath.row];
                    
                    if (_model.name) {
                        cell.detailLabel.text = _model.name;
                    } else {
                        cell.detailLabel.text = @"真实姓名";
                    }
                   
                    [cell.button addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
                    cell.button.tag = 100;
                    
                    return cell;
                    
                case 1:
                    cell.itemLabel.text = _dataSource[indexPath.section][indexPath.row];
                    
                    if (_model.sex) {
                        cell.detailLabel.text = _model.sex;
                    } else {
                        cell.detailLabel.text = @"点击选择";
                    }
                    cell.detailLabel.textColor = [UIColor colorWithRed:56/255.f green:56/255.f blue:56/255.f alpha:1];
                    
                    [cell.button addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
                    cell.button.tag = 101;
                    
                    return cell;
                    
                case 2:
                    cell.itemLabel.text = _dataSource[indexPath.section][indexPath.row];
                    
                    if (_model.identifier) {
                        cell.detailLabel.text = _model.identifier;
                    } else {
                        cell.detailLabel.text = @"身份证号码";
                    }
                    
                    [cell.dividingLineView removeFromSuperview];
                    [cell.button addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
                    cell.button.tag = 102;
                    return cell;
                    
                default:
                    return [UITableViewCell new];
            }
        }
            break;
            
        case 1:
        {
            UploadIDCardTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:uploadIDCardReuseIdentifier];
            
            if (!cell) {
                cell = [[UploadIDCardTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:uploadIDCardReuseIdentifier backgroundColor:nil];
                
                [cell.uploadIDCardButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
                cell.uploadIDCardButton.tag = 103;
                
                [cell.uploadPersonalPortraitButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
                cell.uploadPersonalPortraitButton.tag = 104;
                
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            
            if (_model.idCardImage) {
                [cell.uploadIDCardButton setImage:_model.idCardImage forState:UIControlStateNormal];
            }
            
            if (_model.personalPortraitImage) {
                [cell.uploadPersonalPortraitButton setImage:_model.personalPortraitImage forState:UIControlStateNormal];
            }
            return cell;
        }
    
        case 2:
        {
            ButtonTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:buttonReuseIdentifier];
            
            if (!cell) {
                cell = [[ButtonTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:buttonReuseIdentifier backgroundColor:nil cellHeight:95 buttonHeight:45 buttonOrginX:20 buttonTitle:@"提交" buttonFontSize:19 buttonTitleColor:nil buttonBackgroundColor:nil];
                
                [cell.button addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
                cell.button.tag = 105;
            }
            return cell;
        }
            
        default:
            return [UITableViewCell new];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {

    switch (indexPath.section) {
        case 0:
            return 68;
            
        case 1:
            return 235;
            
        case 2:
            return 95;
            
        default:
            return 95;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 5;
}

- (void)buttonClicked:(UIButton *)sender {
    switch (sender.tag) {
        case 100://输入姓名
            NSLog(@"100");
        {
            [self.view addSubview:_rootView.inputNameTextView];
            if (_model.name && _model.name > 0) {
                _rootView.inputNameTextView.inputTextField.text = _model.name;
            }
            
            [_rootView.inputNameTextView.inputTextField becomeFirstResponder];
            _rootView.inputNameTextView.delegate = self;
            [_rootView.inputNameTextView moveToTargetPositon];
        }
            break;
        
        case 101://输入性别
        {
            [self.view addSubview:_rootView.sexPickerView];
            _rootView.sexPickerView.delegate = self;
            [_rootView.sexPickerView moveToTargetPositon];
        }
            break;
            
        case 102://输入身份证号
             NSLog(@"101");
            [self.view addSubview:_rootView.inputIDTextView];
            
            if (_model.identifier && _model.identifier > 0) {
                _rootView.inputIDTextView.inputTextField.text = _model.identifier;
            }
            
            [_rootView.inputIDTextView.inputTextField becomeFirstResponder];
            _rootView.inputIDTextView.delegate = self;
            [_rootView.inputIDTextView moveToTargetPositon];
            break;
            
        case 103://上传手持身份证照
             NSLog(@"102");
        {
            _model.selectedImage = 0;
            UIActionSheet* actionSheet = [[UIActionSheet alloc]
                                          initWithTitle:nil
                                          delegate:self
                                          cancelButtonTitle:@"取消"
                                          destructiveButtonTitle:nil
                                          otherButtonTitles:@"照相机", @"图库", nil];
            
            [actionSheet showInView:self.view];
        }
            break;
            
        case 104://上传形象照
             NSLog(@"103");
        {
            _model.selectedImage = 1;
            UIActionSheet* actionSheet = [[UIActionSheet alloc]
                                          initWithTitle:nil
                                          delegate:self
                                          cancelButtonTitle:@"取消"
                                          destructiveButtonTitle:nil
                                          otherButtonTitles:@"照相机", @"图库", nil];
            
            [actionSheet showInView:self.view];
        }
            break;
            
        case 105://提交
            [self verifyInfo];
            break;

        default:
            break;
    }
}

- (void)inputTextViewCancelButtonClicked:(UIButton *)sender {
    switch (sender.tag) {
        case 105:
            NSLog(@"input Name");
            [_rootView.inputNameTextView.inputTextField resignFirstResponder];
            break;
            
        case 106:
            NSLog(@"input ID");
            [_rootView.inputIDTextView.inputTextField resignFirstResponder];
        default:
            break;
    }
}

- (void)inputTextViewConfirmButtonClickedWithData:(NSDictionary *)data {
    
    NSString *result = data[@"Result"];
    UIButton *sender = data[@"Button"];
    
    switch (sender.tag) {
        case 107:
            NSLog(@"inputName");
            [_rootView.inputNameTextView.inputTextField resignFirstResponder];
            _model.name = result;
            [_rootView.tableView reloadData];
            break;
            
        case 108:
            NSLog(@"input id");
            _model.identifier = result;
            [_rootView.inputIDTextView.inputTextField resignFirstResponder];
            [_rootView.tableView reloadData];
            break;
            
        default:
            break;
    }
}

/**
 *  选择性别
 */
//- (void)selectSexButtonClicked {
////    [_rootView.nameTextField resignFirstResponder];
//    [self.view addSubview:_rootView.sexPickerView];
//    _rootView.sexPickerView.delegate = self;
//    [_rootView.sexPickerView moveToTargetPositon];
//}

- (void)sexPickerViewConfirmButtonClicked:(NSString *)result {
//    [TesonAlertView showMsg:@"性别信息一旦提交，将无法更改" cancelDelay:3];
    _model.sex = result;
    NSLog(@"sexPickerViewConfirmButtonClicked result:%@", result);
    [_rootView.tableView reloadData];
}


- (void)verifyInfo {
    if (!_model.name) {
        [SVProgressHUD showErrorWithStatus:@"请输入姓名" duration:2];
        return;
    }

    if (_model.name.length < 2 || _model.name.length > 14) {
        [SVProgressHUD showErrorWithStatus:@"姓名长度不正确，请重新输入" duration:2];
        return;
    }
    
    if (!_model.sex || _model.sex.length == 0 ) {
        [SVProgressHUD showErrorWithStatus:@"请选择性别" duration:2];
        return;
    }
    
    if (!_model.identifier) {
        [SVProgressHUD showErrorWithStatus:@"请输入身份证号" duration:2];
        return;
    }
    
    if (_model.identifier.length != 18) {
        [SVProgressHUD showErrorWithStatus:@"请输入18位身份证号" duration:2];
        return;
    }
    
    if (!_model.idCardImage) {
        [SVProgressHUD showErrorWithStatus:@"请选择手持身份证图片" duration:2];
        return;
    }
    
    if (!_model.personalPortraitImage) {
        [SVProgressHUD showErrorWithStatus:@"请选择个人图片" duration:2];
        return;
    }
    
//    if (!_model.idCardImageObjectKey) {
//        [SVProgressHUD showErrorWithStatus:@"请重新上传手持身份证图片" duration:2];
//        return;
//    }
//    
//    if (!_model.personalPortraitImageObjectKey) {
//        [SVProgressHUD showErrorWithStatus:@"请重新上传个人图片" duration:2];
//        return;
//    }
    
//    [self commitIdentifierAuthentication];
    [SVProgressHUD showWithStatus:@"图片上传中..."];
    
    [self uploadIDCardImageToAli];
}

- (void)commitIdentifierAuthentication {
    NSString *sex;
    if ([_model.sex isEqualToString:@"男"]) {
        sex = @"1";
    } else {
        sex = @"0";
    }
    
    [SVProgressHUD showWithStatus:@"提交信息中..."];
    NSDictionary *parameters = @{@"action":@"applyAuth",
                                 @"sessionKey":[TDSingleton instance].sessionKey,
                                 @"realName":_model.name,
                                 @"sex":sex,
                                 @"ID_Number":_model.identifier,
                                 @"ID_Photo":_model.idCardImageObjectKey,
                                 @"photo":_model.personalPortraitImageObjectKey};
    
    [TDNetworking requestWithURL:[TDSingleton instance].gatewayServerBusinessURL
                      parameters:parameters
                         success:^(id responseObject)
     {
         NSLog(@"commitIdentifierAuthentication:%@", responseObject);
         if ([responseObject[@"result"] integerValue] == 1) {
             [SVProgressHUD dismissWithSuccess:@"提交成功！"];
             [TDSingleton instance].userInfoModel.authentication = [responseObject[@"authState"] integerValue];
             [[NSNotificationCenter defaultCenter] postNotificationName:TDIdentifierAuthenticationSuccess object:nil];
             [self.navigationController popViewControllerAnimated:YES];
         } else {
             [self resetUploadState];
             
             [SVProgressHUD dismissWithError:[NSString stringWithFormat:@"%@", responseObject[@"msg"]]];
         }
     } failure:^(NSError *error) {
         NSLog(@"error:%@", error);
         [self resetUploadState];
         
         [SVProgressHUD dismissWithError:[NSString stringWithFormat:@"%@", error]];
     }];
}

- (void)resetUploadState {
    _uploadIDCardSuccess = NO;
    _uploadPersonalImageSuccess = NO;
}

#pragma mark - UIActionSheetDelegate
//选择图片来源
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSLog(@"%ld", (long)buttonIndex);
    switch (buttonIndex) {
        case 0:
        {
            //状态栏字体颜色改为黑色
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
            
            UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
            imagePicker.delegate = self;
            imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;//照相机
            imagePicker.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
            imagePicker.allowsEditing = YES;
            
            [self presentViewController:imagePicker animated:YES completion:nil];
        }
            break;
            
        case 1:
        {
            //状态栏字体颜色改为黑色
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
            
            UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
            imagePicker.delegate = self;
            imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;//图库
            imagePicker.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
            imagePicker.allowsEditing = YES;
            
            [self presentViewController:imagePicker animated:YES completion:nil];
        }
            break;
    }
}

#pragma mark - UIImagePickerControllerDelegate
//完成选取
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    NSLog(@"Info:%@", info);
    
    //状态栏改为白色
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    if ([info[@"UIImagePickerControllerMediaType"] isEqualToString:@"public.image"]) {
        NSLog(@"");
        if (_model.selectedImage == 0) {
            
            _model.idCardImage = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
            
        } else {
            _model.personalPortraitImage = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        }
        
//        [self uploadImageToAli];
        
        [self.rootView.tableView reloadData];
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

// 取消选取
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    //状态栏改为白色
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    [picker dismissViewControllerAnimated:YES completion:^{
        NSLog(@"did cancel imagePickerController");
    }];
}

- (void)uploadIDCardImageToAli {
    
//    if (!_uploadIDCardSuccess) {
    
        NSString * date = [NSString stringWithFormat:@"%.f", round([[NSDate date] timeIntervalSince1970]*1000)];
        NSString * objectName = [[NSString stringWithFormat:@"%@%@", [TDSingleton instance].account, date].MD5String stringByAppendingString:@".png"];
        NSString * objectKey = [NSString stringWithFormat:@"%@/%@", @"auth", objectName];
        
        [LGDefineNetServer uploadImage:_model.idCardImage
                             objectKey:objectKey
                               account:[TDSingleton instance].account success:^
        {
            _model.idCardImageObjectKey = objectKey;
            _uploadIDCardSuccess = YES;
            
            [self uploadPersonalImageToAli];
        } failure:^(NSString *msg) {
            [SVProgressHUD dismissWithError:msg];
        } progress:^(NSString *progressMsg) {
            [SVProgressHUD showWithStatus:progressMsg maskType:SVProgressHUDMaskTypeClear];
        }];
//    }
}

- (void)uploadPersonalImageToAli {
//    if (!_uploadPersonalImageSuccess) {
    
        NSString * date = [NSString stringWithFormat:@"%.f", round([[NSDate date] timeIntervalSince1970]*1000)];
        NSString * objectName = [[NSString stringWithFormat:@"%@%@", [TDSingleton instance].account, date].MD5String stringByAppendingString:@".png"];
        NSString * objectKey = [NSString stringWithFormat:@"%@/%@", @"auth", objectName];
        
        [LGDefineNetServer uploadImage:_model.personalPortraitImage
                             objectKey:objectKey
                               account:[TDSingleton instance].account success:^
         {
             _model.personalPortraitImageObjectKey = objectKey;
             _uploadPersonalImageSuccess = YES;
             
             [self commitIdentifierAuthentication];
         } failure:^(NSString *msg) {
             _uploadIDCardSuccess = NO;
             
             [SVProgressHUD dismissWithError:msg];
         } progress:^(NSString *progressMsg) {
             [SVProgressHUD showWithStatus:progressMsg maskType:SVProgressHUDMaskTypeClear];
         }];
//    }
}

@end
