//
//  ButtonTableViewCell.h
//  AfterSchool
//
//  Created by Teson Draw on 1/15/16.
//  Copyright © 2016 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ButtonTableViewCell : UITableViewCell

@property (nonatomic, assign) CGFloat  heihgt;
@property (nonatomic, assign) CGFloat  buttonHeihgt;
@property (nonatomic, assign) CGFloat  buttonOrginX;

@property (nonatomic, strong) UIButton *button;

- (instancetype)initWithStyle:(UITableViewCellStyle)style
              reuseIdentifier:(NSString *)reuseIdentifier
              backgroundColor:(UIColor *)backgroundColor
                   cellHeight:(CGFloat)cellHeight
                 buttonHeight:(CGFloat)buttonHeight
                 buttonOrginX:(CGFloat)buttonOrginX
                  buttonTitle:(NSString *)buttonTitle
               buttonFontSize:(CGFloat)buttonFontSize
             buttonTitleColor:(UIColor *)buttonTitleColor
        buttonBackgroundColor:(UIColor *)buttonBackgroundColor;

@end
