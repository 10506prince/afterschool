//
//  TextDetailRightArrowTableViewCell.m
//  AfterSchool
//
//  Created by Teson Draw on 1/15/16.
//  Copyright © 2016 Teson Draw. All rights reserved.
//

#import "TextDetailRightArrowTableViewCell.h"
#import "MacrosDefinition.h"
#import "TDPathRef.h"

@implementation TextDetailRightArrowTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style
              reuseIdentifier:(NSString *)reuseIdentifier
                   cellHeight:(CGFloat)cellHeight
                    textColor:(UIColor *)textColor
                 textFontSize:(CGFloat)textFontSize
                  detailColor:(UIColor *)detailColor
               detailFontSize:(CGFloat)detailFontSize
             showDividingLine:(BOOL)showDividingLine
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
//        self.backgroundColor = [UIColor colorWithRed:40/255.f green:40/255.f blue:40/255.f alpha:1];
//        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.height = cellHeight;
        
        [self addSubview:self.itemLabel];
        
        if (textColor) {
            self.itemLabel.textColor = textColor;
        }
        
        if (textFontSize > 1) {
            self.itemLabel.font = [UIFont systemFontOfSize:textFontSize];
        }
        
        [self addSubview:self.detailLabel];
        
        if (detailColor) {
            self.detailLabel.textColor = detailColor;
        }
        
        if (detailFontSize > 1) {
            self.detailLabel.font = [UIFont systemFontOfSize:detailFontSize];
        }
        
        if (showDividingLine) {
            [self addSubview:self.dividingLineView];
        }
        
        [self.layer addSublayer:self.rightArrowLayer];
        [self addSubview:self.button];
    }
    
    return self;
}

//- (instancetype)initWithFrame:(CGRect)frame
//                    textColor:(UIColor *)textColor
//                 textFontSize:(CGFloat)textFontSize
//                  detailColor:(UIColor *)detailColor
//               detailFontSize:(CGFloat)detailFontSize
//             showDividingLine:(BOOL)showDividingLine
//{
//    self = [super initWithFrame:frame];
//    if (self) {
//        
//    }
//    return self;
//}

- (UILabel *)itemLabel {
    if (!_itemLabel) {
        _itemLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, 180, self.height)];
        _itemLabel.textColor = [UIColor blackColor];
        _itemLabel.font = [UIFont systemFontOfSize:16];
    }
    return _itemLabel;
}

- (UILabel *)detailLabel {
    if (!_detailLabel) {
        _detailLabel = [[UILabel alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - 20 - 12 - 180, 0, 180, self.height)];
        _detailLabel.textColor = [UIColor colorWithRed:153/255.f green:153/255.f blue:153/255.f alpha:1];
        _detailLabel.font = [UIFont systemFontOfSize:14];
        _detailLabel.textAlignment = NSTextAlignmentRight;
    }
    return _detailLabel;
}

- (CAShapeLayer *)rightArrowLayer {
    if (!_rightArrowLayer) {
        _rightArrowLayer = [CAShapeLayer layer];
        _rightArrowLayer.lineWidth = 2;
        _rightArrowLayer.fillColor = [UIColor clearColor].CGColor;
        _rightArrowLayer.strokeColor = [UIColor lightGrayColor].CGColor;
        
        CGFloat width = 6;
        CGFloat height = 12;
        CGFloat pointX = SCREEN_WIDTH - 20 - width;
        CGFloat pointY = (self.height - height) / 2;
        CGPoint start = CGPointMake(pointX, pointY);
        
        _rightArrowLayer.path = [TDPathRef rightArrowWithStart:start width:width height:height];
    }
    
    return _rightArrowLayer;
}

- (UIView *)dividingLineView {
    if (!_dividingLineView) {
        _dividingLineView = [[UIView alloc] initWithFrame:CGRectMake(20, self.height - 0.5, SCREEN_WIDTH - 40, 0.5)];
        _dividingLineView.backgroundColor = [UIColor lightGrayColor];
    }
    return _dividingLineView;
}

- (UIButton *)button {
    if (!_button) {
        _button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, self.height)];
    }
    
    return _button;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
