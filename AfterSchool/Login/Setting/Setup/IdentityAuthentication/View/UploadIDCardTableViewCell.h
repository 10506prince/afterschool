//
//  UploadIDCardTableViewCell.h
//  AfterSchool
//
//  Created by Teson Draw on 1/15/16.
//  Copyright © 2016 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UploadIDCardTableViewCell : UITableViewCell

//@property (nonatomic, assign) CGFloat  heihgt;

@property (nonatomic, strong) UILabel *IDCardTitleLabel;
@property (nonatomic, strong) UILabel *IDCardTipLabel;
@property (nonatomic, strong) UILabel *personalImageTitleLabel;

@property (nonatomic, strong) UITextView *imageFormatDescriptionTextView;

//@property (nonatomic, strong) UIImageView *IDCardImageView;
//@property (nonatomic, strong) UIImageView *personalPortraitImageView;

@property (nonatomic, strong) UIButton *uploadIDCardButton;
@property (nonatomic, strong) UIButton *uploadPersonalPortraitButton;


- (instancetype)initWithStyle:(UITableViewCellStyle)style
              reuseIdentifier:(NSString *)reuseIdentifier
              backgroundColor:(UIColor *)backgroundColor;

@end
