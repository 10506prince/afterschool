//
//  UploadIDCardTableViewCell.m
//  AfterSchool
//
//  Created by Teson Draw on 1/15/16.
//  Copyright © 2016 Teson Draw. All rights reserved.
//

#import "UploadIDCardTableViewCell.h"
#import "MacrosDefinition.h"

@implementation UploadIDCardTableViewCell


- (instancetype)initWithStyle:(UITableViewCellStyle)style
              reuseIdentifier:(NSString *)reuseIdentifier
              backgroundColor:(UIColor *)backgroundColor {

    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        if (backgroundColor) {
            self.backgroundColor = backgroundColor;
        } else {
            self.backgroundColor = [UIColor whiteColor];
        }
        
        [self addSubview:self.IDCardTitleLabel];
        [self addSubview:self.IDCardTipLabel];
        [self addSubview:self.personalImageTitleLabel];
        
        [self addSubview:self.uploadIDCardButton];
        [self addSubview:self.uploadPersonalPortraitButton];
        
        [self addSubview:self.imageFormatDescriptionTextView];
    }
    
    return self;
}

- (UILabel *)IDCardTitleLabel {
    if (!_IDCardTitleLabel) {
        _IDCardTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 20, SCREEN_WIDTH / 2, 16)];
        _IDCardTitleLabel.textColor = [UIColor colorWithRed:56/255.f green:56/255.f blue:56/255.f alpha:1];
        _IDCardTitleLabel.font = [UIFont systemFontOfSize:16];
        _IDCardTitleLabel.text = @"手持身份证正面照";
        _IDCardTitleLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _IDCardTitleLabel;
}

- (UILabel *)IDCardTipLabel {
    if (!_IDCardTipLabel) {
        _IDCardTipLabel = [[UILabel alloc] initWithFrame:CGRectMake(((SCREEN_WIDTH / 2) - 130) / 2, CGRectGetMaxY(_IDCardTitleLabel.frame) + 6, 130, 32)];
        _IDCardTipLabel.textColor = [UIColor colorWithRed:56/255.f green:56/255.f blue:56/255.f alpha:1];
        _IDCardTipLabel.font = [UIFont systemFontOfSize:13];
        _IDCardTipLabel.text = @"（素颜的五官显示  身份证内容清晰可见）";
        _IDCardTipLabel.numberOfLines = 0;
    }
    return _IDCardTipLabel;
}

- (UILabel *)personalImageTitleLabel {
    if (!_personalImageTitleLabel) {
        _personalImageTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(SCREEN_WIDTH / 2, 20, SCREEN_WIDTH / 2, 16)];
        _personalImageTitleLabel.textColor = [UIColor colorWithRed:56/255.f green:56/255.f blue:56/255.f alpha:1];
        _personalImageTitleLabel.font = [UIFont systemFontOfSize:16];
        _personalImageTitleLabel.text = @"本人形象照";
        _personalImageTitleLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _personalImageTitleLabel;
}

//- (UIImageView *)IDCardImageView {
//    if (!_IDCardImageView) {
//        _IDCardImageView = [[UIImageView alloc] initWithFrame:CGRectMake((SCREEN_WIDTH / 2 - 80) / 2, CGRectGetMaxY(_IDCardTitleLabel.frame) + 40, 80, 80)];
//        
//        _IDCardImageView.layer.borderColor = [UIColor blackColor].CGColor;
//        _IDCardImageView.layer.borderWidth = 1;
//    }
//    return _IDCardImageView;
//}

- (UIButton *)uploadIDCardButton {
    if (!_uploadIDCardButton) {
        _uploadIDCardButton = [[UIButton alloc] initWithFrame:CGRectMake((SCREEN_WIDTH / 2 - 80) / 2, CGRectGetMaxY(_IDCardTitleLabel.frame) + 51, 80, 80)];
        [_uploadIDCardButton setImage:[UIImage imageNamed:@"upload_image_button"] forState:UIControlStateNormal];
    }
    
    return _uploadIDCardButton;
}

//- (UIImageView *)personalPortraitImageView {
//    if (!_personalPortraitImageView) {
//        _personalPortraitImageView = [[UIImageView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH / 2 + (SCREEN_WIDTH / 2 - 80) / 2, CGRectGetMaxY(_personalImageTitleLabel.frame) + 40, 80, 80)];
//        
//        _personalPortraitImageView.layer.borderColor = [UIColor blackColor].CGColor;
//        _personalPortraitImageView.layer.borderWidth = 1;
//    }
//    return _personalPortraitImageView;
//}

- (UIButton *)uploadPersonalPortraitButton {
    if (!_uploadPersonalPortraitButton) {
        _uploadPersonalPortraitButton = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_WIDTH / 2 + (SCREEN_WIDTH / 2 - 80) / 2, CGRectGetMaxY(_personalImageTitleLabel.frame) + 51, 80, 80)];
        [_uploadPersonalPortraitButton setImage:[UIImage imageNamed:@"upload_image_button"] forState:UIControlStateNormal];
    }
    
    return _uploadPersonalPortraitButton;
}

- (UITextView *)imageFormatDescriptionTextView {
    if (!_imageFormatDescriptionTextView) {
        _imageFormatDescriptionTextView = [[UITextView alloc] initWithFrame:CGRectMake(14, CGRectGetMaxY(_uploadIDCardButton.frame) + 6, SCREEN_WIDTH - 28, 60)];
        
        _imageFormatDescriptionTextView.textColor = [UIColor colorWithRed:153/255.f green:153/255.f blue:153/255.f alpha:1];
        _imageFormatDescriptionTextView.backgroundColor = [UIColor clearColor];
        
        _imageFormatDescriptionTextView.text = @"1、身份证上的信息不能被遮挡，切清晰可见\n2、照片请勿进行任何软件处理\n3、身份证有效期需要在1个月以上";
        _imageFormatDescriptionTextView.editable = NO;
    }
    
    return _imageFormatDescriptionTextView;
}


- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
