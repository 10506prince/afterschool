//
//  ButtonTableViewCell.m
//  AfterSchool
//
//  Created by Teson Draw on 1/15/16.
//  Copyright © 2016 Teson Draw. All rights reserved.
//

#import "ButtonTableViewCell.h"
#import "UIButton+SetBackgroundColor.h"
#import "MacrosDefinition.h"

@implementation ButtonTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style
              reuseIdentifier:(NSString *)reuseIdentifier
              backgroundColor:(UIColor *)backgroundColor
                   cellHeight:(CGFloat)cellHeight
                 buttonHeight:(CGFloat)buttonHeight
                 buttonOrginX:(CGFloat)buttonOrginX
                  buttonTitle:(NSString *)buttonTitle
               buttonFontSize:(CGFloat)buttonFontSize
             buttonTitleColor:(UIColor *)buttonTitleColor
        buttonBackgroundColor:(UIColor *)buttonBackgroundColor
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        self.heihgt       = cellHeight;
        self.buttonHeihgt = buttonHeight;
        self.buttonOrginX = buttonOrginX;
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        if (backgroundColor) {
            self.backgroundColor = backgroundColor;
        } else {
            self.backgroundColor = [UIColor colorWithRed:230/255.f green:230/255.f blue:230/255.f alpha:1];
        }
    
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [self addSubview:self.button];
        
        if (buttonTitle) {
            [_button setTitle:buttonTitle forState:UIControlStateNormal];
        }
        
        if (buttonTitleColor) {
            [_button setTitleColor:buttonTitleColor forState:UIControlStateNormal];
        }
        
        if (buttonBackgroundColor) {
            [_button setBackgroundColor:buttonBackgroundColor];
        }
        
        if (buttonFontSize > 1) {
            _button.titleLabel.font = [UIFont systemFontOfSize:buttonFontSize];
        }
    }
    return self;
}

- (UIButton *)button {
    if (!_button) {
        _button = [[UIButton alloc] initWithFrame:CGRectMake(self.buttonOrginX, (self.heihgt - self.buttonHeihgt) / 2, SCREEN_WIDTH - 2 * self.buttonOrginX, self.buttonHeihgt)];
        
        [_button setBackgroundColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
        [_button setBackgroundColor:[UIColor colorWithRed:38/255.f green:166/255.f blue:154/255.0 alpha:1]];
        [_button setBackgroundColor:[UIColor lightTextColor] forState:UIControlStateDisabled];
        
        _button.layer.cornerRadius = 5;
        _button.layer.masksToBounds = YES;
        _button.titleLabel.font = [UIFont systemFontOfSize:19];
    }
    
    return _button;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
