//
//  TextDetailRightArrowTableViewCell.h
//  AfterSchool
//
//  Created by Teson Draw on 1/15/16.
//  Copyright © 2016 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TextDetailRightArrowTableViewCell : UITableViewCell

@property (nonatomic, strong) UILabel *itemLabel;
@property (nonatomic, strong) UILabel *detailLabel;

@property (nonatomic, strong) CAShapeLayer *rightArrowLayer;

@property (nonatomic, strong) UIView *dividingLineView;

@property (nonatomic, assign) CGFloat height;

@property (nonatomic, strong) UIButton *button;

- (instancetype)initWithStyle:(UITableViewCellStyle)style
              reuseIdentifier:(NSString *)reuseIdentifier
                   cellHeight:(CGFloat)cellHeight
                    textColor:(UIColor *)textColor
                 textFontSize:(CGFloat)textFontSize
                  detailColor:(UIColor *)detailColor
               detailFontSize:(CGFloat)detailFontSize
             showDividingLine:(BOOL)showDividingLine;

@end
