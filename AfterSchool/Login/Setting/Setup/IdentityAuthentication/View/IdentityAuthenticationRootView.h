//
//  IdentityAuthenticationRootView.h
//  AfterSchool
//
//  Created by Teson Draw on 1/15/16.
//  Copyright © 2016 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavigationBarView.h"
#import "TDInputTextView.h"
#import "TDSexPickerView.h"

@interface IdentityAuthenticationRootView : UIView

@property (nonatomic, strong) NavigationBarView *navigationBarView;
@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) TDInputTextView *inputNameTextView;
@property (nonatomic, strong) TDInputTextView *inputIDTextView;

@property (nonatomic, strong) TDSexPickerView  *sexPickerView;

@end
