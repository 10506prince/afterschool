//
//  IdentityAuthenticationRootView.m
//  AfterSchool
//
//  Created by Teson Draw on 1/15/16.
//  Copyright © 2016 Teson Draw. All rights reserved.
//

#import "IdentityAuthenticationRootView.h"
#import "MacrosDefinition.h"
#import "LGUserCenterTableViewCellTwo.h"

@implementation IdentityAuthenticationRootView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithRed:230/255.f green:230/255.f blue:230/255.f alpha:1];
        
        [self addSubview:self.navigationBarView];
        [self addSubview:self.tableView];
    }
    
    return self;
}

- (NavigationBarView *)navigationBarView {
    if (!_navigationBarView) {
        
        _navigationBarView = [[NavigationBarView alloc] initWithTitle:@"身份认证"
                                                           titleColor:nil
                                                      backgroundColor:nil
                                              leftButtonBgViewOriginX:0
                                                 leftButtonImageWidth:22
                                                 leftButtonTitleColor:[UIColor whiteColor]
                                                       leftButtonName:@"返回"
                                                  leftButtonImageName:@"navigation_bar_return_button_left_arrow"
                                                      rightButtonName:nil
                                                 rightButtonImageName:nil];
        
    }
    return _navigationBarView;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 64, self.bounds.size.width, self.bounds.size.height - 64)];
        _tableView.backgroundColor = [UIColor colorWithRed:230/255.f green:230/255.f blue:230/255.f alpha:1];
        _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        _tableView.separatorStyle = UITableViewCellSelectionStyleNone;
    }
    return _tableView;
}

- (TDInputTextView *)inputNameTextView {
    if (!_inputNameTextView) {
        _inputNameTextView = [[TDInputTextView alloc] initWithFrame:CGRectMake((SCREEN_WIDTH - 300) / 2, -144, 300, 144) targetPosition:CGPointMake((SCREEN_WIDTH - 300) / 2, 64)];
        _inputNameTextView.titleLabel.text = @"输入姓名";
        _inputNameTextView.cancelButton.tag = 105;
        _inputNameTextView.confirmButton.tag = 107;
    }
    return _inputNameTextView;
}

- (TDInputTextView *)inputIDTextView {
    if (!_inputIDTextView) {
        _inputIDTextView = [[TDInputTextView alloc] initWithFrame:CGRectMake((SCREEN_WIDTH - 300) / 2, -144, 300, 144) targetPosition:CGPointMake((SCREEN_WIDTH - 300) / 2, 64)];
        _inputIDTextView.titleLabel.text = @"输入身份证号";
        _inputIDTextView.cancelButton.tag = 106;
        _inputIDTextView.confirmButton.tag = 108;
    }
    return _inputIDTextView;
}

- (TDSexPickerView *)sexPickerView {
    if (!_sexPickerView) {
        _sexPickerView = [[TDSexPickerView alloc] initWithFrame:CGRectMake((SCREEN_WIDTH - 300) / 2, SCREEN_HEIGHT, 300, 244) targetPosition:CGPointMake((SCREEN_WIDTH - 300) / 2, SCREEN_HEIGHT - 300)];
    }
    return _sexPickerView;
}

@end
