//
//  BackgroundImageCollectionViewCell.m
//  AfterSchool
//
//  Created by Teson Draw on 12/2/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "BackgroundImageCollectionViewCell.h"

@implementation BackgroundImageCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.layer.borderColor = [UIColor blackColor].CGColor;
        self.layer.borderWidth = 1;
        self.layer.shadowOffset = CGSizeMake(2, 3);
        self.layer.shadowOpacity = 0.3;
        
        [self addSubview:self.imageView];
        [self addSubview:self.nameLabel];
    }
    return self;
}

- (UIImageView *)imageView {
    if (!_imageView) {
        _imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 80, 80)];
    }
    return _imageView;
}

- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_imageView.frame), self.bounds.size.width, self.bounds.size.height - CGRectGetMaxY(_imageView.frame))];
        _nameLabel.backgroundColor = [UIColor clearColor];
        _nameLabel.font = [UIFont systemFontOfSize:12];
        _nameLabel.textColor = [UIColor blackColor];
        _nameLabel.numberOfLines = 0;
        _nameLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _nameLabel;
}

- (void)setSelected:(BOOL)selected {
    if (selected) {
        self.layer.borderColor = [UIColor whiteColor].CGColor;
        self.layer.borderWidth = 1;
        self.nameLabel.textColor = [UIColor whiteColor];
    } else {
        self.layer.borderColor = [UIColor blackColor].CGColor;
        self.layer.borderWidth = 1;
        self.nameLabel.textColor = [UIColor blackColor];
    }
}

@end
