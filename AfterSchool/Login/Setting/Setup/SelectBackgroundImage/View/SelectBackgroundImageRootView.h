//
//  SelectBackgroundImageRootView.h
//  AfterSchool
//
//  Created by Teson Draw on 12/2/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavigationBarView.h"

@interface SelectBackgroundImageRootView : UIView

@property (nonatomic, strong) NavigationBarView * navigationBarView;
@property (nonatomic, strong) UICollectionView *collectionView;

@end
