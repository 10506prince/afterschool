//
//  SelectBackgroundImageRootView.m
//  AfterSchool
//
//  Created by Teson Draw on 12/2/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "SelectBackgroundImageRootView.h"
#import "BackgroundImageCollectionViewCell.h"


@implementation SelectBackgroundImageRootView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self addSubview:self.navigationBarView];
        [self addSubview:self.collectionView];
    }
    return self;
}

- (NavigationBarView *)navigationBarView {
    if (!_navigationBarView) {
        _navigationBarView = [[NavigationBarView alloc] initWithTitle:@"聊天背景"
                                                           titleColor:nil
                                                      backgroundColor:nil
                                              leftButtonBgViewOriginX:0
                                                 leftButtonImageWidth:22
                                                 leftButtonTitleColor:[UIColor whiteColor]
                                                       leftButtonName:@"返回"
                                                  leftButtonImageName:nil
                                                      rightButtonName:@"保存"
                                                 rightButtonImageName:nil];
        
        
    }
    return _navigationBarView;
}

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
        flowLayout.headerReferenceSize = CGSizeMake(self.bounds.size.width - 20, 44);
        
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 64, self.bounds.size.width, self.bounds.size.height - 64) collectionViewLayout:flowLayout];
        [_collectionView registerClass:[BackgroundImageCollectionViewCell class] forCellWithReuseIdentifier:@"reuseIdentifier"];
        _collectionView.backgroundColor = [UIColor colorWithRed:120/255.f green:120/255.f blue:120/255.f alpha:1];
        
//        _collectionView.layer.borderColor = [UIColor redColor].CGColor;
//        _collectionView.layer.borderWidth = 1;
    }
    return _collectionView;
}


@end
