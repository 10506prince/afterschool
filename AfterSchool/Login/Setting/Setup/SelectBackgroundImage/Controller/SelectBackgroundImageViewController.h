//
//  SelectBackgroundImageViewController.h
//  AfterSchool
//
//  Created by Teson Draw on 12/2/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SelectBackgroundImageRootView.h"
#import "BackgroundImageCollectionViewCell.h"

//typedef void (^ReturnTextBlock)(NSString *showText);

@interface SelectBackgroundImageViewController : UIViewController

@property (nonatomic, strong) SelectBackgroundImageRootView *rootView;
@property (nonatomic, strong) NSArray *dataSource;

//@property (nonatomic, copy) ReturnTextBlock returnTextBlock;
@property (nonatomic, strong) NSString *backgroundImageName;

@property (nonatomic, strong) BackgroundImageCollectionViewCell *cell;

@property (nonatomic, assign) NSInteger imageNumber;

//- (void)returnText:(ReturnTextBlock)block;

@end
