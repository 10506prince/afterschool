//
//  SelectBackgroundImageViewController.m
//  AfterSchool
//
//  Created by Teson Draw on 12/2/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "SelectBackgroundImageViewController.h"

#import "MacrosDefinition.h"
#import "TDSingleton.h"

@interface SelectBackgroundImageViewController () <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

@end

@implementation SelectBackgroundImageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initData];
    [self initUserInterface];
}

-(void)initData {
    _imageNumber = [TDSingleton instance].chatBackgroundImageNumber;
    NSLog(@"[TDSingleton instance].chatBackgroundImageNumber:%ld", (long)_imageNumber);
    _dataSource = @[@[@"秋天的田野", @"mimi_take_picture"], @[@"纪念碑谷", @"monument_valley"]];
}

- (void)initUserInterface {
    [self.view addSubview:self.rootView];
}

- (SelectBackgroundImageRootView *)rootView {
    if (!_rootView) {
        _rootView = [[SelectBackgroundImageRootView alloc] initWithFrame:self.view.bounds];
        
        [_rootView.navigationBarView.leftButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        [_rootView.navigationBarView.rightButton addTarget:self action:@selector(saveAction) forControlEvents:UIControlEventTouchUpInside];
        
        _rootView.collectionView.dataSource = self;
        _rootView.collectionView.delegate = self;
    }
    
    return _rootView;
}

- (void)backAction {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)saveAction {
    [self backAction];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

#pragma mark UICollectionView Protocol Method
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _dataSource.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    BackgroundImageCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"reuseIdentifier" forIndexPath:indexPath];
    
    cell.nameLabel.text = _dataSource[indexPath.row][0];
    cell.imageView.image = [UIImage imageNamed:_dataSource[indexPath.row][1]];
    
    if (indexPath.row == [TDSingleton instance].chatBackgroundImageNumber) {
        cell.selected = YES;
    }
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    _backgroundImageName = _dataSource[indexPath.row][0];
    
    [TDSingleton instance].chatBackgroundImageNumber = indexPath.row;
    [TDSingleton instance].chatBackgroundImageName = _dataSource[indexPath.row][0];
    
    [[TDSingleton instance].settingDictionary setValue:[NSNumber numberWithInteger:[TDSingleton instance].chatBackgroundImageNumber] forKey:@"ChatBackgroundImageNumber"];
    [[TDSingleton instance].settingDictionary setValue:[TDSingleton instance].chatBackgroundImageName forKey:@"ChatBackgroundImageName"];
    
    [[TDSingleton instance].settingDictionary writeToFile:[TDSingleton instance].settingPlistFileName atomically:YES];
    
    [collectionView reloadData];
}

#pragma mark UICollectionViewFlowLayout Protocol Method
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    return CGSizeMake(0, 14);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section {
    return CGSizeMake(SCREEN_WIDTH, 10);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(80, 100);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 20, 0, 20);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 6;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 3;
}

//- (void)returnText:(ReturnTextBlock)block {
//    self.returnTextBlock = block;
//}
//
//- (void)viewWillDisappear:(BOOL)animated {
//    
//    if (_backgroundImageName) {
//        self.returnTextBlock(_backgroundImageName);
//    }
//}


@end
