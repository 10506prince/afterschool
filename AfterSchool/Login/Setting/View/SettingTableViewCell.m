//
//  SettingTableViewCell.m
//  AfterSchool
//
//  Created by Teson Draw on 1/5/16.
//  Copyright © 2016 Teson Draw. All rights reserved.
//

#import "SettingTableViewCell.h"

@implementation SettingTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor colorWithRed:40/255.f green:40/255.f blue:40/255.f alpha:1];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [self addSubview:self.markImageView];
        [self addSubview:self.titleLabel];
        [self addSubview:self.separatorLineView];
    }
    return self;
}

- (UIImageView *)markImageView {
    if (!_markImageView) {
        _markImageView = [[UIImageView alloc] initWithFrame:CGRectMake(20, 15, 24, 24)];
    }
    return _markImageView;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_markImageView.frame) + 10, 20, 100, 16)];
        _titleLabel.textColor = [UIColor whiteColor];
        _titleLabel.font = [UIFont systemFontOfSize:16];
    }
    return _titleLabel;
}

- (UIView *)separatorLineView {
    if (!_separatorLineView) {
        _separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(20, 54 - 1, self.bounds.size.width - 20, 1)];
        _separatorLineView.backgroundColor = [UIColor colorWithRed:56/255.f green:56/255.f blue:56/255.f alpha:1];
    }
    return _separatorLineView;
}

@end
