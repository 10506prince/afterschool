//
//  SettingTableViewCell.h
//  AfterSchool
//
//  Created by Teson Draw on 1/5/16.
//  Copyright © 2016 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingTableViewCell : UITableViewCell

@property (nonatomic, strong) UIImageView *markImageView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIView *separatorLineView;

@end
