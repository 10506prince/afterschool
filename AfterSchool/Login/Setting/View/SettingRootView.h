//
//  SettingRootView.h
//  AfterSchool
//
//  Created by Teson Draw on 12/29/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavigationBarView.h"

@interface SettingRootView : UIView

@property (nonatomic, strong) UIView *portraitBackgroundView;
@property (nonatomic, strong) UIView *borderView;

@property (nonatomic, strong) UIImageView *portraitImageView;
@property (nonatomic, strong) UIImageView *rightArrowImageView;

@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *accountLabel;
@property (nonatomic, strong) UILabel *signatureLabel;

@property (nonatomic, strong) UIButton *portraitButton;

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) UISwipeGestureRecognizer *swipeGesture;

@end
