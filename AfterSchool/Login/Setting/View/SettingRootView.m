//
//  SettingRootView.m
//  AfterSchool
//
//  Created by Teson Draw on 12/29/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "SettingRootView.h"
#import "MacrosDefinition.h"

@implementation SettingRootView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithRed:240/255.f green:239/255.f blue:244/255.f alpha:1];
        [self addSubview:self.portraitBackgroundView];
        [_portraitBackgroundView addSubview:self.borderView];
        [_borderView addSubview:self.portraitImageView];
        [_borderView addSubview:self.portraitButton];
        
        [_portraitBackgroundView addSubview:self.nameLabel];
        [_portraitBackgroundView addSubview:self.accountLabel];
        [_portraitBackgroundView addSubview:self.signatureLabel];
        
        [self addSubview:self.tableView];
        [self addGestureRecognizer:self.swipeGesture];
    }
    return self;
}

- (UIView *)portraitBackgroundView {
    if (!_portraitBackgroundView) {
        _portraitBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 170)];
        _portraitBackgroundView.backgroundColor = COLOR(29, 122, 113);
    }
    return _portraitBackgroundView;
}

- (UIView *)borderView {
    if (!_borderView) {
        _borderView = [[UIView alloc] initWithFrame:CGRectMake(20, 55.5, 50, 50)];
        _borderView.backgroundColor = [UIColor whiteColor];
        _borderView.layer.cornerRadius = 5;
    }
    return _borderView;
}

- (UIImageView *)portraitImageView {
    if (!_portraitImageView) {
        _portraitImageView = [[UIImageView alloc] initWithFrame:CGRectMake(2.5, 2.5, 45, 45)];
        _portraitImageView.layer.cornerRadius = 4;
    }
    return _portraitImageView;
}

- (UIButton *)portraitButton {
    if (!_portraitButton) {
        _portraitButton = [[UIButton alloc] initWithFrame:_borderView.bounds];
    }
    
    return _portraitButton;
}

- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_borderView.frame) + 10, _borderView.frame.origin.y + 6, SCREEN_WIDTH - CGRectGetMaxX(_borderView.frame) - 40, 16)];
        _nameLabel.textColor = [UIColor whiteColor];
        _nameLabel.font = [UIFont systemFontOfSize:14];
        _nameLabel.text = @"大力神YouWin";
    }
    return _nameLabel;
}

- (UILabel *)accountLabel {
    if (!_accountLabel) {
        _accountLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_borderView.frame) + 10, CGRectGetMaxY(_nameLabel.frame) + 8, SCREEN_WIDTH - CGRectGetMaxX(_borderView.frame) - 40, 16)];
        _accountLabel.textColor = [UIColor colorWithRed:102/255.f green:204/255.f blue:194/255.f alpha:1];
        _accountLabel.font = [UIFont systemFontOfSize:14];
        _accountLabel.text = @"注册账号：13980557234";
    }
    return _accountLabel;
}

- (UILabel *)signatureLabel {
    if (!_signatureLabel) {
        _signatureLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, CGRectGetMaxY(_borderView.frame) + 24, SCREEN_WIDTH - 40, 16)];
        _signatureLabel.textColor = [UIColor whiteColor];
        _signatureLabel.font = [UIFont systemFontOfSize:15];
        _signatureLabel.text = @"“南来北往的都在梦里等待风华记忆”";
    }
    return _signatureLabel;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_portraitBackgroundView.frame), SCREEN_WIDTH, SCREEN_HEIGHT - CGRectGetMaxY(_portraitBackgroundView.frame))];
        _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        _tableView.backgroundColor = [UIColor colorWithRed:40/255.f green:40/255.f blue:40/255.f alpha:1];
        _tableView.separatorStyle = UITableViewCellSelectionStyleNone;
    }
    return _tableView;
}


- (UISwipeGestureRecognizer *)swipeGesture {
    if (!_swipeGesture) {
        _swipeGesture = [[UISwipeGestureRecognizer alloc] init];
//        _swipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:nil];
        [_swipeGesture setDirection:UISwipeGestureRecognizerDirectionLeft];
    }
    return _swipeGesture;
}

//UISwipeGestureRecognizer *tapGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureAction:)];


@end
