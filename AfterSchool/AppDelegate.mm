                                                                                                           //
//  AppDelegate.m
//  AfterSchool
//
//  Created by Teson Draw on 9/30/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "AppDelegate.h"
#import <BaiduMapAPI_Map/BMKMapView.h>
#import <ShareSDK/ShareSDK.h>
#import "TalkingData.h"

#import "WeiboSDK.h"
#import "WXApi.h"
#import <TencentOpenAPI/TencentOAuth.h>
#import <TencentOpenAPI/QQApiInterface.h>

#import "SelectEntranceViewController.h"

#import "TDSingleton.h"
//#import "APService.h"

#import "JPUSHService.h"

//#import <RongIMKit/RongIMKit.h>
#import "AFNetworking.h"
#import "AFNetworkActivityIndicatorManager.h"
#import "ServerInterface.h"

#import "LGTabBarController.h"
#import "LGNavigationController.h"
#import "LoginViewController.h"
//#import "BigShotViewController.h"

#import <MAMapKit/MAMapKit.h>
#import "NSString+JSON.h"
#import "Toast+UIView.h"

#import "TDOrderEvaluationView.h"
#import "TDNetworking.h"
#import "SVProgressHUD.h"
#import "NotificationName.h"

#import "LGLocationService.h"

#import "Pingpp.h"
#import "WXApiManager.h"

#import "SettingViewController.h"

#import "TDOrderMessageBox.h"

#import "LGDefineNetServer.h"
#import "TDNetworkRequest.h"

#define RONGCLOUD_IM_APPKEY @"8brlm7ufrgov3"//测试环境：8luwapkvuc6gl    //生产环境：8brlm7ufrgov3

@interface AppDelegate () <RCIMConnectionStatusDelegate, RCIMUserInfoDataSource, TDOrderEvaluationViewDelegate, TDOrderMessageBoxDelegate>
{
    UINavigationController * _navigationController;
    BOOL bmkGetNetwork;
    BOOL bmkGetPermission;
    BMKMapManager* _mapManager;
}
@end



@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

    [self initData];
    
    [self initPushServiceWithOptions:launchOptions];
    
    /**
     *  初始化百度地图
     */
    [self initBaiduMap];
    
    /**
     *  初始化高德地图
     */
    [self initAMap];
    
    [self initRongCloudIM];
    
    [self initPingpp];
    
    [self initShareSdk];
    
    [self initTalkData];
    
    [self initNetworkActivity];
    
    [self initReachability];
    
    [self initWindow];
    
    return YES;
}

- (void)initTalkData {
    [TalkingData sessionStarted:@"21E30F738B730891760AF195ADB91A7C" withChannelId:@"iOS-fangxuehou"];
}

- (void)initData {
    [TDSingleton instance].isLogin = NO;
    [TDSingleton instance].hasGetPushID = NO;
    [TDSingleton initUserInfo];

    [self readSettingData];
    
    [TDSingleton initSettingWithData:[TDSingleton instance].settingDictionary];
    
    [TDSingleton instance].URL = [[TDURL alloc] initBaseURL];
}

- (void)readSettingData {
    /**
     *  读取App的参数配置，如果沙盒不存在配置文件，就把包里的文件写到沙盒中，如果沙盒有配置文件，则直接读取
     */
    //获取沙盒路径
    NSArray *sandboxPathArray = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    [TDSingleton instance].settingPlistFileName = [[sandboxPathArray objectAtIndex:0] stringByAppendingPathComponent:@"Setting.plist"];
    NSLog(@"NSDocumentDirectory:%@", [TDSingleton instance].settingPlistFileName);
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL fileExists = [fileManager fileExistsAtPath:[TDSingleton instance].settingPlistFileName];
    
    if (fileExists) {
        [TDSingleton instance].settingDictionary = [[NSMutableDictionary alloc] initWithContentsOfFile:[TDSingleton instance].settingPlistFileName];
        NSLog(@"fileExists settingDictionary:%@", [TDSingleton instance].settingDictionary);
    } else {
        NSString *plistFilePathInProject = [[NSBundle mainBundle] pathForResource:@"Setting" ofType:@"plist"];
        NSError *error;
        if ([fileManager copyItemAtPath:plistFilePathInProject toPath:[TDSingleton instance].settingPlistFileName error:&error]) {
            NSLog(@"copy file success!");
            //拷贝完成后，读取数据
            [TDSingleton instance].settingDictionary = [[NSMutableDictionary alloc] initWithContentsOfFile:[TDSingleton instance].settingPlistFileName];
            
            NSLog(@"file not Exists settingDictionary:%@", [TDSingleton instance].settingDictionary);
        } else {
            NSLog(@"copy file failed! \n%@", error);
        }
    }
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    NSLogSelfMethodName;
    [BMKMapView willBackGround];//当应用即将后台时调用，停止一切调用opengl相关的操作
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    [application setApplicationIconBadgeNumber:0];
    [application cancelAllLocalNotifications];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
//    NSLogSelfMethodName;
    [BMKMapView didForeGround];//当应用恢复前台状态时调用，回复地图的渲染和opengl相关的操作
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)application:(UIApplication *)application
didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    NSLog(@"%@", [NSString stringWithFormat:@"Device Token: %@", deviceToken]);
    [JPUSHService registerDeviceToken:deviceToken];
    
    NSString *token =
    [[[[deviceToken description] stringByReplacingOccurrencesOfString:@"<"
                                                           withString:@""]
      stringByReplacingOccurrencesOfString:@">"
      withString:@""]
     stringByReplacingOccurrencesOfString:@" "
     withString:@""];
    
    [[RCIMClient sharedRCIMClient] setDeviceToken:token];
}

- (void)application:(UIApplication *)application
didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"did Fail To Register For Remote Notifications With Error: %@", error);
}

- (void)application:(UIApplication *)application
didReceiveRemoteNotification:(NSDictionary *)userInfo {
    [JPUSHService handleRemoteNotification:userInfo];
    NSLog(@"didReceiveRemoteNotification:(NSDictionary *)userInfo 收到通知:%@", userInfo);
    
    /**
     * 统计推送打开率2
     */
    [[RCIMClient sharedRCIMClient] recordRemoteNotificationEvent:userInfo];
    /**
     * 获取融云推送服务扩展字段2
     */
    NSDictionary *pushServiceData = [[RCIMClient sharedRCIMClient] getPushExtraFromRemoteNotification:userInfo];
    if (pushServiceData) {
        NSLog(@"该远程推送包含来自融云的推送服务");
        for (id key in [pushServiceData allKeys]) {
            NSLog(@"key = %@, value = %@", key, pushServiceData[key]);
        }
    } else {
        NSLog(@"该远程推送不包含来自融云的推送服务");
    }
}

- (void)application:(UIApplication *)application
didReceiveRemoteNotification:(NSDictionary *)userInfo
fetchCompletionHandler:
(void (^)(UIBackgroundFetchResult))completionHandler {
    [JPUSHService handleRemoteNotification:userInfo];

    NSLog(@"completionHandler 收到通知:%@", userInfo);
    
    completionHandler(UIBackgroundFetchResultNewData);
}

- (void)application:(UIApplication *)application
didReceiveLocalNotification:(UILocalNotification *)notification {
    [JPUSHService showLocalNotificationAtFront:notification identifierKey:nil];
}

- (void)onGetNetworkState:(int)iError
{
    if (0 == iError) {
        NSLog(@"联网成功");
        bmkGetNetwork = YES;
        //[PromptInfo showText:@"联网成功"];
        //        [[NSNotificationCenter defaultCenter] postNotificationName:BMK_GETNETWORD_NOTIFICATION object:nil userInfo:@{@"getNetword" : @(YES)}];
    }
    else{
        NSLog(@"onGetNetworkState %d",iError);
    }
}

- (void)onGetPermissionState:(int)iError
{
    if (0 == iError) {
        NSLog(@"授权成功");
        //[PromptInfo showText:@"授权成功"];
        bmkGetPermission = YES;
        //        [[NSNotificationCenter defaultCenter] postNotificationName:BMK_GETPERMISSION_NOTIFICATION object:nil userInfo:@{@"getPermission" : @(YES)}];
    }
    else {
        NSLog(@"onGetPermissionState %d",iError);
//        NSLog()
    }
}

- (void)initBaiduMap {
    // 要使用百度地图，请先启动BaiduMapManager
    _mapManager = [[BMKMapManager alloc] init];
    
    // 如果要关注网络及授权验证事件，请设定generalDelegate参数
    BOOL ret = [_mapManager start:bmkAk generalDelegate:self];
    
    if (!ret) {
        NSLog(@"manager start failed!");
    }
    
//    if ([[UIDevice currentDevice].systemVersion floatValue] >= 8) {
//        //由于IOS8中定位的授权机制改变 需要进行手动授权
//        CLLocationManager  *locationManager = [[CLLocationManager alloc] init];
//        //获取授权认证
//        [locationManager requestAlwaysAuthorization];
//        [locationManager requestWhenInUseAuthorization];
//    }
}

- (void)initAMap {
    [MAMapServices sharedServices].apiKey = @"8baafb313fb41d32d15d5014a9e1d954";
}

- (void)initWindow {
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    [self.window makeKeyAndVisible];
    self.window.backgroundColor = [UIColor whiteColor];
    
    SelectEntranceViewController *selectEntranceVC = [[SelectEntranceViewController alloc] init];
    UINavigationController *UINC = [[UINavigationController alloc] initWithRootViewController:selectEntranceVC];
    UINC.navigationBarHidden = YES;
    
    self.window.rootViewController = UINC;
}

- (void)initPushServiceWithOptions:(NSDictionary *)launchOptions {
    
    // Required
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 8.0) {
        //可以添加自定义categories
        [JPUSHService registerForRemoteNotificationTypes:(UIUserNotificationTypeBadge |
                                                          UIUserNotificationTypeSound |
                                                          UIUserNotificationTypeAlert)
                                              categories:nil];
    } else {
        //categories 必须为nil
        [JPUSHService registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge |
                                                          UIRemoteNotificationTypeSound |
                                                          UIRemoteNotificationTypeAlert)
                                              categories:nil];
    }

//    [APService setupWithOption:launchOptions];
    [JPUSHService setupWithOption:launchOptions
                           appKey:@"9f4f918c16790d8d87203422"
                          channel:@"Publish channel"
                 apsForProduction:YES];
    
    NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
    [defaultCenter addObserver:self
                      selector:@selector(networkDidSetup:)
                          name:kJPFNetworkDidSetupNotification
                        object:nil];
    [defaultCenter addObserver:self
                      selector:@selector(networkDidClose:)
                          name:kJPFNetworkDidCloseNotification
                        object:nil];
    [defaultCenter addObserver:self
                      selector:@selector(networkDidRegister:)
                          name:kJPFNetworkDidRegisterNotification
                        object:nil];
    [defaultCenter addObserver:self
                      selector:@selector(networkDidLogin:)
                          name:kJPFNetworkDidLoginNotification
                        object:nil];
    [defaultCenter addObserver:self
                      selector:@selector(networkDidReceiveMessage:)
                          name:kJPFNetworkDidReceiveMessageNotification
                        object:nil];
    [defaultCenter addObserver:self
                      selector:@selector(serviceError:)
                          name:kJPFServiceErrorNotification
                        object:nil];
}

- (void)dealloc {
    [self unObserveAllNotifications];
    
    [[NSNotificationCenter defaultCenter]
     removeObserver:self
     name:RCKitDispatchMessageNotification
     object:nil];
}

- (void)unObserveAllNotifications {
    NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
    [defaultCenter removeObserver:self
                             name:kJPFNetworkDidSetupNotification
                           object:nil];
    [defaultCenter removeObserver:self
                             name:kJPFNetworkDidCloseNotification
                           object:nil];
    [defaultCenter removeObserver:self
                             name:kJPFNetworkDidRegisterNotification
                           object:nil];
    [defaultCenter removeObserver:self
                             name:kJPFNetworkDidLoginNotification
                           object:nil];
    [defaultCenter removeObserver:self
                             name:kJPFNetworkDidReceiveMessageNotification
                           object:nil];
    [defaultCenter removeObserver:self
                             name:kJPFServiceErrorNotification
                           object:nil];
}

- (void)networkDidSetup:(NSNotification *)notification {
    NSLog(@"已连接");
}

- (void)networkDidClose:(NSNotification *)notification {
    NSLog(@"未连接");
}

- (void)networkDidRegister:(NSNotification *)notification {
    NSLog(@"networkDidRegister:%@", [notification userInfo]);

    [[notification userInfo] valueForKey:@"RegistrationID"];

    NSLog(@"已注册");
    NSLog(@"注册ID：%@", [[notification userInfo] valueForKey:@"RegistrationID"]);
}

- (void)networkDidLogin:(NSNotification *)notification {

    NSLog(@"已登录");
    
    if ([JPUSHService registrationID]) {
        [TDSingleton instance].pushRegistrationID = [JPUSHService registrationID];
        NSLog(@"get RegistrationID:%@", [JPUSHService registrationID]);
        [TDSingleton instance].hasGetPushID = YES;
        
        if ([TDSingleton instance].sessionKey.length > 0) {
            [TDNetworkRequest submitRegistrationId:[TDSingleton instance].pushRegistrationID
                                           success:^(NSString *message)
            {
                NSLog(message);
            } failure:^(NSString *error) {
                NSLog(error);
            }];
        }
    }
}

- (void)networkDidReceiveMessage:(NSNotification *)notification {
    NSDictionary *userInfo = [notification userInfo];
    
    NSLog(@"networkDidReceiveMessage userInfo:%@", userInfo);
    
    NSString *title = [userInfo valueForKey:@"title"];
    NSString *content = [userInfo valueForKey:@"content"];
    NSDictionary *extra = [userInfo valueForKey:@"extras"];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    
    NSString *currentContent = [NSString
                                stringWithFormat:
                                @"收到自定义消息:%@\ntitle:%@\ncontent:%@\nextra:%@\n",
                                [NSDateFormatter localizedStringFromDate:[NSDate date]
                                                               dateStyle:NSDateFormatterNoStyle
                                                               timeStyle:NSDateFormatterMediumStyle],
                                title, content, [self logDic:extra]];
    NSLog(@"networkDidReceiveMessage:%@", currentContent);
    
//    NSDictionary *dic = [NSString dictionaryFromJSONString:content];
//    NSLog(@"networkDidReceiveMessage content:%@", dic);
    [self processMessageFromJPushWihtContent:content];
}

- (void)receiveMessage:(NSNotification *)notification {
    NSLog(@"receiveMessage:%@", [notification userInfo]);
}

- (void)sendMessageResult:(NSNotification *)notification {
    NSLog(@"sendMessageResult:%@", [notification userInfo]);
}

- (void)eventMessage:(NSNotification *)notification {
    NSLog(@"eventMessage:%@", [notification userInfo]);
}

- (void)messageKey:(NSNotification *)notification {
    NSLog(@"messageKey:%@", [notification userInfo]);
}

- (void)eventKey:(NSNotification *)notification {
    NSLog(@"eventKey:%@", [notification userInfo]);
}

- (NSString *)logDic:(NSDictionary *)dic {
    if (![dic count]) {
        return nil;
    }
    NSString *tempStr1 =
    [[dic description] stringByReplacingOccurrencesOfString:@"\\u"
                                                 withString:@"\\U"];
    NSString *tempStr2 =
    [tempStr1 stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""];
    NSString *tempStr3 =
    [[@"\"" stringByAppendingString:tempStr2] stringByAppendingString:@"\""];
    NSData *tempData = [tempStr3 dataUsingEncoding:NSUTF8StringEncoding];
    NSString *str =
    [NSPropertyListSerialization propertyListFromData:tempData
                                     mutabilityOption:NSPropertyListImmutable
                                               format:NULL
                                     errorDescription:NULL];
    return str;
}

- (void)serviceError:(NSNotification *)notification {
    NSDictionary *userInfo = [notification userInfo];
    NSString *error = [userInfo valueForKey:@"error"];
    NSLog(@"%@", error);
}

- (void)initRongCloudIM {
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    _chatObjectDic = [userDefault dictionaryForKey:@"UserInfo"][[TDSingleton instance].account][@"ChatObject"];
    
    //初始化融云SDK
    [[RCIM sharedRCIM] initWithAppKey:RONGCLOUD_IM_APPKEY];

//    /**
//     * 统计推送打开率1
//     */
//    [[RCIMClient sharedRCIMClient] recordLaunchOptionsEvent:launchOptions];
//    
//    NSDictionary *pushServiceData = [[RCIMClient sharedRCIMClient] getPushExtraFromLaunchOptions:launchOptions];
//    if (pushServiceData) {
//        NSLog(@"该启动事件包含来自融云的推送服务");
//        for (id key in [pushServiceData allKeys]) {
//            NSLog(@"%@", pushServiceData[key]);
//        }
//    } else {
//        NSLog(@"该启动事件不包含来自融云的推送服务");
//    }
//    ...
//}

    //极光已经注册
//    if (iOS8) {
//        // 在 iOS 8 下注册苹果推送，申请推送权限。
//        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeBadge
//                                                                                             |UIUserNotificationTypeSound
//                                                                                             |UIUserNotificationTypeAlert) categories:nil];
//        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
//    } else {
//        // 注册苹果推送，申请推送权限。
//        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound];
//    }
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(didReceiveMessageNotification:)
     name:RCKitDispatchMessageNotification
     object:nil];
    [[RCIM sharedRCIM] setConnectionStatusDelegate:self];
    
//    //设置接收消息代理
//    [RCIM sharedRCIM].receiveMessageDelegate=self;
    
    [[RCIM sharedRCIM] setUserInfoDataSource:self];
}

/**
 * 推送处理2
 */
//注册用户通知设置
//- (void)application:(UIApplication *)application
//didRegisterUserNotificationSettings:
//(UIUserNotificationSettings *)notificationSettings {
//    // register to receive notifications
//    [application registerForRemoteNotifications];
//}

/**
 *  网络状态变化。
 *
 *  @param status 网络状态。
 */
- (void)onRCIMConnectionStatusChanged:(RCConnectionStatus)status {
    if (status == ConnectionStatus_KICKED_OFFLINE_BY_OTHER_CLIENT) {
        NSLog(@"您的帐号在别的设备上登录，您被迫下线！");
        [SVProgressHUD showErrorWithStatus:@"您的账号正在别处登录，请检查是否是本人操作，如不是本人请尽快更改密码。" duration:4];
        [self logout];
//        UIAlertView *alert = [[UIAlertView alloc]
//                              initWithTitle:@"提示"
//                              message:@"您"
//                              @"的帐号在别的设备上登录，您被迫下线！"
//                              delegate:nil
//                              cancelButtonTitle:@"知道了"
//                              otherButtonTitles:nil, nil];
//        [alert show];
    } else if (status == ConnectionStatus_TOKEN_INCORRECT) {
        dispatch_async(dispatch_get_main_queue(), ^{
            UIAlertView *alertView =
            [[UIAlertView alloc] initWithTitle:nil
                                       message:@"Token已过期，请重新登录"
                                      delegate:nil
                             cancelButtonTitle:@"确定"
                             otherButtonTitles:nil, nil];
            ;
            [alertView show];
        });
    }
}

- (void)didReceiveMessageNotification:(NSNotification *)notification {
    NSLog(@"didReceiveMessageNotification：%@", notification);
    [UIApplication sharedApplication].applicationIconBadgeNumber =
    [UIApplication sharedApplication].applicationIconBadgeNumber + 1;
}

//-(void)onRCIMReceiveMessage:(RCMessage *)message left:(int)left
//{
//    NSLog(@"onRCIMReceiveMessage:(RCMessage *)message left:(int)left");
//    if ([message.content isMemberOfClass:[RCInformationNotificationMessage class]]) {
//        RCInformationNotificationMessage *msg=(RCInformationNotificationMessage *)message.content;
//        //NSString *str = [NSString stringWithFormat:@"%@",msg.message];
//        if ([msg.message rangeOfString:@"你已添加了"].location!=NSNotFound) {
////            [RCDDataSource syncFriendList:^(NSMutableArray *friends) {
////            }];
//        }
//    }
//}

- (void)initReachability {
    AFNetworkReachabilityManager * reachability = [AFNetworkReachabilityManager sharedManager];
    [reachability setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        switch (status) {
            case AFNetworkReachabilityStatusUnknown://未知
            {
                
            }
                break;
            case AFNetworkReachabilityStatusNotReachable://不可用
            {
                UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"通知" message:@"当前网络不可用" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
                [alert show];
                [alert dismissWithClickedButtonIndex:0 animated:YES];
            }
                break;
            case AFNetworkReachabilityStatusReachableViaWiFi://wifi
            {
//                UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"通知" message:@"切换到WIFI网络" delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
//                [alert show];
//                [alert dismissWithClickedButtonIndex:0 animated:YES];
            }
                
                break;
            case AFNetworkReachabilityStatusReachableViaWWAN://移动网络
            {
//                UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"通知" message:@"切换到移动网络" delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
//                [alert show];
//                [alert dismissWithClickedButtonIndex:0 animated:YES];
            }
                
                break;
                
            default:
                break;
        }
    }];
    [reachability startMonitoring];
}

- (void)initNetworkActivity {
    AFNetworkActivityIndicatorManager * networkActivity = [AFNetworkActivityIndicatorManager sharedManager];
    [networkActivity setEnabled:YES];
}

- (void)processMessageFromJPushWihtContent:(NSString *)content {
    if ([NSString dictionaryFromJSONString:content]) {
        NSDictionary *dic = [NSString dictionaryFromJSONString:content];
        
        if ([TDSingleton instance].isLogin == YES) {
            NSUInteger mainType = [dic[@"mainType"] integerValue];
            NSUInteger childType = [dic[@"type"] integerValue];
            switch (mainType) {
                case 1://系统类型
                    switch (childType) {
                        case 1://其他设备相同账号登录
                            NSLog(@"您的账号正在别处登录，IP: 请检查是否是本人操作，如不是本人请尽快更改密码。");
                            break;
                            
                        case 2://身份认证
                            [TDSingleton instance].userInfoModel.authentication = [dic[@"authState"] integerValue];
                            break;
                            
                        case 3://大咖认证
                        {
                            NSInteger certifyState = [dic[@"certifyState"] integerValue];
                            NSInteger gameId = [dic[@"gameId"] integerValue];
                            
                            for (LGDakaCertificate *certificate in [TDSingleton instance].userInfoModel.subUsersSimple) {
                                if (certificate.gameId == gameId) {
                                    certificate.certifyState = certifyState;
                                    break;
                                }
                            }
                        }
                            break;
                            
                        default:
                            break;
                    }
                    break;
                    
                case 2://订单类型
                    switch (childType) {
                        case 1:
                        {
                            NSInteger playType = [dic[@"sceneId"] integerValue];
                            _todayOrderModel = [[TodayOrderModel alloc] initWithDictionary:dic];
                            
                            if (playType == 5) {
                                [LGDefineNetServer getWalletInfoSuccess:^(id result) {
                                    NSLog(@"getMoney info:%@", result);
                                    
                                    _todayOrderModel.moneyFromBigShotAccount = [result[@"money"] floatValue];

                                } failure:^(NSString *msg) {
                                    [SVProgressHUD showErrorWithStatus:msg];
                                }];
                            }
                            
                            [TDOrderMessageBox showWithModel:_todayOrderModel delegate:self];
                        }
                            break;
                            
                        case 2://大咖接受订单
                            [[NSNotificationCenter defaultCenter] postNotificationName:TDBigShotAcceptOrder object:content];
                            break;

                        case 3:
                            [self.window makeToast:@"大咖拒绝订单" duration:3 position:@"center"];
                            [[NSNotificationCenter defaultCenter] postNotificationName:TDBigShotRejectOrder object:content];
                            break;
                            
                        case 4:
                            [self.window makeToast:@"大咖开始订单" duration:3 position:@"center"];
                            break;
                            
                        case 5:
//                            [self showOrderEvaluationWindowWithData:dic];
                            break;
                    }
                    break;
            }
        }
    }
}

- (void)showOrderEvaluationWindowWithData:(NSDictionary *)data {
    
    NSLog(@"showOrderEvaluationWindow With Data:%@", data);
    _orderModel = [[BigShotOrderModel alloc] initWithDictionary:data];
    
    NSInteger timeDifference = (NSInteger)(_orderModel.endTime - _orderModel.startTime) / 1000;
    
    NSString *playTime;
    
    if (timeDifference < 60) {
        playTime = [NSString stringWithFormat:@"%ld秒", (long)timeDifference];
    } else if (timeDifference >=60 && timeDifference < 3600 ) {
        playTime = [NSString stringWithFormat:@"%ld分钟", (long)timeDifference / 60];
    } else if (timeDifference >= 3600) {
        playTime = [NSString stringWithFormat:@"%.1f小时", timeDifference / 3600.f];
    }
    
    NSString *timeString = [NSString stringWithFormat:@"%@结束    共计 %@", _orderModel.formatEndTime, playTime];
    
    [TDOrderEvaluationView showWindowWithName:_orderModel.senderName
                                         time:timeString
                                  portraitURL:_orderModel.portraitURL
                                     delegate:self];
}

- (void)submitOrderEvaluation:(NSInteger)evaluation {
    NSDictionary *parameters = @{@"action":@"rateOrder",
                                 @"sessionKey":[TDSingleton instance].sessionKey,
                                 @"orderId":_orderModel.orderID,
                                 @"star":[NSString stringWithFormat:@"%ld", (long)evaluation]};
    
    NSLog(@"submitOrderEvaluation parameters:%@", parameters);

    [TDNetworking requestWithURL:[TDSingleton instance].gatewayServerBusinessURL
                      parameters:parameters
                         success:^(id responseObject)
     {
         NSLog(@"submitOrderEvaluation result:%@", responseObject);
         if ([responseObject[@"result"] integerValue] == 1) {
             [SVProgressHUD dismissWithSuccess:@"评价成功!\nYeah!"];
         } else {
             [SVProgressHUD dismissWithError:[NSString stringWithFormat:@"%@", responseObject[@"msg"]]];
         }
     } failure:^(NSError *error) {
         [SVProgressHUD dismissWithError:[NSString stringWithFormat:@"%@", error]];
         NSLog(@"requestUserInfoWithIdentifier error:%@", error);
     }];
}

- (void)orderEvaluationViewComplaintButtonClicked {
    NSLog(@"orderEvaluationViewComplaintButtonClicked");
}

- (void)orderEvaluationViewDoItLaterButtonClicked {
    NSLog(@"orderEvaluationViewDoItLaterButtonClicked");
//    [self submitOrderEvaluation:score];
}

- (void)orderEvaluationViewGetScore:(NSInteger)score {
    NSLog(@"orderEvaluationViewGetScore:%ld", (long)score);
}

- (void)logout {
    if ([TDSingleton instance].isLogin == YES) {
        
        [[LGLocationService sharedManager] stopUserLocationService];
        
        [TDSingleton instance].autoLogin = NO;
        [TDSingleton instance].isLogin   = NO;
        [TDSingleton instance].account   = @"";
        [TDSingleton instance].password  = @"";
        [TDSingleton instance].token     = @"";
        
        [[TDSingleton instance].settingDictionary setValue:[TDSingleton instance].account forKey:@"Account"];
        [[TDSingleton instance].settingDictionary setValue:[TDSingleton instance].password forKey:@"Password"];
        [[TDSingleton instance].settingDictionary setValue:[NSNumber numberWithBool:NO] forKey:@"AutoLogin"];
        
        [[TDSingleton instance].settingDictionary writeToFile:[TDSingleton instance].settingPlistFileName atomically:YES];
        
        [self logoutWithGatewayServer];
        
        [[RCIMClient sharedRCIMClient] disconnect:NO];
        
        if ([self.window.rootViewController isKindOfClass:[UINavigationController class]]) {
            UINavigationController *UINC = (UINavigationController *)self.window.rootViewController;
            
            if (UINC.viewControllers.count > 1) {
                [UINC popToViewController:UINC.viewControllers[1] animated:YES];
            }
//            if ([UINC.viewControllers[2] isKindOfClass:[SettingViewController class]]) {
//                SettingViewController *vc = (SettingViewController *)UINC.viewControllers[2];
//                [vc logout];
//            }
            NSLog(@"AppDelegate logout viewControllers:%@", UINC.viewControllers);
        }
    }
}

- (void)logoutWithGatewayServer {
    NSDictionary *parameters = @{@"sessionKey":[TDSingleton instance].sessionKey};
    
    NSLog(@"logoutWithGatewayServer URL:%@", [TDSingleton instance].gatewayServerLogoutURL);
    NSLog(@"logoutWithGatewayServer parameters:%@", parameters);
    [TDNetworking requestWithURL:[TDSingleton instance].gatewayServerLogoutURL
                      parameters:parameters
                         success:^(id responseObject)
     {
         NSLog(@"logout GatewayServerWithData result:%@", responseObject);
         if ([responseObject[@"result"] integerValue] == 1) {
             
         } else {
             
         }
     } failure:^(NSError *error) {
         NSLog(@"error:%@", error);
     }];
}

/**
 *  第三方支付-Ping++
 */
- (void)initPingpp {
    [Pingpp setDebugMode:NO];
}

- (void)initShareSdk {
    
    [ShareSDK registerApp:@"d73e9e654b90"];
    
    [ShareSDK connectSinaWeiboWithAppKey:@"4287653967" appSecret:@"27d1a969e1ff05164e7f0a2dbdd4ca80" redirectUri:@"https://api.weibo.com/oauth2/default.html"];
    [ShareSDK connectSinaWeiboWithAppKey:@"4287653967" appSecret:@"27d1a969e1ff05164e7f0a2dbdd4ca80" redirectUri:@"https://api.weibo.com/oauth2/default.html" weiboSDKCls:[WeiboSDK class]];
    
    [ShareSDK connectQZoneWithAppKey:@"1104996912" appSecret:@"QveNOtyYeJ3m0jz0" qqApiInterfaceCls:[QQApiInterface class] tencentOAuthCls:[TencentOAuth class]];
    
    [ShareSDK connectQQWithQZoneAppKey:@"1104996912" qqApiInterfaceCls:[QQApiInterface class] tencentOAuthCls:[TencentOAuth class]];
    
    //公众号
//    [ShareSDK connectWeChatWithAppId:@"wxa333febe202f7ac7" appSecret:@"d4624c36b6795d1d99dcf0547af5443d" wechatCls:[WXApi class]];
    
    //移动应用
    [ShareSDK connectWeChatWithAppId:@"wx9fdbe0ccbadfe82c" appSecret:@"b6d4922288992d36c54f3c93903d518a" wechatCls:[WXApi class]];
}

- (BOOL)application:(UIApplication *)application
      handleOpenURL:(NSURL *)url
{
    return [ShareSDK handleOpenURL:url
                        wxDelegate:self];
}

//Ping++ iOS 8 及以下回调 (shareSDK回调)
- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation
{
    NSLog(@"application:%@\nurl:%@\nsourceApplication:%@\n,annotation:%@", application, url, sourceApplication, annotation);
    
    BOOL retrunValue = NO;
    
    if ([url.absoluteString containsString:@"://pay/"] || [url.absoluteString containsString:@"://safepay/"]) {
        BOOL canHandleURL = [Pingpp handleOpenURL:url withCompletion:nil];
        return canHandleURL;
    } else if ([url.absoluteString containsString:@"state=withdrawCash"]) {
        return [WXApi handleOpenURL:url delegate:[WXApiManager sharedManager]];
    } else {
        return [ShareSDK handleOpenURL:url
                     sourceApplication:sourceApplication
                            annotation:annotation
                            wxDelegate:self];
    }
    
    return retrunValue;
}

//Ping++ iOS9以上回调
- (BOOL)application:(UIApplication *)app
            openURL:(NSURL *)url
            options:(NSDictionary *)options
{
    NSLog(@"openURL:%@\noptions:%@\n", url, options);
    BOOL retrunValue = NO;
    
    if ([url.absoluteString containsString:@"://pay/"] || [url.absoluteString containsString:@"://safepay/"]) {
        BOOL canHandleURL = [Pingpp handleOpenURL:url withCompletion:nil];
        return canHandleURL;
    } else if ([url.absoluteString containsString:@"state=withdrawCash"]) {
        return  [WXApi handleOpenURL:url delegate:[WXApiManager sharedManager]];
    } else {
        return [ShareSDK handleOpenURL:url
                            wxDelegate:self];
    }
    
    return retrunValue;
}

#pragma mark 抢单回调 *开始*
- (void)TDOrderMessageBoxCloseButtonClickedWithModel:(TodayOrderModel *)model {
    [self rejectOrderWithModel:model];
}

- (void)TDOrderMessageBoxGrabOrderButtonClickedWithModel:(TodayOrderModel *)model {
    NSLog(@"TDOrderMessageBoxGrabOrderButtonClickedWithModel:%@", model.description);
    
    if (model.playType == 5) {
        if (model.moneyFromBigShotAccount < (model.amount * 0.3)) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"保证金余额不足，请充值。" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alertView show];
        } else {
            [self acceptOrderWithModel:model];
        }
    } else {
        [self acceptOrderWithModel:model];
    }
}

- (void)acceptOrderWithModel:(TodayOrderModel *)model {
    NSDictionary *parameters = @{@"action":@"acceptOrder",
                                 @"sessionKey":[TDSingleton instance].sessionKey,
                                 @"orderId":model.orderID,
                                 @"senderName":model.senderAccount};
    
    NSLog(@"getOrderWithContent parameters:%@", parameters);
    [TDNetworking requestWithURL:[TDSingleton instance].gatewayServerBusinessURL
                      parameters:parameters
                         success:^(id responseObject)
     {
         NSLog(@"getOrderWithContent:%@", responseObject);
         if ([responseObject[@"result"] integerValue] == 1) {
             [SVProgressHUD showSuccessWithStatus:@"抢单成功!"];
             [[NSNotificationCenter defaultCenter] postNotificationName:TDBigShotAcceptOrder object:nil];
         } else {
             [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@", responseObject[@"msg"]]];
         }
     } failure:^(NSError *error) {
         NSLog(@"error:%@", error);
         [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@", error.description]];
     }];
}

- (void)rejectOrderWithModel:(TodayOrderModel *)model {
    NSDictionary *parameters = @{@"action":@"refuseOrder",
                                 @"sessionKey":[TDSingleton instance].sessionKey,
                                 @"orderId":model.orderID,
                                 @"senderName":model.senderAccount};
    
    NSLog(@"rejectOrder parameters:%@", parameters);
    [TDNetworking requestWithURL:[TDSingleton instance].gatewayServerBusinessURL
                      parameters:parameters
                         success:^(id responseObject)
     {
         NSLog(@"rejectOrder responseObject:%@", responseObject);
         if ([responseObject[@"result"] integerValue] == 1) {
             [SVProgressHUD showSuccessWithStatus:@"拒绝订单成功!"];
         } else {
             [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@", responseObject[@"msg"]]];
         }
     } failure:^(NSError *error) {
         NSLog(@"error:%@", error);
         [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@", error.description]];
     }];
}

#pragma mark 用户聊天头像获取
- (void)getUserInfoWithUserId:(NSString *)userId
                   completion:(void (^)(RCUserInfo *))completion {
    NSLog(@"userId:%@", userId);
    
    if ([[_chatObjectDic allKeys] containsObject:userId]) {
        NSDictionary *dic = [_chatObjectDic objectForKey:userId];
        
        RCUserInfo *user = [[RCUserInfo alloc]init];
        user.userId = userId;
        user.name = [NSString stringWithFormat:@"%@", dic[@"Name"]];
        user.portraitUri = [NSString stringWithFormat:@"%@", dic[@"PortraitURL"]];
        
        return completion(user);
    } else if ([[TDSingleton instance].account isEqualToString:userId]) {
        RCUserInfo *user = [[RCUserInfo alloc]init];
        user.userId = userId;
        
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        
        NSMutableDictionary *userInfoDic = [[NSMutableDictionary alloc] initWithDictionary:[userDefault dictionaryForKey:@"UserInfo"]];
        
        NSMutableDictionary *accountDic = [[NSMutableDictionary alloc] initWithDictionary:userInfoDic[[TDSingleton instance].account]];
        
        user.name = [NSString stringWithFormat:@"%@", accountDic[@"Name"]];
        user.portraitUri = [NSString stringWithFormat:@"%@", accountDic[@"PortraitURL"]];
        
        return completion(user);
    }
    
    return completion(nil);
}


@end
