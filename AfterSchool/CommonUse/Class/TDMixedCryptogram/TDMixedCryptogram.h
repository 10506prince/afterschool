//
//  MixedCryptogram.h
//  PasswordCode
//
//  Created by Teson Draw on 1/10/16.
//  Copyright © 2016 Teson Draw. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface TDMixedCryptogram : NSObject

+ (NSString *)fromPassword:(NSString *)password;

@end
