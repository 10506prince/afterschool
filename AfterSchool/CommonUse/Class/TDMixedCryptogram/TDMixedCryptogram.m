//
//  MixedCryptogram.m
//  PasswordCode
//
//  Created by Teson Draw on 1/10/16.
//  Copyright © 2016 Teson Draw. All rights reserved.
//

#import "TDMixedCryptogram.h"
#import "NSString+MD5String.h"

@implementation TDMixedCryptogram

+ (NSString *)fromPassword:(NSString *)password {
    NSMutableString *mixedCryptogram = [[NSMutableString alloc] init];
    
    [mixedCryptogram appendString:@"329"];
    [mixedCryptogram appendString:password];
    [mixedCryptogram appendString:@"628"];
    
    NSString *result = [mixedCryptogram MD5String];
    result = [result MD5String];
    
    return result;
}

@end
