//
//  TDAttributedString.m
//  AfterSchool
//
//  Created by Teson Draw on 1/10/16.
//  Copyright © 2016 Teson Draw. All rights reserved.
//

#import "TDAttributedString.h"

@implementation TDAttributedString

+ (NSMutableAttributedString *)fromString:(NSString *)string
                              indexString:(NSString *)indexString {
    
    NSDictionary * forePartAttributedStringDic = @{NSFontAttributeName:[UIFont systemFontOfSize:14],
                                                   NSForegroundColorAttributeName:[UIColor blackColor]};
    
    NSDictionary * afterPartAttributedStringDic = @{NSFontAttributeName:[UIFont systemFontOfSize:14],
                                                    NSForegroundColorAttributeName:[UIColor colorWithRed:38/255.f green:166/255.f blue:154/255.f alpha:1]};
    
    
    NSMutableAttributedString * attributedString = [[NSMutableAttributedString alloc] initWithString:string
                                                                                          attributes:forePartAttributedStringDic];
    
    NSRange range = [string rangeOfString:indexString];
    
    [attributedString addAttributes:afterPartAttributedStringDic
                              range:NSMakeRange(range.location + 1, string.length - range.location - 1)];
    
    return attributedString;
}

+ (NSMutableAttributedString *)fromString:(NSString *)string
                        forePartAttribute:(NSDictionary *)forePartAttribute
                              indexString:(NSString *)indexString
                       afterPartAttribute:(NSDictionary *)afterPartAttribute {
    
    NSMutableAttributedString * attributedString = [[NSMutableAttributedString alloc] initWithString:string
                                                                                          attributes:forePartAttribute];
    
    NSRange range = [string rangeOfString:indexString];
    
    [attributedString addAttributes:afterPartAttribute
                              range:NSMakeRange(range.location + 1, string.length - range.location - 1)];
    
    return attributedString;
}

+ (NSMutableAttributedString *)fromForePartString:(NSString *)forePartString
                                  afterPartString:(NSString *)afterPartString {
    
    NSDictionary * forePartAttributedStringDic = @{NSFontAttributeName:[UIFont systemFontOfSize:14],
                                                   NSForegroundColorAttributeName:[UIColor blackColor]};
    
    NSDictionary * afterPartAttributedStringDic = @{NSFontAttributeName:[UIFont systemFontOfSize:14],
                                                    NSForegroundColorAttributeName:[UIColor colorWithRed:38/255.f green:166/255.f blue:154/255.f alpha:1]};
    
    NSMutableAttributedString * forePartAttributedString = [[NSMutableAttributedString alloc] initWithString:forePartString
                                                                                                  attributes:forePartAttributedStringDic];
    NSMutableAttributedString * afterPartAttributedString = [[NSMutableAttributedString alloc] initWithString:afterPartString
                                                                                                   attributes:afterPartAttributedStringDic];
    
    [forePartAttributedString appendAttributedString:afterPartAttributedString];
    
    return forePartAttributedString;
}

+ (NSMutableAttributedString *)fromForePartString:(NSString *)forePartString
                                forePartAttribute:(NSDictionary *)forePartAttribute
                                  afterPartString:(NSString *)afterPartString
                               afterPartAttribute:(NSDictionary *)afterPartAttribute {
    
    NSMutableAttributedString * forePartAttributedString = [[NSMutableAttributedString alloc] initWithString:forePartString
                                                                                                  attributes:forePartAttribute];
    
    NSMutableAttributedString * afterPartAttributedString = [[NSMutableAttributedString alloc] initWithString:afterPartString
                                                                                                   attributes:afterPartAttribute];
    
    [forePartAttributedString appendAttributedString:afterPartAttributedString];
    
    return forePartAttributedString;
}

@end
