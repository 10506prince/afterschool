//
//  TDNumberToDate.m
//  AfterSchool
//
//  Created by Teson Draw on 10/30/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "TDNumberToDate.h"

@implementation TDNumberToDate

+ (NSString *)dateFromString:(NSString *)string
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:@"YYYY-MM-dd"];
    NSDate *confromTimesp = [NSDate dateWithTimeIntervalSince1970:(NSTimeInterval)[string longLongValue]/1000.0];
    
    NSString *dateString = [formatter stringFromDate:confromTimesp];
    
    return dateString;
}

+ (NSString *)dateFromNumber:(long long)number
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    [formatter setDateFormat:@"YYYY-MM-dd HH:mm"];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:(NSTimeInterval)number/1000.0];
    
    NSString *dateTimeString = [formatter stringFromDate:date];
    NSString *dateString = [dateTimeString substringToIndex:10];
    
    NSTimeInterval secondsPerDay = 24 * 60 * 60;
    NSDate *today = [[NSDate alloc] init];
    
    NSDate *yesterday;
    yesterday = [today dateByAddingTimeInterval:-secondsPerDay];
    
    NSString *todayString = [[today description] substringToIndex:10];
    NSString *yesterdayString = [[yesterday description] substringToIndex:10];
    
    NSString *result;
    
    if ([dateString isEqualToString:todayString]) {
        result = [dateTimeString substringFromIndex:(dateTimeString.length - 5)];
    } else if ([dateString isEqualToString:yesterdayString]) {
        result = @"昨天";
    } else {
        result = dateString;
    }
    
    return result;
}

+ (NSString *)dateFromNumber:(long long)number
                  dateFormat:(NSString *)dateFormat
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    [formatter dateFromString:dateFormat];
    
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:(NSTimeInterval)number/1000.0];
    
    NSString *dateTimeString = [formatter stringFromDate:date];
    
    return dateTimeString;
}

@end
