//
//  TDNumberToDate.h
//  AfterSchool
//
//  Created by Teson Draw on 10/30/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TDNumberToDate : NSObject

/**
 *  返回指定日期格式字符串
 *
 *  @param string 数字字符串
 *
 *  @return 指定日期格式字符串
 */
+ (NSString *)dateFromString:(NSString *)string;


/**
 *  返回指定日期格式字符串
 *
 *  @param number long long类型日期数字
 *
 *  @return 指定日期格式字符串
 */
+ (NSString *)dateFromNumber:(long long)number;

/**
 *  返回指定日期格式字符串
 *
 *  @param number     时间戳数字
 *  @param dateFormat 日期格式字符串
 *
 *  @return 指定日期格式字符串
 */
+ (NSString *)dateFromNumber:(long long)number
                  dateFormat:(NSString *)dateFormat;

@end
