//
//  TDDateComponents.h
//  AfterSchool
//
//  Created by Teson Draw on 10/8/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TDDateComponents : NSObject

+ (NSDateComponents *)dateComponentsWithYear:(NSInteger)year
                                       month:(NSInteger)month
                                         day:(NSInteger)day;


+ (NSDateComponents *)dateComponentsFromString:(NSString *)string;

@end
