//
//  TDDateComponents.m
//  AfterSchool
//
//  Created by Teson Draw on 10/8/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "TDDateComponents.h"

@implementation TDDateComponents

+ (NSDateComponents *)dateComponentsWithYear:(NSInteger)year
                                       month:(NSInteger)month
                                         day:(NSInteger)day
{
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    
    [dateComponents setYear:year];
    [dateComponents setMonth:month];
    [dateComponents setDay:day];
    
    return dateComponents;
}

+ (NSDateComponents *)dateComponentsFromString:(NSString *)string {
    NSArray *array = [string componentsSeparatedByString:@"/"];
    
    if (array.count == 3) {
        NSDateComponents * dateComponents = [TDDateComponents dateComponentsWithYear:[array[0] integerValue]
                                                            month:[array[1] integerValue]
                                                              day:[array[2] integerValue]];
        return dateComponents;
    } else {
        return nil;
    }
}

@end
