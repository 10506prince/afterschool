//
//  TDDateConversion.m
//  AfterSchool
//
//  Created by Teson Draw on 11/23/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "TDDateConversion.h"

@implementation TDDateConversion

+ (NSString *)dateFromNumber:(long)number
                  dateFormat:(NSString *)dateFormat
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    [formatter setDateFormat:dateFormat];
    
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:(NSTimeInterval)number];
    
    NSString *dateTimeString = [formatter stringFromDate:date];
    
    return dateTimeString;
}

@end
