//
//  TDDateConversion.h
//  AfterSchool
//
//  Created by Teson Draw on 11/23/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TDDateConversion : NSObject

/**
 *  返回指定日期格式字符串
 *
 *  @param number     时间戳数字
 *  @param dateFormat 日期格式字符串
 *
 *  @return 指定日期格式字符串
 */
+ (NSString *)dateFromNumber:(long)number
                  dateFormat:(NSString *)dateFormat;

@end
