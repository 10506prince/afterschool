//
//  TDDatePickerView.m
//  DatePickerViewTest
//
//  Created by Teson Draw on 3/29/15.
//  Copyright (c) 2015 Teson Draw. All rights reserved.
//

#import "TDDatePickerView.h"
#import "UIButton+SetBackgroundColor.h"

#define SCREEN_HEIGHT [UIScreen mainScreen].bounds.size.height
#define SCREEN_WIDTH [UIScreen mainScreen].bounds.size.width

@implementation TDDatePickerView


- (instancetype)initWithFrame:(CGRect)frame dateComponents:(NSDateComponents *)dateComponents targetPosition:(CGPoint)targetPosition {
    
    self = [super initWithFrame:frame];
    if (self) {
        self.layer.borderColor = [UIColor colorWithRed:196/255.f green:196/255.f blue:196/255.f alpha:1].CGColor;
        self.layer.borderWidth = 1;
        self.layer.cornerRadius = 6;
        self.layer.masksToBounds = YES;
        self.backgroundColor = [UIColor whiteColor];
        
        _homePosition = CGPointMake(frame.origin.x, frame.origin.y);
        _targetPosition = targetPosition;
        [self initDataWithDateComponents:dateComponents];
        [self initUserInterface];
    }
    return self;
}

- (void)initDataWithDateComponents:(NSDateComponents *)dateComponents {
    _yearArray = [[NSMutableArray alloc] init];
    _yearForShowArray = [[NSMutableArray alloc] init];

    for (int i = 1945; i <= 2012 ; i++)
    {
        [_yearArray addObject:[NSString stringWithFormat:@"%d", i]];
        [_yearForShowArray addObject:[NSString stringWithFormat:@"%d年", i]];

        if ([_yearArray[i - 1945] isEqualToString:[NSString stringWithFormat:@"%ld", (long)dateComponents.year]]) {
            _selectedRowYear = i - 1945;
        }
    }

    _monthArray = @[@"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9", @"10", @"11", @"12"];
    _monthForShowArray = @[@"1月", @"2月", @"3月", @"4月", @"5月", @"6月", @"7月", @"8月", @"9月", @"10月", @"11月", @"12月"];

    for (NSInteger i = 0; i < 12; i++) {
        if ([_monthArray[i] isEqualToString:[NSString stringWithFormat:@"%ld", (long)dateComponents.month]]) {
            _selectedRowMonth = i;
        }
    }

    _daysArray = [[NSMutableArray alloc]init];
    _daysForShowArray = [[NSMutableArray alloc]init];

    for (int i = 1; i <= 31; i++)
    {
        [_daysForShowArray addObject:[NSString stringWithFormat:@"%d日",i]];
        [_daysArray addObject:[NSString stringWithFormat:@"%d",i]];

        if ([_daysArray[i - 1] isEqualToString:[NSString stringWithFormat:@"%ld", (long)dateComponents.day]]) {
            _selectedRowDay = i - 1;
        }
    }
}

- (void)initUserInterface {
    [self addSubview:self.pickerView];
    [self addSubview:self.cancelButton];
    [self addSubview:self.confirmButton];
    _pickerView.delegate = self;
    _pickerView.dataSource = self;
    
    [self refreshDate];
}

- (void)refreshDate {
    [_pickerView selectRow:_selectedRowYear inComponent:0 animated:YES];
    [_pickerView selectRow:_selectedRowMonth inComponent:1 animated:YES];
    [_pickerView selectRow:_selectedRowDay inComponent:2 animated:YES];
}

- (UIPickerView *)pickerView {
    if (!_pickerView) {
        _pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0, 300, 200)];
    }
    return _pickerView;
}

- (UIButton *)cancelButton {
    if (!_cancelButton) {
        _cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 200, 300 / 2, 44)];
        [_cancelButton setTitle:@"取消" forState:UIControlStateNormal];
        [_cancelButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_cancelButton setBackgroundColor:[UIColor grayColor] forState:UIControlStateHighlighted];
        _cancelButton.titleLabel.font = [UIFont systemFontOfSize:20];
        _cancelButton.layer.borderColor = [UIColor colorWithRed:196/255.f green:196/255.f blue:196/255.f alpha:1].CGColor;
        _cancelButton.layer.borderWidth = 1;
        [_cancelButton addTarget:self action:@selector(cancelButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    }
    return _cancelButton;
}

- (void)cancelButtonClicked {
    [self moveToHomePositon];
    if (_delegate && [_delegate respondsToSelector:@selector(datePickerViewCancelButtonClicked)]) {
        [_delegate datePickerViewCancelButtonClicked];
    }
}

- (UIButton *)confirmButton {
    if (!_confirmButton) {
        _confirmButton = [[UIButton alloc] initWithFrame:CGRectMake(150 - 1, 200, 150 + 1, 44)];
        [_confirmButton setTitle:@"确定" forState:UIControlStateNormal];
        [_confirmButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_confirmButton setBackgroundColor:[UIColor grayColor] forState:UIControlStateHighlighted];

        _confirmButton.titleLabel.font = [UIFont systemFontOfSize:20];
        _confirmButton.layer.borderColor = [UIColor colorWithRed:196/255.f green:196/255.f blue:196/255.f alpha:1].CGColor;
        _confirmButton.layer.borderWidth = 1;
        [_confirmButton addTarget:self action:@selector(confirmButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    }
    return _confirmButton;
}

- (void)confirmButtonClicked {
    NSString *dateString = [NSString stringWithFormat:@"%@-%02d-%02d", _yearArray[[_pickerView selectedRowInComponent:0]], [_monthArray[[_pickerView selectedRowInComponent:1]] intValue], [_daysArray[[_pickerView selectedRowInComponent:2]] intValue]];
    if (_delegate && [_delegate respondsToSelector:@selector(datePickerViewConfirmButtonClicked:)]) {
        [_delegate datePickerViewConfirmButtonClicked:dateString];
    }
    [self moveToHomePositon];
}

#pragma mark pickerView
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 3;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    switch (component) {
        case 0:
            return _yearArray.count;
            break;

        case 1:
            return _monthArray.count;
            break;

        case 2:
            {
                if (_selectedRowMonth == 0 || _selectedRowMonth == 2 || _selectedRowMonth == 4 || _selectedRowMonth == 6 || _selectedRowMonth == 7 || _selectedRowMonth == 9 || _selectedRowMonth == 11)
                {
                    return 31;
                }
                else if (_selectedRowMonth == 1)
                {
                    int yearInt = [[_yearArray objectAtIndex:_selectedRowYear] intValue];

                    if(((yearInt%4 == 0) && (yearInt%100 != 0)) || (yearInt%400 == 0)){
                        return 29;
                    }
                    else
                    {
                        return 28;
                    }
                } else {
                    return 30;
                }
            }
            break;
    }

    return 0;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    NSString *titleString = [[NSString alloc] init];
    switch (component) {
        case 0:
            titleString = _yearForShowArray[row];
            break;
            
        case 1:
            titleString = _monthForShowArray[row];
            break;
            
        case 2:
            titleString = _daysForShowArray[row];
            break;
    }
    return titleString;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    return 36;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    switch (component) {
        case 0:
            {
                _selectedRowYear = row;
                [pickerView reloadComponent:2];
            }
            break;

        case 1:
            {
                _selectedRowMonth = row;
                [pickerView reloadComponent:2];
            }
            break;

        case 2:
            {
                _selectedRowDay = row;
            }
            break;
    }
}

- (void)moveToTargetPositon {
    [self.superview addSubview:self.maskView];
    [self.superview addSubview:self];

    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.2];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    
    self.maskView.alpha = 0.3;
    self.maskView.backgroundColor = [UIColor grayColor];

    self.frame = CGRectMake(_targetPosition.x, _targetPosition.y, 300, 244);
    [UIView commitAnimations];
}

- (void)moveToHomePositon {

    [UIView beginAnimations:@"RestoreDatePickerViewPosition" context:nil];
    [UIView setAnimationDuration:0.2];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDidStopSelector:@selector(didEndMoveToHomePosition)];
    [UIView setAnimationDelegate:self];
    
    _maskView.alpha = 0;

    self.frame = CGRectMake(_homePosition.x, _homePosition.y, 300, 244);
    [UIView commitAnimations];
}

- (void)didEndMoveToHomePosition {
    [_maskView removeFromSuperview];
    [self removeFromSuperview];
}

- (UIView *)maskView {
    if (!_maskView) {
        _maskView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
        _maskView.backgroundColor = [UIColor clearColor];
        _maskView.userInteractionEnabled = YES;
    }
    return _maskView;
}

@end
