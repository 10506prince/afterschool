//
//  TDDatePickerView.h
//  DatePickerViewTest
//
//  Created by Teson Draw on 3/29/15.
//  Copyright (c) 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol TDDatePickerViewDelegate <NSObject>

@optional
- (void)datePickerViewCancelButtonClicked;
- (void)datePickerViewConfirmButtonClicked:(NSString *)date;

@end

@interface TDDatePickerView : UIView <UIPickerViewDataSource, UIPickerViewDelegate>

@property (nonatomic, strong) UIPickerView *pickerView;
@property (nonatomic, strong) UIButton *cancelButton;
@property (nonatomic, strong) UIButton *confirmButton;

@property (nonatomic, strong) NSMutableArray *yearArray;
@property (nonatomic, strong) NSMutableArray * yearForShowArray;

@property (nonatomic, strong) NSArray *monthArray;
@property (nonatomic, strong) NSArray *monthForShowArray;

@property (nonatomic, strong) NSMutableArray *daysArray;
@property (nonatomic, strong) NSMutableArray *daysForShowArray;

@property (nonatomic, strong) NSArray *hoursArray;
@property (nonatomic, strong) NSArray *hoursForShowArray;

@property (nonatomic, strong) NSMutableArray *minutesArray;
@property (nonatomic, strong) NSMutableArray *minutesForShowArray;

@property (nonatomic, assign) NSInteger selectedRowYear;
@property (nonatomic, assign) NSInteger selectedRowMonth;
@property (nonatomic, assign) NSInteger selectedRowDay;

@property (nonatomic, assign) CGPoint homePosition;
@property (nonatomic, assign) CGPoint targetPosition;
@property (nonatomic, strong) UIView *maskView;

@property (nonatomic, weak) id <TDDatePickerViewDelegate> delegate;

/**
 *  初始化日期选择器（宽度为300，高度为244，其中日期高度位200，“取消”和“确定”按钮的高度是44,宽度为150）
 *
 *  @param frame          在父视图上的位置
 *  @param dateComponents 日期组件，指定默认显示日期
 *  @param targetPosition 指定移动到父视图的某个位置
 *
 *  @return 返回UIView的实例
 */
- (instancetype)initWithFrame:(CGRect)frame dateComponents:(NSDateComponents *)dateComponents targetPosition:(CGPoint)targetPosition;

/**
 *  初始化日期
 *
 *  @param dateComponents NSDateComponents日期格式
 */
- (void)initDataWithDateComponents:(NSDateComponents *)dateComponents;

/**
 *  刷新日期
 */
- (void)refreshDate;
/**
 *  视图移动到指定位置
 */
- (void)moveToTargetPositon;

/**
 *  视图移回原位
 */
- (void)moveToHomePositon;

@end
