//
//  TDUserBriefInfoView.h
//  AfterSchool
//
//  Created by Teson Draw on 11/27/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TDUserBriefInfoView : UIView

@property (nonatomic, strong) UIImageView *portraitImageView;
@property (nonatomic, strong) UIImageView *sexImageView;
@property (nonatomic, strong) UIImageView *rightArrowImageView;

@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *horoscopeLabel;
@property (nonatomic, strong) UILabel *signatureLabel;
@property (nonatomic, strong) UILabel *ageLabel;

@property (nonatomic, strong) UIButton *button;

@end
