//
//  TDUserBriefInfoView.m
//  AfterSchool
//
//  Created by Teson Draw on 11/27/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "TDUserBriefInfoView.h"
#import "MacrosDefinition.h"

@implementation TDUserBriefInfoView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        
        [self addSubview:self.portraitImageView];
        [self addSubview:self.nameLabel];
        [self addSubview:self.sexImageView];
        [_sexImageView addSubview:self.ageLabel];
        
        [self addSubview:self.horoscopeLabel];
        [self addSubview:self.signatureLabel];
        [self addSubview:self.rightArrowImageView];
        [self addSubview:self.button];
    }
    return self;
}

- (UIImageView *)portraitImageView {
    if (!_portraitImageView) {
        _portraitImageView = [[UIImageView alloc] initWithFrame:CGRectMake(12, 14, 80, 80)];
        _portraitImageView.layer.cornerRadius = 40;
        _portraitImageView.layer.masksToBounds = YES;
    }
    return _portraitImageView;
}

- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_portraitImageView.frame) + 12, 28, SCREEN_WIDTH - CGRectGetMaxX(_portraitImageView.frame) - 12 - 24, 20)];
        _nameLabel.textColor = [UIColor blackColor];
        _nameLabel.font = [UIFont systemFontOfSize:18];
    }
    return _nameLabel;
}

- (UIImageView *)sexImageView {
    if (!_sexImageView) {
        _sexImageView = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_portraitImageView.frame) + 12, CGRectGetMaxY(_nameLabel.frame) + 10, 30, 12)];
        _sexImageView.layer.cornerRadius = 3;
        _sexImageView.layer.masksToBounds = YES;
    }
    return _sexImageView;
}

- (UILabel *)ageLabel {
    if (!_ageLabel) {
        _ageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, _sexImageView.bounds.size.width - 2, _sexImageView.bounds.size.height)];
        _ageLabel.textColor = [UIColor whiteColor];
        _ageLabel.font = [UIFont systemFontOfSize:12];
        _ageLabel.text = @"27";
        _ageLabel.textAlignment = NSTextAlignmentRight;
    }
    return _ageLabel;
}

- (UILabel *)horoscopeLabel {
    if (!_horoscopeLabel) {
        _horoscopeLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_sexImageView.frame) + 8, CGRectGetMaxY(_nameLabel.frame) + 10, 100, 12)];
        _horoscopeLabel.textColor = [UIColor colorWithRed:120/255.f green:120/255.f blue:120/255.f alpha:1];
        _horoscopeLabel.font = [UIFont systemFontOfSize:12];
    }
    return _horoscopeLabel;
}

- (UILabel *)signatureLabel {
    if (!_signatureLabel) {
        _signatureLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_portraitImageView.frame) + 12, 110 - 12 - 16, SCREEN_WIDTH - CGRectGetMaxX(_portraitImageView.frame) - 12 - 24, 12)];
        _signatureLabel.textColor = [UIColor colorWithRed:120/255.f green:120/255.f blue:120/255.f alpha:1];
        _signatureLabel.font = [UIFont systemFontOfSize:12];
    }
    return _signatureLabel;
}

- (UIImageView *)rightArrowImageView {
    if (!_rightArrowImageView) {
        _rightArrowImageView = [[UIImageView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - 44, (self.bounds.size.height - 44) / 2, 44, 44)];
        _rightArrowImageView.image = [UIImage imageNamed:@"dakaInfo_cell_right"];
    }
    return _rightArrowImageView;
}

- (UIButton *)button {
    if (!_button) {
        _button = [[UIButton alloc] initWithFrame:self.bounds];
    }
    
    return _button;
}

@end
