//
//  NavigationBarView.h
//  Package
//
//  Created by wangzheng on 14/12/11.
//  Copyright (c) 2014年 wangzheng. All rights reserved.
//

#import <UIKit/UIKit.h>

#define IS_IOS6 [[UIDevice currentDevice].systemVersion floatValue] < 7
#define STATUS_BAR_HEIGHT (IS_IOS6?0:20)


@interface NavigationBarView : UIView

@property (strong, nonatomic) UILabel   *titleLabel;
@property (nonatomic, strong) UILabel   *leftButtonLabel;

@property (strong, nonatomic) UIButton  *leftButton;
@property (strong, nonatomic) UIButton  *rightButton;
@property (nonatomic, strong) UIButton  *handledCategoryButton;

@property (nonatomic, strong) UIView    *leftButtonBackgroundView;

@property (nonatomic, strong) UIImageView *leftButtonImageView;
@property (nonatomic, strong) UIImageView *rightButtonImageView;

/**
 *  背景为图片的导航栏
 *
 *  @param title                导航条居中显示的标题名字
 *  @param titleColor           标题的字体颜色
 *  @param backgroundImageName  导航条背景的图片名字
 *  @param leftButtonImageName  导航条左部的按钮的图片名字
 *  @param rightButtonImageName 导航条右部的按钮的图片名字
 *
 *  @return 导航条UIView的实例
 */
- (instancetype)initWithTitle:(NSString *)title
                   titleColor:(UIColor *)titleColor
          backgroundImageName:(NSString *)backgroundImageName
          leftButtonImageName:(NSString *)leftButtonImageName
         rightButtonImageName:(NSString *)rightButtonImageName;

/**
 *  导航条背景为颜色，按钮的背景图片使用Button自带
 *
 *  @param title                导航条居中显示的标题名字
 *  @param titleColor           标题的字体颜色
 *  @param backgroundColor      导航条背景色
 *  @param leftButtonImageName  导航条左部的按钮的图片名字
 *  @param rightButtonImageName 导航条右部的按钮的图片名字
 *
 *  @return 导航条UIView的实例
 */
- (instancetype)initWithTitle:(NSString *)title
                   titleColor:(UIColor *)titleColor
              backgroundColor:(UIColor *)backgroundColor
          leftButtonImageName:(NSString *)leftButtonImageName
         rightButtonImageName:(NSString *)rightButtonImageName;

/**
 *  导航条背景为颜色，直接设置按钮的背景图片，可使用中文按钮
 *
 *  @param title                导航条居中显示的标题名字
 *  @param titleColor           标题的字体颜色
 *  @param backgroundColor      导航条背景的图片名字
 *  @param leftButtonName       导航条左部的按钮的标题名字
 *  @param leftButtonTitleColor 导航条左部的按钮的标题颜色
 *  @param leftButtonImageName  导航条左部的按钮背景图片的名字
 *  @param rightButtonName      导航条右部的按钮的标题名字
 *  @param rightButtonImageName 导航条右部的按钮背景图片的名字
 *
 *  @return 导航条UIView的实例
 */
- (instancetype)initWithTitle:(NSString *)title
                   titleColor:(UIColor *)titleColor
              backgroundColor:(UIColor *)backgroundColor
               leftButtonName:(NSString *)leftButtonName
         leftButtonTitleColor:(UIColor *)leftButtonTitleColor
          leftButtonImageName:(NSString *)leftButtonImageName
              rightButtonName:(NSString *)rightButtonName
         rightButtonImageName:(NSString *)rightButtonImageName;

/**
 *  导航条- 左部按钮背景图片使用独立的UIImageView加汉字组合，右部按钮为直接设置按钮背景图片
 *
 *  @param title                导航条居中显示的标题名字
 *  @param titleColor           标题的字体颜色
 *  @param backgroundColor      导航条背景的图片名字
 *  @param leftButtonBgViewOriginX  导航条左部的按钮背景视图X轴起始位置
 *  @param leftButtonImageWidth 导航条左部按钮背景图片宽度
 *  @param leftButtonTitleColor 导航条左部的按钮的标题颜色
 *  @param leftButtonName       导航条左部的按钮的标题名字
 *  @param leftButtonImageName  导航条左部的按钮背景图片的名字
 *  @param rightButtonName      导航条右部的按钮的标题名字
 *  @param rightButtonImageName 导航条右部的按钮背景图片的名字
 *
 *  @return 导航条UIView的实例
 */
- (instancetype)initWithTitle:(NSString *)title
                   titleColor:(UIColor *)titleColor
              backgroundColor:(UIColor *)backgroundColor
      leftButtonBgViewOriginX:(CGFloat)leftButtonBgViewOriginX
         leftButtonImageWidth:(CGFloat)leftButtonImageWidth
         leftButtonTitleColor:(UIColor *)leftButtonTitleColor
               leftButtonName:(NSString *)leftButtonName
          leftButtonImageName:(NSString *)leftButtonImageName
              rightButtonName:(NSString *)rightButtonName
         rightButtonImageName:(NSString *)rightButtonImageName;


/**
 *  自定义导航条 （适用于左部有单纯汉字按钮或单纯有图片按钮，右部无按钮的情况）
 *
 *  @param title                导航条居中显示的标题名字
 *  @param titleColor           标题的字体颜色
 *  @param backgroundColor      导航条背景的图片名字
 *  @param leftButtonImageName  左部的按钮背景图片
 *  @param leftButtonImageOriginX 左部图片起始位置
 *  @param leftButtonImageHeight 导航条左部按钮背景图片高度
 *  @param leftButtonOriginX    左部按钮起始位置
 *  @param leftButtonName       导航条左部的按钮的标题名字
 *  @param leftButtonTitleColor 导航条左部的按钮的标题颜色
 *
 *  @return 导航条UIView的实例
 */
- (instancetype)initWithTitle:(NSString *)title
                   titleColor:(UIColor *)titleColor
              backgroundColor:(UIColor *)backgroundColor
          leftButtonImageName:(NSString *)leftButtonImageName
       leftButtonImageOriginX:(CGFloat)leftButtonImageOriginX
        leftButtonImageHeight:(CGFloat)leftButtonImageHeight
            leftButtonOriginX:(CGFloat)leftButtonOriginX
               leftButtonName:(NSString *)leftButtonName
         leftButtonTitleColor:(UIColor *)leftButtonTitleColor;

/**
 *  自定义导航条 （适用左右不含文字，都是图片的按钮）
 *
 *  @param title                导航条居中显示的标题名字
 *  @param titleColor           标题的字体颜色
 *  @param backgroundColor      导航条背景的图片名字
 *  @param leftButtonImageName  左部的按钮背景图片
 *  @param leftButtonImageOriginX 左部图片起始位置
 *  @param leftButtonImageHeight 导航条左部按钮背景图片高度
 *  @param leftButtonOriginX    左部按钮起始位置
 *  @param leftButtonName       导航条左部的按钮的标题名字
 *  @param leftButtonTitleColor 导航条左部的按钮的标题颜色
 *  @param rightButtonImageName 导航条右部的按钮背景图片的名字
 *  @param rightButtonImageOriginX 右部图片起始位置
 *  @param rightButtonImageHeight 右部图片高度
 *  @param rightButtonOriginX   右部按钮起始位置
 *  @param rightButtonName      导航条右部的按钮的标题名字
 *  @param rightButtonTitleColor  导航条右部按钮的标题颜色
 *
 *  @return 导航条UIView的实例
 */
- (instancetype)initWithTitle:(NSString *)title
                   titleColor:(UIColor *)titleColor
              backgroundColor:(UIColor *)backgroundColor
          leftButtonImageName:(NSString *)leftButtonImageName
       leftButtonImageOriginX:(CGFloat)leftButtonImageOriginX
        leftButtonImageHeight:(CGFloat)leftButtonImageHeight
            leftButtonOriginX:(CGFloat)leftButtonOriginX
               leftButtonName:(NSString *)leftButtonName
         leftButtonTitleColor:(UIColor *)leftButtonTitleColor
         rightButtonImageName:(NSString *)rightButtonImageName
      rightButtonImageOriginX:(CGFloat)rightButtonImageOriginX
       rightButtonImageHeight:(CGFloat)rightButtonImageHeight
           rightButtonOriginX:(CGFloat)rightButtonOriginX
              rightButtonName:(NSString *)rightButtonName
        rightButtonTitleColor:(UIColor *)rightButtonTitleColor;

@end
