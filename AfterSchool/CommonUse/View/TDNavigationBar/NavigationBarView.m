//
//  NavigationBarView.m
//  Package
//
//  Created by wangzheng on 14/12/11.
//  Copyright (c) 2014年 wangzheng. All rights reserved.
//

//#define IS_IOS6 [[UIDevice currentDevice].systemVersion floatValue] < 7
//#define STATUS_BAR_HEIGHT (IS_IOS6?0:20)

#import "NavigationBarView.h"
#import "MacrosDefinition.h"

#define TITLE_LABEL_FONT_SIZE 18
#define LEFT_OR_RIGHT_BUTTON_FONT_SIZE 16
#define TITLE_LABEL_FONT_COLOR [UIColor whiteColor]
#define NAVIGATIONBAR_DEFAULT_COLOR [UIColor colorWithRed:38/255.f green:166/255.f blue:154/255.f alpha:1]


@implementation NavigationBarView

- (instancetype)initWithTitle:(NSString *)title
                   titleColor:(UIColor *)titleColor
          backgroundImageName:(NSString *)backgroundImageName
          leftButtonImageName:(NSString *)leftButtonImageName
         rightButtonImageName:(NSString *)rightButtonImageName
{
    self  = [super initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 64)];
    UIImageView *background = [[UIImageView alloc]initWithFrame:self.bounds];
    background.image = [UIImage imageNamed:backgroundImageName];
    [self addSubview:background];
    
    _leftButton = [[UIButton alloc]initWithFrame:CGRectMake(0, STATUS_BAR_HEIGHT, 44, 44)];
    [_leftButton setBackgroundImage:[UIImage imageNamed:leftButtonImageName] forState:UIControlStateNormal];
    [self addSubview:_leftButton];
    
    if (rightButtonImageName) {
        _rightButton = [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH - 50, 20, 44, 44)];
        [_rightButton setBackgroundImage:[UIImage imageNamed:rightButtonImageName] forState:UIControlStateNormal];
        [self addSubview:_rightButton];
    }
    
    if (title) {
        _titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, STATUS_BAR_HEIGHT, SCREEN_WIDTH, 44)];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.font = [UIFont boldSystemFontOfSize:TITLE_LABEL_FONT_SIZE];
        _titleLabel.text = title;
        _titleLabel.textColor = TITLE_LABEL_FONT_COLOR;
        
        if (titleColor) {
            _titleLabel.textColor = titleColor;
        }
        
        [self addSubview:_titleLabel];
    }
    
    return self;
}

- (instancetype)initWithTitle:(NSString *)title
                   titleColor:(UIColor *)titleColor
              backgroundColor:(UIColor *)backgroundColor
          leftButtonImageName:(NSString *)leftButtonImageName
         rightButtonImageName:(NSString *)rightButtonImageName
{
    self  = [super initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 64)];
    
    if (backgroundColor) {
        self.backgroundColor = backgroundColor;
    } else {
        self.backgroundColor = NAVIGATIONBAR_DEFAULT_COLOR;
    }

//    [self addEffectViewWithBackColor:backgroundColor];
    
    
    if (leftButtonImageName) {
        _leftButton = [[UIButton alloc]initWithFrame:CGRectMake(10, STATUS_BAR_HEIGHT, 44, 44)];
        [_leftButton setBackgroundImage:[UIImage imageNamed:leftButtonImageName] forState:UIControlStateNormal];
        
        [self addSubview:_leftButton];
    }

    if (rightButtonImageName) {
        _rightButton = [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH - 50, 20, 44, 44)];
        [_rightButton setBackgroundImage:[UIImage imageNamed:rightButtonImageName] forState:UIControlStateNormal];
        [self addSubview:_rightButton];
    }
    
    if (title) {
        _titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, STATUS_BAR_HEIGHT, SCREEN_WIDTH, 44)];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.font = [UIFont boldSystemFontOfSize:TITLE_LABEL_FONT_SIZE];
        _titleLabel.text = title;
        _titleLabel.textColor = TITLE_LABEL_FONT_COLOR;
        
        if (titleColor) {
            _titleLabel.textColor = titleColor;
        }
        
        [self addSubview:_titleLabel];
    }
    
    return self;
}

- (instancetype)initWithTitle:(NSString *)title
                   titleColor:(UIColor *)titleColor
              backgroundColor:(UIColor *)backgroundColor
               leftButtonName:(NSString *)leftButtonName
         leftButtonTitleColor:(UIColor *)leftButtonTitleColor
          leftButtonImageName:(NSString *)leftButtonImageName
              rightButtonName:(NSString *)rightButtonName
         rightButtonImageName:(NSString *)rightButtonImageName
{
    self  = [super initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 64)];
    

    if (backgroundColor) {
        self.backgroundColor = backgroundColor;
    } else {
        self.backgroundColor = NAVIGATIONBAR_DEFAULT_COLOR;
    }
    
//    [self addEffectViewWithBackColor:backgroundColor];
    
    if (leftButtonName || leftButtonImageName) {
        _leftButton = [[UIButton alloc]initWithFrame:CGRectMake(10, STATUS_BAR_HEIGHT, 44, 44)];
        
        if (leftButtonName) {
            _leftButton = [[UIButton alloc]initWithFrame:CGRectMake(10, STATUS_BAR_HEIGHT, 80, 44)];
            [_leftButton setTitle:leftButtonName forState:UIControlStateNormal];
            if (leftButtonTitleColor) {
                [_leftButton setTitleColor:leftButtonTitleColor forState:UIControlStateNormal];
            }
            
            _leftButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        }
        
        if (leftButtonImageName) {
            [_leftButton setBackgroundImage:[UIImage imageNamed:leftButtonImageName] forState:UIControlStateNormal];
        }
        
        [self addSubview:_leftButton];
    }
    
    if (rightButtonImageName || rightButtonName) {
        _rightButton = [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH - 54, STATUS_BAR_HEIGHT, 44, 44)];
        
        if (rightButtonName) {
            NSInteger length = rightButtonName.length;
            [_rightButton setFrame:CGRectMake(SCREEN_WIDTH - 15 * length - 20, STATUS_BAR_HEIGHT, 15 * length + 10, 44)];
            [_rightButton setTitle:rightButtonName forState:UIControlStateNormal];
            [_rightButton setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
        }
        
        if (rightButtonImageName) {
            [_rightButton setBackgroundImage:[UIImage imageNamed:rightButtonImageName] forState:UIControlStateNormal];
        }

        [self addSubview:_rightButton];
    }
    
    if (title) {
        _titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, STATUS_BAR_HEIGHT, SCREEN_WIDTH, 44)];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.font = [UIFont boldSystemFontOfSize:TITLE_LABEL_FONT_SIZE];
        _titleLabel.text = title;
        _titleLabel.textColor = TITLE_LABEL_FONT_COLOR;
        
        if (titleColor) {
            _titleLabel.textColor = titleColor;
        }
        
        [self addSubview:_titleLabel];
    }
    
    return self;
}

- (instancetype)initWithTitle:(NSString *)title
                   titleColor:(UIColor *)titleColor
              backgroundColor:(UIColor *)backgroundColor
      leftButtonBgViewOriginX:(CGFloat)leftButtonBgViewOriginX
         leftButtonImageWidth:(CGFloat)leftButtonImageWidth
         leftButtonTitleColor:(UIColor *)leftButtonTitleColor
               leftButtonName:(NSString *)leftButtonName
          leftButtonImageName:(NSString *)leftButtonImageName
              rightButtonName:(NSString *)rightButtonName
         rightButtonImageName:(NSString *)rightButtonImageName
{
    self  = [super initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 64)];
    
    if (backgroundColor) {
        self.backgroundColor = backgroundColor;
    } else {
        self.backgroundColor = NAVIGATIONBAR_DEFAULT_COLOR;
    }
    
//    [self addEffectViewWithBackColor:backgroundColor];
    
    _leftButtonBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(leftButtonBgViewOriginX, STATUS_BAR_HEIGHT, 100, 44)];
    _leftButtonImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, leftButtonImageWidth, 44)];
    _leftButtonImageView.image = [UIImage imageNamed:leftButtonImageName];
    
    _leftButtonLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_leftButtonImageView.frame) + 5, 0, 100 - 22 - 2, 44)];
    [_leftButtonLabel setFont:[UIFont systemFontOfSize:LEFT_OR_RIGHT_BUTTON_FONT_SIZE]];
    [_leftButtonLabel setTextColor:[UIColor whiteColor]];
    
    if (leftButtonTitleColor) {
        [_leftButtonLabel setTextColor:leftButtonTitleColor];
    } else {
        [_leftButtonLabel setTextColor:[UIColor whiteColor]];
    }
    
    _leftButtonLabel.text = leftButtonName;
    
    _leftButton = [[UIButton alloc]initWithFrame:_leftButtonBackgroundView.bounds];
    [_leftButton.titleLabel setFont:[UIFont systemFontOfSize:LEFT_OR_RIGHT_BUTTON_FONT_SIZE]];
    
    [_leftButtonBackgroundView addSubview:_leftButtonImageView];
    [_leftButtonBackgroundView addSubview:_leftButtonLabel];
    [_leftButtonBackgroundView addSubview:_leftButton];

    
    [self addSubview:_leftButtonBackgroundView];
    
    if (title) {
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, STATUS_BAR_HEIGHT, SCREEN_WIDTH, 44)];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.font = [UIFont boldSystemFontOfSize:TITLE_LABEL_FONT_SIZE];
        _titleLabel.text = title;
        _titleLabel.textColor = TITLE_LABEL_FONT_COLOR;
        
        if (titleColor) {
            _titleLabel.textColor = titleColor;
        }
        
        [self addSubview:_titleLabel];
    }
    
    if (rightButtonImageName || rightButtonName) {
        _rightButton = [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH - 54, STATUS_BAR_HEIGHT, 44, 44)];
        
        if (rightButtonName) {
            NSInteger length = rightButtonName.length;
            [_rightButton.titleLabel setFont:[UIFont systemFontOfSize:LEFT_OR_RIGHT_BUTTON_FONT_SIZE]];
            [_rightButton setFrame:CGRectMake(SCREEN_WIDTH - 15 * length - 20, STATUS_BAR_HEIGHT, 15 * length + 10, 44)];
            [_rightButton setTitle:rightButtonName forState:UIControlStateNormal];
            [_rightButton setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
        }
        
        if (rightButtonImageName) {
            [_rightButton setBackgroundImage:[UIImage imageNamed:rightButtonImageName] forState:UIControlStateNormal];
        }
        
        [self addSubview:_rightButton];
    }
    
    return self;
}

- (instancetype)initWithTitle:(NSString *)title
                   titleColor:(UIColor *)titleColor
              backgroundColor:(UIColor *)backgroundColor
          leftButtonImageName:(NSString *)leftButtonImageName
       leftButtonImageOriginX:(CGFloat)leftButtonImageOriginX
        leftButtonImageHeight:(CGFloat)leftButtonImageHeight
            leftButtonOriginX:(CGFloat)leftButtonOriginX
               leftButtonName:(NSString *)leftButtonName
         leftButtonTitleColor:(UIColor *)leftButtonTitleColor
{
    self  = [super initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 64)];
    
    if (backgroundColor) {
        self.backgroundColor = backgroundColor;
    } else {
        self.backgroundColor = NAVIGATIONBAR_DEFAULT_COLOR;
    }
    
//    [self addEffectViewWithBackColor:backgroundColor];
    
    /**
     *  标题
     */
    if (title) {
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, STATUS_BAR_HEIGHT, SCREEN_WIDTH, 44)];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.font = [UIFont boldSystemFontOfSize:TITLE_LABEL_FONT_SIZE];
        _titleLabel.text = title;
        _titleLabel.textColor = TITLE_LABEL_FONT_COLOR;
        
        if (titleColor) {
            _titleLabel.textColor = titleColor;
        }
        
        [self addSubview:_titleLabel];
    }
    
    /**
     *  左部按钮图片
     */
    if (leftButtonImageName) {
        _leftButtonImageView = [[UIImageView alloc] initWithFrame:CGRectMake(leftButtonImageOriginX, 20 + (44 - leftButtonImageHeight) / 2, leftButtonImageHeight, leftButtonImageHeight)];
        _leftButtonImageView.image = [UIImage imageNamed:leftButtonImageName];
        _leftButtonImageView.layer.cornerRadius = 5;
        
        [self addSubview:self.leftButtonImageView];
    }
    
    _leftButton = [[UIButton alloc]initWithFrame:CGRectMake(leftButtonOriginX, 20, 60, 44)];
    
    /**
     *  左部文字按钮
     */
    if (leftButtonName) {
        [_leftButton.titleLabel setFont:[UIFont systemFontOfSize:LEFT_OR_RIGHT_BUTTON_FONT_SIZE]];
        [_leftButton setTitle:leftButtonName forState:UIControlStateNormal];
        [_leftButton setTitleColor:leftButtonTitleColor forState:UIControlStateNormal];
    }
    
    [self addSubview:_leftButton];
    
    return self;
}

- (instancetype)initWithTitle:(NSString *)title
                   titleColor:(UIColor *)titleColor
              backgroundColor:(UIColor *)backgroundColor
          leftButtonImageName:(NSString *)leftButtonImageName
       leftButtonImageOriginX:(CGFloat)leftButtonImageOriginX
        leftButtonImageHeight:(CGFloat)leftButtonImageHeight
            leftButtonOriginX:(CGFloat)leftButtonOriginX
               leftButtonName:(NSString *)leftButtonName
         leftButtonTitleColor:(UIColor *)leftButtonTitleColor
         rightButtonImageName:(NSString *)rightButtonImageName
      rightButtonImageOriginX:(CGFloat)rightButtonImageOriginX
       rightButtonImageHeight:(CGFloat)rightButtonImageHeight
           rightButtonOriginX:(CGFloat)rightButtonOriginX
              rightButtonName:(NSString *)rightButtonName
        rightButtonTitleColor:(UIColor *)rightButtonTitleColor
{
    self  = [super initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 64)];
    
    if (backgroundColor) {
        self.backgroundColor = backgroundColor;
    } else {
        self.backgroundColor = NAVIGATIONBAR_DEFAULT_COLOR;
    }
    
    [self addEffectViewWithBackColor:backgroundColor];
    
    /**
     *  标题
     */
    if (title) {
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, STATUS_BAR_HEIGHT, SCREEN_WIDTH, 44)];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.font = [UIFont boldSystemFontOfSize:TITLE_LABEL_FONT_SIZE];
        _titleLabel.text = title;
        _titleLabel.textColor = TITLE_LABEL_FONT_COLOR;
        
        if (titleColor) {
            _titleLabel.textColor = titleColor;
        }
        
        [self addSubview:_titleLabel];
    }
    
    /**
     *  左部按钮图片
     */
    if (leftButtonImageName) {
        _leftButtonImageView = [[UIImageView alloc] initWithFrame:CGRectMake(leftButtonImageOriginX, 20 + (44 - leftButtonImageHeight) / 2, leftButtonImageHeight, leftButtonImageHeight)];
        _leftButtonImageView.image = [UIImage imageNamed:leftButtonImageName];
        _leftButtonImageView.layer.cornerRadius = 5;
        
        [self addSubview:self.leftButtonImageView];
    }

    _leftButton = [[UIButton alloc]initWithFrame:CGRectMake(leftButtonOriginX, 20, 60, 44)];
    
    /**
     *  左部文字按钮
     */
    if (leftButtonName) {
        [_leftButton.titleLabel setFont:[UIFont systemFontOfSize:LEFT_OR_RIGHT_BUTTON_FONT_SIZE]];
        [_leftButton setTitle:leftButtonName forState:UIControlStateNormal];
        [_leftButton setTitleColor:leftButtonTitleColor forState:UIControlStateNormal];
    }
    
    [self addSubview:_leftButton];
    
    /**
     *  右部按钮图片
     */
    if (rightButtonImageName) {
        _rightButtonImageView = [[UIImageView alloc] initWithFrame:CGRectMake(rightButtonImageOriginX, 20 + (44 - rightButtonImageHeight) / 2, rightButtonImageHeight, rightButtonImageHeight)];
        _rightButtonImageView.image = [UIImage imageNamed:rightButtonImageName];
        
        [self addSubview:self.rightButtonImageView];
    }
    
    _rightButton = [[UIButton alloc]initWithFrame:CGRectMake(rightButtonOriginX, 20, 60, 44)];
    
    /**
     *  右部文字按钮
     */
    if (rightButtonName) {
        [_rightButton.titleLabel setFont:[UIFont systemFontOfSize:LEFT_OR_RIGHT_BUTTON_FONT_SIZE]];
        [_rightButton setTitle:rightButtonName forState:UIControlStateNormal];
        [_rightButton setTitleColor:rightButtonTitleColor forState:UIControlStateNormal];
    }
    
    [self addSubview:_rightButton];
    
    return self;
}

- (UIButton *)handledCategoryButton {
    if (!_handledCategoryButton) {
        _handledCategoryButton = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - 100, 20, 44, 44)];
        [_handledCategoryButton setBackgroundImage:[UIImage imageNamed:@"application_handled"] forState:UIControlStateNormal];
        _handledCategoryButton.selected = YES;
    }
    return _handledCategoryButton;
}

- (UIButton *)rightButton {
    if (!_rightButton) {
        _rightButton = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - 14 - 44, 20, 44, 44)];
    }
    
    return _rightButton;
}


- (void)addEffectViewWithBackColor:(UIColor *)backgroundColor {
    
    if (iOS8) {
        self.backgroundColor = [UIColor clearColor];
        
        UIBlurEffect * blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
        UIVisualEffectView * effectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        [effectView setFrame:self.bounds];
        
        [self addSubview:effectView];
        UIView * backView = [[UIView alloc] initWithFrame:self.bounds];
        [backView setBackgroundColor:[UIColor colorWithWhite:0 alpha:0.2]];
        [effectView.contentView addSubview:backView];
        
        if (backgroundColor) {
            [backView setBackgroundColor:backgroundColor];
        }
    } else {
        if (backgroundColor) {
            self.backgroundColor = backgroundColor;
        } else {
            self.backgroundColor = NAVIGATIONBAR_DEFAULT_COLOR;
        }
    }
}

@end
