//
//  LGCollectionView.h
//  AfterSchool
//
//  Created by lg on 15/11/11.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITableView+NibCell.h"

typedef NS_ENUM(NSInteger, LGCollectionViewHeadType) {
    LGCollectionViewHeadTypeDefault = 0,///<默认为LGScrollViewHeadTypeLight
    LGCollectionViewHeadTypeDark,
    LGCollectionViewHeadTypeLight = LGCollectionViewHeadTypeDefault,
};

typedef NS_ENUM(NSInteger, LGCollectionViewBackgroundType) {
    LGCollectionViewBackgroundTypeDefault = 0,///<默认为LGScrollViewBackgroundTypeLight
    LGCollectionViewBackgroundTypeDark,
    LGCollectionViewBackgroundTypeLight = LGCollectionViewHeadTypeDefault,
};

@interface LGCollectionView : UICollectionView

- (void)setBackgroundType:(LGCollectionViewBackgroundType)LGCollectionViewBackgroundType;
- (void)setHeadType:(LGCollectionViewHeadType)LGCollectionViewHeadType;

@end
