//
//  TDInputTextView.h
//  AfterSchool
//
//  Created by Teson Draw on 11/12/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TDTextField.h"

@protocol TDInputTextViewDelegate <NSObject>

@optional
- (void)inputTextViewCancelButtonClicked:(UIButton *)sender;
- (void)inputTextViewConfirmButtonClickedWithData:(NSDictionary *)data;
//- (void)inputTextViewConfirmButtonClicked:(UIButton *)sender result:(NSString *)result;

@end

@interface TDInputTextView : UIView

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) TDTextField *inputTextField;
@property (nonatomic, strong) UIButton *cancelButton;
@property (nonatomic, strong) UIButton *confirmButton;

@property (nonatomic, assign) CGPoint homePosition;
@property (nonatomic, assign) CGPoint targetPosition;
@property (nonatomic, strong) UIView *maskView;

@property (nonatomic, assign) id <TDInputTextViewDelegate> delegate;

/**
 *  初始化文本输入框（宽度为300，高度为144，“取消”和“确定”按钮的高度是44,宽度为150）
 *
 *  @param frame          在父视图上的位置
 *  @param targetPosition 指定移动到父视图的某个位置
 *
 *  @return 返回UIView的实例
 */
- (instancetype)initWithFrame:(CGRect)frame targetPosition:(CGPoint)targetPosition;

/**
 *  视图移动到指定位置
 */
- (void)moveToTargetPositon;

/**
 *  视图移回原位
 */
- (void)moveToHomePositon;

@end
