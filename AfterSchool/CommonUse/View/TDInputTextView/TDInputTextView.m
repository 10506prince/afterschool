//
//  TDInputTextView.m
//  AfterSchool
//
//  Created by Teson Draw on 11/12/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "TDInputTextView.h"
#import "UIButton+SetBackgroundColor.h"
#import "MacrosDefinition.h"

@implementation TDInputTextView

- (instancetype)initWithFrame:(CGRect)frame
               targetPosition:(CGPoint)targetPosition
{
    self = [super initWithFrame:frame];
    if (self) {
        self.layer.borderColor = [UIColor colorWithRed:196/255.f green:196/255.f blue:196/255.f alpha:1].CGColor;
        self.layer.borderWidth = 1;
        self.layer.cornerRadius = 6;
        self.layer.masksToBounds = YES;
        self.backgroundColor = [UIColor whiteColor];
        
        _homePosition = CGPointMake(frame.origin.x, frame.origin.y);
        _targetPosition = targetPosition;
        
        [self initUserInterface];
    }
    
    return self;
}

- (void)initUserInterface {
    [self addSubview:self.titleLabel];
    [self addSubview:self.inputTextField];
    [self addSubview:self.cancelButton];
    [self addSubview:self.confirmButton];
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 14, 300, 16)];
        _titleLabel.textColor = [UIColor blackColor];
        _titleLabel.font = [UIFont systemFontOfSize:18];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _titleLabel;
}

- (TDTextField *)inputTextField {
    if (!_inputTextField) {
        _inputTextField = [[TDTextField alloc] initWithFrame:CGRectMake(12, 40, 300 - 24, 44)];
        _inputTextField.placeholder = @"请输入内容";
        _inputTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
        _inputTextField.returnKeyType = UIReturnKeyDefault;
        _inputTextField.keyboardType = UIKeyboardTypeDefault;
        _inputTextField.autocorrectionType = UITextAutocorrectionTypeNo;
        _inputTextField.layer.cornerRadius = 5;
        
        _inputTextField.layer.borderColor = [UIColor colorWithRed:196/255.f green:196/255.f blue:196/255.f alpha:1].CGColor;
        _inputTextField.layer.borderWidth = 1;
    }
    return _inputTextField;
}

- (UIButton *)cancelButton {
    if (!_cancelButton) {
        _cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 100, 300 / 2, 44)];
        [_cancelButton setTitle:@"取消" forState:UIControlStateNormal];
        [_cancelButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_cancelButton setBackgroundColor:[UIColor grayColor] forState:UIControlStateHighlighted];
        _cancelButton.titleLabel.font = [UIFont systemFontOfSize:18];
        _cancelButton.layer.borderColor = [UIColor colorWithRed:196/255.f green:196/255.f blue:196/255.f alpha:1].CGColor;
        _cancelButton.layer.borderWidth = 1;
        [_cancelButton addTarget:self action:@selector(cancelButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _cancelButton;
}

- (void)cancelButtonClicked:(UIButton *)sender {
    [self moveToHomePositon];
    if (_delegate && [_delegate respondsToSelector:@selector(inputTextViewCancelButtonClicked:)]) {
        [_delegate inputTextViewCancelButtonClicked:sender];
    }
}

- (UIButton *)confirmButton {
    if (!_confirmButton) {
        _confirmButton = [[UIButton alloc] initWithFrame:CGRectMake(150 - 1, 100, 150 + 1, 44)];
        [_confirmButton setTitle:@"确定" forState:UIControlStateNormal];
        [_confirmButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_confirmButton setBackgroundColor:[UIColor grayColor] forState:UIControlStateHighlighted];
        
        _confirmButton.titleLabel.font = [UIFont systemFontOfSize:18];
        _confirmButton.layer.borderColor = [UIColor colorWithRed:196/255.f green:196/255.f blue:196/255.f alpha:1].CGColor;
        _confirmButton.layer.borderWidth = 1;
        [_confirmButton addTarget:self action:@selector(confirmButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _confirmButton;
}

- (void)confirmButtonClicked:(UIButton *)sender {
    NSString *resultString = [_inputTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if (resultString.length > 0) {
        if (_delegate && [_delegate respondsToSelector:@selector(inputTextViewConfirmButtonClickedWithData:)]) {
            NSDictionary *dataDic = @{@"Button":sender,
                                      @"Result":resultString};
            [_delegate inputTextViewConfirmButtonClickedWithData:dataDic];
            _inputTextField.text = @"";
        }
        [self moveToHomePositon];
    }
}

- (void)moveToTargetPositon {
    [self.superview addSubview:self.maskView];
    [self.superview addSubview:self];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.2];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    
    self.maskView.alpha = 0.3;
    self.maskView.backgroundColor = [UIColor grayColor];
    
    self.frame = CGRectMake(_targetPosition.x, _targetPosition.y, 300, 144);
    [UIView commitAnimations];
}

- (void)moveToHomePositon {
    
    [UIView beginAnimations:@"RestoreDatePickerViewPosition" context:nil];
    [UIView setAnimationDuration:0.2];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDidStopSelector:@selector(didEndMoveToHomePosition)];
    [UIView setAnimationDelegate:self];
    
    _maskView.alpha = 0;
    
    self.frame = CGRectMake(_homePosition.x, _homePosition.y, 300, 144);
    [UIView commitAnimations];
}

- (void)didEndMoveToHomePosition {
    [self.maskView removeFromSuperview];
    [self removeFromSuperview];
}

- (UIView *)maskView {
    if (!_maskView) {
        _maskView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
        _maskView.backgroundColor = [UIColor clearColor];
        _maskView.userInteractionEnabled = YES;
    }
    return _maskView;
}

- (void)dealloc {
    NSLog(@"TDInputTextView dealloced");
}

@end
