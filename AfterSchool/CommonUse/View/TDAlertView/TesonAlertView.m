//
//  TesonAlertView.m
//  Alert
//
//  Created by Chengfj on 14/12/5.
//  Copyright (c) 2014年 chengfj. All rights reserved.
//

#import "TesonAlertView.h"
#import "MacrosDefinition.h"


@interface TesonAlertView ()

@property (nonatomic,strong) UILabel * msgLabel;
@property (nonatomic,strong) UIView  * mainView;

@end


@implementation TesonAlertView

- (instancetype)initWithFrame:(CGRect)frame
                          msg:(NSString *)msg
                        delay:(NSTimeInterval)delay
{
    self = [super initWithFrame:frame];
    if (self) {
        //先计算背景框的宽度(文字个数 乘以 字体大小)，背景框的宽度大于文字的宽度即可，再让Label居中。
        NSInteger backgroundWidth = msg.length * 18 + 20;
        
        self.msgLabel = [[UILabel alloc] initWithFrame:CGRectMake(SCREEN_WIDTH / 2 - backgroundWidth / 2, 150, backgroundWidth, 60)];
        
        self.msgLabel.backgroundColor = [UIColor colorWithRed:50/255.f green:50/255.f blue:50/255.f alpha:1];
        self.msgLabel.text = msg;
        self.msgLabel.textColor = [UIColor whiteColor];
        self.msgLabel.textAlignment = NSTextAlignmentCenter;
        self.msgLabel.layer.cornerRadius = 6;
        self.msgLabel.layer.masksToBounds = YES;
        self.msgLabel.font = [UIFont systemFontOfSize:18];

        [self addSubview:self.msgLabel];
        [self performSelector:@selector(cancelAlert) withObject:nil afterDelay:delay];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
                          msg:(NSString *)msg
                      confirm:(NSString *)confirm
                       cancel:(NSString *)cancel
{
    self = [super initWithFrame:frame];
    if (self) {
        self.mainView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 200, 80)];
        self.mainView.center = self.center;
        [self.mainView setBackgroundColor:[UIColor whiteColor]];
        [self addSubview:self.mainView];
        
        self.msgLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 40)];
        self.msgLabel.textAlignment = NSTextAlignmentCenter;
        self.msgLabel.text = msg;
        [self.mainView addSubview:self.msgLabel];
        
        UIButton * confirmBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        confirmBtn.frame = CGRectMake(0, 40, 100, 40);
        confirmBtn.backgroundColor = [UIColor greenColor];
        [confirmBtn setTitle:confirm forState:UIControlStateNormal];
        [confirmBtn addTarget:self action:@selector(confirmBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [self.mainView addSubview:confirmBtn];
        
        UIButton * cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        cancelBtn.frame = CGRectMake(100, 40, 100, 40);
        cancelBtn.backgroundColor = [UIColor greenColor];
        [cancelBtn addTarget:self action:@selector(cancelBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [cancelBtn setTitle:cancel forState:UIControlStateNormal];
        [self.mainView addSubview:cancelBtn];
    }
    return self;
}

- (void)confirmBtnClick {
    //*** 判断代理对象是否存在并且响应协议方法
    if (_delegate && [_delegate respondsToSelector:@selector(alertClickConfirm)]) {
        // *** 调用代理对象方法
        [_delegate alertClickConfirm];
    }
    //取消掉弹窗
    [self cancelAlert];
}

- (void)cancelBtnClick {
    [self cancelAlert];
}

- (void)cancelAlert {
    //将视图从父视图移除
    [self removeFromSuperview];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch  *obj =  [[touches allObjects] lastObject];
    UIView *touchView = obj.view;
    if ([touchView isEqual:self.mainView]) {
    } else {
        [self confirmBtnClick];
    }
}

+ (void)showMsg:(NSString *)msg
    cancelDelay:(NSTimeInterval)delay {
    //*** 获取应用程序window
    UIWindow * keyWindow = [[[UIApplication sharedApplication] delegate] window];
    TesonAlertView * alert = [[TesonAlertView alloc] initWithFrame:[[UIScreen mainScreen] bounds] msg:msg delay:delay];
//    [alert setBackgroundColor:[UIColor colorWithWhite:0.3 alpha:0.4]];
    [keyWindow addSubview:alert];
}

+ (void)showMsg:(NSString *)msg confirmTitle:(NSString *)confirm cancel:(NSString *)cancel delegate:(id<TesonAlertViewDelegate>)delegate{
    UIWindow * keyWindow = [[[UIApplication sharedApplication] delegate] window];
    TesonAlertView * alert = [[TesonAlertView alloc] initWithFrame:[[UIScreen mainScreen] bounds] msg:msg confirm:confirm cancel:cancel];
    alert.delegate = delegate;
    [alert setBackgroundColor:[UIColor colorWithWhite:0.3 alpha:0.4]];
    [keyWindow addSubview:alert];
}

//+ (void)showMessageWithTitle:(NSString *)title message:(NSString *)message cancelButtonTitle:(NSString *)cancelButtonTitle

//UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"模拟充值" message:@"请输入充值金额" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"充值", nil];

@end
