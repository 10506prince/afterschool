//
//  TesonAlertView.h
//  Alert
//
//  Created by Chengfj on 14/12/5.
//  Copyright (c) 2014年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>

//申明协议
@protocol TesonAlertViewDelegate;

@interface TesonAlertView : UIView

//代理对象,内存管理属性必须是weak或者asign,对象必须遵循协议,类型为id(任意类型的可以,遵循协议)
@property (weak, nonatomic) id<TesonAlertViewDelegate> delegate;

- (instancetype)initWithFrame:(CGRect)frame
                          msg:(NSString *)msg
                        delay:(NSTimeInterval)delay;

+ (void)showMsg:(NSString *)msg
    cancelDelay:(NSTimeInterval)delay;

+ (void)showMsg:(NSString *)msg
   confirmTitle:(NSString *)confirm
         cancel:(NSString *)cancel
       delegate:(id <TesonAlertViewDelegate>)delegate;

@end

//协议
@protocol TesonAlertViewDelegate <NSObject>

@optional

- (void)alertClickConfirm;
- (void)removeWindow;

@end
