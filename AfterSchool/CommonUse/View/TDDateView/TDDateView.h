//
//  TDDateView.h
//  AfterSchool
//
//  Created by Teson Draw on 11/23/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    LanyardPositonLeft,
    LanyardPositonRight
} LanyardPositon;


@interface TDDateView : UIView

@property (nonatomic, strong) UIView *dateBackgroundView;
@property (nonatomic, strong) UIView *lanyardView;
@property (nonatomic, strong) UILabel *dateLabel;

@property (nonatomic, assign) LanyardPositon lanyardPosition;

- (instancetype)initWithOrigin:(CGPoint)orgin
               lanyardPosition:(LanyardPositon)lanyardPosition;

@end
