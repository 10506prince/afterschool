//
//  TDDateView.m
//  AfterSchool
//
//  Created by Teson Draw on 11/23/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "TDDateView.h"

@implementation TDDateView

- (instancetype)initWithOrigin:(CGPoint)orgin
               lanyardPosition:(LanyardPositon)lanyardPosition
{
    self = [super initWithFrame:CGRectMake(orgin.x, orgin.y, 72, 20)];
    if (self) {
        _lanyardPosition = lanyardPosition;
        
        if (_lanyardPosition == LanyardPositonLeft) {
            [self addSubview:self.lanyardView];
            [self addSubview:self.dateBackgroundView];
        } else {
            [self addSubview:self.dateBackgroundView];
            [self addSubview:self.lanyardView];
        }
    }
    return self;
}

- (UIView *)dateBackgroundView {
    if (!_dateBackgroundView) {
        
        if (_lanyardPosition == LanyardPositonLeft) {
            _dateBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_lanyardView.frame), 0, 62, 20)];
        } else {
            _dateBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 62, 20)];
        }
        
        _dateBackgroundView.backgroundColor = [UIColor whiteColor];
        [_dateBackgroundView addSubview:self.dateLabel];
    }
    return _dateBackgroundView;
}

- (UILabel *)dateLabel {
    if (!_dateLabel) {
        _dateLabel = [[UILabel alloc] initWithFrame:_dateBackgroundView.bounds];
        _dateLabel.textColor = [UIColor colorWithRed:162/255.f green:162/255.f blue:162/255.f alpha:1];
        _dateLabel.font = [UIFont systemFontOfSize:12];
        _dateLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _dateLabel;
}

- (UIView *)lanyardView {
    if (!_lanyardView) {
        
        if (_lanyardPosition == LanyardPositonLeft) {
            _lanyardView = [[UIView alloc] initWithFrame:CGRectMake(0, (20 - 4) / 2, 10, 4)];
        } else {
            _lanyardView = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_dateBackgroundView.frame), (20 - 4) / 2, 10, 4)];
        }

        _lanyardView.backgroundColor = [UIColor whiteColor];
    }
    return _lanyardView;
}

@end
