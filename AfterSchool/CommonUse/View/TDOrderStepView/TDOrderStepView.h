//
//  TDOrderStepView.h
//  AfterSchool
//
//  Created by Teson Draw on 11/23/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TDOrderStepView : UIView

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *detailLabel;
@property (nonatomic, strong) UILabel *timeLabel;

@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UIButton *button;


/**
 *  订单步骤视图
 *
 *  @param origin          起始位置
 *  @param backgroundColor 背景颜色
 *
 *  @return 视图实例
 */
- (instancetype)initWithOrigin:(CGPoint)origin
               backgroundColor:(UIColor *)backgroundColor;

@end
