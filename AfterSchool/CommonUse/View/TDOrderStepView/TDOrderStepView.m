//
//  TDOrderStepView.m
//  AfterSchool
//
//  Created by Teson Draw on 11/23/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "TDOrderStepView.h"
#import "MacrosDefinition.h"

@implementation TDOrderStepView

- (instancetype)initWithOrigin:(CGPoint)origin
               backgroundColor:(UIColor *)backgroundColor
{
    self = [super initWithFrame:CGRectMake(origin.x, origin.y, 200, 50)];
    if (self) {
        if (backgroundColor) {
            self.backgroundColor = backgroundColor;
        } else {
            self.backgroundColor = [UIColor colorWithRed:180/255.f green:201/255.f blue:60/255.f alpha:1];
        }
        
        self.layer.cornerRadius = 5;
        self.layer.masksToBounds = YES;
        
        [self addSubview:self.titleLabel];
        [self addSubview:self.detailLabel];
        [self addSubview:self.timeLabel];
        [self addSubview:self.imageView];
        [self addSubview:self.button];
    }
    return self;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 100, 16)];
        _titleLabel.textColor = [UIColor whiteColor];
        _titleLabel.font = [UIFont systemFontOfSize:16];
//        _titleLabel.layer.borderColor = [UIColor blackColor].CGColor;
//        _titleLabel.layer.borderWidth = 1;
    }
    return _titleLabel;
}

- (UILabel *)detailLabel {
    if (!_detailLabel) {
        _detailLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, CGRectGetMaxY(_titleLabel.frame) + 6, 200 - 20, 12)];
        _detailLabel.textColor = [UIColor whiteColor];
        _detailLabel.font = [UIFont systemFontOfSize:12];
//        _detailLabel.layer.borderColor = [UIColor blackColor].CGColor;
//        _detailLabel.layer.borderWidth = 1;
    }
    return _detailLabel;
}

- (UILabel *)timeLabel {
    if (!_timeLabel) {
        _timeLabel = [[UILabel alloc] initWithFrame:CGRectMake((200 - 10 - 20 - 22 - 36), 10, 36, 12)];
        _timeLabel.textColor = [UIColor whiteColor];
        _timeLabel.font = [UIFont systemFontOfSize:12];
        _timeLabel.textAlignment = NSTextAlignmentRight;
//        _timeLabel.layer.borderColor = [UIColor blackColor].CGColor;
//        _timeLabel.layer.borderWidth = 1;
    }
    return _timeLabel;
}

- (UIImageView *)imageView {
    if (!_imageView) {
        _imageView = [[UIImageView alloc] initWithFrame:CGRectMake((200 - 20) - 10, (50 - 20) / 2, 20, 20)];
    }
    return _imageView;
}

- (UIButton *)button {
    if (!_button) {
        _button = [[UIButton alloc] initWithFrame:self.bounds];
    }
    
    return _button;
}

@end
