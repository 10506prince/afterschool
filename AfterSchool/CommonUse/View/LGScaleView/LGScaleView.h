//
//  LGScaleView.h
//  AfterSchool
//
//  Created by lg on 15/12/9.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LGScaleView : UIView

@property (nonatomic, assign) IBInspectable float scale;

@end
