//
//  LGTableView.h
//  AfterSchool
//
//  Created by lg on 15/11/10.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITableView+NibCell.h"

typedef NS_ENUM(NSInteger, LGTableViewHeadType) {
    LGTableViewHeadTypeDefault = 0,///<默认为LGScrollViewHeadTypeLight
    LGTableViewHeadTypeDark,
    LGTableViewHeadTypeLight = LGTableViewHeadTypeDefault,
};

typedef NS_ENUM(NSInteger, LGTableViewBackgroundType) {
    LGTableViewBackgroundTypeDefault = 0,///<默认为LGScrollViewBackgroundTypeLight
    LGTableViewBackgroundTypeDark,
    LGTableViewBackgroundTypeLight = LGTableViewHeadTypeDefault,
};

@interface LGTableView : UITableView

- (void)setBackgroundType:(LGTableViewBackgroundType)LGTableViewBackgroundType;
- (void)setHeadType:(LGTableViewHeadType)LGTableViewHeadType;

- (void)removeHeadLayer;

@end
