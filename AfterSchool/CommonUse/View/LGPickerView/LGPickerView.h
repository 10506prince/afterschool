//
//  LGPickerView.h
//  AfterSchool
//
//  Created by lg on 15/10/30.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol LGPickerViewDelegate <NSObject>

@optional
- (void)lgPickerViewCancelButtonClicked;
- (void)lgPickerViewConfirmButtonClicked:(id)data;

@end

@interface LGPickerView : UIView <UIPickerViewDataSource, UIPickerViewDelegate>

@property (nonatomic, strong) UIPickerView *pickerView;
@property (nonatomic, strong) UIButton *cancelButton;
@property (nonatomic, strong) UIButton *confirmButton;

@property (nonatomic, assign) CGPoint homePosition;
@property (nonatomic, assign) CGPoint targetPosition;
@property (nonatomic, strong) UIView *maskView;

@property (nonatomic, strong) NSMutableArray *sexValueArray;
@property (nonatomic, strong) NSMutableArray * sexChineseArray;

@property (nonatomic, weak) id <LGPickerViewDelegate> delegate;

/**
 *  初始化性别选择器（宽度为300，高度为144，其中日期高度位100，“取消”和“确定”按钮的高度是44,宽度为150）
 *
 *  @param frame          在父视图上的位置
 *  @param targetPosition 指定移动到父视图的某个位置
 *
 *  @return 返回UIView的实例
 */
- (instancetype)initWithFrame:(CGRect)frame targetPosition:(CGPoint)targetPosition;

/**
 *  视图移动到指定位置
 */
- (void)moveToTargetPositon;

/**
 *  视图移回原位
 */
- (void)moveToHomePositon;

@end
