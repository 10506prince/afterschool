//
//  LGBasePickerView.h
//  AfterSchool
//
//  Created by lg on 15/11/7.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol LGPickerViewDelegate <NSObject>

@optional
- (void)lgPickerViewCancelButtonClicked;
- (void)lgPickerViewConfirmButtonClicked:(NSDictionary *)data;

@end


@interface LGBasePickerView : UIView <UIPickerViewDataSource,UIPickerViewDelegate>

@property (nonatomic, strong) UIPickerView *pickerView;
@property (nonatomic, strong) UIButton *cancelButton;
@property (nonatomic, strong) UIButton *confirmButton;

@property (nonatomic, assign) CGPoint homePosition;
@property (nonatomic, assign) CGPoint targetPosition;
@property (nonatomic, strong) UIView *maskView;

@property (nonatomic, strong) NSArray <NSDictionary *> *nameAndvalueArray;

@property (nonatomic, weak) id <LGPickerViewDelegate> delegate;

/**
 *  初始化选择器（宽度为300，高度为244，其中日期高度位200，“取消”和“确定”按钮的高度是44,宽度为150）
 *
 *  @param frame          在父视图上的位置
 *  @param targetPosition 指定移动到父视图的某个位置
 *
 *  @return 返回UIView的实例
 */
- (instancetype)initWithFrame:(CGRect)frame targetPosition:(CGPoint)targetPosition;

/**
 *  @brief  初始化选择器（宽度为300，高度为244，其中日期高度位200，“取消”和“确定”按钮的高度是44,宽度为150）
 *
 *  @param homePosition   左上角在父视图上的位置
 *  @param targetPosition 左上角指定移动到父视图上的位置
 *
 *  @return 返回UIView的实例
 */
- (instancetype)initWithHomePositon:(CGPoint)homePosition targetPosition:(CGPoint)targetPosition;

/**
 *  视图移动到指定位置
 */
- (void)moveToTargetPositon;

/**
 *  视图移回原位
 */
- (void)moveToHomePositon;

@end

@interface UIButton (SetBackgroundColor)

- (void)setBackgroundColor:(UIColor *)backgroundColor forState:(UIControlState)state;

@end
