//
//  LGPickerView.m
//  AfterSchool
//
//  Created by lg on 15/10/30.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGPickerView.h"
#import "UIButton+SetBackgroundColor.h"

#define SCREEN_HEIGHT [UIScreen mainScreen].bounds.size.height
#define SCREEN_WIDTH [UIScreen mainScreen].bounds.size.width

@interface LGPickerView ()

@property (nonatomic, strong) UIView * showView;

@end

@implementation LGPickerView

//- (instancetype)init
//{
//    self = [super init];
//    if (self) {
//        [self setUp];
//    }
//    return self;
//}
//- (instancetype)initWithCoder:(NSCoder *)coder
//{
//    self = [super initWithCoder:coder];
//    if (self) {
//        [self setUp];
//    }
//    return self;
//}
//- (instancetype)initWithFrame:(CGRect)frame
//{
//    self = [super initWithFrame:frame];
//    if (self) {
//        [self setUp];
//    }
//    return self;
//}

- (void)setUp {
    [self setFrame:[UIScreen mainScreen].bounds];
    [self setBackgroundColor:[UIColor colorWithWhite:0 alpha:0.3]];
    
    self.showView = [[UIView alloc] init];
    [self.showView setBounds:CGRectMake(0, 0, 300, 144)];
    self.showView.layer.borderColor = [UIColor colorWithRed:196/255.f green:196/255.f blue:196/255.f alpha:1].CGColor;
    self.showView.layer.borderWidth = 1;
    self.showView.layer.cornerRadius = 6;
    self.showView.layer.masksToBounds = YES;
    self.showView.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.showView];
    
    _sexValueArray = [[NSMutableArray alloc] init];
    _sexChineseArray = [[NSMutableArray alloc] init];
    
    for (int i = 1; i <= 10; i++) {
        [_sexValueArray addObject:@(i)];
        [_sexChineseArray addObject:[NSString stringWithFormat:@"%d小时",i]];
    }
    
    [self addSubview:self.pickerView];
    [self addSubview:self.cancelButton];
    [self addSubview:self.confirmButton];
    _pickerView.delegate = self;
    _pickerView.dataSource = self;
    
    [_pickerView selectRow:1 inComponent:0 animated:YES];
    
    
}

//- (void)layoutSubviews {
//    [super layoutSubviews];
//    
//    [self.showView setCenter:self.center];
//    
//    
//}


- (instancetype)initWithFrame:(CGRect)frame
               targetPosition:(CGPoint)targetPosition
{
    self = [super initWithFrame:frame];
    if (self) {
        self.layer.borderColor = [UIColor colorWithRed:196/255.f green:196/255.f blue:196/255.f alpha:1].CGColor;
        self.layer.borderWidth = 1;
        self.layer.cornerRadius = 6;
        self.layer.masksToBounds = YES;
        self.backgroundColor = [UIColor whiteColor];
        
        _homePosition = CGPointMake(frame.origin.x, frame.origin.y);
        _targetPosition = targetPosition;
        [self initData];
        [self initUserInterface];
    }
    
    return self;
}

- (void)initData {
    _sexValueArray = [[NSMutableArray alloc] init];
    _sexChineseArray = [[NSMutableArray alloc] init];
    
    for (int i = 1; i <= 10; i++) {
        [_sexValueArray addObject:@(i)];
        [_sexChineseArray addObject:[NSString stringWithFormat:@"%d小时",i]];
    }
}

- (void)initUserInterface {
    [self addSubview:self.pickerView];
    [self addSubview:self.cancelButton];
    [self addSubview:self.confirmButton];
    _pickerView.delegate = self;
    _pickerView.dataSource = self;
    
    [_pickerView selectRow:1 inComponent:0 animated:YES];
}

- (UIPickerView *)pickerView {
    if (!_pickerView) {
        _pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0, 300, 200)];
    }
    return _pickerView;
}

- (UIButton *)cancelButton {
    if (!_cancelButton) {
        _cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 200, 300 / 2, 44)];
        [_cancelButton setTitle:@"取消" forState:UIControlStateNormal];
        [_cancelButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_cancelButton setBackgroundColor:[UIColor grayColor] forState:UIControlStateHighlighted];
        _cancelButton.titleLabel.font = [UIFont systemFontOfSize:20];
        _cancelButton.layer.borderColor = [UIColor colorWithRed:196/255.f green:196/255.f blue:196/255.f alpha:1].CGColor;
        _cancelButton.layer.borderWidth = 1;
        [_cancelButton addTarget:self action:@selector(cancelButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    }
    return _cancelButton;
}

- (void)cancelButtonClicked {
    [self moveToHomePositon];
    if (_delegate && [_delegate respondsToSelector:@selector(lgPickerViewCancelButtonClicked)]) {
        [_delegate lgPickerViewCancelButtonClicked];
    }
}

- (UIButton *)confirmButton {
    if (!_confirmButton) {
        _confirmButton = [[UIButton alloc] initWithFrame:CGRectMake(150 - 1, 200, 150 + 1, 44)];
        [_confirmButton setTitle:@"确定" forState:UIControlStateNormal];
        [_confirmButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_confirmButton setBackgroundColor:[UIColor grayColor] forState:UIControlStateHighlighted];
        
        _confirmButton.titleLabel.font = [UIFont systemFontOfSize:20];
        _confirmButton.layer.borderColor = [UIColor colorWithRed:196/255.f green:196/255.f blue:196/255.f alpha:1].CGColor;
        _confirmButton.layer.borderWidth = 1;
        [_confirmButton addTarget:self action:@selector(confirmButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    }
    return _confirmButton;
}

- (void)confirmButtonClicked {
    NSString *resultString = _sexChineseArray[[_pickerView selectedRowInComponent:0]];
    if (_delegate && [_delegate respondsToSelector:@selector(lgPickerViewConfirmButtonClicked:)]) {
        [_delegate lgPickerViewConfirmButtonClicked:resultString];
    }
    [self moveToHomePositon];
}

#pragma mark pickerView
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return _sexValueArray.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return _sexChineseArray[row];
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    return 36;
}

- (void)moveToTargetPositon {
    [self.superview addSubview:self.maskView];
    [self.superview addSubview:self];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.2];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    
    self.maskView.alpha = 0.3;
    self.maskView.backgroundColor = [UIColor grayColor];
    
    self.frame = CGRectMake(_targetPosition.x, _targetPosition.y, 300, 244);
    [UIView commitAnimations];
}

- (void)moveToHomePositon {
    
    [UIView beginAnimations:@"RestoreDatePickerViewPosition" context:nil];
    [UIView setAnimationDuration:0.2];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDidStopSelector:@selector(didEndMoveToHomePosition)];
    [UIView setAnimationDelegate:self];
    
    _maskView.alpha = 0;
    
    self.frame = CGRectMake(_homePosition.x, _homePosition.y, 300, 244);
    [UIView commitAnimations];
}

- (void)didEndMoveToHomePosition {
    [_maskView removeFromSuperview];
    [self removeFromSuperview];
}

- (UIView *)maskView {
    if (!_maskView) {
        _maskView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
        _maskView.backgroundColor = [UIColor clearColor];
        _maskView.userInteractionEnabled = YES;
    }
    return _maskView;
}
@end
