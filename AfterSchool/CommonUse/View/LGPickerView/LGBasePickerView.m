//
//  LGBasePickerView.m
//  AfterSchool
//
//  Created by lg on 15/11/7.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGBasePickerView.h"
#define SCREEN_HEIGHT [UIScreen mainScreen].bounds.size.height
#define SCREEN_WIDTH [UIScreen mainScreen].bounds.size.width

@implementation LGBasePickerView

- (instancetype)initWithFrame:(CGRect)frame
               targetPosition:(CGPoint)targetPosition
{
    self = [super initWithFrame:frame];
    if (self) {
        self.layer.borderColor = [UIColor colorWithRed:196/255.f green:196/255.f blue:196/255.f alpha:1].CGColor;
        self.layer.borderWidth = 1;
        self.layer.cornerRadius = 6;
        self.layer.masksToBounds = YES;
        self.backgroundColor = [UIColor whiteColor];
        
        _homePosition = CGPointMake(frame.origin.x, frame.origin.y);
        _targetPosition = targetPosition;
        [self initData];
        [self initUserInterface];
    }
    
    return self;
}

- (instancetype)initWithHomePositon:(CGPoint)homePosition targetPosition:(CGPoint)targetPosition
{
    self = [super initWithFrame:CGRectMake(homePosition.x, homePosition.y, 300, 144)];
    if (self) {
        self.layer.borderColor = [UIColor colorWithRed:196/255.f green:196/255.f blue:196/255.f alpha:1].CGColor;
        self.layer.borderWidth = 1;
        self.layer.cornerRadius = 6;
        self.layer.masksToBounds = YES;
        self.backgroundColor = [UIColor whiteColor];
        
        _homePosition = homePosition;
        _targetPosition = targetPosition;
        [self initData];
        [self initUserInterface];
    }
    return self;
}

- (void)initData {
}

- (void)initUserInterface {
    [self addSubview:self.pickerView];
    [self addSubview:self.cancelButton];
    [self addSubview:self.confirmButton];
    _pickerView.delegate = self;
    _pickerView.dataSource = self;
    
    [_pickerView selectRow:0 inComponent:0 animated:YES];
}

- (UIPickerView *)pickerView {
    if (!_pickerView) {
        _pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0, 300, 200)];
    }
    return _pickerView;
}

- (void)setNameAndvalueArray:(NSArray<NSDictionary *> *)nameAndvalueArray {
    _nameAndvalueArray = nameAndvalueArray;
    [self.pickerView reloadAllComponents];
}
- (UIButton *)cancelButton {
    if (!_cancelButton) {
        _cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 200, 300 / 2, 44)];
        [_cancelButton setTitle:@"取消" forState:UIControlStateNormal];
        [_cancelButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_cancelButton setBackgroundColor:[UIColor grayColor] forState:UIControlStateHighlighted];
        _cancelButton.titleLabel.font = [UIFont systemFontOfSize:20];
        _cancelButton.layer.borderColor = [UIColor colorWithRed:196/255.f green:196/255.f blue:196/255.f alpha:1].CGColor;
        _cancelButton.layer.borderWidth = 1;
        [_cancelButton addTarget:self action:@selector(cancelButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    }
    return _cancelButton;
}

- (void)cancelButtonClicked {
    [self moveToHomePositon];
    if (_delegate && [_delegate respondsToSelector:@selector(lgPickerViewCancelButtonClicked)]) {
        [_delegate lgPickerViewCancelButtonClicked];
    }
}

- (UIButton *)confirmButton {
    if (!_confirmButton) {
        _confirmButton = [[UIButton alloc] initWithFrame:CGRectMake(150 - 1, 200, 150 + 1, 44)];
        [_confirmButton setTitle:@"确定" forState:UIControlStateNormal];
        [_confirmButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_confirmButton setBackgroundColor:[UIColor grayColor] forState:UIControlStateHighlighted];
        
        _confirmButton.titleLabel.font = [UIFont systemFontOfSize:20];
        _confirmButton.layer.borderColor = [UIColor colorWithRed:196/255.f green:196/255.f blue:196/255.f alpha:1].CGColor;
        _confirmButton.layer.borderWidth = 1;
        [_confirmButton addTarget:self action:@selector(confirmButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    }
    return _confirmButton;
}

- (void)confirmButtonClicked {
    NSDictionary * result = _nameAndvalueArray[[_pickerView selectedRowInComponent:0]];
    if (_delegate && [_delegate respondsToSelector:@selector(lgPickerViewConfirmButtonClicked:)]) {
        [_delegate lgPickerViewConfirmButtonClicked:result];
    }
    [self moveToHomePositon];
}

#pragma mark pickerView
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return _nameAndvalueArray.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    NSDictionary * dic = [_nameAndvalueArray objectAtIndex:row];
    NSString * name = [NSString stringWithFormat:@"%@", [dic objectForKey:@"name"]];
    return name;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    return 36;
}

- (void)moveToTargetPositon {

    [self.superview insertSubview:self.maskView belowSubview:self];
    [self setHidden:NO];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.2];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    
    self.maskView.alpha = 0.3;
    self.maskView.backgroundColor = [UIColor grayColor];
    
    self.frame = CGRectMake(_targetPosition.x, _targetPosition.y, 300, 244);
    [UIView commitAnimations];
}

- (void)moveToHomePositon {
    
    [UIView beginAnimations:@"RestoreDatePickerViewPosition" context:nil];
    [UIView setAnimationDuration:0.2];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDidStopSelector:@selector(didEndMoveToHomePosition)];
    [UIView setAnimationDelegate:self];
    
    _maskView.alpha = 0;
    
    self.frame = CGRectMake(_homePosition.x, _homePosition.y, 300, 244);
    [UIView commitAnimations];
}

- (void)didEndMoveToHomePosition {
    [self.maskView removeFromSuperview];
    [self setHidden:YES];
}

- (UIView *)maskView {
    if (!_maskView) {
        _maskView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
        _maskView.backgroundColor = [UIColor clearColor];
        _maskView.userInteractionEnabled = YES;
    }
    return _maskView;
}

@end

@implementation UIButton (SetBackgroundColor)

- (void)setBackgroundColor:(UIColor *)backgroundColor forState:(UIControlState)state {
    [self setBackgroundImage:[UIButton imageWithColor:backgroundColor] forState:state];
}

+ (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

@end
