//
//  CustomTextField.m
//  MWOA
//
//  Created by Teson Draw on 2/27/15.
//  Copyright (c) 2015 Teson Draw. All rights reserved.
//

#import "CustomTextField.h"

@implementation CustomTextField

- (CGRect)editingRectForBounds:(CGRect)bounds {
    CGRect insetRect = CGRectMake(bounds.origin.x + 38, bounds.origin.y, bounds.size.width - 5, bounds.size.height);
    return insetRect;
}

- (CGRect)placeholderRectForBounds:(CGRect)bounds {
    CGRect insetRect = CGRectMake(bounds.origin.x + 38, bounds.origin.y, bounds.size.width - 5, bounds.size.height);
    return insetRect;
}

- (CGRect)textRectForBounds:(CGRect)bounds {
    CGRect insetRect = CGRectMake(bounds.origin.x + 38, bounds.origin.y, bounds.size.width - 5, bounds.size.height);
    return insetRect;
}

- (CGRect)leftViewRectForBounds:(CGRect)bounds {
    CGRect imageRect = [super leftViewRectForBounds:bounds];
    imageRect.origin.x += 6;
    return imageRect;
}

- (CGRect)clearButtonRectForBounds:(CGRect)bounds {
    CGRect rect = [super clearButtonRectForBounds:bounds];
    rect.origin.x -= 8;
    
    return rect;
}

@end
