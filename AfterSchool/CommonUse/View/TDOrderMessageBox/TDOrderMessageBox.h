//
//  TDMessageBox.h
//  AfterSchool
//
//  Created by Teson Draw on 12/1/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TodayOrderModel.h"

@protocol TDOrderMessageBoxDelegate <NSObject>

- (void)TDOrderMessageBoxCloseButtonClickedWithModel:(TodayOrderModel *)model;
- (void)TDOrderMessageBoxGrabOrderButtonClickedWithModel:(TodayOrderModel *)model;

@end

@interface TDOrderMessageBox : UIView

@property (nonatomic, strong) UIView *boardView;
@property (nonatomic, strong) UIView *portraitBackgroundView;
@property (nonatomic, strong) UIView *separatorLine;
@property (nonatomic, strong) UIView *buttonBackgroundView;
@property (nonatomic, strong) UIView *addressBackgroundView;

@property (nonatomic, strong) UIImageView *portraitImageView;
@property (nonatomic, strong) UIImageView *sexImageView;
@property (nonatomic, strong) UIImageView *credibilityImageView;
@property (nonatomic, strong) UIImageView *addressMarkImageView;
@property (nonatomic, strong) UIImageView *closeButtonImageView;

@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *ageLabel;
@property (nonatomic, strong) UILabel *typeLabel;
@property (nonatomic, strong) UILabel *rewardLabel;
@property (nonatomic, strong) UILabel *starTimeLabel;
@property (nonatomic, strong) UILabel *durationLabel;
@property (nonatomic, strong) UILabel *amountLabel;
@property (nonatomic, strong) UILabel *addressLabel;
@property (nonatomic, strong) UILabel *buttonNameLabel;
@property (nonatomic, strong) UILabel *countdownLabel;
@property (nonatomic, strong) UILabel *unitLabel;

@property (nonatomic, strong) UILabel *playTypeLabel;
@property (nonatomic, strong) UILabel *gameServerLabel;
@property (nonatomic, strong) UILabel *guaranteeDepositLabel;

@property (nonatomic, strong) UIButton *grabOrderButton;
@property (nonatomic, strong) UIButton *closeButton;

@property (nonatomic, assign) NSInteger countdown;

@property (nonatomic, strong) TodayOrderModel *model;

@property (nonatomic, strong) NSTimer *timer;

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) CAShapeLayer *downTriangleLayer;

@property (nonatomic, strong) id <TDOrderMessageBoxDelegate> delegate;


@property (nonatomic, strong) NSMutableArray *dataSource;

+ (void)show;

//+ (void)showWithModel:(TodayOrderModel *)model;
+ (void)showWithModel:(TodayOrderModel *)model delegate:(id <TDOrderMessageBoxDelegate>)delegate;

@end
