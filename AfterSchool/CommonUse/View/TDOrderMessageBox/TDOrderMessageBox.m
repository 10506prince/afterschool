//
//  TDMessageBox.m
//  AfterSchool
//
//  Created by Teson Draw on 12/1/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "TDOrderMessageBox.h"
#import "UIButton+SetBackgroundColor.h"
#import <CoreText/CoreText.h>
#import "MacrosDefinition.h"
#import "UIImageView+WebCache.h"
#import "TDDateConversion.h"

#import "TDPathRef.h"
#import "TDAttributedString.h"

@interface TDOrderMessageBox () <UITableViewDataSource, UITableViewDelegate>

@end

@implementation TDOrderMessageBox

- (void)initData {
    _dataSource = [[NSMutableArray alloc] init];
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self initData];
        [self setBackgroundColor:[UIColor colorWithWhite:0.3 alpha:0.4]];
        
        [self addSubview:self.boardView];
        [_boardView addSubview:self.portraitBackgroundView];
        [_portraitBackgroundView addSubview:self.portraitImageView];
        [_portraitBackgroundView addSubview:self.nameLabel];
        [_portraitBackgroundView addSubview:self.gameServerLabel];
        [_portraitBackgroundView addSubview:self.playTypeLabel];
        
        [_boardView addSubview:self.tableView];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        
        [self.layer addSublayer:self.downTriangleLayer];
        
        [self addSubview:self.buttonBackgroundView];
        [_buttonBackgroundView addSubview:self.buttonNameLabel];
        [_buttonBackgroundView addSubview:self.countdownLabel];
        [_buttonBackgroundView addSubview:self.grabOrderButton];
        
        [self addSubview:self.closeButtonImageView];
        [self addSubview:self.closeButton];

    }
    return self;
}

- (UIView *)boardView {
    if (!_boardView) {
        
        if (iPhone4s) {
            _boardView = [[UIView alloc] initWithFrame:CGRectMake((self.bounds.size.width - 288) / 2, 34, 288, 300)];
        } else {
           _boardView = [[UIView alloc] initWithFrame:CGRectMake((self.bounds.size.width - 288) / 2, 84, 288, 324)];
        }
        
        _boardView.backgroundColor = [UIColor whiteColor];
        _boardView.layer.cornerRadius = 5;
        _boardView.layer.masksToBounds = YES;
    }
    return _boardView;
}

- (UIView *)portraitBackgroundView {
    if (!_portraitBackgroundView) {
        _portraitBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 288, 80)];
        _portraitBackgroundView.backgroundColor = [UIColor colorWithRed:38/255.f green:166/255.f blue:154/255.f alpha:1];
    }
    return _portraitBackgroundView;
}

- (UIImageView *)portraitImageView {
    if (!_portraitImageView) {
        _portraitImageView = [[UIImageView alloc] initWithFrame:CGRectMake(15, 10, 60, 60)];
        _portraitImageView.layer.cornerRadius = 30;
        _portraitImageView.layer.masksToBounds = YES;
        _portraitImageView.image = [UIImage imageNamed:@"about_me_money_logo"];
    }
    return _portraitImageView;
}

- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_portraitImageView.frame) + 8, 20, 160, 23)];
        _nameLabel.textColor = [UIColor whiteColor];
        _nameLabel.font = [UIFont systemFontOfSize:21];
        _nameLabel.text = @"Angelababy~~";
    }
    return _nameLabel;
}

- (UILabel *)gameServerLabel {
    if (!_gameServerLabel) {
        _gameServerLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_portraitImageView.frame) + 8, CGRectGetMaxY(_nameLabel.frame) + 15, 140, 16)];
        _gameServerLabel.textColor = [UIColor whiteColor];
        _gameServerLabel.font = [UIFont systemFontOfSize:16];
        _gameServerLabel.text = @"暗影岛";
    }
    return _gameServerLabel;
}

- (UILabel *)playTypeLabel {
    if (!_playTypeLabel) {
        _playTypeLabel = [[UILabel alloc] initWithFrame:CGRectMake(_portraitBackgroundView.bounds.size.width - 80, 35, 100, 16)];
        _playTypeLabel.textColor = [UIColor whiteColor];
        _playTypeLabel.font = [UIFont systemFontOfSize:16];
        _playTypeLabel.text = @"约妹子";
        
//        _playTypeLabel.layer.borderColor = [UIColor blackColor].CGColor;
//        _playTypeLabel.layer.borderWidth = 1;
    }
    return _playTypeLabel;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(14, CGRectGetMaxY(_portraitBackgroundView.frame) + 20, _boardView.bounds.size.width - 24, _boardView.bounds.size.height - CGRectGetMaxY(_portraitBackgroundView.frame) - 38)];
        _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = [UIColor whiteColor];
        
        _tableView.scrollEnabled = NO;
//        UIView *tableHeadView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, 15)];
//        _orderInfoTableView.tableHeaderView = tableHeadView;
//        _tableView.layer.borderColor = [UIColor redColor].CGColor;
//        _tableView.layer.borderWidth = 1;
    }
    return _tableView;
}

- (UILabel *)guaranteeDepositLabel {
    if (!_guaranteeDepositLabel) {
        _guaranteeDepositLabel = [[UILabel alloc] initWithFrame:CGRectMake(28, _boardView.bounds.size.height - 18 - 30, _boardView.bounds.size.width - 56, 30)];
        _guaranteeDepositLabel.textColor = [UIColor colorWithRed:1.f green:145/255.f blue:0 alpha:1];
        _guaranteeDepositLabel.font = [UIFont systemFontOfSize:12];
        _guaranteeDepositLabel.numberOfLines  = 0;
        _guaranteeDepositLabel.text = @"为保证服务质量，系统将按订单总金额收取30%的保证金。若订单完成即可返还！";
    }
    return _guaranteeDepositLabel;
}

- (CAShapeLayer *)downTriangleLayer {
    
    if (!_downTriangleLayer) {
        _downTriangleLayer = [CAShapeLayer layer];
        _downTriangleLayer.lineWidth = 1;
        _downTriangleLayer.fillColor = [UIColor whiteColor].CGColor;
        _downTriangleLayer.strokeColor = [UIColor whiteColor].CGColor;
        
        _downTriangleLayer.path = [TDPathRef downTriangleWithStart:CGPointMake(self.bounds.size.width / 2 - 15, CGRectGetMaxY(_boardView.frame)) width:30 height:15];
    }
    
    return _downTriangleLayer;
}

//- (UIImageView *)sexImageView {
//    if (!_sexImageView) {
//        _sexImageView = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_portraitImageView.frame) + 14, CGRectGetMaxY(_nameLabel.frame) + 10, 30, 12)];
//        _sexImageView.image = [UIImage imageNamed:@"sex0"];
//    }
//    return _sexImageView;
//}
//
//- (UILabel *)ageLabel {
//    if (!_ageLabel) {
//        _ageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, _sexImageView.bounds.size.width, _sexImageView.bounds.size.height)];
//        _ageLabel.textColor = [UIColor whiteColor];
//        _ageLabel.font = [UIFont systemFontOfSize:12];
//        _ageLabel.text = @"27";
//        _ageLabel.textAlignment = NSTextAlignmentRight;
//    }
//    return _ageLabel;
//}
//
//- (UIImageView *)credibilityImageView {
//    if (!_credibilityImageView) {
//        _credibilityImageView = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_sexImageView.frame) + 10, CGRectGetMaxY(_nameLabel.frame) + 10, 15, 15)];
//        _credibilityImageView.image = [UIImage imageNamed:@"creditRating9"];
//    }
//    return _credibilityImageView;
//}
//
//- (UILabel *)typeLabel {
//    if (!_typeLabel) {
////        _typeLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_portraitImageView.frame) + 14, CGRectGetMaxY(_sexImageView.frame) + 6, 80, 16)];
//        _typeLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_rewardLabel.frame), CGRectGetMaxY(_sexImageView.frame) + 6, 90, 16)];
//        _typeLabel.textColor = [UIColor colorWithRed:202/255.f green:202/255.f blue:202/255.f alpha:1];
//        _typeLabel.font = [UIFont systemFontOfSize:14];
//        _typeLabel.text = @"普通型";
////        _typeLabel.layer.borderColor = [UIColor blackColor].CGColor;
////        _typeLabel.layer.borderWidth = 1;
//    }
//    return _typeLabel;
//}
//
//- (UILabel *)rewardLabel {
//    if (!_rewardLabel) {
//        _rewardLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_portraitImageView.frame) + 14, CGRectGetMaxY(_sexImageView.frame) + 6, 100, 16)];
//        
////        _rewardLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_typeLabel.frame), CGRectGetMaxY(_sexImageView.frame) + 6, 110, 16)];
//        
//        _rewardLabel.textColor = [UIColor colorWithRed:202/255.f green:202/255.f blue:202/255.f alpha:1];
//        _rewardLabel.font = [UIFont systemFontOfSize:14];
//        _rewardLabel.text = @"打赏：200元";
////        _rewardLabel.layer.borderColor = [UIColor whiteColor].CGColor;
////        _rewardLabel.layer.borderWidth = 1;
//    }
//    return _rewardLabel;
//}
//
//- (UILabel *)starTimeLabel {
//    if (!_starTimeLabel) {
//        _starTimeLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_portraitBackgroundView.frame) + 20, _boardView.bounds.size.width, 18)];
//        _starTimeLabel.textColor = [UIColor colorWithRed:120/255.f green:120/255.f blue:120/255.f alpha:1];
//        _starTimeLabel.font = [UIFont systemFontOfSize:18];
//        _starTimeLabel.textAlignment = NSTextAlignmentCenter;
//        _starTimeLabel.text = @"开始时间：12月16日12:00";
//    }
//    return _starTimeLabel;
//}
//
//- (UIView *)separatorLine {
//    if (!_separatorLine) {
//        _separatorLine = [[UIView alloc] initWithFrame:CGRectMake(28, CGRectGetMaxY(_starTimeLabel.frame) + 16, _boardView.bounds.size.width - 56, 1)];
//        _separatorLine.backgroundColor = [UIColor colorWithRed:238/255.f green:240/255.f blue:240/255.f alpha:1];
//    }
//    return _separatorLine;
//}
//
//- (UILabel *)durationLabel {
//    if (!_durationLabel) {
//        _durationLabel = [[UILabel alloc] initWithFrame:CGRectMake(_boardView.bounds.size.width - 100 - 26, CGRectGetMaxY(_separatorLine.frame) + 6, 100, 16)];
//        _durationLabel.textColor = [UIColor colorWithRed:120/255.f green:120/255.f blue:120/255.f alpha:1];
//        _durationLabel.textColor = [UIColor blackColor];
//        _durationLabel.font = [UIFont systemFontOfSize:16];
//        _durationLabel.text = @"时长：2小时";
//        _durationLabel.textAlignment = NSTextAlignmentRight;
//    }
//    return _durationLabel;
//}
//
//- (UILabel *)amountLabel {
//    if (!_amountLabel) {
//        _amountLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_separatorLine.frame) + 63, self.bounds.size.width, 40)];
//        _amountLabel.textColor = [UIColor colorWithRed:68/255.f green:68/255.f blue:68/255.f alpha:1];
//        _amountLabel.font = [UIFont systemFontOfSize:40];
//        _amountLabel.textAlignment = NSTextAlignmentCenter;
//        _amountLabel.text = @"300元";
//    }
//    return _amountLabel;
//}
//
//- (UIView *)addressBackgroundView {
//    if (!_addressBackgroundView) {
//        _addressBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, _boardView.bounds.size.height - 56, _boardView.bounds.size.width, 56)];
//        _addressBackgroundView.backgroundColor = [UIColor colorWithRed:241/255.f green:241/255.f blue:241/255.f alpha:1];
//    }
//    return _addressBackgroundView;
//}
//
//- (UIImageView *)addressMarkImageView {
//    if (!_addressMarkImageView) {
//        _addressMarkImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 6, 44, 44)];
//        _addressMarkImageView.image = [UIImage imageNamed:@"grab_order_address_mark"];
//    }
//    return _addressMarkImageView;
//}
//
//- (UILabel *)addressLabel {
//    if (!_addressLabel) {
//        _addressLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_addressMarkImageView.frame) + 8, 14, _boardView.bounds.size.width - CGRectGetMaxX(_addressMarkImageView.frame) - 8 - 12, 56 - 12)];
//        _addressLabel.textColor = [UIColor blackColor];
//        _addressLabel.font = [UIFont systemFontOfSize:12];
//        _addressLabel.numberOfLines = 0;
//        _addressLabel.text = @"四川省成都市天府五街软件园E6-1";
//    }
//    return _addressLabel;
//}

- (UIView *)buttonBackgroundView {
    if (!_buttonBackgroundView) {
        _buttonBackgroundView = [[UIView alloc] initWithFrame:CGRectMake((self.bounds.size.width - 120) / 2, CGRectGetMaxY(_boardView.frame) + 20, 120, 120)];
        _buttonBackgroundView.backgroundColor = [UIColor colorWithRed:250/255.f green:145/255.f blue:0/255.f alpha:1];
        _buttonBackgroundView.layer.cornerRadius = 60;
    }
    return _buttonBackgroundView;
}

- (UILabel *)buttonNameLabel {
    if (!_buttonNameLabel) {
        _buttonNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 33, _buttonBackgroundView.bounds.size.width, 29)];
        _buttonNameLabel.textColor = [UIColor whiteColor];
        _buttonNameLabel.font = [UIFont systemFontOfSize:29];
        _buttonNameLabel.text = @"抢单";
        _buttonNameLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _buttonNameLabel;
}

- (UILabel *)countdownLabel {
    if (!_countdownLabel) {
        _countdownLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_buttonNameLabel.frame) + 17, _buttonBackgroundView.bounds.size.width, 29)];
        _countdownLabel.textColor = [UIColor whiteColor];
        _countdownLabel.font = [UIFont systemFontOfSize:29];
        _countdownLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _countdownLabel;
}

- (UIButton *)grabOrderButton {
    if (!_grabOrderButton) {
        _grabOrderButton = [[UIButton alloc] initWithFrame:_buttonBackgroundView.bounds];
        [_grabOrderButton addTarget:self action:@selector(grabOrderButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        
//        _grabOrderButton.layer.borderColor = [UIColor blackColor].CGColor;
//        _grabOrderButton.layer.borderWidth = 1;
    }
    
    return _grabOrderButton;
}

- (void)grabOrderButtonClicked {
    if (_delegate && [_delegate respondsToSelector:@selector(TDOrderMessageBoxGrabOrderButtonClickedWithModel:)]) {
        [_delegate TDOrderMessageBoxGrabOrderButtonClickedWithModel:self.model];
    }
    
    [self removeFromSuperview];
}

- (UIImageView *)closeButtonImageView {
    if (!_closeButtonImageView) {
        _closeButtonImageView = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_boardView.frame) - 19 - 6, _boardView.frame.origin.y - 19 + 6, 38, 38)];
        _closeButtonImageView.image = [UIImage imageNamed:@"order_close_message"];
//        _closeButtonImageView.layer.borderColor = [UIColor whiteColor].CGColor;
//        _closeButtonImageView.layer.borderWidth = 1;
    }
    return _closeButtonImageView;
}

- (UIButton *)closeButton {
    if (!_closeButton) {
        _closeButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 60, 60)];
        _closeButton.center = _closeButtonImageView.center;
        [_closeButton addTarget:self action:@selector(closeButtonClicked) forControlEvents:UIControlEventTouchUpInside];
//        _closeButton.layer.borderColor = [UIColor redColor].CGColor;
//        _closeButton.layer.borderWidth = 1;
    }
    
    return _closeButton;
}

- (void)closeButtonClicked {
    
    if (_delegate && [_delegate respondsToSelector:@selector(TDOrderMessageBoxCloseButtonClickedWithModel:)]) {
        [_delegate TDOrderMessageBoxCloseButtonClickedWithModel:self.model];
    }
    
    [self removeFromSuperview];
}

+ (void)show {
    UIWindow * keyWindow = [[[UIApplication sharedApplication] delegate] window];

    TDOrderMessageBox *messageBox = [[TDOrderMessageBox alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    [keyWindow addSubview:messageBox];
}

+ (void)showWithModel:(TodayOrderModel *)model delegate:(id <TDOrderMessageBoxDelegate>)delegate {
    
    UIWindow * keyWindow = [[[UIApplication sharedApplication] delegate] window];
    
    TDOrderMessageBox *messageBox = [[TDOrderMessageBox alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    messageBox.model = model;
    messageBox.delegate = delegate;
    
    [messageBox refreshUI];
    [keyWindow addSubview:messageBox];
}

- (void)refreshUI {
    self.nameLabel.text = self.model.senderName;
    self.gameServerLabel.text = self.model.gameServer;
    self.playTypeLabel.text = self.model.playTypeChinese;
    if (self.model.playType == 5) {
        [self.boardView addSubview:self.guaranteeDepositLabel];
    }

    if (self.model.portraitURL.length > 0) {
        [self.portraitImageView sd_setImageWithURL:[NSURL URLWithString:self.model.portraitURL] placeholderImage:[UIImage imageNamed:@"user_default_head"]];
    } else {
        self.portraitImageView.image = [UIImage imageNamed:@"user_default_head"];
    }
    
    if (self.model.playType == 5) {
        NSString *currentDan = [NSString stringWithFormat:@"当前段位：%@", self.model.currentDan];
        [_dataSource addObject:[TDAttributedString fromString:currentDan indexString:@"："]];
        
        NSString *targetDan = [NSString stringWithFormat:@"目标段位：%@", self.model.targetDan];
        [_dataSource addObject:[TDAttributedString fromString:targetDan indexString:@"："]];
        
        NSString *orderStartTime = [NSString stringWithFormat:@"开始时间：%@", self.model.orderStartTime];
        [_dataSource addObject:[TDAttributedString fromString:orderStartTime indexString:@"："]];
        
        NSString *orderEndTime = [NSString stringWithFormat:@"完成时间：%@", self.model.orderEndTime];
        [_dataSource addObject:[TDAttributedString fromString:orderEndTime indexString:@"："]];
        
        NSString *amount = [NSString stringWithFormat:@"赏金：%ld元", (long)self.model.amount];
        [_dataSource addObject:[TDAttributedString fromString:amount indexString:@"："]];
        
    } else {
        NSString *orderStartTime = [NSString stringWithFormat:@"时间：%@", self.model.orderStartTime];
        [_dataSource addObject:[TDAttributedString fromString:orderStartTime indexString:@"："]];
        
        NSString *duration = [NSString stringWithFormat:@"时长：%@", self.model.duration];
        [_dataSource addObject:[TDAttributedString fromString:duration indexString:@"："]];
        
        NSString *amount = [NSString stringWithFormat:@"赏金：%ld元", (long)self.model.amount];
        [_dataSource addObject:[TDAttributedString fromString:amount indexString:@"："]];
        
        NSString *online = [NSString stringWithFormat:@"方式：%@", self.model.online];
        [_dataSource addObject:[TDAttributedString fromString:online indexString:@"："]];
        
        NSString *address = [NSString stringWithFormat:@"地点：%@", self.model.address];
        [_dataSource addObject:[TDAttributedString fromString:address indexString:@"："]];
    }
    
    [self.tableView reloadData];
    
//    if (self.model.sex == 0) {
//        self.sexImageView.image = [UIImage imageNamed:@"sex0"];
//    } else {
//        self.sexImageView.image = [UIImage imageNamed:@"sex1"];
//    }
//    
//    self.ageLabel.text = [NSString stringWithFormat:@"%lu", (unsigned long)self.model.age];
//    
//    self.typeLabel.text = self.model.condition;
//    self.rewardLabel.text = [NSString stringWithFormat:@"打赏：%.0f元", self.model.reward];
//    
//    self.starTimeLabel.text = [TDDateConversion dateFromNumber:(long)(self.model.planStartTime/1000) dateFormat:@"开始时间：MM月dd日 HH:mm"];
//    
//    self.durationLabel.text = [NSString stringWithFormat:@"时长：%.0f小时", self.model.planKeepTime];
//    
//    self.amountLabel.text = [NSString stringWithFormat:@"%.0f元", (self.model.money + self.model.tokenMoney)];
//    
//    self.addressLabel.text = self.model.address;
//
//    self.credibilityImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"creditRating%lu", (unsigned long)self.model.creditLevel]];
    
    if (self.model.timeOut == 0) {
        self.countdown = 30;
    } else {
        self.countdown = self.model.timeOut;
    }
    
    self.countdownLabel.text = [NSString stringWithFormat:@"%ld", (long)self.countdown];
    
    _timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerAction) userInfo:nil repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
}

- (void)timerAction {
    if (_countdown >= 0) {
        self.countdownLabel.text = [NSString stringWithFormat:@"%ld", (long)self.countdown];
        _countdown--;
        
    } else {
        [self deallocTimer];
        [self removeFromSuperview];
        
//        if (_orderModeType == OrderModelTypeMaster) {
//            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:@"对方没有应答，请选择其他大咖或更换约玩条件" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
//            [alertView show];
//            
//            [self.navigationController popToRootViewControllerAnimated:YES];
//        } else if (_orderModeType == OrderModelTypeScene) {
//            OrderTimeoutViewController *vc = [[OrderTimeoutViewController alloc] init];
//            [self.navigationController pushViewController:vc animated:YES];
//        }
    }
}

//- (void)calculateLeftTime {
//    _model.secondsCountDown -= 1;
//    NSString *string = [NSString stringWithFormat:@"(%ld秒后)可重发", (long)_model.secondsCountDown];
//    [_rootView.getVerificationCodeButton setTitle:string forState:UIControlStateNormal];
//    
//    if (_model.secondsCountDown == 0) {
//        [self stopTimer];
//    }
//}

- (void)deallocTimer {
    if (_timer) {
        [_timer invalidate];
        _timer = nil;
    }
}

#pragma mark TableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return _dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *reuseIdentifier = @"reuseIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    }
    
    [cell.textLabel setAttributedText:_dataSource[indexPath.row]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textLabel.numberOfLines = 0;
    
//    cell.layer.borderColor = [UIColor blackColor].CGColor;
//    cell.layer.borderWidth = 1;

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
//    return 36;
    // 列寬
    CGFloat contentWidth = tableView.frame.size.width;
    // 用何種字體進行顯示
    UIFont *font = [UIFont systemFontOfSize:14];
    // 該行要顯示的內容
    NSString *content = [_dataSource[indexPath.row] string];
    // 計算出顯示完內容需要的最小尺寸
    CGSize size = [content sizeWithFont:font constrainedToSize:CGSizeMake(contentWidth, 1000.0f) lineBreakMode:UILineBreakModeWordWrap];
    // 這裏返回需要的高度
    return size.height + 20;
}

@end
