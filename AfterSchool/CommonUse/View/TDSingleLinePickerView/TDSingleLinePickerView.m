//
//  TDSexPickerView.m
//  AfterSchool
//
//  Created by Teson Draw on 10/8/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "TDSingleLinePickerView.h"
#import "UIButton+SetBackgroundColor.h"
#import "MacrosDefinition.h"

@implementation TDSingleLinePickerView

- (instancetype)initWithFrame:(CGRect)frame
               targetPosition:(CGPoint)targetPosition
{
    self = [super initWithFrame:frame];
    if (self) {
        self.layer.borderColor = [UIColor colorWithRed:196/255.f green:196/255.f blue:196/255.f alpha:1].CGColor;
        self.layer.borderWidth = 1;
        self.layer.cornerRadius = 6;
        self.layer.masksToBounds = YES;
        self.backgroundColor = [UIColor whiteColor];
        
        _homePosition = CGPointMake(frame.origin.x, frame.origin.y);
        _targetPosition = targetPosition;
        [self initData];
        [self initUserInterface];
    }
    
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
                   dataSource:(NSArray *)dataSource
                   defaultRow:(NSInteger)defaultRow
               targetPosition:(CGPoint)targetPosition {
    self = [super initWithFrame:frame];
    if (self) {
        self.layer.borderColor = [UIColor colorWithRed:196/255.f green:196/255.f blue:196/255.f alpha:1].CGColor;
        self.layer.borderWidth = 1;
        self.layer.cornerRadius = 6;
        self.layer.masksToBounds = YES;
        self.backgroundColor = [UIColor whiteColor];
        
        _homePosition = CGPointMake(frame.origin.x, frame.origin.y);
        _targetPosition = targetPosition;
        _defaultRow = defaultRow;
        
        [self initDataWithDataSource:(NSArray *)dataSource];
        [self initUserInterface];
    }
    
    return self;
}

- (void)initDataWithDataSource:(NSArray *)dataSource {
    _valueArray = [[NSMutableArray alloc] init];
    _chineseArray = [[NSMutableArray alloc] initWithArray:dataSource];
    
    for (int i = 0; i < dataSource.count; i++) {
        [_valueArray addObject:[NSNumber numberWithInt:i]];
    }
}

- (void)initData {
    _valueArray = [[NSMutableArray alloc] init];
    _chineseArray = [[NSMutableArray alloc] init];
    
    [_valueArray addObject:@"0"];
    [_valueArray addObject:@"1"];
    
    [_chineseArray addObject:@"女"];
    [_chineseArray addObject:@"男"];
}

- (void)initUserInterface {
    [self addSubview:self.pickerView];
    [self addSubview:self.cancelButton];
    [self addSubview:self.confirmButton];
    _pickerView.delegate = self;
    _pickerView.dataSource = self;
    
    [_pickerView selectRow:_defaultRow inComponent:0 animated:YES];
}

- (UIPickerView *)pickerView {
    if (!_pickerView) {
        _pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0, 300, 200)];
    }
    return _pickerView;
}

- (UIButton *)cancelButton {
    if (!_cancelButton) {
        _cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 200, 300 / 2, 44)];
        [_cancelButton setTitle:@"取消" forState:UIControlStateNormal];
        [_cancelButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_cancelButton setBackgroundColor:[UIColor grayColor] forState:UIControlStateHighlighted];
        _cancelButton.titleLabel.font = [UIFont systemFontOfSize:20];
        _cancelButton.layer.borderColor = [UIColor colorWithRed:196/255.f green:196/255.f blue:196/255.f alpha:1].CGColor;
        _cancelButton.layer.borderWidth = 1;
        [_cancelButton addTarget:self action:@selector(cancelButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    }
    return _cancelButton;
}

- (void)cancelButtonClicked {
    [self moveToHomePositon];
    if (_delegate && [_delegate respondsToSelector:@selector(singleLinePickerViewCancelButtonClicked)]) {
        [_delegate singleLinePickerViewCancelButtonClicked];
    }
}

- (UIButton *)confirmButton {
    if (!_confirmButton) {
        _confirmButton = [[UIButton alloc] initWithFrame:CGRectMake(150 - 1, 200, 150 + 1, 44)];
        [_confirmButton setTitle:@"确定" forState:UIControlStateNormal];
        [_confirmButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_confirmButton setBackgroundColor:[UIColor grayColor] forState:UIControlStateHighlighted];
        
        _confirmButton.titleLabel.font = [UIFont systemFontOfSize:20];
        _confirmButton.layer.borderColor = [UIColor colorWithRed:196/255.f green:196/255.f blue:196/255.f alpha:1].CGColor;
        _confirmButton.layer.borderWidth = 1;
        [_confirmButton addTarget:self action:@selector(confirmButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    }
    return _confirmButton;
}

- (void)confirmButtonClicked {
    NSString *resultString = _chineseArray[[_pickerView selectedRowInComponent:0]];
    NSInteger row = [_pickerView selectedRowInComponent:0];
    
    NSDictionary *dic = @{@"Value":[NSNumber numberWithInteger:row],
                          @"Chinese":resultString};
    
    if (_delegate && [_delegate respondsToSelector:@selector(singleLinePickerViewConfirmButtonClicked:)]) {
        [_delegate singleLinePickerViewConfirmButtonClicked:dic];
    }
    [self moveToHomePositon];
}

#pragma mark pickerView
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return _chineseArray.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return _chineseArray[row];
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    return 36;
}

- (void)moveToTargetPositon {
    [self.superview addSubview:self.maskView];
    [self.superview addSubview:self];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.2];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    
    self.maskView.alpha = 0.3;
    self.maskView.backgroundColor = [UIColor grayColor];
    
    self.frame = CGRectMake(_targetPosition.x, _targetPosition.y, 300, 244);
    [UIView commitAnimations];
}

- (void)moveToHomePositon {
    
    [UIView beginAnimations:@"RestoreDatePickerViewPosition" context:nil];
    [UIView setAnimationDuration:0.2];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDidStopSelector:@selector(didEndMoveToHomePosition)];
    [UIView setAnimationDelegate:self];
    
    _maskView.alpha = 0;
    
    self.frame = CGRectMake(_homePosition.x, _homePosition.y, 300, 244);
    [UIView commitAnimations];
}

- (void)didEndMoveToHomePosition {
    [_maskView removeFromSuperview];
    [self removeFromSuperview];
}

- (UIView *)maskView {
    if (!_maskView) {
        _maskView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
        _maskView.backgroundColor = [UIColor clearColor];
        _maskView.userInteractionEnabled = YES;
    }
    return _maskView;
}

@end
