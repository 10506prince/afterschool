//
//  TDSexPickerView.h
//  AfterSchool
//
//  Created by Teson Draw on 10/8/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TDSingleLinePickerViewDelegate <NSObject>

@optional
- (void)singleLinePickerViewCancelButtonClicked;
- (void)singleLinePickerViewConfirmButtonClicked:(NSDictionary *)result;

@end

@interface TDSingleLinePickerView : UIView <UIPickerViewDataSource, UIPickerViewDelegate>

@property (nonatomic, strong) UIPickerView *pickerView;
@property (nonatomic, strong) UIButton *cancelButton;
@property (nonatomic, strong) UIButton *confirmButton;

@property (nonatomic, assign) CGPoint homePosition;
@property (nonatomic, assign) CGPoint targetPosition;
@property (nonatomic, strong) UIView *maskView;

@property (nonatomic, strong) NSMutableArray *valueArray;
@property (nonatomic, strong) NSMutableArray *chineseArray;

@property (nonatomic, assign) NSInteger defaultRow;

@property (nonatomic, weak) id <TDSingleLinePickerViewDelegate> delegate;

/**
 *  初始化性别选择器（宽度为300，高度为144，其中日期高度位100，“取消”和“确定”按钮的高度是44,宽度为150）
 *
 *  @param frame          在父视图上的位置
 *  @param targetPosition 指定移动到父视图的某个位置
 *
 *  @return 返回UIView的实例
 */
- (instancetype)initWithFrame:(CGRect)frame targetPosition:(CGPoint)targetPosition;

/**
 *  初始化性别选择器（宽度为300，高度为144，其中日期高度位100，“取消”和“确定”按钮的高度是44,宽度为150）
 *
 *  @param frame          在父视图上的位置
 *  @param dataSource     字符串数组
 *  @param defaultRow     UIPickerView默认选中的
 *  @param targetPosition 指定移动到父视图的某个位置
 *
 *  @return 返回UIView的实例
 */
- (instancetype)initWithFrame:(CGRect)frame
                   dataSource:(NSArray *)dataSource
                   defaultRow:(NSInteger)defaultRow
               targetPosition:(CGPoint)targetPosition;

/**
 *  视图移动到指定位置
 */
- (void)moveToTargetPositon;

/**
 *  视图移回原位
 */
- (void)moveToHomePositon;

@end
