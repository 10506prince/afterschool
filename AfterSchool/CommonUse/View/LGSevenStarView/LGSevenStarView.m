//
//  LGSevenStarView.m
//  AfterSchool
//
//  Created by lg on 15/12/11.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGSevenStarView.h"

#define SINGLE_LINE_WIDTH           (1 / [UIScreen mainScreen].scale)
#define SINGLE_LINE_ADJUST_OFFSET   ((1 / [UIScreen mainScreen].scale) / 2)


#define LgColor(c,alp) [UIColor colorWithRed:(c)/255.0 green:(c)/255.0 blue:(c)/255.0 alpha:alp]
#define LgColorRGB(r,g,b,alp) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:alp]

//IB_DESIGNABLE
@interface LGSevenStarView ()

@property (nonatomic, assign) IBInspectable NSUInteger reference;///<参考值

@property (nonatomic, assign) IBInspectable NSUInteger assassinate;///<刺杀
@property (nonatomic, assign) IBInspectable NSUInteger existence;///<生存
@property (nonatomic, assign) IBInspectable NSUInteger assist;///<助攻
@property (nonatomic, assign) IBInspectable NSUInteger physical;///<物理
@property (nonatomic, assign) IBInspectable NSUInteger magic;///<魔法
@property (nonatomic, assign) IBInspectable NSUInteger defense;///<防御
@property (nonatomic, assign) IBInspectable NSUInteger money;///<金钱

@property (nonatomic, strong) CAShapeLayer * lineLayer;///<数据线
@property (nonatomic, strong) NSMutableArray <CAShapeLayer *> * backGroundLayers;///<背景图图层
@property (nonatomic, strong) NSMutableArray <CAShapeLayer *> * backLineLayers;///<背景线图层
@property (nonatomic, strong) NSMutableArray <UILabel *> * labels;///<字符提示

@end

@implementation LGSevenStarView

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setUp];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUp];
    }
    return self;
}
- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self setUp];
    }
    return self;
}
- (void)setUp {
    
    ///添加背景图图层
    self.backGroundLayers = [NSMutableArray array];
    
    NSArray * collors = @[
                          LgColorRGB(209, 217, 237, 1),
                          LgColorRGB(148, 163, 211, 1),
                          LgColorRGB(99, 118, 177, 1),
                          LgColorRGB(45, 62, 128, 1),
                          ];//各层颜色
    for (int i = 0; i < 4; i++) {
        CAShapeLayer * layer = [self getBackgroundLayerWithFillColor:collors[i]];
        [self.backGroundLayers addObject:layer];
        [self.layer addSublayer:layer];
    }
    
    
    ///添加背景线图层
    self.backLineLayers = [NSMutableArray array];
    
    for (int i = 0; i < 7; i++) {
        CAShapeLayer * layer = [self getBackgroundLineLayer];
        [self.backLineLayers addObject:layer];
        [self.layer addSublayer:layer];
    }
    
    
    ///添加字符提示
    self.labels = [NSMutableArray array];
    NSArray * name = @[@"刺杀",@"生存",@"助攻",@"物理",@"魔法",@"防御",@"金钱"];
    for (int i = 0; i < 7; i++) {
        UILabel * label = [self getLabelWithName:name[i]];
        [self.labels addObject:label];
        [self addSubview:label];
    }

    
    ///添加数据线图层
    self.lineLayer = [self getLinelayer];
    [self.layer addSublayer:self.lineLayer];
    
    self.reference = 150;
    
//    self.assassinate = 30;
//    self.existence = 40;
//    self.assist = 48;
//    self.physical = 79;
//    self.magic = 89;
//    self.defense = 0;
//    self.money = 8;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat width = self.bounds.size.width;
    CGFloat height = self.bounds.size.height;
    
    UILabel * label = self.labels[0];
    CGSize size = [label sizeThatFits:CGSizeZero];
    
    CGFloat spacing = size.width > size.height ? size.width : size.height;
    CGFloat radius = width < height ? width/2 : height/2;
    
    CGPoint center = CGPointMake(width/2, height/2);
    CGFloat backLayerRadius = radius - spacing;
    CGFloat labelRadius = radius - spacing/2;
    
    ///调整字符位置
    NSArray <NSValue *> * labelPointArray = [self getPolygonPointsWithCenter:center edges:7 radius:labelRadius];
    for (int i = 0; i < 7; i++) {
        UILabel * label = self.labels[i];
        [label sizeToFit];
        NSValue * value = labelPointArray[i];
        CGPoint point = [value CGPointValue];
        [label setCenter:point];
    }
    
    ///画背景图
    int count = 4;
    for (CAShapeLayer * layer in self.backGroundLayers) {
        layer.path = [self getPolygonWithCenter:center edges:7 radius:count * (backLayerRadius/4)];
        count--;
    }
    
    ///画背景线
    NSArray <NSValue *> * backLinePointArray = [self getPolygonPointsWithCenter:center edges:7 radius:backLayerRadius];
    
    for (int i = 0; i < 7; i++) {
        CAShapeLayer * layer = self.backLineLayers[i];
        NSValue * value = backLinePointArray[i];
        CGPoint point = [value CGPointValue];
        layer.path = [self getBackLinePathWithCenter:center targetPoint:point];
    }
    
    
    ///画数据线
    self.lineLayer.path = [self getSevenStarLinePathWithCenter:center edges:7 radius:backLayerRadius value:self.sevenStarValue];
    
}

///获取背景图图层
- (CAShapeLayer *)getBackgroundLayerWithFillColor:(UIColor *)fillColor {
    
    CAShapeLayer * layer = [CAShapeLayer layer];
    layer.fillColor = fillColor.CGColor;
    layer.strokeColor = LgColorRGB(188, 200, 238, 1).CGColor;
    layer.lineWidth = SINGLE_LINE_WIDTH;
    
    return layer;
}

///获取背景线图层
- (CAShapeLayer *)getBackgroundLineLayer {
    
    CAShapeLayer * layer = [CAShapeLayer layer];
    layer.fillColor = [UIColor clearColor].CGColor;
    layer.strokeColor = LgColorRGB(188, 200, 238, 1).CGColor;
    layer.lineWidth = SINGLE_LINE_WIDTH;
    
    return layer;
}

///获取字符提示
- (UILabel *)getLabelWithName:(NSString *)name {
    UILabel * label = [[UILabel alloc] init];
    [label setText:name];
    [label setFont:[UIFont systemFontOfSize:12]];
    [label setTextColor:LgColor(120, 1)];
    
    return label;
}

///获取数据线图层
- (CAShapeLayer *)getLinelayer {
    CAShapeLayer * layer= [CAShapeLayer layer];
    layer.fillColor = LgColorRGB(219, 63, 61, 0.5).CGColor;
    layer.strokeColor = LgColorRGB(219, 63, 61, 1).CGColor;
    layer.lineWidth = 1;
//    layer.strokeStart = 0;
//    layer.strokeEnd = 0;
    
    return layer;
}

#pragma mark - UIColor
//- (void)setBackStrokeColor:(UIColor *)backStrokeColor {
//    _backStrokeColor = backStrokeColor;
//    
//    self.backgroundRound.strokeColor = _backStrokeColor.CGColor;
//}
//
//- (void)setScaleStrokeColor:(UIColor *)scaleStrokeColor {
//    _scaleStrokeColor = scaleStrokeColor;
//    
//    self.scaleArc.strokeColor = _scaleStrokeColor.CGColor;
//    [self.scaleLabel setTextColor:_scaleStrokeColor];
//}
//
//- (void)setFillColor:(UIColor *)fillColor {
//    _fillColor = fillColor;
//    
//    self.backgroundRound.fillColor = _fillColor.CGColor;
//}


#pragma mark - Value
//@property (nonatomic, assign) IBInspectable NSUInteger reference;///<参考值
//
//@property (nonatomic, assign) IBInspectable NSUInteger assassinate;///<刺杀
//@property (nonatomic, assign) IBInspectable NSUInteger existence;///<生存
//@property (nonatomic, assign) IBInspectable NSUInteger assist;///<助攻
//@property (nonatomic, assign) IBInspectable NSUInteger physical;///<物理
//@property (nonatomic, assign) IBInspectable NSUInteger magic;///<魔法
//@property (nonatomic, assign) IBInspectable NSUInteger defense;///<防御
//@property (nonatomic, assign) IBInspectable NSUInteger money;///<金钱

- (void)setReference:(NSUInteger)reference {
    _reference = reference;
}

- (void)setAssassinate:(NSUInteger)assassinate {
    _assassinate = assassinate;
    _sevenStarValue.assassinate = _assassinate;
}

- (void)setExistence:(NSUInteger)existence {
    _existence = existence;
    _sevenStarValue.existence = _existence;
}

- (void)setAssist:(NSUInteger)assist {
    _assist = assist;
    _sevenStarValue.assist = _assist;
}

- (void)setPhysical:(NSUInteger)physical {
    _physical = physical;
    _sevenStarValue.physical = _physical;
}

- (void)setMagic:(NSUInteger)magic {
    _magic = magic;
    _sevenStarValue.magic = _magic;
}

- (void)setDefense:(NSUInteger)defense {
    _defense = defense;
    _sevenStarValue.defense = _defense;
}

- (void)setMoney:(NSUInteger)money {
    _money = money;
    _sevenStarValue.money = _money;
}

- (void)setSevenStarValue:(LGGamePlayerSevenStar)sevenStarValue {
    _sevenStarValue = sevenStarValue;
    
//    self.lineLayer.path = [self getSevenStarLinePathWithCenter:<#(CGPoint)#> edges:<#(NSUInteger)#> radius:<#(CGFloat)#> value:<#(LGGamePlayerSevenStar)#>]
//    [self layoutIfNeeded];
//    [self setNeedsLayout];
//    [self setNeedsFocusUpdate];
//    [self setNeedsDisplay];
}

///获取背景线路径
- (CGPathRef)getBackLinePathWithCenter:(CGPoint)center targetPoint:(CGPoint)point {
    CGMutablePathRef linePath = CGPathCreateMutable();
    CGPathMoveToPoint(linePath, NULL, center.x, center.y);
    CGPathAddLineToPoint(linePath, NULL, point.x, point.y);
    
    UIBezierPath * path = [UIBezierPath bezierPathWithCGPath:linePath];
    CGPathRelease(linePath);
    
    return path.CGPath;
}

///获取七星图数据线路径
- (CGPathRef)getSevenStarLinePathWithCenter:(CGPoint)center edges:(NSUInteger)edges radius:(CGFloat)radius value:(LGGamePlayerSevenStar)sevenStarValue {
    
    CGFloat totleRef = self.reference;
    
    NSUInteger radiuss[7] = {
        round(sevenStarValue.assassinate/totleRef * radius),
        round(sevenStarValue.existence/totleRef * radius),
        round(sevenStarValue.assist/totleRef * radius),
        round(sevenStarValue.physical/totleRef * radius),
        round(sevenStarValue.magic/totleRef * radius),
        round(sevenStarValue.defense/totleRef * radius),
        round(sevenStarValue.money/totleRef * radius)
    };
    
    ///获取点
    NSMutableArray * pointArray = [NSMutableArray array];
    double one = 2 * M_PI / edges;///每个区域夹角
    double start = -M_PI_2;///开始角度，默认顺时针计算
    for (int i = 0; i < edges; i++) {
        CGFloat x = cos(start) * radiuss[i] + center.x;
        CGFloat y = sin(start) * radiuss[i] + center.y;
        
        CGPoint point = CGPointMake(roundf(x), roundf(y));
        [pointArray addObject:[NSValue valueWithCGPoint:point]];
        
        start += one;///+为顺时针，-为逆时针
    }
    
    ///获取路径
    CGMutablePathRef path = CGPathCreateMutable();
    
    NSValue * value = pointArray[0];
    CGPoint point = [value CGPointValue];
    CGPathMoveToPoint(path, NULL, point.x, point.y);
    
    for (int i = 1; i < pointArray.count; i++) {
        NSValue * value = pointArray[i];
        CGPoint point = [value CGPointValue];
        
        CGPathAddLineToPoint(path, NULL, point.x, point.y);
    }
    CGPathCloseSubpath(path);///闭合
    
    UIBezierPath * sevenStarValuePath = [UIBezierPath bezierPathWithCGPath:path];
    CGPathRelease(path);
    
    return sevenStarValuePath.CGPath;
}

/**
 获取多边形路径
 center 中心
 edges 边数
 radius 半径
 */
- (CGPathRef)getPolygonWithCenter:(CGPoint)center edges:(NSUInteger)edges radius:(CGFloat)radius {
    
    NSArray * pointArray = [self getPolygonPointsWithCenter:center edges:edges radius:radius];
    
    CGMutablePathRef path = CGPathCreateMutable();
    
    NSValue * value = pointArray[0];
    CGPoint point = [value CGPointValue];
    CGPathMoveToPoint(path, NULL, point.x, point.y);
    
    for (int i = 1; i < pointArray.count; i++) {
        NSValue * value = pointArray[i];
        CGPoint point = [value CGPointValue];
        
        CGPathAddLineToPoint(path, NULL, point.x, point.y);
    }
    CGPathCloseSubpath(path);
    
    UIBezierPath * bezierPath = [UIBezierPath bezierPathWithCGPath:path];
    CGPathRelease(path);
    
    return bezierPath.CGPath;
}

///获取多边形点
- (NSArray<NSValue *> *)getPolygonPointsWithCenter:(CGPoint)center edges:(NSUInteger)edges radius:(CGFloat)radius {
    
    NSMutableArray * pointArray = [NSMutableArray array];
    
    double one = 2 * M_PI / edges;///每个区域夹角
    double start = -M_PI_2;///开始角度，默认顺时针计算
    
    for (int i = 0; i < edges; i++) {
        CGFloat x = cos(start) * radius + center.x;
        CGFloat y = sin(start) * radius + center.y;
        
        CGPoint point = CGPointMake(roundf(x), roundf(y));
        [pointArray addObject:[NSValue valueWithCGPoint:point]];
        
        start += one;///+为顺时针，-为逆时针
    }
    
    return pointArray;
}

@end
