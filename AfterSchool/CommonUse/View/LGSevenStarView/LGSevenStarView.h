//
//  LGSevenStarView.h
//  AfterSchool
//
//  Created by lg on 15/12/11.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LGGamePlayerInfo.h"

@interface LGSevenStarView : UIView

@property (nonatomic, assign) IBInspectable LGGamePlayerSevenStar sevenStarValue;

@end
