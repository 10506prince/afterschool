//
//  GameGoldTipView.h
//  AfterSchool
//
//  Created by Teson Draw on 11/24/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GameGoldTipView : UIView

@property (nonatomic, strong) UILabel *descriptionLabel;

- (instancetype)initWithOrigin:(CGPoint)orgin;

@end
