//
//  GameGoldTipView.m
//  AfterSchool
//
//  Created by Teson Draw on 11/24/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "GameGoldTipView.h"

@implementation GameGoldTipView

- (instancetype)initWithOrigin:(CGPoint)orgin
{
    self = [super initWithFrame:CGRectMake(orgin.x, orgin.y, 200, 44)];
    if (self) {
        self.backgroundColor = [UIColor colorWithRed:0.000 green:0.502 blue:0.502 alpha:1.000];
        self.layer.cornerRadius = 5;
        [self addSubview:self.descriptionLabel];
    }
    return self;
}

- (UILabel *)descriptionLabel {
    if (!_descriptionLabel) {
        _descriptionLabel = [[UILabel alloc] initWithFrame:self.bounds];
        _descriptionLabel.textColor = [UIColor whiteColor];
        _descriptionLabel.font = [UIFont systemFontOfSize:16];
        _descriptionLabel.textAlignment = NSTextAlignmentCenter;
        _descriptionLabel.text = @"1 游戏币 = 1 元人民币";
    }
    return _descriptionLabel;
}

@end
