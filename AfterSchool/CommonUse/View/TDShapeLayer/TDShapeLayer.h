//
//  TDShapeLayer.h
//  AfterSchool
//
//  Created by Teson Draw on 12/2/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>

@interface TDShapeLayer : CAShapeLayer

@property (nonatomic, assign) CGFloat width;
@property (nonatomic, assign) CGFloat height;

//- (instancetype)initWithWidth:(CGFloat)width height:(CGFloat)height;
+ (CAShapeLayer *)setArrowLayerWithWidth:(CGFloat)width
                                  height:(CGFloat)height;

+ (CAShapeLayer *)scaleLayerWithSuperviewWidth:(CGFloat)width
                                           height:(CGFloat)height;

@end
