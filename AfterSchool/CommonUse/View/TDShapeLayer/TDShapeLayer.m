//
//  TDShapeLayer.m
//  AfterSchool
//
//  Created by Teson Draw on 12/2/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "TDShapeLayer.h"

@implementation TDShapeLayer


//- (instancetype)initWithWidth:(CGFloat)width height:(CGFloat)height
//{
//    self = [super init];
//    if (self) {
//        _width = width;
//        _height = height;
//        [self addSublayer:[self getArrowLayer]];
//    }
//    return self;
//}

+ (CAShapeLayer *)setArrowLayerWithWidth:(CGFloat)width
                                  height:(CGFloat)height {
    CAShapeLayer * arrow = [CAShapeLayer layer];
    arrow.lineWidth = 2;
    arrow.fillColor = [UIColor clearColor].CGColor;
    arrow.strokeColor = [UIColor lightGrayColor].CGColor;
    CGPathRef path = [self getArrowPathWithOffset:CGSizeZero width:width height:height];
    arrow.path = path;
    
    return arrow;
}

+ (CGPathRef)getArrowPathWithOffset:(CGSize)offset
                              width:(CGFloat)width
                             height:(CGFloat)height {
    CGMutablePathRef path = CGPathCreateMutable();
    
    CGPoint start = CGPointMake(width - 22, height/2 + 7);
    CGPoint mid = CGPointMake(width - 15, height/2);
    CGPoint end = CGPointMake(width - 22, height/2 - 7);
    
    CGPathMoveToPoint(path, NULL, start.x, start.y);
    CGPathAddLineToPoint(path, NULL, mid.x, mid.y);
    CGPathAddLineToPoint(path, NULL, end.x, end.y);
    
    UIBezierPath * arrowPath = [UIBezierPath bezierPathWithCGPath:path];
    CGPathRelease(path);
    
    return arrowPath.CGPath;
}


+ (CAShapeLayer *)scaleLayerWithSuperviewWidth:(CGFloat)width
                                        height:(CGFloat)height {
    CAShapeLayer * scaleLayer = [CAShapeLayer layer];
    scaleLayer.lineWidth = 1;
    scaleLayer.fillColor = [UIColor clearColor].CGColor;
    scaleLayer.strokeColor = [UIColor lightGrayColor].CGColor;
    
    CGMutablePathRef path = CGPathCreateMutable();
    
    NSInteger leftMarginLength = (width - 250) / 2;
    NSInteger topMarginLength = height * 2 / 3 - 20;
    CGPoint start = CGPointMake(leftMarginLength, topMarginLength);
    CGPoint end = CGPointMake(width - leftMarginLength, topMarginLength);
    
    CGPoint verticalLine1Start =  CGPointMake(leftMarginLength, topMarginLength - 5);
    CGPoint verticalLine1End = CGPointMake(leftMarginLength, topMarginLength + 5);
    
    CGPoint verticalLine2Start =  CGPointMake(leftMarginLength + 50, topMarginLength - 5);
    CGPoint verticalLine2End = CGPointMake(leftMarginLength + 50, topMarginLength + 5);
    
    CGPoint verticalLine3Start =  CGPointMake(leftMarginLength + 2 * 50, topMarginLength - 5);
    CGPoint verticalLine3End = CGPointMake(leftMarginLength + + 2 * 50, topMarginLength + 5);
    
    CGPoint verticalLine4Start =  CGPointMake(leftMarginLength + 3 * 50, topMarginLength - 5);
    CGPoint verticalLine4End = CGPointMake(leftMarginLength + 3 * 50, topMarginLength + 5);
    
    CGPoint verticalLine5Start =  CGPointMake(leftMarginLength + 4 * 50, topMarginLength - 5);
    CGPoint verticalLine5End = CGPointMake(leftMarginLength + 4 * 50, topMarginLength + 5);
    
    CGPoint verticalLine6Start =  CGPointMake(leftMarginLength + 5 * 50, topMarginLength - 5);
    CGPoint verticalLine6End = CGPointMake(leftMarginLength + 5 * 50, topMarginLength + 5);
    
    CGPathMoveToPoint(path, NULL, start.x, start.y);
    CGPathAddLineToPoint(path, NULL, end.x, end.y);
    
    CGPathMoveToPoint(path, NULL, verticalLine1Start.x, verticalLine1Start.y);
    CGPathAddLineToPoint(path, NULL, verticalLine1End.x, verticalLine1End.y);
    
    CGPathMoveToPoint(path, NULL, verticalLine2Start.x, verticalLine1Start.y);
    CGPathAddLineToPoint(path, NULL, verticalLine2End.x, verticalLine1End.y);
    
    CGPathMoveToPoint(path, NULL, verticalLine3Start.x, verticalLine1Start.y);
    CGPathAddLineToPoint(path, NULL, verticalLine3End.x, verticalLine1End.y);
    
    CGPathMoveToPoint(path, NULL, verticalLine4Start.x, verticalLine1Start.y);
    CGPathAddLineToPoint(path, NULL, verticalLine4End.x, verticalLine1End.y);
    
    CGPathMoveToPoint(path, NULL, verticalLine5Start.x, verticalLine1Start.y);
    CGPathAddLineToPoint(path, NULL, verticalLine5End.x, verticalLine1End.y);
    
    CGPathMoveToPoint(path, NULL, verticalLine6Start.x, verticalLine1Start.y);
    CGPathAddLineToPoint(path, NULL, verticalLine6End.x, verticalLine1End.y);
    
    scaleLayer.path = path;
    CGPathRelease(path);
    
    return scaleLayer;
}

@end
