//
//  TDOrderEvaluationView.m
//  AfterSchool
//
//  Created by Teson Draw on 11/16/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "TDOrderEvaluationView.h"
#import "MacrosDefinition.h"
#import "UIButton+SetBackgroundColor.h"
#import "UIImageView+WebCache.h"

#import "TDPathRef.h"

@implementation TDOrderEvaluationView

+ (void)showWindowWithName:(NSString *)name
                      time:(NSString *)time
               portraitURL:(NSString *)portraitURL
                  delegate:(id <TDOrderEvaluationViewDelegate>)delegate
{
    UIWindow *keyWindow = [[[UIApplication sharedApplication] delegate] window];
    
    TDOrderEvaluationView *view = [[TDOrderEvaluationView alloc] initWithFrame:[[UIScreen mainScreen] bounds]
                                                                          name:name
                                                                          time:time
                                                                   portraitURL:portraitURL
                                                                      delegate:delegate];
    [keyWindow addSubview:view];
}

- (instancetype)initWithFrame:(CGRect)frame
                         name:(NSString *)name
                         time:(NSString *)time
                  portraitURL:(NSString *)portraitURL
                     delegate:(id <TDOrderEvaluationViewDelegate>)delegate
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initData];
        
        [self setBackgroundColor:[UIColor colorWithWhite:0.3 alpha:0.4]];
        
        [self addSubview:self.mainView];
        [_mainView addSubview:self.portraitBackgroundView];
        [_mainView addSubview:self.portraitImageView];
        [_mainView addSubview:self.nameLabel];
        [_mainView addSubview:self.timeLabel];
        
        [_mainView.layer addSublayer:self.downTriangleLayer];
        [_mainView addSubview:self.titleLabel];

        [self addStar];

        [self addSubview:self.doItLaterButton];
        
        _delegate = delegate;
        _nameLabel.text = name;
        _timeLabel.text = time;
        [_portraitImageView sd_setImageWithURL:[NSURL URLWithString:portraitURL] placeholderImage:[UIImage imageNamed:@"user_default_head"]];
    }
    
    return self;
}

- (void)initData {
    _score = 0;
}

- (UIView *)mainView {
    if (!_mainView) {
        _mainView = [[UIView alloc] initWithFrame:CGRectMake((SCREEN_WIDTH - 288) / 2, 60, 288, 324)];
        _mainView.backgroundColor = [UIColor whiteColor];
        _mainView.layer.cornerRadius = 6;
        _mainView.layer.masksToBounds = YES;
    }
    return _mainView;
}

- (UIView *)portraitBackgroundView {
    if (!_portraitBackgroundView) {
        _portraitBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 288, 164)];
        _portraitBackgroundView.backgroundColor = [UIColor colorWithRed:38/255.f green:166/255.f blue:154/255.f alpha:1];
    }
    return _portraitBackgroundView;
}

- (UIImageView *)portraitImageView {
    if (!_portraitImageView) {
        _portraitImageView = [[UIImageView alloc] initWithFrame:CGRectMake((288 - 80) / 2, 15, 80, 80)];
        _portraitImageView.layer.cornerRadius = 40;
        _portraitImageView.layer.masksToBounds = YES;
    }
    return _portraitImageView;
}

- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_portraitImageView.frame) + 14, _mainView.bounds.size.width, 16)];
        _nameLabel.textColor = [UIColor whiteColor];
        _nameLabel.font = [UIFont systemFontOfSize:16];
        _nameLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _nameLabel;
}

- (UILabel *)timeLabel {
    if (!_timeLabel) {
        _timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_nameLabel.frame) + 10, _mainView.bounds.size.width, 12)];
        _timeLabel.textColor = [UIColor whiteColor];
        _timeLabel.font = [UIFont systemFontOfSize:12];
        _timeLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _timeLabel;
}

- (UIImageView *)invertedTriangleImageView {
    if (!_invertedTriangleImageView) {
        _invertedTriangleImageView = [[UIImageView alloc] initWithFrame:CGRectMake(_mainView.bounds.size.width - 63, CGRectGetMaxY(_portraitBackgroundView.frame), 14, 10)];
        _invertedTriangleImageView.image = [UIImage imageNamed:@"separator_darkgray_inverted_triangle"];
    }
    return _invertedTriangleImageView;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_portraitBackgroundView.frame) + 25, 288, 16)];
        _titleLabel.textColor = [UIColor colorWithRed:102/255.f green:102/255.f blue:102/255.f alpha:1];
        _titleLabel.font = [UIFont systemFontOfSize:16];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.text = @"给个评分er呗！";
    }
    return _titleLabel;
}

- (UIView *)starBackgroundView {
    if (!_starBackgroundView) {
        _starBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_titleLabel.frame) + 4, 288, 68)];
        
        _starBackgroundView.layer.borderColor = [UIColor blackColor].CGColor;
        _starBackgroundView.layer.borderWidth = 1;
    }
    return _starBackgroundView;
}

- (void)addStar {
    RatingBar *ratingBar = [[RatingBar alloc] initWithFrame:CGRectMake((_mainView.bounds.size.width - 200) / 2, CGRectGetMaxY(_titleLabel.frame), 200, 76) deselectedImage:@"order_evaluation_gray_star" selectedImage:@"order_evaluation_solid_star" delegate:self];
    
    [_mainView addSubview:ratingBar];
}

- (void)starButtonClicked:(UIButton *)sender {
    
    _score = sender.tag % 100;
    NSLog(@"starButtonClicked score:%ld", (long)_score);
    
    [self showStarWithTag:sender.tag];
}

- (void)showStarWithTag:(NSInteger)tag {

    for (int i = 101; i < 106; i++) {
        UIButton *button = [_mainView viewWithTag:i];
        
        if (i <= tag) {
            [button setImage:[UIImage imageNamed:@"order_evaluation_solid_star"] forState:UIControlStateNormal];
            [button setImage:[UIImage imageNamed:@"order_evaluation_solid_star"] forState:UIControlStateHighlighted];
        } else {
            [button setImage:[UIImage imageNamed:@"order_evaluation_gray_star"] forState:UIControlStateNormal];
            [button setImage:[UIImage imageNamed:@"order_evaluation_gray_star"] forState:UIControlStateHighlighted];
        }
    }
}

- (UIButton *)doItLaterButton {
    if (!_doItLaterButton) {
        _doItLaterButton = [[UIButton alloc] initWithFrame:CGRectMake((self.bounds.size.width - 100) / 2, CGRectGetMaxY(_mainView.frame) - 25, 100, 50)];
        [_doItLaterButton setTitle:@"等会儿" forState:UIControlStateNormal];
        [_doItLaterButton setTitleColor:[UIColor colorWithRed:38/255.f green:166/255.f blue:154/255.f alpha:1] forState:UIControlStateNormal];
        
        [_doItLaterButton setBackgroundColor:[UIColor whiteColor]];
        [_doItLaterButton setBackgroundColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
        _doItLaterButton.layer.cornerRadius = 6;
        _doItLaterButton.layer.masksToBounds = YES;
        _doItLaterButton.titleLabel.font = [UIFont systemFontOfSize:16];
        [_doItLaterButton addTarget:self action:@selector(doItLaterButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        
        _doItLaterButton.layer.borderColor = [UIColor colorWithRed:38/255.f green:166/255.f blue:154/255.f alpha:1].CGColor;
        _doItLaterButton.layer.borderWidth = 2;
    }
    
    return _doItLaterButton;
}

- (void)doItLaterButtonClicked {
    if (_delegate && [_delegate respondsToSelector:@selector(orderEvaluationViewDoItLaterButtonClicked)]) {
        [_delegate orderEvaluationViewDoItLaterButtonClicked];
    }
    [self removeFromSuperview];
}

- (UIButton *)complaintButton {
    if (!_complaintButton) {
        _complaintButton = [[UIButton alloc] initWithFrame:CGRectMake(288 - 86, 324 - 44, 86, 44)];
        [_complaintButton setTitle:@"我要投诉？" forState:UIControlStateNormal];
        [_complaintButton setBackgroundColor:[UIColor clearColor]];
        _complaintButton.titleLabel.font = [UIFont systemFontOfSize:12];
        
        [_complaintButton setTitleColor:[UIColor colorWithRed:182/255.f green:182/255.f blue:182/255.f alpha:1] forState:UIControlStateNormal];
        [_complaintButton addTarget:self action:@selector(complaintButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return _complaintButton;
}

- (void)complaintButtonClicked {
    if (_delegate && [_delegate respondsToSelector:@selector(orderEvaluationViewComplaintButtonClicked)]) {
        [_delegate orderEvaluationViewComplaintButtonClicked];
    }
    
    [self removeFromSuperview];
}

- (CAShapeLayer *)downTriangleLayer {
    
    if (!_downTriangleLayer) {
        _downTriangleLayer = [CAShapeLayer layer];
        _downTriangleLayer.lineWidth = 1;
        _downTriangleLayer.fillColor = [UIColor colorWithRed:38/255.f green:166/255.f blue:154/255.f alpha:1].CGColor;
        _downTriangleLayer.strokeColor = [UIColor colorWithRed:38/255.f green:166/255.f blue:154/255.f alpha:1].CGColor;
        _downTriangleLayer.path = [TDPathRef downTriangleWithStart:CGPointMake(_mainView.bounds.size.width - 63, CGRectGetMaxY(_portraitBackgroundView.frame)) width:15 height:15];
    }

    return _downTriangleLayer;
}

#pragma mark - RatingBar delegate
- (void)ratingChanged:(float)newRating {
    NSLog(@"rating:%.0f", newRating);
    NSInteger score = (NSInteger)newRating;
    
    if (score > 0) {
        if (_delegate && [_delegate respondsToSelector:@selector(orderEvaluationViewGetScore:)]) {
            [_delegate orderEvaluationViewGetScore:score];
        }
        
        [self performSelector:@selector(removeSelfFromSuperview) withObject:nil afterDelay:0.5];
    }
}

- (void)removeSelfFromSuperview {
    [self removeFromSuperview];
}

@end
