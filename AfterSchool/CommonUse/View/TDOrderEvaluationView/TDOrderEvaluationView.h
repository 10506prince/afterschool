//
//  TDOrderEvaluationView.h
//  AfterSchool
//
//  Created by Teson Draw on 11/16/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RatingBar.h"

@protocol TDOrderEvaluationViewDelegate <NSObject>

@optional
- (void)orderEvaluationViewComplaintButtonClicked;
//- (void)orderEvaluationViewConfirmButtonClickedWithScore:(NSInteger)score;
- (void)orderEvaluationViewDoItLaterButtonClicked;
- (void)orderEvaluationViewGetScore:(NSInteger)score;

@end

@interface TDOrderEvaluationView : UIView <RatingBarDelegate>

@property (nonatomic, strong) UIView      *mainView;
@property (nonatomic, strong) UIView      *portraitBackgroundView;
@property (nonatomic, strong) UIView      *starBackgroundView;

@property (nonatomic, strong) UIImageView *portraitImageView;
@property (nonatomic, strong) UIImageView *invertedTriangleImageView;

@property (nonatomic, strong) UILabel     *nameLabel;
@property (nonatomic, strong) UILabel     *timeLabel;
@property (nonatomic, strong) UILabel     *titleLabel;

@property (nonatomic, strong) UIButton    *complaintButton;
@property (nonatomic, strong) UIButton    *doItLaterButton;

@property (nonatomic, strong) CAShapeLayer *downTriangleLayer;
//@property (nonatomic, strong) UIButton    *confirmButton;

@property (nonatomic, assign) NSInteger   score;

@property (nonatomic, assign) id <TDOrderEvaluationViewDelegate> delegate;

//@property (nonatomic, strong) UISwipeGestureRecognizer *leftSwipeGestureRecognizer;
//@property (nonatomic, strong) UISwipeGestureRecognizer *rightSwipeGestureRecognizer;

//@property (nonatomic, strong) UIPanGestureRecognizer *panGestureRecognizer;
//@property (nonatomic, strong) UITapGestureRecognizer *tapGestureRecognizer;


+ (void)showWindowWithName:(NSString *)name
                      time:(NSString *)time
               portraitURL:(NSString *)portraitURL
                  delegate:(id <TDOrderEvaluationViewDelegate>)delegate;
@end
