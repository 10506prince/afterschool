//
//  OptionView.m
//  MWOA
//
//  Created by Teson Draw on 3/16/15.
//  Copyright (c) 2015 Teson Draw. All rights reserved.
//

#import "OptionView.h"
#import "MacrosDefinition.h"

@implementation OptionView

- (instancetype)initWithOrigin:(CGPoint)orgin
                    optionName:(NSString *)optionName
                        remark:(NSString *)remark
                showRightArrow:(BOOL)showRightArrow
{
    self = [super init];
    if (self) {
        [self setFrame:CGRectMake(orgin.x, orgin.y, SCREEN_WIDTH, 50)];
        self.userInteractionEnabled = YES;

        //选项名称
        [self addSubview:self.titleLabel];
        _titleLabel.text = optionName;
        
        //备注标识
        if (remark) {
            self.remarkLabel = [[UILabel alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - 120 - 16, 19, 120, 14)];
            self.remarkLabel.text = remark;
            self.remarkLabel.font = [UIFont systemFontOfSize:20];
            self.remarkLabel.textAlignment = NSTextAlignmentRight;
            [self addSubview:self.remarkLabel];
        }
        
        //是否添加"向右箭头"图片
        if (showRightArrow) {
            [self addSubview:self.nextOperationImageView];
            
            //添加下一步操作按钮
            self.optionButton = [[UIButton alloc] initWithFrame:self.bounds];
            [self addSubview:self.optionButton];
        }
        
        //分隔线
        _separatorImageView = [[UIImageView alloc] initWithFrame:CGRectMake(14, 50 - 1, SCREEN_WIDTH - 14, 1)];
        _separatorImageView.backgroundColor = [UIColor colorWithRed:160/255.f green:160/255.f blue:160/255.f alpha:1];
        [self addSubview:_separatorImageView];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
                        title:(NSString *)title
               titleTextColor:(UIColor *)titleTextColor
                titleFontSize:(CGFloat)titleFontSize
              backgroundColor:(UIColor *)backgroundColor
                    showArrow:(BOOL)showArrow
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = backgroundColor;
        
        //选项名称
        [self addSubview:self.titleLabel];
        _titleLabel.text = title;
        _titleLabel.font = [UIFont systemFontOfSize:titleFontSize];
        _titleLabel.textColor = titleTextColor;
        
        if (showArrow) {
            [self addSubview:self.nextOperationImageView];
        }
        
        self.optionButton = [[UIButton alloc] initWithFrame:self.bounds];
        [self addSubview:self.optionButton];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
                        title:(NSString *)title
               titleTextColor:(UIColor *)titleTextColor
                titleFontSize:(CGFloat)titleFontSize
              backgroundColor:(UIColor *)backgroundColor
                    showArrow:(BOOL)showArrow
                   showSwitch:(BOOL)showSwitch {
    
    self = [super initWithFrame:frame];
    if (self) {
        if (backgroundColor) {
            self.backgroundColor = backgroundColor;
        } else {
            self.backgroundColor = [UIColor whiteColor];
        }
        
        //选项名称
        [self addSubview:self.titleLabel];
        _titleLabel.text = title;
        
        if (titleFontSize) {
           _titleLabel.font = [UIFont systemFontOfSize:titleFontSize];
        }
        
        if (titleTextColor) {
            _titleLabel.textColor = titleTextColor;
        }

        if (showArrow) {
            [self addSubview:self.nextOperationImageView];
        }
        
        if (showSwitch) {
            self.switchButton = [[UISwitch alloc] initWithFrame:CGRectMake(self.bounds.size.width - 65, 10.5, 25, 5)];
            [self addSubview:self.switchButton];
        } else {
            self.optionButton = [[UIButton alloc] initWithFrame:self.bounds];
            [self addSubview:self.optionButton];
        }
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
              backgroundColor:(UIColor *)backgroundColor
                    imageName:(NSString *)imageName
                 imageOriginX:(CGFloat)imageOriginX
                    imageSize:(CGSize)imageSize
                         text:(NSString *)text
                  textOriginX:(CGFloat)textOriginX
                    textColor:(UIColor *)textColor
                     fontSize:(CGFloat)fontSize
               showRightArrow:(BOOL)showRightArrow {
    self = [super initWithFrame:frame];
    if (self) {
        if (backgroundColor) {
            self.backgroundColor = backgroundColor;
        } else {
            self.backgroundColor = [UIColor whiteColor];
        }
        
        if (imageName) {
            _markImageView = [[UIImageView alloc] initWithFrame:CGRectMake(imageOriginX, (self.bounds.size.height - imageSize.height) / 2, imageSize.width, imageSize.height)];
            _markImageView.image = [UIImage imageNamed:imageName];
            
            [self addSubview:_markImageView];
        }
        
        if (text) {
            _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(textOriginX, 0, 180, self.frame.size.height)];
            _titleLabel.text = text;
            
            /**
             *  文字大小
             */
            if (fontSize < 1) {
                _titleLabel.font = [UIFont systemFontOfSize:14];
            } else {
                _titleLabel.font = [UIFont systemFontOfSize:fontSize];
            }
            
            /**
             *  文字颜色
             */
            if (textColor) {
                _titleLabel.textColor = textColor;
            } else {
                _titleLabel.textColor = [UIColor blackColor];
            }
            
            [self addSubview:_titleLabel];
        }
        
        if (showRightArrow) {
            [self addSubview:self.nextOperationImageView];
        }
        
        self.optionButton = [[UIButton alloc] initWithFrame:self.bounds];
        [self addSubview:self.optionButton];
    }
    
    return self;
}

//无备注，无“向右箭头”图片，有开关控件的选项条视图
- (instancetype)initWithOrigin:(CGPoint)orgin
                    optionName:(NSString *)optionName
{
    self = [super init];
    if (self) {
        [self setFrame:CGRectMake(orgin.x, orgin.y, SCREEN_WIDTH, 50)];
        self.userInteractionEnabled = YES;

        //选项名称
        [self addSubview:self.titleLabel];
        _titleLabel.text = optionName;
        
        self.switchButton = [[UISwitch alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - 65, 10.5, 25, 5)];
        [self addSubview:self.switchButton];
        
        //分隔线
        _separatorImageView = [[UIImageView alloc] initWithFrame:CGRectMake(14, 50 - 1, SCREEN_WIDTH - 14, 1)];
        _separatorImageView.backgroundColor = [UIColor colorWithRed:160/255.f green:160/255.f blue:160/255.f alpha:1];
        [self addSubview:_separatorImageView];
    }
    return self;
}

- (instancetype)initDetailsWithOrigin:(CGPoint)orgin
                           optionName:(NSString *)optionName
{
    self = [super init];
    if (self) {
        [self setFrame:CGRectMake(orgin.x, orgin.y, SCREEN_WIDTH, 50)];
        self.userInteractionEnabled = YES;
        
        //选项名称
        [self addSubview:self.titleLabel];
        _titleLabel.text = optionName;

        //是否添加"向右箭头"图片
        [self addSubview:self.nextOperationImageView];
        
        //添加下一步操作按钮
        self.optionButton = [[UIButton alloc] initWithFrame:self.bounds];
        [self addSubview:self.optionButton];
        
        [self addSubview:self.contentLabel];
        //分隔线
        _separatorImageView = [[UIImageView alloc] initWithFrame:CGRectMake(14, 50 - 1, SCREEN_WIDTH - 14, 1)];
        _separatorImageView.backgroundColor = [UIColor colorWithRed:160/255.f green:160/255.f blue:160/255.f alpha:1];
        [self addSubview:_separatorImageView];
    }
    return self;
}

- (instancetype)initCellWithFrame:(CGRect)frame
                            title:(NSString *)title
                   titleTextColor:(UIColor *)titleTextColor
                    titleFontSize:(CGFloat)titleFontSize
                  backgroundColor:(UIColor *)backgroundColor
                      contentText:(NSString *)contentText
                 contentTextColor:(UIColor *)contentTextColor
                  contentFontSize:(CGFloat)contentFontSize
                        showArrow:(BOOL)showArrow
                   showBottomLine:(BOOL)showBottomLine
{
    self = [super initWithFrame:frame];
    if (self) {
        if (backgroundColor) {
            self.backgroundColor = backgroundColor;
        } else {
            self.backgroundColor = [UIColor whiteColor];
        }
        
        //选项名称
        [self addSubview:self.titleLabel];
        _titleLabel.text = title;
        
        if (titleFontSize) {
            _titleLabel.font = [UIFont systemFontOfSize:titleFontSize];
        }
        
        if (titleTextColor) {
            _titleLabel.textColor = titleTextColor;
        }
        
        if (showArrow) {
            [self addSubview:self.nextOperationImageView];
        }
        
        _contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.bounds.size.width - 120 - 40, 0, 120, self.bounds.size.height)];
        _contentLabel.text = contentText;
        _contentLabel.textAlignment = NSTextAlignmentRight;
        
        if (contentTextColor) {
            _contentLabel.textColor = contentTextColor;
        } else {
            _contentLabel.textColor = [UIColor colorWithRed:147/255.f green:149/255.f blue:149/255.f alpha:1];
        }
        
        if (contentFontSize) {
            _contentLabel.font = [UIFont systemFontOfSize:contentFontSize];
        } else {
            _contentLabel.font = [UIFont systemFontOfSize:16];
        }
        
        [self addSubview:_contentLabel];
        
        if (showBottomLine) {
            //分隔线
            _separatorImageView = [[UIImageView alloc] initWithFrame:CGRectMake(12, self.bounds.size.height - 1, self.bounds.size.width - 12, 1)];
            _separatorImageView.backgroundColor = [UIColor colorWithRed:242/255.f green:243/255.f blue:244/255.f alpha:1];
            [self addSubview:_separatorImageView];
        }
        
        self.optionButton = [[UIButton alloc] initWithFrame:self.bounds];
        [self addSubview:self.optionButton];
    }
    
    return self;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(12, 0, 180, self.frame.size.height)];
        _titleLabel.font = [UIFont systemFontOfSize:18];
        _titleLabel.textColor = [UIColor blackColor];
    }
    return _titleLabel;
}

- (UILabel *)contentLabel {
    if (!_contentLabel) {
        _contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_titleLabel.frame), 0, SCREEN_WIDTH - CGRectGetMaxX(_titleLabel.frame) - 10 - 15 - 10, 50)];
        _contentLabel.textAlignment = NSTextAlignmentRight;
        _contentLabel.textColor = [UIColor darkGrayColor];
        _contentLabel.font = [UIFont systemFontOfSize:16];
    }
    return _contentLabel;
}

- (UIImageView *)nextOperationImageView {
    if (!_nextOperationImageView) {
        _nextOperationImageView = [[UIImageView alloc] initWithFrame:CGRectMake(self.bounds.size.width - 16 - 12, (self.bounds.size.height - 16) / 2, 12, 16)];
        _nextOperationImageView.image = [UIImage imageNamed:@"gray_color_right_arrow"];
    }
    return _nextOperationImageView;
}



@end
