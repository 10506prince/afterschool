//
//  OptionView.h
//  MWOA
//
//  Created by Teson Draw on 3/16/15.
//  Copyright (c) 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OptionView : UIView

@property (nonatomic, strong) UIImageView   *separatorImageView;

@property (nonatomic, strong) UILabel       *titleLabel;
@property (nonatomic, strong) UILabel       *remarkLabel;
@property (nonatomic, strong) UIButton      *optionButton;

@property (nonatomic, strong) UISwitch      *switchButton;

@property (nonatomic, strong) UILabel       *contentLabel;

@property (nonatomic, strong) UIImageView   *nextOperationImageView;

@property (nonatomic, strong) UIImageView   *markImageView;


//无开关控件的选项条视图
- (instancetype)initWithOrigin:(CGPoint)orgin
                    optionName:(NSString *)optionName
                        remark:(NSString *)remark
                showRightArrow:(BOOL)showRightArrow;

- (instancetype)initWithFrame:(CGRect)frame
                        title:(NSString *)title
               titleTextColor:(UIColor *)titleTextColor
                titleFontSize:(CGFloat)titleFontSize
              backgroundColor:(UIColor *)backgroundColor
                    showArrow:(BOOL)showArrow;

/**
 *  自定义操作条-继承于UIView
 *
 *  @param frame           操作条大小
 *  @param title           标题
 *  @param titleTextColor  标题颜色
 *  @param titleFontSize   标题文字大小
 *  @param backgroundColor 背景色
 *  @param showArrow       是否显示向右箭头
 *  @param showSwitch      是否显示切换开关
 *
 *  @return 返回操作条的实例
 */
- (instancetype)initWithFrame:(CGRect)frame
                        title:(NSString *)title
               titleTextColor:(UIColor *)titleTextColor
                titleFontSize:(CGFloat)titleFontSize
              backgroundColor:(UIColor *)backgroundColor
                    showArrow:(BOOL)showArrow
                   showSwitch:(BOOL)showSwitch;

/**
 *  自定义操作条-继承于UIView
 *
 *  @param frame            单元条框大小和位置
 *  @param title            标题
 *  @param titleTextColor   标题颜色
 *  @param titleFontSize    标题文字大小
 *  @param backgroundColor  背景色
 *  @param contentText      内容文字
 *  @param contentTextColor 内容文字颜色
 *  @param contentFontSize  内容文字大小
 *  @param showArrow        是否显示向右箭头
 *  @param showBottomLine   是否显示底部间隔条
 *
 *  @return 返回自定义单元格实例
 */
- (instancetype)initCellWithFrame:(CGRect)frame
                            title:(NSString *)title
                   titleTextColor:(UIColor *)titleTextColor
                    titleFontSize:(CGFloat)titleFontSize
                  backgroundColor:(UIColor *)backgroundColor
                      contentText:(NSString *)contentText
                 contentTextColor:(UIColor *)contentTextColor
                  contentFontSize:(CGFloat)contentFontSize
                        showArrow:(BOOL)showArrow
                   showBottomLine:(BOOL)showBottomLine;

//有开关控件的选项条视图
- (instancetype)initWithOrigin:(CGPoint)orgin
                    optionName:(NSString *)optionName;

//显示详细信息的选项条
- (instancetype)initDetailsWithOrigin:(CGPoint)orgin
                           optionName:(NSString *)optionName;

/**
 *  选项条-适合带有图片和文字选项条
 *
 *  @param frame           大小和位置
 *  @param backgroundColor 背景色
 *  @param imageName       图片名称
 *  @param imageOriginX    图片起始位置
 *  @param text            文字内容
 *  @param textOriginX     文字起始位置
 *  @param textColor       文字颜色
 *  @param fontSize        文字大小
 *  @param showRightArrow  是否显示右箭头
 *
 *  @return 返回选项条视图
 */
- (instancetype)initWithFrame:(CGRect)frame
              backgroundColor:(UIColor *)backgroundColor
                    imageName:(NSString *)imageName
                 imageOriginX:(CGFloat)imageOriginX
                    imageSize:(CGSize)imageSize
                         text:(NSString *)text
                  textOriginX:(CGFloat)textOriginX
                    textColor:(UIColor *)textColor
                     fontSize:(CGFloat)fontSize
               showRightArrow:(BOOL)showRightArrow;

@end
