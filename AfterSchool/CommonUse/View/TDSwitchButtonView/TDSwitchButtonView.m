//
//  TDSwitchButtonView.m
//  AfterSchool
//
//  Created by Teson Draw on 11/24/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "TDSwitchButtonView.h"

#import "MacrosDefinition.h"

@implementation TDSwitchButtonView

- (instancetype)initWithFrame:(CGRect)frame
          leftButtonImageName:(NSString *)leftButtonImageName
         rightButtonImageName:(NSString *)rightButtonImageName
{
    self = [super initWithFrame:frame];
    if (self) {
        
        _leftButtonImageName = leftButtonImageName;
        _rightButtonImageName = rightButtonImageName;
        
        [self addSubview:self.imageView];
        _imageView.image = [UIImage imageNamed:_leftButtonImageName];
        
        [self addSubview:self.leftButton];
        [self addSubview:self.rightButton];
    }
    return self;
}

- (UIImageView *)imageView {
    if (!_imageView) {
        _imageView = [[UIImageView alloc] initWithFrame:self.bounds];
    }
    return _imageView;
}

- (UIButton *)leftButton {
    if (!_leftButton) {
        _leftButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 132 / 2, 44)];
        _leftButton.tag = 100;
        [_leftButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return _leftButton;
}

- (UIButton *)rightButton {
    if (!_rightButton) {
        _rightButton = [[UIButton alloc] initWithFrame:CGRectMake(132 / 2, 0, 132 / 2, 44)];
        _rightButton.tag = 101;
        [_rightButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return _rightButton;
}

- (void)buttonClicked:(UIButton *)sender {
    switch (sender.tag) {
        case 100:
            _imageView.image = [UIImage imageNamed:_leftButtonImageName];
            break;
            
        case 101:
            _imageView.image = [UIImage imageNamed:_rightButtonImageName];
            break;
            
        default:
            break;
    }
    
    if (_delegate && [_delegate respondsToSelector:@selector(switchButtonViewButtonClicked:)]) {
        [_delegate switchButtonViewButtonClicked:sender];
    }
}

@end
