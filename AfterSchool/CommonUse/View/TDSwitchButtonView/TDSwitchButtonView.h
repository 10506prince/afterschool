//
//  TDSwitchButtonView.h
//  AfterSchool
//
//  Created by Teson Draw on 11/24/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TDSwitchButtonViewDelegate <NSObject>

- (void)switchButtonViewButtonClicked:(UIButton *)sender;

@end

@interface TDSwitchButtonView : UIView

@property (nonatomic, strong) UIImageView *imageView;

@property (nonatomic, strong) UIButton    *leftButton;
@property (nonatomic, strong) UIButton    *rightButton;

@property (nonatomic, strong) NSString    *leftButtonImageName;
@property (nonatomic, strong) NSString    *rightButtonImageName;

@property (nonatomic, assign) id <TDSwitchButtonViewDelegate> delegate;

- (instancetype)initWithFrame:(CGRect)frame
          leftButtonImageName:(NSString *)leftButtonImageName
         rightButtonImageName:(NSString *)rightButtonImageName;

@end
