//
//  TDTextField.m
//  AfterSchool
//
//  Created by Teson Draw on 10/20/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "TDTextField.h"

@implementation TDTextField

- (CGRect)editingRectForBounds:(CGRect)bounds {
    CGRect insetRect = CGRectMake(bounds.origin.x + 12, bounds.origin.y, bounds.size.width - 8, bounds.size.height);
    return insetRect;
}

- (CGRect)placeholderRectForBounds:(CGRect)bounds {
    CGRect insetRect = CGRectMake(bounds.origin.x + 12, bounds.origin.y, bounds.size.width - 8, bounds.size.height);
    return insetRect;
}

- (CGRect)textRectForBounds:(CGRect)bounds {
    CGRect insetRect = CGRectMake(bounds.origin.x + 12, bounds.origin.y, bounds.size.width - 8, bounds.size.height);
    return insetRect;
}

- (CGRect)clearButtonRectForBounds:(CGRect)bounds {
    CGRect rect = [super clearButtonRectForBounds:bounds];
    rect.origin.x -= 8;
    
    return rect;
}

@end
