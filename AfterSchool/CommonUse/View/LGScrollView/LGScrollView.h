//
//  LGScrollView.h
//  AfterSchool
//
//  Created by lg on 15/11/4.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, LGScrollViewHeadType) {
    LGScrollViewHeadTypeDefault = 0,///<默认为LGScrollViewHeadTypeLight
    LGScrollViewHeadTypeDark,
    LGScrollViewHeadTypeLight = LGScrollViewHeadTypeDefault,
};

typedef NS_ENUM(NSInteger, LGScrollViewBackgroundType) {
    LGScrollViewBackgroundTypeDefault = 0,///<默认为LGScrollViewBackgroundTypeLight
    LGScrollViewBackgroundTypeDark,
    LGScrollViewBackgroundTypeLight = LGScrollViewHeadTypeDefault,
};

@interface LGScrollView : UIScrollView

- (void)setBackgroundType:(LGScrollViewBackgroundType)LGScrollViewBackgroundType;
- (void)setHeadType:(LGScrollViewHeadType)LGScrollViewHeadType;

@end
