//
//  LGScrollView.m
//  AfterSchool
//
//  Created by lg on 15/11/4.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGScrollView.h"
#import "UIColor+RGB.h"

@interface LGScrollView () {
    CALayer * _headLayer;
}

@property (nonatomic, strong) CALayer * headLayer;

@end

@implementation LGScrollView

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setUp];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self setUp];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUp];
    }
    return self;
}

- (void)setUp {

    
    UIEdgeInsets ed = UIEdgeInsetsMake(64, 0, 0, 0);
    [self setContentInset:ed];
    [self setScrollIndicatorInsets:ed];
    
    [self setDelaysContentTouches:YES];
    [self setAlwaysBounceVertical:YES];
    
    [self setBackgroundType:LGScrollViewBackgroundTypeDefault];
}

- (void)setBackgroundType:(LGScrollViewBackgroundType)LGScrollViewBackgroundType {
    
    switch (LGScrollViewBackgroundType) {
        case LGScrollViewBackgroundTypeDark: {
            [self setBackgroundColor:[UIColor getColorDec:@"070070070"]];
        }
            break;
        case LGScrollViewBackgroundTypeLight: {
            [self setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
        }
            
        default:
            break;
    }
}
- (void)setHeadType:(LGScrollViewHeadType)LGScrollViewHeadType {
    if (!self.headLayer) {
        [self addHeadLayer];
    }
    
    switch (LGScrollViewHeadType) {
        case LGScrollViewHeadTypeLight:
        {
            [_headLayer setBackgroundColor:[UIColor groupTableViewBackgroundColor].CGColor];
        }
            break;
        case LGScrollViewHeadTypeDark: {
            [_headLayer setBackgroundColor:[UIColor getColorDec:@"070070070"].CGColor];
        }
            
        default:
            break;
    }
}

- (void)addHeadLayer {
    
//        _bubbleGradient.colors = [NSArray arrayWithObjects:
//                                  (id)[UIColor colorWithWhite:0 alpha:.75].CGColor,
//                                  (id)[UIColor colorWithWhite:0 alpha:.75].CGColor,
//                                  (id)[UIColor colorWithWhite:0.13 alpha:.75].CGColor,
//                                  (id)[UIColor colorWithWhite:0.33 alpha:.75].CGColor,
//                                  nil];

//        _bubbleGradient.locations = [NSArray arrayWithObjects:
//                                     [NSNumber numberWithFloat:0],
//                                     [NSNumber numberWithFloat:0.6],
//                                     [NSNumber numberWithFloat:.61],
//                                     [NSNumber numberWithFloat:1],
//                                     nil];

//        _bubbleGradient.startPoint = CGPointMake(0.0f, 1.0f);
//        _bubbleGradient.endPoint = CGPointMake(0.0f, 0.0f);
//        _bubbleGradient.mask = _shapeLayer;
    
    _headLayer = [[CALayer alloc] init];
    [_headLayer setFrame:CGRectMake(0, -self.bounds.size.height, self.bounds.size.width, self.bounds.size.height)];
    [self.layer addSublayer:_headLayer];
}

@end
