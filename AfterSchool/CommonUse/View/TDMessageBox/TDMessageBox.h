//
//  TDMessageBox.h
//  AfterSchool
//
//  Created by Teson Draw on 12/1/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TDMessageBox : UIView

@property (nonatomic, strong) UIView *boardView;
@property (nonatomic, strong) UIView *separatorLine;

@property (nonatomic, strong) UILabel *label1;
@property (nonatomic, strong) UILabel *label2;
@property (nonatomic, strong) UILabel *label3;

@property (nonatomic, strong) UIButton *button;

+ (void)show;

@end
