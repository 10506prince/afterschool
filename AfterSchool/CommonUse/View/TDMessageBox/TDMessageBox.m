//
//  TDMessageBox.m
//  AfterSchool
//
//  Created by Teson Draw on 12/1/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "TDMessageBox.h"
#import "UIButton+SetBackgroundColor.h"
#import <CoreText/CoreText.h>

@implementation TDMessageBox

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColor colorWithWhite:0.3 alpha:0.4]];
        
        [self addSubview:self.boardView];
        [_boardView addSubview:self.label1];
        [_boardView addSubview:self.label2];
        [_boardView addSubview:self.label3];
        [_boardView addSubview:self.separatorLine];
        [_boardView addSubview:self.button];
    }
    return self;
}

- (UIView *)boardView {
    if (!_boardView) {
        _boardView = [[UIView alloc] initWithFrame:CGRectMake((self.bounds.size.width - 300) / 2, 100, 300, 200)];
        _boardView.backgroundColor = [UIColor whiteColor];
        _boardView.layer.cornerRadius = 5;
        _boardView.layer.masksToBounds = YES;
    }
    return _boardView;
}

- (UILabel *)label1 {
    if (!_label1) {
        _label1 = [[UILabel alloc] initWithFrame:CGRectMake(0, 30, _boardView.bounds.size.width, 16)];
        _label1.textColor = [UIColor colorWithRed:133/255.f green:133/255.f blue:133/255.f alpha:1];
        _label1.font = [UIFont systemFontOfSize:14];
        _label1.textAlignment = NSTextAlignmentCenter;
        _label1.text = @"通过向朋友发送邀请，在好友成功注册后，";
    }
    return _label1;
}

- (UILabel *)label2 {
    if (!_label2) {
        _label2 = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_label1.frame) + 16, _boardView.bounds.size.width, 16)];
        _label2.textColor = [UIColor colorWithRed:133/255.f green:133/255.f blue:133/255.f alpha:1];
        _label2.font = [UIFont systemFontOfSize:14];
        _label2.textAlignment = NSTextAlignmentCenter;
        _label2.text = @"可获得游戏币30枚并自动存入您的账户。";
    }
    return _label2;
}

- (UILabel *)label3 {
    if (!_label3) {
        _label3 = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_label2.frame) + 30, _boardView.bounds.size.width, 16)];
        _label3.textColor = [UIColor colorWithRed:133/255.f green:133/255.f blue:133/255.f alpha:1];
        _label3.font = [UIFont systemFontOfSize:14];
        _label3.textAlignment = NSTextAlignmentCenter;
        
        NSMutableAttributedString *attriString = [[NSMutableAttributedString alloc] initWithString:@"游戏币只可用于 结算抵扣"];
        if (attriString.length == 10) {
            
        }
        [attriString addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:14] range:NSMakeRange(attriString.length - 4, 4)];

        _label3.attributedText = attriString;
    }
    return _label3;
}

- (UIView *)separatorLine {
    if (!_separatorLine) {
        _separatorLine = [[UIView alloc] initWithFrame:CGRectMake(0, _boardView.bounds.size.height - 47, _boardView.bounds.size.width, 1)];
        _separatorLine.backgroundColor = [UIColor colorWithRed:238/255.f green:240/255.f blue:240/255.f alpha:1];
    }
    return _separatorLine;
}

- (UIButton *)button {
    if (!_button) {
        _button = [[UIButton alloc] initWithFrame:CGRectMake(0, _boardView.bounds.size.height - 46, _boardView.bounds.size.width, 46)];
        [_button setTitle:@"确定" forState:UIControlStateNormal];
        [_button setTitleColor:[UIColor colorWithRed:76/255.f green:184/255.f blue:243/255.f alpha:1] forState:UIControlStateNormal];
        [_button setBackgroundColor:[UIColor clearColor]];
        [_button setBackgroundColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
        _button.titleLabel.font = [UIFont systemFontOfSize:20];
        [_button addTarget:self action:@selector(buttonClicked) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return _button;
}

- (void)buttonClicked {
    [self removeFromSuperview];
}

+ (void)show {
    UIWindow * keyWindow = [[[UIApplication sharedApplication] delegate] window];
    
    TDMessageBox *messageBox = [[TDMessageBox alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    [keyWindow addSubview:messageBox];
}

@end
