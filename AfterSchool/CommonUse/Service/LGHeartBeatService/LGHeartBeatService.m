//
//  LGHeartBeatService.m
//  AfterSchool
//
//  Created by lg on 16/1/8.
//  Copyright © 2016年 Teson Draw. All rights reserved.
//

#import "LGHeartBeatService.h"
#import "LGNetworking.h"
#import "ServerInterface.h"
#import "TDSingleton.h"

#define Count 30

@interface LGHeartBeatService ()

@property (nonatomic, strong) NSTimer * timer;

@end

@implementation LGHeartBeatService

+ (LGHeartBeatService*)sharedManager {
    static dispatch_once_t once;
    static LGHeartBeatService *sharedManager;
    dispatch_once(&once, ^ { sharedManager = [[LGHeartBeatService alloc] init]; });
    return sharedManager;
}

- (void)dealloc
{
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setUp];
    }
    return self;
}

- (void)setUp {
}

- (void)sendHeartBeat {
    NSMutableDictionary * params = [NSMutableDictionary dictionary];
    
    NSString * url = [NSString stringWithFormat:@"http://%@:%@/heart", [TDSingleton instance].gatewayServerIP,[TDSingleton instance].httpPort];
    
    NSString * sessionKey = [TDSingleton instance].sessionKey;
    
    [params setObject:sessionKey forKey:@"sessionKey"];

    
    [LGNetworking getRequestWithURL:url parameters:params success:^(id responseObject) {
        //NSLog(@"\n\n   responseObject = %@\n\n", responseObject);
        NSUInteger status = [[responseObject objectForKey:@"result"] integerValue];
        if (status == 1) {
            NSLog(@"发送心跳成功");
        } else {
            NSString * msg = [responseObject objectForKey:@"msg"];
            NSLog(@"%@", msg);
        }
    } failure:^(NSError *error) {
        NSLog(@"---%@", [error localizedDescription]);
    }];
}

- (void)start {
    if (!self.timer) {
        self.timer = [NSTimer timerWithTimeInterval:Count target:self selector:@selector(timerAction:) userInfo:nil repeats:YES];
        [[NSRunLoop mainRunLoop] addTimer:self.timer forMode:NSRunLoopCommonModes];
        [self.timer fire];
    } else {
        [self.timer fire];
    }
}
- (void)stop {
    [self.timer invalidate];
    self.timer = nil;
}

- (IBAction)timerAction:(id)sender {
    NSLog(@"%@ %s",NSStringFromClass([self class]), __FUNCTION__);
    [self sendHeartBeat];
}

@end
