//
//  LGHeartBeatService.h
//  AfterSchool
//
//  Created by lg on 16/1/8.
//  Copyright © 2016年 Teson Draw. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LGHeartBeatService : NSObject

+ (LGHeartBeatService*)sharedManager;

- (void)start;
- (void)stop;

@end
