//
//  LGLocationService.h
//  AfterSchool
//
//  Created by lg on 15/12/1.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <BaiduMapAPI_Location/BMKLocationComponent.h>
#import "NotificationName.h"

@interface LGLocationService : NSObject

+ (LGLocationService*)sharedManager;

///开始定位服务
- (void)startUserLocationService;

///更新定位服务（通知中心重新发送了位置更新通知）
- (void)updateUserLocationService;

///关闭定位服务
- (void)stopUserLocationService;

@property (nonatomic, strong) BMKUserLocation * userLocation;

@end
