//
//  LGLocationService.m
//  AfterSchool
//
//  Created by lg on 15/12/1.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGLocationService.h"
#import "LGDefineNetServer.h"
#import "TDSingleton.h"

@interface LGLocationService () <BMKLocationServiceDelegate> {
    BMKLocationService *_locService;///<定位服务
    
    BOOL _locationing;
}

@end

@implementation LGLocationService
+ (LGLocationService*)sharedManager {
    static dispatch_once_t once;
    static LGLocationService *sharedManager;
    dispatch_once(&once, ^ { sharedManager = [[LGLocationService alloc] init]; });
    return sharedManager;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setUp];
    }
    return self;
}

- (void)setUp {
    
    _locationing = NO;
    
    _locService = [[BMKLocationService alloc]init];
    [_locService setDistanceFilter:100];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 9.0) {
        [_locService setAllowsBackgroundLocationUpdates:YES];
    }
    _locService.delegate = self;
}

- (void)dealloc {
    if (_locService != nil) {
        _locService = nil;
    }
}

- (void)startUserLocationService {
    if (_locationing) {
        return;
    }
    [_locService startUserLocationService];
}

- (void)updateUserLocationService {
    if (_locationing) {
        [self didUpdateBMKUserLocation:_userLocation];
    } else {
        [_locService startUserLocationService];
    }
}

- (void)stopUserLocationService {
    if (_locationing) {
        [_locService stopUserLocationService];
    }
}

#pragma mark - BMKLocationServiceDelegate
/**
 *在将要启动定位时，会调用此函数
 */
- (void)willStartLocatingUser {
    NSLog(@"将要开始定位");
    _locationing = YES;
    [[NSNotificationCenter defaultCenter] postNotificationName:LGLocationStart object:nil];
}

/**
 *在停止定位后，会调用此函数
 */
- (void)didStopLocatingUser {
    NSLog(@"停止定位");
    _locationing = NO;
    [[NSNotificationCenter defaultCenter] postNotificationName:LGLocationStop object:nil];
}

/**
 *用户方向更新后，会调用此函数
 *@param userLocation 新的用户位置
 */
- (void)didUpdateUserHeading:(BMKUserLocation *)userLocation {
    
    self.userLocation = userLocation;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:LGLocationUpdateHead object:userLocation userInfo:nil];
}

/**
 *用户位置更新后，会调用此函数
 *@param userLocation 新的用户位置
 */
- (void)didUpdateBMKUserLocation:(BMKUserLocation *)userLocation {
    NSLog(@"更新定位");
    self.userLocation = userLocation;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:LGLocationUpdate object:userLocation userInfo:nil];
    
    ///上传位置
    LGPoiUserInfo * userLocationInfo = [[LGPoiUserInfo alloc] init];
    userLocationInfo.user_id = [TDSingleton instance].userInfoModel.account;
    userLocationInfo.location = userLocation.location.coordinate;
        
    if (userLocationInfo.user_id != 0) {
        [LGDefineNetServer updateLocation:userLocationInfo success:^{
        } failure:^{
        }];
    }
}

/**
 *定位失败后，会调用此函数
 *@param error 错误号
 */
- (void)didFailToLocateUserWithError:(NSError *)error {
    [[NSNotificationCenter defaultCenter] postNotificationName:LGLocationFail object:error userInfo:nil];
}

@end
