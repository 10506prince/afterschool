//
//  LGGeoService.h
//  AfterSchool
//
//  Created by lg on 15/12/2.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "LGGeocodeResult.h"

typedef void(^LGGeoCodeServiceSuccess)(void);
typedef void(^LGGeoCodeServiceSuccessResult)(id result);
typedef void(^LGGeoCodeServiceFailureResultMsg)(NSString * msg);

typedef void(^LGReverseGeoCodeSuccessResultBlock)(LGReverseGeoCodeResult * result);

@interface LGGeoCodeService : NSObject

+(void)reverseGeocodeWithPt:(CLLocationCoordinate2D)pt success:(LGReverseGeoCodeSuccessResultBlock)success failure:(LGGeoCodeServiceFailureResultMsg)failure;

+(void)geocodeWithCity:(NSString *)city address:(NSString *)address success:(LGGeoCodeServiceSuccessResult)success failure:(LGGeoCodeServiceFailureResultMsg)failure;

+ (void)reverseGeocodeWithPt:(CLLocationCoordinate2D)pt pois:(BOOL)poi success:(LGGeoCodeServiceSuccessResult)success failure:(LGGeoCodeServiceFailureResultMsg)failure;

@end
