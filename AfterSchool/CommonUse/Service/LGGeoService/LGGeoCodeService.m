//
//  LGGeoService.m
//  AfterSchool
//
//  Created by lg on 15/12/2.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGGeoCodeService.h"
#import "ServerInterface.h"
#import "LGNetworking.h"
#import <BaiduMapAPI_Search/BMKSearchComponent.h>

@interface LGGeoCodeService () {
    BMKGeoCodeSearch *_geoCodeSearch;
}

@end

@implementation LGGeoCodeService

+ (LGGeoCodeService*)sharedManager {
    static dispatch_once_t once;
    static LGGeoCodeService *sharedManager;
    dispatch_once(&once, ^ { sharedManager = [[LGGeoCodeService alloc] init]; });
    return sharedManager;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        _geoCodeSearch = [[BMKGeoCodeSearch alloc] init];
    }
    return self;
}

+ (void)reverseGeocodeWithPt:(CLLocationCoordinate2D)pt success:(LGReverseGeoCodeSuccessResultBlock)success failure:(LGGeoCodeServiceFailureResultMsg)failure {
    
    NSString * url = geocodeUrl;
    
    NSString * location = [NSString stringWithFormat:@"%f,%f", pt.latitude,pt.longitude];
    
    NSMutableDictionary * params = [NSMutableDictionary dictionary];
    [params setObject:poiAk forKey:@"ak"];
    [params setObject:@"json" forKey:@"output"];
    
    NSParameterAssert(location);
    [params setObject:location forKey:@"location"];
    [params setObject:@(0) forKey:@"pois"];//是否显示周边poi
    
    //NSLog(@"\n\n---url = %@\n---params = %@\n\n",url,params);
    
    [LGNetworking getRequestWithURL:url parameters:params success:^(id responseObject) {
        //NSLog(@"\n\n   responseObject = %@\n\n", responseObject);
        NSUInteger status = [[responseObject objectForKey:@"status"] integerValue];
        if (status == 0) {
            LGReverseGeoCodeResult * result = [LGReverseGeoCodeResult objectWithKeyValues:[responseObject objectForKey:@"result"]];
            success(result);
        } else {
            NSString * msg = [responseObject objectForKey:@"msg"];
            NSLog(@"%@", msg);
            failure(msg);
        }
    } failure:^(NSError *error) {
        NSLog(@"---%@", [error localizedDescription]);
        failure([error localizedDescription]);
    }];
}

+ (void)reverseGeocodeWithPt:(CLLocationCoordinate2D)pt pois:(BOOL)poi success:(LGGeoCodeServiceSuccessResult)success failure:(LGGeoCodeServiceFailureResultMsg)failure {
    
    NSString * url = geocodeUrl;
    
    NSString * location = [NSString stringWithFormat:@"%f,%f", pt.latitude,pt.longitude];
    
    NSMutableDictionary * params = [NSMutableDictionary dictionary];
    [params setObject:poiAk forKey:@"ak"];
    [params setObject:@"json" forKey:@"output"];
    
    NSParameterAssert(location);
    [params setObject:location forKey:@"location"];
    [params setObject:@(poi) forKey:@"pois"];//是否显示周边poi
    
    //NSLog(@"\n\n---url = %@\n---params = %@\n\n",url,params);
    
    [LGNetworking getRequestWithURL:url parameters:params success:^(id responseObject) {
        //NSLog(@"\n\n   responseObject = %@\n\n", responseObject);
        NSUInteger status = [[responseObject objectForKey:@"status"] integerValue];
        if (status == 0) {
            LGReverseGeoCodeResult * result = [LGReverseGeoCodeResult objectWithKeyValues:[responseObject objectForKey:@"result"]];
            success(result.pois);
        } else {
            NSString * msg = [responseObject objectForKey:@"msg"];
            NSLog(@"%@", msg);
            failure(msg);
        }
    } failure:^(NSError *error) {
        NSLog(@"---%@", [error localizedDescription]);
        failure([error localizedDescription]);
    }];
}

+ (void)geocodeWithCity:(NSString *)city address:(NSString *)address success:(LGGeoCodeServiceSuccessResult)success failure:(LGGeoCodeServiceFailureResultMsg)failure {
    
    NSString * url = geocodeUrl;
    
    NSMutableDictionary * params = [NSMutableDictionary dictionary];
    [params setObject:poiAk forKey:@"ak"];
    [params setObject:@"json" forKey:@"output"];
    
    NSParameterAssert(address);
    [params setObject:address forKey:@"address"];
    
    if (city) {
        [params setObject:city forKey:@"city"];
    }
    
    NSLog(@"\n\n---url = %@\n---params = %@\n\n",url,params);
    
    [LGNetworking getRequestWithURL:url parameters:params success:^(id responseObject) {
        NSLog(@"\n\n   responseObject = %@\n\n", responseObject);
        NSUInteger status = [[responseObject objectForKey:@"status"] integerValue];
        if (status == 0) {
            NSDictionary * result = [responseObject objectForKey:@"result"];
            LGGeoCodeResult * geoCodeResult = [LGGeoCodeResult objectWithKeyValues:result];
            success(geoCodeResult);
        } else {
            NSString * msg = [responseObject objectForKey:@"msg"];
            NSLog(@"%@", msg);
            failure(msg);
        }
    } failure:^(NSError *error) {
        NSLog(@"---%@", [error localizedDescription]);
        failure([error localizedDescription]);
    }];
}



@end
