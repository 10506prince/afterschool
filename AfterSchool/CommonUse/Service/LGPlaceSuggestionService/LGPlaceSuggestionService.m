//
//  LGPlaceSuggestionService.m
//  AfterSchool
//
//  Created by lg on 15/12/3.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGPlaceSuggestionService.h"
#import "LGNetworking.h"
#import "ServerInterface.h"

@implementation LGPlaceSuggestionSearch

@end

@implementation LGPlaceSuggestionResult

@end

@implementation LGPlaceSuggestionService
+ (void)getSuggestionListWithSearchParam:(LGPlaceSuggestionSearch *)searchInfo success:(LGPlaceSuggestionServiceSuccessResult)success failure:(LGPlaceSuggestionServiceFailure)failure {
    NSString * url = placeSuggestionUrl;
    
//    NSString * location = [NSString stringWithFormat:@"%f,%f", pt.latitude,pt.longitude];
    
    NSMutableDictionary * params = [NSMutableDictionary dictionary];
    [params setObject:bmkAk forKey:@"ak"];
    [params setObject:@"json" forKey:@"output"];
    [params setObject:[[NSBundle mainBundle] bundleIdentifier] forKey:@"mcode"];
    
    NSParameterAssert(searchInfo.keyWord);
    NSParameterAssert(searchInfo.region);
    
    [params setObject:searchInfo.keyWord forKey:@"q"];
    [params setObject:searchInfo.region forKey:@"region"];
    
    if (searchInfo.location) {
        [params setObject:searchInfo.location forKey:@"location"];
    }
    
    NSLog(@"\n\n---url = %@\n---params = %@\n\n",url,params);
    
//    [LGNetworking requestWithURL:url parameters:params success:^(id responseObject) {
//        //NSLog(@"\n\n   responseObject = %@\n\n", responseObject);
//        NSUInteger status = [[responseObject objectForKey:@"status"] integerValue];
//        if (status == 0) {
//            NSArray <LGPlaceSuggestionResult *> * result = [LGPlaceSuggestionResult objectArrayWithKeyValuesArray:[responseObject objectForKey:@"result"]];
//            success(result);
//        } else {
//            NSString * msg = [responseObject objectForKey:@"message"];
//            NSLog(@"%@", msg);
//            failure();
//        }
//    } failure:^(NSError *error) {
//        NSLog(@"---%@", [error localizedDescription]);
//        failure();
//    }];
    
    [LGNetworking getRequestWithURL:url parameters:params success:^(id responseObject) {
        //NSLog(@"\n\n   responseObject = %@\n\n", responseObject);
        NSUInteger status = [[responseObject objectForKey:@"status"] integerValue];
        if (status == 0) {
            NSArray <LGPlaceSuggestionResult *> * result = [LGPlaceSuggestionResult objectArrayWithKeyValuesArray:[responseObject objectForKey:@"result"]];
            success(result);
        } else {
            NSString * msg = [responseObject objectForKey:@"message"];
            NSLog(@"%@", msg);
            failure();
        }
    } failure:^(NSError *error) {
        NSLog(@"---%@", [error localizedDescription]);
        failure();
    }];
}
@end


