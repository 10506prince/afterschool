//
//  LGPlaceSuggestionService.h
//  AfterSchool
//
//  Created by lg on 15/12/3.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "LGPoiUserInfo.h"
#import "LGGeocodeResult.h"

typedef void(^LGPlaceSuggestionServiceSuccess)(void);
typedef void(^LGPlaceSuggestionServiceFailure)(void);

typedef void(^LGPlaceSuggestionServiceSuccessResult)(id result);
typedef void(^LGPlaceSuggestionServiceFailureResult)(NSError * error);

@class LGPlaceSuggestionSearch,LGPlaceSuggestionResult;

///检索参数
@interface LGPlaceSuggestionSearch : NSObject

///输入建议关键字（支持拼音）：上海、天安门、中关、shanghai（必须）
@property (nonatomic, copy) NSString * keyWord;
///所属城市/区域名称或代号：全国、北京市、131、江苏省等（必须）
@property (nonatomic, copy) NSString * region;
///传入location参数后，返回结果将以距离进行排序:	40.047857537164,116.31353434477（非必须）
@property (nonatomic, copy) NSString * location;
/////返回数据格式，可选json、xml两种（默认xml）
//@property (nonatomic, copy) NSString * outPut;
/////开发者访问密钥，必选。
//@property (nonatomic, copy) NSString * ak;
/////用户的权限签名
//@property (nonatomic, copy) NSString * sn;
/////设置sn后该值必选
//@property (nonatomic, copy) NSString * timestamp;

@end

///检索结果
@interface LGPlaceSuggestionResult : NSObject
///坐标
@property (nonatomic, strong) LGLocation *location;
///唯一id
@property (nonatomic, copy) NSString *uid;
///城市
@property (nonatomic, copy) NSString *city;
///方向
@property (nonatomic, copy) NSString *district;
///城市id
@property (nonatomic, copy) NSString *cityid;
///商圈
@property (nonatomic, copy) NSString *business;
///名称
@property (nonatomic, copy) NSString *name;

@end



@interface LGPlaceSuggestionService : NSObject

+ (void)getSuggestionListWithSearchParam:(LGPlaceSuggestionSearch *)searchInfo success:(LGPlaceSuggestionServiceSuccessResult)success failure:(LGPlaceSuggestionServiceFailure)failure;

@end


