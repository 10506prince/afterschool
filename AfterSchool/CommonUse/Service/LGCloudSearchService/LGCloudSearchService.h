//
//  LGCloudSearchService.h
//  AfterSchool
//
//  Created by lg on 15/12/2.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LGCloudSearchOption.h"

typedef void(^LGCloudSearchServiceSuccess)(void);
typedef void(^LGCloudSearchServiceFailure)(void);
typedef void(^LGCloudSearchServiceFailureResultMsg)(NSString *msg);

/**
 *  @brief 百度云检索成功返回block
 *
 *  @param result POI结果列表
 *  @param size   分页参数，当前页返回数量
 *  @param total  分页参数，所有召回数量
 */
typedef void(^LGCloudSearchServiceSuccessResult)(NSArray <LGCloudPOIInfo *> *result, NSUInteger size, NSUInteger total);
//typedef void(^LGCloudSearchServiceSuccessResult)(id result);
typedef void(^LGCloudSearchServiceFailureResult)(NSError * error);

@interface LGCloudSearchService : NSObject

+ (void)localSearchWithSearchParam:(LGCloudLocalSearchInfo *)searchInfo success:(LGCloudSearchServiceSuccessResult)success failure:(LGCloudSearchServiceFailureResultMsg)failure;
+ (void)nearbySearchWithSearchParam:(LGCloudNearbySearchInfo *)searchInfo success:(LGCloudSearchServiceSuccessResult)success failure:(LGCloudSearchServiceFailureResultMsg)failure;
+ (void)boundSearchWithSearchParam:(LGCloudBoundSearchInfo *)searchInfo success:(LGCloudSearchServiceSuccessResult)success failure:(LGCloudSearchServiceFailureResultMsg)failure;
//+ (void)detailSearchWithSearchParam:(LGCloudDetailSearchInfo *)searchInfo success:(LGCloudSearchServiceSuccessResult)success failure:(LGCloudSearchServiceFailure)failure;

@end
