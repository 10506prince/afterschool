//
//  LGCloudSearchService.m
//  AfterSchool
//
//  Created by lg on 15/12/2.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGCloudSearchService.h"
#import "ServerInterface.h"
#import "LGNetworking.h"

@implementation LGCloudSearchService

+ (void)localSearchWithSearchParam:(LGCloudLocalSearchInfo *)searchInfo success:(LGCloudSearchServiceSuccessResult)success failure:(LGCloudSearchServiceFailureResultMsg)failure {
    NSString * url = localCloudSearchUrl;
    NSMutableDictionary * params = [NSMutableDictionary dictionary];
    
    [params setObject:poiAk forKey:@"ak"];
    [params setObject:poiTableId forKey:@"geotable_id"];
    if (searchInfo.pageIndex) {
        [params setObject:@(searchInfo.pageIndex) forKey:@"page_index"];
    }
    if (searchInfo.pageSize) {
        [params setObject:@(searchInfo.pageSize) forKey:@"page_size"];
    }
    
    //NSParameterAssert(searchInfo.keyword);
    NSParameterAssert(searchInfo.region);
    
    if (searchInfo.keyword) {
        [params setObject:searchInfo.keyword forKey:@"q"];
    }
    [params setObject:searchInfo.region forKey:@"region"];
    
    if (searchInfo.sortby) {
        [params setObject:searchInfo.sortby forKey:@"sortby"];
    }
    if (searchInfo.filter) {
        [params setObject:searchInfo.filter forKey:@"filter"];
//        [params setObject:@"role_type:2|online_state:1|attack:6188,105188|grade:41,131" forKey:@"filter"];
    }
    
    //NSLog(@"\n\n---url = %@\n---searchInfo = %@\n--- params = %@\n\n",url,searchInfo,params);
    
    [LGNetworking getRequestWithURL:url parameters:params success:^(id responseObject) {
        //NSLog(@"\n\n   responseObject = %@\n\n", responseObject);
        NSUInteger status = [[responseObject objectForKey:@"status"] integerValue];
        if (status == 0) {
            NSArray * pois = [LGCloudPOIInfo objectArrayWithKeyValuesArray:responseObject[@"contents"]];
            NSUInteger size = [[responseObject objectForKey:@"size"] integerValue];
            NSUInteger total = [[responseObject objectForKey:@"total"] integerValue];
            success(pois, size, total);
        } else {
            NSString * message = [responseObject objectForKey:@"message"];
            NSLog(@"%@", message);
            failure(message);
        }
    } failure:^(NSError *error) {
        NSLog(@"---%@", [error localizedDescription]);
        failure([error localizedDescription]);
    }];
}

+ (void)nearbySearchWithSearchParam:(LGCloudNearbySearchInfo *)searchInfo success:(LGCloudSearchServiceSuccessResult)success failure:(LGCloudSearchServiceFailureResultMsg)failure {
    NSString * url = nearbyCloudSearchUrl;
    NSMutableDictionary * params = [NSMutableDictionary dictionary];
    
    [params setObject:poiAk forKey:@"ak"];
    [params setObject:poiTableId forKey:@"geotable_id"];
    if (searchInfo.pageIndex) {
        [params setObject:@(searchInfo.pageIndex) forKey:@"page_index"];
    }
    if (searchInfo.pageSize) {
        [params setObject:@(searchInfo.pageSize) forKey:@"page_size"];
    }
    
    NSParameterAssert(searchInfo.location);
    [params setObject:searchInfo.location forKey:@"location"];
    
    if (searchInfo.radius) {
        [params setObject:@(searchInfo.radius) forKey:@"radius"];
    }
    if (searchInfo.keyword) {
        [params setObject:searchInfo.keyword forKey:@"q"];
    }
    if (searchInfo.sortby) {
        [params setObject:searchInfo.sortby forKey:@"sortby"];
    }
    if (searchInfo.filter) {
        [params setObject:searchInfo.filter forKey:@"filter"];
    }
    
    //NSLog(@"\n\n---url = %@\n---searchInfo = %@\n\n",url,searchInfo);
    
    [LGNetworking getRequestWithURL:url parameters:params success:^(id responseObject) {
        //NSLog(@"\n\n   responseObject = %@\n\n", responseObject);
        NSUInteger status = [[responseObject objectForKey:@"status"] integerValue];
        if (status == 0) {
            NSArray <LGCloudPOIInfo *> * pois = [LGCloudPOIInfo objectArrayWithKeyValuesArray:responseObject[@"contents"]];
            NSUInteger size = [[responseObject objectForKey:@"size"] integerValue];
            NSUInteger total = [[responseObject objectForKey:@"total"] integerValue];
            success(pois, size, total);
        } else {
            NSString * message = [responseObject objectForKey:@"message"];
            NSLog(@"%@", message);
            failure(message);
        }
    } failure:^(NSError *error) {
        NSLog(@"---%@", [error localizedDescription]);
        failure([error localizedDescription]);
    }];
}

+ (void)boundSearchWithSearchParam:(LGCloudBoundSearchInfo *)searchInfo success:(LGCloudSearchServiceSuccessResult)success failure:(LGCloudSearchServiceFailureResultMsg)failure {
    NSString * url = boundCloudSearchUrl;
    NSMutableDictionary * params = [NSMutableDictionary dictionary];
    
    [params setObject:poiAk  forKey:@"ak"];
    [params setObject:poiTableId forKey:@"geotable_id"];
    if (searchInfo.pageIndex) {
        [params setObject:@(searchInfo.pageIndex) forKey:@"page_index"];
    }
    if (searchInfo.pageSize) {
        [params setObject:@(searchInfo.pageSize) forKey:@"page_size"];
    }
    
    NSParameterAssert(searchInfo.keyword);
    NSParameterAssert(searchInfo.bounds);
    [params setObject:searchInfo.keyword forKey:@"q"];
    [params setObject:searchInfo.bounds forKey:@"bounds"];
    
    if (searchInfo.sortby) {
        [params setObject:searchInfo.sortby forKey:@"sortby"];
    }
    if (searchInfo.filter) {
        [params setObject:searchInfo.filter forKey:@"filter"];
    }
    
    //NSLog(@"\n\n---url = %@\n---params = %@\n\n",url,params);
    
    [LGNetworking getRequestWithURL:url parameters:params success:^(id responseObject) {
        //NSLog(@"\n\n   responseObject = %@\n\n", responseObject);
        NSUInteger status = [[responseObject objectForKey:@"status"] integerValue];
        if (status == 0) {
            NSArray * pois = [LGCloudPOIInfo objectArrayWithKeyValuesArray:responseObject[@"contents"]];
            NSUInteger size = [[responseObject objectForKey:@"size"] integerValue];
            NSUInteger total = [[responseObject objectForKey:@"total"] integerValue];
            success(pois, size, total);
        } else {
            NSString * message = [responseObject objectForKey:@"message"];
            NSLog(@"%@", message);
            failure(message);
        }
    } failure:^(NSError *error) {
        NSLog(@"---%@", [error localizedDescription]);
        failure([error localizedDescription]);
    }];
}

//+ (void)detailSearchWithSearchParam:(LGCloudDetailSearchInfo *)searchInfo success:(LGCloudSearchServiceSuccessResult)success failure:(LGCloudSearchServiceFailure)failure {
//    
//    NSMutableDictionary * params = [NSMutableDictionary dictionary];
//    [params setObject:poiAk forKey:@"ak"];
//    [params setObject:poiTableId forKey:@"geotable_id"];
//    
//    NSString * url = [detailCloudSearchUrl stringByAppendingPathComponent:searchInfo.uid];
//    
//    NSLog(@"\n\n---url = %@\n---searchInfo = %@\n\n",url,searchInfo);
//    
//    [LGNetworking getRequestWithURL:url parameters:params success:^(id responseObject) {
//        NSLog(@"\n\n   responseObject = %@\n\n", responseObject);
//        NSUInteger status = [[responseObject objectForKey:@"result"] integerValue];
//        if (status == 0) {
//            success(@"这里还没处理。。。");
//        } else {
//            failure();
//        }
//    } failure:^(NSError *error) {
//        NSLog(@"---%@", [error localizedDescription]);
//        failure();
//    }];
//}

@end
