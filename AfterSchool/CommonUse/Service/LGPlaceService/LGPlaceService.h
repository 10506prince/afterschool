//
//  LGPlaceService.h
//  AfterSchool
//
//  Created by lg on 15/12/10.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LGServiceCommonModel.h"
#import "MJExtension.h"

@class LGPlaceResult;

typedef void(^LGPlaceServiceSuccess)(void);
typedef void(^LGPlaceServiceFailure)(void);

typedef void(^LGPlaceServiceSuccessResult)(NSArray <LGPlaceResult *> * result);
typedef void(^LGPlaceServiceFailureResult)(NSError * error);

@interface LGPlaceBaseSearch : NSObject

@property (nonatomic, copy) NSString * keyWord;

///检索结果详细程度。取值为1 或空，则返回基本信息；取值为2，返回检索POI详细信息(必须)
@property (nonatomic, assign) NSUInteger scope;
///检索过滤条件，当scope取值为2时，可以设置filter进行排序(非必须)

@property (nonatomic, copy) NSString * filter;
@property (nonatomic, assign) NSUInteger pageSize;
@property (nonatomic, assign) NSUInteger pageNum;

@end

@interface LGPlaceLocalSearch : LGPlaceBaseSearch

///检索区域（市级以上行政区域），如果取值为“全国”或某省份，则返回指定区域的POI及数量。 格式：北京、131、全国（必须）
@property (nonatomic, copy) NSString * region;

@end

@interface LGPlaceNearbySearch : LGPlaceBaseSearch

///周边检索中心点，不支持多个点 38.76623,116.43213 lat<纬度>,lng<经度> (必须)
@property (nonatomic, copy) NSString * location;

///周边检索半径，单位为米 (非必须) 默认：2000
@property (nonatomic, assign) NSUInteger radius;

@end

@interface LGPlaceBoundsSearch : LGPlaceBaseSearch

///38.76623,116.43213,39.54321,116.46773 lat,lng(左下角坐标),lat,lng(右上角坐标)(必须)
@property (nonatomic, copy) NSString * bounds;

@end

@interface LGPlaceResult : NSObject


@property (nonatomic, copy) NSString *street_id;

@property (nonatomic, assign) NSInteger detail;

@property (nonatomic, copy) NSString *uid;

@property (nonatomic, copy) NSString *name;

@property (nonatomic, strong) LGLocation *location;

@property (nonatomic, copy) NSString *address;

///自定义（用来按距离排序）
@property (nonatomic, assign) double distance;

@end

@interface LGPlaceService : UIView

+ (void)getPlaceListWithSearchParam:(LGPlaceBaseSearch *)searchInfo success:(LGPlaceServiceSuccessResult)success failure:(LGPlaceServiceFailure)failure;

@end

