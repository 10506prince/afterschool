//
//  LGPlaceService.m
//  AfterSchool
//
//  Created by lg on 15/12/10.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGPlaceService.h"
#import "LGNetworking.h"
#import "ServerInterface.h"

@implementation LGPlaceBaseSearch

@end

@implementation LGPlaceLocalSearch

@end

@implementation LGPlaceNearbySearch

@end

@implementation LGPlaceBoundsSearch

@end

@implementation LGPlaceResult

@end


@implementation LGPlaceService

+ (void)getPlaceListWithSearchParam:(LGPlaceBaseSearch *)searchInfo success:(LGPlaceServiceSuccessResult)success failure:(LGPlaceServiceFailure)failure {
    
    NSString * url = placeUrl;
    
    NSMutableDictionary * params = [NSMutableDictionary dictionary];
    [params setObject:bmkAk forKey:@"ak"];
    [params setObject:@"json" forKey:@"output"];
    [params setObject:[[NSBundle mainBundle] bundleIdentifier] forKey:@"mcode"];
    
    NSParameterAssert(searchInfo.keyWord);
    [params setObject:searchInfo.keyWord forKey:@"q"];
    
    if ([searchInfo isKindOfClass:[LGPlaceLocalSearch class]]) {
        LGPlaceLocalSearch * search = (LGPlaceLocalSearch *)searchInfo;
        NSParameterAssert(search.region);
        [params setObject:search.region forKey:@"region"];
    } else if ([searchInfo isKindOfClass:[LGPlaceNearbySearch class]]) {
        LGPlaceNearbySearch * search = (LGPlaceNearbySearch *)searchInfo;
        NSParameterAssert(search.location);
        [params setObject:search.location forKey:@"location"];
        
        if (search.radius > 0) {
            [params setObject:@(search.radius) forKey:@"radisu"];
        }
    } else if([searchInfo isKindOfClass:[LGPlaceBoundsSearch class]]) {
        LGPlaceBoundsSearch * search = (LGPlaceBoundsSearch *)searchInfo;
        NSParameterAssert(search.bounds);
        [params setObject:search.bounds forKey:@"bounds"];
    }
    
    if (searchInfo.scope > 0) {
        [params setObject:@(searchInfo.scope) forKey:@"scope"];
    }
    
    if (searchInfo.pageSize > 0) {
        [params setObject:@(searchInfo.pageSize) forKey:@"page_size"];
    }
    
    if (searchInfo.pageNum > 0) {
        [params setObject:@(searchInfo.pageNum) forKey:@"page_num"];
    }
    
    NSLog(@"\n\n---url = %@\n---params = %@\n\n",url,params);
    [LGNetworking getRequestWithURL:url parameters:params success:^(id responseObject) {
        //NSLog(@"\n\n   responseObject = %@\n\n", responseObject);
        NSUInteger status = [[responseObject objectForKey:@"status"] integerValue];
        if (status == 0) {
            NSArray <LGPlaceResult *> * result = [LGPlaceResult objectArrayWithKeyValuesArray:[responseObject objectForKey:@"results"]];
            success(result);
        } else {
            NSString * msg = [responseObject objectForKey:@"message"];
            NSLog(@"%@", msg);
            failure();
        }
    } failure:^(NSError *error) {
        NSLog(@"---%@", [error localizedDescription]);
        failure();
    }];
}

@end


