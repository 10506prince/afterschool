//
//  LGServiceCommonModel.h
//  AfterSchool
//
//  Created by lg on 15/12/10.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LGLocation : NSObject

@property (nonatomic, assign) double lat;///<纬度
@property (nonatomic, assign) double lng;///<经度

@end
