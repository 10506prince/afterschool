//
//  LGDefineNetServer.m
//  AfterSchool
//
//  Created by lg on 15/10/28.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGDefineNetServer.h"
#import "ServerInterface.h"
#import "LGNetworking.h"
#import "TDSingleton.h"

#import <AliyunOSSiOS/OSSService.h>
#import <AliyunOSSiOS/OSSCompat.h>
#import "NSString+MD5String.h"

@implementation LGDefineNetServer

@end

@implementation LGDefineNetServer (Master)

+ (void)getUserDetailWithAccount:(LGDaKaInfo *)userInfo success:(LGDefineNetServerSuccessResult)success failure:(LGDefineNetServerFailureResultMsg)failure {
    NSString * url = [TDSingleton instance].gatewayServerBusinessURL;
    NSString * sessionKey = [TDSingleton instance].sessionKey;
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    
    [param setObject:actionDaKaDetail forKey:@"action"];
    [param setObject:userInfo.account forKey:@"user"];
    [param setObject:sessionKey forKey:@"sessionKey"];
    
    //NSLog(@"\n\n---url = %@\n---sessionKey = %@\n---param = %@\n\n",url,sessionKey,param);
    
    [LGNetworking requestWithURL:url parameters:param success:^(id responseObject) {
        //NSLog(@"%@",responseObject);
        NSInteger status = [[responseObject objectForKey:@"result"] integerValue];
        if (status == 1) {
            NSDictionary * usersInfo = [responseObject objectForKey:@"userInfo"];
            LGDaKaInfoHelp * userInfoHelp = [LGDaKaInfoHelp objectWithKeyValues:usersInfo];
            success(userInfoHelp);
        } else {
            NSString * msg = [responseObject objectForKey:@"msg"];
            NSLog(@"%@", msg);
            failure(msg);
        }
        
    } failure:^(NSError *error) {
        NSLog(@"%@", [error localizedDescription]);
        failure([error localizedDescription]);
    }];
}

+ (void)getUsersInfoWithAccounts:(NSArray<NSString *> *)accounts success:(LGDefineNetServerGetUsersInfoSuccessResult)success failure:(LGDefineNetServerFailureResultMsg)failure {
    
    if (accounts.count == 0) {
        return;
    }
    
    NSMutableString * accountsStr = [NSMutableString stringWithFormat:@""];
    for (NSString * account in accounts) {
        [accountsStr appendFormat:@"%@,", account];
    }
    
    NSString * url = [TDSingleton instance].gatewayServerBusinessURL;
    NSString * sessionKey = [TDSingleton instance].sessionKey;
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    
    [param setObject:actionDaKaInfo forKey:@"action"];
    [param setObject:accountsStr forKey:@"users"];
    [param setObject:sessionKey forKey:@"sessionKey"];
    
    //NSLog(@"\n\n---url = %@\n---sessionKey = %@\n---param = %@\n\n",url,sessionKey,param);
    
    [LGNetworking requestWithURL:url parameters:param success:^(id responseObject) {
        //NSLog(@"%@",responseObject);
        NSInteger status = [[responseObject objectForKey:@"result"] integerValue];
        if (status == 1) {
            NSArray<NSDictionary *> * usersInfo = [responseObject objectForKey:@"usersInfo"];
            if (usersInfo && usersInfo.count > 0) {
                
                NSArray<LGDaKaInfoHelp *> * userInfoHelps = [LGDaKaInfoHelp objectArrayWithKeyValuesArray:usersInfo];
                success(userInfoHelps);
            }
        } else {
            NSString * msg = [responseObject objectForKey:@"msg"];
            NSLog(@"%@",msg);
            failure(msg);
        }
        
    } failure:^(NSError *error) {
        NSLog(@"%@", [error localizedDescription]);
        failure([error localizedDescription]);
    }];
}

+ (void)reportUsersInfoPosition:(NSString *)position success:(LGDefineNetServerSuccessResult)success failure:(LGDefineNetServerFailureResultMsg)failure {
    NSString * url = [TDSingleton instance].gatewayServerBusinessURL;
    NSString * sessionKey = [TDSingleton instance].sessionKey;
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    
    [param setObject:actionDaKaLocationReport forKey:@"action"];
    [param setObject:position forKey:@"position"];
    [param setObject:sessionKey forKey:@"sessionKey"];
    
    //NSLog(@"\n\n---url = %@\n---sessionKey = %@\n---param = %@\n\n",url,sessionKey,param);
    
    [LGNetworking requestWithURL:url parameters:param success:^(id responseObject) {
        //NSLog(@"%@",responseObject);
        NSInteger status = [[responseObject objectForKey:@"result"] integerValue];
        if (status == 1) {
            success(nil);
        } else {
            NSString * msg = [responseObject objectForKey:@"msg"];
            NSLog(@"%@", msg);
            failure(msg);
        }
        
    } failure:^(NSError *error) {
        NSLog(@"%@", [error localizedDescription]);
        failure([error localizedDescription]);
    }];

}

+ (void)changeUserInfoReceiveOrderState:(BOOL)unreceiveOrder success:(LGDefineNetServerSuccessResultMsg)success failure:(LGDefineNetServerFailureResultMsg)failure {
    NSString * url = [TDSingleton instance].gatewayServerBusinessURL;
    NSString * sessionKey = [TDSingleton instance].sessionKey;
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    
    [param setObject:actionUserRectiveStateChange forKey:@"action"];
    [param setObject:@(unreceiveOrder) forKey:@"state"];
    [param setObject:sessionKey forKey:@"sessionKey"];
    
    //NSLog(@"\n\n---url = %@\n---sessionKey = %@\n---param = %@\n\n",url,sessionKey,param);
    [LGNetworking requestWithURL:url parameters:param success:^(id responseObject) {
        //NSLog(@"%@",responseObject);
        NSInteger status = [[responseObject objectForKey:@"result"] integerValue];
        NSString * msg = [responseObject objectForKey:@"msg"];
        if (status == 1) {
            success(msg);
        } else {
            failure(msg);
        }
        
    } failure:^(NSError *error) {
        NSLog(@"%@", [error localizedDescription]);
        failure([error localizedDescription]);
    }];
}
@end

@implementation LGDefineNetServer (UserInfo)

/*
+ (void)updateUserMapId:(NSString *)userMapId success :(LGDefineNetServerSuccess)success failure:(LGDefineNetServerFailure)failure {
    NSString * url = [TDSingleton instance].gatewayServerBusinessURL;
    NSString * sessionKey = [TDSingleton instance].sessionKey;
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    
    [param setObject:actionUserMapIdUpdate forKey:@"action"];
    [param setObject:sessionKey forKey:@"sessionKey"];
    [param setObject:userMapId forKey:@"mapUserId"];
    
    NSLog(@"\n\n---url = %@\n---sessionKey = %@\n---param = %@\n\n",url,sessionKey,param);
    
    [LGNetworking requestWithURL:url parameters:param success:^(id responseObject) {
        NSLog(@"\n\n---responseObject = %@\n\n", responseObject);
        NSInteger status = [[responseObject objectForKey:@"result"] integerValue];
        if (status == 1) {
            success();
        } else {
            failure();
        }
    } failure:^(NSError *error) {
        NSLog(@"---%@", [error localizedDescription]);
        failure();
    }];
}
 */

///获取阿里云OSS token
+ (void)getAliOSSTokenWithAccount:(NSString *)account success:(void(^)(OSSFederationToken * token, NSString * endpoint, NSString * bucketName))success failure:(LGDefineNetServerFailureResultMsg)failure {

    NSString * url = [TDSingleton instance].URL.getAliOSSTokenURL;
    
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    NSParameterAssert(account.length);
    [param setObject:account forKey:@"userName"];

    NSLog(@"\n\n---url = %@\n---param = %@\n\n",url,param);
    [LGNetworking requestWithURL:url parameters:param success:^(id responseObject) {
        NSLog(@"%@", responseObject);
        NSInteger state = [[responseObject objectForKey:@"result"] integerValue];
        if (state == 1) {
            
            NSString * endpoint = [responseObject objectForKey:@"endpoint"];
            NSString * bucketName = [responseObject objectForKey:@"bucket"];
            
            OSSFederationToken * token = [OSSFederationToken new];
            token.tAccessKey = [responseObject objectForKey:@"accessKeyId"];
            token.tSecretKey = [responseObject objectForKey:@"accessKeySecret"];
            token.tToken = [responseObject objectForKey:@"securityToken"];
            token.expirationTimeInGMTFormat = [responseObject objectForKey:@"expiration"];
            
            success(token, endpoint, bucketName);
        } else {
            NSString * msg = [responseObject objectForKey:@"msg"];
            failure(msg);
        }

    } failure:^(NSError *error) {
        NSLog(@"%@", [error localizedDescription]);
        failure([error localizedDescription]);
    }];
}

///上传头像
+ (void)uploadUserIcon:(NSData *)imageData account:(NSString *)account success:(LGDefineNetServerUpLoadUserIconSuccess)success failure:(LGDefineNetServerFailureResultMsg)failure progress:(LGDefineNetServerUpLoadUserIconProgress)progress {
    
    ///获取阿里云上传头像token
    [LGDefineNetServer getAliOSSTokenWithAccount:account success:^(OSSFederationToken *token, NSString *endpoint, NSString *bucketName) {
        
        NSString * date = [NSString stringWithFormat:@"%.f", round([[NSDate date] timeIntervalSince1970]*1000)];
        NSString * objectName = [[NSString stringWithFormat:@"%@%@", account, date].MD5String stringByAppendingString:@".png"];
        NSString * objectKey = [@"headIcon/" stringByAppendingString:objectName];
        
        ///创建上传对象
        OSSPutObjectRequest * put = [OSSPutObjectRequest new];
        // 必填参数
        put.uploadingData = imageData;
        put.bucketName = bucketName;
        put.objectKey = objectKey;
        // 选填参数
        put.contentType = @".png";
        put.contentDisposition = date;
        put.uploadProgress = ^(int64_t bytesSent, int64_t totalByteSent, int64_t totalBytesExpectedToSend) {
            NSLog(@"%lld, %lld, %lld", bytesSent, totalByteSent, totalBytesExpectedToSend);
            NSString * progressMsg = [NSString stringWithFormat:@"上传中 %.f%%", ((float)totalByteSent)/totalBytesExpectedToSend*100];
            progress(progressMsg);
        };
        
        
        ///STS鉴权模式
        id<OSSCredentialProvider> credential = [[OSSFederationCredentialProvider alloc] initWithFederationTokenGetter:^OSSFederationToken * {
            return token;
        }];
        OSSClient * client = [[OSSClient alloc] initWithEndpoint:endpoint credentialProvider:credential];
        
        ///开始上传
        OSSTask * putTask = [client putObject:put];
        [putTask continueWithBlock:^id(OSSTask *task) {
            //NSLog(@"objectKey: %@", put.objectKey);
            //NSLog(@"task:%@",task);
            if (!task.error) {
                success(objectKey);
            } else {
                failure([task.error localizedDescription]);
            }
            return nil;
        }];
    } failure:^(NSString *msg) {
        failure(msg);
    }];
}

///上传图片Teson Draw
+ (void)uploadImage:(UIImage *)image
          objectKey:(NSString *)objectKey
            account:(NSString *)account
            success:(TDUploadUserIconSuccess)success
            failure:(LGDefineNetServerFailureResultMsg)failure
           progress:(LGDefineNetServerUpLoadUserIconProgress)progress
{
    ///获取阿里云上传头像token
    [LGDefineNetServer getAliOSSTokenWithAccount:account success:^(OSSFederationToken *token, NSString *endpoint, NSString *bucketName) {
        
//        NSString * date = [NSString stringWithFormat:@"%.f", round([[NSDate date] timeIntervalSince1970]*1000)];
//        NSString * objectName = [[NSString stringWithFormat:@"%@%@", account, date].MD5String stringByAppendingString:@".png"];
//        NSString * objectKey = [NSString stringWithFormat:@"%@/%@", folderName, objectName];
        
        NSData *imageData = UIImagePNGRepresentation(image);
        
        if (!imageData) {
            imageData = UIImageJPEGRepresentation(image, 1);
        }
        
        ///创建上传对象
        OSSPutObjectRequest * put = [OSSPutObjectRequest new];
        // 必填参数
        put.uploadingData = imageData;
        put.bucketName = bucketName;
        put.objectKey = objectKey;
        // 选填参数
        put.contentType = @".png";
        put.contentDisposition = @"图片";
        put.uploadProgress = ^(int64_t bytesSent, int64_t totalByteSent, int64_t totalBytesExpectedToSend) {
            NSLog(@"%lld, %lld, %lld", bytesSent, totalByteSent, totalBytesExpectedToSend);
            NSString * progressMsg = [NSString stringWithFormat:@"上传中 %0.0f%%", (float)totalByteSent/(float)totalBytesExpectedToSend*100.0];
            progress(progressMsg);
        };
        
        
        ///STS鉴权模式
        id<OSSCredentialProvider> credential = [[OSSFederationCredentialProvider alloc] initWithFederationTokenGetter:^OSSFederationToken * {
            return token;
        }];
        OSSClient * client = [[OSSClient alloc] initWithEndpoint:endpoint credentialProvider:credential];
        
        ///开始上传
        OSSTask * putTask = [client putObject:put];
        [putTask continueWithBlock:^id(OSSTask *task) {
            //NSLog(@"objectKey: %@", put.objectKey);
            //NSLog(@"task:%@",task);
            if (!task.error) {
                success();
            } else {
                failure([task.error localizedDescription]);
            }
            return nil;
        }];
    } failure:^(NSString *msg) {
        failure(msg);
    }];
}

/////上传图片Teson Draw
//+ (void)uploadImage:(UIImage *)image folderName:(NSString *)folderName account:(NSString *)account success:(LGDefineNetServerUpLoadUserIconSuccess)success failure:(LGDefineNetServerFailureResultMsg)failure progress:(LGDefineNetServerUpLoadUserIconProgress)progress {
//    
//    ///获取阿里云上传头像token
//    [LGDefineNetServer getAliOSSTokenWithAccount:account success:^(OSSFederationToken *token, NSString *endpoint, NSString *bucketName) {
//        
//        NSString * date = [NSString stringWithFormat:@"%.f", round([[NSDate date] timeIntervalSince1970]*1000)];
//        NSString * objectName = [[NSString stringWithFormat:@"%@%@", account, date].MD5String stringByAppendingString:@".png"];
//        NSString * objectKey = [NSString stringWithFormat:@"%@/%@", folderName, objectName];
//        
//         NSData *imageData = UIImagePNGRepresentation(image);
//        
//        if (!imageData) {
//            imageData = UIImageJPEGRepresentation(image, 1);
//        }
//        
//        ///创建上传对象
//        OSSPutObjectRequest * put = [OSSPutObjectRequest new];
//        // 必填参数
//        put.uploadingData = imageData;
//        put.bucketName = bucketName;
//        put.objectKey = objectKey;
//        // 选填参数
//        put.contentType = @".png";
//        put.contentDisposition = date;
//        put.uploadProgress = ^(int64_t bytesSent, int64_t totalByteSent, int64_t totalBytesExpectedToSend) {
//            NSLog(@"%lld, %lld, %lld", bytesSent, totalByteSent, totalBytesExpectedToSend);
//            NSString * progressMsg = [NSString stringWithFormat:@"上传中 %%%0.2f", (float)totalByteSent/(float)totalBytesExpectedToSend*100.0];
//            progress(progressMsg);
//        };
//        
//        
//        ///STS鉴权模式
//        id<OSSCredentialProvider> credential = [[OSSFederationCredentialProvider alloc] initWithFederationTokenGetter:^OSSFederationToken * {
//            return token;
//        }];
//        OSSClient * client = [[OSSClient alloc] initWithEndpoint:endpoint credentialProvider:credential];
//        
//        ///开始上传
//        OSSTask * putTask = [client putObject:put];
//        [putTask continueWithBlock:^id(OSSTask *task) {
//            //NSLog(@"objectKey: %@", put.objectKey);
//            //NSLog(@"task:%@",task);
//            if (!task.error) {
//                success(objectKey);
//            } else {
//                failure([task.error localizedDescription]);
//            }
//            return nil;
//        }];
//    } failure:^(NSString *msg) {
//        failure(msg);
//    }];
//}

///更新头像
+ (void)updateUserIcon:(UIImage *)image success:(LGDefineNetServerUpdateUserIconSuccessResult)success failure:(LGDefineNetServerFailureResultMsg)failure progress:(LGDefineNetServerUpdateUserIconProgress)progress {

    NSParameterAssert(image);
    NSString * account = [TDSingleton instance].userInfoModel.account;
    NSData * imageData = UIImagePNGRepresentation(image);
    
    [LGDefineNetServer uploadUserIcon:imageData account:account success:^(NSString *objectKey) {
        ///第二步
        NSString * url = [TDSingleton instance].gatewayServerBusinessURL;
        NSString * sessionKey = [TDSingleton instance].sessionKey;
        NSMutableDictionary * param = [NSMutableDictionary dictionary];
        [param setObject:actionUserIconUpdateStep2 forKey:@"action"];
        [param setObject:sessionKey forKey:@"sessionKey"];
        [param setObject:objectKey forKey:@"icon"];
        
        progress(@"更新中...");
        //NSLog(@"\n\n---url = %@\n---sessionKey = %@\n---param = %@\n\n",url,sessionKey,param);
        [LGNetworking requestWithURL:url parameters:param success:^(id responseObject) {
            NSInteger status = [[responseObject objectForKey:@"result"] integerValue];
            if (status == 1) {
                NSString * iconURL = [responseObject objectForKey:@"iconURL"];
                success(iconURL);
            } else {
                NSString * msg = [responseObject objectForKey:@"msg"];
                failure(msg);
            }
        } failure:^(NSError *error) {
            NSLog(@"---%@", [error localizedDescription]);
            failure([error localizedDescription]);
        }];
    } failure:^(NSString *msg) {
        failure(msg);
    } progress:^(NSString *progressMsg) {
        progress(progressMsg);
    }];
}

///上传用户头像
+ (void)uploadUserIconWithAccount:(NSString *)account time:(NSTimeInterval)time sign:(NSString *)sign icon:(UIImage *)icon success:(LGDefineNetServerSuccessResult)success failure:(LGDefineNetServerFailureResultMsg)failure {
    NSString * url = [TDSingleton instance].uploadImageURL;
    NSString * sessionKey = [TDSingleton instance].sessionKey;
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    
    [param setObject:account forKey:@"userName"];
    [param setObject:@(time) forKey:@"time"];
    [param setObject:sign forKey:@"sign"];
    
    NSData *imageData = UIImagePNGRepresentation(icon);
    
    //NSUInteger length = [imageData length]/1024;
    //NSLog(@"image length:%ld", (unsigned long)length);
    NSString *mimeTypeString;
    NSString *fileName;
    
    if (imageData) {
        mimeTypeString = @"image/png";
        fileName = @"icon.png";
    } else {
        imageData = UIImageJPEGRepresentation(icon, 1);
        mimeTypeString = @"image/jpeg";
        fileName = @"icon.jpg";
    }
    
    //NSLog(@"mimeType:%@", mimeTypeString);
    
    NSLog(@"\n\n---url = %@\n---sessionKey = %@\n---param = %@\n\n",url,sessionKey,param);

    [LGNetworking requestWithURL:url parameters:param constructingBody:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileData:imageData
                                    name:@"icon"
                                fileName:fileName
                                mimeType:mimeTypeString];
    } success:^(id responseObject) {
        //NSLog(@"submitImage result:%@", responseObject);
        NSUInteger status = [[responseObject objectForKey:@"result"] integerValue];
        if (status == 1) {
            success(responseObject);
        } else {
            NSString * msg = [responseObject objectForKey:@"msg"];
            NSLog(@"%@", msg);
            failure(msg);
        }
    } failure:^(NSError *error) {
        NSLog(@"%@", [error localizedDescription]);
        failure([error localizedDescription]);
    }];
}

+ (void)modifyUserPasswordWithUseerId:(NSString *)userId oldPswd:(NSString *)oldPswd newPswd:(NSString *)newPswd success:(LGDefineNetServerSuccess)success failure:(LGDefineNetServerFailureResultMsg)failure {
    
    NSParameterAssert(userId.length);
    NSParameterAssert(oldPswd.length);
    NSParameterAssert(newPswd.length);
    
    NSString * url = [TDSingleton instance].gatewayServerBusinessURL;
    NSString * sessionKey = [TDSingleton instance].sessionKey;
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    
    [param setObject:actionModifyPassword forKey:@"action"];
    [param setObject:sessionKey forKey:@"sessionKey"];
    [param setObject:oldPswd forKey:@"oldPwd"];
    [param setObject:newPswd forKey:@"newPwd"];
    
    NSLog(@"\n\n---url = %@\n---sessionKey = %@\n---param = %@\n\n",url,sessionKey,param);
    
    [LGNetworking requestWithURL:url parameters:param success:^(id responseObject) {
        NSInteger status = [[responseObject objectForKey:@"result"] integerValue];
        if (status == 1) {
            success();
        } else {
            NSString * msg = [responseObject objectForKey:@"msg"];
            failure(msg);
        }
    } failure:^(NSError *error) {
        NSLog(@"%@", [error localizedDescription]);
        failure([error localizedDescription]);
    }];

}

+ (void)getUserInfo:(LGDaKaInfo *)userInfo success:(LGDefineNetServerSuccessResult)success failure:(LGDefineNetServerFailureResultMsg)failure {
    
    NSString * url = [TDSingleton instance].gatewayServerBusinessURL;
    NSString * sessionKey = [TDSingleton instance].sessionKey;
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    
    [param setObject:actionDaKaInfo forKey:@"action"];
    [param setObject:userInfo.account forKey:@"users"];
    [param setObject:[TDSingleton instance].sessionKey forKey:@"sessionKey"];
    
    NSLog(@"\n\n---url = %@\n---sessionKey = %@\n---param = %@\n\n",url,sessionKey,param);
    
    [LGNetworking requestWithURL:url parameters:param success:^(id responseObject) {
         NSLog(@"%@",responseObject);
         NSInteger status = [[responseObject objectForKey:@"result"] integerValue];
         if (status == 1) {
             NSArray * usersInfo = [responseObject objectForKey:@"usersInfo"];
             if (usersInfo && usersInfo.count > 0) {
                 
                 LGDaKaInfoHelp * userInfoHelp = [LGDaKaInfoHelp objectWithKeyValues:[usersInfo firstObject]];
                 success(userInfoHelp);
             } else {
                 
                 failure(@"没找到用户");
             }
         } else {
             NSString * msg = [responseObject objectForKey:@"msg"];
             NSLog(@"%@", msg);
             failure(msg);
         }
         
     } failure:^(NSError *error) {
         NSLog(@"%@", [error localizedDescription]);
         failure([error localizedDescription]);
     }];
}
@end

@implementation LGDefineNetServer (Wallet)

+ (void)reChargeWithMoney:(float)money tokenMoney:(float)tokenMoney success:(LGDefineNetServerSuccessResult)success failure:(LGDefineNetServerFailureResultMsg)failure {
    NSString * url = [TDSingleton instance].gatewayServerBusinessURL;
    NSString * sessionKey = [TDSingleton instance].sessionKey;
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    
    [param setObject:actionAddMoneyTest forKey:@"action"];
    [param setObject:sessionKey forKey:@"sessionKey"];
    [param setObject:@(money) forKey:@"money"];
    [param setObject:@(tokenMoney) forKey:@"tokenMoney"];
    
    NSLog(@"\n\n---url = %@\n---sessionKey = %@\n---param = %@\n\n",url,sessionKey,param);
    
    [LGNetworking requestWithURL:url parameters:param success:^(id responseObject) {
        //NSLog(@"\n\n---responseObject = %@\n\n", responseObject);
        success(responseObject);
    } failure:^(NSError *error) {
        NSLog(@"---%@", [error localizedDescription]);
        failure([error localizedDescription]);
    }];

}

+ (void)getWalletInfoSuccess:(LGDefineNetServerSuccessResult)success failure:(LGDefineNetServerFailureResultMsg)failure {
    
    NSString * url = [TDSingleton instance].gatewayServerBusinessURL;
    NSString * sessionKey = [TDSingleton instance].sessionKey;
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    
    [param setObject:actionMoneyGet forKey:@"action"];
    [param setObject:sessionKey forKey:@"sessionKey"];
    
    NSLog(@"\n\n---url = %@\n---sessionKey = %@\n---param = %@\n\n",url,sessionKey,param);
    
    [LGNetworking requestWithURL:url parameters:param success:^(id responseObject) {
        //NSLog(@"\n\n---responseObject = %@\n\n", responseObject);
        success(responseObject);
    } failure:^(NSError *error) {
        NSLog(@"---%@", [error localizedDescription]);
        failure([error localizedDescription]);
    }];
}

+ (void)getCharge:(LGCharge *)charge success:(LGDefineNetServerSuccessResult)success failure:(LGDefineNetServerFailureResultMsg)failure {
    
    NSString * url = [TDSingleton instance].gatewayServerBusinessURL;
    NSString * sessionKey = [TDSingleton instance].sessionKey;
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    
    [param setObject:actionChargeGet forKey:@"action"];
    [param setObject:sessionKey forKey:@"sessionKey"];
    
    [param setObject:@(charge.amount) forKey:@"amount"];
    [param setObject:charge.subject forKey:@"subject"];
    [param setObject:charge.body forKey:@"body"];
    [param setObject:charge.channel forKey:@"channel"];
    
    NSLog(@"\n\n---url = %@\n---sessionKey = %@\n---param = %@\n\n",url,sessionKey,param);
    
    [LGNetworking requestWithURL:url parameters:param success:^(id responseObject) {
        //NSLog(@"\n\n---responseObject = %@\n\n", responseObject);
        NSUInteger status = [[responseObject objectForKey:@"result"] integerValue];
        if (status) {
            NSDictionary *charge = responseObject[@"charge"];
            success(charge);
        } else {
            NSString * msg = [responseObject objectForKey:@"msg"];
            NSLog(@"%@", msg);
            failure(msg);
        }
    } failure:^(NSError *error) {
        NSLog(@"---%@", [error localizedDescription]);
        failure([error localizedDescription]);
    }];
}

+ (void)reChargeWithCharge:(LGCharge *)charge success:(LGDefineNetServerSuccessResult)success failure:(LGDefineNetServerFailureResultMsg)failure {
    
}

@end

@implementation LGDefineNetServer (CommonAddress)
+ (void)getCommonAddressSuccess:(LGDefineNetServerGetCommonAddressSuccessResult)success failure:(LGDefineNetServerFailureResultMsg)failure {
    
    NSString * url = [TDSingleton instance].gatewayServerBusinessURL;
    NSString * sessionKey = [TDSingleton instance].sessionKey;
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    
    [param setObject:actionAddressGet forKey:@"action"];
    [param setObject:sessionKey forKey:@"sessionKey"];

    
    NSLog(@"\nurl = %@\nsessionKey = %@\nparam = %@",url,sessionKey,param);
    
    [LGNetworking requestWithURL:url parameters:param success:^(id responseObject) {
        NSUInteger status = [[responseObject objectForKey:@"result"] integerValue];
        if (status == 1) {
            NSArray * addressInfo = [responseObject objectForKey:@"addressInfo"];
            NSArray <LGCommonAddress *> * addressArray = [LGCommonAddress objectArrayWithKeyValuesArray:addressInfo];
            success(addressArray);
        } else {
            NSString * msg = [responseObject objectForKey:@"msg"];
            failure(msg);
        }
    } failure:^(NSError *error) {
        NSLog(@"%@", [error localizedDescription]);
        failure([error localizedDescription]);
    }];
}

+ (void)addCommonAddress:(LGCommonAddress *)address success:(LGDefineNetServerSuccess)success failure:(LGDefineNetServerFailureResultMsg)failure {
    NSString * url = [TDSingleton instance].gatewayServerBusinessURL;
    NSString * sessionKey = [TDSingleton instance].sessionKey;
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    
    [param setObject:actionAddressAdd forKey:@"action"];
    [param setObject:sessionKey forKey:@"sessionKey"];
    [param setObject:address.name forKey:@"name"];
    [param setObject:address.address forKey:@"address"];
    [param setObject:address.position forKey:@"position"];
    
    NSLog(@"\nurl = %@\nsessionKey = %@\nparam = %@",url,sessionKey,param);
    
    [LGNetworking requestWithURL:url parameters:param success:^(id responseObject) {
        NSUInteger status = [[responseObject objectForKey:@"result"] integerValue];
        if (status == 1) {
            
            address.id = [[responseObject objectForKey:@"id"] doubleValue];
            success();
        } else {
            NSString * msg = [responseObject objectForKey:@"msg"];
            failure(msg);
        }
    } failure:^(NSError *error) {
        NSLog(@"%@", [error localizedDescription]);
        failure([error localizedDescription]);
    }];
}

+ (void)removeCommonAddress:(LGCommonAddress *)address success:(LGDefineNetServerSuccess)success failure:(LGDefineNetServerFailureResultMsg)failure {
    NSString * url = [TDSingleton instance].gatewayServerBusinessURL;
    NSString * sessionKey = [TDSingleton instance].sessionKey;
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    
    [param setObject:actionAddressDelete forKey:@"action"];
    [param setObject:sessionKey forKey:@"sessionKey"];
    [param setObject:@(address.id) forKey:@"addressId"];
    
    NSLog(@"\nurl = %@\nsessionKey = %@\nparam = %@",url,sessionKey,param);
    
    [LGNetworking requestWithURL:url parameters:param success:^(id responseObject) {
        NSUInteger status = [[responseObject objectForKey:@"result"] integerValue];
        if (status == 1) {
            success();
        } else {
            NSString * msg = [responseObject objectForKey:@"msg"];
            failure(msg);
        }
    } failure:^(NSError *error) {
        NSLog(@"%@", [error localizedDescription]);
        failure([error localizedDescription]);
    }];
}

+ (void)updateCommonAddress:(LGCommonAddress *)address success:(LGDefineNetServerSuccessResultMsg)success failure:(LGDefineNetServerFailureResultMsg)failure {
    NSString * url = [TDSingleton instance].gatewayServerBusinessURL;
    NSString * sessionKey = [TDSingleton instance].sessionKey;
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    
    [param setObject:actionAddressModify forKey:@"action"];
    [param setObject:sessionKey forKey:@"sessionKey"];
    
    [param setObject:@(address.id) forKey:@"addressId"];
    [param setObject:address.name forKey:@"name"];
    [param setObject:address.address forKey:@"address"];
    [param setObject:address.position forKey:@"position"];
    
    NSLog(@"\nurl = %@\nsessionKey = %@\nparam = %@",url,sessionKey,param);
    
    [LGNetworking requestWithURL:url parameters:param success:^(id responseObject) {
        NSUInteger status = [[responseObject objectForKey:@"result"] integerValue];
        if (status == 1) {
            success(@"更新成功");
        } else {
            NSString * msg = [responseObject objectForKey:@"msg"];
            failure(msg);
        }
    } failure:^(NSError *error) {
        NSLog(@"%@", [error localizedDescription]);
        failure([error localizedDescription]);
    }];
}

+ (void)updateDefaultAddress:(LGCommonAddress *)address success:(LGDefineNetServerSuccessResult)success failure:(LGDefineNetServerFailureResultMsg)failure {
    NSString * url = [TDSingleton instance].gatewayServerBusinessURL;
    NSString * sessionKey = [TDSingleton instance].sessionKey;
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    
    [param setObject:actionAddressModifyDefault forKey:@"action"];
    [param setObject:sessionKey forKey:@"sessionKey"];
    [param setObject:@(address.id) forKey:@"addressId"];
    
    NSLog(@"\nurl = %@\nsessionKey = %@\nparam = %@",url,sessionKey,param);
    
    [LGNetworking requestWithURL:url parameters:param success:^(id responseObject) {
        NSUInteger status = [[responseObject objectForKey:@"result"] integerValue];
        if (status == 1) {
            LGGamePlayerInfo * defaultGamePlayerInfo = [LGGamePlayerInfo objectWithKeyValues:[responseObject objectForKey:@""]];
            success(defaultGamePlayerInfo);
        } else {
            NSString * msg = [responseObject objectForKey:@"msg"];
            failure(msg);
        }
    } failure:^(NSError *error) {
        NSLog(@"%@", [error localizedDescription]);
        failure([error localizedDescription]);
    }];
}
@end


@implementation LGDefineNetServer (RoleBinding)

+ (void)getRoleBindingsSuccess:(LGDefineNetServerGetRoleBindingsSuccessResult)success failure:(LGDefineNetServerFailureResultMsg)failure {
    NSString * url = [TDSingleton instance].gatewayServerBusinessURL;
    NSString * sessionKey = [TDSingleton instance].sessionKey;
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    
    [param setObject:actionPlayersGet forKey:@"action"];
    [param setObject:sessionKey forKey:@"sessionKey"];
    
    //NSLog(@"\nurl = %@\nsessionKey = %@\nparam = %@",url,sessionKey,param);
    [LGNetworking requestWithURL:url parameters:param success:^(id responseObject) {
        //NSLog(@"%@", responseObject);
        NSInteger status = [[responseObject objectForKey:@"result"] integerValue];
        if (status == 1) {
            long long defaultPlayerId = [[responseObject objectForKey:@"defaultPlayerId"] longLongValue];
            
            NSArray * playerInfos = [responseObject objectForKey:@"playerInfo"];
            NSArray <LGGamePlayerInfo *> * gamePlayerInfos = [LGGamePlayerInfo objectArrayWithKeyValuesArray:playerInfos];
            
            success(gamePlayerInfos, defaultPlayerId);
        } else {
            NSString * msg = [responseObject objectForKey:@"msg"];
            NSLog(@"%@", msg);
            failure(msg);
        }
    } failure:^(NSError *error) {
        NSLog(@"%@", [error localizedDescription]);
        failure([error localizedDescription]);
    }];
}

+ (void)addRoleBinding:(LGGamePlayerInfo *)role success:(LGDefineNetServerAddRoleBindingSuccessResult)success failure:(LGDefineNetServerFailureResultMsg)failure {
    NSString * url = [TDSingleton instance].gatewayServerBusinessURL;
    NSString * sessionKey = [TDSingleton instance].sessionKey;
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    
    NSParameterAssert(role.playerName);
    NSParameterAssert(role.serverName);
    
    [param setObject:actionPlayerAdd forKey:@"action"];
    [param setObject:sessionKey forKey:@"sessionKey"];
    [param setObject:role.serverName forKey:@"serverName"];
    [param setObject:role.playerName forKey:@"playerName"];
    
    NSLog(@"\nurl = %@\nsessionKey = %@\nparam = %@",url,sessionKey,param);
    
    [LGNetworking requestWithURL:url parameters:param success:^(id responseObject) {
        NSUInteger status = [[responseObject objectForKey:@"result"] integerValue];
        if (status == 1) {
            LGGamePlayer * defaultGameAccount = [LGGamePlayer objectWithKeyValues:[responseObject objectForKey:@"defaultPlayer"]];
            LGGamePlayer * addGameAccount = [LGGamePlayer objectWithKeyValues:[responseObject objectForKey:@"nowPlayer"]];
            success(addGameAccount,defaultGameAccount);
        } else {
            NSString * msg = [responseObject objectForKey:@"msg"];
            failure(msg);
        }
    } failure:^(NSError *error) {
        NSLog(@"%@", [error localizedDescription]);
        failure([error localizedDescription]);
    }];
}

+ (void)removeRoleBinding:(LGGamePlayerInfo *)role success:(LGDefineNetServerSuccess)success failure:(LGDefineNetServerFailureResultMsg)failure {
    NSString * url = [TDSingleton instance].gatewayServerBusinessURL;
    NSString * sessionKey = [TDSingleton instance].sessionKey;
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    
    [param setObject:actionPlayerDelete forKey:@"action"];
    [param setObject:sessionKey forKey:@"sessionKey"];
    [param setObject:@(role.id) forKey:@"playerId"];
    
    NSLog(@"\nurl = %@\nsessionKey = %@\nparam = %@",url,sessionKey,param);
    
    [LGNetworking requestWithURL:url parameters:param success:^(id responseObject) {
        NSUInteger status = [[responseObject objectForKey:@"result"] integerValue];
        if (status == 1) {
            success();
        } else {
            NSString * msg = [responseObject objectForKey:@"msg"];
            failure(msg);
        }
    } failure:^(NSError *error) {
        failure([error localizedDescription]);
    }];
}

+ (void)updateDefaultRoleBinding:(LGGamePlayerInfo *)role success:(LGDefineNetServerUpdateDefaultRoleBindingSuccessResult)success failure:(LGDefineNetServerFailureResultMsg)failure {
    NSString * url = [TDSingleton instance].gatewayServerBusinessURL;
    NSString * sessionKey = [TDSingleton instance].sessionKey;
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    
    [param setObject:actionPlayerModifyDefault forKey:@"action"];
    [param setObject:sessionKey forKey:@"sessionKey"];
    
    [param setObject:@(role.id) forKey:@"playerId"];

    NSLog(@"\nurl = %@\nsessionKey = %@\nparam = %@",url,sessionKey,param);
    
    [LGNetworking requestWithURL:url parameters:param success:^(id responseObject) {
        NSUInteger state = [[responseObject objectForKey:@"result"] integerValue];
        if (state == 1) {
            LGGamePlayer * defaultGamePlay = [LGGamePlayer objectWithKeyValues:[responseObject objectForKey:@"defaultPlayer"]];
            success(defaultGamePlay);
        } else {
            NSString * msg = [responseObject objectForKey:@"msg"];
            failure(msg);
        }
    } failure:^(NSError *error) {
        NSLog(@"%@", [error localizedDescription]);
        failure([error localizedDescription]);
    }];
}
@end


@implementation LGDefineNetServer (Order)

+ (void)creatDistributeOrder:(LGOrderInfo *)order success:(LGDefineNetServerSuccessResult)success failure:(LGDefineNetServerFailureResultMsg)failure {
    NSString * url = [TDSingleton instance].gatewayServerBusinessURL;
    NSString * sessionKey = [TDSingleton instance].sessionKey;
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    
    [param setObject:actionOrderFastCall forKey:@"action"];
    [param setObject:sessionKey forKey:@"sessionKey"];
    
    NSParameterAssert(order.masters.length);
    NSParameterAssert(order.planStartTime);
    NSParameterAssert(order.planKeepTime);
    NSParameterAssert(order.selectGameAccount);
    
    [param setObject:order.masters forKey:@"masters"];
    [param setObject:@(order.money) forKey:@"money"];
    [param setObject:@(order.tokenMoney) forKey:@"tokenMoney"];
    [param setObject:@(order.planStartTime) forKey:@"planStartTime"];
    [param setObject:@(order.rewardMoney) forKey:@"reward"];
    [param setObject:@(order.planKeepTime) forKey:@"planKeepTime"];
    [param setObject:@(!order.offLine) forKey:@"online"];
    [param setObject:@(order.sceneId) forKey:@"sceneId"];
    
    [param setObject:order.info forKey:@"info"];
    
    if (order.targetGrade.length > 0) {
        [param setObject:order.targetGrade forKey:@"target"];
    }
    if (order.address.length > 0) {
        [param setObject:order.address forKey:@"address"];
    }
    
    [param setObject:@(order.selectGameAccount.id) forKey:@"playerId"];
    [param setObject:order.selectGameAccount.playerName forKey:@"playerName"];
    [param setObject:order.selectGameAccount.serverName forKey:@"serverName"];
    
    NSLog(@"\nurl = %@\nsessionKey = %@\nparam = %@",url,sessionKey,param);
    
    [LGNetworking requestWithURL:url parameters:param success:^(id responseObject) {
        NSLog(@"creatDistributeOrder responseObject：%@", responseObject);
        NSInteger result = [[responseObject objectForKey:@"result"] integerValue];
        if (result == 1) {
            success(responseObject);
        } else {
            NSString * msg = [responseObject objectForKey:@"msg"];
            NSLog(@"%@", msg);
            failure(msg);
        }
    } failure:^(NSError *error) {
        NSLog(@"%@", [error localizedDescription]);
        failure([error localizedDescription]);
    }];
}

+ (void)creatDirectivityOrder:(LGOrderInfo *)order success:(LGDefineNetServerSuccessResult)success failure:(LGDefineNetServerFailureResultMsg)failure {
    NSString * url = [TDSingleton instance].gatewayServerBusinessURL;
    NSString * sessionKey = [TDSingleton instance].sessionKey;
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    
    [param setObject:actionOrderCallMaster forKey:@"action"];
    [param setObject:sessionKey forKey:@"sessionKey"];
    
    NSParameterAssert(order.masters.length);
    NSParameterAssert(order.planStartTime);
    NSParameterAssert(order.planKeepTime);
    NSParameterAssert(order.selectGameAccount);
    
    [param setObject:order.masters forKey:@"master"];
    [param setObject:@(order.money) forKey:@"money"];
    [param setObject:@(order.tokenMoney) forKey:@"tokenMoney"];
    [param setObject:@(order.rewardMoney) forKey:@"reward"];
    [param setObject:@(order.planStartTime) forKey:@"planStartTime"];
    [param setObject:@(order.planKeepTime) forKey:@"planKeepTime"];
    [param setObject:@(!order.offLine) forKey:@"online"];
    if (order.address.length > 0) {
        [param setObject:order.address forKey:@"address"];
    }
    
    [param setObject:@(order.selectGameAccount.id) forKey:@"playerId"];
    [param setObject:order.selectGameAccount.playerName forKey:@"playerName"];
    [param setObject:order.selectGameAccount.serverName forKey:@"serverName"];
    
    [param setObject:@"" forKey:@"info"];
    
    NSLog(@"\nurl = %@\nsessionKey = %@\nparam = %@",url,sessionKey,param);
    
    [LGNetworking requestWithURL:url parameters:param success:^(id responseObject) {

        NSInteger result = [[responseObject objectForKey:@"result"] integerValue];
        if (result == 1) {
            success(responseObject);
        } else {
            NSString * msg = [responseObject objectForKey:@"msg"];
            NSLog(@"%@", msg);
            failure(msg);
        }
    } failure:^(NSError *error) {
        NSLog(@"%@", [error localizedDescription]);
        failure([error localizedDescription]);
    }];
}

+ (void)getTodayOrderWithPage:(NSUInteger)page Success:(LGDefineNetServerSuccessResult)success failure:(LGDefineNetServerFailureResultMsg)failure {
    NSString * url = [TDSingleton instance].gatewayServerBusinessURL;
    NSString * sessionKey = [TDSingleton instance].sessionKey;
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    
    [param setObject:actionOrderGetTodayOrders forKey:@"action"];
    [param setObject:sessionKey forKey:@"sessionKey"];
    [param setObject:@(page) forKey:@"page"];
    [param setObject:@(1000) forKey:@"pageNum"];
    
    NSLog(@"\nurl = %@\nsessionKey = %@\nparam = %@",url,sessionKey,param);
    
    [LGNetworking requestWithURL:url parameters:param success:^(id responseObject) {
        NSUInteger status = [[responseObject objectForKey:@"result"] integerValue];
        
        if (status == 1) {
            success(responseObject);
        } else {
            NSString * msg = [responseObject objectForKey:@"msg"];
            NSLog(@"%@", msg);
            failure(msg);
        }
        
    } failure:^(NSError *error) {
        NSLog(@"%@", [error localizedDescription]);
        failure([error localizedDescription]);
    }];
}

+ (void)cancelOrder:(LGOrderInfo *)order success:(LGDefineNetServerSuccessResult)success failure:(LGDefineNetServerFailureResultMsg)failure {
    NSString * url = [TDSingleton instance].gatewayServerBusinessURL;
    NSString * sessionKey = [TDSingleton instance].sessionKey;
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    
    [param setObject:actionOrderCancel forKey:@"action"];
    [param setObject:sessionKey forKey:@"sessionKey"];
    
    [param setObject:@(order.id) forKey:@"orderId"];
    [param setObject:@(order.timeOutFlag) forKey:@"timeout"];
    
    NSLog(@"\nurl = %@\nsessionKey = %@\nparam = %@",url,sessionKey,param);
    
    [LGNetworking requestWithURL:url parameters:param success:^(id responseObject) {
        NSUInteger status = [[responseObject objectForKey:@"result"] integerValue];
        
        if (status == 1) {
            success(responseObject);
        } else {
            NSString * msg = [responseObject objectForKey:@"msg"];
            NSLog(@"%@", msg);
            failure(msg);
        }
    } failure:^(NSError *error) {
        NSLog(@"%@", [error localizedDescription]);
        failure([error localizedDescription]);
    }];
}

+ (void)blockOrder:(LGOrderInfo *)order success:(LGDefineNetServerSuccessResult)success failure:(LGDefineNetServerFailureResultMsg)failure {
    NSString * url = [TDSingleton instance].gatewayServerBusinessURL;
    NSString * sessionKey = [TDSingleton instance].sessionKey;
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    
    [param setObject:actionOrderBlock forKey:@"action"];
    [param setObject:sessionKey forKey:@"sessionKey"];
    [param setObject:@(order.id) forKey:@"orderId"];
    
    NSLog(@"\nurl = %@\nsessionKey = %@\nparam = %@",url,sessionKey,param);
    
    [LGNetworking requestWithURL:url parameters:param success:^(id responseObject) {
        NSUInteger status = [[responseObject objectForKey:@"result"] integerValue];
        
        if (status == 1) {
            success(responseObject);
        } else {
            NSString * msg = [responseObject objectForKey:@"msg"];
            NSLog(@"%@", msg);
            failure(msg);
        }
    } failure:^(NSError *error) {
        NSLog(@"%@", [error localizedDescription]);
        failure([error localizedDescription]);
    }];
}
+ (void)startOrder:(LGOrderInfo *)order success:(LGDefineNetServerSuccessResult)success failure:(LGDefineNetServerFailureResultMsg)failure {
    NSString * url = [TDSingleton instance].gatewayServerBusinessURL;
    NSString * sessionKey = [TDSingleton instance].sessionKey;
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    
    [param setObject:actionOrderStart forKey:@"action"];
    [param setObject:sessionKey forKey:@"sessionKey"];
    [param setObject:@(order.id) forKey:@"orderId"];
    
    NSLog(@"\nurl = %@\nsessionKey = %@\nparam = %@",url,sessionKey,param);
    
    [LGNetworking requestWithURL:url parameters:param success:^(id responseObject) {
        NSUInteger status = [[responseObject objectForKey:@"result"] integerValue];
        if (status == 1) {
            success(responseObject);
        } else {
            NSString * msg = [responseObject objectForKey:@"msg"];
            NSLog(@"%@", msg);
            failure(msg);
        }
    } failure:^(NSError *error) {
        NSLog(@"%@", [error localizedDescription]);
        failure([error localizedDescription]);
    }];
}

+ (void)finishOrder:(LGOrderInfo *)order success:(LGDefineNetServerSuccessResult)success failure:(LGDefineNetServerFailureResultMsg)failure {
    NSString * url = [TDSingleton instance].gatewayServerBusinessURL;
    NSString * sessionKey = [TDSingleton instance].sessionKey;
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    
    [param setObject:actionOrderEnd forKey:@"action"];
    [param setObject:sessionKey forKey:@"sessionKey"];
    [param setObject:@(order.id) forKey:@"orderId"];
    
    NSLog(@"\nurl = %@\nsessionKey = %@\nparam = %@",url,sessionKey,param);
    
    [LGNetworking requestWithURL:url parameters:param success:^(id responseObject) {
        NSUInteger status = [[responseObject objectForKey:@"result"] integerValue];
        
        if (status == 1) {
            success(responseObject);
        } else {
            NSString * msg = [responseObject objectForKey:@"msg"];
            NSLog(@"%@", msg);
            failure(msg);
        }
    } failure:^(NSError *error) {
        NSLog(@"%@", [error localizedDescription]);
        failure([error localizedDescription]);
    }];
}
+ (void)acceptOrder:(LGOrderInfo *)order success:(LGDefineNetServerSuccessResult)success failure:(LGDefineNetServerFailureResultMsg)failure {
    NSString * url = [TDSingleton instance].gatewayServerBusinessURL;
    NSString * sessionKey = [TDSingleton instance].sessionKey;
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    
    [param setObject:actionOrderAccept forKey:@"action"];
    [param setObject:sessionKey forKey:@"sessionKey"];
    
    NSLog(@"\nurl = %@\nsessionKey = %@\nparam = %@",url,sessionKey,param);
    
    [LGNetworking requestWithURL:url parameters:param success:^(id responseObject) {
        NSUInteger status = [[responseObject objectForKey:@"result"] integerValue];
        
        if (status == 1) {
            success(responseObject);
        } else {
            NSString * msg = [responseObject objectForKey:@"msg"];
            NSLog(@"%@", msg);
            failure(msg);
        }
    } failure:^(NSError *error) {
        NSLog(@"%@", [error localizedDescription]);
        failure([error localizedDescription]);
    }];
}

+ (void)appraiseOrder:(LGOrderInfo *)order star:(int)star success:(LGDefineNetServerSuccessResult)success failure:(LGDefineNetServerFailureResultMsg)failure {
    NSString * url = [TDSingleton instance].gatewayServerBusinessURL;
    NSString * sessionKey = [TDSingleton instance].sessionKey;
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    
    [param setObject:actionOrderRate forKey:@"action"];
    [param setObject:sessionKey forKey:@"sessionKey"];
    [param setObject:@(order.id) forKey:@"orderId"];
    [param setObject:@(star) forKey:@"star"];
    
    NSLog(@"\nurl = %@\nsessionKey = %@\nparam = %@",url,sessionKey,param);
    
    [LGNetworking requestWithURL:url parameters:param success:^(id responseObject) {
        NSUInteger status = [[responseObject objectForKey:@"result"] integerValue];
        
        if (status == 1) {
            success(responseObject);
        } else {
            NSString * msg = [responseObject objectForKey:@"msg"];
            NSLog(@"%@", msg);
            failure(msg);
        }
    } failure:^(NSError *error) {
        NSLog(@"%@", [error localizedDescription]);
        failure([error localizedDescription]);
    }];
}
@end

@implementation LGDefineNetServer (custom)

+ (void)method:(id)obj success:(LGDefineNetServerSuccess)success failure:(LGDefineNetServerFailureResultMsg)failure {
    NSString * url = [TDSingleton instance].gatewayServerBusinessURL;
    NSString * sessionKey = [TDSingleton instance].sessionKey;
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    
    [param setObject:@"" forKey:@"action"];
    [param setObject:sessionKey forKey:@"sessionKey"];
    
    NSLog(@"\nurl = %@\nsessionKey = %@\nparam = %@",url,sessionKey,param);
    
    [LGNetworking requestWithURL:url parameters:param success:^(id responseObject) {
        success();
    } failure:^(NSError *error) {
        failure([error localizedDescription]);
    }];
}

@end

@implementation LGDefineNetServer (Poi)
/*
+ (void)creatOnePoiData:(LGPoiUserInfo *)data success:(LGDefineNetServerSuccessResult)success failure:(LGDefineNetServerFailureResult)failure {
    NSString * url = [NSString stringWithFormat:@"%@%@", poiBaseUrl, actionPoiCreat];

    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    
    [param setObject:poiTableId forKey:@"geotable_id"];
    [param setObject:poiAk forKey:@"ak"];
    
    [param setObject:@(data.location.latitude) forKey:@"latitude"];
    [param setObject:@(data.location.longitude) forKey:@"longitude"];
    [param setObject:@(data.coord_type) forKey:@"coord_type"];
    
    [param setObject:data.user_id forKey:@"user_id"];
    [param setObject:@(data.online_state) forKey:@"online_state"];
    [param setObject:@(data.role_type) forKey:@"role_type"];
    [param setObject:@(data.sex) forKey:@"sex"];
    [param setObject:@(data.attack) forKey:@"attack"];
    [param setObject:data.grade forKey:@"grade"];
    [param setObject:data.serverName forKey:@"serverName"];
    
    NSLog(@"\nurl = %@\nparam = %@",url,param);
    
    [LGNetworking requestWithURL:url parameters:param success:^(id responseObject) {
        NSInteger status = [[responseObject objectForKey:@"status"] integerValue];
        if (status == 0) {
            NSUInteger mapUserId = [[responseObject objectForKey:@"id"] integerValue];
            NSString * str = [NSString stringWithFormat:@"%ld", (unsigned long)mapUserId];
            [LGDefineNetServer updateUserMapId:str success:^{
                success(nil);
            } failure:^{
                failure(nil);
            }];
        } else {
            NSLog(@"creatOnePoiData error :%@", [responseObject objectForKey:@"message"]);
            failure(nil);
        }
    } failure:^(NSError *error) {
        failure(error);
    }];
}
*/
+ (void)updateOnePoiData:(LGPoiUserInfo *)data success:(LGDefineNetServerSuccessResult)success failure:(LGDefineNetServerFailureResultMsg)failure {
    NSString * url = [NSString stringWithFormat:@"%@%@", poiBaseUrl, actionPoiUpdate];
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    [param setObject:poiTableId forKey:@"geotable_id"];
    [param setObject:poiAk forKey:@"ak"];
    //[param setObject:data.user_id forKey:@"user_id"];
    
    [param setObject:@(data.location.latitude) forKey:@"latitude"];
    [param setObject:@(data.location.longitude) forKey:@"longitude"];
    [param setObject:@(data.coord_type) forKey:@"coord_type"];
    
    [param setObject:@(data.online_state) forKey:@"online_state"];
    
    NSLog(@"\nurl = %@\nparam = %@",url,param);
    
    [LGNetworking requestWithURL:url parameters:param success:^(id responseObject) {
        //        success();
    } failure:^(NSError *error) {
        //        failure();
    }];
}

+ (void)deleteOnePoiData:(LGPoiUserInfo *)data success:(LGDefineNetServerSuccessResult)success failure:(LGDefineNetServerFailureResultMsg)failure {
    NSString * url = [NSString stringWithFormat:@"%@%@", poiBaseUrl, actionPoiDelete];
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    
    [param setObject:@"" forKey:@"id"];//UInt64 当不存在唯一索引字段时必须，存在唯一索引字段可选
    [param setObject:poiTableId forKey:@"geotable_id"];
    [param setObject:poiAk forKey:@"ak"];
    
    
    NSLog(@"\nurl = %@\nparam = %@",url,param);
    
    [LGNetworking requestWithURL:url parameters:param success:^(id responseObject) {
        //        success();
    } failure:^(NSError *error) {
        //        failure();
    }];
}

+ (void)getOnePoiDetail:(NSInteger)poiDataId success:(LGDefineNetServerSuccessResult)success failure:(LGDefineNetServerFailureResultMsg)failure {
    NSString * url = [NSString stringWithFormat:@"%@%@", poiBaseUrl, actionPoiUpdate];
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    
    [param setObject:@(poiDataId) forKey:@"id"];//UInt64 当不存在唯一索引字段时必须，存在唯一索引字段可选
    [param setObject:poiTableId forKey:@"geotable_id"];
    [param setObject:poiAk forKey:@"ak"];
    
    
    NSLog(@"\nurl = %@\nparam = %@",url,param);
    
    [LGNetworking requestWithURL:url parameters:param success:^(id responseObject) {
        //        success();
    } failure:^(NSError *error) {
        //        failure();
    }];
}

+ (void)getPoiDataList:(id)data success:(LGDefineNetServerSuccessResult)success failure:(LGDefineNetServerFailureResultMsg)failure {
    NSString * url = [NSString stringWithFormat:@"%@%@", poiBaseUrl, actionPoiUpdate];
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    
    [param setObject:@"" forKey:@"id"];//UInt64 当不存在唯一索引字段时必须，存在唯一索引字段可选
    [param setObject:@"" forKey:@"title"];//
    [param setObject:@"" forKey:@"address"];//String(256)
    [param setObject:@"" forKey:@"tags"];//Double
    [param setObject:@"" forKey:@"latitude"];//Double
    [param setObject:@"" forKey:@"longitude"];//UInt32
    [param setObject:@"" forKey:@"coord_type"];//String(50) 1．GPS经纬度坐标 2．测局加密经纬度坐标 3．度加密经纬度坐标 4．度加密墨卡托坐标 必选
    [param setObject:@"" forKey:@"geotable_id"];//String(50) 必选，加密后的id
    
    [param setObject:poiTableId forKey:@"geotable_id"];
    [param setObject:poiAk forKey:@"ak"];
    
    
    NSLog(@"\nurl = %@\nparam = %@",url,param);
    
    [LGNetworking requestWithURL:url parameters:param success:^(id responseObject) {
        //        success();
    } failure:^(NSError *error) {
        //        failure();
    }];
}

+ (void)updateLocation:(LGPoiUserInfo *)userInfo success:(void (^)(void))success failure:(void (^)(void))failure {
    
    NSString * url = [NSString stringWithFormat:@"%@%@", poiBaseUrl, actionPoiUpdate];
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    [param setObject:poiTableId forKey:@"geotable_id"];
    [param setObject:poiAk forKey:@"ak"];
    [param setObject:userInfo.user_id forKey:@"user_id"];
    
    [param setObject:@(userInfo.location.latitude) forKey:@"latitude"];
    [param setObject:@(userInfo.location.longitude) forKey:@"longitude"];
    [param setObject:@(3) forKey:@"coord_type"];
    
//    NSLog(@"\nurl = %@\nparam = %@",url,param);
    
    [LGNetworking requestWithURL:url parameters:param success:^(id responseObject) {
        NSInteger status = [[responseObject objectForKey:@"status"] integerValue];
        if (status == 0) {
            success();
        } else {
            failure();
        }
    } failure:^(NSError *error) {
        NSLog(@"%@", [error localizedDescription]);
        failure();
    }];
}


+ (void)setOnlineState:(BOOL)online account:(NSString *)account success:(void (^)(void))success failure:(void (^)(void))failure {
    
    NSParameterAssert(account.length);
    
    NSString * url = [NSString stringWithFormat:@"%@%@", poiBaseUrl, actionPoiUpdate];
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    [param setObject:poiTableId forKey:@"geotable_id"];
    [param setObject:poiAk forKey:@"ak"];
    [param setObject:account forKey:@"user_id"];
    
    [param setObject:@(online) forKey:@"online_state"];
    
    NSLog(@"\nurl = %@\nparam = %@",url,param);
    
    [LGNetworking requestWithURL:url parameters:param success:^(id responseObject) {
        NSInteger status = [[responseObject objectForKey:@"status"] integerValue];
        if (status == 0) {
            success();
        } else {
            failure();
        }
    } failure:^(NSError *error) {
        NSLog(@"%@", [error localizedDescription]);
        failure();
    }];
}

+ (void)setReceiveOrderSwitch:(BOOL)open account:(NSString *)account success:(void (^)(void))success failure:(LGDefineNetServerFailureResultMsg)failure {
    
    NSString * url = [NSString stringWithFormat:@"%@%@", poiBaseUrl, actionPoiUpdate];
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    [param setObject:poiTableId forKey:@"geotable_id"];
    [param setObject:poiAk forKey:@"ak"];
    [param setObject:account forKey:@"user_id"];
    
    [param setObject:@(!open) forKey:@"state"];
    
    NSLog(@"\nurl = %@\nparam = %@",url,param);
    
    [LGNetworking requestWithURL:url parameters:param success:^(id responseObject) {
        NSInteger status = [[responseObject objectForKey:@"status"] integerValue];
        if (status == 0) {
            success();
        } else {
            NSString * msg= [responseObject objectForKey:@"message"];
            failure(msg);
        }
    } failure:^(NSError *error) {
        NSLog(@"%@", [error localizedDescription]);
        failure([error localizedDescription]);
    }];
}

@end

@implementation LGDefineNetServer (Game)

+ (void)getGameLOLServerListSuccess:(LGDefineNetServerSuccessResult)success failure:(LGDefineNetServerFailureResultMsg)failure {
    
    NSString * url = [[TDSingleton instance].gameCenterURI stringByAppendingPathComponent:actionGameLOLServerList];
    NSString * sessionKey = [TDSingleton instance].sessionKey;
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    
    [param setObject:sessionKey forKey:@"sessionKey"];
    
    //NSLog(@"\n\n---url = %@\n---sessionKey = %@\n---param = %@\n\n",url,sessionKey,param);
    [LGNetworking requestWithURL:url parameters:param cachePolicy:NSURLRequestReturnCacheDataElseLoad success:^(id responseObject) {
        NSUInteger status = [[responseObject objectForKey:@"result"] integerValue];
        if (status == 1) {
            NSArray * array = [responseObject objectForKey:@"servers"];
            success(array);
        } else {
            NSString * msg = [responseObject objectForKey:@"msg"];
            NSLog(@"%@", msg);
            failure(msg);
        }
    } failure:^(NSError *error) {
        NSLog(@"---%@", [error localizedDescription]);
        failure([error localizedDescription]);
    }];
}

+ (void)getGameLOLGamePlayerDetailWithGamePlayer:(LGGamePlayerInfo *)gamePlayerInfo success:(LGDefineNetServerSuccessResult)success failure:(LGDefineNetServerFailureResultMsg)failure {
    
    NSString * url = [[TDSingleton instance].gameCenterURI stringByAppendingPathComponent:actionGameLOLGamePlayerDetail];
    NSString * sessionKey = [TDSingleton instance].sessionKey;
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    
    [param setObject:sessionKey forKey:@"sessionKey"];
    [param setObject:gamePlayerInfo.serverName forKey:@"serverName"];
    [param setObject:gamePlayerInfo.playerName forKey:@"playerName"];
    
    //NSLog(@"\n\n---url = %@\n---sessionKey = %@\n---param = %@\n\n",url,sessionKey,param);
    [LGNetworking requestWithURL:url parameters:param cachePolicy:NSURLRequestReturnCacheDataElseLoad success:^(id responseObject) {
        //NSLog(@"\n\n---responseObject = %@\n\n", responseObject);
        NSUInteger status = [[responseObject objectForKey:@"result"] integerValue];
        if (status == 1) {
            LGGamePlayer * gamePlayerDetail = [LGGamePlayer objectWithKeyValues:responseObject];
            success(gamePlayerDetail);
        } else {
            NSString * msg = [responseObject objectForKey:@"msg"];
            NSLog(@"%@", msg);
            failure(msg);
        }
    } failure:^(NSError *error) {
        NSLog(@"---%@", [error localizedDescription]);
        failure([error localizedDescription]);
    }];
    
}

+ (void)getGameLOLGamePlayerMatchListWithGamePlayer:(LGGamePlayerInfo *)gamePlayerInfo gameHero:(LGGameHeroInfo *)gameHeroInfo page:(NSUInteger)page success:(LGDefineNetServerSuccessResult)success failure:(LGDefineNetServerFailureResultMsg)failure {
    
    NSString * url = [[TDSingleton instance].gameCenterURI stringByAppendingPathComponent:actionGameLOLGamePlayerMatchList];
    NSString * sessionKey = [TDSingleton instance].sessionKey;
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    
    [param setObject:actionGameLOLGamePlayerMatchList forKey:@"action"];
    [param setObject:sessionKey forKey:@"sessionKey"];
    [param setObject:gamePlayerInfo.serverName forKey:@"serverName"];
    [param setObject:gamePlayerInfo.playerName forKey:@"playerName"];
    if (gameHeroInfo) {
        [param setObject:gameHeroInfo.name_en forKey:@"hero"];
    }
    
    [param setObject:@(page) forKey:@"page"];
    
    //NSLog(@"\n\n---url = %@\n---sessionKey = %@\n---param = %@\n\n",url,sessionKey,param);
    
    [LGNetworking requestWithURL:url parameters:param cachePolicy:NSURLRequestReturnCacheDataElseLoad success:^(id responseObject) {
        //NSLog(@"\n\n   responseObject = %@\n\n", responseObject);
        NSUInteger status = [[responseObject objectForKey:@"result"] integerValue];
        if (status == 1) {
            success(responseObject);
        } else {
            NSString * msg = [responseObject objectForKey:@"msg"];
            NSLog(@"%@", msg);
            failure(msg);
        }
    } failure:^(NSError *error) {
        NSLog(@"---%@", [error localizedDescription]);
        failure([error localizedDescription]);
    }];
}

+ (void)getGameLOLGamePlayerMatchDetailWithGameMatch:(LGGameMatchInfo *)gameMatchInfo success:(LGDefineNetServerSuccessResult)success failure:(LGDefineNetServerFailureResultMsg)failure {
    
    NSString * url = [[TDSingleton instance].gameCenterURI stringByAppendingPathComponent:actionGameLOLGamePlayerMatchDetail];
    NSString * sessionKey = [TDSingleton instance].sessionKey;
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    
    [param setObject:actionGameLOLGamePlayerMatchDetail forKey:@"action"];
    [param setObject:sessionKey forKey:@"sessionKey"];
    
    [param setObject:gameMatchInfo.serverName forKey:@"serverName"];
    [param setObject:gameMatchInfo.playerName forKey:@"playerName"];
    [param setObject:@(gameMatchInfo.id) forKey:@"matchId"];
    
    //NSLog(@"\n\n---url = %@\n---sessionKey = %@\n---param = %@\n\n",url,sessionKey,param);
    [LGNetworking requestWithURL:url parameters:param cachePolicy:NSURLRequestReturnCacheDataElseLoad success:^(id responseObject) {
        //NSLog(@"\n\n---responseObject = %@\n\n", responseObject);
        NSUInteger status = [[responseObject objectForKey:@"result"] integerValue];
        if (status == 1) {
            
            NSString * winTeamStr = [responseObject objectForKey:@"winTeam"];
            NSString * topStr = [responseObject objectForKey:@"top"];
            NSString * loseTeamStr = [responseObject objectForKey:@"loseTeam"];
            
            NSError * error = nil;
            
            id winTeamArray = [NSJSONSerialization JSONObjectWithData:[winTeamStr dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&error];
//            NSLog(@"%@", winTeamDic);
            
            id topArray = [NSJSONSerialization JSONObjectWithData:[topStr dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&error];
//            NSLog(@"%@", topDic);
            
            id loseTeamArray = [NSJSONSerialization JSONObjectWithData:[loseTeamStr dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&error];
//            NSLog(@"%@", loseTeamDic);
            
            NSArray * winTeamObjArray = [LGGameTeamMemberInfo objectArrayWithKeyValuesArray:winTeamArray];
            NSArray * loseTeamObjArray = [LGGameTeamMemberInfo objectArrayWithKeyValuesArray:loseTeamArray];
            NSArray * topObjArray = [LGGameTeamDataDetail objectArrayWithKeyValuesArray:topArray];
            
            LGGameMatch * gameMatch = [[LGGameMatch alloc] initWithGameMatchInfo:gameMatchInfo];
            
            gameMatch.winTeam = winTeamObjArray;
            gameMatch.loseTeam = loseTeamObjArray;
            gameMatch.top = topObjArray;
            
            success(gameMatch);
        } else {
            NSString * msg = [responseObject objectForKey:@"msg"];
            NSLog(@"%@", msg);
            failure(msg);
        }
    } failure:^(NSError *error) {
        NSLog(@"---%@", [error localizedDescription]);
        failure([error localizedDescription]);
    }];
}

@end


@implementation LGDefineNetServer (Certification)

+ (void)certificateDaka:(LGDakaCertificate *)dakaCertificate success:(LGDefineNetServerCertificateDakaSuccess)success failure:(LGDefineNetServerFailureResultMsg)failure {
    
    NSString * url = [TDSingleton instance].gatewayServerBusinessURL;
    NSString * sessionKey = [TDSingleton instance].sessionKey;
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    
    [param setObject:actionCertificateDaka forKey:@"action"];
    [param setObject:sessionKey forKey:@"sessionKey"];
    
    [param setObject:@(0) forKey:@"gameId"];
    [param setObject:dakaCertificate.city forKey:@"city"];
    [param setObject:@(dakaCertificate.priceOnline) forKey:@"priceOnline"];
    [param setObject:@(dakaCertificate.priceOffline) forKey:@"priceOffline"];
    [param setObject:@(dakaCertificate.type) forKey:@"type"];
    
    NSLog(@"\n\n---url = %@\n---sessionKey = %@\n---param = %@\n\n",url,sessionKey,param);
    [LGNetworking requestWithURL:url parameters:param success:^(id responseObject) {
        //NSLog(@"\n\n   responseObject = %@\n\n", responseObject);
        NSUInteger status = [[responseObject objectForKey:@"result"] integerValue];
        if (status == 1) {
            NSInteger certifyState = [[responseObject objectForKey:@"certifyState"] integerValue];
            success(certifyState);
        } else {
            NSString * msg = [responseObject objectForKey:@"msg"];
            NSLog(@"%@", msg);
            failure(msg);
        }
    } failure:^(NSError *error) {
        NSLog(@"---%@", [error localizedDescription]);
        failure([error localizedDescription]);
    }];
}

@end
