//
//  LGDefineNetServer.h
//  AfterSchool
//
//  Created by lg on 15/10/28.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import "SVProgressHUD.h"

#import "LGDaKaInfo.h"
#import "LGDaKaInfoHelp.h"
#import "LGDakaCertificate.h"

#import "LGOrderInfo.h"
#import "LGCommonAddress.h"

#import "LGPoiUserInfo.h"

#import "LGCharge.h"

#import "LGGamePlayer.h"
#import "LGGameHeroInfo.h"
#import "LGGameTypeInfo.h"
#import "LGGameMatch.h"

@class LGBaseCloudSearchInfo,LGCloudSearchInfo;
@class LGCloudBoundSearchInfo,LGCloudDetailSearchInfo,LGCloudLocalSearchInfo,LGCloudNearbySearchInfo;

typedef void(^LGDefineNetServerSuccess)(void);

typedef void(^LGDefineNetServerSuccessResult)(id result);

typedef void(^LGDefineNetServerSuccessResultMsg)(NSString * msg);
typedef void(^LGDefineNetServerFailureResultMsg)(NSString * msg);

@interface LGDefineNetServer : NSObject

@end


///客户端报告位置坐标

typedef void(^LGDefineNetServerGetUsersInfoSuccessResult)(NSArray <LGDaKaInfoHelp *> * result);

///获取大咖摘要数据
@interface LGDefineNetServer (Master)
///获取大咖详细数据
+ (void)getUserDetailWithAccount:(LGDaKaInfo *)userInfo success:(LGDefineNetServerSuccessResult)success failure:(LGDefineNetServerFailureResultMsg)failure;

///获取一组用户摘要信息
+ (void)getUsersInfoWithAccounts:(NSArray<NSString *> *)accounts success:(LGDefineNetServerGetUsersInfoSuccessResult)success failure:(LGDefineNetServerFailureResultMsg)failure;

///客户端报告位置坐标 position:"成都,107,108"
+ (void)reportUsersInfoPosition:(NSString *)position success:(LGDefineNetServerSuccessResult)success failure:(LGDefineNetServerFailureResultMsg)failure;

///修改大咖是否接受订单通知
+ (void)changeUserInfoReceiveOrderState:(BOOL)unreceiveOrder success:(LGDefineNetServerSuccessResultMsg)success failure:(LGDefineNetServerFailureResultMsg)failure;

@end


typedef void(^LGDefineNetServerUpdateUserIconSuccessResult)(NSString * iconUrl);
typedef void(^LGDefineNetServerUpdateUserIconProgress)(NSString * progressMsg);

typedef void(^LGDefineNetServerUpLoadUserIconSuccess)(NSString * objectKey);
typedef void(^LGDefineNetServerUpLoadUserIconProgress)(NSString * progressMsg);
typedef void(^TDUploadUserIconSuccess)(void);

@interface LGDefineNetServer (UserInfo)
///绑定百度LBS存储(服务器做了)
//+ (void)updateUserMapId:(NSString *)userMapId success:(LGDefineNetServerSuccess)success failure:(LGDefineNetServerFailure)failure;

///上传头像
+ (void)uploadUserIcon:(NSData *)imageData account:(NSString *)account success:(LGDefineNetServerUpLoadUserIconSuccess)success failure:(LGDefineNetServerFailureResultMsg)failure progress:(LGDefineNetServerUpLoadUserIconProgress)progress;

///修改用户头像
+ (void)updateUserIcon:(UIImage *)image success:(LGDefineNetServerUpdateUserIconSuccessResult)success failure:(LGDefineNetServerFailureResultMsg)failure progress:(LGDefineNetServerUpdateUserIconProgress)progress;

///修改用户登录密码
+ (void)modifyUserPasswordWithUseerId:(NSString *)userId oldPswd:(NSString *)oldPswd newPswd:(NSString *)newPswd success:(LGDefineNetServerSuccess)success failure:(LGDefineNetServerFailureResultMsg)failure;

///获取用户摘要信息
+ (void)getUserInfo:(LGDaKaInfo *)userInfo success:(LGDefineNetServerSuccessResult)success failure:(LGDefineNetServerFailureResultMsg)failure;

///上传图片Teson Draw
+ (void)uploadImage:(UIImage *)image
          objectKey:(NSString *)objectKey
            account:(NSString *)account
            success:(TDUploadUserIconSuccess)success
            failure:(LGDefineNetServerFailureResultMsg)failure
           progress:(LGDefineNetServerUpLoadUserIconProgress)progress;

//+ (void)uploadImage:(UIImage *)image folderName:(NSString *)folderName account:(NSString *)account success:(LGDefineNetServerUpLoadUserIconSuccess)success failure:(LGDefineNetServerFailureResultMsg)failure progress:(LGDefineNetServerUpLoadUserIconProgress)progress;

@end

@interface LGDefineNetServer (Wallet)

///模拟充值功能
+ (void)reChargeWithMoney:(float)money tokenMoney:(float)tokenMoney success:(LGDefineNetServerSuccessResult)success failure:(LGDefineNetServerFailureResultMsg)failure;
///获取钱包数据
+ (void)getWalletInfoSuccess:(LGDefineNetServerSuccessResult)success failure:(LGDefineNetServerFailureResultMsg)failure;

///充值功能
+ (void)getCharge:(LGCharge *)charge success:(LGDefineNetServerSuccessResult)success failure:(LGDefineNetServerFailureResultMsg)failure;

@end



typedef void(^LGDefineNetServerGetRoleBindingsSuccessResult)(NSArray<LGGamePlayerInfo *> * result, long long defaultAccountId);
typedef void(^LGDefineNetServerAddRoleBindingSuccessResult)(LGGamePlayerInfo * gamePlayer ,LGGamePlayer * defaultGamePlayer);
typedef void(^LGDefineNetServerUpdateDefaultRoleBindingSuccessResult)(LGGamePlayer * defaultGamePlayer);

@interface LGDefineNetServer (RoleBinding)

///获取绑定游戏角色
+ (void)getRoleBindingsSuccess:(LGDefineNetServerGetRoleBindingsSuccessResult)success failure:(LGDefineNetServerFailureResultMsg)failure;

///添加绑定游戏角色
+ (void)addRoleBinding:(LGGamePlayerInfo *)role success:(LGDefineNetServerAddRoleBindingSuccessResult)success failure:(LGDefineNetServerFailureResultMsg)failure;

///删除绑定游戏角色
+ (void)removeRoleBinding:(LGGamePlayerInfo *)role success:(LGDefineNetServerSuccess)success failure:(LGDefineNetServerFailureResultMsg)failure;

///修改默认绑定游戏角色
+ (void)updateDefaultRoleBinding:(LGGamePlayerInfo *)role success:(LGDefineNetServerUpdateDefaultRoleBindingSuccessResult)success failure:(LGDefineNetServerFailureResultMsg)failure;

@end


typedef void(^LGDefineNetServerGetCommonAddressSuccessResult)(NSArray <LGCommonAddress *> * addresses);
@interface LGDefineNetServer (CommonAddress)
///获取常用地址
+ (void)getCommonAddressSuccess:(LGDefineNetServerGetCommonAddressSuccessResult)success failure:(LGDefineNetServerFailureResultMsg)failure;

///添加常用地址
+ (void)addCommonAddress:(LGCommonAddress *)address success:(LGDefineNetServerSuccess)success failure:(LGDefineNetServerFailureResultMsg)failure;

///删除常用地址
+ (void)removeCommonAddress:(LGCommonAddress *)address success:(LGDefineNetServerSuccess)success failure:(LGDefineNetServerFailureResultMsg)failure;

///修改常用地址
+ (void)updateCommonAddress:(LGCommonAddress *)address success:(LGDefineNetServerSuccessResultMsg)success failure:(LGDefineNetServerFailureResultMsg)failure;

///修改默认地址
+ (void)updateDefaultAddress:(LGCommonAddress *)address success:(LGDefineNetServerSuccessResult)success failure:(LGDefineNetServerFailureResultMsg)failure;
@end


@interface LGDefineNetServer (Order)

///创建订单（一键约）
+ (void)creatDistributeOrder:(LGOrderInfo *)order success:(LGDefineNetServerSuccessResult)success failure:(LGDefineNetServerFailureResultMsg)failure;

///创建订单（约指定大咖）
+ (void)creatDirectivityOrder:(LGOrderInfo *)order success:(LGDefineNetServerSuccessResult)success failure:(LGDefineNetServerFailureResultMsg)failure;

///获取今日订单（大咖）
+ (void)getTodayOrderWithPage:(NSUInteger)page Success:(LGDefineNetServerSuccessResult)success failure:(LGDefineNetServerFailureResultMsg)failure;

///接受订单（大咖）
+ (void)acceptOrder:(LGOrderInfo *)order success:(LGDefineNetServerSuccessResult)success failure:(LGDefineNetServerFailureResultMsg)failure;

///开始订单（大咖）
+ (void)startOrder:(LGOrderInfo *)order success:(LGDefineNetServerSuccessResult)success failure:(LGDefineNetServerFailureResultMsg)failure;

///结束订单（大咖）
+ (void)finishOrder:(LGOrderInfo *)order success:(LGDefineNetServerSuccessResult)success failure:(LGDefineNetServerFailureResultMsg)failure;

///取消订单（用户）
+ (void)cancelOrder:(LGOrderInfo *)order success:(LGDefineNetServerSuccessResult)success failure:(LGDefineNetServerFailureResultMsg)failure;

///冻结订单（用户）
+ (void)blockOrder:(LGOrderInfo *)order success:(LGDefineNetServerSuccessResult)success failure:(LGDefineNetServerFailureResultMsg)failure;

///评价订单（用户）
+ (void)appraiseOrder:(LGOrderInfo *)order star:(int)star success:(LGDefineNetServerSuccessResult)success failure:(LGDefineNetServerFailureResultMsg)failure;

@end

@interface LGDefineNetServer (Poi)

///添加一条Poi数据
//+ (void)creatOnePoiData:(LGPoiUserInfo *)data success:(LGDefineNetServerSuccessResult)success failure:(LGDefineNetServerFailureResult)failure;

///更新位置
+ (void)updateLocation:(LGPoiUserInfo *)userInfo success:(void(^)(void))success failure:(void(^)(void))failure;

///设置用户上下线状态
+ (void)setOnlineState:(BOOL)online account:(NSString *)account success:(void(^)(void))success failure:(void(^)(void))failure;

///设置是否接受订单通知
+ (void)setReceiveOrderSwitch:(BOOL)open account:(NSString *)account success:(void(^)(void))success failure:(LGDefineNetServerFailureResultMsg)failure;

@end

@interface LGDefineNetServer (Game)
///获取LOL游戏服务器列表
+ (void)getGameLOLServerListSuccess:(LGDefineNetServerSuccessResult)success failure:(LGDefineNetServerFailureResultMsg)failure;

///获取LOL游戏角色详细数据
+ (void)getGameLOLGamePlayerDetailWithGamePlayer:(LGGamePlayerInfo *)gamePlayerInfo success:(LGDefineNetServerSuccessResult)success failure:(LGDefineNetServerFailureResultMsg)failure;

///获取LOL游戏最近所有比赛列表(可以指定单个英雄)
+ (void)getGameLOLGamePlayerMatchListWithGamePlayer:(LGGamePlayerInfo *)gamePlayerInfo gameHero:(LGGameHeroInfo *)gameHeroInfo page:(NSUInteger)page success:(LGDefineNetServerSuccessResult)success failure:(LGDefineNetServerFailureResultMsg)failure;

///获取LOL游戏角色单场比赛数据
+ (void)getGameLOLGamePlayerMatchDetailWithGameMatch:(LGGameMatchInfo *)gameMatchInfo success:(LGDefineNetServerSuccessResult)success failure:(LGDefineNetServerFailureResultMsg)failure;

@end


typedef void(^LGDefineNetServerCertificateDakaSuccess)(NSInteger state);
@interface LGDefineNetServer (Certification)

///大咖认证
+ (void)certificateDaka:(LGDakaCertificate *)dakaCertificate success:(LGDefineNetServerCertificateDakaSuccess)success failure:(LGDefineNetServerFailureResultMsg)failure;


@end







