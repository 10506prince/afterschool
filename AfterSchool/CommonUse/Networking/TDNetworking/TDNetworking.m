//
//  TDNetworking.m
//  SMSVerificationCode
//
//  Created by Teson Draw on 9/22/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "TDNetworking.h"
#import "AppDelegate.h"

@implementation TDNetworking


+ (void)requestWithURL:(NSString *)url
            parameters:(NSDictionary *)parameters
               success:(void(^)(id responseObject))success
               failure:(void(^)(NSError *error))failure
{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];

    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", @"text/plain", nil];
    
    [manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        
        if ([responseObject[@"result"] integerValue] == 19) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"19TD" message:[NSString stringWithFormat:@"%@", responseObject[@"msg"]] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alertView show];
            
            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            [appDelegate logout];
            
        } else {
            success(responseObject);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        failure(error);
    }];
}

+ (void)requestWithURL:(NSString *)url
            parameters:(NSDictionary *)parameters
      constructingBody:(void(^)(id<AFMultipartFormData> formData))constructingBody
               success:(void(^)(id responseObject))success
               failure:(void(^)(NSError *error))failure
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", @"text/plain", nil];
    
    [manager POST:url parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        constructingBody(formData);
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject[@"result"] integerValue] == 19) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"19TD" message:[NSString stringWithFormat:@"%@", responseObject[@"msg"]] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alertView show];
            
            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            [appDelegate logout];
        } else {
            success(responseObject);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
}

@end
