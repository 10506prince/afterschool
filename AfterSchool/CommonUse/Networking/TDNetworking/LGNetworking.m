//
//  LGNetworking.m
//  AfterSchool
//
//  Created by lg on 15/10/29.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGNetworking.h"

@implementation LGNetworking

+ (void)requestWithURL:(NSString *)url
            parameters:(NSDictionary *)parameters
               success:(void(^)(id responseObject))success
               failure:(void(^)(NSError *error))failure
{
    AFNetworkReachabilityManager * reachabilityManager = [AFNetworkReachabilityManager sharedManager];
    
    if(!reachabilityManager.isReachable) {
        NSLog(@"网络不可用");
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"通知" message:@"当前网络不可用" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    [[AFNetworkActivityIndicatorManager sharedManager] incrementActivityCount];
    
    AFHTTPRequestOperationManager * httpManager = [AFHTTPRequestOperationManager manager];
    httpManager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", @"text/plain", nil];
    
    [httpManager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [[AFNetworkActivityIndicatorManager sharedManager] decrementActivityCount];
        success(responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [[AFNetworkActivityIndicatorManager sharedManager] decrementActivityCount];
        failure(error);
    }];
}

+ (void)requestWithURL:(NSString *)url
            parameters:(NSDictionary *)parameters
      constructingBody:(void(^)(id<AFMultipartFormData> formData))constructingBody
               success:(void(^)(id responseObject))success
               failure:(void(^)(NSError *error))failure
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", @"text/plain", nil];
    
    [manager POST:url parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        constructingBody(formData);
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
}
@end
