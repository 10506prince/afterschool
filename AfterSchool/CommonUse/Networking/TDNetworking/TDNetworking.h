//
//  TDNetworking.h
//  SMSVerificationCode
//
//  Created by Teson Draw on 9/22/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"

@interface TDNetworking : NSObject

+ (void)requestWithURL:(NSString *)url
             parameters:(NSDictionary *)parameters
               success:(void(^)(id responseObject))success
               failure:(void(^)(NSError *error))failure;

+ (void)requestWithURL:(NSString *)url
            parameters:(NSDictionary *)parameters
      constructingBody:(void(^)(id<AFMultipartFormData> formData))constructingBody
               success:(void(^)(id responseObject))success
               failure:(void(^)(NSError *error))failure;


@end
