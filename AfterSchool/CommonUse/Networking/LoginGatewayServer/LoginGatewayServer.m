//
//  LoginGatewayServer.m
//  AfterSchool
//
//  Created by Teson Draw on 10/26/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "LoginGatewayServer.h"
#import "TDNetworking.h"
#import "TDSingleton.h"
#import "SVProgressHUD.h"
#import "LGDefineNetServer.h"
#import "LGLocationService.h"
#import "LGHeartBeatService.h"
#import "LGSceneInfo.h"
#import "LGLevelingPrice.h"

#import <RongIMKit/RongIMKit.h>

@implementation LoginGatewayServer

+ (void)submitMobileNumber:(NSString *)mobileNumber
                  password:(NSString *)password
                   success:(void(^)(void))success
                   failure:(void(^)(NSString *message))failure {
    NSDictionary *parameters = @{@"userName":mobileNumber,
                                 @"userPwd":password};
    
    NSLog(@"LoginVC submitMobileNumber parameter:%@", parameters);
    
    [TDNetworking requestWithURL:[TDSingleton instance].URL.loginServerLoginURL
                      parameters:parameters
                         success:^(id responseObject)
     {
         NSLog(@"submitMobileNumber result:%@", responseObject);
         if ([responseObject[@"result"] integerValue] == 1) {
             
             [self loginGatewayServerWithData:responseObject
                                 mobileNumber:mobileNumber
                                      success:^
             {
                 success();
             } failure:^(NSString *message) {
                 failure(message);
             }];
         } else {
             NSString *msg = [NSString stringWithFormat:@"%@", responseObject[@"msg"]];
             failure(msg);
         }
     } failure:^(NSError *error) {
         NSLog(@"submitMobileNumber error:%@", error);
         NSString *message = [NSString stringWithFormat:@"%@", [error localizedDescription]];
         failure(message);
     }];
}

+ (void)loginGatewayServerWithData:(NSDictionary *)data
                      mobileNumber:(NSString *)mobileNumber
                           success:(void(^)(void))success
                           failure:(void(^)(NSString *message))failure {
    
    [TDSingleton getUserInfoWithData:data];
    
    NSString *sign = [NSString stringWithFormat:@"%@", data[@"sign"]];
    NSString *time = [NSString stringWithFormat:@"%@", data[@"time"]];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:mobileNumber forKey:@"userName"];
    [parameters setObject:[TDSingleton instance].gatewayServerID forKey:@"serverId"];
    
    [parameters setObject:time forKey:@"time"];
    [parameters setObject:sign forKey:@"sign"];
    
    if ([TDSingleton instance].pushRegistrationID.length > 0) {
        [parameters setObject:[TDSingleton instance].pushRegistrationID forKey:@"pushRegistrationId"];
    }
    
//    NSDictionary *parameters = @{@"userName":mobileNumber,
//                                 @"serverId":[TDSingleton instance].gatewayServerID,
//                                 @"time":time,
//                                 @"sign":sign,
//                                 @"pushRegistrationId":[TDSingleton instance].pushRegistrationID};
    
    NSLog(@"loginGatewayServerWithData URL:%@", [TDSingleton instance].gatewayServerLoginURL);
    NSLog(@"loginGatewayServerWithData parameters:%@", parameters);
    [TDNetworking requestWithURL:[TDSingleton instance].gatewayServerLoginURL
                      parameters:parameters
                         success:^(id responseObject)
     {
         NSLog(@"loginGatewayServerWithData result:%@", responseObject);
        
//         if ([responseObject[@"userInfo"][@"type"] integerValue] == 0) {
             if ([responseObject[@"result"] integerValue] == 1) {
                 
                 [self processLoginGatewayServerData:responseObject];
                 
                 [TDSingleton instance].gameCenterURI = [responseObject objectForKey:@"gameCenterURI"];
                 [TDSingleton instance].orderSystemPrice = [[responseObject objectForKey:@"orderSystemPrice"] integerValue];
                 
                 ///获取情景模式筛选条件
                 NSArray * scenes = [responseObject objectForKey:@"scenes"];
                 [TDSingleton instance].scenes = [LGSceneInfo objectArrayWithKeyValuesArray:scenes];
                 
                 ///获取保晋级配置列表
                 NSArray * levelingPrices = [responseObject objectForKey:@"levelingPrices"];
                 [TDSingleton instance].levelingPrices = [LGLevelingPrice objectArrayWithKeyValuesArray:levelingPrices];
                 
                 ///获取好友列表
                 NSArray * friends = [responseObject objectForKey:@"friendList"];
                 NSArray <LGDaKaInfo *> *contacsList = [LGDaKaInfo dakaInofArrayWithFriendOrBlackDicArray:friends];
                 [[TDSingleton instance].userInfoModel.contacsListCache addObjectsFromArray:contacsList];
                 
                 ///获取黑名单列表
                 NSArray * blacks = [responseObject objectForKey:@"blackList"];
                 NSArray <LGDaKaInfo *> *blackList = [LGDaKaInfo dakaInofArrayWithFriendOrBlackDicArray:blacks];
                 [[TDSingleton instance].userInfoModel.blackListCache addObjectsFromArray:blackList];
                 
                 ///更改登录状态
                 if ([TDSingleton instance].userInfoModel.account.length != 0) {
                     [LGDefineNetServer setOnlineState:YES account:[TDSingleton instance].userInfoModel.account success:^{
                     } failure:^{
                     }];
                 }
                 
//                 [TDSingleton instance].userInfoModel
                 
                 ///开始发送心跳
                 [[LGHeartBeatService sharedManager] start];
                 
                 ///开始定位
                 [[LGLocationService sharedManager] startUserLocationService];
                 
                 [self getToken];
                 success();
             } else {
                 failure([NSString stringWithFormat:@"%@", responseObject[@"msg"]]);
             }
//         } else {
//             failure(@"请使用玩家账号登录!");
//         }
     } failure:^(NSError *error) {
         NSLog(@"error:%@", error);
         NSString *message = [NSString stringWithFormat:@"%@", error.description];
         failure(message);
     }];
}

+ (void)processLoginGatewayServerData:(NSDictionary *)data {
    [TDSingleton instance].customerService = data[@"kefuInfo"];
    [TDSingleton instance].sessionKey = [NSString stringWithFormat:@"%@", data[@"sessionKey"]];
    [TDSingleton instance].userInfoModel = [[UserInfoModel alloc] initWithData:data[@"userInfo"]];
    NSLog(@"userInfoModel:%@", [TDSingleton instance].userInfoModel.description);

    [[TDSingleton instance].settingDictionary setValue:[TDSingleton instance].account forKey:@"Account"];
    [[TDSingleton instance].settingDictionary setValue:[TDSingleton instance].password forKey:@"Password"];
    [[TDSingleton instance].settingDictionary setValue:[NSNumber numberWithBool:YES] forKey:@"AutoLogin"];
    [[TDSingleton instance].settingDictionary writeToFile:[TDSingleton instance].settingPlistFileName atomically:YES];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    
    NSDictionary *userInfoDic = [userDefault dictionaryForKey:@"UserInfo"];
    
    if (userInfoDic == nil) {
        userInfoDic = [[NSMutableDictionary alloc] init];
        
        NSMutableDictionary *accountDic = [[NSMutableDictionary alloc] init];
        [accountDic setObject:[TDSingleton instance].userInfoModel.nickName forKey:@"Name"];
        [accountDic setObject:[TDSingleton instance].userInfoModel.portraitURL forKey:@"PortraitURL"];
        
        NSDictionary *chatObjectDic = [[NSDictionary alloc] init];
        [accountDic setObject:chatObjectDic forKey:@"ChatObject"];
        
        [userInfoDic setValue:accountDic forKey:[TDSingleton instance].account];
        
        [userDefault setObject:userInfoDic forKey:@"UserInfo"];
    } else {
        
        /**
         *  判断是否有该账号的记录
         */
        if (![[userInfoDic allKeys] containsObject:[TDSingleton instance].account]) {
            NSMutableDictionary * userInfoAllDic = [[NSMutableDictionary alloc] initWithDictionary:userInfoDic];
            
            NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
            [dic setObject:[TDSingleton instance].userInfoModel.nickName forKey:@"Name"];
            [dic setObject:[TDSingleton instance].userInfoModel.portraitURL forKey:@"PortraitURL"];
            
            NSDictionary *chatObjectDic = [[NSDictionary alloc] init];
            [dic setObject:chatObjectDic forKey:@"ChatObject"];

            [userInfoAllDic setObject:dic forKey:[TDSingleton instance].account];
            
            [userDefault setObject:userInfoAllDic forKey:@"UserInfo"];
        } else {
            /**
             *  更新当前用户的昵称和头像URL
             */
            
            /**
             *  全部用户信息
             */
            NSMutableDictionary * userInfoAllDic = [[NSMutableDictionary alloc] initWithDictionary:userInfoDic];
            
            /**
             *  获取当前账号对应的字典
             */
            NSDictionary *dic = userInfoAllDic[[TDSingleton instance].account];
            NSMutableDictionary *currentUserDic = [[NSMutableDictionary alloc] initWithDictionary:dic];
            [currentUserDic setObject:[TDSingleton instance].userInfoModel.nickName forKey:@"Name"];
            [currentUserDic setObject:[TDSingleton instance].userInfoModel.portraitURL forKey:@"PortraitURL"];
            
            [userInfoAllDic setObject:currentUserDic forKey:[TDSingleton instance].account];
            
            [userDefault setObject:userInfoAllDic forKey:@"UserInfo"];
        }
    }
    
    [userDefault synchronize];
}

+ (void)getToken {
//    if ([[[TDSingleton instance].settingDictionary[@"Instant messaging token"] allKeys] containsObject:[TDSingleton instance].account]) {
//        [TDSingleton instance].token = [TDSingleton instance].settingDictionary[@"Instant messaging token"][[TDSingleton instance].account];
//    }

//    if ([TDSingleton instance].token.length == 0) {
        NSDictionary *parameters = @{@"userName":[TDSingleton instance].userInfoModel.account,
                                     @"nickName":[TDSingleton instance].userInfoModel.nickName,
                                         @"icon":[TDSingleton instance].userInfoModel.portraitURL};
        
        
        NSLog(@"getToken parameters:%@", parameters);
        [TDNetworking requestWithURL:[TDSingleton instance].URL.getTokenURL
                          parameters:parameters
                             success:^(id responseObject)
         {
             NSLog(@"getToken result:%@", responseObject);
             if ([responseObject[@"result"] integerValue] == 1) {
                 NSString * token = [NSString stringWithFormat:@"%@", responseObject[@"token"]];
                 
                 [self saveToken:token];
                 
                 [self loginRongCloudWithToken:token];
             }
         } failure:^(NSError *error) {
             NSLog(@"getToken error:%@", error);
         }];
//    } else {
//        [self loginRongCloudWithToken:[TDSingleton instance].token];
//    }
}

+ (void)saveToken:(NSString *)token {
    
    if (![[[TDSingleton instance].settingDictionary[@"Instant messaging token"] allKeys] containsObject:[TDSingleton instance].account]) {
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:[TDSingleton instance].settingDictionary[@"Instant messaging token"]];
        [dic setValue:token forKey:[TDSingleton instance].account];
        
        [[TDSingleton instance].settingDictionary setValue:dic forKey:@"Instant messaging token"];
        
        [[TDSingleton instance].settingDictionary writeToFile:[TDSingleton instance].settingPlistFileName atomically:YES];
    }
}

/**
 *  登录融云
 *
 */
+ (void)loginRongCloudWithToken:(NSString *)token
{
    [[RCIM sharedRCIM] connectWithToken:token
                                success:^(NSString *userId)
     {
         NSLog(@"loginRongCloud successfully with userId: %@.", userId);
     } error:^(RCConnectErrorCode status) {
         NSLog(@"login Rong Cloud error status: %ld.", (long)status);
     } tokenIncorrect:^{
         NSLog(@"token 无效 ，请确保生成token 使用的appkey 和初始化时的appkey 一致");
     }];
}

@end
