//
//  LoginGatewayServer.h
//  AfterSchool
//
//  Created by Teson Draw on 10/26/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LoginGatewayServer : NSObject

+ (void)submitMobileNumber:(NSString *)mobileNumber
                  password:(NSString *)password
                   success:(void(^)(void))success
                   failure:(void(^)(NSString *message))failure;

+ (void)loginGatewayServerWithData:(NSDictionary *)data
                      mobileNumber:(NSString *)mobileNumber
                           success:(void(^)(void))success
                           failure:(void(^)(NSString *message))failure;

@end
