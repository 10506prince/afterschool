//
//  LGNetworking.m
//  AfterSchool
//
//  Created by lg on 15/10/29.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGNetworking.h"
#import "AppDelegate.h"

@implementation LGNetworking

+ (void)requestWithURL:(NSString *)url
            parameters:(NSDictionary *)parameters
               success:(void(^)(id responseObject))success
               failure:(void(^)(NSError *error))failure
{
    AFHTTPRequestOperationManager * httpManager = [AFHTTPRequestOperationManager manager];
    [httpManager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [httpManager.requestSerializer setTimeoutInterval:30];
    
    [[AFNetworkActivityIndicatorManager sharedManager] incrementActivityCount];
    [httpManager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {

        if ([responseObject[@"result"] integerValue] == 19) {
            NSError * error = [NSError errorWithDomain:@"com.lg.network" code:-19 userInfo:@{NSLocalizedDescriptionKey:@"服务器更新，请重新登录"}];
            failure(error);
            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            [appDelegate logout];
        } else {
            success(responseObject);
        }
        
        [[AFNetworkActivityIndicatorManager sharedManager] decrementActivityCount];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [[AFNetworkActivityIndicatorManager sharedManager] decrementActivityCount];
        failure(error);
    }];
}

+ (void)requestWithURL:(NSString *)url
            parameters:(NSDictionary *)parameters
           cachePolicy:(NSURLRequestCachePolicy)cachePolicy
               success:(void(^)(id responseObject))success
               failure:(void(^)(NSError *error))failure
{
    AFHTTPRequestOperationManager * httpManager = [AFHTTPRequestOperationManager manager];
    [httpManager.requestSerializer setCachePolicy:cachePolicy];
    [httpManager.requestSerializer setTimeoutInterval:30];
    
    [[AFNetworkActivityIndicatorManager sharedManager] incrementActivityCount];
    [httpManager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if ([responseObject[@"result"] integerValue] == 19) {
            NSError * error = [NSError errorWithDomain:@"com.lg.network" code:-19 userInfo:@{NSLocalizedDescriptionKey:@"服务器更新，请重新登录"}];
            failure(error);
            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            [appDelegate logout];
        } else {
            success(responseObject);
        }
        
        [[AFNetworkActivityIndicatorManager sharedManager] decrementActivityCount];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [[AFNetworkActivityIndicatorManager sharedManager] decrementActivityCount];
        failure(error);
    }];
}


+ (void)requestWithURL:(NSString *)url
            parameters:(NSDictionary *)parameters
      constructingBody:(void(^)(id<AFMultipartFormData> formData))constructingBody
               success:(void(^)(id responseObject))success
               failure:(void(^)(NSError *error))failure
{
    AFHTTPRequestOperationManager *httpManager = [AFHTTPRequestOperationManager manager];
    
    [[AFNetworkActivityIndicatorManager sharedManager] incrementActivityCount];
    [httpManager POST:url parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        constructingBody(formData);
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if ([responseObject[@"result"] integerValue] == 19) {
            NSError * error = [NSError errorWithDomain:@"com.lg.network" code:-19 userInfo:@{NSLocalizedDescriptionKey:@"服务器更新，请重新登录"}];
            failure(error);
            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            [appDelegate logout];
        } else {
            success(responseObject);
        }
        
        [[AFNetworkActivityIndicatorManager sharedManager] decrementActivityCount];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
        [[AFNetworkActivityIndicatorManager sharedManager] decrementActivityCount];
    }];
}

+ (void)getRequestWithURL:(NSString *)url
               parameters:(NSDictionary *)parameters
                  success:(void(^)(id responseObject))success
                  failure:(void(^)(NSError *error))failure
{
    AFHTTPRequestOperationManager * httpManager = [AFHTTPRequestOperationManager manager];
    [httpManager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];///忽略缓存
    [httpManager.requestSerializer setTimeoutInterval:30];
    
    [[AFNetworkActivityIndicatorManager sharedManager] incrementActivityCount];
    [httpManager GET:url parameters:parameters success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        [[AFNetworkActivityIndicatorManager sharedManager] decrementActivityCount];
        success(responseObject);
    } failure:^(AFHTTPRequestOperation * _Nonnull operation, NSError * _Nonnull error) {
        [[AFNetworkActivityIndicatorManager sharedManager] decrementActivityCount];
        failure(error);
    }];
}

+ (void)getRequestWithURL:(NSString *)url
               parameters:(NSDictionary *)parameters
              cachePolicy:(NSURLRequestCachePolicy)cachePolicy
               success:(void(^)(id responseObject))success
               failure:(void(^)(NSError *error))failure
{
    AFHTTPRequestOperationManager * httpManager = [AFHTTPRequestOperationManager manager];
    [httpManager.requestSerializer setCachePolicy:cachePolicy];///设置缓存策略
    [httpManager.requestSerializer setTimeoutInterval:30];
    
    [[AFNetworkActivityIndicatorManager sharedManager] incrementActivityCount];
    [httpManager GET:url parameters:parameters success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        [[AFNetworkActivityIndicatorManager sharedManager] decrementActivityCount];
        success(responseObject);
    } failure:^(AFHTTPRequestOperation * _Nonnull operation, NSError * _Nonnull error) {
        [[AFNetworkActivityIndicatorManager sharedManager] decrementActivityCount];
        failure(error);
    }];
}
@end
