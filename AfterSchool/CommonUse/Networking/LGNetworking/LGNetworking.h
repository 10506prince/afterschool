//
//  LGNetworking.h
//  AfterSchool
//
//  Created by lg on 15/10/29.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIKit+AFNetworking.h"
#import "AFNetworking.h"

@interface LGNetworking : NSObject

+ (void)requestWithURL:(NSString *)url
            parameters:(NSDictionary *)parameters
               success:(void(^)(id responseObject))success
               failure:(void(^)(NSError *error))failure;

+ (void)requestWithURL:(NSString *)url
            parameters:(NSDictionary *)parameters
           cachePolicy:(NSURLRequestCachePolicy)cachePolicy
               success:(void(^)(id responseObject))success
               failure:(void(^)(NSError *error))failure;

+ (void)requestWithURL:(NSString *)url
            parameters:(NSDictionary *)parameters
      constructingBody:(void(^)(id<AFMultipartFormData> formData))constructingBody
               success:(void(^)(id responseObject))success
               failure:(void(^)(NSError *error))failure;

+ (void)getRequestWithURL:(NSString *)url
            parameters:(NSDictionary *)parameters
               success:(void(^)(id responseObject))success
               failure:(void(^)(NSError *error))failure;

+ (void)getRequestWithURL:(NSString *)url
               parameters:(NSDictionary *)parameters
              cachePolicy:(NSURLRequestCachePolicy)cachePolicy
                  success:(void(^)(id responseObject))success
                  failure:(void(^)(NSError *error))failure;

@end
