//
//  TDNetworkRequest.m
//  AfterSchool
//
//  Created by Teson Draw on 11/24/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "TDNetworkRequest.h"
#import "TDNetworking.h"
#import "TDSingleton.h"
#import "SVProgressHUD.h"


@implementation TDNetworkRequest


+ (void)requestWithAccount:(NSString *)account
                    action:(ACCOUNT_ACTION)action
                   success:(void(^)(id responseObject))success {
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    [parameters setObject:account forKey:@"targetUserName"];
    [parameters setObject:[TDSingleton instance].sessionKey forKey:@"sessionKey"];

    switch (action) {
        case AddFriend:
            [parameters setObject:@"addFriend" forKey:@"action"];
            break;
            
        case DeleteFriend:
            [parameters setObject:@"deleteFriend" forKey:@"action"];
            break;
            
        case AddBlacklist:
            [parameters setObject:@"addBlackList" forKey:@"action"];
            break;
            
        case DeleteBlacklist:
            [parameters setObject:@"deleteBlackList" forKey:@"action"];
            break;
    }
    
    [TDNetworking requestWithURL:[TDSingleton instance].gatewayServerBusinessURL
                      parameters:parameters
                         success:^(id responseObject)
     {
         if ([responseObject[@"result"] integerValue] == 1) {
             success(responseObject);
         } else {
             [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@", responseObject[@"msg"]] duration:2];
         }
     } failure:^(NSError *error) {
         NSLog(@"error:%@", error);
         [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@", error] duration:2];
     }];
}

+ (void)requestWithContactsAction:(CONTACTS_ACTION)action
                          success:(void(^)(id responseObject))success {
//    [SVProgressHUD show];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    [parameters setObject:[TDSingleton instance].sessionKey forKey:@"sessionKey"];
    
    switch (action) {
        case GetFriendContacts:
            [parameters setObject:@"getFriends" forKey:@"action"];
            break;
            
        case GetBlacklist:
            [parameters setObject:@"getBlackList" forKey:@"action"];
            break;
    }
    
    [TDNetworking requestWithURL:[TDSingleton instance].gatewayServerBusinessURL
                      parameters:parameters
                         success:^(id responseObject)
     {
         if ([responseObject[@"result"] integerValue] == 1) {
//             [SVProgressHUD dismiss];
             success(responseObject);
         } else {
             [SVProgressHUD dismissWithError:[NSString stringWithFormat:@"%@", responseObject[@"msg"]] afterDelay:2];
         }
     } failure:^(NSError *error) {
         NSLog(@"error:%@", error);
         [SVProgressHUD dismissWithError:[NSString stringWithFormat:@"%@", error] afterDelay:2];
     }];
}

+ (void)getOrdersWithType:(OrderType)type
                     page:(NSUInteger)page
                  success:(void(^)(id responseObject))success {

    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    [parameters setObject:[TDSingleton instance].sessionKey forKey:@"sessionKey"];
    [parameters setObject:[NSString stringWithFormat:@"%ld", (unsigned long)page] forKey:@"page"];
    
    
    switch (type) {
        case OrderAll:
            [parameters setObject:@"getAllOrders" forKey:@"action"];
            break;
            
        case OrderInProgress:
            [parameters setObject:@"getGoingOrders" forKey:@"action"];
            break;
            
        case OrderFinished:
            [parameters setObject:@"getFinishedOrders" forKey:@"action"];
            break;
            
        case OrderNotStarted:
            [parameters setObject:@"getNewOrders" forKey:@"action"];
            break;
            
        case OrderCancelled:
            [parameters setObject:@"getCanceledOrders" forKey:@"action"];
            break;
    }
    
    [TDNetworking requestWithURL:[TDSingleton instance].gatewayServerBusinessURL
                      parameters:parameters
                         success:^(id responseObject)
     {
         if ([responseObject[@"result"] integerValue] == 1) {
             success(responseObject);
         } else {
         }
     } failure:^(NSError *error) {
         NSLog(@"error:%@", error);
     }];
}

+ (void)getUserInfoWithAccount:(NSString *)account
                       success:(void(^)(id responseObject))success {
    
    NSDictionary *parameters = @{@"action":@"getUserSimple",
                                 @"sessionKey":[TDSingleton instance].sessionKey,
                                 @"user":account};
    
    [TDNetworking requestWithURL:[TDSingleton instance].gatewayServerBusinessURL
                      parameters:parameters
                         success:^(id responseObject)
     {
         if ([responseObject[@"result"] integerValue] == 1) {
             success(responseObject);
         } else {
             [SVProgressHUD dismissWithError:[NSString stringWithFormat:@"%@", responseObject[@"msg"]] afterDelay:2];
         }
     } failure:^(NSError *error) {
         NSLog(@"error:%@", error);
         [SVProgressHUD dismissWithError:[NSString stringWithFormat:@"%@", error] afterDelay:2];
     }];
}

#pragma mark Start Order
+ (void)startOrderWithOrderID:(NSString *)orderID
                      success:(void(^)(NSString *message))success
                      failure:(void(^)(NSString *error))failure {
    
    NSDictionary *parameters = @{@"action":@"startOrder",
                                 @"sessionKey":[TDSingleton instance].sessionKey,
                                 @"orderId":orderID};
    
    [TDNetworking requestWithURL:[TDSingleton instance].gatewayServerBusinessURL
                      parameters:parameters
                         success:^(id responseObject)
     {
         NSString *message = [NSString stringWithFormat:@"%@", responseObject[@"msg"]];
         
         if ([responseObject[@"result"] integerValue] == 1) {
             success(message);
         } else {
             failure(message);
         }
     } failure:^(NSError *error) {
         failure([NSString stringWithFormat:@"%@", error.description]);
     }];
}

#pragma mark Cancel Order
+ (void)cancelOrderWithOrderID:(NSString *)orderID
                       timeout:(NSString *)timeout
                       success:(void(^)(NSString *message))success
                       failure:(void(^)(NSString *error))failure {
    
    NSDictionary *parameters = @{@"action":@"cancelOrder",
                                 @"sessionKey":[TDSingleton instance].sessionKey,
                                 @"orderId":orderID,
                                 @"timeout":timeout};
    
    [TDNetworking requestWithURL:[TDSingleton instance].gatewayServerBusinessURL
                      parameters:parameters
                         success:^(id responseObject)
     {
         NSString *message = [NSString stringWithFormat:@"%@", responseObject[@"msg"]];
         
         if ([responseObject[@"result"] integerValue] == 1) {
             success(message);
         } else {
             failure(message);
         }
     } failure:^(NSError *error) {
         failure([NSString stringWithFormat:@"%@", error.description]);
     }];
}

#pragma mark Complete Order
+ (void)bigShotCompleteOrderWithOrderID:(NSString *)orderID
                                success:(void(^)(NSString *message))success
                                failure:(void(^)(NSString *error))failure {
    
    NSDictionary *parameters = @{@"action":@"endOrder",
                                 @"sessionKey":[TDSingleton instance].sessionKey,
                                 @"orderId":orderID};
    
    [TDNetworking requestWithURL:[TDSingleton instance].gatewayServerBusinessURL
                      parameters:parameters
                         success:^(id responseObject)
     {
         NSString *message = [NSString stringWithFormat:@"%@", responseObject[@"msg"]];
         
         if ([responseObject[@"result"] integerValue] == 1) {
             success(message);
         } else {
             failure(message);
         }
     } failure:^(NSError *error) {
         failure([NSString stringWithFormat:@"%@", error.description]);
     }];
}

+ (void)getPenaltyWithOrderID:(NSString *)orderID
                      success:(void(^)(float penalty))success
                      failure:(void(^)(NSString *error))failure {
    
    NSDictionary *parameters = @{@"action":@"getCancelOrderPenalty",
                                 @"sessionKey":[TDSingleton instance].sessionKey,
                                 @"orderId":orderID};
    
    [TDNetworking requestWithURL:[TDSingleton instance].gatewayServerBusinessURL
                      parameters:parameters
                         success:^(id responseObject)
     {
         NSString *message = [NSString stringWithFormat:@"%@", responseObject[@"msg"]];
         
         float penalty = [responseObject[@"penalty"] floatValue];
         
         if ([responseObject[@"result"] integerValue] == 1) {
             success(penalty);
         } else {
             failure(message);
         }
     } failure:^(NSError *error) {
         failure([NSString stringWithFormat:@"%@", error.description]);
     }];
}

+ (void)userCompleteOrderWithOrderID:(NSString *)orderID
                             success:(void(^)(NSString *message))success
                             failure:(void(^)(NSString *error))failure {
    
    NSDictionary *parameters = @{@"action":@"confirmEndOrder",
                                 @"sessionKey":[TDSingleton instance].sessionKey,
                                 @"orderId":orderID};
    
    [TDNetworking requestWithURL:[TDSingleton instance].gatewayServerBusinessURL
                      parameters:parameters
                         success:^(id responseObject)
     {
         NSString *message = [NSString stringWithFormat:@"%@", responseObject[@"msg"]];
         
         if ([responseObject[@"result"] integerValue] == 1) {
             success(message);
         } else {
             failure(message);
         }
     } failure:^(NSError *error) {
         failure([NSString stringWithFormat:@"%@", error.description]);
     }];
}

#pragma mark Freeze Order
+ (void)freezeOrderWithOrderID:(NSString *)orderID
                       success:(void(^)(NSString *message))success
                       failure:(void(^)(NSString *error))failure {
    
    NSDictionary *parameters = @{@"action":@"blockOrder",
                                 @"sessionKey":[TDSingleton instance].sessionKey,
                                 @"orderId":orderID};
    
    [TDNetworking requestWithURL:[TDSingleton instance].gatewayServerBusinessURL
                      parameters:parameters
                         success:^(id responseObject)
     {
         NSString *message = [NSString stringWithFormat:@"%@", responseObject[@"msg"]];
         
         if ([responseObject[@"result"] integerValue] == 1) {
             success(message);
         } else {
             failure(message);
         }
     } failure:^(NSError *error) {
         failure([NSString stringWithFormat:@"%@", error.description]);
     }];
}

+ (void)deleteOrderWithOrderId:(NSString *)orderId
                       success:(void(^)(NSString *message))success
                       failure:(void(^)(NSString *error))failure
{
    NSDictionary *parameters = @{@"action":@"deleteOrder",
                                 @"sessionKey":[TDSingleton instance].sessionKey,
                                 @"orderId":orderId};
    
    [TDNetworking requestWithURL:[TDSingleton instance].gatewayServerBusinessURL
                      parameters:parameters
                         success:^(id responseObject)
     {
         NSString *message = [NSString stringWithFormat:@"%@", responseObject[@"msg"]];
         
         if ([responseObject[@"result"] integerValue] == 1) {
             success(message);
         } else {
             failure(message);
         }
     } failure:^(NSError *error) {
         failure([NSString stringWithFormat:@"%@", error.description]);
     }];
}

+ (void)submitRegistrationId:(NSString *)registrationId
                     success:(void(^)(NSString *message))success
                     failure:(void(^)(NSString *error))failure {
    
    NSDictionary *parameters = @{@"action":@"updatePushId",
                                 @"sessionKey":[TDSingleton instance].sessionKey,
                                 @"pushRegistrationId":registrationId};
    
    [TDNetworking requestWithURL:[TDSingleton instance].gatewayServerBusinessURL
                      parameters:parameters
                         success:^(id responseObject)
     {
         NSString *message = [NSString stringWithFormat:@"%@", responseObject[@"msg"]];
         
         if ([responseObject[@"result"] integerValue] == 1) {
             success(@"Registration ID 上传成功！");
         } else {
             failure(message);
         }
     } failure:^(NSError *error) {
         failure([NSString stringWithFormat:@"%@", error.description]);
     }];
}

@end
