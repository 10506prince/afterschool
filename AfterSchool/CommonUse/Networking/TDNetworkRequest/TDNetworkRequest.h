//
//  TDNetworkRequest.h
//  AfterSchool
//
//  Created by Teson Draw on 11/24/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum : NSUInteger {
    AddFriend,          ///<添加好友
    DeleteFriend,       ///<删除好友
    AddBlacklist,       ///<添加到黑名单
    DeleteBlacklist,    ///<从黑名单中删除
} ACCOUNT_ACTION;

typedef enum : NSUInteger {
    GetFriendContacts,  ///<获取好友列表
    GetBlacklist        ///<获取黑名单
} CONTACTS_ACTION;

typedef enum :NSUInteger {
    OrderAll,       ///<全部订单
    OrderInProgress,///<进行中的订单
    OrderFinished,  ///<已完成的订单
    OrderNotStarted,///<未开始的订单
    OrderCancelled  ///<废弃的订单
} OrderType;

@interface TDNetworkRequest : NSObject

+ (void)requestWithAccount:(NSString *)account
                    action:(ACCOUNT_ACTION)action
                   success:(void(^)(id responseObject))success;

+ (void)requestWithContactsAction:(CONTACTS_ACTION)action
                          success:(void(^)(id responseObject))success;


+ (void)getOrdersWithType:(OrderType)type
                     page:(NSUInteger)page
                  success:(void(^)(id responseObject))success;

//+ (void)cancelOrderWithOrderID:(NSString *)orderID
//                       success:(void(^)(id responseObject))success;


+ (void)getUserInfoWithAccount:(NSString *)account
                       success:(void(^)(id responseObject))success;


+ (void)startOrderWithOrderID:(NSString *)orderID
                      success:(void(^)(NSString *message))success
                      failure:(void(^)(NSString *error))failure;

+ (void)cancelOrderWithOrderID:(NSString *)orderID
                       timeout:(NSString *)timeout
                      success:(void(^)(NSString *message))success
                      failure:(void(^)(NSString *error))failure;

/**
 *  大咖结束订单
 *
 *  @param orderID 订单编号
 *  @param success 返回成功
 *  @param failure 失败字符串
 */
+ (void)bigShotCompleteOrderWithOrderID:(NSString *)orderID
                                success:(void(^)(NSString *message))success
                                failure:(void(^)(NSString *error))failure;

/**
 *  用户获取违约金
 *
 *  @param orderID 订单编号
 *  @param success 返回成功
 *  @param failure 失败字符串
 */
+ (void)getPenaltyWithOrderID:(NSString *)orderID
                      success:(void(^)(float penalty))success
                      failure:(void(^)(NSString *error))failure;

/**
 *  用户结束订单
 *
 *  @param orderID 订单编号
 *  @param success 返回成功
 *  @param failure 失败字符串
 */
+ (void)userCompleteOrderWithOrderID:(NSString *)orderID
                             success:(void(^)(NSString *message))success
                             failure:(void(^)(NSString *error))failure;

/**
 *  冻结订单
 *
 *  @param orderID 订单编号
 *  @param success 返回成功
 *  @param failure 失败字符串
 */
+ (void)freezeOrderWithOrderID:(NSString *)orderID
                       success:(void(^)(NSString *message))success
                       failure:(void(^)(NSString *error))failure;

/**
 *  删除订单
 *
 *  @param orderID 订单编号
 *  @param success 成功的描述
 *  @param failure 失败的描述
 */
+ (void)deleteOrderWithOrderId:(NSString *)orderId
                       success:(void(^)(NSString *message))success
                       failure:(void(^)(NSString *error))failure;

/**
 *  上传极光推送的注册id到App服务器
 *
 *  @param registrationId 注册id（极光回调）
 *  @param success        成功的描述
 *  @param failure        失败的描述
 */
+ (void)submitRegistrationId:(NSString *)registrationId
                     success:(void(^)(NSString *message))success
                     failure:(void(^)(NSString *error))failure;

@end
