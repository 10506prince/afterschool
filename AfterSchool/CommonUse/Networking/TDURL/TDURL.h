//
//  TDURL.h
//  AfterSchool
//
//  Created by Teson Draw on 10/24/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <Foundation/Foundation.h>

//#define LAN

#ifdef LAN
    #define BASE_IP @"192.168.1.2"
    static NSString * poiTableId = @"126618";///<POI数据表Id(内网)
    static NSString * poiAk = @"PUtgVO5Ke1fmRUgZhG2YvLB1";///<POI访问权限AK（内网）

#else
    #define BASE_IP @"112.74.66.100"
    static NSString * poiTableId = @"126619";///<POI数据表Id(外网)
    static NSString * poiAk = @"PUtgVO5Ke1fmRUgZhG2YvLB1";///<POI访问权限AK就（外网）

#endif

@interface TDURL : NSObject

@property (nonatomic, strong) NSString *registerURL;
@property (nonatomic, strong) NSString *loginServerLoginURL;
@property (nonatomic, strong) NSString *getTokenURL;
@property (nonatomic, strong) NSString *setPasswordURL;

@property (nonatomic, copy) NSString * getAliOSSTokenURL;

- (instancetype)initBaseURL;

@end
