//
//  TDURL.m
//  AfterSchool
//
//  Created by Teson Draw on 10/24/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "TDURL.h"

@implementation TDURL

- (instancetype)initBaseURL {
    self = [super init];
    if (self) {
        _registerURL         = [NSString stringWithFormat:@"http://%@:8281/register", BASE_IP];
        _loginServerLoginURL = [NSString stringWithFormat:@"http://%@:8281/login", BASE_IP];
        _getTokenURL         = [NSString stringWithFormat:@"http://%@:8281/getRongToken", BASE_IP];
        _setPasswordURL      = [NSString stringWithFormat:@"http://%@:8281/updatePwd", BASE_IP];
        _getAliOSSTokenURL   = [NSString stringWithFormat:@"http://%@:8281/getAliOSSToken", BASE_IP];
    }
    return self;
}

@end
