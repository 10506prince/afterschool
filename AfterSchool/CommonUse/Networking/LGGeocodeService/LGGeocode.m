//
//  LGGeocode.m
//  AfterSchool
//
//  Created by lg on 15/11/24.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGGeocode.h"

@interface LGGeocode ()
{
    bool isGeoSearch;
}
@end

@implementation LGGeocode
+ (LGGeocode*)sharedManager {
    static dispatch_once_t once;
    static LGGeocode *sharedManager;
    dispatch_once(&once, ^ { sharedManager = [[LGGeocode alloc] init]; });
    return sharedManager;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setUp];
    }
    return self;
}

- (void)setUp {
    _geocodesearch = [[BMKGeoCodeSearch alloc]init];
    _geocodesearch.delegate = self;
}

- (void)dealloc {
    if (_geocodesearch != nil) {
        _geocodesearch = nil;
    }
}

- (void)onGetGeoCodeResult:(BMKGeoCodeSearch *)searcher result:(BMKGeoCodeResult *)result errorCode:(BMKSearchErrorCode)error {
    if (error == 0) {
        NSLog(@"%@,%@", searcher,result);
    }
}

- (void)onGetReverseGeoCodeResult:(BMKGeoCodeSearch *)searcher result:(BMKReverseGeoCodeResult *)result errorCode:(BMKSearchErrorCode)error {
    if (error == 0) {
        NSLog(@"%@,%@", searcher,result);
    }
}

-(IBAction)reverseGeocodeWithPt:(CLLocationCoordinate2D)pt success:(LGGeocodeServerSuccessResult)success failure:(LGGeocodeServerFailure)failure
{
    isGeoSearch = false;

    BMKReverseGeoCodeOption *reverseGeocodeSearchOption = [[BMKReverseGeoCodeOption alloc]init];
    reverseGeocodeSearchOption.reverseGeoPoint = pt;
    BOOL flag = [_geocodesearch reverseGeoCode:reverseGeocodeSearchOption];
    if(flag)
    {
        NSLog(@"反geo检索发送成功");
    }
    else
    {
        NSLog(@"反geo检索发送失败");
    }
    
}

-(IBAction)geocodeWithCity:(NSString *)city address:(NSString *)address success:(LGGeocodeServerSuccessResult)success failure:(LGGeocodeServerFailure)failure
{
    isGeoSearch = true;
    BMKGeoCodeSearchOption *geocodeSearchOption = [[BMKGeoCodeSearchOption alloc]init];
    geocodeSearchOption.city= city;
    geocodeSearchOption.address = address;
    BOOL flag = [_geocodesearch geoCode:geocodeSearchOption];
    if(flag)
    {
        NSLog(@"geo检索发送成功");
    }
    else
    {
        NSLog(@"geo检索发送失败");
    }
    
}

@end
