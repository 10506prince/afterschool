//
//  LGGeocode.h
//  AfterSchool
//
//  Created by lg on 15/11/24.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <BaiduMapAPI_Search/BMKSearchComponent.h>

typedef void(^LGGeocodeServerSuccess)(void);
typedef void(^LGGeocodeServerFailure)(void);

typedef void(^LGGeocodeServerSuccessResult)(id result);
typedef void(^LGGeocodeServerFailureResult)(NSError * error);

@interface LGGeocode : NSObject<BMKGeoCodeSearchDelegate> {
    BMKGeoCodeSearch* _geocodesearch;
}

-(IBAction)reverseGeocodeWithPt:(CLLocationCoordinate2D)pt success:(LGGeocodeServerSuccessResult)success failure:(LGGeocodeServerFailure)failure;
-(IBAction)geocodeWithCity:(NSString *)city address:(NSString *)address success:(LGGeocodeServerSuccessResult)success failure:(LGGeocodeServerFailure)failure;
@end
