//
//  ServerInterface.h
//  AfterSchool
//
//  Created by Teson Draw on 10/24/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#ifndef ServerInterface_h
#define ServerInterface_h

#import "TDURL.h"

#pragma mark - POI接口
//static NSString * poiTableId = @"121218";///<POI数据表Id
//static NSString * poiAk = @"SkyQpwFfmNfc1AS9MfWoPSZX";///<POI访问权限AK

static NSString * bmkAk = @"NHnrZ9BU6sCxwehGivWO57WC";///<iOS类型
//static NSString * bmkAk = @"PUtgVO5Ke1fmRUgZhG2YvLB1";///<服务器类型
static NSString * poiBaseUrl = @"http://api.map.baidu.com/geodata/v3/poi/";//

static NSString * actionPoiCreat = @"create";///<创建一条POI数据
static NSString * actionPoiUpdate = @"update";///<更新一条POI数据
static NSString * actionPoiList = @"list";///<查询指定条件的数据（poi）列表接口
static NSString * actionPoiDetail = @"detail";///<查询指定id的数据（poi）详情接口
static NSString * actionPoiDelete = @"delete";///<删除数据（poi）接口（支持批量）

#pragma mark - DakaInfo
static NSString * actionDaKaInfo = @"getUsersSimple";///<大咖摘要Action
static NSString * actionDaKaDetail = @"getUserDetail";///<大咖详情Aciton
static NSString * actionDaKaLocationReport = @"reportPosition";///<报告大咖位置Aciton
static NSString * actionUserRectiveStateChange = @"changeState";///<改变大咖是否接单状态Aciton

#pragma mark - UserInfo
static NSString * actionUserMapIdUpdate = @"updateMapUserId";///<修改用户地图存储id
static NSString * actionAddMoneyTest = @"addMoney";///<模拟充值Action
static NSString * actionMoneyGet = @"getMoney";///<获取钱包数据Action
static NSString * actionUserIconUpdateStep1 = @"updateIconStep1";///<修改用户头像第一步Action
static NSString * actionUserIconUpdateStep2 = @"updateIconStep2";///<修改用户头像第二部Action

static NSString * actionChargeGet = @"getCharge";///<获取充值单
static NSString * actionModifyPassword = @"updatePwd";///<修改用户密码

static NSString * actionCertificateDaka = @"applyBigshot";///<申请大咖认证

#pragma mark Player
static NSString * actionPlayersGet = @"getPlayers";///<获取绑定游戏角色Action
static NSString * actionPlayerAdd = @"addPlayer";///<添加绑定游戏角色Aciton
static NSString * actionPlayerDelete = @"deletePlayer";///<删除绑定游戏角色Action
static NSString * actionPlayerModifyDefault = @"modifyDefaultPlayer";///<修改默认游戏角色Aciton

#pragma mark Address
static NSString * actionAddressGet = @"getAddress";///<获取常用地址Aciton
static NSString * actionAddressAdd = @"addAddress";///<添加常用地址Action
static NSString * actionAddressDelete = @"deleteAddress";///<删除常用地址Aciton
static NSString * actionAddressModify = @"modifyAddress";///<修改常用地址Action
static NSString * actionAddressModifyDefault = @"modifyDefaultAddress";///<修改默认地址Aciton

#pragma mark Order
static NSString * actionOrderFastCall = @"fastCall";///<创建订单（一键约）Action
static NSString * actionOrderCallMaster = @"callMaster";///<创建订单（约指定大咖）Aciton

static NSString * actionOrderGetTodayOrders = @"getTodayOrders";///<获取今日订单（大咖端）
static NSString * actionOrderAccept = @"acceptOrder";///<接受订单（大咖端）Action
static NSString * actionOrderStart = @"startOrder";///<开始订单（大咖端）Aciton
static NSString * actionOrderEnd = @"endOrder";///<结束订单（大咖端）Action
static NSString * actionOrderCancel = @"cancelOrder";///<取消订单（用户端）Aciton
static NSString * actionOrderBlock = @"blockOrder";///<冻结订单（用户端）Action
static NSString * actionOrderRate = @"rateOrder";///<评价订单（用户端）Aciton

#pragma mark - Game
static NSString * actionGameLOLServerList = @"getLOLserverList";///<获取LOL游戏服务器列表
static NSString * actionGameLOLGamePlayerDetail = @"getPlayerDetail";///<获取LOL游戏角色详细数据
static NSString * actionGameLOLGamePlayerMatchList = @"getPlayerAllMatches";///<获取LOL游戏服务器列表
static NSString * actionGameLOLGamePlayerMatchDetail = @"getPlayerOneMatch";///<获取LOL游戏角色单场比赛数据

#pragma mark - CloudSearch
static NSString * localCloudSearchUrl = @"http://api.map.baidu.com/geosearch/v3/local";
static NSString * nearbyCloudSearchUrl = @"http://api.map.baidu.com/geosearch/v3/nearby";
static NSString * boundCloudSearchUrl = @"http://api.map.baidu.com/geosearch/v3/bound";
static NSString * detailCloudSearchUrl = @"http://api.map.baidu.com/geosearch/v3/detail";

#pragma mark - Geocode
static NSString * geocodeUrl = @"http://api.map.baidu.com/geocoder/v2/";

#pragma mark - Place Suggestion API
static NSString * placeSuggestionUrl = @"http://api.map.baidu.com/place/v2/suggestion/";

#pragma mark - Place API
static NSString * placeUrl = @"http://api.map.baidu.com/place/v2/search";
#endif /* ServerInterface_h */
