//
//  Common.h
//  qisuedu
//
//  Created by apple on 15/3/11.
//  Copyright (c) 2015年 apple. All rights reserved.
//

#ifndef qisuedu_Common_h
#define qisuedu_Common_h

#import <UIKit/UIKit.h>
#import <objc/message.h>

/*********************************打印信息********************************/
#ifdef DEBUG
#else
#define NSLog(...)
#define debugMethod()
#endif

//// 日志输出
//#ifdef DEBUG
//#define LGLog(...) NSLog(__VA_ARGS__)
//#else
//#define LGLog(...)
//#endif

#define NSLogSelfMethodName NSLog(@"%s", __FUNCTION__)
//#define selfControllerName [NSString stringWithFormat:@"%s", __FUNCTION__]
#define selfControllerName NSStringFromClass([self class])

// 过期提醒
#define LGDeprecated(instead) NS_DEPRECATED(2_0, 2_0, 2_0, 2_0, instead)

// 运行时objc_msgSend
#define LGMsgSend(...) ((void (*)(void *, SEL, UIView *))objc_msgSend)(__VA_ARGS__)
#define LGMsgTarget(target) (__bridge void *)(target)

/********************************* 颜色 ********************************/
// 颜色
#define LgColor(c,alp) [UIColor colorWithRed:(c)/255.0 green:(c)/255.0 blue:(c)/255.0 alpha:alp]
#define LgColorRGB(r,g,b,alp) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:alp]

//RGB的颜色转换
#define LGColorFromHexRGB(hexRGBValue,alp) [UIColor \
colorWithRed:((float)((hexRGBValue & 0xFF0000) >> 16))/255.0 \
green:((float)((hexRGBValue & 0xFF00) >> 8))/255.0 \
blue:((float)(hexRGBValue & 0xFF))/255.0 \
alpha:((float)(alp))]

#define BTN_BLUE @"041182246"

#define APPBorderLightColor LgColorRGB(102, 204, 194, 1)
#define APPLightColor LgColorRGB(38,166,154,1)
#define APPDarkColor LgColorRGB(29,122,113,1)

#define APPOrangeLightColor LgColorRGB(255,145,0,1)
#define APPTextColor LgColor(56,1)

/*********************************屏幕尺寸********************************/
// 屏幕尺寸
#define LgScreenWidth ([UIScreen mainScreen].bounds.size.width)
#define LgScreenHeigh ([UIScreen mainScreen].bounds.size.height)

#define LgTextFieldCornerRadius (5)
#define LgButtonCornerRadius (3)

/*********************************系统版本********************************/
#pragma mark -- 系统版本
/**
 是 iOS8 或 iOS8以上
 */
#define iOS8 ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
/**
 是 iOS7 或 iOS7以上
 */
#define iOS7 ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)
/**
 是 iOS9 或 iOS9以上
 */
#define iOS9 ([[[UIDevice currentDevice] systemVersion] floatValue] >= 9.0)

#pragma mark -- 手机版本

//#define iPhone4 ([UIScreen mainScreen].bounds.size.height == 480)
//#define iPhone5 ([UIScreen mainScreen].bounds.size.height == 568)
//#define iPhone6 ([UIScreen mainScreen].bounds.size.height == 667)
//#define iPhone6P ([UIScreen mainScreen].bounds.size.height == 736)

//画一像素的线
/*用法
 CGFloat xPos = 5;
 UIView *view = [[UIView alloc] initWithFrame:CGrect(xPos - SINGLE_LINE_ADJUST_OFFSET, 0, SINGLE_LINE_WIDTH, 100)];//竖线
 CGFloat yPos = 5;
 UIView *view = [[UIView alloc] initWithFrame:CGrect(0, yPos - SINGLE_LINE_ADJUST_OFFSET, 100, SINGLE_LINE_WIDTH)];//横线
 */
#define SINGLE_LINE_WIDTH           (1 / [UIScreen mainScreen].scale)
#define SINGLE_LINE_ADJUST_OFFSET   ((1 / [UIScreen mainScreen].scale) / 2)


////通知关键字
//#define MY_LOCATION_UPDATE_NOTIFICATION @"MY_LOCATION_UPDATE_NOTIFICATION"//位置更新
//#define BMK_GETNETWORD_NOTIFICATION @"BMK_GETNETWORD_NOTIFICATION"//联网成功GetNetwork
//#define BMK_GETPERMISSION_NOTIFICATION @"BMK_GETPERMISSION_NOTIFICATION"//授权成功GetPermission

#endif
