//
//  MacrosDefinition.h
//  CodeRepository
//
//  Created by apple on 4/28/15.
//  Copyright (c) 2015 Teson Draw. All rights reserved.
//

#ifndef CodeRepository_MacrosDefinition_h
#define CodeRepository_MacrosDefinition_h

//屏幕高度和宽度
#define SCREEN_HEIGHT [UIScreen mainScreen].bounds.size.height
#define SCREEN_WIDTH  [UIScreen mainScreen].bounds.size.width

//机型定义
#define iPhone4s ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 960), [[UIScreen mainScreen] currentMode].size) : NO)

#define iPhone5s ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)

#define iPhone6 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(750, 1334), [[UIScreen mainScreen] currentMode].size) : NO)

#define iPhone6Plus ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1242, 2208), [[UIScreen mainScreen] currentMode].size) : NO)

//获取RGB颜色函数
#define COLOR(Red, Green, Blue) [UIColor colorWithRed:Red/255.0 green:Green/255.0 blue:Blue/255.0 alpha:1.0] 

#define BUTTON_CORNER_RADIUS 3

//#define SHOW_TEXT

#define RONG_CLOUD_MESSAGES

#define iOS8 ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

#endif
