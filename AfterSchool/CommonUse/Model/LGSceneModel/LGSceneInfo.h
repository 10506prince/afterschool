//
//  LGSceneInfo.h
//  AfterSchool
//
//  Created by lg on 15/12/12.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LGSceneInfo : NSObject

@property (nonatomic, assign) NSUInteger sceneId;///<情景模式ID
@property (nonatomic, assign) NSUInteger order;///<显示优先级
@property (nonatomic, copy) NSString * name;///<情景名称

@property (nonatomic, copy) NSString * filter;///<数值筛选条件
@property (nonatomic, copy) NSString * keyWord;///<字符串筛选条件

@property (nonatomic, copy) NSString * attack;///<战斗力（格式：0,100）(是个范围值，代表搜索战力在（self+0,self+100）之间的值)
@property (nonatomic, copy) NSString * grade;///<段位（格式：0，1）（范围）

@property (nonatomic, copy) NSString * sortby;///<排序方式 @"distance:1";

//@property (nonatomic, assign) float reward;///<建议打赏单价
@property (nonatomic, assign) float priceOnline;///<建议线上单价
@property (nonatomic, assign) float priceOffline;///<建议线下单价

@end



