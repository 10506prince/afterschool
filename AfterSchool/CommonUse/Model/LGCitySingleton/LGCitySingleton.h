//
//  LGCitySingleton.h
//  AfterSchool
//
//  Created by lg on 15/11/24.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <Foundation/Foundation.h>

@class LGChina,LGProvince,LGCity;

@interface LGCitySingleton : NSObject

+ (LGCitySingleton*)sharedManager;

@property (nonatomic, strong) LGChina * china;

@end

@interface LGChina : NSObject
@property (nonatomic, strong) NSArray<LGCity *> *other;
@property (nonatomic, strong) NSArray<LGCity *> *municipalities;
@property (nonatomic, strong) NSArray<LGProvince *> *provinces;
@end

@interface LGProvince : NSObject

@property (nonatomic, copy) NSString *n;

@property (nonatomic, copy) NSString *g;

@property (nonatomic, strong) NSArray<LGCity *> *cities;

@end

@interface LGCity : NSObject

@property (nonatomic, copy) NSString *n;

@property (nonatomic, copy) NSString *g;

@end

