//
//  LGCitySingleton.m
//  AfterSchool
//
//  Created by lg on 15/11/24.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGCitySingleton.h"
#import "MJExtension.h"

@implementation LGCitySingleton

+ (LGCitySingleton*)sharedManager {
    static dispatch_once_t once;
    static LGCitySingleton *sharedManager;
    dispatch_once(&once, ^ { sharedManager = [[LGCitySingleton alloc] init]; });
    return sharedManager;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setUp];
    }
    return self;
}

- (void)setUp {
    NSString * path = [[NSBundle mainBundle] pathForAuxiliaryExecutable:@"citysJson"];
    NSError * error = [[NSError alloc] init];

    NSData * chinaData = [NSData dataWithContentsOfFile:path];
    NSDictionary * chinaDic = [NSJSONSerialization JSONObjectWithData:chinaData options:NSJSONReadingAllowFragments error:&error];

    if (error.code == 0) {
        NSLog(@"%@",chinaDic);
        self.china = [LGChina objectWithKeyValues:chinaDic];
        
    } else {
        NSLog(@"%@", [error localizedDescription]);
    }
}

@end

@implementation LGChina

+ (NSDictionary *)objectClassInArray{
    return @{@"other" : [LGCity class], @"municipalities" : [LGCity class], @"provinces" : [LGProvince class]};
}
@end


@implementation LGProvince

+ (NSDictionary *)objectClassInArray{
    return @{@"cities" : [LGCity class]};
}

@end


@implementation LGCity
@end


