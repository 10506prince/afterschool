//
//  LGPoiUserInfo.m
//  AfterSchool
//
//  Created by lg on 15/11/5.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGPoiUserInfo.h"

@implementation LGPoiInfo
- (instancetype)init
{
    self = [super init];
    if (self) {
        _location = CLLocationCoordinate2DMake(0, 0);
        _coord_type = 3;
    }
    return self;
}

@end

@implementation LGPoiUserInfo
- (instancetype)init
{
    self = [super init];
    if (self) {
        _user_id = @"";
        _online_state = 0;
        _role_type = 0;
        _sex = 0;
        _attack = 0;
        _grade = @"";
        _serverName = @"";
    }
    return self;
}

@end
