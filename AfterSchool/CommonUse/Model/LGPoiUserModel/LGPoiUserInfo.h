//
//  LGPoiUserInfo.h
//  AfterSchool
//
//  Created by lg on 15/11/5.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "MJExtension.h"

@interface LGPoiInfo : NSObject

//百度默认Poi字段
@property (nonatomic, assign) NSUInteger id;///<唯一标识
@property (nonatomic, assign) CLLocationCoordinate2D location;///<坐标
//@property (nonatomic, strong) NSArray<NSNumber *> *location;///<经纬度
@property (nonatomic, copy) NSString *province;///<省名称
@property (nonatomic, assign) NSUInteger city_id;///<市id
@property (nonatomic, copy) NSString *city;///<市名
@property (nonatomic, copy) NSString *district;///<区名
@property (nonatomic, copy) NSString *title;///<名称
@property (nonatomic, copy) NSString *address;///<地址
@property (nonatomic, assign) NSUInteger coord_type;///<用户上传的坐标的类型(默认3) 可选，1.GPS经纬度坐标2.国测局加密经纬度坐标3.百度加密经纬度坐标4.百度加密墨卡托坐标
@property (nonatomic, copy) NSString *tags;///<标签
@property (nonatomic, assign) NSUInteger geotable_id;///<表主键
@property (nonatomic, assign) NSUInteger create_time;///<创建时间
@property (nonatomic, assign) NSUInteger modify_time;///<修改时间

@end

@interface LGPoiUserInfo : LGPoiInfo

//自定义字段
@property (nonatomic, copy) NSString *user_id;///<用户id
@property (nonatomic, assign) NSUInteger role_type;///<角色类型
@property (nonatomic, assign) NSUInteger online_state;///<在线状态
@property (nonatomic, copy) NSString * serverName;///<默认游戏服
@property (nonatomic, copy) NSString * grade;///<段位
@property (nonatomic, assign) NSUInteger attack;///<战斗力
@property (nonatomic, assign) NSUInteger sex;///<性别

@end
