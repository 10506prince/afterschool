//
//  LGCloudSearchService.m
//  AfterSchool
//
//  Created by lg on 15/11/21.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGCloudSearchOption.h"

@implementation LGBaseCloudSearchInfo
@end

@implementation LGCloudSearchInfo
@end

@implementation LGCloudBoundSearchInfo
@end

@implementation LGCloudLocalSearchInfo
@end

@implementation LGCloudNearbySearchInfo
@end

@implementation LGCloudDetailSearchInfo
@end

@implementation LGCloudPOIInfo
- (CLLocationCoordinate2D)isLocationValue {
    CLLocationDegrees latitude = [self.location[1] doubleValue];///维度
    CLLocationDegrees longitude = [self.location[0] doubleValue];///经度
    return CLLocationCoordinate2DMake(latitude, longitude);
}
@end

@implementation LGBaseCloudPOIInfo
@end