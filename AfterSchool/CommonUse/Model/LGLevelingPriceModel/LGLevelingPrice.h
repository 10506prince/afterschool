//
//  LGLevelingPrice.h
//  AfterSchool
//
//  Created by lg on 15/12/26.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSString+Other.h"

@interface LGLevelingPrice : NSObject

@property (nonatomic, copy) NSString * nowGrade;///<当前段位
@property (nonatomic, assign) NSUInteger nowGradeId;///<当前段位Id
@property (nonatomic, copy) NSString * targetGrade;///<目标段位
@property (nonatomic, assign) NSUInteger targetGradeId;///<目标段位Id
@property (nonatomic, assign) NSUInteger sid;///<id
@property (nonatomic, assign) NSUInteger useTime;///<花费时间
@property (nonatomic, assign) float reward;///<建议价格

@end
