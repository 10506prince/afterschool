//
//  LGOrderModel.h
//  AfterSchool
//
//  Created by lg on 15/10/29.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <UIKit/UIKit.h>
#import "NSObject+Property.h"
#import "LGGamePlayerInfo.h"

@interface LGOrderInfo : NSObject

///创建订单需要
@property (nonatomic, assign) float money;///<需要花费零钱
@property (nonatomic, assign) float tokenMoney;///<需要花费代币
@property (nonatomic, assign) float rewardMoney __deprecated_msg("弃用");///<打赏金额（当前已经弃用）

@property (nonatomic, copy) NSString * masters;///<大咖（格式：15196614938,15196614939）
@property (nonatomic, assign) CLLocationCoordinate2D location;///<地址pt
@property (nonatomic, copy) NSString * address;///<约玩地址

@property (nonatomic, assign) BOOL offLine;///<是否是线下
@property (nonatomic, copy) NSString * info;///<额外信息（情景模式名称）
@property (nonatomic, assign) NSUInteger sceneId;///<情景模式Id（情景模式参数）
@property (nonatomic, copy) NSString * targetGrade;///<目标段位(保晋级模式参数)

@property (nonatomic, assign) NSTimeInterval planStartTime;///<计划开始时间时间戳
@property (nonatomic, assign) NSUInteger planKeepTime;//<计划约玩时间，单位:小时

@property (nonatomic, assign) BOOL timeOutFlag;///<超时取消标志
@property (nonatomic, strong) LGGamePlayerInfo * selectGameAccount;///<选中的游戏账号

///从服务器获取到订单需要
@property (nonatomic, assign) long long id;///<订单Id

@property (nonatomic, assign) NSTimeInterval createTime;///<创建时间
@property (nonatomic, copy) NSString * senderNickName;///<发送订单玩家昵称
@property (nonatomic, copy) NSString * senderUserName;///<发送订单玩家账户
@property (nonatomic, copy) NSString * receiverNickName;///<接收订单玩家昵称
@property (nonatomic, copy) NSString * receiverUserName;///<接收订单玩家账户
@property (nonatomic, copy) NSString * comment;///<
@property (nonatomic, assign) NSTimeInterval timeOut;///<超时时间

@property (nonatomic, assign) long long serialNo;///<订单序列号
@property (nonatomic, assign) NSUInteger rating;///<用户评价
@property (nonatomic, assign) NSUInteger reward;///<
@property (nonatomic, assign) NSUInteger state;///<

@end
