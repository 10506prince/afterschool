//
//  LGOrderModel.m
//  AfterSchool
//
//  Created by lg on 15/10/29.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGOrderInfo.h"

@implementation LGOrderInfo

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setUp];
    }
    return self;
}

- (void)setUp {
    NSTimeInterval time = [[NSDate date] timeIntervalSince1970] * 1000;
    self.planStartTime = round(time);
    self.money = 0;
    self.tokenMoney = 0;
    self.rewardMoney = 0;
}

@end
