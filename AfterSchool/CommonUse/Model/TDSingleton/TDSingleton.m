//
//  SingletonInstance.m
//  MWOA
//
//  Created by Teson Draw on 3/17/15.
//  Copyright (c) 2015 Teson Draw. All rights reserved.
//

#import "TDSingleton.h"

@implementation TDSingleton

static TDSingleton *_instance = nil;

+ (TDSingleton *)instance
{
    if (!_instance) {
        _instance = [[super alloc] init];
    }
    return _instance;
}

+ (void)getUserInfoWithData:(id)data {
    [TDSingleton instance].uploadImageURL = [NSString stringWithFormat:@"%@", data[@"imageUploadURL"]];
    [TDSingleton instance].gatewayServerID = [NSString stringWithFormat:@"%@", data[@"serverId"]];
    [TDSingleton instance].gatewayServerIP = [NSString stringWithFormat:@"%@", data[@"serverIp"]];
    
    [TDSingleton instance].tcpPort = [NSString stringWithFormat:@"%@", data[@"tcpPort"]];
    [TDSingleton instance].httpPort = [NSString stringWithFormat:@"%@", data[@"httpPort"]];
    
    [TDSingleton instance].gatewayServerLoginURL = [NSString stringWithFormat:@"http://%@:%@/login", data[@"serverIp"], data[@"httpPort"]];
    [TDSingleton instance].gatewayServerBusinessURL = [NSString stringWithFormat:@"http://%@:%@/business", data[@"serverIp"], data[@"httpPort"]];
    [TDSingleton instance].gatewayServerLogoutURL = [NSString stringWithFormat:@"http://%@:%@/logout", data[@"serverIp"], data[@"httpPort"]];
}

+ (void)initUserInfo {
    [TDSingleton instance].uploadImageURL           = @"";
    [TDSingleton instance].portraitImageURL         = @"";
    [TDSingleton instance].pushRegistrationID       = @"";
    [TDSingleton instance].gatewayServerLoginURL    = @"";
    [TDSingleton instance].gatewayServerBusinessURL = @"";//大咖信息URL
    [TDSingleton instance].gatewayServerIP          = @"";
    [TDSingleton instance].gatewayServerID          = @"";
    [TDSingleton instance].sessionKey               = @"";
}

+ (void)initSettingWithData:(NSDictionary *)data {
    [TDSingleton instance].token = @"";
    
    [TDSingleton instance].account = [NSString stringWithFormat:@"%@", [TDSingleton instance].settingDictionary[@"Account"]];
    [TDSingleton instance].password = [NSString stringWithFormat:@"%@", [TDSingleton instance].settingDictionary[@"Password"]];
    [TDSingleton instance].autoLogin = [[[TDSingleton instance].settingDictionary objectForKey:@"AutoLogin"] boolValue];
    [TDSingleton instance].fontSizeType = [[[TDSingleton instance].settingDictionary objectForKey:@"FontSizeType"] integerValue];
    
    [TDSingleton instance].chatBackgroundImageNumber = [[[TDSingleton instance].settingDictionary objectForKey:@"ChatBackgroundImageNumber"] integerValue];
    [TDSingleton instance].chatBackgroundImageName = [[TDSingleton instance].settingDictionary objectForKey:@"ChatBackgroundImageName"];
    
    if ([TDSingleton instance].autoLogin && ([TDSingleton instance].account.length == 0)) {
        NSLog(@"initSettingWithData 逻辑错误");
    }
}

@end
