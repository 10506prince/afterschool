//
//  SingletonInstance.h
//  MWOA
//
//  Created by Teson Draw on 3/17/15.
//  Copyright (c) 2015 Teson Draw. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserInfoModel.h"
#import "TDURL.h"
#import <CoreLocation/CoreLocation.h>
#import "FontSizeType.h"
#import "LGLevelingPrice.h"

@class LGSceneInfo,LGLevelingPrice;

@interface TDSingleton : NSObject

@property (nonatomic, retain) NSString *uploadImageURL;     //图片上传URL
@property (nonatomic, strong) NSString *portraitImageURL;   //下载的头像的URL
@property (nonatomic, strong) NSString *pushRegistrationID; //用于推送的注册ID

@property (nonatomic, strong) NSString *gatewayServerLoginURL;//网关服登录URL
@property (nonatomic, strong) NSString *gatewayServerBusinessURL;//业务端口 - 包括摘要等

@property (nonatomic, strong) NSString *gatewayServerLogoutURL;

@property (nonatomic, strong) NSString *gatewayServerIP;    //网关服务器IP
@property (nonatomic, strong) NSString *gatewayServerID;   //网关服务器ID

@property (nonatomic, copy) NSString *gameCenterURI;    //游戏数据URL

@property (nonatomic, strong) NSString *httpPort;           //短连接端口号
@property (nonatomic, strong) NSString *tcpPort;            //长连接端口号

@property (nonatomic, strong) NSString *sessionKey;

@property (nonatomic, strong) TDURL    *URL;
@property (nonatomic, strong) UserInfoModel *userInfoModel;

@property (nonatomic, strong) NSString *settingPlistFileName;
@property (nonatomic, strong) NSDictionary *settingDictionary;

@property (nonatomic, strong) NSDictionary *instantMessaging;

@property (nonatomic, strong) NSString *token;
@property (nonatomic, strong) NSString *account;
@property (nonatomic, strong) NSString *password;

@property (nonatomic, assign) BOOL autoLogin;
@property (nonatomic, assign) BOOL hasGetPushID;
@property (nonatomic, assign) BOOL isLogin;//是否登录主界面

@property (nonatomic, assign) FontSizeType fontSizeType;//字体大小
@property (nonatomic, assign) NSInteger chatBackgroundImageNumber;
@property (nonatomic, strong) NSString *chatBackgroundImageName;

@property (nonatomic, assign) NSUInteger orderSystemPrice;///<系统定价

@property (nonatomic, strong) NSDictionary *customerService;

//@property (copy) NSString * cityName;///<选择的城市名
@property (copy) NSString * currentCityName;///<当前所在的城市名
//@property (assign) CLLocationCoordinate2D buoyPt;///<浮标位置

@property (nonatomic, strong) NSArray <LGSceneInfo *> * scenes;///<情景模式条件
@property (nonatomic, strong) NSArray <LGLevelingPrice *> * levelingPrices;///<保晋级价格配置

+ (TDSingleton *)instance;

+ (void)initUserInfo;

//即将编写，从本地文件读取
//+ (void)initUserInfoWithData:(NSDictionary *)data;


+ (void)getUserInfoWithData:(id)data;

+ (void)initSettingWithData:(NSDictionary *)data;

@end
