//
//  LGGamePlayerInfo.h
//  AfterSchool
//
//  Created by lg on 15/11/11.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MJExtension.h"
#import "NSObject+Property.h"
#import "NSString+Null.h"

typedef struct {
    NSUInteger assassinate;///<刺杀
    NSUInteger existence;///<生存
    NSUInteger assist;///<助攻
    NSUInteger physical;///<物理
    NSUInteger magic;///<魔法
    NSUInteger defense;///<防御
    NSUInteger money;///<金钱
} LGGamePlayerSevenStar;

@interface LGGamePlayerInfo : NSObject

@property (nonatomic, assign) long long id;///<游戏角色id
@property (nonatomic, copy) NSString * icon;///<游戏角色头像地址
@property (nonatomic, copy) NSString * playerName;///<角色名
@property (nonatomic, copy) NSString * serverName;///<服务器名
@property (nonatomic, assign) NSUInteger level;///<游戏角色等级

@property (nonatomic, copy) NSString * grade;///<段位
@property (nonatomic, readonly) NSUInteger gradeId;///<段位编号
@property (nonatomic, assign) NSInteger attack;///<战斗力

@property (nonatomic, assign) NSUInteger totalRounds;///<总场数
@property (nonatomic, assign) NSUInteger loseRounds;///<负场
@property (nonatomic, assign) NSUInteger winRounds;///<胜场

@property (nonatomic, copy) NSString * sevenStar;
//@property (nonatomic, assign) LGGamePlayerSevenStar sevenStarValue;//七星数据 [48,39,42,74,92,49,24] 第一位=刺杀，第二位=生存，第三位=助攻，第四位=物理，第五位=魔法，第六位=防御，第七位=金钱

@property (nonatomic, assign, readonly, getter=isSeventStarValue) LGGamePlayerSevenStar sevenStarValue;

@end
