//
//  LGGameMath.m
//  AfterSchool
//
//  Created by lg on 15/11/16.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGGameMatch.h"
#import "NSObject+Property.h"

@implementation LGGameMatch

- (instancetype)initWithGameMatchInfo:(LGGameMatchInfo *)gameMatchInfo {
    self = [super init];
    if (self) {
        NSArray * propertys = [LGGameMatchInfo propertys];
        for (NSString * property in propertys) {
            [self setValue:[gameMatchInfo valueForKey:property] forKey:property];
        }
    }
    return self;
}
@end



