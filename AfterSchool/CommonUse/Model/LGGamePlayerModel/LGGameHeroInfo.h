//
//  LGGameHeroInfo.h
//  AfterSchool
//
//  Created by lg on 15/11/16.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MJExtension.h"

@interface LGGameHeroInfo : NSObject

@property (nonatomic, copy) NSString * name_en;///<英文名
@property (nonatomic, copy) NSString * name_ch;///<中文名
@property (nonatomic, copy) NSString * img;///<头像地址

@property (nonatomic, assign) NSUInteger winRate;///<胜率
@property (nonatomic, assign) NSUInteger matchStat;///<使用次数

@end
