//
//  LGGameMath.h
//  AfterSchool
//
//  Created by lg on 15/11/16.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGGameMatchInfo.h"
#import "LGGameTeamInfo.h"

@interface LGGameMatch : LGGameMatchInfo

@property (nonatomic, strong) NSArray <LGGameTeamMemberInfo *> * winTeam;
@property (nonatomic, strong) NSArray <LGGameTeamMemberInfo *> * loseTeam;
@property (nonatomic, strong) NSArray <LGGameTeamDataDetail *> * top;

- (instancetype)initWithGameMatchInfo:(LGGameMatchInfo *)gameMatchInfo;

@end
