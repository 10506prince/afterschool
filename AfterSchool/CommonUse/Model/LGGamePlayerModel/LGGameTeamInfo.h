//
//  LGGameTeamInfo.h
//  AfterSchool
//
//  Created by lg on 15/11/17.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LGExpandProtocol.h"

@class LGGameEquipInfo,LGGameSkillInfo,LGGameTeamDataDetail;

//团队
@interface LGGameTeamInfo : NSObject
@end

//队员
@interface LGGameTeamMemberInfo : NSObject <LGExpandProtocol>

@property (nonatomic, copy) NSString *attack;///<战斗力
@property (nonatomic, copy) NSString *kill;///<击杀/死亡/助攻
@property (nonatomic, strong) NSArray<LGGameEquipInfo *> *equips;///<装备
@property (nonatomic, strong) NSArray<LGGameSkillInfo *> *skills;///<技能
@property (nonatomic, strong) NSArray<LGGameTeamDataDetail *> *detail;///<详细数据

@property (nonatomic, copy) NSString *level;///<等级
@property (nonatomic, copy) NSString *grade;///<段位
@property (nonatomic, copy) NSString *gold;///<金钱
//@property (nonatomic, copy) NSString *mvp;///<是否是MVP
@property (nonatomic, copy) NSString *icon;///<头像地址
@property (nonatomic, copy) NSString *name;///<昵称

@property (nonatomic, assign) BOOL mvp;///<是否是MVP

@end

//装备
@interface LGGameEquipInfo : NSObject

@property (nonatomic, copy) NSString *img;///<图标地址
@property (nonatomic, copy) NSString *title;///<描述

@end

//技能
@interface LGGameSkillInfo : NSObject

@property (nonatomic, copy) NSString *img;///<图标地址

@end

//数据
@interface LGGameTeamDataDetail : NSObject

@property (nonatomic, copy) NSString *key;///<
@property (nonatomic, copy) NSString *value;///<

@end


