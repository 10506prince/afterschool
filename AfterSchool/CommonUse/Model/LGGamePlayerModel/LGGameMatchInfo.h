//
//  LGGameMatchInfo.h
//  AfterSchool
//
//  Created by lg on 15/11/16.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MJExtension.h"

@interface LGGameMatchInfo : NSObject

@property (nonatomic, assign) long long id;///<比赛id
@property (nonatomic, copy) NSString * img;///<
@property (nonatomic, assign) BOOL mvp;///<是否是MVP
@property (nonatomic, assign) BOOL fiveKill;///<5杀
@property (nonatomic, copy) NSString * type;///<比赛类型
@property (nonatomic, copy) NSString * time;///<
@property (nonatomic, copy) NSString * rst;///<输赢

#pragma mark - 自定义
@property (nonatomic, copy) NSString * playerName;
@property (nonatomic, copy) NSString * serverName;

@end
