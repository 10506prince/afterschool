//
//  LGGamePlayerInfo.m
//  AfterSchool
//
//  Created by lg on 15/11/11.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGGamePlayerInfo.h"
#import "NSString+Null.h"
#import "NSString+Other.h"

@implementation LGGamePlayerInfo
- (NSString *)description
{
    return [NSString stringWithFormat:@"\n角色名:%@ \n段位:%@ \n服务器:%@\n", self.playerName, self.grade, self.serverName];
}

- (LGGamePlayerSevenStar)isSeventStarValue {
    if (self.sevenStar.length > 0) {
        id array = [NSJSONSerialization JSONObjectWithData:[self.sevenStar dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:nil];

        if ([array isKindOfClass:[NSArray class]]) {
            if (((NSArray *)array).count == 7) {
                int i = 0;
                NSUInteger aaa[7] = {0};
                for (NSNumber * number in array) {
                    NSUInteger value = [number integerValue];
                    aaa[i] = value;
                    i++;
                }
                LGGamePlayerSevenStar tempSevenStar = {aaa[0],aaa[1],aaa[2],aaa[3],aaa[4],aaa[5],aaa[6]};
                return tempSevenStar;
            }
        }
    }
    
    LGGamePlayerSevenStar result = {0};
    return result;
}

- (NSUInteger)gradeId {
    
    return [self.grade gradeId];
}

@end
