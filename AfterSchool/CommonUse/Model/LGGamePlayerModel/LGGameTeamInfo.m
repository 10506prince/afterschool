//
//  LGGameTeamInfo.m
//  AfterSchool
//
//  Created by lg on 15/11/17.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGGameTeamInfo.h"

@implementation LGGameTeamInfo
@end

@interface LGGameTeamMemberInfo () {
    BOOL isExpand;
}

@end
@implementation LGGameTeamMemberInfo

///实现协议
- (BOOL)expand {
    return isExpand;
}

- (void)setExpand:(BOOL)expand {
    isExpand = expand;
}

+ (NSDictionary *)objectClassInArray{
    return @{@"equips" : [LGGameEquipInfo class], @"skills" : [LGGameSkillInfo class], @"detail" : [LGGameTeamDataDetail class]};
}

@end

@implementation LGGameEquipInfo
@end


@implementation LGGameSkillInfo
@end


@implementation LGGameTeamDataDetail
@end


