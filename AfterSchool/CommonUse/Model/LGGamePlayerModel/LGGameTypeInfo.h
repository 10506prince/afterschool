//
//  LGGameTypeInfo.h
//  AfterSchool
//
//  Created by lg on 15/11/16.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MJExtension.h"

@interface LGGameTypeInfo : NSObject

@property (nonatomic, copy) NSString * m;///<模式
@property (nonatomic, assign) NSUInteger w;///<赢的场数
@property (nonatomic, assign) NSUInteger l;///<输的场数
@property (nonatomic, copy) NSString * o;///<胜率
@property (nonatomic, copy) NSString * t;///<normal,rank
@property (nonatomic, copy) NSString * rank;

@end
