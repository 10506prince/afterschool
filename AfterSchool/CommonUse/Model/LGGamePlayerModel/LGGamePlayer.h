//
//  LGGamePlayer.h
//  AfterSchool
//
//  Created by lg on 15/10/29.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGGamePlayerInfo.h"

@interface LGGamePlayer : LGGamePlayerInfo

@property (nonatomic, assign) long long userId;///<用户id

@property (nonatomic, copy) NSString * recentMatches;///<最近比赛
@property (nonatomic, copy) NSString * summary;///<战况概览
@property (nonatomic, copy) NSString * useHeros;///<常用英雄

@end




