//
//  UserInfoModel.h
//  AfterSchool
//
//  Created by Teson Draw on 10/16/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LGGamePlayer.h"
#import "LGCommonAddress.h"
#import "LGDaKaInfo.h"
#import "LGDakaCertificate.h"

@interface UserInfoModel : NSObject

@property (nonatomic, copy) NSString *account;///<账户（手机号）
@property (nonatomic, copy) NSString *nickName;///<用户名
@property (nonatomic, copy) NSString *birthday;///<生日 时间戳格式
@property (nonatomic, copy) NSString *birthdayDateFormat;///<生日 yyyy/MM/dd格式
@property (nonatomic, copy) NSString *regTime;///<注册时间
@property (nonatomic, copy) NSString *sex;///<性别（0-女 1-男）
@property (nonatomic, copy) NSString *portraitURL;///<头像地址
@property (nonatomic, copy) NSString *constallation;///<星座

@property (nonatomic, assign) NSUInteger userType;///<用户类型
//@property (nonatomic, assign) NSUInteger mapUserId __deprecated_msg("弃用");///<用户地图信息存储id
@property (nonatomic, assign) NSUInteger creditLev;///<信用等级
@property (nonatomic, assign) NSUInteger authentication;///<身份认证 0->未认证 1->认证中 2-->已认证
@property (nonatomic, strong) NSArray <LGDakaCertificate *> * subUsersSimple;///<大咖审核
@property (nonatomic, assign) long long id;///<用户唯一id
@property (nonatomic, copy) NSString *info;///<个人简介
@property (nonatomic, copy) NSString *pushId;///<推送Id
@property (nonatomic, assign) NSUInteger type;///<类型 0是普通用户，1是大咖
@property (nonatomic, assign) BOOL unReceiveOrderState;///<大咖是否接受订单通知状态，yes：不接收 no：接收
@property (nonatomic, strong) LGGamePlayer * defaultPlayer;///<默认游戏角色

@property (nonatomic, strong) NSMutableArray <LGDaKaInfo *> * contacsListCache;///<好友列表缓存
@property (nonatomic, strong) NSMutableArray <LGDaKaInfo *> * blackListCache;///<黑名单缓存
@property (nonatomic, strong) NSArray <LGCommonAddress *> * commonAddresses;///<常用地址缓存
@property (nonatomic, strong) NSArray <LGGamePlayerInfo *> * gamePlayers;///<游戏账号列表缓存

- (instancetype)initWithData:(NSDictionary *)data;

@end
