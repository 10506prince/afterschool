//
//  UserInfoModel.m
//  AfterSchool
//
//  Created by Teson Draw on 10/16/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "UserInfoModel.h"
#import "NSDate+StringFormatter.h"
#import "NSDate+constellation.h"

@implementation UserInfoModel

- (instancetype)initWithData:(NSDictionary *)data {
    self = [super init];
    if (self) {
        
        _contacsListCache = [NSMutableArray array];
        _blackListCache = [NSMutableArray array];
//        _commonAddresses = [NSMutableArray array];
//        _gamePlayers = [NSMutableArray array];
        
        
        _account  = [NSString stringWithFormat:@"%@", data[@"account"]];
        _nickName = [NSString stringWithFormat:@"%@", data[@"nickName"]];
        _birthday = [NSString stringWithFormat:@"%@", data[@"birth"]];
        
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:[_birthday integerValue]/1000];
        _constallation = [NSDate getConstellationFromDate:date];
        
        _birthdayDateFormat = [[NSDate dateWithTimeIntervalSince1970:[_birthday integerValue]/1000] defaultString];
        _regTime  = [NSString stringWithFormat:@"%@", data[@"regTime"]];
        _sex      = [NSString stringWithFormat:@"%@", data[@"sex"]];
        
        _portraitURL = [NSString stringWithFormat:@"%@", data[@"icon"]];
        _userType = [data[@"type"] integerValue];
        
//        _mapUserId = [[data objectForKey:@"mapUserId"] integerValue];
        _creditLev = [[data objectForKey:@"creditLev"] integerValue];
        _authentication = [[data objectForKey:@"authState"] integerValue];
        
        self.id = [[data objectForKey:@"id"] longLongValue];
        
        if (data[@"info"]) {
            self.info = [NSString stringWithFormat:@"%@", data[@"info"]];
        } else {
            self.info = @"这个家伙很懒，什么都没有留下。";
        }
        
        self.pushId = [data objectForKey:@"pushId"];
        self.type = [[data objectForKey:@"type"] integerValue];
        self.unReceiveOrderState = [[data objectForKey:@"state"] boolValue];
        self.defaultPlayer = [LGGamePlayer objectWithKeyValues:data[@"defaultPlayer"]];
        self.subUsersSimple = [LGDakaCertificate objectArrayWithKeyValuesArray:data[@"subUsersSimple"]];
        //self.defaultPlayer = [LGGamePlayerInfo objectWithKeyValues:data[@"defaultPlayer"]];
    }
    return self;
}

- (NSString *)description {
    NSString *result = [NSString stringWithFormat:@"\naccount:%@\nnickName:%@\nbirthday:%@\nregTime:%@\nsex:%@\nportraitURL:%@\nuserType:%ld", _account, _nickName, _birthday, _regTime, _sex, _portraitURL, (unsigned long)_userType];
    return result;
}

@end
