//
//  LGGeocodeResult.h
//  AfterSchool
//
//  Created by lg on 15/11/24.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "LGServiceCommonModel.h"
#import "MJExtension.h"

@class LGAddressComponent,LGGeoCodeResult,LGReverseGeoCodeResult,LGAddressPoi,LGAddressPoiPoint;

///此类表示地址结果的层次化信息

@interface LGAddressComponent : NSObject

/// 街道门牌号码
@property (nonatomic, copy) NSString* street_number;
/// 街道名称
@property (nonatomic, copy) NSString* street;
/// 区县名称
@property (nonatomic, copy) NSString* district;
/// 城市名称
@property (nonatomic, copy) NSString* city;
/// 省份名称
@property (nonatomic, copy) NSString* province;
/// 国家名称
@property (nonatomic, copy) NSString* country;
/// 国家code
@property (nonatomic, assign) NSUInteger country_code;
/// 和当前坐标点的方向，当有门牌号的时候返回数据
@property (nonatomic, copy) NSString* direction;
/// 和当前坐标点的距离，当有门牌号的时候返回数据
@property (nonatomic, copy) NSString* distance;

@end



///反地址编码结果
@interface LGReverseGeoCodeResult : NSObject

///层次化地址信息
@property (nonatomic, strong) LGAddressComponent * addressComponent;
///
@property (nonatomic, assign) NSUInteger cityCode;
///地址名称
@property (nonatomic, copy) NSString* formatted_address;
///商圈名称
@property (nonatomic, copy) NSString* business;
///地址坐标
@property (nonatomic,readonly,getter=islocationValue) CLLocationCoordinate2D locationValue;
@property (nonatomic, strong) LGLocation * location;
///地址周边POI信息，成员类型为BMKPoiInfo
@property (nonatomic, strong) NSArray <LGAddressPoi *>* pois;
@property (nonatomic, strong) NSArray <LGAddressPoi *>* poiRegions;
/// 当前位置结合POI的语义化结果描述。
@property (nonatomic, copy) NSString * sematic_description;

@end

///地址编码结果
@interface LGGeoCodeResult : NSObject

@property (nonatomic, assign) NSUInteger confidence;
@property (nonatomic, copy) NSString * level;
@property (nonatomic, assign) NSUInteger precise;

///地理编码位置
@property (nonatomic,readonly,getter=islocationValue) CLLocationCoordinate2D locationValue;
@property (nonatomic, strong) LGLocation * location;
///地理编码地址
@property (nonatomic, strong) NSString* address;

@end




@interface LGAddressPoi : NSObject

@property (nonatomic, copy) NSString *zip;

@property (nonatomic, copy) NSString *uid;

@property (nonatomic, copy) NSString *distance;

@property (nonatomic, strong) LGAddressPoiPoint *point;

@property (nonatomic, copy) NSString *addr;

@property (nonatomic, copy) NSString *poiType;

@property (nonatomic, copy) NSString *tag;

@property (nonatomic, copy) NSString *direction;

@property (nonatomic, copy) NSString *cp;

@property (nonatomic, copy) NSString *name;

@property (nonatomic, copy) NSString *tel;

@end

@interface LGAddressPoiPoint : NSObject

@property (nonatomic, assign) double x;///<经度
@property (nonatomic, assign) double y;///<纬度

@end

