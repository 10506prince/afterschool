//
//  LGGeocodeResult.m
//  AfterSchool
//
//  Created by lg on 15/11/24.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGGeocodeResult.h"

@implementation LGAddressComponent
@end

@implementation LGReverseGeoCodeResult
+ (NSDictionary *)objectClassInArray{
    return @{@"pois" : [LGAddressPoi class],@"poiRegions" : [LGAddressPoi class]};
}

- (CLLocationCoordinate2D)islocationValue {
    return CLLocationCoordinate2DMake(_location.lat, _location.lng);
}
@end

@implementation LGGeoCodeResult
- (CLLocationCoordinate2D)islocationValue {
    return CLLocationCoordinate2DMake(_location.lat, _location.lng);
}

@end



@implementation LGAddressPoi
@end

@implementation LGAddressPoiPoint
@end


