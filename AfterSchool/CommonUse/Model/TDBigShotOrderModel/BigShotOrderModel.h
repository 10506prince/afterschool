//
//  BigShotOrderModel.h
//  AfterSchool
//
//  Created by Teson Draw on 11/7/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BigShotOrderModel : NSObject

@property (nonatomic, assign) NSUInteger type;

@property (nonatomic, strong) NSString *orderID;
@property (nonatomic, strong) NSString *senderAccount;
@property (nonatomic, strong) NSString *senderName;

@property (nonatomic, assign) float money;
@property (nonatomic, assign) float tokenMoney;

@property (nonatomic, strong) NSString *planStartTime;
@property (nonatomic, strong) NSDate *timeoutTime;
@property (nonatomic, assign) NSUInteger planKeepTime;//unit：hour

@property (nonatomic, strong) NSString *address;

@property (nonatomic, strong) NSString *portraitURL;

@property (nonatomic, assign) long long startTime;
@property (nonatomic, assign) long long endTime;
@property (nonatomic, strong) NSString *formatEndTime;

- (instancetype)initWithDictionary:(NSDictionary *)dictionary;

@end
