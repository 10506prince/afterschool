//
//  BigShotOrderModel.m
//  AfterSchool
//
//  Created by Teson Draw on 11/7/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "BigShotOrderModel.h"
#import "NSDate+StringFormatter.h"


@implementation BigShotOrderModel

- (instancetype)initWithDictionary:(NSDictionary *)dictionary {
    self = [super init];
    
    if (self) {
        _type = [dictionary[@"type"] integerValue];
        _orderID = [NSString stringWithFormat:@"%@", dictionary[@"orderId"]];
        _senderAccount = [NSString stringWithFormat:@"%@", dictionary[@"senderUserName"]];
        _senderName = [NSString stringWithFormat:@"%@", dictionary[@"senderNickName"]];
        
        if (dictionary[@"money"]) {
            _money = [dictionary[@"money"] floatValue];
        }
        
        if (dictionary[@"tokenMoney"]) {
            _tokenMoney = [dictionary[@"tokenMoney"] floatValue];
        }
        
        if (dictionary[@"planStartTime"]) {
            NSTimeInterval timeInterval = [dictionary[@"planStartTime"] longLongValue];
            NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeInterval/1000];
            _planStartTime = [date formatStringWithHyphen];
        }

        if (dictionary[@"timeOut"]) {
             _timeoutTime = [NSDate dateWithTimeIntervalSince1970:[dictionary[@"timeOut"] longValue]];
        }
       
        if (dictionary[@"planKeepTime"]) {
            _planKeepTime = [dictionary[@"planKeepTime"] integerValue];
        }
        
        if (dictionary[@"address"]) {
            _address = [NSString stringWithFormat:@"%@", dictionary[@"address"]];
        }
        
        if (dictionary[@"senderIcon"]) {
            _portraitURL = [NSString stringWithFormat:@"%@", dictionary[@"senderIcon"]];
        }
        
        if (dictionary[@"startTime"]) {
            _startTime = [dictionary[@"startTime"] longLongValue];
        }
        
        if (dictionary[@"endTime"]) {
            _endTime = [dictionary[@"endTime"] longLongValue];
            _formatEndTime = [[NSDate dateWithTimeIntervalSince1970:_endTime/1000] chineseDatetime];
        }
    }
    return self;
}

- (NSString *)description {
    NSString *result = [NSString stringWithFormat:@"\nAccount:%@\nName:%@\nOrderId:%@\n", _senderAccount, _senderName, _orderID];
    
    return result;
}

@end
