//
//  LGCommonAddress.h
//  AfterSchool
//
//  Created by lg on 15/12/2.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface LGCommonAddress : NSObject

@property (nonatomic, assign) NSUInteger id;
@property (nonatomic, copy) NSString * name;
@property (nonatomic, copy) NSString * address;
@property (nonatomic, assign) double distance;
@property (nonatomic, copy) NSString * position;///<经纬度:(经度，纬度)
@property (nonatomic, readonly, assign) CLLocationCoordinate2D pt;

- (instancetype)initWithCommonAddress:(LGCommonAddress *)commonAddress;

@end
