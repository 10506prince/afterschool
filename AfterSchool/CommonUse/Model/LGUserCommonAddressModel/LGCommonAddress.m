//
//  LGCommonAddress.m
//  AfterSchool
//
//  Created by lg on 15/12/2.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGCommonAddress.h"

@implementation LGCommonAddress
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.name = @"常用地址";
    }
    return self;
}

- (instancetype)initWithCommonAddress:(LGCommonAddress *)commonAddress
{
    self = [super init];
    if (self) {
        self.id = commonAddress.id;
        self.name = commonAddress.name;
        self.address = commonAddress.address;
    }
    return self;
}

- (CLLocationCoordinate2D)pt {
    NSArray * array = [self.position componentsSeparatedByString:@","];
    if (array.count == 2) {
        CLLocationDegrees lat = [array[1] doubleValue];
        CLLocationDegrees lng = [array[0] doubleValue];
        
        return CLLocationCoordinate2DMake(lat, lng);
    }
    return CLLocationCoordinate2DMake(0, 0);
}
@end
