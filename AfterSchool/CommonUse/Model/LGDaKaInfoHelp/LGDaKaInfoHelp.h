//
//  LGDaKaInfoHelp.h
//  AfterSchool
//
//  Created by lg on 15/10/19.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MJExtension.h"
#import "LGGamePlayer.h"

@interface LGDaKaInfoHelp : NSObject

@property (nonatomic, copy) NSString *account;///<账户
@property (nonatomic, assign) long long birth;///<生日
@property (nonatomic, assign) NSUInteger creditLev;///<信用积分等级
@property (nonatomic, copy) NSString *icon;///<用户头像地址
@property (nonatomic, assign) long long id;///<用户唯一id
@property (nonatomic, copy) NSString *info;///<个人简介
@property (nonatomic, copy) NSString *nickName;///<昵称
@property (nonatomic, copy) NSString *pushId;///<推送Id
@property (nonatomic, assign) long long regTime;///<注册时间
@property (nonatomic, assign) NSUInteger sex;///<性别（0-女 1-男）
@property (nonatomic, assign) NSUInteger type;///<类型
@property (nonatomic, assign) float score;///<评价

@property (nonatomic, assign) NSUInteger money;
@property (nonatomic, assign) NSUInteger tokenMoney;

@property (nonatomic, assign) NSUInteger playTotalMoney;
@property (nonatomic, assign) NSUInteger playTotalCount;
@property (nonatomic, assign) NSUInteger playTotalTime;
@property (nonatomic, assign) NSUInteger priceOnline;///<每小时陪玩价格（线上）
@property (nonatomic, assign) NSUInteger priceOffline;///<每小时陪玩价格（线下）

@property (nonatomic, assign) NSUInteger credit;

@property (nonatomic, strong) LGGamePlayerInfo * defaultPlayer;///<默认游戏角色

@property (nonatomic, strong) NSArray<LGGamePlayerInfo *> *playerInfo;

@end


