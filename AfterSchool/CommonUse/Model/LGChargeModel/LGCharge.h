//
//  LGCharge.h
//  AfterSchool
//
//  Created by lg on 15/12/18.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <Foundation/Foundation.h>

static NSString * alipayUrl = @"alipay19810104wl";
static NSString * wxpayUrl = @"wx9fdbe0ccbadfe82c";

@interface LGCharge : NSObject

@property (nonatomic, assign) NSUInteger amount;///<金额(分)
@property (nonatomic, copy) NSString * subject;///<
@property (nonatomic, copy) NSString * body;///<
@property (nonatomic, copy) NSString * channel;///<支付渠道：alipay,wx

@end
