//
//  LGDakaDetail.h
//  AfterSchool
//
//  Created by lg on 15/11/11.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGDaKaInfo.h"

@interface LGDakaDetail : LGDaKaInfo

@property (nonatomic, strong) NSArray<LGGamePlayerInfo *> * gamePlayerList;

+ (instancetype)lgdakaDetailWith:(LGDaKaInfoHelp *)dakaInfoHelp;

@end
