//
//  LGDakaCertificate.h
//  AfterSchool
//
//  Created by lg on 16/1/18.
//  Copyright © 2016年 Teson Draw. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LGDakaCertificate : NSObject

@property (nonatomic, assign) long long id;
@property (nonatomic, assign) NSInteger gameId;
@property (nonatomic, copy) NSString * city;
@property (nonatomic, assign) float priceOnline;
@property (nonatomic, assign) float priceOffline;
@property (nonatomic, assign) NSInteger certifyState;
@property (nonatomic, assign) NSInteger type;

@end
