//
//  LGDaKaInfo.h
//  AfterSchool
//
//  Created by lg on 15/10/10.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MJExtension.h"
#import "LGDaKaInfoHelp.h"

@interface LGDaKaInfo : NSObject

//头像
//名称
//信用等级
//距离
//性别
//年龄
//星座
//个性签名


//游戏段位
//游戏大区
//游戏局数
//战斗力

@property (nonatomic, copy) NSString * account;///<账号

//@property (nonatomic, copy) NSString * headImage;       ///<头像
@property (nonatomic, copy) NSString * headImageUrl;///<头像地址
@property (nonatomic, copy) NSString * nickName;         ///<用户昵称
@property (nonatomic, copy) NSString * nickNameAbb;         ///<用户昵称中文首字母
@property (nonatomic, assign) NSUInteger creditRating;  ///<信用等级
@property (nonatomic, assign) float score;              ///<评价
@property (nonatomic, assign) double distance;          ///<距离(m)
@property (nonatomic, assign) NSUInteger sex;           ///<性别(1:男 0:女)
@property (nonatomic, assign) NSUInteger age;           ///<年龄
@property (nonatomic, copy) NSString * constellation;   ///<星座
@property (nonatomic, copy) NSString * signature;       ///<个性签名

@property (nonatomic, copy) NSString * gameRank;        ///<游戏段位
@property (nonatomic, copy) NSString * gameServer;      ///<游戏大区
@property (nonatomic, assign) NSUInteger gameNumber;    ///<游戏局数
@property (nonatomic, assign) NSUInteger gameFighting;    ///<战斗力

@property (nonatomic, assign) NSUInteger playTotalCount;    ///<陪玩总局数
@property (nonatomic, assign) NSUInteger playTotalMoney;    ///<陪玩总打赏
@property (nonatomic, assign) NSUInteger playTotalTime;    ///<陪玩总时长
@property (nonatomic, assign) NSUInteger priceOnline;      ///<每小时陪玩价格（线上）
@property (nonatomic, assign) NSUInteger priceOffline;    ///<每小时陪玩价格（线下）


//@property (nonatomic, copy) NSString * gameLevel;       ///<游戏角色等级
//@property (nonatomic, copy) NSString * gameWons;        ///<胜场数
//@property (nonatomic, copy) NSString * gameLosts;       ///<负场数

@property (nonatomic, strong) LGGamePlayerInfo * defaultGamePlayer;

- (instancetype)initWithDakaInfoHelp:(LGDaKaInfoHelp *)dakaInfoHelp;
+ (instancetype)dakaInfoWithDakaInfoHelp:(LGDaKaInfoHelp *)dakaInfoHelp;
+ (NSArray<LGDaKaInfo *> *)dakaInofArrayWithDakaInfoHelpArray:(NSArray<LGDaKaInfoHelp *> *)array;


/**
 *  @brief 返回对应好友或黑名单字典
 *
 *  @return 好友或黑名单
 */
- (NSDictionary *)friendOrBlack;

/**
 *  @brief 好友或黑名单字典数组转换为用户模型数组
 *
 *  @param array 好友或黑名单字典数组
 *
 *  @return 用户模型数组
 */
+ (NSArray<LGDaKaInfo *> *)dakaInofArrayWithFriendOrBlackDicArray:(NSArray<NSDictionary *> *)array;

@end

@interface NSDictionary (LGDakaInfo)
+ (NSArray <NSDictionary *>*)friendOrBlackListFromUserList:(NSArray <LGDaKaInfo *>*)userList;
@end

