//
//  LGDaKaInfo.m
//  AfterSchool
//
//  Created by lg on 15/10/10.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGDaKaInfo.h"
#import "NSDate+constellation.h"
#import "NSString+Null.h"
#import "PinYinForObjc.h"

@implementation LGDaKaInfo

- (NSString *)description {
    NSString * str = [NSString stringWithFormat:@"%@,%@,%@,%@,%@,%ld,%ld,", _headImageUrl,_nickName,_constellation,_gameRank,_gameServer,(unsigned long)_gameNumber,(unsigned long)_gameFighting];
    return str;
}

+ (instancetype)dakaInfoWithDakaInfoHelp:(LGDaKaInfoHelp *)dakaInfoHelp {
    LGDaKaInfo * selfObject = [[LGDaKaInfo alloc] initWithDakaInfoHelp:dakaInfoHelp];
    return selfObject;
}

- (instancetype)initWithDakaInfoHelp:(LGDaKaInfoHelp *)dakaInfoHelp
{
    self = [super init];
    if (self) {
        self.headImageUrl = dakaInfoHelp.icon;
        //self.headImage = dakaInfoHelp.icon;
        self.nickName = dakaInfoHelp.nickName;
        self.creditRating = dakaInfoHelp.creditLev;
        self.sex = dakaInfoHelp.sex;
        self.score = dakaInfoHelp.score;

        NSDate * birthday = [NSDate dateWithTimeIntervalSince1970:dakaInfoHelp.birth/1000];
        
        self.age = [NSDate getAgeFromBirthday:birthday];
        self.constellation = [NSDate getConstellationFromDate:birthday];
        self.signature = dakaInfoHelp.info;

        if (dakaInfoHelp.defaultPlayer.grade.length == 0) {
            self.gameRank = @"无";
        } else {
            self.gameRank = dakaInfoHelp.defaultPlayer.grade;
        }
        
        self.gameServer = dakaInfoHelp.defaultPlayer.serverName;
        self.gameNumber = dakaInfoHelp.defaultPlayer.totalRounds;
        self.gameFighting = dakaInfoHelp.defaultPlayer.attack;
        self.account = dakaInfoHelp.account;
        
        self.playTotalCount = dakaInfoHelp.playTotalCount;
        self.playTotalMoney = dakaInfoHelp.playTotalMoney;
        self.playTotalTime = dakaInfoHelp.playTotalTime;
        self.priceOnline = dakaInfoHelp.priceOnline;
        self.priceOffline = dakaInfoHelp.priceOffline;
        
        self.defaultGamePlayer = dakaInfoHelp.defaultPlayer;
    }
    return self;
}

+ (NSArray<LGDaKaInfo *> *)dakaInofArrayWithDakaInfoHelpArray:(NSArray<LGDaKaInfoHelp *> *)array {
    NSMutableArray<LGDaKaInfo *> * resultArray = [NSMutableArray array];
    for (id obj in array) {
        if ([obj isKindOfClass:[LGDaKaInfoHelp class]]) {
            LGDaKaInfo * dakaInfo = [LGDaKaInfo dakaInfoWithDakaInfoHelp:(LGDaKaInfoHelp *)obj];
            [resultArray addObject:dakaInfo];
        }
    }
    
    return resultArray;
}

+ (NSArray<LGDaKaInfo *> *)dakaInofArrayWithFriendOrBlackDicArray:(NSArray<NSDictionary *> *)array {
    NSMutableArray<LGDaKaInfo *> * resultArray = [NSMutableArray array];
    for (NSDictionary * dic in array) {
        LGDaKaInfo * user = [[LGDaKaInfo alloc] init];
        user.account = [dic objectForKey:@"userName"];
        user.nickName = [dic objectForKey:@"nickName"];
        user.headImageUrl = [dic objectForKey:@"icon"];
        
        [resultArray addObject:user];
    }
    return resultArray;
}

- (NSDictionary *)friendOrBlack {
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    [dic setObject:self.account.length > 0 ? self.account : @"" forKey:@"userName"];
    [dic setObject:self.nickName.length > 0 ? self.nickName : @"" forKey:@"nickName"];
    [dic setObject:self.headImageUrl.length > 0 ? self.headImageUrl : @"" forKey:@"icon"];
    [dic setObject:self.nickName.length > 0 ? [PinYinForObjc chineseToPinYinHead:self.nickName] : @"" forKey:@"nickNameAbb"];
    return dic;
}

@end

@implementation NSDictionary (LGDakaInfo)

+ (NSArray <NSDictionary *>*)friendOrBlackListFromUserList:(NSArray <LGDaKaInfo *>*)userList {
    NSMutableArray * array = [NSMutableArray array];
    for (LGDaKaInfo * user in userList) {
        NSDictionary * dic = user.friendOrBlack;
        [array addObject:dic];
    }
    return array;
}

@end
