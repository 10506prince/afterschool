//
//  LGDakaDetail.m
//  AfterSchool
//
//  Created by lg on 15/11/11.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGDakaDetail.h"

@implementation LGDakaDetail

+ (instancetype)lgdakaDetailWith:(LGDaKaInfoHelp *)dakaInfoHelp {
    LGDakaDetail * dakaDetail = [[LGDakaDetail alloc] initWithDakaInfoHelp:dakaInfoHelp];
    return dakaDetail;
}

- (instancetype)initWithDakaInfoHelp:(LGDaKaInfoHelp *)dakaInfoHelp {
    self = [super initWithDakaInfoHelp:dakaInfoHelp];
    if (self) {
        self.gamePlayerList = dakaInfoHelp.playerInfo;
    }
    return self;
}
@end
