//
//  NotificationName.m
//  AfterSchool
//
//  Created by Teson Draw on 12/5/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//


#import "NotificationName.h"

NSString *const TDUpdateFriends          = @"TDUpdateFriends";//更新好友
NSString *const TDUpdateBlacklist        = @"TDUpdateBlacklist";//更新黑名单

//NSString *const TDChatWithBigShot        = @"TDChatWithBigShot";//与大咖聊天
NSString *const TDBigShotGetOrder        = @"TDBigShotGetOrder";//大咖收到订单
NSString *const TDBigShotAcceptOrder     = @"TDBigShotAcceptOrder";//大咖接受订单
NSString *const TDBigShotRejectOrder     = @"TDBigShotRejectOrder";//大咖拒绝订单
NSString *const TDChargeSuccess          = @"TDChargeSuccess";//充值成功
NSString *const TDRemoveMessageSearchBar = @"TDRemoveMessageSearchBar";//删除消息界面搜索框
NSString *const TDDidSelectChatObject    = @"TDDidSelectChatObject";//从搜索列表选择一个聊天对象
NSString *const TDWithdrawCashSuccess    = @"TDWithdrawCashSuccess";//提现成功
NSString *const TDEvaluatedFinishedOrder = @"TDWithdrawCashSuccess";//评价已完成的订单

NSString *const TDDeleteChatRecords      = @"TDDeleteChatRecord";   //删除聊天记录
NSString *const TDDeleteChatContacts     = @"TDDeleteContacts";     //删除聊天联系人

NSString *const TDUserCancelOrderSuccess    = @"TDUserCancelOrderSuccess";//用户成功取消订单
NSString *const TDUserCompleteOrderSuccess  = @"TDUserCompleteOrderSuccess";//用户成功结束订单

NSString *const TDBigShotStartOrderSuccess    = @"TDStartOrderSuccess";//大咖成功取消订单
NSString *const TDBigShotCancelOrderSuccess   = @"TDBigShotCancelOrderSuccess";//大咖成功取消订单
NSString *const TDBigShotCompleteOrderSuccess = @"TDBigShotCompleteOrderSuccess";//大咖成功取消订单

NSString *const TDIdentifierAuthenticationSuccess = @"TDIdentifierAuthenticationSuccess";//身份认证成功

#pragma mark - 定位服务通知
NSString *const LGLocationStart      = @"LGLocationStart";
NSString *const LGLocationStop       = @"LGLocationStop";
NSString *const LGLocationFail       = @"LGLocationFail";
NSString *const LGLocationUpdate     = @"LGLocationUpdate";
NSString *const LGLocationUpdateHead = @"LGLocationUpdateHead";

#pragma mark - 设置城市服务
NSString *const LGSetCityName = @"LGSetCityName";

NSString *const LGSetUserIcon = @"LGSetUserIcon";