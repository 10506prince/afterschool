//
//  NotificationName.h
//  AfterSchool
//
//  Created by Teson Draw on 11/25/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifndef NotificationName_h
#define NotificationName_h

extern NSString *const TDUpdateFriends;         //更新好友
extern NSString *const TDUpdateBlacklist;       //更新黑名单
//extern NSString *const TDChatWithBigShot;       //与大咖聊天
extern NSString *const TDBigShotGetOrder;       //大咖收到订单
extern NSString *const TDBigShotAcceptOrder;    //大咖接受订单
extern NSString *const TDBigShotRejectOrder;    //大咖接受订单
extern NSString *const TDChargeSuccess;         //充值成功
extern NSString *const TDRemoveMessageSearchBar;//删除消息界面搜索框
extern NSString *const TDDidSelectChatObject;   //从搜索列表选择一个聊天对象
extern NSString *const TDWithdrawCashSuccess;   //提现成功
extern NSString *const TDEvaluatedFinishedOrder;//评价已完成的订单

extern NSString *const TDDeleteChatRecords;     //删除聊天记录
extern NSString *const TDDeleteChatContacts;    //删除联系人

extern NSString *const TDUserCancelOrderSuccess;//用户成功取消订单
extern NSString *const TDUserCompleteOrderSuccess;//用户成功结束订单

extern NSString *const TDBigShotStartOrderSuccess;//大咖成功取消订单
extern NSString *const TDBigShotCancelOrderSuccess;//大咖成功取消订单
extern NSString *const TDBigShotCompleteOrderSuccess;//大咖成功取消订单

extern NSString *const TDIdentifierAuthenticationSuccess;//身份认证成功

#pragma mark - 定位服务通知
extern NSString *const LGLocationStart;// = @"LGLocationStart";
extern NSString *const LGLocationStop;// = @"LGLocationStop";
extern NSString *const LGLocationFail;// = @"LGLocationFail";
extern NSString *const LGLocationUpdate;// = @"LGLocationUpdate";
extern NSString *const LGLocationUpdateHead;// = @"LGLocationUpdateHead";

#pragma mark - 设置城市通知
extern NSString *const LGSetCityName;// = @"LGSetCityName";

#pragma mark - 设置头像通知
extern NSString *const LGSetUserIcon;
#endif /* NotificationName_h */
