//
//  FontSizeType.h
//  AfterSchool
//
//  Created by Teson Draw on 12/3/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#ifndef FontSizeType_h
#define FontSizeType_h

typedef enum : NSUInteger {
    FontSizeSmall = 0,  ///<稍小
    FontSizeStandard,   ///<标准
    FontSizeBig,        ///<大号
    FontSizeBigger,     ///<稍大
    FontSizeLarge,      ///<巨大
    FontSizeVeryLarge   ///<超大
} FontSizeType;


#endif /* FontSizeType_h */
