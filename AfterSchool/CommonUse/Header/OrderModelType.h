//
//  OrderModelType.h
//  AfterSchool
//
//  Created by Teson Draw on 11/30/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#ifndef OrderModelType_h
#define OrderModelType_h

typedef enum : NSUInteger {
    OrderModelTypeMaster = 0,       ///<指定大咖类型
    OrderModelTypeScene,        ///<情景模式类型
} OrderModelType;

#endif /* OrderModelType_h */
