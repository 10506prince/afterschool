//
//  UIImage+ScaleImage.h
//  AfterSchool
//
//  Created by Teson Draw on 12/30/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (ScaleImage)

+ (UIImage *)scaleImage:(UIImage *)image toSize:(CGSize)size;

@end
