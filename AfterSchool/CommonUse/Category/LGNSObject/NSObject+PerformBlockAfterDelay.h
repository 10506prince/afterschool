//
//  NSObject+PerformBlockAfterDelay.h
//  qisuedu
//
//  Created by apple on 15/3/24.
//  Copyright (c) 2015年 apple. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (PerformBlockAfterDelay)

- (void)performBlock:(void (^)(void))block afterDelay:(NSTimeInterval)delay;

//- (void)runBlockAfterDelay:(NSTimeInterval)delay block:(void (^)(void))block;

@end
