//
//  UITableView+NibCell.h
//  qisuedu
//
//  Created by lg on 15/8/20.
//  Copyright (c) 2015年 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableView (NibCell)
//- (void)registerClass:(Class)cellClass forCellReuseIdentifier:(NSString *)identifier;
//- (void)registerNib:(UINib *)nib forCellReuseIdentifier:(NSString *)identifier;
//- (void)registerClass:(Class)aClass forHeaderFooterViewReuseIdentifier:(NSString *)identifier;
//- (void)registerNib:(UINib *)nib forHeaderFooterViewReuseIdentifier:(NSString *)identifier;

/**
 *  注册一个nib类型的cell,Identifier问cell的类名
 *
 *  @param cellClass cell类
 */
- (void)registerNibCellWithClass:(Class)cellClass;
- (id)dequeueReusableCellWithClass:(Class)cellClass;

- (void)registerNibHeaderFooterViewWithClass:(Class)HeaderFooterViewClass;
- (id)dequeueReusableHeaderFooterViewWithClass:(Class)HeaderFooterViewClass;

@end

@interface UICollectionView (NibCell)
//- (void)registerClass:(Class)cellClass forCellReuseIdentifier:(NSString *)identifier;
//- (void)registerNib:(UINib *)nib forCellReuseIdentifier:(NSString *)identifier;
//- (void)registerClass:(Class)aClass forHeaderFooterViewReuseIdentifier:(NSString *)identifier;
//- (void)registerNib:(UINib *)nib forHeaderFooterViewReuseIdentifier:(NSString *)identifier;

/**
 *  注册一个nib类型的cell,Identifier问cell的类名
 *
 *  @param cellClass cell类
 */
- (void)registerNibCellWithClass:(Class)cellClass;
//- (id)dequeueReusableCellWithClass:(Class)cellClass;
- (id)dequeueReusableCellWithClass:(Class)cellClass forIndexPath:(NSIndexPath *)indexPath;

@end
