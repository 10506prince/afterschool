//
//  UITableView+NibCell.m
//  qisuedu
//
//  Created by lg on 15/8/20.
//  Copyright (c) 2015年 apple. All rights reserved.
//

#import "UITableView+NibCell.h"

@implementation UITableView (NibCell)
- (void)registerNibCellWithClass:(Class)cellClass
{
    NSString * cellName = NSStringFromClass([cellClass class]);
    [self registerNib:[UINib nibWithNibName:cellName bundle:[NSBundle bundleWithIdentifier:cellName]] forCellReuseIdentifier:cellName];
}

- (id)dequeueReusableCellWithClass:(Class)cellClass
{
    return [self dequeueReusableCellWithIdentifier:NSStringFromClass([cellClass class])];
}

- (void)registerNibHeaderFooterViewWithClass:(Class)HeaderFooterViewClass
{
    NSString * HeaderFooterViewName = NSStringFromClass([HeaderFooterViewClass class]);
    [self registerNib:[UINib nibWithNibName:HeaderFooterViewName bundle:[NSBundle bundleWithIdentifier:HeaderFooterViewName]] forHeaderFooterViewReuseIdentifier:HeaderFooterViewName];
}

- (id)dequeueReusableHeaderFooterViewWithClass:(Class)HeaderFooterViewClass
{
    return [self dequeueReusableHeaderFooterViewWithIdentifier:NSStringFromClass([HeaderFooterViewClass class])];
}

@end

@implementation UICollectionView (NibCell)
- (void)registerNibCellWithClass:(Class)cellClass
{
    NSString * cellName = NSStringFromClass([cellClass class]);
//    [self registerNib:[UINib nibWithNibName:cellName bundle:[NSBundle bundleWithIdentifier:cellName]] forCellReuseIdentifier:cellName];
    [self registerNib:[UINib nibWithNibName:cellName bundle:[NSBundle bundleWithIdentifier:cellName]] forCellWithReuseIdentifier:cellName];
}

//- (id)dequeueReusableCellWithClass:(Class)cellClass
//{
////    return [self dequeueReusableCellWithIdentifier:NSStringFromClass([cellClass class])];
////    return [self deque:NSStringFromClass([cellClass class])];
//    return nil;
//}

- (id)dequeueReusableCellWithClass:(Class)cellClass forIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell * cell = [self dequeueReusableCellWithReuseIdentifier:NSStringFromClass([cellClass class]) forIndexPath:indexPath];
    return cell;
}

@end
