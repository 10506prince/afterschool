//
//  UIScrollView+ScreenShot.h
//  AfterSchool
//
//  Created by lg on 16/1/5.
//  Copyright © 2016年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIScrollView (ScreenShot)

- (UIImage *)screenShot;

@end
