//
//  UIColor+RGB.h
//  baiduMap
//
//  Created by lg on 15/9/28.
//  Copyright © 2015年 李刚. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (RGB)

/**
 *  @brief  通过16进制RGB设定颜色
 *
 *  @param hexColor 16进制RGB颜色
 *
 *  @return 颜色
 */
+ (UIColor *)getColor:(NSString *)hexColor;

/**
 *  @brief  通过10进制RGB设定颜色
 *
 *  @param decColor 10进制RGB颜色（000123444）
 *
 *  @return 颜色
 */
+ (UIColor *)getColorDec:(NSString *)decColor;

+ (NSString *)getHexStringByColor:(UIColor *)originColor;


+ (UIColor *)lgDarkColor;
+ (UIColor *)lgLightColor;

@end
