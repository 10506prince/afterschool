//
//  UIColor+RGB.m
//  baiduMap
//
//  Created by lg on 15/9/28.
//  Copyright © 2015年 李刚. All rights reserved.
//

#import "UIColor+RGB.h"

@implementation UIColor (RGB)

+ (UIColor *)getColor:(NSString *)hexColor
{
    unsigned int red,green,blue;
    NSRange range;
    range.length = 2;
    
    range.location = 0;
    [[NSScanner scannerWithString:[hexColor substringWithRange:range]] scanHexInt:&red];
    
    range.location = 2;
    [[NSScanner scannerWithString:[hexColor substringWithRange:range]] scanHexInt:&green];
    
    range.location = 4;
    [[NSScanner scannerWithString:[hexColor substringWithRange:range]] scanHexInt:&blue];
    
    return [UIColor colorWithRed:(float)(red/255.0f) green:(float)(green / 255.0f) blue:(float)(blue / 255.0f) alpha:1.0f];
}

+ (UIColor *)getColorDec:(NSString *)decColor
{
    NSInteger red,green,blue;
    NSRange range;
    range.length = 3;
    
    range.location = 0;
    [[NSScanner scannerWithString:[decColor substringWithRange:range]] scanInteger:&red];
    
    range.location = 3;
    [[NSScanner scannerWithString:[decColor substringWithRange:range]] scanInteger:&green];
    
    range.location = 6;
    [[NSScanner scannerWithString:[decColor substringWithRange:range]] scanInteger:&blue];
    
    return [UIColor colorWithRed:(float)(red/255.0f) green:(float)(green / 255.0f) blue:(float)(blue / 255.0f) alpha:1.0f];
}

/**
 *  获取UIColor对象的rgb值。
 *
 *  @param originColor
 *
 *  @return
 */
+ (NSString *)getHexStringByColor:(UIColor *)originColor
{
    NSDictionary *colorDic
    = [self getRGBDictionaryByColor:originColor];
    int r = [colorDic[@"R"] floatValue] * 255;
    int g = [colorDic[@"G"] floatValue] * 255;
    int b = [colorDic[@"B"] floatValue] * 255;
    NSString *red = [NSString stringWithFormat:@"%02x", r];
    NSString *green = [NSString stringWithFormat:@"%02x", g];
    NSString *blue = [NSString stringWithFormat:@"%02x", b];
    
    return [NSString stringWithFormat:@"#%@%@%@", red, green, blue];
}

+ (NSDictionary *)getRGBDictionaryByColor:(UIColor *)originColor
{
    CGFloat r=0,g=0,b=0,a=0;
    if ([self respondsToSelector:@selector(getRed:green:blue:alpha:)]) {
        [originColor getRed:&r green:&g blue:&b alpha:&a];
    }
    else {
        const CGFloat *components = CGColorGetComponents(originColor.CGColor);
        r = components[0];
        g = components[1];
        b = components[2];
        a = components[3];
    }
    
    return @{@"R":@(r),
             @"G":@(g),
             @"B":@(b),
             @"A":@(a)};
}

+ (UIColor *)lgDarkColor {
    return [UIColor getColorDec:@"070070070"];
}

+ (UIColor *)lgLightColor {
    return [UIColor groupTableViewBackgroundColor];
}
@end
