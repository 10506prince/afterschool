//
//  UIColor+RandomColor.m
//  qisuedu
//
//  Created by apple on 15/6/19.
//  Copyright (c) 2015年 apple. All rights reserved.
//

#import "UIColor+RandomColor.h"

@implementation UIColor (RandomColor)

+ (UIColor *)randomColor
{
    CGFloat random = arc4random()%255 / 255.0;
    
    UIColor * randomColor = [UIColor colorWithRed:random green:random blue:random alpha:1];
    
    NSLog(@"%@", randomColor);
    
    return randomColor;
}

+ (UIColor *)randomColorOne
{
    CGFloat redRandom = arc4random()%255 / 255.0;
    CGFloat greenRandom = arc4random()%255 / 255.0;
    CGFloat blueRandom = arc4random()%255 / 255.0;
    
    UIColor * randomColor = [UIColor colorWithRed:redRandom green:greenRandom blue:blueRandom alpha:1];
    
    return randomColor;
}

@end
