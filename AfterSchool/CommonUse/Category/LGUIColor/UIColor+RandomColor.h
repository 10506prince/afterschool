//
//  UIColor+RandomColor.h
//  qisuedu
//
//  Created by apple on 15/6/19.
//  Copyright (c) 2015年 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

// 颜色
#define LgColor(c,alp) [UIColor colorWithRed:(c)/255.0 green:(c)/255.0 blue:(c)/255.0 alpha:alp]
#define LgColorRGB(r,g,b,alp) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:alp]

//RGB的颜色转换
#define LGColorFromHexRGB(hexRGBValue,alp) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:(float)(alp)]

@interface UIColor (RandomColor)

+ (UIColor *)randomColor;
+ (UIColor *)randomColorOne;

@end
