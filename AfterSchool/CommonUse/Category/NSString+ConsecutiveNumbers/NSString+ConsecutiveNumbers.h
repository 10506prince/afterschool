//
//  NSString+ConsecutiveNumbers.h
//  StringTest
//
//  Created by Teson Draw on 1/11/16.
//  Copyright © 2016 Teson Draw. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (ConsecutiveNumbers)

- (NSString *)consecutiveNumbers;

@end
