//
//  NSString+ConsecutiveNumbers.m
//  StringTest
//
//  Created by Teson Draw on 1/11/16.
//  Copyright © 2016 Teson Draw. All rights reserved.
//

#import "NSString+ConsecutiveNumbers.h"

@implementation NSString (ConsecutiveNumbers)


- (NSString *)consecutiveNumbers {
    
    if (self == nil || self.length == 0) {
        return nil;
    }
    
    NSString *character;
    NSString *result = @"";
    
    BOOL found = NO;
    int foundIndex = 0;
    
    for (int i = 0; i < self.length; i++) {
        character = [self substringWithRange:NSMakeRange(i, 1)];
        
        if ([character isEqual: @"0"]|
            [character isEqual: @"1"]|
            [character isEqual: @"2"]|
            [character isEqual: @"3"]|
            [character isEqual: @"4"]|
            [character isEqual: @"5"]|
            [character isEqual: @"6"]|
            [character isEqual: @"7"]|
            [character isEqual: @"8"]|
            [character isEqual: @"9"]) {
            
            
            if (found) {
                if ((i - foundIndex) == 1) {
                    foundIndex = i;
                    result = [result stringByAppendingString:character];//是数字的累加起来
                } else {
                    break;
                }
            } else {
                found = YES;
                foundIndex = i;
                result = [result stringByAppendingString:character];//是数字的累加起来
            }
        }
    }
    
    return result;
}

@end
