//
//  UIView+UINib.m
//  qisuedu
//
//  Created by lg on 15/8/20.
//  Copyright (c) 2015年 apple. All rights reserved.
//

#import "UIView+UINib.h"

@implementation UIView (UINib)
+ (instancetype)viewFromNibWithoutOwner
{
    NSArray *views = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:nil options:nil];
    return views[0];
}
@end
