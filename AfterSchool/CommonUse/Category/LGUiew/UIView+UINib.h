//
//  UIView+UINib.h
//  qisuedu
//
//  Created by lg on 15/8/20.
//  Copyright (c) 2015年 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (UINib)

+ (instancetype)viewFromNibWithoutOwner;

@end
