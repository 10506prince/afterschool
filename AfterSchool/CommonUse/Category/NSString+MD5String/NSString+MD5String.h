//
//  NSString+MD5String.h
//  PasswordCode
//
//  Created by Teson Draw on 1/10/16.
//  Copyright © 2016 Teson Draw. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (MD5String)

- (NSString *)MD5String;

@end
