//
//  UIViewController+UINib.m
//  qisuedu
//
//  Created by lg on 15/8/20.
//  Copyright (c) 2015年 apple. All rights reserved.
//

#import "UIViewController+UINib.h"

@implementation UIViewController (UINib)
+ (instancetype)viewControllerFromNib
{
    return [[self alloc] initWithNibName:NSStringFromClass([self class]) bundle:nil];
}
@end
