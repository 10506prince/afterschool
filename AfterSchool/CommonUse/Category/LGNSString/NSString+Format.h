//
//  NSString+Json.h
//  qisuedu
//
//  Created by apple on 15/3/19.
//  Copyright (c) 2015年 apple. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Format)
/**
 *  删除字符串中的控制符
 *
 *  @param dataStr 原始字符串
 *
 *  @return 返回的字符串
 */
+ (NSString *)deleteControlCharacter:(NSString *)dataStr;

/**
 *  删除字符串中除换行符的其他字符
 *
 *  @param dataStr 原始字符串
 *
 *  @return 返回的字符串
 */
+ (NSString *)deleteControlCharactersSkipNewLineCharacters:(NSString *)dataStr;

/**
 *  删除字符串头尾的空格和换行
 *
 *  @param dataStr 原始字符串
 *
 *  @return 返回的字符串
 */
+ (NSString *)deleteNewLineAndWhitespaceCharactersInHeadAndEnd:(NSString *)dataStr;

+ (BOOL)isValidateMobile:(NSString *)mobile;
+ (BOOL)isValidateEmail:(NSString *)email;

@end
