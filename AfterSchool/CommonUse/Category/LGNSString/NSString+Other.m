//
//  NSString+Other.m
//  AfterSchool
//
//  Created by lg on 15/12/26.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "NSString+Other.h"

@implementation NSString (Other)

- (NSUInteger)gradeId {
    if ([self isEqualToString:@"无段位"]) {
        return 0;
    } else if ([self isEqualToString:@"青铜V"]) {
        return 1;
    } else if ([self isEqualToString:@"青铜V"]) {
        return 11;
    } else if ([self isEqualToString:@"青铜IV"]) {
        return 12;
    } else if ([self isEqualToString:@"青铜III"]) {
        return 13;
    } else if ([self isEqualToString:@"青铜II"]) {
        return 14;
    } else if ([self isEqualToString:@"青铜I"]) {
        return 15;
    } else if ([self isEqualToString:@"白银V"]) {
        return 21;
    } else if ([self isEqualToString:@"白银IV"]) {
        return 22;
    } else if ([self isEqualToString:@"白银III"]) {
        return 23;
    } else if ([self isEqualToString:@"白银II"]) {
        return 24;
    } else if ([self isEqualToString:@"白银I"]) {
        return 25;
    } else if ([self isEqualToString:@"黄金V"]) {
        return 31;
    } else if ([self isEqualToString:@"黄金IV"]) {
        return 32;
    } else if ([self isEqualToString:@"黄金III"]) {
        return 33;
    } else if ([self isEqualToString:@"黄金II"]) {
        return 34;
    } else if ([self isEqualToString:@"黄金I"]) {
        return 35;
    } else if ([self isEqualToString:@"白金V"]) {
        return 41;
    } else if ([self isEqualToString:@"白金IV"]) {
        return 42;
    } else if ([self isEqualToString:@"白金III"]) {
        return 43;
    } else if ([self isEqualToString:@"白金II"]) {
        return 44;
    } else if ([self isEqualToString:@"白金I"]) {
        return 45;
    } else if ([self isEqualToString:@"钻石V"]) {
        return 51;
    } else if ([self isEqualToString:@"钻石IV"]) {
        return 52;
    } else if ([self isEqualToString:@"钻石III"]) {
        return 53;
    } else if ([self isEqualToString:@"钻石II"]) {
        return 54;
    } else if ([self isEqualToString:@"钻石I"]) {
        return 55;
    } else if ([self isEqualToString:@"超凡大师"]) {
        return 61;
    } else if ([self isEqualToString:@"最强王者"]) {
        return 71;
    }
    return 0;
}
@end
