//
//  NSString+Json.m
//  qisuedu
//
//  Created by apple on 15/3/19.
//  Copyright (c) 2015年 apple. All rights reserved.
//

#import "NSString+Format.h"

//    + (NSCharacterSet *)controlCharacterSet;
//    + (NSCharacterSet *)whitespaceCharacterSet;
//    + (NSCharacterSet *)whitespaceAndNewlineCharacterSet;
//    + (NSCharacterSet *)decimalDigitCharacterSet;
//    + (NSCharacterSet *)letterCharacterSet;
//    + (NSCharacterSet *)lowercaseLetterCharacterSet;
//    + (NSCharacterSet *)uppercaseLetterCharacterSet;
//    + (NSCharacterSet *)nonBaseCharacterSet;
//    + (NSCharacterSet *)alphanumericCharacterSet;
//    + (NSCharacterSet *)decomposableCharacterSet;
//    + (NSCharacterSet *)illegalCharacterSet;
//    + (NSCharacterSet *)punctuationCharacterSet;
//    + (NSCharacterSet *)capitalizedLetterCharacterSet;
//    + (NSCharacterSet *)symbolCharacterSet;
//    + (NSCharacterSet *)newlineCharacterSet NS_AVAILABLE(10_5, 2_0);

@implementation NSString (Format)

+ (NSString *)deleteControlCharacter:(NSString *)dataStr
{
    NSScanner *scanner = [[NSScanner alloc] initWithString:dataStr];
    [scanner setCharactersToBeSkipped:nil];
    NSMutableString *result = [[NSMutableString alloc] init];
    
    NSString *temp;
    
    NSCharacterSet * controlCharacters = [NSCharacterSet controlCharacterSet];
    // 扫描
    while (![scanner isAtEnd])
    {
        temp = nil;
        BOOL flag = NO;
        //        [scanner scanCharactersFromSet:newLineCharacters intoString:&temp];
        
        flag = [scanner scanUpToCharactersFromSet:controlCharacters intoString:&temp];
        if (flag) {//找到了控制符
            if (temp) {//控制符之间有内容
                [result appendString:temp];
            }
            //            NSLog(@"%ld", scanner.scanLocation);
            // 替换控制符
            if ([scanner scanCharactersFromSet:controlCharacters intoString:NULL]) {
            }
        }
    }
    return result;
}

+ (NSString *)deleteNewLineAndWhitespaceCharactersInHeadAndEnd:(NSString *)dataStr
{
    return [dataStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

+ (NSString *)deleteControlCharactersSkipNewLineCharacters:(NSString *)dataStr
{
//    NSCharacterSet * newLineCharacters = [NSCharacterSet newlineCharacterSet];
    NSCharacterSet * controlCharacters = [NSCharacterSet controlCharacterSet];
    
    NSScanner *scanner = [[NSScanner alloc] initWithString:dataStr];
    [scanner setCharactersToBeSkipped:nil];
    NSMutableString *resultTemp = [[NSMutableString alloc] init];
    
    NSString *temp;
    NSString *newLineTemp;
    
    // 扫描
    while (![scanner isAtEnd])
    {
        temp = nil;
        BOOL flag = NO;
        
        flag = [scanner scanUpToCharactersFromSet:controlCharacters intoString:&temp];
        if (flag) {//找到了控制符
            if (temp) {//控制符之间有内容
                [resultTemp appendString:temp];
            }
//            NSLog(@"%ld", scanner.scanLocation);
            // 替换换行符
            if ([scanner scanCharactersFromSet:controlCharacters intoString:&newLineTemp]) {
//                NSLog(@"%ld", scanner.scanLocation);
//                NSLog(@"%@", newLineTemp);
//
//                NSString * string = [newLineTemp stringByReplacingOccurrencesOfString:@"\r\n" withString:@"\\r\\n"];
//                NSLog(@"%@", string);

                if (resultTemp.length > 0 && ![scanner isAtEnd]) // Dont append space to beginning or end of result
                {
                    NSRange range = [newLineTemp rangeOfString:@"\r\n"];
//                    NSLog(@"add huan hang");
                    if (range.length > 0) {
                        
                        [resultTemp appendString:@"\\r\\n"];
                    }
                }
            }
        }
    }

    return resultTemp;
}

/*手机号码验证 MODIFIED BY HELENSONG*/

+ (BOOL)isValidateMobile:(NSString *)mobile

{
    
    //手机号以13， 15，18开头，八个 \d 数字字符
    
    NSString *phoneRegex = @"^((13[0-9])|(15[^4,\\D])|(18[0,0-9]))\\d{8}$";
    
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",phoneRegex];
    
    return [phoneTest evaluateWithObject:mobile];
    
}

/*邮箱验证 MODIFIED BY HELENSONG*/

+ (BOOL)isValidateEmail:(NSString *)email

{
    
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:email];
    
}

@end
