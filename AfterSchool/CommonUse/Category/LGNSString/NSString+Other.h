//
//  NSString+Other.h
//  AfterSchool
//
//  Created by lg on 15/12/26.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Other)

- (NSUInteger)gradeId;

@end
