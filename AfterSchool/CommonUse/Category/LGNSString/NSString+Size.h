//
//  NSString+Size.h
//  动态计算UITableViewCell高度详解
//
//  Created by apple on 15/6/17.
//  Copyright (c) 2015年 qisu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface NSString (Size)

/**
 *  计算字符串高度（以单词控制换行）
 *
 *  @param size 区间 如：CGSizeMake(320, MAXFLOAT)
 *  @param font 显示字符的字体
 *
 *  @return 高度（整数）
 */
- (CGSize)calculateSize:(CGSize)size font:(UIFont *)font;

/**
 *  计算字符串高度（以字符控制换行）
 *
 *  @param size 区间 如：CGSizeMake(320, MAXFLOAT)
 *  @param font 显示字符的字体
 *
 *  @return 高度（整数）
 */
- (CGSize)calculateTwoSize:(CGSize)size font:(UIFont *)font;

@end
