//
//  NSString+Null.m
//  qisuedu
//
//  Created by apple on 15/4/2.
//  Copyright (c) 2015年 apple. All rights reserved.
//

#import "NSString+Null.h"

@implementation NSString (Null)

+ (BOOL)isNull:(NSString *)str {
    if (str == nil || [str isKindOfClass:[NSNull class]]||[str isEqualToString:@""]) {
        return YES;
    }
    else {
        return NO;
    }
}


@end
