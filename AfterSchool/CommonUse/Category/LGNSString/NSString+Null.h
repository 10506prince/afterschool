//
//  NSString+Null.h
//  qisuedu
//
//  Created by apple on 15/4/2.
//  Copyright (c) 2015年 apple. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Null)

+ (BOOL)isNull:(NSString *)str;

@end
