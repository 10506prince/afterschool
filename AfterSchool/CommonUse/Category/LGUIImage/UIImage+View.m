//
//  UIImage+View.m
//  baiduMap
//
//  Created by 李刚 on 15/9/25.
//  Copyright © 2015年 李刚. All rights reserved.
//

#import "UIImage+View.h"

@implementation UIImage (View)

//将UIView转成UIImage
+ (UIImage *)imageFromView:(UIView *)view
{
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, NO, view.layer.contentsScale);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image=UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

@end
