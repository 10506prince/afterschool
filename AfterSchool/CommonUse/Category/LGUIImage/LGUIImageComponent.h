//
//  LGUIImage.h
//  AfterSchool
//
//  Created by lg on 16/1/9.
//  Copyright © 2016年 Teson Draw. All rights reserved.
//

#ifndef LGUIImageComponent_h
#define LGUIImageComponent_h

#import "UIImage+Color.h"
#import "UIImage+View.h"
#import "UIImage+Scale.h"


#endif /* LGUIImage_h */
