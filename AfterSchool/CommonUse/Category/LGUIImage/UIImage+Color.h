//
//  UIImage+Color.h
//  AfterSchool
//
//  Created by lg on 16/1/9.
//  Copyright © 2016年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Color)

//UIColor 转UIImage
+ (UIImage*) imageWithColor:(UIColor*)color;

+ (UIImage*) imageWithColor:(UIColor*)color size:(CGSize)size;

@end
