//
//  UIButton+AddLineAtLocation.m
//  AfterSchool
//
//  Created by Teson Draw on 1/4/16.
//  Copyright © 2016 Teson Draw. All rights reserved.
//

#import "UIButton+AddLineAtLocation.h"

@implementation UIButton (AddLineAtLocation)

- (void)addLineAtLocation:(Location)location color:(UIColor *)color {

    UIView *lineView = [[UIView alloc] init];
    
    if (color) {
        lineView.backgroundColor = color;
    } else {
        lineView.backgroundColor = [UIColor colorWithRed:230/255.f green:230/255.f blue:230/255.f alpha:1];
    }
    
    switch (location) {
        case LocationTop:
            lineView.frame = CGRectMake(0, 0, self.bounds.size.width, 1);
            break;
            
        case LocationLeft:
            lineView.frame = CGRectMake(0, 0, 1, self.bounds.size.height);
            break;
            
        case LocationBottom:
            lineView.frame = CGRectMake(0, self.bounds.size.height - 1, self.bounds.size.width, 1);
            break;
            
        case LocationRight:
            lineView.frame = CGRectMake(self.bounds.size.width - 1, 0, 1, self.bounds.size.height);
            break;
    }
    
    [self addSubview:lineView];
}

@end
