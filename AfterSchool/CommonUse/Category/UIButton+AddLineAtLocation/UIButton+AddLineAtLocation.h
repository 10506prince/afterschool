//
//  UIButton+AddLineAtLocation.h
//  AfterSchool
//
//  Created by Teson Draw on 1/4/16.
//  Copyright © 2016 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    LocationTop,    ///<上
    LocationLeft,   ///<左
    LocationBottom, ///<下
    LocationRight   ///<右
} Location;

@interface UIButton (AddLineAtLocation)

- (void)addLineAtLocation:(Location)location color:(UIColor *)color;

@end
