//
//  NSString+PhoneNumber.h
//  AfterSchool
//
//  Created by Teson Draw on 10/8/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (PhoneNumber)

- (BOOL)isMobilePhoneNumber;
- (BOOL)isNumber;
- (BOOL)isFloat;

@end
