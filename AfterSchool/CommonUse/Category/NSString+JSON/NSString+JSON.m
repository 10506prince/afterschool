//
//  NSString+JSON.m
//  AfterSchool
//
//  Created by Teson Draw on 11/6/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "NSString+JSON.h"

@implementation NSString (JSON)

+ (NSDictionary *)dictionaryFromJSONString:(NSString *)string {
    NSData *JSONData = [string dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *result = [NSJSONSerialization JSONObjectWithData:JSONData
                                                                 options:NSJSONReadingMutableLeaves
                                                                   error:nil];
    return result;
}

@end
