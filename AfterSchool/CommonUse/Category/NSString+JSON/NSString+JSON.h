//
//  NSString+JSON.h
//  AfterSchool
//
//  Created by Teson Draw on 11/6/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (JSON)

+ (NSDictionary *)dictionaryFromJSONString:(NSString *)string;

@end
