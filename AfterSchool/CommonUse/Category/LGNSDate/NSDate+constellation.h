//
//  NSDate+constellation.h
//  AfterSchool
//
//  Created by lg on 15/10/20.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (constellation)
/**
 *  @brief  通过date返回星座
 *
 *  @param date 生日
 *
 *  @return 星座（eg:魔羯座）
 */
+ (NSString *)getConstellationFromDate:(NSDate *)date;

/**
 *  @brief  通过date返回年龄
 *
 *  @param date 生日
 *
 *  @return 年龄
 */
+ (NSUInteger)getAgeFromBirthday:(NSDate *)date;

@end
