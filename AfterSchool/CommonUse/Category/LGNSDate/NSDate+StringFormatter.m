//
//  NSDate+StringFormatter.m
//  AfterSchool
//
//  Created by lg on 15/11/7.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "NSDate+StringFormatter.h"

@implementation NSDate (StringFormatter)

- (NSString *)stringWithFormatter:(NSString *)formatterStr {
    NSString * result = nil;
    
    NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:formatterStr];
    
    result = [formatter stringFromDate:self];
    return result;
}

- (NSString *)defaultString {

    return [self stringWithFormatter:@"yyyy/MM/dd"];
}

- (NSString *)formatStringWithHyphen {
    return [self stringWithFormatter:@"yyyy-MM-dd"];
}

- (NSString *)chineseDate {
    return [self stringWithFormatter:@"yyyy年MM月dd日"];
}

- (NSString *)chineseDatetime {
    return [self stringWithFormatter:@"yyyy年MM月dd日 HH点mm分"];
}

- (NSString *)chineseMonthDay {
    return [self stringWithFormatter:@"MM月dd日"];
}

- (NSString *)time {
    return [self stringWithFormatter:@"HH:mm"];
}

//- (NSString *)weekDay {
//    NSCalendar *currentCalendar = [NSCalendar currentCalendar];
//    NSDateComponents *components = [currentCalendar componentsInTimeZone:[NSTimeZone systemTimeZone] fromDate:self];
//    
//    return [self stringWithFormatter:@"HH:mm"];
//}

@end
