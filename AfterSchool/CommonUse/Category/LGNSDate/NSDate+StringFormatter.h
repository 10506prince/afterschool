//
//  NSDate+StringFormatter.h
//  AfterSchool
//
//  Created by lg on 15/11/7.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (StringFormatter)

/**
 *  @brief  返回指定格式的时间字符串
 *
 *  @param formatterStr 指定格式（eg:@"yyyy年MM月dd日"）
 *
 *  @return 指定格式的时间字符串
 */
- (NSString *)stringWithFormatter:(NSString *)formatterStr;

/**
 *  @brief  返回默认格式的时间字符串
 *
 *  @return 默认格式的时间字符串（eg:@"yyyy/M/d"）
 */
- (NSString *)defaultString;


/**
 *  change date to string, slicing with hyphen
 *
 *  @return format string like @"2015-11-13"
 */
- (NSString *)formatStringWithHyphen;

/**
 *  时间戳转换为中文日期
 *
 *  @return like @"yyyy年MM月dd日"
 */
- (NSString *)chineseDate;

/**
 *  时间戳转换为中文日期时间
 *
 *  @return like @"yyyy年MM月dd日 HH点mm分"
 */
- (NSString *)chineseDatetime;

/**
 *  时间戳转换为中文-月份和日子
 *
 *  @return like @"12月11日"
 */
- (NSString *)chineseMonthDay;


/**
 *  时间戳转换为短时间（不带秒）
 *
 *  @return like @"13:15"
 */
- (NSString *)time;

@end
