//
//  LGPayViewController.h
//  AfterSchool
//
//  Created by lg on 15/12/18.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^LGPayViewControllerFinishBlock)(BOOL success);

@interface LGPayViewController : UIViewController

- (void)showInView:(UIView *)view money:(float)money;

- (void)addFinishBlock:(LGPayViewControllerFinishBlock)finishBlock;

@end
