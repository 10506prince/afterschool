//
//  LGPayViewController.m
//  AfterSchool
//
//  Created by lg on 15/12/18.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGPayViewController.h"
#import "Pingpp.h"
#import "LGDefineNetServer.h"
#import "SVProgressHUD.h"

typedef NS_ENUM(NSUInteger, LGPayType) {
    LGPayTypeAliPay = 100,
    LGPayTypeWeiXin = 101,
};

@interface LGPayViewController ()

@property (assign) float money;///<支付金额(元)
@property (nonatomic, copy) LGPayViewControllerFinishBlock finishBlock;///<支付完成回调

@property (nonatomic, weak) IBOutlet UIView * displayView;
@property (nonatomic, weak) IBOutlet UILabel * moneyLabel;
@property (nonatomic, weak) IBOutlet UIButton * alipayBtn;
@property (nonatomic, weak) IBOutlet UIButton * weiXinBtn;
@end

@implementation LGPayViewController

+ (instancetype)viewControllerWithMoney:(float)money finishBlock:(LGPayViewControllerFinishBlock)finishBlock {
    LGPayViewController * selfVc = [[LGPayViewController alloc] initWithNibName:@"LGPayViewController" bundle:nil];
    selfVc.money = money;
    selfVc.finishBlock = finishBlock;
    
    return selfVc;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initData];
    [self initUserInterface];
}


- (void)initData {
    self.money = 0;
}

- (void)initUserInterface {
    self.alipayBtn.tag = LGPayTypeAliPay;
    [self.alipayBtn.layer setCornerRadius:5];
    [self.alipayBtn.layer setBorderColor:[UIColor blackColor].CGColor];
    [self.alipayBtn.layer setBorderWidth:1/[UIScreen mainScreen].scale];
    
    self.weiXinBtn.tag = LGPayTypeWeiXin;
    [self.weiXinBtn.layer setCornerRadius:5];
    [self.weiXinBtn.layer setBorderColor:[UIColor blackColor].CGColor];
    [self.weiXinBtn.layer setBorderWidth:1/[UIScreen mainScreen].scale];
    
    [self.displayView.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.displayView.layer setShadowOpacity:0.5];
    [self.displayView.layer setShadowOffset:CGSizeMake(0, -3)];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];
//    [self backAction:nil];
    [self hideAnimation];
}

#pragma mark - Action
- (IBAction)backAction:(id)sender {
    [self.view removeFromSuperview];
}

- (IBAction)rechargeAction:(UIButton *)sender {
    
    LGCharge * charge = [[LGCharge alloc] init];
    charge.amount = self.money * 100;
    charge.body = [NSString stringWithFormat:@"充值%.f元", self.money];
    charge.subject = @"充值";
    
    NSString * payUrl = nil;
    LGPayType type = sender.tag;
    switch (type) {
        case LGPayTypeAliPay:
            charge.channel = @"alipay";
            payUrl = alipayUrl;
            break;
            
        case LGPayTypeWeiXin:
            charge.channel = @"wx";
            payUrl = wxpayUrl;
            break;
    }
    
    
    __weak typeof(self) weakSelf = self;
    
    [SVProgressHUD showWithStatus:@"正在充值..." maskType:SVProgressHUDMaskTypeClear];
    [LGDefineNetServer getCharge:charge success:^(id result) {
        [Pingpp createPayment:result appURLScheme:payUrl withCompletion:^(NSString *result, PingppError *error) {
            
            LGPayViewControllerFinishBlock block = weakSelf.finishBlock;
            
            if ([result isEqualToString:@"success"]) {
                if (block) {
                    block(YES);
                }
                [SVProgressHUD dismissWithSuccess:@"充值成功！"];
            } else {
                if (block) {
                    block(NO);
                }
                NSLog(@"Error: code=%lu msg=%@", (unsigned long)error.code, [error getMsg]);
                [SVProgressHUD dismissWithError:@"充值失败！"];
            }
        }];
        /*
        [Pingpp createPayment:result viewController:weakSelf appURLScheme:payUrl withCompletion:^(NSString *result, PingppError *error) {
            if ([result isEqualToString:@"success"]) {
                [SVProgressHUD dismissWithSuccess:@"充值成功！"];
                
            } else {
                NSLog(@"Error: code=%lu msg=%@", (unsigned long)error.code, [error getMsg]);
                [SVProgressHUD dismissWithError:@"充值失败！"];
            }
            
        }];
         */
    } failure:^(NSString *msg) {
        [SVProgressHUD dismissWithError:msg];
        LGPayViewControllerFinishBlock block = weakSelf.finishBlock;
        if (block) {
            block(NO);
        }
    }];
}

#pragma mark - 

- (void)addFinishBlock:(LGPayViewControllerFinishBlock)finishBlock {
    self.finishBlock = finishBlock;
}

//- (void)showWithMoney:(float)money {
//    self.money = money;
//    [self.moneyLabel setText:[NSString stringWithFormat:@"还需支付%.f元",self.money]];
//    
//    CGRect frame = self.displayView.frame;
//    frame.origin.y = self.view.bounds.size.height - 200;
//    
//    [UIView animateWithDuration:0.5 animations:^{
//        self.view.alpha = 0.3;
//        [self.displayView setFrame:frame];
//    } completion:^(BOOL finished) {
//        
//    }];
//}

- (void)showInView:(UIView *)view money:(float)money {
    self.money = money;
    [self.moneyLabel setText:[NSString stringWithFormat:@"还需支付%.f元",self.money]];
    
    [view addSubview:self.view];
    
    CGRect initFrame = self.displayView.frame;
    CGFloat originY = initFrame.origin.y;
    
    initFrame.origin.y = [UIScreen mainScreen].bounds.size.height;
    self.displayView.frame = initFrame;
    
    [self.view setBackgroundColor:[UIColor colorWithWhite:0 alpha:0]];
    
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        CGRect fram = self.displayView.frame;
        fram.origin.y = originY;
        
        [self.view setBackgroundColor:[UIColor colorWithWhite:0 alpha:0.3]];
        self.displayView.frame = fram;
    } completion:^(BOOL finished) {
        
    }];
}

- (void)hideAnimation {
    
    CGRect initFrame = self.displayView.frame;
    
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        CGRect fram = self.displayView.frame;
        fram.origin.y = [UIScreen mainScreen].bounds.size.height;
        
        [self.view setBackgroundColor:[UIColor colorWithWhite:0 alpha:0]];
        self.displayView.frame = fram;
    } completion:^(BOOL finished) {
        [self backAction:nil];
        self.displayView.frame = initFrame;
    }];
}


@end
