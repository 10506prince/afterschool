//
//  LGRewarcAlertViewController.h
//  AfterSchool
//
//  Created by lg on 15/12/3.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LGDaKaInfo.h"

@class LGRewardAlertModel;

@interface LGRewarcAlertViewController : UIViewController

@property (nonatomic, strong) LGRewardAlertModel * rewardModel;

@end

@interface LGRewardAlertModel : NSObject

@property (nonatomic, assign) NSUInteger playTotalCount;    ///<陪玩总局数
@property (nonatomic, assign) NSUInteger playTotalMoney;    ///<陪玩总打赏
@property (nonatomic, assign) NSUInteger playTotalTime;    ///<陪玩总时长
@property (nonatomic, assign) NSUInteger priceOnline;     ///<每小时陪玩价格（线上）
@property (nonatomic, assign) NSUInteger priceOffline;    ///<每小时陪玩价格（线下）

- (instancetype)initWithLGDakaInfo:(LGDaKaInfo *)dakaInfo;

@end
