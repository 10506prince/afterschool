//
//  LGRewarcAlertViewController.m
//  AfterSchool
//
//  Created by lg on 15/12/3.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGRewarcAlertViewController.h"
#import "Common.h"
#import "TDSingleton.h"

@interface LGRewarcAlertViewController ()

@property (nonatomic, weak) IBOutlet UIView * alertView;

@property (nonatomic, weak) IBOutlet UILabel * rewarcLabel;
@property (nonatomic, weak) IBOutlet UILabel * playCountLabel;
@property (nonatomic, weak) IBOutlet UILabel * playTimeTotalLabel;
@property (nonatomic, weak) IBOutlet UILabel * rewarcTotalLabel;

@property (nonatomic, weak) IBOutlet UIButton * cancelBtn;

@property NSAttributedStringEnumerationOptions * attributedStringOptions;

@end

@implementation LGRewarcAlertViewController

- (void)dealloc
{
    NSLogSelfMethodName;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initData];
    [self initUserInterface];
}

- (void)initData {
//    NSArray * btns = @[_sinaBtn,_weiXinBtn,_weiXinTimeLineBtn,_qqBtn,_qzoneBtn];
//    int tags[5] = {ShareBtnTypeSinaWeibo,ShareBtnTypeWechatSession,ShareBtnTypeWechatTimeline,ShareBtnTypeqqFriend,ShareBtnTypeqZone};
//    
//    for (int i = 0; i < btns.count; i++) {
//        UIButton * btn = btns[i];
//        [btn addTarget:self action:@selector(shareBtnAction:) forControlEvents:UIControlEventTouchUpInside];
//        [btn setTag:tags[i]];
//    }
    
    [_cancelBtn addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
}
- (void)initUserInterface {

    [_alertView.layer setCornerRadius:8];
    [_alertView.layer setMasksToBounds:YES];
    
    UIView * line = [[UIView alloc] initWithFrame:CGRectMake(0, 300 - 46 - SINGLE_LINE_ADJUST_OFFSET, 262, SINGLE_LINE_WIDTH)];
    [line setBackgroundColor:LgColor(202, 1)];
    [_alertView addSubview:line];
    
    UIView * line1 = [[UIView alloc] initWithFrame:CGRectMake(0, 16 + 12 +20 + 40 + 12 - SINGLE_LINE_ADJUST_OFFSET, 262, SINGLE_LINE_WIDTH)];
    [line1 setBackgroundColor:LgColor(202, 1)];
    [_alertView addSubview:line1];
}

//- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
//    [super touchesBegan:touches withEvent:event];
//    [self backAction];
//}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [super touchesEnded:touches withEvent:event];
    [self backAction];
}

#pragma mark - Action
- (void)backAction {
    [self.view removeFromSuperview];
}

- (void)setRewardModel:(LGRewardAlertModel *)rewardModel {
    _rewardModel = rewardModel;
    
    NSUInteger systemPrice = [TDSingleton instance].orderSystemPrice;
    NSUInteger rewardMoney = _rewardModel.priceOnline - systemPrice;
    
    if (systemPrice > _rewardModel.priceOnline) {
        rewardMoney = 0;
    }

    NSString * rewardMoneyStr = [NSString stringWithFormat:@"%ld元", (unsigned long)rewardMoney];
    NSString * playCountStr = [NSString stringWithFormat:@"%ld次", (unsigned long)_rewardModel.playTotalCount];
    NSString * playTotalTimeStr = [NSString stringWithFormat:@"%ld小时", (unsigned long)_rewardModel.playTotalTime];
    NSString * rewardTotalStr = [NSString stringWithFormat:@"%ld元", (unsigned long)_rewardModel.playTotalMoney];
    
    NSDictionary * rewardMoneyAttributedOptionDic = @{
                                                      NSFontAttributeName:[UIFont systemFontOfSize:40],
                                                      NSForegroundColorAttributeName:LgColorRGB(250, 142, 47, 1)
                                                      };
    NSDictionary * playAttributedOptionDic = @{
                                                      NSFontAttributeName:[UIFont systemFontOfSize:22],
                                                      NSForegroundColorAttributeName:LgColor(120, 1)
                                                      };
    NSDictionary * endAttributedOptionDic = @{
                                                      NSFontAttributeName:[UIFont systemFontOfSize:12],
                                                      NSForegroundColorAttributeName:LgColor(202, 1)
                                                      };
    
    
    NSMutableAttributedString * rewardMoneyAttributedStr = [[NSMutableAttributedString alloc] initWithString:rewardMoneyStr attributes:rewardMoneyAttributedOptionDic];
    [rewardMoneyAttributedStr addAttributes:endAttributedOptionDic range:NSMakeRange(rewardMoneyAttributedStr.length - 1, 1)];
    
    NSMutableAttributedString * playCountAttributedStr = [[NSMutableAttributedString alloc] initWithString:playCountStr attributes:playAttributedOptionDic];
    [playCountAttributedStr addAttributes:endAttributedOptionDic range:NSMakeRange(playCountStr.length - 1, 1)];
    
    NSMutableAttributedString * playTotalTimeAttributedStr = [[NSMutableAttributedString alloc] initWithString:playTotalTimeStr attributes:playAttributedOptionDic];
    [playTotalTimeAttributedStr addAttributes:endAttributedOptionDic range:NSMakeRange(playTotalTimeStr.length - 2, 2)];
    
    NSMutableAttributedString * rewardTotalAttributedStr = [[NSMutableAttributedString alloc] initWithString:rewardTotalStr attributes:playAttributedOptionDic];
    [rewardTotalAttributedStr addAttributes:endAttributedOptionDic range:NSMakeRange(rewardTotalStr.length - 1, 1)];
    
    [self.rewarcLabel setAttributedText:rewardMoneyAttributedStr];
    [self.playCountLabel setAttributedText:playCountAttributedStr];
    [self.playTimeTotalLabel setAttributedText:playTotalTimeAttributedStr];
    [self.rewarcTotalLabel setAttributedText:rewardTotalAttributedStr];
    
}

@end

@implementation LGRewardAlertModel

- (instancetype)initWithLGDakaInfo:(LGDaKaInfo *)dakaInfo {
    self = [super init];
    if (self) {
        self.priceOnline = dakaInfo.priceOnline;
        self.priceOffline = dakaInfo.priceOffline;
        
        self.playTotalTime = dakaInfo.playTotalTime;
        self.playTotalMoney = dakaInfo.playTotalMoney;
        self.playTotalCount = dakaInfo.playTotalCount;
    }
    return self;
}
@end


