//
//  LGSelectAddressViewController.h
//  AfterSchool
//
//  Created by lg on 15/12/2.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGViewController.h"
#import "LGCommonAddress.h"
#import "LGSelectAddressRootView.h"

#define MAX_COMMON_ADDRESS_NUMBER 3

@class LGSelectAddressResult,LGAddressPoi,LGPlaceResult;

typedef void(^LGSelectAddressViewControllerClickEnterActionBlock)(LGSelectAddressResult * address);

///选择地址数据类型
typedef NS_ENUM(NSUInteger, LGSelectAddressResultType) {
    LGSelectAddressResultTypeSearch,///<搜索结果
    LGSelectAddressResultTypeCommon,///<常用地点
    LGSelectAddressResultTypeNearPoi,///<地图选点
};

///选择地址数据模型
@interface LGSelectAddressResult : NSObject

@property (nonatomic, copy) NSString * name;
@property (nonatomic, copy) NSString * address;
@property (nonatomic, assign) double distance;
@property (nonatomic, assign) CLLocationCoordinate2D pt;
@property (nonatomic, assign) LGSelectAddressResultType type;

- (instancetype)initWithLGAddressPoi:(LGAddressPoi *)address;
- (instancetype)initWithLGCommonAddress:(LGCommonAddress *)commonAddress;
- (instancetype)initWithLGPlaceResult:(LGPlaceResult *)address;

@end

///选择地址视图控制器
@interface LGSelectAddressViewController : LGViewController

///如果要显示常用地址，添加到该数组中
@property (nonatomic, strong) NSArray <LGCommonAddress *> * commonAddress;

@property (nonatomic, strong) LGSelectAddressRootView * rootView;
@property (copy) LGSelectAddressViewControllerClickEnterActionBlock clickEnterActionBlock;

@end
