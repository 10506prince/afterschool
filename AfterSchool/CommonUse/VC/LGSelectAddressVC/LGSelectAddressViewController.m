//
//  LGSelectAddressViewController.m
//  AfterSchool
//
//  Created by lg on 15/12/2.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGSelectAddressViewController.h"
#import <BaiduMapAPI_Map/BMKMapView.h>
#import <BaiduMapAPI_Utils/BMKUtilsComponent.h>
#import "Common.h"
#import "TDSingleton.h"

#import "LGLocationService.h"
#import "LGDefineNetServer.h"
#import "LGGeoCodeService.h"
//#import "LGPlaceSuggestionService.h"
#import "LGPlaceService.h"


@implementation LGSelectAddressResult

- (instancetype)initWithLGCommonAddress:(LGCommonAddress *)commonAddress
{
    self = [super init];
    if (self) {
        self.name = commonAddress.name;
        self.address = commonAddress.address;
        self.distance = commonAddress.distance;
        self.pt = commonAddress.pt;
        self.type = LGSelectAddressResultTypeCommon;
    }
    return self;
}

- (instancetype)initWithLGAddressPoi:(LGAddressPoi *)address
{
    self = [super init];
    if (self) {
        self.name = address.name;
        self.address = address.addr;
        self.distance = [address.distance doubleValue];
        self.pt = CLLocationCoordinate2DMake(address.point.y, address.point.x);
        self.type = LGSelectAddressResultTypeNearPoi;
    }
    return self;
}

- (instancetype)initWithLGPlaceResult:(LGPlaceResult *)address
{
    self = [super init];
    if (self) {
        self.name = address.name;
        self.address = address.address;
        self.distance = address.distance;
        self.pt = CLLocationCoordinate2DMake(address.location.lat, address.location.lng);
        self.type = LGSelectAddressResultTypeSearch;
    }
    return self;
}
@end

@interface LGSelectAddressViewController () <UITableViewDataSource,UITableViewDelegate,BMKMapViewDelegate,UITextFieldDelegate>

@property (nonatomic, strong) NSArray <LGAddressPoi *> * nearPois;
@property (nonatomic, strong) NSArray <LGPlaceResult *> * nearPlaces;
@property (nonatomic, strong) NSArray <LGPlaceResult *> * placeResults;

@property (nonatomic, strong) NSMutableArray <LGSelectAddressResult *> * dataSource;
@property (nonatomic, assign) CLLocationCoordinate2D selectPt;
@property (nonatomic, strong) BMKUserLocation * currentLocation;

@end

@implementation LGSelectAddressViewController

- (void)dealloc
{
    NSLogSelfMethodName;
    
    if (_rootView.mapView.delegate) {
        _rootView.mapView.delegate = nil;
    }
    if (_rootView.mapView) {
        _rootView.mapView = nil;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initData];
    [self initUserInterface];
}

- (void)initData {
    self.currentLocation = [LGLocationService sharedManager].userLocation;
    _selectPt = self.currentLocation.location.coordinate;
    
    self.dataSource = [NSMutableArray array];
    
    self.nearPois = nil;///添加常用地址到列表数据源中
}

- (void)initUserInterface {
    [self.view addSubview:self.rootView];
}

- (LGSelectAddressRootView *)rootView {
    if (!_rootView) {
        _rootView = [[LGSelectAddressRootView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        
        [_rootView.navigationBarView.leftButton addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
        [_rootView.navigationBarView.rightButton addTarget:self action:@selector(enterAction:) forControlEvents:UIControlEventTouchUpInside];
        
        [_rootView.mapView setDelegate:self];
        [_rootView.tableView setDelegate:self];
        [_rootView.tableView setDataSource:self];
        [_rootView.searchField setDelegate:self];
    }
    return _rootView;
}


- (void)setNearPois:(NSArray<LGAddressPoi *> *)nearPois {
    _nearPois = nearPois;
    
    [self.dataSource removeAllObjects];
    
    for (LGCommonAddress * commonAddr in _commonAddress) {
        LGSelectAddressResult * result = [[LGSelectAddressResult alloc] initWithLGCommonAddress:commonAddr];
        [self.dataSource addObject:result];
    }
    
    for (LGAddressPoi * addr in _nearPois) {
        LGSelectAddressResult * result = [[LGSelectAddressResult alloc] initWithLGAddressPoi:addr];
        [self.dataSource addObject:result];
    }
    NSIndexSet * set = [NSIndexSet indexSetWithIndex:0];
    [self.rootView.tableView reloadSections:set withRowAnimation:UITableViewRowAnimationAutomatic];
}
- (void)setNearPlaces:(NSArray<LGPlaceResult *> *)nearPlaces {
    _nearPlaces = nearPlaces;
    
    [self.dataSource removeAllObjects];
    
    for (LGCommonAddress * commonAddr in _commonAddress) {
        LGSelectAddressResult * result = [[LGSelectAddressResult alloc] initWithLGCommonAddress:commonAddr];
        [self.dataSource addObject:result];
    }
    
    for (LGPlaceResult * addr in _nearPlaces) {
        LGSelectAddressResult * result = [[LGSelectAddressResult alloc] initWithLGPlaceResult:addr];
        [self.dataSource addObject:result];
    }
    NSIndexSet * set = [NSIndexSet indexSetWithIndex:0];
    [self.rootView.tableView reloadSections:set withRowAnimation:UITableViewRowAnimationAutomatic];
}

- (void)setPlaceResults:(NSArray<LGPlaceResult *> *)placeResults {
    _placeResults = placeResults;
    
    [self.dataSource removeAllObjects];
    
    for (LGPlaceResult * addr in _placeResults) {
        LGSelectAddressResult * result = [[LGSelectAddressResult alloc] initWithLGPlaceResult:addr];
        [self.dataSource addObject:result];
    }
    NSIndexSet * set = [NSIndexSet indexSetWithIndex:0];
    [self.rootView.tableView reloadSections:set withRowAnimation:UITableViewRowAnimationAutomatic];
}

#pragma mark - Action
- (IBAction)backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)enterAction:(id)sender {
    
    NSIndexPath * indexPath = [self.rootView.tableView indexPathForSelectedRow];
    
    LGSelectAddressResult * address = nil;
    
    if (indexPath != nil) {
        address = [self.dataSource objectAtIndex:indexPath.row];
    }
    
    LGSelectAddressViewControllerClickEnterActionBlock block = self.clickEnterActionBlock;
    
    if (block) {
        block(address);
    }
    
    [self backAction:nil];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    LGSelectAddressTableViewCellOne * cell = [tableView dequeueReusableCellWithClass:[LGSelectAddressTableViewCellOne class]];
    
    LGSelectAddressResult * address = [self.dataSource objectAtIndex:indexPath.row];
    
    cell.name = address.name;
    cell.address = address.address;
    cell.distance = address.distance;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return LGSelectAddressTableViewCellOneHeight;
}


#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
}

#pragma mark - BMKMapViewDelegate

- (void)mapViewDidFinishLoading:(BMKMapView *)mapView {
    [mapView updateLocationData:self.currentLocation];
    [mapView setCenterCoordinate:self.currentLocation.location.coordinate];
}

- (void)mapView:(BMKMapView *)mapView regionDidChangeAnimated:(BOOL)animated {
 
    [self getLocationInfo];
}

#pragma mark - UITextFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    [self.rootView.navigationBarView.rightButton setTitle:@"取消" forState:UIControlStateNormal];
    
    [self.rootView.navigationBarView.rightButton removeTarget:self action:@selector(enterAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.rootView.navigationBarView.rightButton addTarget:self action:@selector(cancelSearchAction:) forControlEvents:UIControlEventTouchUpInside];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    
    NSString * key = textField.text;
    [self getPlaceListWithKeyWord:key];
    
    [self.rootView.navigationBarView.rightButton setTitle:@"确定" forState:UIControlStateNormal];
    
    [self.rootView.navigationBarView.rightButton removeTarget:self action:@selector(cancelSearchAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.rootView.navigationBarView.rightButton addTarget:self action:@selector(enterAction:) forControlEvents:UIControlEventTouchUpInside];
    return YES;
}

- (IBAction)cancelSearchAction:(id)sender {
    
    self.rootView.searchField.text = nil;
    [self.rootView.searchField resignFirstResponder];
    
    [self.rootView.navigationBarView.rightButton setTitle:@"确定" forState:UIControlStateNormal];
    
    [self.rootView.navigationBarView.rightButton removeTarget:self action:@selector(cancelSearchAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.rootView.navigationBarView.rightButton addTarget:self action:@selector(enterAction:) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - 获取位置信息
- (void)getLocationInfo {
    CLLocationCoordinate2D pt = self.rootView.mapView.centerCoordinate;
    
    __weak typeof(self) weakSelf = self;
    [LGGeoCodeService reverseGeocodeWithPt:pt success:^(LGReverseGeoCodeResult *result) {

        LGPlaceLocalSearch * search = [[LGPlaceLocalSearch alloc] init];
        
        search.keyWord = @"网吧";
        search.region = result.addressComponent.city;
        
        [LGPlaceService getPlaceListWithSearchParam:search success:^(NSArray<LGPlaceResult *> *result) {
            
            NSMutableArray <LGPlaceResult *> * placeResults = [NSMutableArray arrayWithArray:result];
            for (LGPlaceResult * place in placeResults) {
                BMKMapPoint point1 = BMKMapPointForCoordinate(pt);
                BMKMapPoint point2 = BMKMapPointForCoordinate(CLLocationCoordinate2DMake(place.location.lat, place.location.lng));
                CLLocationDistance distance = BMKMetersBetweenMapPoints(point1,point2);
                place.distance = distance;
            }
            
            
            [placeResults sortUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
                LGPlaceResult * place1 = (LGPlaceResult *)obj1;
                LGPlaceResult * place2 = (LGPlaceResult *)obj2;
                if (place1.distance > place2.distance) {
                    return NSOrderedDescending;
                } else if (place1.distance < place2.distance) {
                    return NSOrderedAscending;
                } else {
                    return NSOrderedSame;
                }
            }];
            
            weakSelf.nearPlaces = placeResults;
        } failure:^{
            
        }];
    } failure:^(NSString *msg) {
    }];
    
//    [LGGeoCodeService reverseGeocodeWithPt:pt pois:NO success:^(id result) {
//        if ([result isKindOfClass:[NSArray class]] && ((NSArray *)result).count > 0) {
//            weakSelf.nearPois = result;
//        }
//    } failure:^{
//        
//    }];
}

- (void)getPlaceListWithCity:(NSString *)city {

    
}

- (void)getPlaceListWithKeyWord:(NSString *)keyWord {
    LGPlaceLocalSearch * search = [[LGPlaceLocalSearch alloc] init];
    
    search.keyWord = keyWord;
    search.region = [TDSingleton instance].currentCityName;
    
    [LGPlaceService getPlaceListWithSearchParam:search success:^(id result) {
        if ([result isKindOfClass:[NSArray class]]) {
            self.placeResults = result;
        }
    } failure:^{
        
    }];
    
}
//- (void)getSuggestionWithKeyWord:(NSString *)keyWord {
//    LGPlaceSuggestionSearch * search = [[LGPlaceSuggestionSearch alloc] init];
//    search.keyWord = keyWord;
//    search.region = @"全国";
//    
//    __weak typeof(self) weakSelf = self;
//    
//    [SVProgressHUD showWithStatus:@"正在搜索..."];
//    [LGPlaceSuggestionService getSuggestionListWithSearchParam:search success:^(id result) {
//        if ([result isKindOfClass:[NSArray class]]) {
//            weakSelf.suggestionResults = result;
//            NSIndexSet * indexSet = [NSIndexSet indexSetWithIndex:0];
//            [weakSelf.rootView.tableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationAutomatic];
//        }
//        [SVProgressHUD dismissWithSuccess:@"搜索完成"];
//    } failure:^{
//        weakSelf.suggestionResults = nil;
//        NSIndexSet * indexSet = [NSIndexSet indexSetWithIndex:0];
//        [weakSelf.rootView.tableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationAutomatic];
//        [SVProgressHUD dismissWithError:@"搜索失败"];
//    }];
//}

@end
