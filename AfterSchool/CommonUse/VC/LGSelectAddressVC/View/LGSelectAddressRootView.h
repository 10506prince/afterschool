//
//  LGSelectAddressRootView.h
//  AfterSchool
//
//  Created by lg on 15/12/2.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <BaiduMapAPI_Map/BMKMapView.h>
#import "NavigationBarView.h"
#import "LGSelectAddressTableViewCellOne.h"

@interface LGSelectAddressRootView : UIView

@property (nonatomic, strong) NavigationBarView * navigationBarView;

@property (nonatomic, strong) BMKMapView * mapView;
@property (nonatomic, strong) UIImageView * mapCenterPointView;

@property (nonatomic, strong) UITableView * tableView;
@property (nonatomic, strong) UITextField * searchField;
@property (nonatomic, strong) UISearchBar * searchBar;

@end
