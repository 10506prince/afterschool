//
//  LGSelectAddressTableViewCellOne.h
//  AfterSchool
//
//  Created by lg on 15/12/2.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITableView+NibCell.h"

#define LGSelectAddressTableViewCellOneHeight 44.0f

@interface LGSelectAddressTableViewCellOne : UITableViewCell

@property (nonatomic, copy) NSString * name;
@property (nonatomic, copy) NSString * address;
@property (nonatomic, assign) double distance; ///米

@end
