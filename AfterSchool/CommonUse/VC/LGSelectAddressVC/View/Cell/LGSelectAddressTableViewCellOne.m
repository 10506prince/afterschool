//
//  LGSelectAddressTableViewCellOne.m
//  AfterSchool
//
//  Created by lg on 15/12/2.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGSelectAddressTableViewCellOne.h"
#import "Common.h"
#import "CAShapeLayer+CustomPath.h"

@interface LGSelectAddressTableViewCellOne ()

@property (nonatomic, weak) IBOutlet UILabel * nameLabel;
@property (nonatomic, weak) IBOutlet UILabel * addressLabel;
@property (nonatomic, weak) IBOutlet UILabel * distanceLabel;
@property (nonatomic, weak) IBOutlet UIImageView * selectImageView;

@property (nonatomic, strong) CAShapeLayer * lineLayer;

@end

@implementation LGSelectAddressTableViewCellOne

- (void)awakeFromNib {
    self.lineLayer = [CAShapeLayer getLineLayer];
    [self.layer addSublayer:self.lineLayer];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    [self.selectImageView setHighlighted:selected];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.lineLayer.path = [CAShapeLayer linePathWithRect:self.bounds edgeInsets:UIEdgeInsetsMake(0, 20, 0, 20)];
}

#pragma mark - setValue
- (void)setName:(NSString *)name {
    _name = [name copy];
    
    [self.nameLabel setText:_name];
}

- (void)setAddress:(NSString *)address {
    _address = [address copy];
    
    [self.addressLabel setText:_address];
}

- (void)setDistance:(double)distance {
    _distance = distance;
    
    [self.distanceLabel setText:[NSString stringWithFormat:@"%.1fkm", _distance/1000.0]];
}

@end
