//
//  LGPickerSelectViewController.m
//  AfterSchool
//
//  Created by lg on 15/12/3.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGPickerSelectViewController.h"
#import "UIButton+SetBackgroundColor.h"

@implementation LGDataSource
- (instancetype)initWithName:(NSString *)name value:(id)value
{
    self = [super init];
    if (self) {
        self.name = name;
        self.value = value;
    }
    return self;
}
@end


@interface LGPickerSelectViewController () <UIPickerViewDataSource,UIPickerViewDelegate>

@property (nonatomic, strong) IBInspectable UIColor * borderColor;
@property (nonatomic, assign) IBInspectable CGFloat borderWidth;
@property (nonatomic, assign) IBInspectable CGFloat cornerRadius;

@property (nonatomic, weak) IBOutlet UIView * backView;
@property (nonatomic, weak) IBOutlet UIPickerView * pickerView;
@property (nonatomic, weak) IBOutlet UIButton * cancelBtn;
@property (nonatomic, weak) IBOutlet UIButton * confirmBtn;

@property (copy) LGPickerSelectViewControllerBlock confirmClickBlock;
@property (copy) LGPickerSelectViewControllerBlock cancelClickBlock;

- (void)dismiss;

@end

@implementation LGPickerSelectViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self.cancelBtn.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [self.cancelBtn.layer setBorderWidth:1];
    [self.cancelBtn setBackgroundColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
    
    [self.confirmBtn.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [self.confirmBtn.layer setBorderWidth:1];
    [self.confirmBtn setBackgroundColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
    
    [self.backView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [self.backView.layer setBorderWidth:1];
    [self.backView.layer setCornerRadius:8];
    [self.backView.layer setMasksToBounds:YES];
}

- (void)setBorderColor:(UIColor *)borderColor {
    _borderColor = borderColor;
    
    [self.backView.layer setBorderColor:_borderColor.CGColor];
}

- (void)setBorderWidth:(CGFloat)borderWidth {
    _borderWidth = borderWidth;
    
    [self.backView.layer setBorderWidth:_borderWidth];
}

- (void)setCornerRadius:(CGFloat)cornerRadius {
    _cornerRadius = cornerRadius;
    
    [self.backView.layer setCornerRadius:_cornerRadius];
}

- (void)setDataSource:(NSArray<LGDataSource *> *)dataSource {
    _dataSource = dataSource;
    
    [self.pickerView reloadAllComponents];
}

#pragma mark - Action
- (IBAction)cancelAction:(id)sender {
    
    [self dismiss];
    
    LGPickerSelectViewControllerBlock block = self.cancelClickBlock;
    if (block) {
        block(nil);
    }
}
- (IBAction)confirmAction:(id)sender {
    
    NSUInteger selectRow = [self.pickerView selectedRowInComponent:0];
    
    LGDataSource * data = [self.dataSource objectAtIndex:selectRow];
    LGPickerSelectViewControllerBlock block = self.confirmClickBlock;
    if (block) {
        block(data);
    }
}

#pragma mark - PublicMethod
- (void)addConfirmClickBlock:(LGPickerSelectViewControllerBlock)confirmClickBlock {
    self.confirmClickBlock = confirmClickBlock;
}

- (void)addCancelClickBlock:(LGPickerSelectViewControllerBlock)cancelClickBlock {
    self.cancelClickBlock = cancelClickBlock;
}

- (void)showInView:(UIView *)view {
    [view addSubview:self.view];
    
    CGRect fram = self.backView.frame;
    CGFloat originY = fram.origin.y;
    
    fram.origin.y = [UIScreen mainScreen].bounds.size.height;
    
    self.backView.frame = fram;
    [self.view setBackgroundColor:[UIColor colorWithWhite:0 alpha:0]];
    [UIView animateWithDuration:0.3 animations:^{
        
        CGRect fram = self.backView.frame;
        fram.origin.y = originY;
        
        [self.view setBackgroundColor:[UIColor colorWithWhite:0 alpha:0.3]];
        self.backView.frame = fram;
    }];
}

- (void)dismiss {
    [self.view removeFromSuperview];
}
#pragma mark - UIPickerViewDataSource,UIPickerViewDelegate
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return self.dataSource.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    LGDataSource * data = [self.dataSource objectAtIndex:row];
    return data.name;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    return 36;
}
@end
