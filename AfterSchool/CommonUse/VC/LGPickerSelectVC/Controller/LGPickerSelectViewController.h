//
//  LGPickerSelectViewController.h
//  AfterSchool
//
//  Created by lg on 15/12/3.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>

///数据类型
@interface LGDataSource : NSObject

@property (nonatomic, copy) NSString * name;
@property (nonatomic, strong) id value;

- (instancetype)initWithName:(NSString *)name value:(id)value;

@end


typedef void (^LGPickerSelectViewControllerBlock)(LGDataSource * data);

///picker选择器                        
@interface LGPickerSelectViewController : UIViewController

@property (nonatomic, strong) NSArray <LGDataSource *> * dataSource;


- (void)addConfirmClickBlock:(LGPickerSelectViewControllerBlock)confirmClickBlock;
- (void)addCancelClickBlock:(LGPickerSelectViewControllerBlock)cancelClickBlock;

- (void)showInView:(UIView *)view;

@end
