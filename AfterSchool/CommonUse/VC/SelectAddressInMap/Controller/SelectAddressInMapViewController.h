//
//  SelectAddressInMapViewController.h
//  AfterSchool
//
//  Created by lg on 15/11/2.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGViewController.h"
#import "SelectAddressInMapRootView.h"

@interface SelectAddressInMapViewController : LGViewController

@property (nonatomic, strong) SelectAddressInMapRootView * rootView;

@end
