//
//  SelectAddressInMapViewController.m
//  AfterSchool
//
//  Created by lg on 15/11/2.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "SelectAddressInMapViewController.h"
//#import <BaiduMapAPI_Base/BMKBaseComponent.h>
#import <BaiduMapAPI_Map/BMKMapComponent.h>
#import <BaiduMapAPI_Search/BMKSearchComponent.h>
#import <BaiduMapAPI_Location/BMKLocationComponent.h>
#import "Common.h"

@interface SelectAddressInMapViewController () <UITableViewDataSource,UITableViewDelegate,BMKMapViewDelegate, BMKGeoCodeSearchDelegate,BMKLocationServiceDelegate>
{
    NSMutableArray * _dataSource;
    
    CLLocationCoordinate2D _lastPoint;
    CLLocationCoordinate2D _currentPoint;
    
    BMKGeoCodeSearch * _geocodesearch;
    BMKLocationService * _locationService;
}
@end

@implementation SelectAddressInMapViewController

- (void)dealloc
{
    NSLogSelfMethodName;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initData];
    [self initUserInterface];
    [self initLocService];
    [self initGeocodeSearch];
}

- (void)initData {
    _dataSource = [NSMutableArray array];
    
}

- (void)initUserInterface {
    [self.view addSubview:self.rootView];
}

- (SelectAddressInMapRootView *)rootView {
    if (!_rootView) {
        _rootView = [[SelectAddressInMapRootView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        [_rootView.navigationBarView.leftButton addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
        
        [_rootView.mapView setDelegate:self];
        [_rootView.tableView setDelegate:self];
        [_rootView.tableView setDataSource:self];
    }
    return _rootView;
}


- (void)initGeocodeSearch {
    _geocodesearch = [[BMKGeoCodeSearch alloc] init];
    [_geocodesearch setDelegate:self];
}
///初始化定位服务
- (void)initLocService {
    //初始化定位服务（获取我的位置）
    _locationService = [[BMKLocationService alloc] init];
    [_locationService setDelegate:self];
    [_locationService startUserLocationService];
}

#pragma mark - Action
- (IBAction)backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)onClickReverseGeocode
{
    BMKReverseGeoCodeOption *reverseGeocodeSearchOption = [[BMKReverseGeoCodeOption alloc]init];
    
    reverseGeocodeSearchOption.reverseGeoPoint = _currentPoint;
    BOOL flag = [_geocodesearch reverseGeoCode:reverseGeocodeSearchOption];
    if(flag)
    {
        NSLog(@"反geo检索发送成功");
    }
    else
    {
        NSLog(@"反geo检索发送失败");
    }
}

-(IBAction)onClickGeocode
{
    //    isGeoSearch = true;
    //    BMKGeoCodeSearchOption *geocodeSearchOption = [[BMKGeoCodeSearchOption alloc]init];
    //    geocodeSearchOption.city= _cityText.text;
    //    geocodeSearchOption.address = _addrText.text;
    //    BOOL flag = [_geocodesearch geoCode:geocodeSearchOption];
    //    if(flag)
    //    {
    //        NSLog(@"geo检索发送成功");
    //    }
    //    else
    //    {
    //        NSLog(@"geo检索发送失败");
    //    }
    
}

///开始定位（定位服务）
- (void)startLocation {
    NSLogSelfMethodName;
    [_locationService startUserLocationService];
    self.rootView.mapView.showsUserLocation = NO;//先关闭显示的定位图层
    self.rootView.mapView.userTrackingMode = BMKUserTrackingModeFollow;//设置定位的状态
    self.rootView.mapView.showsUserLocation = YES;//显示定位图层
}

///停止定位（定位服务）
- (void)stopLocation {
    NSLogSelfMethodName;
    [_locationService stopUserLocationService];
    self.rootView.mapView.showsUserLocation = NO;
}
#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _dataSource.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString * cellId = @"selectAddressCell";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
    }
    
    NSString * address = [_dataSource objectAtIndex:indexPath.row];
    [cell.textLabel setText:address];
    
    return cell;
}


#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell * cell = [tableView cellForRowAtIndexPath:indexPath];
    [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell * cell = [tableView cellForRowAtIndexPath:indexPath];
    [cell setAccessoryType:UITableViewCellAccessoryNone];
}

#pragma mark - BMKMapViewDelegate

- (void)mapViewDidFinishLoading:(BMKMapView *)mapView {
    
}
//根据anntation生成对应的View
- (BMKAnnotationView *)mapView:(BMKMapView *)view viewForAnnotation:(id <BMKAnnotation>)annotation
{
    NSString *AnnotationViewID = @"annotationViewID";
    //根据指定标识查找一个可被复用的标注View，一般在delegate中使用，用此函数来代替新申请一个View
    BMKAnnotationView *annotationView = [view dequeueReusableAnnotationViewWithIdentifier:AnnotationViewID];
    if (annotationView == nil) {
        annotationView = [[BMKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:AnnotationViewID];
        ((BMKPinAnnotationView*)annotationView).pinColor = BMKPinAnnotationColorRed;
        ((BMKPinAnnotationView*)annotationView).animatesDrop = YES;
    }
    
    annotationView.centerOffset = CGPointMake(0, -(annotationView.frame.size.height * 0.5));
    annotationView.annotation = annotation;
    annotationView.canShowCallout = TRUE;
    return annotationView;
}

- (void)mapView:(BMKMapView *)mapView regionDidChangeAnimated:(BOOL)animated {
    [_dataSource removeAllObjects];
    [self.rootView.tableView reloadData];
    [self onClickReverseGeocode];
}

#pragma mark - BMKLocationServiceDelegate 定位服务代理

/**
 *在地图View将要启动定位时，会调用此函数
 *@param mapView 地图View
 */
- (void)willStartLocatingUser
{
    NSLog(@"start locate");
}

/**
 *用户位置更新后，会调用此函数
 *@param userLocation 新的用户位置
 */
- (void)didUpdateBMKUserLocation:(BMKUserLocation *)userLocation
{
    //    NSLogSelfMethodName;
    _currentPoint.latitude = userLocation.location.coordinate.latitude;
    _currentPoint.longitude = userLocation.location.coordinate.longitude;
    
    //实时更新我的位置
    [self.rootView.mapView updateLocationData:userLocation];
    
    [self.rootView.mapView setCenterCoordinate:userLocation.location.coordinate animated:NO];
    
    //[_locationService stopUserLocationService];
}

/**
 *在地图View停止定位后，会调用此函数
 *@param mapView 地图View
 */
- (void)didStopLocatingUser
{
    NSLog(@"stop locate");
}


/**
 *定位失败后，会调用此函数
 *@param mapView 地图View
 *@param error 错误号，参考CLError.h中定义的错误号
 */
- (void)didFailToLocateUserWithError:(NSError *)error
{
    NSLog(@"location error");
    NSLog(@"%@", [error localizedDescription]);
    //    [PromptInfo showText:@"定位失败"];
    //    [self stopLocation];
    [self performSelector:@selector(startLocation) withObject:nil afterDelay:10];
}


#pragma mark - BMKGeoCodeSearchDelegate
- (void)onGetGeoCodeResult:(BMKGeoCodeSearch *)searcher result:(BMKGeoCodeResult *)result errorCode:(BMKSearchErrorCode)error
{
//    NSArray* array = [NSArray arrayWithArray:_mapView.annotations];
//    [_mapView removeAnnotations:array];
//    array = [NSArray arrayWithArray:_mapView.overlays];
//    [_mapView removeOverlays:array];
//    if (error == 0) {
//        BMKPointAnnotation* item = [[BMKPointAnnotation alloc]init];
//        item.coordinate = result.location;
//        item.title = result.address;
//        [_mapView addAnnotation:item];
//        _mapView.centerCoordinate = result.location;
//        NSString* titleStr;
//        NSString* showmeg;
//        
//        titleStr = @"正向地理编码";
//        showmeg = [NSString stringWithFormat:@"经度:%f,纬度:%f",item.coordinate.latitude,item.coordinate.longitude];
//        
//        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:titleStr message:showmeg delegate:self cancelButtonTitle:nil otherButtonTitles:@"确定",nil];
//        [myAlertView show];
//    }
}

-(void) onGetReverseGeoCodeResult:(BMKGeoCodeSearch *)searcher result:(BMKReverseGeoCodeResult *)result errorCode:(BMKSearchErrorCode)error
{
    if (error == 0) {
        NSLog(@"%@", result);
    }
}



@end
