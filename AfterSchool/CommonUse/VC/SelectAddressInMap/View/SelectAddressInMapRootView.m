//
//  SelectAddressInMapRootView.m
//  AfterSchool
//
//  Created by lg on 15/11/2.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "SelectAddressInMapRootView.h"

@implementation SelectAddressInMapRootView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUp];
    }
    return self;
}

- (void)setUp {
    [self addSubview:self.mapView];
    [self addSubview:self.mapCenterPointView];
    [self addSubview:self.tableView];
    [self addSubview:self.navigationBarView];
}

- (NavigationBarView *)navigationBarView {
    if (!_navigationBarView) {
        _navigationBarView = [[NavigationBarView alloc] initWithTitle:@"地址"
                                                           titleColor:nil
                                                      backgroundColor:nil
                                              leftButtonBgViewOriginX:0
                                                 leftButtonImageWidth:22
                                                 leftButtonTitleColor:nil
                                                       leftButtonName:@"返回"
                                                  leftButtonImageName:@"navigation_bar_return_button_left_arrow"
                                                      rightButtonName:nil
                                                 rightButtonImageName:nil];
    }
    return _navigationBarView;
}

- (BMKMapView *)mapView {
    if (!_mapView) {
        _mapView = [[BMKMapView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height/2)];
        
        [_mapView setTrafficEnabled:NO];//路况
        [_mapView setBaiduHeatMapEnabled:NO];//热力图
        [_mapView setZoomEnabled:YES];//多点缩放
        [_mapView setZoomEnabledWithTap:NO];//点击缩放
        [_mapView setScrollEnabled:YES];//移动地图
        [_mapView setOverlookEnabled:NO];//仰俯视
        [_mapView setRotateEnabled:NO];//旋转
        [_mapView setShowMapScaleBar:NO];//比例尺
        [_mapView setChangeWithTouchPointCenterEnabled:NO];//以手势中心点为轴进行旋转和缩放
        
        [_mapView setShowsUserLocation:YES];//显示定位图层
        //[_mapView setIsSelectedAnnotationViewFront:YES];//总是让选中的标注显示在最前面
        
        [_mapView setUserTrackingMode:BMKUserTrackingModeNone];
        [_mapView setZoomLevel:16];//默认视图级别
    }
    return _mapView;
}

- (UIImageView *)mapCenterPointView {
    if (!_mapCenterPointView) {
        _mapCenterPointView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"anjuke_icon_itis_position"]];
        [_mapCenterPointView setCenter:self.mapView.center];
    }
    return _mapCenterPointView;
}

- (UITableView *)tableView {
    if (!_tableView) {

        CGRect frame = CGRectMake(0, self.bounds.size.height/2, self.bounds.size.width, self.bounds.size.height/2);
        _tableView = [[UITableView alloc] initWithFrame:frame style:UITableViewStylePlain];
    }
    return _tableView;
}

@end
