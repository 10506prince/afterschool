//
//  SelectAddressInMapRootView.h
//  AfterSchool
//
//  Created by lg on 15/11/2.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <BaiduMapAPI_Map/BMKMapView.h>
#import "NavigationBarView.h"

@interface SelectAddressInMapRootView : UIView

@property (nonatomic, strong) NavigationBarView * navigationBarView;

@property (nonatomic, strong) BMKMapView * mapView;
@property (nonatomic, strong) UIImageView * mapCenterPointView;

@property (nonatomic, strong) UITableView * tableView;


@end
