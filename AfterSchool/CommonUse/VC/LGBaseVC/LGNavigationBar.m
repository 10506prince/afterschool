//
//  LGNavigationBar.m
//  AfterSchool
//
//  Created by lg on 15/11/3.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGNavigationBar.h"
#import "Common.h"

@implementation LGNavigationBar

- (void)dealloc
{
    NSLog(@"%s", __FUNCTION__);
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setUp];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self setUp];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUp];
    }
    return self;
}

- (void)setUp {
    [self setBarStyle:UIBarStyleDefault];
    [self setTintColor:[UIColor redColor]];
    [self setBarTintColor:[UIColor clearColor]];
    [self setTranslucent:YES];
//    if (iOS7) {
//        self.translucent = YES;
//    } else {
//        self.translucent = NO;
//    }
}
@end
