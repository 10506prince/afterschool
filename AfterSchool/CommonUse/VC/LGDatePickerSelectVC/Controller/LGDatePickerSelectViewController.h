//
//  LGPickerSelectViewController.h
//  AfterSchool
//
//  Created by lg on 15/12/3.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef void (^LGDatePickerSelectViewControllerBlock)(NSDate * date);

@interface LGDatePickerSelectViewController : UIViewController

- (void)addConfirmClickBlock:(LGDatePickerSelectViewControllerBlock)confirmClickBlock;
- (void)addCancelClickBlock:(LGDatePickerSelectViewControllerBlock)cancelClickBlock;

- (void)showInView:(UIView *)view;

- (NSDate *)defaultDate;

@end
