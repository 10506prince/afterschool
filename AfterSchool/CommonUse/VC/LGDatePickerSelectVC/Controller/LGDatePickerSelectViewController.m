//
//  LGPickerSelectViewController.m
//  AfterSchool
//
//  Created by lg on 15/12/3.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGDatePickerSelectViewController.h"
#import "UIButton+SetBackgroundColor.h"

@interface LGDatePickerSelectViewController ()

@property (nonatomic, strong) IBInspectable UIColor * borderColor;
@property (nonatomic, assign) IBInspectable CGFloat borderWidth;
@property (nonatomic, assign) IBInspectable CGFloat cornerRadius;

@property (nonatomic, weak) IBOutlet UIView * backView;
@property (nonatomic, weak) IBOutlet UIDatePicker * pickerView;
@property (nonatomic, weak) IBOutlet UIButton * cancelBtn;
@property (nonatomic, weak) IBOutlet UIButton * confirmBtn;

@property (copy) LGDatePickerSelectViewControllerBlock confirmClickBlock;
@property (copy) LGDatePickerSelectViewControllerBlock cancelClickBlock;

- (void)dismiss;

@end

@implementation LGDatePickerSelectViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.cancelBtn.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [self.cancelBtn.layer setBorderWidth:1];
    [self.cancelBtn setBackgroundColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
    
    [self.confirmBtn.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [self.confirmBtn.layer setBorderWidth:1];
    [self.confirmBtn setBackgroundColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
    
    [self.backView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [self.backView.layer setBorderWidth:1];
    [self.backView.layer setCornerRadius:8];
    [self.backView.layer setMasksToBounds:YES];
    
    
    NSDate * today = [NSDate date];
    [self.pickerView setMinimumDate:today];
    [self.pickerView setDatePickerMode:UIDatePickerModeDateAndTime];
}

- (void)setBorderColor:(UIColor *)borderColor {
    _borderColor = borderColor;
    
    [self.backView.layer setBorderColor:_borderColor.CGColor];
}

- (void)setBorderWidth:(CGFloat)borderWidth {
    _borderWidth = borderWidth;
    
    [self.backView.layer setBorderWidth:_borderWidth];
}

- (void)setCornerRadius:(CGFloat)cornerRadius {
    _cornerRadius = cornerRadius;
    
    [self.backView.layer setCornerRadius:_cornerRadius];
}

#pragma mark - Action
- (IBAction)cancelAction:(id)sender {
    
    LGDatePickerSelectViewControllerBlock block = self.cancelClickBlock;
    if (block) {
        block(nil);
    }
    [self dismiss];
}
- (IBAction)confirmAction:(id)sender {
    
    NSDate * date = self.pickerView.date;
    NSDate * datee = [self targetDate:date];
    
    LGDatePickerSelectViewControllerBlock block = self.confirmClickBlock;
    if (block) {
        block(datee);
    }
    [self dismiss];
}

#pragma mark - PublicMethod
- (void)addConfirmClickBlock:(LGDatePickerSelectViewControllerBlock)confirmClickBlock {
    self.confirmClickBlock = confirmClickBlock;
}

- (void)addCancelClickBlock:(LGDatePickerSelectViewControllerBlock)cancelClickBlock {
    self.cancelClickBlock = cancelClickBlock;
}

- (void)showInView:(UIView *)view {
    
    NSDate * today = [NSDate date];
    [self.pickerView setMinimumDate:today];
    [self.pickerView setDate:today animated:YES];
    
    [view addSubview:self.view];
    
    CGRect fram = self.backView.frame;
    CGFloat originY = fram.origin.y;
    
    fram.origin.y = [UIScreen mainScreen].bounds.size.height;
    
    self.backView.frame = fram;
    [self.view setBackgroundColor:[UIColor colorWithWhite:0 alpha:0]];
    [UIView animateWithDuration:0.3 animations:^{
        
        CGRect fram = self.backView.frame;
        fram.origin.y = originY;
        
        [self.view setBackgroundColor:[UIColor colorWithWhite:0 alpha:0.3]];
        self.backView.frame = fram;
    }];
}

- (void)dismiss {
    [self.view removeFromSuperview];
}

- (NSDate *)targetDate:(NSDate*)date {

    NSCalendar * calendar = [NSCalendar currentCalendar];
    NSDateComponents * dateComponents = [calendar components:NSCalendarUnitMinute |NSCalendarUnitHour | NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:date];
    
    dateComponents.hour++;
    
    NSDate * datee = [calendar dateFromComponents:dateComponents];
    
    return datee;
}

- (NSDate *)defaultDate {
    NSDate * date = [NSDate date];
    return [self targetDate:date];
}

@end
