//
//  LGDatePickerViewController.m
//  AfterSchool
//
//  Created by lg on 15/12/23.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGDatePickerViewController.h"

@interface LGDatePickerViewController ()

@property (nonatomic, strong) NSCalendar * calendar;

@property (nonatomic, strong) IBOutlet UIPickerView * pickerView;

//@property (nonatomic, strong)
@end

@implementation LGDatePickerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}


@end
