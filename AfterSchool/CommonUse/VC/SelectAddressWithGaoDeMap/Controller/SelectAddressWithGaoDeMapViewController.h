//
//  SelectAddressWithGaoDeMap.h
//  AfterSchool
//
//  Created by lg on 15/11/3.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <RongIMKit/RongIMKit.h>
#import "NavigationBarView.h"

@interface SelectAddressWithGaoDeMapViewController : RCLocationPickerViewController

@property (nonatomic, strong) NavigationBarView * navigationView;

@end
