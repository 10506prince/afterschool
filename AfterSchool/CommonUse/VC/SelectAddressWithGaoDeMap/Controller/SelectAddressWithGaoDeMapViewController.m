//
//  SelectAddressWithGaoDeMap.m
//  AfterSchool
//
//  Created by lg on 15/11/3.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "SelectAddressWithGaoDeMapViewController.h"

@implementation SelectAddressWithGaoDeMapViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view addSubview:self.navigationView];
}

- (NavigationBarView *)navigationView {
    if (!_navigationView) {
        _navigationView = [[NavigationBarView alloc] initWithTitle:@"选取位置"
                                                        titleColor:nil
                                                   backgroundColor:nil
                                           leftButtonBgViewOriginX:0
                                              leftButtonImageWidth:22
                                              leftButtonTitleColor:nil
                                                    leftButtonName:@"返回"
                                               leftButtonImageName:@"navigation_bar_return_button_left_arrow" 
                                                   rightButtonName:@"完成"
                                              rightButtonImageName:nil];
        
        [_navigationView.leftButton addTarget:self action:@selector(leftBarButtonItemPressed:) forControlEvents:UIControlEventTouchUpInside];
        [_navigationView.rightButton addTarget:self action:@selector(rightBarButtonItemPressed:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _navigationView;
}


- (void)leftBarButtonItemPressed:(id)sender {
    [super leftBarButtonItemPressed:sender];
}

- (void)rightBarButtonItemPressed:(id)sender {
    [super rightBarButtonItemPressed:sender];
    [self leftBarButtonItemPressed:nil];
}

@end
