//
//  ShareViewController.m
//  AfterSchool
//
//  Created by lg on 15/12/1.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGShareViewController.h"
#import "Common.h"
#import <ShareSDK/ShareSDK.h>

@interface LGShareViewController () {
    SSPublishContentMediaType publishContentMediaType;
    
}

@property (nonatomic, weak) IBOutlet UIView * shareView;
@property (nonatomic, weak) IBOutlet UIButton * cancelBtn;
@property (nonatomic, weak) IBOutlet UIButton * sinaBtn;
@property (nonatomic, weak) IBOutlet UIButton * weiXinBtn;
@property (nonatomic, weak) IBOutlet UIButton * weiXinTimeLineBtn;
@property (nonatomic, weak) IBOutlet UIButton * qqBtn;
@property (nonatomic, weak) IBOutlet UIButton * qzoneBtn;

@end

@implementation LGShareViewController

- (void)dealloc
{
    //NSLogSelfMethodName;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initData];
    [self initUserInterface];
}

- (void)initData {
    NSArray * btns = @[_sinaBtn,_weiXinBtn,_weiXinTimeLineBtn,_qqBtn,_qzoneBtn];
    int tags[5] = {ShareBtnTypeSinaWeibo,ShareBtnTypeWechatSession,ShareBtnTypeWechatTimeline,ShareBtnTypeqqFriend,ShareBtnTypeqZone};
    
    for (int i = 0; i < btns.count; i++) {
        UIButton * btn = btns[i];
        [btn addTarget:self action:@selector(shareBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [btn setTag:tags[i]];
    }
    
    [_cancelBtn addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
}
- (void)initUserInterface {
    //[self.view addSubview:self.rootView];
    [_shareView.layer setCornerRadius:8];
    [_shareView.layer setMasksToBounds:YES];
    
    UIView * line = [[UIView alloc] initWithFrame:CGRectMake(0, 272 - 38 - SINGLE_LINE_ADJUST_OFFSET, 300, SINGLE_LINE_WIDTH)];
    [line setBackgroundColor:[UIColor lightGrayColor]];
    [_shareView addSubview:line];
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [super touchesEnded:touches withEvent:event];
    [self backAction];
}
#pragma mark - Action
- (void)backAction {
    [self.view removeFromSuperview];
}

- (IBAction)shareBtnAction:(UIButton *)sender {
    ShareBtnType shareType = sender.tag;
    switch (shareType) {
        case ShareBtnTypeqqFriend:
        {
            [self creatShareWithShareType:ShareTypeQQ];
        }
            break;
        case ShareBtnTypeqZone:
        {
            [self creatShareWithShareType:ShareTypeQQSpace];
        }
            break;
        case ShareBtnTypeWechatTimeline:
        {
            [self creatShareWithShareType:ShareTypeWeixiTimeline];
        }
            break;
        case ShareBtnTypeWechatSession:
        {
            [self creatShareWithShareType:ShareTypeWeixiSession];
        }
            break;
        case ShareBtnTypeSinaWeibo:
        {
            [self creatShareWithShareType:ShareTypeSinaWeibo];
        }
            break;
            
        default:
            break;
    }
}

- (id<ISSContent>) getShareContentWithShareType:(ShareType)shareType {
    
    NSString *imagePath = [[NSBundle mainBundle] pathForResource:@"app_icon_180x180" ofType:@"png"];
    
    id <ISSCAttachment> image = [ShareSDK imageWithPath:imagePath];
    
    if (self.publishContentImage) {
//        UIImage 
        NSData * imageData = UIImagePNGRepresentation(self.publishContentImage);
        image = [ShareSDK imageWithData:imageData fileName:@"shareImage" mimeType:@"png"];
    }
    
    //构造分享内容
    id<ISSContent> shareContent = [ShareSDK content:@"你还在为上分难犯愁吗，你还在为找不到妹子彷徨吗，你还在为独自玩游戏感到无聊吗，上放学约，你的烦恼从此不在！"
                                     defaultContent:@"你还在为上分难犯愁吗，你还在为找不到妹子彷徨吗，你还在为独自玩游戏感到无聊吗，上放学约，你的烦恼从此不在！"
                                              image:image
                                              title:@"放学约"
                                                url:@"http://www.fangxueyue.com:9281/"
                                        description:@"你还在为上分难犯愁吗，你还在为找不到妹子彷徨吗，你还在为独自玩游戏感到无聊吗，上放学约，你的烦恼从此不在！"
                                          mediaType:SSPublishContentMediaTypeImage];
    
    //    SSPublishContentMediaTypeText = 0, /**< 文本 */
    //    SSPublishContentMediaTypeImage = 1, /**< 图片 */
    //    SSPublishContentMediaTypeNews = 2, /**< 新闻 */
    //    SSPublishContentMediaTypeMusic = 3, /**< 音乐 */
    //    SSPublishContentMediaTypeVideo = 4, /**< 视频 */
    //    SSPublishContentMediaTypeApp = 5, /**< 应用,仅供微信使用 */
    //    SSPublishContentMediaTypeNonGif = 6, /**< 非Gif消息,仅供微信使用 */
    //    SSPublishContentMediaTypeGif = 7 /**< Gif消息,仅供微信使用 */
    
    return shareContent;
}

- (void)creatShareWithShareType:(ShareType)shareType {
    
    //构造分享内容
    id<ISSContent> publishContent = [self getShareContentWithShareType:shareType];
    
    
    [ShareSDK shareContent:publishContent type:shareType authOptions:nil shareOptions:nil statusBarTips:YES result:^(ShareType type, SSResponseState state, id<ISSPlatformShareInfo> statusInfo, id<ICMErrorInfo> error, BOOL end) {
        if (state == SSPublishContentStateSuccess)
        {
            NSLog(NSLocalizedString(@"TEXT_SHARE_SUC", @"发表成功"));
        } else if (state == SSPublishContentStateFail)
        {
        } else if (state == SSPublishContentStateCancel) {
            
        }
    }];
}

@end
