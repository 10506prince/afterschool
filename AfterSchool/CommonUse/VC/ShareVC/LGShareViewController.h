//
//  LGShareViewController.h
//  AfterSchool
//
//  Created by lg on 15/12/1.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, ShareBtnType) {
    ShareBtnTypeSinaWeibo,///<新浪分享
    ShareBtnTypeWechatSession,///<微信好友分享
    ShareBtnTypeWechatTimeline,///<微信朋友圈分享
    ShareBtnTypeqqFriend,///<QQ好友分享
    ShareBtnTypeqZone///<QQ空间分享
};

@interface LGShareViewController : UIViewController

@property (nonatomic, strong) UIImage * publishContentImage;

@end
