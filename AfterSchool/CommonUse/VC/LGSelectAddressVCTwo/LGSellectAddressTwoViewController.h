//
//  LGSellectAddressTwoViewController.h
//  AfterSchool
//
//  Created by lg on 15/12/9.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGViewController.h"
#import "LGSelectAddressTwoRootView.h"
#import "LGSelectAddressViewController.h"

typedef void(^LgselectAddressTwoFinishBlock)(id result);

@interface LGSellectAddressTwoViewController : LGViewController

+ (instancetype)LGSellectAddressTwoViewControllerWithFinishBlock:(LgselectAddressTwoFinishBlock)finishBlock;

@end
