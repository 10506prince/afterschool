//
//  LGSellectAddressTwoViewController.m
//  AfterSchool
//
//  Created by lg on 15/12/9.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGSellectAddressTwoViewController.h"
#import <BaiduMapAPI_Map/BMKMapView.h>
#import "Common.h"

#import "LGLocationService.h"
#import "LGDefineNetServer.h"
#import "LGGeoCodeService.h"
#import "LGPlaceSuggestionService.h"
//#import "LGPlaceService.h"

@interface LGSellectAddressTwoViewController () <UITableViewDataSource,UITableViewDelegate,BMKMapViewDelegate>

@property (nonatomic, strong) NSArray <LGAddressPoi *> * nearPois;

@property (nonatomic, strong) NSMutableArray <LGSelectAddressResult *> * dataSource;
@property (nonatomic, assign) CLLocationCoordinate2D selectPt;
@property (nonatomic, strong) BMKUserLocation * currentLocation;


@property (nonatomic, strong) LGSelectAddressTwoRootView * rootView;
@property (copy) LgselectAddressTwoFinishBlock clickEnterActionBlock;

@end

@implementation LGSellectAddressTwoViewController


+ (instancetype)LGSellectAddressTwoViewControllerWithFinishBlock:(LgselectAddressTwoFinishBlock)finishBlock {
    LGSellectAddressTwoViewController * VC = [[LGSellectAddressTwoViewController alloc] init];
    VC.clickEnterActionBlock = finishBlock;
    
    return VC;
}

- (void)dealloc
{
    NSLogSelfMethodName;
    
    if (_rootView.mapView.delegate) {
        _rootView.mapView.delegate = nil;
    }
    if (_rootView.mapView) {
        _rootView.mapView = nil;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initData];
    [self initUserInterface];
}

- (void)initData {
    self.currentLocation = [LGLocationService sharedManager].userLocation;
    _selectPt = self.currentLocation.location.coordinate;
    
    self.dataSource = [NSMutableArray array];
}

- (void)initUserInterface {
    [self.view addSubview:self.rootView];
}

- (LGSelectAddressTwoRootView *)rootView {
    if (!_rootView) {
        _rootView = [[LGSelectAddressTwoRootView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        
        [_rootView.navigationBarView.leftButton addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
        [_rootView.navigationBarView.rightButton addTarget:self action:@selector(enterAction:) forControlEvents:UIControlEventTouchUpInside];
        
        [_rootView.mapView setDelegate:self];
        [_rootView.tableView setDelegate:self];
        [_rootView.tableView setDataSource:self];
    }
    return _rootView;
}


- (void)setNearPois:(NSArray<LGAddressPoi *> *)nearPois {
    _nearPois = nearPois;
    
    [self.dataSource removeAllObjects];
    
    for (LGAddressPoi * addr in _nearPois) {
        LGSelectAddressResult * result = [[LGSelectAddressResult alloc] initWithLGAddressPoi:addr];
        [self.dataSource addObject:result];
    }
    NSIndexSet * set = [NSIndexSet indexSetWithIndex:0];
    [self.rootView.tableView reloadSections:set withRowAnimation:UITableViewRowAnimationAutomatic];
}


#pragma mark - Action
- (IBAction)backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)enterAction:(id)sender {
    
    NSIndexPath * indexPath = [self.rootView.tableView indexPathForSelectedRow];
    
    LGSelectAddressResult * address = nil;
    
    if (indexPath != nil) {
        address = [self.dataSource objectAtIndex:indexPath.row];
    }
    
    LgselectAddressTwoFinishBlock block = self.clickEnterActionBlock;
    
    if (block) {
        block(address);
    }
    
    [self backAction:nil];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    LGSelectAddressTableViewCellOne * cell = [tableView dequeueReusableCellWithClass:[LGSelectAddressTableViewCellOne class]];
    
    LGSelectAddressResult * address = [self.dataSource objectAtIndex:indexPath.row];
    
    cell.name = address.name;
    cell.address = address.address;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return LGSelectAddressTableViewCellOneHeight;
}


#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell * cell = [tableView cellForRowAtIndexPath:indexPath];
    [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell * cell = [tableView cellForRowAtIndexPath:indexPath];
    [cell setAccessoryType:UITableViewCellAccessoryNone];
}

#pragma mark - BMKMapViewDelegate

- (void)mapViewDidFinishLoading:(BMKMapView *)mapView {
    [mapView updateLocationData:self.currentLocation];
    [mapView setCenterCoordinate:self.currentLocation.location.coordinate];
}

- (void)mapView:(BMKMapView *)mapView regionDidChangeAnimated:(BOOL)animated {
    
    [self getLocationInfo];
}


#pragma mark - 获取位置信息
- (void)getLocationInfo {
    CLLocationCoordinate2D pt = self.rootView.mapView.centerCoordinate;
    
    __weak typeof(self) weakSelf = self;
    
    [LGGeoCodeService reverseGeocodeWithPt:pt pois:YES success:^(id result) {
        
        if ([result isKindOfClass:[NSArray class]] && ((NSArray *)result).count > 0) {
            weakSelf.nearPois = result;
        }
    } failure:^(NSString *msg) {
    }];
}

//- (void)getPlaceListWithKeyWord:(NSString *)keyWord {
//    LGPlaceLocalSearch * search = [[LGPlaceLocalSearch alloc] init];
//    
//    NSString * selectCityName = [TDSingleton instance].cityName;
//    NSString * currenctCityName = [TDSingleton instance].currentCityName;
//    
//    search.keyWord = keyWord;
//    search.region = selectCityName.length > 0 ? selectCityName : currenctCityName.length > 0 ? currenctCityName : @"全国";
//    
//    [LGPlaceService getPlaceListWithSearchParam:search success:^(id result) {
//        if ([result isKindOfClass:[NSArray class]]) {
//            self.placeResults = result;
//        }
//    } failure:^{
//        
//    }];
//    
//}

//- (void)getSuggestionWithKeyWord:(NSString *)keyWord {
//    LGPlaceSuggestionSearch * search = [[LGPlaceSuggestionSearch alloc] init];
//    search.keyWord = keyWord;
//    search.region = @"全国";
//
//    __weak typeof(self) weakSelf = self;
//
//    [SVProgressHUD showWithStatus:@"正在搜索..."];
//    [LGPlaceSuggestionService getSuggestionListWithSearchParam:search success:^(id result) {
//        if ([result isKindOfClass:[NSArray class]]) {
//            weakSelf.suggestionResults = result;
//            NSIndexSet * indexSet = [NSIndexSet indexSetWithIndex:0];
//            [weakSelf.rootView.tableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationAutomatic];
//        }
//        [SVProgressHUD dismissWithSuccess:@"搜索完成"];
//    } failure:^{
//        weakSelf.suggestionResults = nil;
//        NSIndexSet * indexSet = [NSIndexSet indexSetWithIndex:0];
//        [weakSelf.rootView.tableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationAutomatic];
//        [SVProgressHUD dismissWithError:@"搜索失败"];
//    }];
//}


@end
