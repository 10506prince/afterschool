//
//  LGTabBarController.m
//  nvcTvcVcTest
//
//  Created by lg on 15/10/20.
//  Copyright © 2015年 lg. All rights reserved.
//

#import "LGTabBarController.h"

@interface LGTabBarController ()

@end

@implementation LGTabBarController

- (void)dealloc
{
    NSLog(@"%s 标签控制器释放了", __FUNCTION__);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
