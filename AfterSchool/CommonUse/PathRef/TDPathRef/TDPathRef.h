//
//  TDPathRef.h
//  AfterSchool
//
//  Created by Teson Draw on 1/4/16.
//  Copyright © 2016 Teson Draw. All rights reserved.
//


//#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>

@interface TDPathRef : NSObject

+ (CGPathRef)rightArrowWithStart:(CGPoint)start;

+ (CGPathRef)rightArrowWithStart:(CGPoint)start
                           width:(CGFloat)width
                          height:(CGFloat)height;

+ (CGPathRef)leftTriangleWithStart:(CGPoint)start;

+ (CGPathRef)leftTriangleWithStart:(CGPoint)start
                             width:(CGFloat)width
                            height:(CGFloat)height;

+ (CGPathRef)downTriangleWithStart:(CGPoint)start
                             width:(CGFloat)width
                            height:(CGFloat)height;

@end
