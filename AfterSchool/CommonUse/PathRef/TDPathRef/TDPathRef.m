//
//  TDPathRef.m
//  AfterSchool
//
//  Created by Teson Draw on 1/4/16.
//  Copyright © 2016 Teson Draw. All rights reserved.
//

#import "TDPathRef.h"

@implementation TDPathRef

+ (CGPathRef)rightArrowWithStart:(CGPoint)start {
    CGMutablePathRef path = CGPathCreateMutable();
    
    CGPathMoveToPoint(path, NULL, start.x, start.y);
    CGPathAddLineToPoint(path, NULL, start.x + 7, start.y + 7);
    CGPathAddLineToPoint(path, NULL, start.x, start.y + 14);
    
    UIBezierPath * bezierPath = [UIBezierPath bezierPathWithCGPath:path];
    CGPathRelease(path);
    
    return bezierPath.CGPath;
}

+ (CGPathRef)rightArrowWithStart:(CGPoint)start
                           width:(CGFloat)width
                          height:(CGFloat)height {
    
    CGMutablePathRef path = CGPathCreateMutable();
    
    CGPathMoveToPoint(path, NULL, start.x, start.y);
    CGPathAddLineToPoint(path, NULL, start.x + width, start.y + height / 2);
    CGPathAddLineToPoint(path, NULL, start.x, start.y + height);
    
    UIBezierPath * bezierPath = [UIBezierPath bezierPathWithCGPath:path];
    CGPathRelease(path);
    
    return bezierPath.CGPath;
}

+ (CGPathRef)leftTriangleWithStart:(CGPoint)start  {
    CGMutablePathRef path = CGPathCreateMutable();
    
    CGPathMoveToPoint(path, NULL, start.x, start.y);
    CGPathAddLineToPoint(path, NULL, start.x - 6, start.y + 6);
    CGPathAddLineToPoint(path, NULL, start.x, start.y + 12);
    
    UIBezierPath * bezierPath = [UIBezierPath bezierPathWithCGPath:path];
    CGPathRelease(path);
    
    return bezierPath.CGPath;
}

+ (CGPathRef)leftTriangleWithStart:(CGPoint)start
                             width:(CGFloat)width
                            height:(CGFloat)height
{
    CGMutablePathRef path = CGPathCreateMutable();
    
    CGPathMoveToPoint(path, NULL, start.x, start.y);
    CGPathAddLineToPoint(path, NULL, start.x - width, start.y + height / 2);
    CGPathAddLineToPoint(path, NULL, start.x, start.y + height);
    
    UIBezierPath * bezierPath = [UIBezierPath bezierPathWithCGPath:path];
    CGPathRelease(path);
    
    return bezierPath.CGPath;
}

+ (CGPathRef)downTriangleWithStart:(CGPoint)start
                             width:(CGFloat)width
                            height:(CGFloat)height
{
    CGMutablePathRef path = CGPathCreateMutable();
    
    CGPathMoveToPoint(path, NULL, start.x, start.y);
    CGPathAddLineToPoint(path, NULL, start.x + width / 2, start.y + height);
    CGPathAddLineToPoint(path, NULL, start.x + width, start.y);
    
    UIBezierPath * bezierPath = [UIBezierPath bezierPathWithCGPath:path];
    CGPathRelease(path);
    
    return bezierPath.CGPath;
}


@end
