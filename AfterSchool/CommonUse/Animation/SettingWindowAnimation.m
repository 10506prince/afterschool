//
//  SettingWindowAnimation.m
//  AfterSchool
//
//  Created by Teson Draw on 12/29/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "SettingWindowAnimation.h"
#import "MacrosDefinition.h"

@implementation SettingWindowAnimation


+ (void)showWithView:(UIView *)view {
    
    [UIView animateWithDuration:0.3 animations:^{
        view.center = CGPointMake(SCREEN_WIDTH + SCREEN_WIDTH / 2 - 55, SCREEN_HEIGHT / 2);
    } completion:^(BOOL finished) {
    }];
}

+ (void)hideWithView:(UIView *)view {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    
//    view.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1, 1);
    view.center = CGPointMake(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2);
    
    [UIView commitAnimations];
}


@end
