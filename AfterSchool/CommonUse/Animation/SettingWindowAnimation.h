//
//  SettingWindowAnimation.h
//  AfterSchool
//
//  Created by Teson Draw on 12/29/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface SettingWindowAnimation : NSObject

+ (void)showWithView:(UIView *)view;
+ (void)hideWithView:(UIView *)view;

@end
