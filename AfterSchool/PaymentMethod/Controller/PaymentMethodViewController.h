//
//  PaymentMethodViewController.h
//  AfterSchool
//
//  Created by Teson Draw on 12/8/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PaymentMethodRootView.h"

typedef void(^PaymentMethodViewControllerFinish)(BOOL result);

@interface PaymentMethodViewController : UIViewController

@property (nonatomic, strong) PaymentMethodRootView *rootView;
@property (nonatomic, assign) NSUInteger amount;

@property (nonatomic, copy) NSString *channel;

@property (copy) PaymentMethodViewControllerFinish finishBlock;

- (void)addFinishBlock:(PaymentMethodViewControllerFinish)finishBlock;

@end
