//
//  PaymentMethodViewController.m
//  AfterSchool
//
//  Created by Teson Draw on 12/8/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "PaymentMethodViewController.h"
#import "MacrosDefinition.h"
#import "Pingpp.h"
#import "SVProgressHUD.h"
#import "TDNetworking.h"
#import "TDSingleton.h"
#import "NotificationName.h"
#import "Common.h"
#import "TalkingData.h"

#import "NSString+PhoneNumber.h"

@implementation PaymentMethodViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initUserInterface];
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [TalkingData trackPageBegin:selfControllerName];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [TalkingData trackPageEnd:selfControllerName];
}
- (void)initUserInterface {
    [self.view addSubview:self.rootView];
    
    if (_amount > 0) {
        _rootView.amountTextField.text = [NSString stringWithFormat:@"%lu", (unsigned long)_amount];
    }
}

- (PaymentMethodRootView *)rootView {
    if (!_rootView) {
        _rootView = [[PaymentMethodRootView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
        [_rootView.navigationBarView.leftButton addTarget:self action:@selector(returnButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        [_rootView.alipayButton addTarget:self action:@selector(payButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [_rootView.weixinButton addTarget:self action:@selector(payButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _rootView;
}

- (void)returnButtonClicked {
    [self.navigationController popViewControllerAnimated:YES];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)payButtonClicked:(UIButton *)sender {
    /**
     *  关闭键盘
     */
    [_rootView.amountTextField resignFirstResponder];
    
    /**
     *  判断是否是数字
     */
    NSString *amountText = [_rootView.amountTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if (![amountText isNumber] && ![amountText isFloat]) {
        [SVProgressHUD showErrorWithStatus:@"请输入数字" duration:2];
        return;
    }
    
    CGFloat amount = [_rootView.amountTextField.text floatValue];
   
    switch (sender.tag) {
        case 100:
            _channel = @"alipay";
            break;
            
        case 101:
            _channel = @"wx";
            break;
            
        default:
            _channel = @"alipay";
            break;
    }
    
    NSString *amountString = [NSString stringWithFormat:@"%.0f", amount * 100];
    NSString *body = [NSString stringWithFormat:@"充值%.2f元", amount];
    [SVProgressHUD showErrorWithStatus:@"拼命加载中..."];
    NSDictionary *parameters = @{@"action":@"getCharge",
                                 @"amount":amountString,
                                 @"subject":@"充值",
                                 @"body":body,
                                 @"channel":_channel,
                                 @"sessionKey":[TDSingleton instance].sessionKey};

    NSLog(@"charge parameters:%@", parameters);

    [TDNetworking requestWithURL:[TDSingleton instance].gatewayServerBusinessURL
                      parameters:parameters
                         success:^(id responseObject)
     {
         NSLog(@"getCharge:%@", responseObject);
         if ([responseObject[@"result"] integerValue] == 1) {
             NSDictionary *charge = responseObject[@"charge"];
             [self createPaymentWithCharge:charge];
          } else {
             [SVProgressHUD dismissWithError:[NSString stringWithFormat:@"%@", responseObject[@"msg"]]];
         }
     } failure:^(NSError *error) {
         NSLog(@"didReceiveMessageNotification error:%@", error);
         [SVProgressHUD dismissWithError:[NSString stringWithFormat:@"%@", error.description]];
     }];
}

- (void)addFinishBlock:(PaymentMethodViewControllerFinish)finishBlock {
    self.finishBlock = finishBlock;
}

- (void)createPaymentWithCharge:(NSDictionary *)charge {
    NSString *appURLScheme;
    if ([_channel isEqualToString:@"alipay"]) {
        appURLScheme = @"alipay19810104wl";
    } else {
        appURLScheme = @"wx9fdbe0ccbadfe82c";
    }
    
    __weak typeof(self) weakSelf = self;
    
    [Pingpp createPayment:charge
           viewController:self
             appURLScheme:appURLScheme
           withCompletion:^(NSString *result, PingppError *error)
    {
        PaymentMethodViewControllerFinish block = weakSelf.finishBlock;
        
        
        if ([result isEqualToString:@"success"]) {
            // 支付成功
            if (block) {
                block(YES);
            }
            
            [SVProgressHUD dismissWithSuccess:@"充值成功！"];

            [[NSNotificationCenter defaultCenter] postNotificationName:TDChargeSuccess object:nil];
            
            [self.navigationController popViewControllerAnimated:YES];
        } else {
            // 支付失败或取消
            if (block) {
                block(NO);
            }
            NSLog(@"Error: code=%lu msg=%@", (unsigned long)error.code, [error getMsg]);
            [SVProgressHUD dismissWithError:@"充值失败！"];
        }
    }];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [_rootView.amountTextField resignFirstResponder];
}

@end
