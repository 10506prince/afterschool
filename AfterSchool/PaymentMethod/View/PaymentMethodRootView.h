//
//  PaymentMethodRootView.h
//  AfterSchool
//
//  Created by Teson Draw on 12/8/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavigationBarView.h"

@interface PaymentMethodRootView : UIView

@property (nonatomic, strong) NavigationBarView *navigationBarView;

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *itemNameLabel;
@property (nonatomic, strong) UILabel *unitLabel;

@property (nonatomic, strong) UITextField *amountTextField;

@property (nonatomic, strong) UIButton *alipayButton;
@property (nonatomic, strong) UIButton *weixinButton;


@end
