//
//  PaymentMethodRootView.m
//  AfterSchool
//
//  Created by Teson Draw on 12/8/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "PaymentMethodRootView.h"
#import "UIButton+SetBackgroundColor.h"

@implementation PaymentMethodRootView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        
        [self addSubview:self.navigationBarView];
//        [self addSubview:self.titleLabel];
        [self addSubview:self.itemNameLabel];
        [self addSubview:self.amountTextField];
        [self addSubview:self.unitLabel];
        [self addSubview:self.alipayButton];
        [self addSubview:self.weixinButton];
    }
    return self;
}

- (NavigationBarView *)navigationBarView {
    if (!_navigationBarView) {
        _navigationBarView = [[NavigationBarView alloc] initWithTitle:@"充值" titleColor:[UIColor whiteColor] backgroundColor:nil leftButtonImageName:@"navigation_bar_return_button" rightButtonImageName:nil];
    }
    return _navigationBarView;
}


//- (UILabel *)titleLabel {
//    if (!_titleLabel) {
//        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 80, self.bounds.size.width, 20)];
//        _titleLabel.textColor = [UIColor blackColor];
//        _titleLabel.font = [UIFont systemFontOfSize:16];
//        _titleLabel.textAlignment = NSTextAlignmentCenter;
//        _titleLabel.text = @"充值";
//    }
//    return _titleLabel;
//}

- (UILabel *)itemNameLabel {
    if (!_itemNameLabel) {
        _itemNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 80, 60, 44)];
        _itemNameLabel.textColor = [UIColor blackColor];
        _itemNameLabel.font = [UIFont systemFontOfSize:16];
        _itemNameLabel.textAlignment = NSTextAlignmentLeft;
        _itemNameLabel.text = @"金额：";
    }
    return _itemNameLabel;
}

- (UITextField *)amountTextField {
    if (!_amountTextField) {
        _amountTextField = [[UITextField alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_itemNameLabel.frame), _itemNameLabel.frame.origin.y, 80, 44)];
        _amountTextField.layer.borderColor = [UIColor blackColor].CGColor;
        _amountTextField.layer.borderWidth = 1;
        _amountTextField.keyboardType = UIKeyboardTypeNumberPad;
    }
    return _amountTextField;
}

- (UILabel *)unitLabel {
    if (!_unitLabel) {
        _unitLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_amountTextField.frame) + 4, _amountTextField.frame.origin.y, 20, 44)];
        _unitLabel.textColor = [UIColor blackColor];
        _unitLabel.font = [UIFont systemFontOfSize:16];
        _unitLabel.text = @"元";
    }
    return _unitLabel;
}

- (UIButton *)alipayButton {
    if (!_alipayButton) {
        _alipayButton = [[UIButton alloc] initWithFrame:CGRectMake((self.bounds.size.width - 180) / 2, CGRectGetMaxY(_amountTextField.frame) + 20,  180, 44)];
        [_alipayButton setTitle:@"支付宝" forState:UIControlStateNormal];
        [_alipayButton setBackgroundColor:[UIColor colorWithRed:37/255.f green:183/255.f blue:237/255.0 alpha:1]];
        [_alipayButton setBackgroundColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
        _alipayButton.layer.cornerRadius = 5;
        _alipayButton.layer.masksToBounds = YES;
        _alipayButton.titleLabel.font = [UIFont systemFontOfSize:20];
        _alipayButton.tag = 100;
    }
    
    return _alipayButton;
}

- (UIButton *)weixinButton {
    if (!_weixinButton) {
        _weixinButton = [[UIButton alloc] initWithFrame:CGRectMake((self.bounds.size.width - 180) / 2, CGRectGetMaxY(_alipayButton.frame) + 20,  180, 44)];
        [_weixinButton setTitle:@"微信" forState:UIControlStateNormal];
        [_weixinButton setBackgroundColor:[UIColor colorWithRed:37/255.f green:183/255.f blue:237/255.0 alpha:1]];
        [_weixinButton setBackgroundColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
        _weixinButton.layer.cornerRadius = 5;
        _weixinButton.layer.masksToBounds = YES;
        _weixinButton.titleLabel.font = [UIFont systemFontOfSize:20];
        _weixinButton.tag = 101;
    }
    
    return _weixinButton;
}

@end
