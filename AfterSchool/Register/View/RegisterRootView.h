//
//  RegisterRootView.h
//  AfterSchool
//
//  Created by Teson Draw on 10/7/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavigationBarView.h"
#import "CustomTextField.h"
#import "TDTextField.h"
#import "UIUnderlinedButton.h"

@interface RegisterRootView : UIView

@property (nonatomic, strong) NavigationBarView *navigationBarView;
@property (nonatomic, strong) CustomTextField   *mobileNumberTextField;
@property (nonatomic, strong) TDTextField       *verificationCodeTextField;

@property (nonatomic, retain) UIButton          *getVerificationCodeButton;
@property (nonatomic, strong) UIButton          *nextStepButton;
@property (nonatomic, strong) UIButton          *userAgreementButton;

@property (nonatomic, strong) UILabel           *userAgreementLabel;

@end
