//
//  InputInfoRootView.h
//  AfterSchool
//
//  Created by Teson Draw on 10/7/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomTextField.h"
#import "NavigationBarView.h"
#import "OptionView.h"
#import "TDDatePickerView.h"
#import "TDSexPickerView.h"
#import "TDTextField.h"



@interface InputInfoRootView : UIView

@property (nonatomic, strong) NavigationBarView *navigationBarView;

@property (nonatomic, strong) UIImageView      *avatarImageView;
@property (nonatomic, strong) UIButton         *portraitButton;
@property (nonatomic, strong) UIButton         *completeButton;

@property (nonatomic, strong) TDTextField      *nameTextField;

@property (nonatomic, strong) OptionView       *selectSexOptionView;
@property (nonatomic, strong) OptionView       *selectBirthdayOptionView;

@property (nonatomic, strong) TDDatePickerView *datePickerView;
@property (nonatomic, strong) TDSexPickerView  *sexPickerView;

@end
