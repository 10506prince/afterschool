//
//  InputInfoRootView.m
//  AfterSchool
//
//  Created by Teson Draw on 10/7/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "InputInfoRootView.h"
#import "MacrosDefinition.h"
#import "UIButton+SetBackgroundColor.h"
#import "TDDateComponents.h"
#import "Common.h"

@implementation InputInfoRootView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
 
    if (self) {
        self.backgroundColor = [UIColor colorWithRed:241/255.f green:241/255.f blue:243/255.f alpha:1];
        [self addSubview:self.navigationBarView];
        
        [self addSubview:self.avatarImageView];
        [_avatarImageView addSubview:self.portraitButton];
        
        [self addSubview:self.nameTextField];
        [self addSubview:self.selectSexOptionView];
        [self addSubview:self.selectBirthdayOptionView];
        [self addSubview:self.completeButton];
    }
    return self;
}

- (NavigationBarView *)navigationBarView {
    if (!_navigationBarView) {
        _navigationBarView = [[NavigationBarView alloc] initWithTitle:@"注册" titleColor:[UIColor whiteColor] backgroundColor:nil leftButtonBgViewOriginX:0 leftButtonImageWidth:22 leftButtonTitleColor:[UIColor whiteColor] leftButtonName:@"上一页" leftButtonImageName:@"navigation_bar_return_button_left_arrow" rightButtonName:nil rightButtonImageName:nil];
    }
    return _navigationBarView;
}

- (UIImageView *)avatarImageView {
    if (!_avatarImageView) {
        _avatarImageView = [[UIImageView alloc] initWithFrame:CGRectMake((SCREEN_WIDTH - 100) / 2, 64 + 32, 100, 100)];
        _avatarImageView.layer.borderColor = [UIColor colorWithRed:223/255.f green:225/255.f blue:229/255.f alpha:1].CGColor;
        _avatarImageView.layer.borderWidth = 1;
        _avatarImageView.image = [UIImage imageNamed:@"register_select_avatar_background"];
        _avatarImageView.layer.cornerRadius = 50;
        _avatarImageView.layer.masksToBounds = YES;
        _avatarImageView.userInteractionEnabled = YES;
    }
    
    return _avatarImageView;
}

- (UIButton *)portraitButton {
    if (!_portraitButton) {
        _portraitButton = [[UIButton alloc] initWithFrame:_avatarImageView.bounds];
    }
    return _portraitButton;
}

- (TDTextField *)nameTextField {
    if (!_nameTextField) {
        _nameTextField = [[TDTextField alloc] initWithFrame:CGRectMake(20, CGRectGetMaxY(_avatarImageView.frame) + 20, SCREEN_WIDTH - 40, 44)];
        _nameTextField.placeholder = @"请输入姓名";
        _nameTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
        _nameTextField.returnKeyType = UIReturnKeyDefault;
        _nameTextField.tag = 100;
        _nameTextField.keyboardType = UIKeyboardTypeDefault;
        _nameTextField.autocorrectionType = UITextAutocorrectionTypeNo;

        _nameTextField.backgroundColor = [UIColor whiteColor];
        NSLog(@"nameTextField font:%@", _nameTextField.font);
    }
    
    return _nameTextField;
}

- (OptionView *)selectSexOptionView {
    if (!_selectSexOptionView) {
        _selectSexOptionView = [[OptionView alloc] initWithFrame:CGRectMake(20, CGRectGetMaxY(_nameTextField.frame) + 10, SCREEN_WIDTH - 40, 44) title:@"请选择性别" titleTextColor:COLOR(201, 202, 202) titleFontSize:17 backgroundColor:[UIColor whiteColor] showArrow:YES];
    }
    return _selectSexOptionView;
}

- (OptionView *)selectBirthdayOptionView {
    if (!_selectBirthdayOptionView) {
        _selectBirthdayOptionView = [[OptionView alloc] initWithFrame:CGRectMake(20, CGRectGetMaxY(_selectSexOptionView.frame) + 10, SCREEN_WIDTH - 40, 44) title:@"请选择生日" titleTextColor:COLOR(201, 202, 202) titleFontSize:17 backgroundColor:[UIColor whiteColor] showArrow:YES];
    }
    return _selectBirthdayOptionView;
}

- (UIButton *)completeButton {
    if (!_completeButton) {
        _completeButton = [[UIButton alloc] initWithFrame:CGRectMake(20, CGRectGetMaxY(_selectBirthdayOptionView.frame) + 20, SCREEN_WIDTH - 40, 46)];
        [_completeButton setTitle:@"完成" forState:UIControlStateNormal];
        [_completeButton setBackgroundColor:APPLightColor];
        [_completeButton setBackgroundColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
 
        _completeButton.titleLabel.font = [UIFont systemFontOfSize:20];
        NSLog(@"_completeButton.titleLabel.font:%@", _completeButton.titleLabel.font);
    }
    return _completeButton;
}

- (TDDatePickerView *)datePickerView {
    if (!_datePickerView) {
        _datePickerView = [[TDDatePickerView alloc] initWithFrame:CGRectMake((SCREEN_WIDTH - 300) / 2, SCREEN_HEIGHT, 300, 244) dateComponents:[TDDateComponents dateComponentsWithYear:2000 month:6 day:15] targetPosition:CGPointMake((SCREEN_WIDTH - 300) / 2, SCREEN_HEIGHT - 300)];
    }
    return _datePickerView;
}

- (TDSexPickerView *)sexPickerView {
    if (!_sexPickerView) {
        _sexPickerView = [[TDSexPickerView alloc] initWithFrame:CGRectMake((SCREEN_WIDTH - 300) / 2, SCREEN_HEIGHT, 300, 244) targetPosition:CGPointMake((SCREEN_WIDTH - 300) / 2, SCREEN_HEIGHT - 300)];
    }
    return _sexPickerView;
}

@end
