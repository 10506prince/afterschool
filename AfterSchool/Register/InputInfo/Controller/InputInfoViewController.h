//
//  InputInfoViewController.h
//  AfterSchool
//
//  Created by Teson Draw on 10/7/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InputInfoRootView.h"
#import "RegisterModel.h"

@interface InputInfoViewController : UIViewController

@property (nonatomic, strong) InputInfoRootView *rootView;
@property (nonatomic, strong) RegisterModel     *registerModel;
@property (nonatomic, strong) UIImage           *portraitImage;

@end
