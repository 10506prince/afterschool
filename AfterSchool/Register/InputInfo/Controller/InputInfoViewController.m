//
//  InputInfoViewController.m
//  AfterSchool
//
//  Created by Teson Draw on 10/7/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "InputInfoViewController.h"
#import "MacrosDefinition.h"

#import "TesonAlertView.h"
#import "Toast+UIView.h"
#import "UIImage+ScaleImage.h"
#import "TDNetworking.h"

#import "TDSingleton.h"


#import "SelectEntranceViewController.h"
#import "SVProgressHUD.h"
#import "LoginGatewayServer.h"

#import "LGDefineNetServer.h"

#import "TDMixedCryptogram.h"

@interface InputInfoViewController () <TDDatePickerViewDelegate, TDSexPickerViewDelegate, UIActionSheetDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@end

@implementation InputInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initUserInterface];
}

- (void)initUserInterface {
    [self.view addSubview:self.rootView];
}

- (InputInfoRootView *)rootView {
    if (!_rootView) {
        _rootView = [[InputInfoRootView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
        
        [_rootView.navigationBarView.leftButton addTarget:self action:@selector(returnButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        
        [_rootView.portraitButton addTarget:self action:@selector(portraitButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        
        [_rootView.selectSexOptionView.optionButton addTarget:self action:@selector(selectSexButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        
        [_rootView.selectBirthdayOptionView.optionButton addTarget:self action:@selector(selectBirthButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        
        [_rootView.completeButton addTarget:self action:@selector(completeButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    }
    return _rootView;
}

- (void)returnButtonClicked {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)portraitButtonClicked {
    UIActionSheet* actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:nil
                                  delegate:self
                                  cancelButtonTitle:@"取消"
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:@"照相机", @"图库", nil];
    
    [actionSheet showInView:self.view];
}

//选择图片来源
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSLog(@"%ld", (long)buttonIndex);
    switch (buttonIndex) {
        case 0:
        {
            //状态栏字体颜色改为黑色
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
            
            UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
            imagePicker.delegate = self;
            imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;//照相机
            imagePicker.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
            imagePicker.allowsEditing = YES;
            
            [self presentViewController:imagePicker animated:YES completion:nil];
        }
            break;
            
        case 1:
        {
            //状态栏字体颜色改为黑色
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
            
            UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
            imagePicker.delegate = self;
            imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;//图库
            imagePicker.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
            imagePicker.allowsEditing = YES;
            
            [self presentViewController:imagePicker animated:YES completion:nil];
        }
            break;
    }
}

//完成选取
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    NSLog(@"Info:%@", info);
    
    //状态栏改为白色
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    if ([info[@"UIImagePickerControllerMediaType"] isEqualToString:@"public.image"]) {
        _portraitImage = info[@"UIImagePickerControllerEditedImage"];
        
        NSLog(@"image size:%@", NSStringFromCGSize(_portraitImage.size));
        
        _portraitImage = [UIImage scaleImage:_portraitImage toSize:CGSizeMake(200, 200)];
        
        NSLog(@"after crop image size:%@", NSStringFromCGSize(_portraitImage.size));
        
        self.rootView.avatarImageView.image = _portraitImage;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

// 取消选取
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    //状态栏改为白色
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    [picker dismissViewControllerAnimated:YES completion:^{
        NSLog(@"did cancel imagePickerController");
    }];
}

/**
 *  选择性别
 */
- (void)selectSexButtonClicked {
    [_rootView.nameTextField resignFirstResponder];
    [self.view addSubview:_rootView.sexPickerView];
    _rootView.sexPickerView.delegate = self;
    [_rootView.sexPickerView moveToTargetPositon];
}

- (void)sexPickerViewConfirmButtonClicked:(NSString *)result {
    [TesonAlertView showMsg:@"性别信息一旦提交，将无法更改" cancelDelay:3];
    _rootView.selectSexOptionView.titleLabel.text = result;
}

/**
 *  选择生日
 */
- (void)selectBirthButtonClicked {
    [_rootView.nameTextField resignFirstResponder];
    
    [self.view addSubview:_rootView.datePickerView];
    _rootView.datePickerView.delegate = self;
    [_rootView.datePickerView moveToTargetPositon];
}

- (void)datePickerViewConfirmButtonClicked:(NSString *)date {
    _rootView.selectBirthdayOptionView.titleLabel.text = date;
}

- (void)completeButtonClicked {
    if ([self verifyName] && [self verifySex] && [self verifyBirthday]) {
        [self submitImage];
    }
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [_rootView.nameTextField resignFirstResponder];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (BOOL)verifyName {
    NSString *tempString = [_rootView.nameTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if (tempString.length == 0) {
        [self.view makeToast:@"请输入昵称" duration:2 position:[NSValue valueWithCGPoint:CGPointMake((SCREEN_WIDTH / 2), 110)]];
        return NO;
    }
    
    NSUInteger stringLength = [self lengthOfString:tempString];
    
    if (stringLength > 16 || stringLength < 3) {
        [self.view makeToast:@"昵称长度应在3~16个字符之间" duration:2 position:[NSValue valueWithCGPoint:CGPointMake((SCREEN_WIDTH / 2), 110)]];
        return NO;
    }
    
    return YES;
}

- (NSUInteger)lengthOfString:(NSString *)string {
    NSUInteger length = 0;
    NSUInteger chineseCharacterNumber = 0;
    
    for (int i=0; i<string.length; ++i)
    {
        NSRange range = NSMakeRange(i, 1);
        NSString *subString = [string substringWithRange:range];
        const char    *cString = [subString UTF8String];
        if (strlen(cString) == 3)
        {
            chineseCharacterNumber += 1;
        }
    }
    
    length = string.length + chineseCharacterNumber;
    
    return length;
}

- (BOOL)verifySex {
    if (_rootView.selectSexOptionView.titleLabel.text.length != 1) {
        [self.view makeToast:@"请选择性别" duration:2 position:[NSValue valueWithCGPoint:CGPointMake((SCREEN_WIDTH / 2), 110)]];
        return NO;
    }
    
    return YES;
}

- (BOOL)verifyBirthday {
    if (_rootView.selectBirthdayOptionView.titleLabel.text.length != 10) {
        [self.view makeToast:@"请选择生日" duration:2 position:[NSValue valueWithCGPoint:CGPointMake((SCREEN_WIDTH / 2), 110)]];
        return NO;
    }
    
    return YES;
}

/**
 *  上传图片成功后再上传注册信息
 */
- (void)submitImage {
    
    if (_portraitImage) {
        
        NSData *imageData = UIImagePNGRepresentation(_portraitImage);
        
        [SVProgressHUD showWithStatus:nil maskType:SVProgressHUDMaskTypeClear];
        [LGDefineNetServer uploadUserIcon:imageData account:self.registerModel.mobileNumber success:^(NSString *objectKey) {
            [SVProgressHUD dismiss];
            [self submitRegisterInfoWithObjectKey:objectKey];
        } failure:^(NSString *msg) {
            [SVProgressHUD dismissWithError:msg];
        } progress:^(NSString *progressMsg) {
            [SVProgressHUD showWithStatus:progressMsg maskType:SVProgressHUDMaskTypeClear];
        }];
    } else {
        [self submitRegisterInfoWithObjectKey:@""];
    }
}

///**
// *  上传图片成功后再上传注册信息
// */
//- (void)submitImage {
//
//    if (_portraitImage) {
//        
//        NSData *imageData = UIImagePNGRepresentation(_portraitImage);
//    
//        NSUInteger length = [imageData length]/1024;
//        NSLog(@"image length:%ld", (unsigned long)length);
//        NSString *mimeTypeString;
//        NSString *fileName;
//        
//        if (imageData) {
//            mimeTypeString = @"image/png";
//            fileName = @"icon.png";
//        } else {
//            imageData = UIImageJPEGRepresentation(_portraitImage, 1);
//            mimeTypeString = @"image/jpeg";
//            fileName = @"icon.jpg";
//        }
//        
//        NSLog(@"mimeType:%@", mimeTypeString);
//
//        NSDictionary *parameters = @{@"userName":_registerModel.mobileNumber,
//                                         @"time":_registerModel.time,
//                                         @"sign":_registerModel.sign};
//        
//        [TDNetworking requestWithURL:[TDSingleton instance].uploadImageURL
//                          parameters:parameters
//                    constructingBody:^(id<AFMultipartFormData> formData)
//        {
//            [formData appendPartWithFileData:imageData
//                                        name:@"icon"
//                                    fileName:fileName
//                                    mimeType:mimeTypeString];
//        } success:^(id responseObject) {
//            NSLog(@"submitImage result:%@", responseObject);
//            if ([responseObject[@"result"] integerValue] == 1) {
//                [TDSingleton instance].portraitImageURL = [NSString stringWithFormat:@"%@", responseObject[@"imageWebURL"]];
//                [self submitRegisterInfo];
//            } else {
//                [self.view makeToast:[NSString stringWithFormat:@"%@", responseObject[@"msg"]] duration:2 position:[NSValue valueWithCGPoint:CGPointMake((SCREEN_WIDTH / 2), 110)]];
//            }
//        } failure:^(NSError *error) {
//            NSLog(@"error:%@", error);
//            [self.view makeToast:@"连接出错，请检查网络" duration:2 position:@"center"];
//        }];
//    } else {
//        [self submitRegisterInfo];
//    }
//}

- (void)submitRegisterInfoWithObjectKey:(NSString *)objectKey {
    NSString *nickName = [_rootView.nameTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *sex = [_rootView.selectSexOptionView.titleLabel.text isEqualToString:@"男"] ? @"1" : @"0";
    NSString *birthday = _rootView.selectBirthdayOptionView.titleLabel.text;
    
    NSString *password = [_registerModel.mobileNumber substringFromIndex:_registerModel.mobileNumber.length - 6];
    NSString *mixedCryptogram = [TDMixedCryptogram fromPassword:password];
    
    NSDictionary *parameters = @{@"action":@"step3",
                                 @"userName":_registerModel.mobileNumber,
                                 @"code":_registerModel.verificationCode,
                                 @"nickName":nickName,
                                 @"sex":sex,
                                 @"birth":birthday,
                                 @"icon":objectKey,
                                 @"pwd":mixedCryptogram};
    
    [TDNetworking requestWithURL:[TDSingleton instance].URL.registerURL
                      parameters:parameters
                         success:^(id responseObject)
     {
         NSLog(@"step3:submitRegisterInfo result:%@", responseObject);
         if ([responseObject[@"result"] integerValue] == 1) {
             
             /*
              LGPoiUserInfo * poiUserInfo = [[LGPoiUserInfo alloc] init];
              //poiUserInfo.user_id = [responseObject objectForKey:@""];
              [LGDefineNetServer creatOnePoiData:poiUserInfo success:^(id result) {
              NSLog(@"%@", result);
              } failure:^(NSError *error) {
              }];
              */
             
             [self loginGatewayServerWithData:responseObject];
         } else {
             [self.view makeToast:[NSString stringWithFormat:@"%@", responseObject[@"msg"]] duration:2 position:[NSValue valueWithCGPoint:CGPointMake((SCREEN_WIDTH / 2), 110)]];
         }
     } failure:^(NSError *error) {
         NSLog(@"error:%@", error);
         [self.view makeToast:@"连接出错，请检查网络" duration:2 position:@"center"];
     }];
}

//- (void)submitRegisterInfo {
//    NSString *nickName = [_rootView.nameTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
//    NSString *sex = [_rootView.selectSexOptionView.titleLabel.text isEqualToString:@"男"] ? @"1" : @"0";
//    NSString *birthday = _rootView.selectBirthdayOptionView.titleLabel.text;
//    
//    NSString *password = [_registerModel.mobileNumber substringFromIndex:_registerModel.mobileNumber.length - 6];
//    NSString *mixedCryptogram = [TDMixedCryptogram fromPassword:password];
//    
//    NSDictionary *parameters = @{@"action":@"step3",
//                               @"userName":_registerModel.mobileNumber,
//                                   @"code":_registerModel.verificationCode,
//                               @"nickName":nickName,
//                                    @"sex":sex,
//                                  @"birth":birthday,
//                                   @"icon":[TDSingleton instance].portraitImageURL,
//                                    @"pwd":mixedCryptogram};
//    
//    [TDNetworking requestWithURL:[TDSingleton instance].URL.registerURL
//                      parameters:parameters
//                         success:^(id responseObject)
//    {
//        NSLog(@"step3:submitRegisterInfo result:%@", responseObject);
//        if ([responseObject[@"result"] integerValue] == 1) {
//            
//            /*
//            LGPoiUserInfo * poiUserInfo = [[LGPoiUserInfo alloc] init];
//            //poiUserInfo.user_id = [responseObject objectForKey:@""];
//            [LGDefineNetServer creatOnePoiData:poiUserInfo success:^(id result) {
//                NSLog(@"%@", result);
//            } failure:^(NSError *error) {
//            }];
//             */
//            
//            [self loginGatewayServerWithData:responseObject];
//        } else {
//            [self.view makeToast:[NSString stringWithFormat:@"%@", responseObject[@"msg"]] duration:2 position:[NSValue valueWithCGPoint:CGPointMake((SCREEN_WIDTH / 2), 110)]];
//        }
//    } failure:^(NSError *error) {
//        NSLog(@"error:%@", error);
//        [self.view makeToast:@"连接出错，请检查网络" duration:2 position:@"center"];
//    }];
//}

- (void)loginGatewayServerWithData:(NSDictionary *)data {
    [TDSingleton instance].account = _registerModel.mobileNumber;

    //初始密码为手机号后6位
    [TDSingleton instance].password = [_registerModel.mobileNumber substringFromIndex:(_registerModel.mobileNumber.length - 6)];
    [TDSingleton instance].autoLogin = YES;
    
    [SVProgressHUD showWithStatus:@"正在登录服务器..." maskType:2];
    
    [LoginGatewayServer loginGatewayServerWithData:data
                                      mobileNumber:_registerModel.mobileNumber
                                           success:^
    {
        [SVProgressHUD dismiss];
        [self enterMainWindow];
    } failure:^(NSString *message) {
        [SVProgressHUD dismissWithError:message];
    }];
}

- (void)enterMainWindow {
    UIViewController * rootVc = [self.navigationController.viewControllers firstObject];
    if ([rootVc isKindOfClass:[SelectEntranceViewController class]]) {
        SelectEntranceViewController * vc = (SelectEntranceViewController *)rootVc;
        [vc loginSuccess];
    }
}

@end
