//
//  RegisterModel.h
//  AfterSchool
//
//  Created by Teson Draw on 10/8/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RegisterModel : NSObject

@property (nonatomic, assign) NSInteger secondsCountDown;
@property (nonatomic, copy)   NSString  *verificationCode;
@property (nonatomic, copy)   NSString  *mobileNumber;
@property (nonatomic, copy)   NSString  *time;//验证用
@property (nonatomic, copy)   NSString  *sign;//验证用

@end
