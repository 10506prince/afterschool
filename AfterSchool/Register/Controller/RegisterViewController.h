//
//  RegisterViewController.h
//  AfterSchool
//
//  Created by Teson Draw on 10/7/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RegisterRootView.h"
#import "RegisterModel.h"

@interface RegisterViewController : UIViewController

@property (nonatomic, strong) RegisterRootView *rootView;
@property (nonatomic, strong) RegisterModel    *model;

@property (nonatomic, strong) NSTimer          *timer;
@property (nonatomic, strong) NSThread         *runTimerThread;

@end
