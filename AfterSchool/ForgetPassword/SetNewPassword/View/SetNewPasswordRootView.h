//
//  SetNewPasswordRootView.h
//  AfterSchool
//
//  Created by Teson Draw on 10/7/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavigationBarView.h"
#import "CustomTextField.h"

@interface SetNewPasswordRootView : UIView

@property (nonatomic, strong) NavigationBarView *navigationBarView;
@property (nonatomic, strong) CustomTextField   *passwordTextField;
@property (nonatomic, strong) CustomTextField   *confirmPasswordTextField;
@property (nonatomic, strong) UIButton          *completeButton;

@end
