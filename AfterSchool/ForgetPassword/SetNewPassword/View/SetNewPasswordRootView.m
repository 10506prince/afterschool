//
//  SetNewPasswordRootView.m
//  AfterSchool
//
//  Created by Teson Draw on 10/7/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "SetNewPasswordRootView.h"
#import "MacrosDefinition.h"
#import "UIButton+SetBackgroundColor.h"


@implementation SetNewPasswordRootView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        
        [self addSubview:self.navigationBarView];
        [self addSubview:self.passwordTextField];
        [self addSubview:self.confirmPasswordTextField];
        [self addSubview:self.completeButton];
    }
    return self;
}

- (NavigationBarView *)navigationBarView {
    if (!_navigationBarView) {
        _navigationBarView = [[NavigationBarView alloc] initWithTitle:@"设置密码" titleColor:[UIColor whiteColor] backgroundColor:[UIColor colorWithRed:39/255.f green:39/255.f blue:41/255.f alpha:1] leftButtonName:nil leftButtonImageName:@"navigation_bar_return_button" rightButtonName:nil rightButtonImageName:nil];
    }
    return _navigationBarView;
}

- (CustomTextField *)passwordTextField {
    if (!_passwordTextField) {
        _passwordTextField = [[CustomTextField alloc] initWithFrame:CGRectMake(20, 64 + 30, SCREEN_WIDTH - 40, 50)];
        _passwordTextField.placeholder = @"输入密码";
        _passwordTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
        _passwordTextField.returnKeyType = UIReturnKeyNext;
        _passwordTextField.tag = 100;
        _passwordTextField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
        _passwordTextField.autocorrectionType = UITextAutocorrectionTypeNo;
        _passwordTextField.layer.borderColor = [UIColor blackColor].CGColor;
        _passwordTextField.layer.borderWidth = 1;
        _passwordTextField.layer.cornerRadius = 5;
    }
    
    return _passwordTextField;
}

- (CustomTextField *)confirmPasswordTextField {
    if (!_confirmPasswordTextField) {
        _confirmPasswordTextField = [[CustomTextField alloc] initWithFrame:CGRectMake(20, CGRectGetMaxY(_passwordTextField.frame) + 20, SCREEN_WIDTH - 40, 50)];
        _confirmPasswordTextField.placeholder = @"再次输入密码";
        _confirmPasswordTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
        _confirmPasswordTextField.returnKeyType = UIReturnKeyNext;
        _confirmPasswordTextField.tag = 100;
        _confirmPasswordTextField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
        _confirmPasswordTextField.autocorrectionType = UITextAutocorrectionTypeNo;
        _confirmPasswordTextField.layer.borderColor = [UIColor blackColor].CGColor;
        _confirmPasswordTextField.layer.borderWidth = 1;
        _confirmPasswordTextField.layer.cornerRadius = 5;
    }
    
    return _confirmPasswordTextField;
}

- (UIButton *)completeButton {
    if (!_completeButton) {
        _completeButton = [[UIButton alloc] initWithFrame:CGRectMake(20, CGRectGetMaxY(_confirmPasswordTextField.frame) + 20, SCREEN_WIDTH - 40, 50)];
        [_completeButton setTitle:@"完成" forState:UIControlStateNormal];
        [_completeButton setBackgroundColor:[UIColor colorWithRed:37/255.f green:183/255.f blue:237/255.0 alpha:1]];
        [_completeButton setBackgroundColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
        _completeButton.layer.cornerRadius = 5;
        _completeButton.layer.masksToBounds = YES;
    }
    return _completeButton;
}

@end
