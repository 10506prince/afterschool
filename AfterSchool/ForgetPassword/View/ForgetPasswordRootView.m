//
//  ForgetPasswordRootView.m
//  AfterSchool
//
//  Created by Teson Draw on 10/7/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "ForgetPasswordRootView.h"
#import "MacrosDefinition.h"
#import "UIButton+SetBackgroundColor.h"

@implementation ForgetPasswordRootView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        
        [self addSubview:self.navigationBarView];
        [self addSubview:self.mobileNumberTextField];
        [self addSubview:self.verificationCodeTextField];
        [self addSubview:self.getVerificationCodeButton];
    }
    return self;
}

- (NavigationBarView *)navigationBarView {
    if (!_navigationBarView) {
        _navigationBarView = [[NavigationBarView alloc] initWithTitle:@"验证" titleColor:[UIColor whiteColor] backgroundColor:[UIColor colorWithRed:39/255.f green:39/255.f blue:41/255.f alpha:1] leftButtonName:nil leftButtonImageName:@"navigation_bar_return_button" rightButtonName:@"下一步" rightButtonImageName:nil];
    }
    return _navigationBarView;
}

- (CustomTextField *)mobileNumberTextField {
    if (!_mobileNumberTextField) {
        _mobileNumberTextField = [[CustomTextField alloc] initWithFrame:CGRectMake(20, 64 + 30, SCREEN_WIDTH - 40, 50)];
        _mobileNumberTextField.placeholder = @"输入手机号";
        _mobileNumberTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
        _mobileNumberTextField.returnKeyType = UIReturnKeyNext;
        _mobileNumberTextField.tag = 100;
        _mobileNumberTextField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
        _mobileNumberTextField.autocorrectionType = UITextAutocorrectionTypeNo;
        _mobileNumberTextField.layer.borderColor = [UIColor blackColor].CGColor;
        _mobileNumberTextField.layer.borderWidth = 1;
        _mobileNumberTextField.layer.cornerRadius = 5;
    }
    
    return _mobileNumberTextField;
}

- (CustomTextField *)verificationCodeTextField {
    if (!_verificationCodeTextField) {
        _verificationCodeTextField = [[CustomTextField alloc] initWithFrame:CGRectMake(20, CGRectGetMaxY(_mobileNumberTextField.frame) + 20, 160, 50)];
        _verificationCodeTextField.placeholder = @"输入验证码";
        _verificationCodeTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
        _verificationCodeTextField.returnKeyType = UIReturnKeySend;
        _verificationCodeTextField.tag = 101;
        _verificationCodeTextField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
        _verificationCodeTextField.autocorrectionType = UITextAutocorrectionTypeNo;
        _verificationCodeTextField.layer.borderColor = [UIColor blackColor].CGColor;
        _verificationCodeTextField.layer.borderWidth = 1;
        _verificationCodeTextField.layer.cornerRadius = 5;
    }
    
    return _verificationCodeTextField;
}

- (UIButton *)getVerificationCodeButton {
    if (!_getVerificationCodeButton) {
        _getVerificationCodeButton = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_mobileNumberTextField.frame) - 100, _verificationCodeTextField.frame.origin.y, 100, 50)];
        [_getVerificationCodeButton setTitle:@"获取验证码" forState:UIControlStateNormal];
        [_getVerificationCodeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_getVerificationCodeButton setBackgroundColor:[UIColor colorWithRed:253/255.f green:155/255.f blue:39/255.0 alpha:1]];
        [_getVerificationCodeButton setBackgroundColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
        _getVerificationCodeButton.layer.cornerRadius = 5;
        _getVerificationCodeButton.layer.masksToBounds = YES;
    }
    return _getVerificationCodeButton;
}

@end
