//
//  ForgetPasswordRootView.h
//  AfterSchool
//
//  Created by Teson Draw on 10/7/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavigationBarView.h"
#import "CustomTextField.h"

@interface ForgetPasswordRootView : UIView

@property (nonatomic, strong) NavigationBarView *navigationBarView;
@property (nonatomic, strong) CustomTextField   *mobileNumberTextField;
@property (nonatomic, strong) CustomTextField   *verificationCodeTextField;
@property (nonatomic, retain) UIButton          *getVerificationCodeButton;

@end
