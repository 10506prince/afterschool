//
//  ForgetPasswordViewController.m
//  AfterSchool
//
//  Created by Teson Draw on 10/7/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "ForgetPasswordViewController.h"
#import "SetNewPasswordViewController.h"

#import "MacrosDefinition.h"

@interface ForgetPasswordViewController ()

@end

@implementation ForgetPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initUserInterface];
}

- (void)initUserInterface {
    [self.view addSubview:self.rootView];
}

- (ForgetPasswordRootView *)rootView {
    if (!_rootView) {
        _rootView = [[ForgetPasswordRootView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];

        [_rootView.navigationBarView.leftButton addTarget:self action:@selector(returnButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        [_rootView.navigationBarView.rightButton addTarget:self action:@selector(nextStepButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        [_rootView.getVerificationCodeButton addTarget:self action:@selector(getVerificationCodeButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    }
    return _rootView;
}

- (void)returnButtonClicked {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)nextStepButtonClicked {
    SetNewPasswordViewController *vc = [[SetNewPasswordViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)getVerificationCodeButtonClicked {
    
}

@end
