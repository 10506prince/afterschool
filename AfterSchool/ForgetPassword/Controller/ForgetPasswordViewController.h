//
//  ForgetPasswordViewController.h
//  AfterSchool
//
//  Created by Teson Draw on 10/7/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ForgetPasswordRootView.h"

@interface ForgetPasswordViewController : UIViewController

@property (nonatomic, strong) ForgetPasswordRootView *rootView;

@end
