//
//  LGRadarNearbyInfo.m
//  AfterSchool
//
//  Created by lg on 15/10/20.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGRadarNearbyInfo.h"

@implementation LGRadarNearbyInfo

+ (instancetype)lgRadarNearbyInfoWithBmkRadarNearbyInfo:(BMKRadarNearbyInfo *)info {
    LGRadarNearbyInfo * selfObj = [[LGRadarNearbyInfo alloc] init];
    selfObj.userId = info.userId;
    selfObj.pt = info.pt;
    selfObj.distance = info.distance;
    selfObj.extInfo = info.extInfo;
    selfObj.mobileType = info.mobileType;
    selfObj.osType = info.osType;
    selfObj.timeStamp = info.timeStamp;
    
    return selfObj;
//    ///用户id
//    @property (nonatomic, strong) NSString* userId;
//    ///地址坐标
//    @property (nonatomic, assign) CLLocationCoordinate2D pt;
//    ///距离
//    @property (nonatomic, assign) NSUInteger distance;
//    ///扩展信息
//    @property (nonatomic, strong) NSString* extInfo;
//    ///设备类型
//    @property (nonatomic, strong) NSString* mobileType;
//    ///设备系统
//    @property (nonatomic, strong) NSString* osType;
//    ///时间戳
//    @property (nonatomic, assign) NSTimeInterval timeStamp;
}
@end
