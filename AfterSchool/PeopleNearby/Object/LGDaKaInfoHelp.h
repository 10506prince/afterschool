//
//  LGDaKaInfoHelp.h
//  AfterSchool
//
//  Created by lg on 15/10/19.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LGDaKaInfoHelp : NSObject

@property (nonatomic, assign) long long id;///<唯一编号

@property (nonatomic, copy) NSString *pushId;///<推送Id

@property (nonatomic, assign) NSInteger p_totalRounds;///<总场数

@property (nonatomic, assign) NSInteger p_attack;///<战斗力

@property (nonatomic, assign) long long regTime;///<注册时间

@property (nonatomic, assign) long long birth;///<生日

@property (nonatomic, assign) NSInteger sex;///<性别（0-女 1-男）

@property (nonatomic, assign) NSInteger type;///<类型

@property (nonatomic, copy) NSString *p_playerName;///<角色名

@property (nonatomic, copy) NSString *icon;///<头像

@property (nonatomic, copy) NSString *nickName;///<昵称

@property (nonatomic, assign) NSInteger creditLev;///<信用积分等级

@property (nonatomic, assign) NSInteger p_level;///<段位

@property (nonatomic, assign) NSInteger p_loseRrounds;///<负场

@property (nonatomic, copy) NSString *account;///<

@property (nonatomic, copy) NSString *info;///<个人简介

@property (nonatomic, copy) NSString *p_serverName;///<服务器名

@property (nonatomic, assign) NSInteger p_winRounds;///<胜场

@end
