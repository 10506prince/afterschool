//
//  LGRadarNearbyInfo.h
//  AfterSchool
//
//  Created by lg on 15/10/20.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <BaiduMapAPI/BMKRadarResult.h>
#import "LGDaKaInfo.h"

@interface LGRadarNearbyInfo : BMKRadarNearbyInfo

@property (nonatomic, strong) LGDaKaInfo * dakaInfo;

+ (instancetype)lgRadarNearbyInfoWithBmkRadarNearbyInfo:(BMKRadarNearbyInfo *)info;

@end
