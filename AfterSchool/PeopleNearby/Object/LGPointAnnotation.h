//
//  LGPointAnnotation.h
//  baiduMap
//
//  Created by lg on 15/9/29.
//  Copyright © 2015年 李刚. All rights reserved.
//

#import <BaiduMapAPI/BMKPointAnnotation.h>
#import "LGDaKaInfo.h"

@interface LGPointAnnotation : BMKPointAnnotation

@property (nonatomic, strong) LGDaKaInfo * dakaInfo;

@end
