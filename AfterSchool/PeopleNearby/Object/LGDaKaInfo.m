//
//  LGDaKaInfo.m
//  AfterSchool
//
//  Created by lg on 15/10/10.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGDaKaInfo.h"
#import "NSDate+constellation.h"

@implementation LGDaKaInfo

- (NSString *)description {
    NSString * str = [NSString stringWithFormat:@"%@,%@,%@,%@,%@,%ld,%ld,", _headImage,_name,_constellation,_gameRank,_gameServer,_gameNumber,_gameFighting];
    return str;
}

+ (instancetype)dakaInfoWithDakaInfoHelp:(LGDaKaInfoHelp *)dakaInfoHelp {
    LGDaKaInfo * selfObject = [[LGDaKaInfo alloc] initWithDakaInfoHelp:dakaInfoHelp];
    return selfObject;
}

- (instancetype)initWithDakaInfoHelp:(LGDaKaInfoHelp *)dakaInfoHelp
{
    self = [super init];
    if (self) {
//        self.headImageUrl = dakaInfoHelp.
        self.headImage = dakaInfoHelp.icon;
        self.name = dakaInfoHelp.nickName;
        self.creditRating = dakaInfoHelp.p_level;
        self.sex = dakaInfoHelp.sex;

        NSDate * birthday = [NSDate dateWithTimeIntervalSince1970:dakaInfoHelp.birth/1000];
        
        self.age = [NSDate getAgeFromBirthday:birthday];
        self.constellation = [NSDate getConstellationFromDate:birthday];
        self.signature = dakaInfoHelp.info;
//        self.gameRank = dakaInfoHelp.p_level;
        self.gameRank = @"黄金4";
        self.gameServer = dakaInfoHelp.p_serverName;
        self.gameNumber = dakaInfoHelp.p_totalRounds;
        self.gameFighting = dakaInfoHelp.p_attack;
        self.account = dakaInfoHelp.account;
    }
    return self;
}

+ (NSArray *)dakaInofArrayWithDakaInfoHelpArray:(NSArray *)array {
    NSMutableArray * resultArray = [NSMutableArray array];
    for (id obj in array) {
        if ([obj isKindOfClass:[LGDaKaInfoHelp class]]) {
            LGDaKaInfo * dakaInfo = [LGDaKaInfo dakaInfoWithDakaInfoHelp:(LGDaKaInfoHelp *)obj];
            [resultArray addObject:dakaInfo];
        }
    }
    
    return resultArray;
}

@end
