//
//  LGDaKaInfo.h
//  AfterSchool
//
//  Created by lg on 15/10/10.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LGDaKaInfoHelp.h"

@interface LGDaKaInfo : NSObject

//头像
//名称
//信用等级
//距离
//性别
//年龄
//星座
//个性签名


//游戏段位
//游戏大区
//游戏局数
//战斗力

@property (nonatomic, copy) NSString * account;

@property (nonatomic, copy) NSString * headImage;       ///<头像
@property (nonatomic, copy) NSString * headImageUrl;
@property (nonatomic, copy) NSString * name;            ///<用户游戏昵称
@property (nonatomic, assign) NSUInteger creditRating;  ///<信用等级
@property (nonatomic, assign) double distance;          ///<距离(km)
@property (nonatomic, assign) NSUInteger sex;           ///<性别(1:男 0:女)
@property (nonatomic, assign) NSUInteger age;           ///<年龄
@property (nonatomic, copy) NSString * constellation;   ///<星座
@property (nonatomic, copy) NSString * signature;       ///<个性签名

@property (nonatomic, copy) NSString * gameRank;        ///<游戏段位
@property (nonatomic, copy) NSString * gameServer;      ///<游戏大区
@property (nonatomic, assign) NSUInteger gameNumber;    ///<游戏局数
@property (nonatomic, assign) NSUInteger gameFighting;    ///<战斗力

//@property (nonatomic, copy) NSString * gameLevel;       ///<游戏角色等级
//@property (nonatomic, copy) NSString * gameWons;        ///<胜场数
//@property (nonatomic, copy) NSString * gameLosts;       ///<负场数

+ (instancetype)dakaInfoWithDakaInfoHelp:(LGDaKaInfoHelp *)dakaInfoHelp;
+ (NSArray *)dakaInofArrayWithDakaInfoHelpArray:(NSArray *)array;

@end
