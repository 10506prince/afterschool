//
//  LGPointAnnotation.h
//  baiduMap
//
//  Created by lg on 15/9/29.
//  Copyright © 2015年 李刚. All rights reserved.
//

#import <BaiduMapAPI_Map/BMKPointAnnotation.h>
#import "LGDaKaInfo.h"
#import "LGCloudSearchOption.h"

@interface LGPointAnnotation : BMKPointAnnotation

@property (nonatomic, strong) LGDaKaInfo * dakaInfo;

+ (instancetype)lgpointAnnotationWithLGCloudPoiInfo:(LGCloudPOIInfo *)info;

@end
