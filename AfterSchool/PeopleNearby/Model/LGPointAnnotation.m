//
//  LGPointAnnotation.m
//  baiduMap
//
//  Created by lg on 15/9/29.
//  Copyright © 2015年 李刚. All rights reserved.
//

#import "LGPointAnnotation.h"

@implementation LGPointAnnotation

+ (instancetype)lgpointAnnotationWithLGCloudPoiInfo:(LGCloudPOIInfo *)info {
    LGPointAnnotation * selfObj = [[LGPointAnnotation alloc] init];
    
    [selfObj setCoordinate:info.locationValue];
//    selfObj.coordinate = info.locationValue;
    selfObj.title = info.user_id;
    selfObj.subtitle = @"";
    
    return selfObj;
}

@end
