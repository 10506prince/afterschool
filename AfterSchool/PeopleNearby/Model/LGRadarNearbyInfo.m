//
//  LGRadarNearbyInfo.m
//  AfterSchool
//
//  Created by lg on 15/10/20.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGRadarNearbyInfo.h"

@implementation LGRadarNearbyInfo

- (instancetype)init
{
    self = [super init];
    if (self) {
    }
    return self;
}

+ (instancetype)lgRadarNearbyInfoWithBmkRadarNearbyInfo:(BMKRadarNearbyInfo *)info {
    LGRadarNearbyInfo * selfObj = [[LGRadarNearbyInfo alloc] init];
    selfObj.userId = info.userId;
    selfObj.pt = info.pt;
    selfObj.distance = info.distance;
    
    selfObj.timeStamp = info.timeStamp;
    selfObj.extInfo = info.extInfo;
    selfObj.mobileType = info.mobileType;
    selfObj.osType = info.osType;
    
    return selfObj;
//    ///用户id
//    @property (nonatomic, strong) NSString* userId;
//    ///地址坐标
//    @property (nonatomic, assign) CLLocationCoordinate2D pt;
//    ///距离
//    @property (nonatomic, assign) NSUInteger distance;
//    ///扩展信息
//    @property (nonatomic, strong) NSString* extInfo;
//    ///设备类型
//    @property (nonatomic, strong) NSString* mobileType;
//    ///设备系统
//    @property (nonatomic, strong) NSString* osType;
//    ///时间戳
//    @property (nonatomic, assign) NSTimeInterval timeStamp;
}

+ (instancetype)lgRadarNearbyInfoWithLGCloudPoiInfo:(LGCloudPOIInfo *)info {
    LGRadarNearbyInfo * selfObj = [[LGRadarNearbyInfo alloc] init];
    selfObj.userId = info.user_id;
    selfObj.pt = info.locationValue;
    selfObj.distance = info.distance;
    
    selfObj.timeStamp = info.modify_time;
    selfObj.extInfo = info.user_id;
    selfObj.mobileType = @"";
    selfObj.osType = @"";
    
    return selfObj;
}
@end
