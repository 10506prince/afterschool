//
//  PeopleGameDetailViewController.m
//  AfterSchool
//
//  Created by lg on 15/10/14.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "PeopleGameDetailViewController.h"
#import "Common.h"
#import "TalkingData.h"
#import "Common.h"
#import "SVProgressHUD.h"
#import "LGDefineNetServer.h"
#import "TDNetworkRequest.h"
#import "NotificationName.h"
#import "TDSingleton.h"

#import "LGDakaDetail.h"

#import "PeopleGameDetailRootView.h"
#import "GameDetailViewController.h"

#import "CreatAssignOrderViewController.h"
#import "NotificationName.h"

#import "SendMessageViewController.h"


@interface PeopleGameDetailViewController () {
    LGDaKaInfo * _dakaInfo;
    LGDakaDetail * _dakaDetail;
}

@property (nonatomic, strong) LGDaKaInfo * dakaInfo;
@property (nonatomic, strong) LGDakaDetail * dakaDetail;

@property (nonatomic, strong) PeopleGameDetailRootView * rootView;

@end

@implementation PeopleGameDetailViewController

- (instancetype)init
{
    self = [super init];
    if (self) {
    }
    return self;
}

- (instancetype)initWithDakaInfo:(LGDaKaInfo *)dakaInfo {
    self = [super init];
    if (self) {
        self.dakaInfo = dakaInfo;
    }
    return self;
}

- (void)dealloc
{
    NSLogSelfMethodName;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tabBarController.tabBar setHidden:YES];
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [TalkingData trackPageBegin:selfControllerName];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [TalkingData trackPageEnd:selfControllerName];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initData];
    [self initUserInterface];
    
    ///判断查看的是不是自己
    if ([self.dakaInfo.account isEqualToString:[TDSingleton instance].userInfoModel.account]) {
        [self isSelfOrBlack];
    } else {
        ///判断查看的是不是好友
        NSArray <LGDaKaInfo *> * friendList = [TDSingleton instance].userInfoModel.contacsListCache;
        BOOL friendFlag = NO;
        for (LGDaKaInfo * friend in friendList) {
            if ([friend.account isEqualToString:self.dakaInfo.account]) {
                friendFlag = YES;
                break;
            }
        }
        
        if (friendFlag) {
            [self isFriend];
        } else {
            ///判断查看的是不是拉黑的人
            NSArray <LGDaKaInfo *> * blackList = [TDSingleton instance].userInfoModel.blackListCache;
            BOOL blackFlag = NO;
            for (LGDaKaInfo * black in blackList) {
                if ([black.account isEqualToString:self.dakaInfo.account]) {
                    blackFlag = YES;
                }
            }
            
            if (blackFlag) {
                [self isSelfOrBlack];
            }
        }
    }
    
    [self getDakaDetail];
}

- (void)initData {
//    self.friendList = [NSMutableArray arrayWithArray:[TDSingleton instance].userInfoModel.contacsListCache];
//    self.blackList = [NSMutableArray arrayWithArray:[TDSingleton instance].userInfoModel.blackListCache];
}

- (void)initUserInterface {
    [self.view addSubview:self.rootView];
}

- (PeopleGameDetailRootView *)rootView {
    if (!_rootView) {
        _rootView = [[PeopleGameDetailRootView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        
        [self addChildViewController:_rootView.detailVC];
        
        
        [_rootView.navigationBarView.leftButton addTarget:self action:@selector(popAction:) forControlEvents:UIControlEventTouchUpInside];
        
        
        [_rootView.addFriendBtn addTarget:self action:@selector(addFriendItemClicked:) forControlEvents:UIControlEventTouchUpInside];
        [_rootView.messageBtn addTarget:self action:@selector(pushChat:) forControlEvents:UIControlEventTouchUpInside];
        [_rootView.playBtn addTarget:self action:@selector(pushCreatOrder:) forControlEvents:UIControlEventTouchUpInside];
        
        
        __weak typeof(self) weakSelf = self;
        [_rootView.detailVC addGameAccountDetailBtnBlock:^(LGGamePlayerInfo *gameAccountInfo) {
            [weakSelf pushGamePlayerDetail:gameAccountInfo];
        }];
    }
    return _rootView;
}

#pragma mark - 底部栏显示方式

///自己或黑名单
- (void)isSelfOrBlack {
    [self.rootView.bottomView setHidden:YES];
    [self.rootView.detailVC.view setFrame:CGRectMake(0, 64, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-64)];
}
///好友
- (void)isFriend {
    [self.rootView.addFriendBtn setHidden:YES];
    [self.rootView.messageBtn setFrame:CGRectMake(20, 10, (LgScreenWidth - 40 - 20)/2, self.rootView.messageBtn.bounds.size.height)];
    [self.rootView.playBtn setFrame:CGRectMake(CGRectGetMaxX(self.rootView.messageBtn.frame) + 20, 10, (LgScreenWidth - 40 - 20)/2, self.rootView.playBtn.bounds.size.height)];
}
///非好友，非黑名单
- (void)defaultBottomView {
    
}

#pragma mark - action
- (IBAction)popAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)pushGamePlayerDetail:(LGGamePlayerInfo *)gamePlayerInfo {
    GameDetailViewController * vc = [[GameDetailViewController alloc] initWithGamePlayerInfo:gamePlayerInfo];
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)pushChat:(id)sender {
    
    SendMessageViewController *vc = [[SendMessageViewController alloc]init];
    vc.conversationType = ConversationType_PRIVATE;
    vc.targetId = _dakaInfo.account;
    vc.userName = _dakaInfo.nickName;
    vc.entryMode = 2;
    
    [self.navigationController pushViewController:vc animated:YES];
    
//    [self.navigationController.tabBarController setSelectedIndex:1];
//    [[NSNotificationCenter defaultCenter] postNotificationName:TDChatWithBigShot object:_dakaInfo];
}

- (void)enterChatWindowWithTargetId:(NSString *)targetId userName:(NSString *)userName {
    SendMessageViewController *vc = [[SendMessageViewController alloc]init];
    vc.conversationType = ConversationType_PRIVATE;
    vc.targetId = targetId;
    vc.userName = userName;
    
    vc.entryMode = 2;
    
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)pushCreatOrder:(id)sender {
    if ([TDSingleton instance].userInfoModel.defaultPlayer.id != 0) {
        
        CreatAssignOrderViewController * vc = [[CreatAssignOrderViewController alloc] initWithDakaInfo:_dakaInfo];
        [self.navigationController pushViewController:vc animated:YES];
    } else {
        [SVProgressHUD showErrorWithStatus:@"请绑定游戏账号后再来尝试"];
    }
}

- (IBAction)addFriendItemClicked:(id)sender {

    __weak typeof(self) weakSelf = self;
    
    [TDNetworkRequest requestWithAccount:_dakaInfo.account
                                  action:AddFriend
                                 success:^(id responseObject)
    {
        [SVProgressHUD showSuccessWithStatus:@"添加好友成功！" duration:1.5 ];
//        [self.friendList addObject:weakSelf.dakaInfo];
//        [TDSingleton instance].userInfoModel.contacsListCache = self.friendList;
        [[TDSingleton instance].userInfoModel.contacsListCache addObject:weakSelf.dakaInfo];
        [[NSNotificationCenter defaultCenter] postNotificationName:TDUpdateFriends object:nil];
        
        [UIView animateWithDuration:0.5 animations:^{
            [weakSelf isFriend];
        }];
    }];
}

#pragma mark - network
- (void)getDakaDetail {
    
    __weak typeof(self) weakSelf = self;
    
    [LGDefineNetServer getUserDetailWithAccount:_dakaInfo success:^(id result) {
        
        weakSelf.dakaDetail = [LGDakaDetail lgdakaDetailWith:result];
        
        if (!weakSelf.dakaDetail) {
            return;
        }
        
        weakSelf.rootView.detailVC.peopleInfoView.userInfo = weakSelf.dakaDetail;
        
        weakSelf.rootView.detailVC.peopleRateView.rateLevel = roundf(weakSelf.dakaDetail.score);
        weakSelf.rootView.detailVC.peopleRateView.price = weakSelf.dakaDetail.priceOnline;

        
        long long defaultPlayerId = weakSelf.dakaDetail.defaultGamePlayer.id;
        
        NSMutableArray <LGGamePlayerInfo *> * gamePlayerList = [NSMutableArray array];
        for (int i = 0; i < 5; i++) {
            
            LGGamePlayerInfo * nullPlayer = [[LGGamePlayerInfo alloc] init];
            nullPlayer.id = -1;
            [gamePlayerList addObject:nullPlayer];
        }
        
        int j = 0;
        for (LGGamePlayerInfo * gamePlayerInfo in weakSelf.dakaDetail.gamePlayerList) {
            
            if (j < 5) {
                [gamePlayerList replaceObjectAtIndex:j withObject:gamePlayerInfo];
                
                if (gamePlayerInfo.id == defaultPlayerId) {
                    [gamePlayerList exchangeObjectAtIndex:0 withObjectAtIndex:j];
                }
            }
            j++;
        }
        
        weakSelf.rootView.detailVC.gameAccounts = gamePlayerList;
        weakSelf.rootView.detailVC.defaultGameAccountId = defaultPlayerId;
    } failure:^(NSString *msg) {        
    }];
}

@end
