//
//  PeopleDetailViewController.m
//  AfterSchool
//
//  Created by lg on 15/12/30.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "PeopleDetailViewController.h"
#import "GameAccountCollectionViewCell.h"
#import "GameDetailViewController.h"
#import "Common.h"
#import "TalkingData.h"

@interface PeopleDetailViewController () <UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic, weak) IBOutlet GameAccountInfoView * gameAccountInfoView;
@property (nonatomic, weak) IBOutlet UIButton * gameDetailBtn;

@property (copy) PeopleDetailViewControllerBlock gameAccountBtnBlock;

@end

@implementation PeopleDetailViewController

- (void)dealloc
{
    NSLogSelfMethodName;
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [TalkingData trackPageBegin:selfControllerName];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [TalkingData trackPageEnd:selfControllerName];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initData];
    [self initUserInterface];
}

- (void)initData {
}

- (void)initUserInterface {
    [self.collectionView registerClass:[GameAccountCollectionViewCell class] forCellWithReuseIdentifier:NSStringFromClass([GameAccountCollectionViewCell class])];
}

#pragma mark - setValue
- (void)setGameAccounts:(NSArray<LGGamePlayerInfo *> *)gameAccounts {
    _gameAccounts = gameAccounts;
    
    [self.collectionView reloadData];
    
    if (_gameAccounts.count > 0) {
        
        LGGamePlayerInfo * gameAccountInfo = [_gameAccounts firstObject];
        [self.collectionView selectItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0] animated:YES scrollPosition:UICollectionViewScrollPositionNone];
        
        self.gameAccountInfoView.gameAccountInfo = gameAccountInfo;
        [self.gameDetailBtn setHidden:gameAccountInfo.id == -1];
    }
}

- (void)addGameAccountDetailBtnBlock:(PeopleDetailViewControllerBlock)gameAccountDetailBlock {
    self.gameAccountBtnBlock = gameAccountDetailBlock;
}
#pragma mark - Action
- (IBAction)toGameAccountDetailAction:(id)sender {
   
    NSArray <NSIndexPath *> * indexPaths = [self.collectionView indexPathsForSelectedItems];
    
    
    if (indexPaths.count > 0) {
        NSIndexPath * indexPath = [indexPaths firstObject];
        LGGamePlayerInfo * gameAccountInfo = [self.gameAccounts objectAtIndex:indexPath.item];
        
        PeopleDetailViewControllerBlock block = self.gameAccountBtnBlock;
        if (block) {
            block(gameAccountInfo);
        }
    }
}

#pragma UICollectionViewDataSource, UICollectionViewDelegateFlowLayout

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.gameAccounts.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    LGGamePlayerInfo * gamePlayerInfo = [self.gameAccounts objectAtIndex:indexPath.item];
    
    GameAccountCollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([GameAccountCollectionViewCell class]) forIndexPath:indexPath];
    cell.gamePlayerInfo = gamePlayerInfo;
    [cell setDefaultGameAccount:gamePlayerInfo.id == self.defaultGameAccountId];
    
    return cell;
}

#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    LGGamePlayerInfo * gameAccountInfo = [self.gameAccounts objectAtIndex:indexPath.item];
    
    self.gameAccountInfoView.gameAccountInfo = gameAccountInfo;
    [self.gameDetailBtn setHidden:gameAccountInfo.id == -1];
}

//#pragma mark - UICollectionViewDelegateFlowLayout
//- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
//    
//    return CGSizeMake(55, 62);
//}
//
//- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
//    return UIEdgeInsetsZero;
//}
//
//- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
//
//    return 20;
//}
//
//- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
//
//    return 0;
//}
//
//- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
//    return CGSizeZero;
//}
//
//- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section {
//    return CGSizeZero;
//}

@end
