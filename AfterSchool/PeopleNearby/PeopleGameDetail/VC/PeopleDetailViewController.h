//
//  PeopleDetailViewController.h
//  AfterSchool
//
//  Created by lg on 15/12/30.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PeopleInfoView.h"
#import "PeopleRateView.h"
#import "GameAccountInfoView.h"
#import "LGGamePlayerInfo.h"

typedef void(^PeopleDetailViewControllerBlock)(LGGamePlayerInfo * gameAccountInfo);

@interface PeopleDetailViewController : UIViewController

@property (nonatomic, weak) IBOutlet PeopleInfoView * peopleInfoView;
@property (nonatomic, weak) IBOutlet PeopleRateView * peopleRateView;

@property (nonatomic, weak) IBOutlet UICollectionView * collectionView;

@property (nonatomic, strong) NSArray <LGGamePlayerInfo *> * gameAccounts;
@property (nonatomic) long long defaultGameAccountId;

- (void)addGameAccountDetailBtnBlock:(PeopleDetailViewControllerBlock)gameAccountDetailBlock;

@end
