//
//  PeopleGameDetailViewController.h
//  AfterSchool
//
//  Created by lg on 15/10/14.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGViewController.h"

#import "LGDaKaInfo.h"

@interface PeopleGameDetailViewController : LGViewController

- (instancetype)init __deprecated_msg("用initWithDakaInfo替换");

- (instancetype)initWithDakaInfo:(LGDaKaInfo *)dakaInfo;

@end
