//
//  PeopleGameDetailRootView.h
//  AfterSchool
//
//  Created by lg on 15/10/14.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavigationBarView.h"
#import "LGCollectionView.h"

#import "PeopleGameDetailCollectionViewCellOne.h"
#import "PeopleGameDetailCollectionViewCellTwo.h"
#import "PeopleGameDetailCollectionViewCellThree.h"

#import "PeopleGameDetailCollectionHeaderView.h"

#import "PeopleDetailViewController.h"

@interface PeopleGameDetailRootView : UIView

@property (nonatomic, strong) NavigationBarView *navigationBarView;///<状态栏View

@property (nonatomic, strong) PeopleDetailViewController * detailVC;
@property (nonatomic, strong) UIView * bottomView;
@property (nonatomic, strong) UIView * lineView;
@property (nonatomic, strong) UIButton * addFriendBtn;
@property (nonatomic, strong) UIButton * messageBtn;
@property (nonatomic, strong) UIButton * playBtn;

@end





