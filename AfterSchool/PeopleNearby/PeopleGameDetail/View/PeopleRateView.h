//
//  PeopleRateView.h
//  AfterSchool
//
//  Created by lg on 15/12/30.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PeopleRateView : UIView

@property (nonatomic) NSUInteger rateLevel;
@property (nonatomic) float price;

@end
