//
//  PeopleGameDetailCollectionHeaderView.m
//  AfterSchool
//
//  Created by lg on 15/11/13.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "PeopleGameDetailCollectionHeaderView.h"
#import "UIColor+RGB.h"

@interface PeopleGameDetailCollectionHeaderView () {
    UILabel * _label;
}

@end

@implementation PeopleGameDetailCollectionHeaderView

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        _label = [[UILabel alloc]initWithFrame:CGRectMake(12, 0, self.bounds.size.width - 12, self.bounds.size.height)];
        
        [_label setFont:[UIFont systemFontOfSize:22]];
        [_label setTintColor:[UIColor getColorDec:@"140140140"]];
        [_label setTextAlignment:NSTextAlignmentLeft];
        
        [self addSubview:_label];
    }
    return self;
}

-(void)setText:(NSString *)text
{
    _text = text;
    
    _label.text = text;
    
}
@end
