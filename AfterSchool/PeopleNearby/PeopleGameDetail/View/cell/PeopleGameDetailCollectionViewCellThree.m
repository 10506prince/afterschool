//
//  PeopleGameDetailCollectionViewCellThree.m
//  AfterSchool
//
//  Created by lg on 15/11/12.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "PeopleGameDetailCollectionViewCellThree.h"
#import "UIImageView+WebCache.h"

@interface PeopleGameDetailCollectionViewCellThree ()

@property (nonatomic, weak) IBOutlet UIImageView * imageView;

@end

@implementation PeopleGameDetailCollectionViewCellThree

- (void)awakeFromNib {
    // Initialization code
}

- (void)setGamePlayInfo:(LGGamePlayerInfo *)gamePlayInfo {
    _gamePlayInfo = gamePlayInfo;
    
    if (_gamePlayInfo.icon.length > 0) {
        [self.imageView setImage:[UIImage imageNamed:@"user_default_head"]];
    } else {
        NSURL * url = [NSURL URLWithString:_gamePlayInfo.icon];
        [self.imageView sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"user_default_head"]];
    }
}

@end
