//
//  PeopleGameDetailCollectionViewCellTwo.h
//  AfterSchool
//
//  Created by lg on 15/11/12.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LGGamePlayer.h"

#define PeopleGameDetailCollectionViewCellTwoHeight 320.0f

@class PeopleGameDetailCollectionViewCellTwo;

@protocol PeopleGameDetailCollectionViewCellTwoDelegate <NSObject>

- (void)cell:(PeopleGameDetailCollectionViewCellTwo *)peopleGameDetailCollectionViewCellTwo clickIconWithIndexPath:(NSIndexPath *)indexPath;

@end

@interface PeopleGameDetailCollectionViewCellTwo : UICollectionViewCell

@property(nonatomic, strong) LGGamePlayerInfo * gamePlayer;
@property (nonatomic, strong) NSIndexPath * indexPath;
@property (nonatomic, weak) id <PeopleGameDetailCollectionViewCellTwoDelegate> delegate;

@end
