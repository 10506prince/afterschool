//
//  PeopleGameDetailCollectionViewCellTwo.m
//  AfterSchool
//
//  Created by lg on 15/11/12.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "PeopleGameDetailCollectionViewCellTwo.h"
#import "UIColor+RGB.h"
#import "Common.h"
#import "UIButton+WebCache.h"
#import "LGSevenStarView.h"

@interface PeopleGameDetailCollectionViewCellTwo ()

@property (nonatomic, strong) CALayer * sevenStarBackgroundLayer;
@property (nonatomic, strong) CAShapeLayer * sevenStarLineLayer;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sevenStarLead;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sevenStarTrail;

@property (nonatomic, weak) IBOutlet UIButton * gameIcon;
@property (nonatomic, weak) IBOutlet UILabel * gameName;
@property (nonatomic, weak) IBOutlet UILabel * gameRank;
@property (nonatomic, weak) IBOutlet UILabel * gameServerNameAndRounds;
@property (nonatomic, weak) IBOutlet UILabel * gameAttack;
@property (nonatomic, strong) IBOutlet LGSevenStarView * sevenStarView;

@end

@implementation PeopleGameDetailCollectionViewCellTwo

- (void)dealloc
{
    NSLogSelfMethodName;
}

- (void)awakeFromNib {
    
    self.sevenStarBackgroundLayer = [CALayer layer];
    [self.sevenStarView.layer addSublayer:self.sevenStarBackgroundLayer];
    
    [self.gameIcon.layer setBorderWidth:1];
    [self.gameIcon.layer setBorderColor:[UIColor orangeColor].CGColor];
}

- (void)setGamePlayer:(LGGamePlayerInfo *)gamePlayer {
    _gamePlayer = gamePlayer;
    
    //头像
    if (_gamePlayer.icon.length == 0) {
        [self.gameIcon setImage:[UIImage imageNamed:@"user_default_head"] forState:UIControlStateNormal];
    } else {
        NSURL * url = [NSURL URLWithString:_gamePlayer.icon];
        [self.gameIcon sd_setImageWithURL:url forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"user_default_head"]];
    }
    
    //名称
    if (_gamePlayer) {
        [self.gameName setText:_gamePlayer.playerName];
    } else {
        [self.gameName setText:@""];
    }
    
    //段位
    if (_gamePlayer.grade.length == 0) {
        [self.gameRank setText:@"无段位"];
    } else {
        [self.gameRank setText:_gamePlayer.grade];
    }
    
    //战斗力
    [self.gameAttack setText:[NSString stringWithFormat:@"%ld", (unsigned long)_gamePlayer.attack]];
    
    //大区，局数
    if (_gamePlayer) {
        [self.gameServerNameAndRounds setText:[NSString stringWithFormat:@"%@,%ld局", _gamePlayer.serverName, (unsigned long)_gamePlayer.totalRounds]];
    } else {
        [self.gameServerNameAndRounds setText:@""];
    }
    
    //七星图
    self.sevenStarView.sevenStarValue = _gamePlayer.sevenStarValue;
}

- (IBAction)clickIcon:(id)sender{
    if (_delegate && [_delegate respondsToSelector:@selector(cell:clickIconWithIndexPath:)]) {
        [_delegate cell:self clickIconWithIndexPath:self.indexPath];
    }
}


@end
