//
//  PeopleGameDetailCollectionViewCellThree.h
//  AfterSchool
//
//  Created by lg on 15/11/12.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LGGamePlayerInfo.h"

#define PeopleGameDetailCollectionViewCellThreeHeight 100.0f

@interface PeopleGameDetailCollectionViewCellThree : UICollectionViewCell

@property (nonatomic, strong) LGGamePlayerInfo * gamePlayInfo;

@end
