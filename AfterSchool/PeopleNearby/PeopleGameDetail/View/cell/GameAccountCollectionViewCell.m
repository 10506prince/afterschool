//
//  GameAccountCollectionViewCell.m
//  AfterSchool
//
//  Created by lg on 15/12/30.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "GameAccountCollectionViewCell.h"
#import "UIImageView+WebCache.h"
#import "Common.h"

@interface GameAccountCollectionViewCell ()

@property (nonatomic, strong) UIImageView * gameAccountIconImageView;
@property (nonatomic, strong) UILabel * defaultLabel;

@property (nonatomic, strong) CAShapeLayer * selectLayer;

@end

@implementation GameAccountCollectionViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setUp];
    }
    return self;
}
- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self setUp];
    }
    return self;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUp];
    }
    return self;
}


- (void)setUp {

    [self.contentView addSubview:self.gameAccountIconImageView];
    [self.contentView addSubview:self.defaultLabel];
    
    self.selectedBackgroundView = [[UIView alloc] init];
    self.backgroundView = [[UIView alloc] init];
    
//    [self.selectedBackgroundView setBackgroundColor:APPColor];
//    [self.backgroundView setBackgroundColor:[UIColor orangeColor]];
    
    self.selectLayer = [CAShapeLayer layer];
    [self.selectLayer setFillColor:APPLightColor.CGColor];
    [self.selectedBackgroundView.layer addSublayer:self.selectLayer];
}

- (UIImageView *)gameAccountIconImageView {
    if (!_gameAccountIconImageView) {
        _gameAccountIconImageView = [[UIImageView alloc] init];
        [_gameAccountIconImageView.layer setCornerRadius:2];
        [_gameAccountIconImageView.layer setBorderColor:[UIColor whiteColor].CGColor];
        [_gameAccountIconImageView.layer setBorderWidth:1/[UIScreen mainScreen].scale];
        [_gameAccountIconImageView.layer setMasksToBounds:YES];
    }
    return _gameAccountIconImageView;
}

- (UILabel *)defaultLabel {
    if (!_defaultLabel) {
        _defaultLabel = [[UILabel alloc] init];
        [_defaultLabel setText:@"默"];
        [_defaultLabel setTextColor:[UIColor whiteColor]];
        [_defaultLabel setFont:[UIFont systemFontOfSize:12]];
        [_defaultLabel setTextAlignment:NSTextAlignmentCenter];
        [_defaultLabel setBackgroundColor:[UIColor orangeColor]];
        
        [_defaultLabel setBounds:CGRectMake(0, 0, 15, 15)];
    }
    return _defaultLabel;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat edge = self.bounds.size.width < self.bounds.size.height ? self.bounds.size.width : self.bounds.size.height;
    
    self.selectLayer.path = [self getSelectLayerWithRect:self.bounds];
    
    [self.gameAccountIconImageView setFrame:CGRectMake(2, 2, edge - 4, edge - 4)];
    
    [self.defaultLabel setFrame:CGRectMake(CGRectGetMaxX(self.gameAccountIconImageView.frame) - 1 - self.defaultLabel.bounds.size.width, CGRectGetMaxY(self.gameAccountIconImageView.frame) - 1 - self.defaultLabel.bounds.size.height, self.defaultLabel.bounds.size.width, self.defaultLabel.bounds.size.height)];
}

- (CGPathRef)getSelectLayerWithRect:(CGRect)rect {
    
    CGFloat edge = rect.size.width < rect.size.height ? rect.size.width : rect.size.height;
    
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathMoveToPoint(path, NULL, 2, 0);
    CGPathAddArc(path, NULL, 2, 2, 2, -M_PI_2, -M_PI, YES);
    CGPathAddLineToPoint(path, NULL, 0, edge - 2);
    CGPathAddArc(path, NULL, 2, edge - 2, 2, M_PI, M_PI_2, YES);
    CGPathAddLineToPoint(path, NULL, edge/2 - 5, edge);
    CGPathAddLineToPoint(path, NULL, edge/2, edge + 5);
    CGPathAddLineToPoint(path, NULL, edge/2 + 5, edge);
    CGPathAddLineToPoint(path, NULL, edge - 2, edge);
    CGPathAddArc(path, NULL, edge - 2, edge - 2, 2, M_PI_2, 0, YES);
    CGPathAddLineToPoint(path, NULL, edge, 2);
    CGPathAddArc(path, NULL, edge - 2, 2, 2, 0, -M_PI_2, YES);
    CGPathCloseSubpath(path);
    
    UIBezierPath * selectLayer = [UIBezierPath bezierPathWithCGPath:path];
    CGPathRelease(path);
    
    return selectLayer.CGPath;
}

- (void)setGamePlayerInfo:(LGGamePlayerInfo *)gamePlayerInfo {
    _gamePlayerInfo = gamePlayerInfo;
    
    [self.gameAccountIconImageView sd_setImageWithURL:[NSURL URLWithString:_gamePlayerInfo.icon] placeholderImage:[UIImage imageNamed:@"user_default_head.png"]];
}

- (void)setDefaultGameAccount:(BOOL)defaultGameAccount {
    _defaultGameAccount = defaultGameAccount;
    
    [self.defaultLabel setHidden:!_defaultGameAccount];
}
@end
