//
//  PeopleGameDetailCollectionViewCellOne.m
//  AfterSchool
//
//  Created by lg on 15/11/12.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "PeopleGameDetailCollectionViewCellOne.h"
#import "UIImageView+WebCache.h"
#import "UIButton+WebCache.h"

@interface PeopleGameDetailCollectionViewCellOne ()

@property (nonatomic, weak) IBOutlet UILabel * ageAndConstellation;
@property (nonatomic, weak) IBOutlet UILabel * userName;
@property (nonatomic, weak) IBOutlet UILabel * distance;
@property (nonatomic, weak) IBOutlet UIButton * creditBtn;
@property (nonatomic, weak) IBOutlet UIButton * userIconBtn;
@property (nonatomic, weak) IBOutlet UILabel * signature;

@property (nonatomic, weak) IBOutlet UIButton * rewardBtn;

@end

@implementation PeopleGameDetailCollectionViewCellOne

- (void)awakeFromNib {
    [self.userIconBtn.layer setBorderWidth:1];
    [self.userIconBtn.layer setBorderColor:[UIColor whiteColor].CGColor];
    
    [self.rewardBtn.layer setCornerRadius:self.rewardBtn.bounds.size.height/2];
    [self.rewardBtn.layer setMasksToBounds:YES];
    
    [self.ageAndConstellation setTransform:CGAffineTransformMakeRotation(-M_PI_2/90 * 12)];
}

- (void)setUserInfo:(LGDaKaInfo *)userInfo {
    _userInfo = userInfo;
    
    //用户名
    [self.userName setText:userInfo.nickName];
    
    //信用等级
    if (userInfo.creditRating == 0) {
        [self.creditBtn setBackgroundImage:[UIImage imageNamed:@"creditRating0"] forState:UIControlStateNormal | UIControlStateHighlighted];
    } else {
        [self.creditBtn setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"creditRating%ld", (long)(userInfo.creditRating - 1)]] forState:UIControlStateNormal | UIControlStateHighlighted];
    }
    
    //头像
    if (userInfo.headImageUrl.length == 0) {
        [self.userIconBtn setBackgroundImage:[UIImage imageNamed:@"user_default_head"] forState:UIControlStateNormal];
    } else {
        NSURL * url = [NSURL URLWithString:userInfo.headImageUrl];
        [self.userIconBtn sd_setBackgroundImageWithURL:url forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"user_default_head"]];
    }
    
    //距离
    [self.distance setText:[NSString stringWithFormat:@"相距%.2f千米", userInfo.distance]];
    
    //年龄和星座
    [self.ageAndConstellation setText:[NSString stringWithFormat:@"%@,%ld", userInfo.constellation, (unsigned long)userInfo.age]];
    
    //个性签名
    if (userInfo.signature.length == 0) {
        [self.signature setText:@"这个人很懒，什么都没有留下..."];
    } else {
        [self.signature setText:userInfo.signature];
    }
}

#pragma Action
- (IBAction)clickUserIcon:(id)sender {
    NSLog(@"点击了头像");
 
    void(^block)() = self.clickUserIconBlock;
    
    if (block) {
        block();
    }
}
- (IBAction)clickReward:(id)sender {
    NSLog(@"点击了赏");
    
    void(^block)() = self.clickRewardBlock;
    
    if (block) {
        block();
    }
}
- (IBAction)clickCredit:(id)sender {
    NSLog(@"点击了信用等级");
    
    void(^block)() = self.clickCreditBlock;
    
    if (block) {
        block();
    }
}

@end
