//
//  PeopleGameDetailCollectionHeaderView.h
//  AfterSchool
//
//  Created by lg on 15/11/13.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>

static NSString * PeopleGameDetailCollectionHeaderViewId = @"PeopleGameDetailCollectionHeaderViewId";

@interface PeopleGameDetailCollectionHeaderView : UICollectionReusableView

@property (nonatomic, copy) NSString * text;

@end
