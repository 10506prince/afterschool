//
//  PeopleGameDetailCollectionViewCellOne.h
//  AfterSchool
//
//  Created by lg on 15/11/12.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LGDaKaInfo.h"

#define PeopleGameDetailCollectionViewCellOneHeight 400.0f

@interface PeopleGameDetailCollectionViewCellOne : UICollectionViewCell

@property (nonatomic, strong) LGDaKaInfo * userInfo;

@property (copy) void(^clickUserIconBlock)();
@property (copy) void(^clickRewardBlock)();
@property (copy) void(^clickCreditBlock)();

@end
