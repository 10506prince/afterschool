//
//  GameAccountCollectionViewCell.h
//  AfterSchool
//
//  Created by lg on 15/12/30.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITableView+NibCell.h"
#import "LGGamePlayerInfo.h"

#define GameAccountCollectionViewCellHeight 62

@interface GameAccountCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) LGGamePlayerInfo * gamePlayerInfo;
@property (nonatomic) BOOL defaultGameAccount;

@end
