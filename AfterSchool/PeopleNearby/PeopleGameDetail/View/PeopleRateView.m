//
//  PeopleRateView.m
//  AfterSchool
//
//  Created by lg on 15/12/30.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "PeopleRateView.h"

@interface PeopleRateView ()

@property (nonatomic, strong) IBOutlet UILabel * rateLabelStr;
@property (nonatomic, strong) IBOutlet UILabel * userPriceLabelStr;
@property (nonatomic, strong) IBOutlet UILabel * userPriceLabel;

@property (nonatomic, strong) IBOutlet UIImageView * star1;
@property (nonatomic, strong) IBOutlet UIImageView * star2;
@property (nonatomic, strong) IBOutlet UIImageView * star3;
@property (nonatomic, strong) IBOutlet UIImageView * star4;
@property (nonatomic, strong) IBOutlet UIImageView * star5;

@property (nonatomic, strong) UIView * line;

@end

IB_DESIGNABLE
@implementation PeopleRateView
- (void)dealloc
{
    
}
- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setUp];
    }
    return self;
}
- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self setUp];
    }
    return self;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUp];
    }
    return self;
}

- (void)setUp {
    
    [self setBackgroundColor:[UIColor whiteColor]];
    
    [self addSubview:self.rateLabelStr];
    [self addSubview:self.userPriceLabelStr];
    [self addSubview:self.userPriceLabel];
    
    self.star1 = [self getStarImageView];
    self.star2 = [self getStarImageView];
    self.star3 = [self getStarImageView];
    self.star4 = [self getStarImageView];
    self.star5 = [self getStarImageView];
    
    [self addSubview:self.star1];
    [self addSubview:self.star2];
    [self addSubview:self.star3];
    [self addSubview:self.star4];
    [self addSubview:self.star5];
    
    [self addSubview:self.line];
}


///信用评级Str
- (UILabel *)rateLabelStr {
    if (!_rateLabelStr) {
        _rateLabelStr = [[UILabel alloc] init];
        [_rateLabelStr setText:@"评价"];
        [_rateLabelStr setFont:[UIFont systemFontOfSize:12]];
        [_rateLabelStr setTextColor:[UIColor lightGrayColor]];
    }
    return _rateLabelStr;
}
///约玩身价Str
- (UILabel *)userPriceLabelStr {
    if (!_userPriceLabelStr) {
        _userPriceLabelStr = [[UILabel alloc] init];
        [_userPriceLabelStr setText:@"平均赏金"];
        [_userPriceLabelStr setFont:[UIFont systemFontOfSize:12]];
        [_userPriceLabelStr setTextColor:[UIColor lightGrayColor]];
    }
    return _userPriceLabelStr;
}
///约玩身价
- (UILabel *)userPriceLabel {
    if (!_userPriceLabel) {
        _userPriceLabel = [[UILabel alloc] init];
        [_userPriceLabel setText:@"$00.00"];
        [_userPriceLabel setFont:[UIFont systemFontOfSize:18]];
        [_userPriceLabel setTextColor:[UIColor orangeColor]];
    }
    return _userPriceLabel;
}
///星、
- (UIImageView *)getStarImageView {
    UIImageView * imageView = [[UIImageView alloc] init];
    [imageView setBounds:CGRectMake(0, 0, 20, 20)];
    [imageView setImage:[UIImage imageNamed:@"order_evaluation_solid_star.png"]];
    
    return imageView;
}

- (UIView *)line {
    if (!_line) {
        _line = [[UIView alloc] init];
        [_line setBackgroundColor:[UIColor lightGrayColor]];
    }
    return _line;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self.rateLabelStr sizeToFit];
    [self.rateLabelStr setFrame:CGRectMake(20, 20, self.rateLabelStr.bounds.size.width, self.rateLabelStr.bounds.size.height)];
    
    [self.userPriceLabelStr sizeToFit];
    [self.userPriceLabelStr setCenter:CGPointMake(self.bounds.size.width - self.userPriceLabelStr.bounds.size.width/2 - 40, self.userPriceLabelStr.bounds.size.height/2 + 20)];
    
    [self.userPriceLabel sizeToFit];
    [self.userPriceLabel setCenter:CGPointMake(self.userPriceLabelStr.center.x, CGRectGetMaxY(self.userPriceLabelStr.frame) + 10 + self.userPriceLabel.bounds.size.height/2)];
    
    NSArray * stars = @[self.star1,self.star2,self.star3,self.star4,self.star5];
    int i = 0;
    for (UIImageView * imageView in stars) {
        [imageView setFrame:CGRectMake(20 + i*(imageView.bounds.size.width + 10), CGRectGetMaxY(self.rateLabelStr.frame) + 10, imageView.bounds.size.width, imageView.bounds.size.height)];
        i++;
    }
    
    [self.line setFrame:CGRectMake(self.bounds.size.width * 46.0/75, 20, 1/[UIScreen mainScreen].scale, self.bounds.size.height - 40)];
}

#pragma setValue
- (void)setRateLevel:(NSUInteger)rateLevel {
    _rateLevel = rateLevel;
    
    NSArray * stars = @[self.star1,self.star2,self.star3,self.star4,self.star5];
    int i = 0;
    for (UIImageView * imageView in stars) {
        if (i>=rateLevel) {
            [imageView setImage:[UIImage imageNamed:@"order_evaluation_gray_star"]];
        } else {
            [imageView setImage:[UIImage imageNamed:@"order_evaluation_solid_star"]];
        }
        i++;
    }
}

- (void)setPrice:(float)price {
    _price = price;
    
    [self.userPriceLabel setText:[NSString stringWithFormat:@"￥%.2f", _price]];
    [self setNeedsLayout];
}
@end
