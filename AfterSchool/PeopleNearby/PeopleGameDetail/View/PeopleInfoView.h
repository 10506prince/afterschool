//
//  PeopleInfoView.h
//  AfterSchool
//
//  Created by lg on 15/12/30.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LGDaKaInfo.h"

@interface PeopleInfoView : UIView

@property (nonatomic, strong) LGDaKaInfo * userInfo;

@end
