//
//  PeopleGameDetailRootView.m
//  AfterSchool
//
//  Created by lg on 15/10/14.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "PeopleGameDetailRootView.h"
#import "UIColor+RGB.h"
#import "Common.h"

@implementation PeopleGameDetailRootView

- (void)dealloc
{
    NSLogSelfMethodName;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setUp];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self setUp];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUp];
    }
    return self;
}

- (void)setUp {

    self.detailVC = [[PeopleDetailViewController alloc] initWithNibName:@"PeopleDetailViewController" bundle:nil];
    [self.detailVC.view setFrame:CGRectMake(0, 64, self.bounds.size.width, self.bounds.size.height - 64 - 65)];
    
    [self addSubview:self.detailVC.view];
    [self addSubview:self.bottomView];
    
    [self addSubview:self.navigationBarView];
}

- (NavigationBarView *)navigationBarView {
    if (!_navigationBarView) {

        _navigationBarView = [[NavigationBarView alloc] initWithTitle:@"资料"
                                                           titleColor:nil
                                                      backgroundColor:nil
                                              leftButtonBgViewOriginX:0
                                                 leftButtonImageWidth:22
                                                 leftButtonTitleColor:[UIColor whiteColor]
                                                       leftButtonName:@"返回"
                                                  leftButtonImageName:@"navigation_bar_return_button_left_arrow"
                                                      rightButtonName:nil
                                                 rightButtonImageName:nil];
    }
    return _navigationBarView;
}

- (UIView *)bottomView {
    if (!_bottomView) {
        _bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, self.bounds.size.height - 65, self.bounds.size.width, 65)];
//        [_bottomView setBackgroundColor:LgColor(70, 1)];
        [_bottomView setBackgroundColor:[UIColor whiteColor]];
        
        [_bottomView addSubview:self.lineView];
        [_bottomView addSubview:self.addFriendBtn];
        [_bottomView addSubview:self.messageBtn];
        [_bottomView addSubview:self.playBtn];
    }
    return _bottomView;
}

- (UIView *)lineView {
    if (!_lineView) {
        _lineView = [[UIView alloc] initWithFrame:CGRectMake(20, 0, self.bottomView.bounds.size.width - 40, 1/[UIScreen mainScreen].scale)];
        [_lineView setBackgroundColor:LgColor(204, 1)];
    }
    return _lineView;
}

- (UIButton *)addFriendBtn {
    if (!_addFriendBtn) {
        _addFriendBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_addFriendBtn setFrame:CGRectMake(20, 10, (self.bounds.size.width - 20 - 20 - 20)/3, 45)];
        [_addFriendBtn setTitle:@"加友" forState:UIControlStateNormal];
        [_addFriendBtn.titleLabel setFont:[UIFont systemFontOfSize:18]];
        
        [_addFriendBtn.layer setCornerRadius:3];
        [_addFriendBtn setBackgroundColor:APPLightColor];
    }
    return _addFriendBtn;
}
- (UIButton *)messageBtn {
    if (!_messageBtn) {
        _messageBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_messageBtn setFrame:CGRectMake(20 + 10 + (self.bounds.size.width - 20 - 20 - 20)/3, 10, (self.bounds.size.width - 20 - 20 - 20)/3, 45)];
        [_messageBtn setTitle:@"聊天" forState:UIControlStateNormal];
        
        [_messageBtn.layer setCornerRadius:3];
        [_messageBtn setBackgroundColor:APPLightColor];
    }
    return _messageBtn;
}
- (UIButton *)playBtn {
    if (!_playBtn) {
        _playBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_playBtn setFrame:CGRectMake(20 + 20 + 2 * (self.bounds.size.width - 20 - 20 - 20)/3, 10, (self.bounds.size.width - 20 - 20 - 20)/3, 45)];
        [_playBtn setTitle:@"约玩" forState:UIControlStateNormal];
        
        [_playBtn.layer setCornerRadius:3];
        [_playBtn setBackgroundColor:[UIColor orangeColor]];
    }
    return _playBtn;
}
@end
