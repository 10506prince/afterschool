//
//  GameAcountInfoView.h
//  AfterSchool
//
//  Created by lg on 15/12/30.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LGGamePlayerInfo.h"

@interface GameAccountInfoView : UIView

@property (nonatomic, strong) LGGamePlayerInfo * gameAccountInfo;

@end
