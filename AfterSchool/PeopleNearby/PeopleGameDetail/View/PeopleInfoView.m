//
//  PeopleInfoView.m
//  AfterSchool
//
//  Created by lg on 15/12/30.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "PeopleInfoView.h"
#import "UIImageView+WebCache.h"
#import "UIImage+ImageWithColor.h"
#import "UIButton+WebCache.h"
#import "Common.h"

@interface PeopleInfoView()

@property (nonatomic, strong) IBOutlet UIImageView * userInfoBackImageView;///<背景图
@property (nonatomic, strong) IBOutlet UIButton * userIconBtn;///<头像

@property (nonatomic, strong) IBOutlet UILabel * userNameLabel;///<用户名
@property (nonatomic, strong) IBOutlet UIImageView * sexImageView;///<性别
@property (nonatomic, strong) IBOutlet UILabel * ageLabel;///<年龄
@property (nonatomic, strong) IBOutlet UILabel * constellationLabel;///<星座
@property (nonatomic, strong) IBOutlet UIImageView * distanceImageView;
@property (nonatomic, strong) IBOutlet UILabel * distanceLabel;///<距离
@property (nonatomic, strong) IBOutlet UILabel * signatureLabel;///<个性签名

@property (nonatomic, copy) NSString * backImageUrl;
@property (nonatomic, copy) NSString * userIconUrl;
@property (nonatomic, copy) NSString * userName;
@property (nonatomic, copy) NSString * constellation;
@property (nonatomic, copy) NSString * signature;

@property (nonatomic) float distance;
@property (nonatomic) NSUInteger age;
@property (nonatomic) NSUInteger sex;

@end

IB_DESIGNABLE
@implementation PeopleInfoView

- (void)dealloc
{
    
}
- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setUp];
    }
    return self;
}
- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self setUp];
    }
    return self;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUp];
    }
    return self;
}

- (void)setUp {
    [self setBackgroundColor:[UIColor whiteColor]];
    [self addSubview:self.userInfoBackImageView];
    [self addSubview:self.userIconBtn];
    [self addSubview:self.userNameLabel];
    [self addSubview:self.distanceImageView];
    [self addSubview:self.distanceLabel];
    [self addSubview:self.signatureLabel];
    [self addSubview:self.sexImageView];
    [self addSubview:self.ageLabel];
    [self addSubview:self.constellationLabel];
    
//    [self.userInfoBackImageView setImage:[UIImage imageWithColor:LgColor(70, 1)]];
}

///用户自定义背景
- (UIImageView *)userInfoBackImageView {
    if (!_userInfoBackImageView) {
        _userInfoBackImageView = [[UIImageView alloc] init];
        [_userInfoBackImageView setImage:[UIImage imageNamed:@"userDetail_default_bg"]];
    }
    return _userInfoBackImageView;
}
///用户头像
- (UIButton *)userIconBtn {
    if (!_userIconBtn) {
        _userIconBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_userIconBtn.layer setBorderWidth:1];
        [_userIconBtn.layer setBorderColor:[UIColor whiteColor].CGColor];
        [_userIconBtn setBackgroundImage:[UIImage imageNamed:@"user_default_head.png"] forState:UIControlStateNormal];
        
        [_userIconBtn.layer setCornerRadius:2];
        [_userIconBtn.layer setMasksToBounds:YES];
        
        [_userIconBtn.layer setShadowColor:[UIColor blackColor].CGColor];
        [_userIconBtn.layer setShadowOffset:CGSizeMake(0, 0)];
        [_userIconBtn.layer setShadowRadius:1];
        [_userIconBtn.layer setShadowOpacity:0.5];
    }
    return _userIconBtn;
}
///用户名
- (UILabel *)userNameLabel {
    if (!_userNameLabel) {
        _userNameLabel = [[UILabel alloc] init];
        [_userNameLabel setText:@"用户名"];
        [_userNameLabel setFont:[UIFont systemFontOfSize:21]];
        [_userNameLabel setTextColor:[UIColor whiteColor]];
    }
    return _userNameLabel;
}
///个性签名
- (UILabel *)signatureLabel {
    if (!_signatureLabel) {
        _signatureLabel = [[UILabel alloc] init];
        [_signatureLabel setText:@"个性签名"];
        [_signatureLabel setFont:[UIFont systemFontOfSize:15]];
        [_signatureLabel setTextColor:LgColor(34, 1)];
    }
    return _signatureLabel;
}
- (UIImageView *)distanceImageView {
    if (!_distanceImageView) {
        _distanceImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
        [_distanceImageView setImage:[UIImage imageNamed:@"game_detail_apart"]];
    }
    return _distanceImageView;
}
- (UILabel *)distanceLabel {
    if (!_distanceLabel) {
        _distanceLabel = [[UILabel alloc] init];
        [_distanceLabel setText:@"0.00km"];
        [_distanceLabel setFont:[UIFont systemFontOfSize:12]];
        [_distanceLabel setTextColor:[UIColor lightGrayColor]];
    }
    return _distanceLabel;
}

- (UIImageView *)sexImageView {
    if (!_sexImageView) {
        _sexImageView = [[UIImageView alloc] init];
        [_sexImageView setBounds:CGRectMake(0, 0, 30, 12)];
    }
    return _sexImageView;
}

- (UILabel *)ageLabel {
    if (!_ageLabel) {
        _ageLabel = [[UILabel alloc] init];
        [_ageLabel setText:@"0"];
        [_ageLabel setTextColor:[UIColor whiteColor]];
        [_ageLabel setFont:[UIFont systemFontOfSize:12]];
    }
    return _ageLabel;
}

- (UILabel *)constellationLabel {
    if (!_constellationLabel) {
        _constellationLabel = [[UILabel alloc] init];
        [_constellationLabel setText:@"星座"];
        [_constellationLabel setTextColor:[UIColor lightGrayColor]];
        [_constellationLabel setFont:[UIFont systemFontOfSize:12]];
    }
    return _constellationLabel;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self.userInfoBackImageView setFrame:CGRectMake(0, 0, self.bounds.size.width, 280)];
    
    [self.userIconBtn setFrame:CGRectMake(20, 230, 80, 80)];
    
    [self.userNameLabel sizeToFit];
    [self.userNameLabel setFrame:CGRectMake(110, 250, self.userNameLabel.bounds.size.width, self.userNameLabel.bounds.size.height)];
    
    [self.signatureLabel sizeToFit];
    [self.signatureLabel setFrame:CGRectMake(20, 330, self.signatureLabel.bounds.size.width < (self.bounds.size.width - 40) ? self.signatureLabel.bounds.size.width : (self.bounds.size.width - 40), self.signatureLabel.bounds.size.height)];
    
    [self.distanceLabel sizeToFit];
    [self.distanceLabel setFrame:CGRectMake(self.bounds.size.width - 20 - self.distanceLabel.bounds.size.width, 290, self.distanceLabel.bounds.size.width, self.distanceLabel.bounds.size.height)];
    [self.distanceImageView setCenter:CGPointMake(self.distanceLabel.frame.origin.x  - self.distanceImageView.bounds.size.width/2 - 5, self.distanceLabel.center.y)];
    
    [self.sexImageView setFrame:CGRectMake(110, 290, self.sexImageView.bounds.size.width, self.sexImageView.bounds.size.height)];
    
    [self.ageLabel sizeToFit];
    [self.ageLabel setCenter:CGPointMake(130, self.sexImageView.center.y)];
    
    [self.constellationLabel sizeToFit];
    [self.constellationLabel setCenter:CGPointMake(150 + self.constellationLabel.bounds.size.width/2, self.sexImageView.center.y)];
}

#pragma setValue
//- (void)setBackImageUrl:(NSString *)backImageUrl {
//    _backImageUrl = backImageUrl;
//    
//    [self.userInfoBackImageView sd_setImageWithURL:[NSURL URLWithString:_backImageUrl] placeholderImage:[UIImage imageNamed:@"userDetail_default_bg"]];
//}
//
//- (void)setUserIconUrl:(NSString *)userIconUrl {
//    _userIconUrl = userIconUrl;
//    
//    [self.userIconBtn sd_setBackgroundImageWithURL:[NSURL URLWithString:_userIconUrl] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"user_default_head"]];
//    [self.userIconBtn sd_setBackgroundImageWithURL:[NSURL URLWithString:_userIconUrl] forState:UIControlStateHighlighted placeholderImage:[UIImage imageNamed:@"user_default_head"]];
//}
//
//- (void)setUserName:(NSString *)userName {
//    _userName = userName;
//    
//    [self.userNameLabel setText:_userName];
//    [self setNeedsLayout];
//}
//
//- (void)setSignature:(NSString *)signature {
//    _signature = signature;
//    
//    [self.signatureLabel setText:_signature];
//    [self setNeedsLayout];
//}
//
//- (void)setConstellation:(NSString *)constellation {
//    _constellation = constellation;
//    
//    [self.constellationLabel setText:[NSString stringWithFormat:@"%@座", _constellation]];
//    [self setNeedsLayout];
//}
//
//- (void)setSex:(NSUInteger)sex {
//    _sex = sex;
//
//    [self.sexImageView setImage:_sex == 0 ? [UIImage imageNamed:@"sex0"] : [UIImage imageNamed:@"sex1"]];
//}
//
//- (void)setAge:(NSUInteger)age {
//    _age = age;
//    
//    [self.ageLabel setText:[NSString stringWithFormat:@"%ld", (unsigned long)_age]];
//}
//
//- (void)setDistance:(float)distance {
//    _distance = distance;
//    
//    [self.distanceLabel setText:[NSString stringWithFormat:@"%.2fkm", _distance]];
//    [self setNeedsLayout];
//}

- (void)setUserInfo:(LGDaKaInfo *)userInfo {
    _userInfo = userInfo;
    
    [self.userInfoBackImageView sd_setImageWithURL:[NSURL URLWithString:@""] placeholderImage:[UIImage imageNamed:@"userDetail_default_bg"]];
    
    [self.userIconBtn sd_setBackgroundImageWithURL:[NSURL URLWithString:_userInfo.headImageUrl] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"user_default_head"]];
    [self.userIconBtn sd_setBackgroundImageWithURL:[NSURL URLWithString:_userInfo.headImageUrl] forState:UIControlStateHighlighted placeholderImage:[UIImage imageNamed:@"user_default_head"]];
    
    [self.userNameLabel setText:_userInfo.nickName];
    
    
    if (_userInfo.signature.length > 0) {
        [self.signatureLabel setText:_userInfo.signature];
    } else {
        [self.signatureLabel setText:@"这个人很懒，什么都没有留下"];
    }
    
    [self.constellationLabel setText:[NSString stringWithFormat:@"%@座", _userInfo.constellation]];
    
    [self.sexImageView setImage:_userInfo.sex == 0 ? [UIImage imageNamed:@"sex0"] : [UIImage imageNamed:@"sex1"]];
    
    [self.ageLabel setText:[NSString stringWithFormat:@"%ld", (unsigned long)_userInfo.age]];
    
    [self.distanceLabel setText:[NSString stringWithFormat:@"%.2fkm", _userInfo.distance]];
    
    [self setNeedsLayout];
}
@end








