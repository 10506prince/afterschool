//
//  GameAcountInfoView.m
//  AfterSchool
//
//  Created by lg on 15/12/30.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "GameAccountInfoView.h"
#import "Common.h"
#import "CAShapeLayer+CustomPath.h"

@interface GameAccountInfoView ()

@property (nonatomic, strong) UILabel * gameGradeStrLabel;///<段位Str
@property (nonatomic, strong) UILabel * gameServerStrLabel;///<服务器Str
@property (nonatomic, strong) UILabel * gameFightingStrLabel;///<战斗力Str

@property (nonatomic, strong) UILabel * gameNickNameLabel;
@property (nonatomic, strong) UILabel * gameGradeLabel;///<段位
@property (nonatomic, strong) UILabel * gameServerLabel;///<服务器
@property (nonatomic, strong) UILabel * gameFightingLabel;///<战斗力

@property (nonatomic, strong) CAShapeLayer * line1;
@property (nonatomic, strong) CAShapeLayer * line2;
@property (nonatomic, strong) CAShapeLayer * line3;

@property (nonatomic, strong) IBInspectable UIColor * borderColor;
@property (nonatomic, copy) IBInspectable NSString * nickName;
@property (nonatomic, copy) IBInspectable NSString * grade;
@property (nonatomic, copy) IBInspectable NSString * server;
@property (nonatomic, copy) IBInspectable NSString * attack;
@end

IB_DESIGNABLE
@implementation GameAccountInfoView
- (void)dealloc
{
    
}
- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setUp];
    }
    return self;
}
- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self setUp];
    }
    return self;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUp];
    }
    return self;
}

- (void)setUp {
    
    [self.layer setCornerRadius:3];
    [self.layer setBorderWidth:1/[UIScreen mainScreen].scale];
    
    self.gameNickNameLabel = [self getGameNickNameLabel];
    
    self.gameGradeStrLabel = [self getStrLabel];
    self.gameServerStrLabel = [self getStrLabel];
    self.gameFightingStrLabel = [self getStrLabel];
    
    self.gameGradeLabel = [self getValueLabel];
    self.gameServerLabel = [self getValueLabel];
    self.gameFightingLabel = [self getValueLabel];
    
    self.line1 = [self getLineLayer];
    self.line2 = [self getLineLayer];
    self.line3 = [self getLineLayer];
    
    [self addSubview:self.gameNickNameLabel];
    [self addSubview:self.gameGradeStrLabel];
    [self addSubview:self.gameServerStrLabel];
    [self addSubview:self.gameFightingStrLabel];
    [self addSubview:self.gameGradeLabel];
    [self addSubview:self.gameServerLabel];
    [self addSubview:self.gameFightingLabel];
    
    [self.layer addSublayer:self.line1];
    [self.layer addSublayer:self.line2];
    [self.layer addSublayer:self.line3];
    
    
    [self.gameGradeStrLabel setText:@"段位"];
    [self.gameServerStrLabel setText:@"服务器"];
    [self.gameFightingStrLabel setText:@"战斗力"];
    [self setBorderColor:LgColor(204, 1)];
    [self setNickName:@"游戏账号"];
    [self setGrade:@"无"];
    [self setServer:@"无"];
    [self setAttack:@"无"];
}

- (UIView *)getLine {
    UIView * line = [[UIView alloc] init];
    [line setBackgroundColor:[UIColor lightGrayColor]];
    
    return line;
}

- (CAShapeLayer *)getLineLayer {
    CAShapeLayer * lineLayer = [CAShapeLayer layer];
    [lineLayer setFillColor:[UIColor clearColor].CGColor];
    [lineLayer setLineWidth:1/[UIScreen mainScreen].scale];
    
    return lineLayer;
}

- (UILabel *)getGameNickNameLabel {
    UILabel * label = [[UILabel alloc] init];
    [label setText:@"游戏昵称"];
    [label setFont:[UIFont systemFontOfSize:18]];
    [label setTextColor:[UIColor blackColor]];
    [label setTextAlignment:NSTextAlignmentCenter];
    
    return label;
}

///Str格式
- (UILabel *)getStrLabel {
    
    UILabel * strLabel = [[UILabel alloc] init];
    [strLabel setFont:[UIFont systemFontOfSize:14]];
    [strLabel setTextColor:[UIColor lightGrayColor]];
    return strLabel;
}
///Value格式
- (UILabel *)getValueLabel {
    UILabel * valueLabel = [[UILabel alloc] init];
    [valueLabel setFont:[UIFont systemFontOfSize:15]];
    [valueLabel setTextColor:LgColor(56, 1)];
    return valueLabel;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self.gameNickNameLabel sizeToFit];
    [self.gameNickNameLabel setCenter:CGPointMake(self.bounds.size.width/2, 26)];
    
    [self.gameGradeStrLabel sizeToFit];
    [self.gameGradeStrLabel setCenter:CGPointMake(self.bounds.size.width/6, 53 + 22)];
    
    [self.gameServerStrLabel sizeToFit];
    [self.gameServerStrLabel setCenter:CGPointMake(self.bounds.size.width/2, 53 + 22)];
    
    [self.gameFightingStrLabel sizeToFit];
    [self.gameFightingStrLabel setCenter:CGPointMake(self.bounds.size.width/6 * 5, 53 + 22)];
    
    [self.gameGradeLabel sizeToFit];
    [self.gameGradeLabel setCenter:CGPointMake(self.gameGradeStrLabel.center.x, CGRectGetMaxY(self.gameGradeStrLabel.frame) + 16)];
    
    [self.gameServerLabel sizeToFit];
    [self.gameServerLabel setCenter:CGPointMake(self.gameServerStrLabel.center.x, CGRectGetMaxY(self.gameServerStrLabel.frame) + 16)];
    
    [self.gameFightingLabel sizeToFit];
    [self.gameFightingLabel setCenter:CGPointMake(self.gameFightingStrLabel.center.x, CGRectGetMaxY(self.gameFightingStrLabel.frame) + 16)];
    
    
    self.line1.path = [CAShapeLayer getLinePathWithSuperBounds:self.bounds startPoint:CGPointMake(0, 53) endPoint:CGPointMake(self.bounds.size.width, 53)];
    self.line2.path = [CAShapeLayer getLinePathWithSuperBounds:self.bounds startPoint:CGPointMake(self.bounds.size.width/3, 53) endPoint:CGPointMake(self.bounds.size.width/3, self.bounds.size.height)];
    self.line3.path = [CAShapeLayer getLinePathWithSuperBounds:self.bounds startPoint:CGPointMake(self.bounds.size.width/3*2, 53) endPoint:CGPointMake(self.bounds.size.width/3*2, self.bounds.size.height)];
}

#pragma mark - setValue
- (void)setBorderColor:(UIColor *)borderColor {
    _borderColor = borderColor;
    
    self.line1.strokeColor = _borderColor.CGColor;
    self.line2.strokeColor = _borderColor.CGColor;
    self.line3.strokeColor = _borderColor.CGColor;
    self.layer.borderColor = _borderColor.CGColor;
}
- (void)setNickName:(NSString *)nickName {
    _nickName = [nickName copy];
    
    [self.gameNickNameLabel setText:_nickName];
    [self setNeedsLayout];
}
- (void)setGrade:(NSString *)grade {
    _grade = [grade copy];
    
    [self.gameGradeLabel setText:_grade];
    [self setNeedsLayout];
}
- (void)setServer:(NSString *)server {
    _server = [server copy];
    
    [self.gameServerLabel setText:_server];
    [self setNeedsLayout];
}
- (void)setAttack:(NSString *)attack {
    _attack = [attack copy];
    
    [self.gameFightingLabel setText:_attack];
    [self setNeedsLayout];
}

#pragma mark - setDataModel
- (void)setGameAccountInfo:(LGGamePlayerInfo *)gameAccountInfo {
    _gameAccountInfo = gameAccountInfo;
    
    if (_gameAccountInfo.playerName.length > 0) {
        self.nickName = _gameAccountInfo.playerName;
    } else {
        self.nickName = @"无";
    }
    
    if (_gameAccountInfo.grade.length > 0) {
        self.grade = _gameAccountInfo.grade;
    } else {
        self.grade = @"无";
    }
    
    if (_gameAccountInfo.serverName.length > 0) {
        self.server = _gameAccountInfo.serverName;
    } else {
        self.server = @"无";
    }
    
    self.attack = [NSString stringWithFormat:@"%ld", (unsigned long)_gameAccountInfo.attack];
}

@end
