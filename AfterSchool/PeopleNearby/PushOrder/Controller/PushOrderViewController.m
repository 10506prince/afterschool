//
//  CityViewController.m
//  AfterSchool
//
//  Created by lg on 15/10/22.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "PushOrderViewController.h"
#import "Common.h"
#import "NSString+JSON.h"
//#import "UserOrderStatusViewController.h"
#import "UIColor+RGB.h"
#import "UIButton+SetBackgroundColor.h"
#import "TalkingData.h"

#import "TDSingleton.h"
#import "TDNetworking.h"
#import "Toast+UIView.h"
#import "SVProgressHUD.h"
#import "TDNetworkRequest.h"

#import "NotificationName.h"
#import "OrderTimeoutViewController.h"

#import "LGDefineNetServer.h"

#import "NavigationBarView.h"
#import "WaitAcceptOrderViewController.h"
#import "NoneUserAcceptOrderViewController.h"

@interface PushOrderViewController ()

@property (nonatomic, strong) LGOrderInfo *currentOrder;
@property (nonatomic, assign) OrderModelType orderModelType;
@property (nonatomic, strong) LGSceneInfo * scene;

@property (nonatomic, strong) NavigationBarView * navigationBarView;
@property (nonatomic, strong) WaitAcceptOrderViewController * waitAcceptOrderVC;
@property (nonatomic, strong) NoneUserAcceptOrderViewController * noneUserAcceptOrderVC;

@end

@implementation PushOrderViewController

- (void)dealloc {
    NSLogSelfMethodName;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (instancetype)initWithOrder:(LGOrderInfo *)order orderModelType:(OrderModelType)orderModelType sceneInfo:(LGSceneInfo *)sceneInfo {
    self = [super init];
    if (self) {
        self.currentOrder = order;
        self.orderModelType = orderModelType;
        self.scene = sceneInfo;
        self.title = sceneInfo.name;
    }
    return self;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [TalkingData trackPageBegin:selfControllerName];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [TalkingData trackPageEnd:selfControllerName];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initData];
    [self initUserInterface];
    
    [self addAcceptOrderNotification];
    [self addRejectOrderNotification];
    
    if (self.orderModelType == OrderModelTypeMaster) {
        [self.waitAcceptOrderVC setDetail:@"大咖正在靠近..."];
    } else {
        /*
        约妹子
        
        - 调整姿势保持耐心，一大波妹子正在靠近 0
        
        约男神
        
        - 调整姿势保持耐心，一大波男神正在靠近 1
        
        想上分
        
        - 调整姿势保持耐心，一大波大神正在靠近 2
        
        保晋级
        
        - 调整姿势保持耐心，一大波打手正在靠近 3
        
        无聊
        
        - 调整姿势保持耐心，一大波大咖正在靠近 4
        */
        
        NSArray * detailStrs = @[@"一大波妹子正在靠近",@"一大波男神正在靠近",@"一大波大神正在靠近",@"一大波打手正在靠近",@"一大波大咖正在靠近",];
        NSString * detailStr = nil;
        switch (self.scene.sceneId) {
            case 1:
                detailStr = detailStrs[4];
                break;
            case 5:
                detailStr = detailStrs[3];
                break;
            case 3:
                detailStr = detailStrs[1];
                break;
            case 4:
                detailStr = detailStrs[2];
                break;
            case 2:
                detailStr = detailStrs[0];
                break;
                
            default:
                break;
        }
        
        [self.waitAcceptOrderVC setDetail:detailStr];
    }
}

- (void)initData {
    
}

- (void)initUserInterface {
    
    ///等待接单
    self.waitAcceptOrderVC = [[WaitAcceptOrderViewController alloc] initWithNibName:@"WaitAcceptOrderViewController" bundle:nil];
    [self.waitAcceptOrderVC.view setFrame:[UIScreen mainScreen].bounds];
    [self addChildViewController:self.waitAcceptOrderVC];
    
    ///无人接单
    self.noneUserAcceptOrderVC = [[NoneUserAcceptOrderViewController alloc] initWithNibName:@"NoneUserAcceptOrderViewController" bundle:nil];
    [self.noneUserAcceptOrderVC.view setFrame:[UIScreen mainScreen].bounds];
    [self addChildViewController:self.noneUserAcceptOrderVC];
    [self.noneUserAcceptOrderVC.changeBtn addTarget:self action:@selector(changeAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.noneUserAcceptOrderVC.continueBtn addTarget:self action:@selector(continueAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    [self.navigationBarView.leftButton addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.waitAcceptOrderVC.view];
    [self.view addSubview:self.navigationBarView];
    
    
    __weak typeof(self) weakSelf = self;
    
    if (self.currentOrder.timeOut > 0) {
        [self.waitAcceptOrderVC.countDownView setStartCount:self.currentOrder.timeOut];
    } else {
        [self.waitAcceptOrderVC.countDownView setStartCount:30];
    }
    
    [self.waitAcceptOrderVC.countDownView start];
    [self.waitAcceptOrderVC.countDownView addFinishActionBlock:^{
        [weakSelf.waitAcceptOrderVC.countDownView stop];
        [weakSelf.view insertSubview:weakSelf.noneUserAcceptOrderVC.view belowSubview:weakSelf.navigationBarView];
    }];
}

- (NavigationBarView *)navigationBarView {
    if (!_navigationBarView) {
        _navigationBarView = [[NavigationBarView alloc] initWithTitle:@"派单中..."
                                                           titleColor:nil
                                                      backgroundColor:nil
                                              leftButtonBgViewOriginX:0
                                                 leftButtonImageWidth:22
                                                 leftButtonTitleColor:[UIColor whiteColor]
                                                       leftButtonName:@"取消"
                                                  leftButtonImageName:@"navigation_bar_return_button_left_arrow" rightButtonName:nil
                                                 rightButtonImageName:nil];
    }
    return _navigationBarView;
}

- (void)addAcceptOrderNotification {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(bigShotAcceptOrder:) name:TDBigShotAcceptOrder object:nil];
}
- (void)addRejectOrderNotification {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(bigShotRejectOrder:) name:TDBigShotRejectOrder object:nil];
}

#pragma mark - Action
- (IBAction)backAction:(id)sender {

    [self.waitAcceptOrderVC.countDownView stop];

    if (self.currentOrder) {
        self.currentOrder.timeOutFlag = YES;
        [LGDefineNetServer cancelOrder:self.currentOrder success:^(id result) {
        } failure:^(NSString *msg) {
            [SVProgressHUD showErrorWithStatus:msg];
        }];
    }
    [self.navigationController popToRootViewControllerAnimated:YES];
}

///修改约单条件
- (IBAction)changeAction:(id)sender {
    [self.waitAcceptOrderVC.countDownView stop];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)continueAction:(id)sender {
    
    __weak typeof(self) weakSelf = self;
    
    if (self.orderModelType == OrderModelTypeMaster) {
        if (weakSelf.currentOrder) {
            [SVProgressHUD showWithStatus:@"处理中..." maskType:SVProgressHUDMaskTypeClear];
            [LGDefineNetServer creatDirectivityOrder:self.currentOrder success:^(id result) {
                [SVProgressHUD dismiss];
                [weakSelf.waitAcceptOrderVC.countDownView stop];
                [weakSelf.waitAcceptOrderVC.countDownView setStartCount:[[result objectForKey:@"timeOut"] integerValue]];
                [weakSelf.waitAcceptOrderVC.countDownView start];
                [weakSelf.view insertSubview:weakSelf.waitAcceptOrderVC.view belowSubview:weakSelf.navigationBarView];
            } failure:^(NSString *msg) {
                NSLog(@"%@",msg);
                [SVProgressHUD dismissWithError:msg];
            }];
        } else {
            [weakSelf.waitAcceptOrderVC.countDownView stop];
            [weakSelf.waitAcceptOrderVC.countDownView start];
            [weakSelf.view insertSubview:weakSelf.waitAcceptOrderVC.view belowSubview:weakSelf.navigationBarView];
        }
    } else {
        if (weakSelf.currentOrder) {
            [SVProgressHUD showWithStatus:@"处理中..." maskType:SVProgressHUDMaskTypeClear];
            [LGDefineNetServer creatDistributeOrder:self.currentOrder success:^(id result) {
                [SVProgressHUD dismiss];
                [weakSelf.waitAcceptOrderVC.countDownView stop];
                [weakSelf.waitAcceptOrderVC.countDownView setStartCount:[[result objectForKey:@"timeOut"] integerValue]];
                [weakSelf.waitAcceptOrderVC.countDownView start];
                [weakSelf.view insertSubview:weakSelf.waitAcceptOrderVC.view belowSubview:weakSelf.navigationBarView];
            } failure:^(NSString *msg) {
                NSLog(@"%@",msg);
                [SVProgressHUD dismissWithError:msg];
            }];
        } else {
            [weakSelf.waitAcceptOrderVC.countDownView stop];
            [weakSelf.waitAcceptOrderVC.countDownView start];
            [weakSelf.view insertSubview:weakSelf.waitAcceptOrderVC.view belowSubview:weakSelf.navigationBarView];
        }
    }
}

#pragma mark - Notification
- (void)bigShotAcceptOrder:(NSNotification *)sender
{
    NSString *content = sender.object;
    NSLog(@"BigShotViewController content:%@", content);
    NSDictionary *dic = [NSString dictionaryFromJSONString:content];
    NSLog(@"bigShotAcceptOrder dic:%@", dic);
    
    [self.waitAcceptOrderVC.countDownView stop];
    
    NSLog(@"%@", self.navigationController.viewControllers);
    [self.navigationController popToRootViewControllerAnimated:YES];
//    UserOrderStatusViewController *vc = [[UserOrderStatusViewController alloc] init];
//    vc.bigShotInfo = _bigShotInfo;
//    vc.orderID = [NSString stringWithFormat:@"%@", dic[@"orderId"]];
//    [self.navigationController pushViewController:vc animated:YES];
}
- (void)bigShotRejectOrder:(NSNotification *)sender
{
//    NSString *content = sender.object;
//    NSDictionary *dic = [NSString dictionaryFromJSONString:content];
    
    [self.waitAcceptOrderVC.countDownView stop];
    
//    [SVProgressHUD showErrorWithStatus:@"大咖拒绝接单！"];
    [self.navigationController popViewControllerAnimated:YES];
}



#pragma mark - 



@end
