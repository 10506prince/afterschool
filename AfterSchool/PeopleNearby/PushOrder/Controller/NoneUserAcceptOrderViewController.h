//
//  NoneUserAcceptOrderViewController.h
//  AfterSchool
//
//  Created by lg on 16/1/7.
//  Copyright © 2016年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NoneUserAcceptOrderViewController : UIViewController

@property (nonatomic, weak) IBOutlet UIButton * changeBtn;
@property (nonatomic, weak) IBOutlet UIButton * continueBtn;
@end
