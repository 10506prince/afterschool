//
//  WaitAcceptOrderViewController.m
//  AfterSchool
//
//  Created by lg on 16/1/7.
//  Copyright © 2016年 Teson Draw. All rights reserved.
//

#import "WaitAcceptOrderViewController.h"
#import "Common.h"
#import "TalkingData.h"

@interface WaitAcceptOrderViewController ()

@property (nonatomic, weak) IBOutlet UILabel * detailLabel;

@end

@implementation WaitAcceptOrderViewController

- (void)dealloc
{
    NSLogSelfMethodName;
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [TalkingData trackPageBegin:selfControllerName];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [TalkingData trackPageEnd:selfControllerName];
}
- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)setDetail:(NSString *)detail {
    _detail = [detail copy];
    
    [self.detailLabel setText:_detail];
    [self.view setNeedsUpdateConstraints];
}

@end
