//
//  NoneUserAcceptOrderViewController.m
//  AfterSchool
//
//  Created by lg on 16/1/7.
//  Copyright © 2016年 Teson Draw. All rights reserved.
//

#import "NoneUserAcceptOrderViewController.h"
#import "Common.h"
#import "TalkingData.h"

@interface NoneUserAcceptOrderViewController ()

@end

@implementation NoneUserAcceptOrderViewController

- (void)dealloc
{
    NSLogSelfMethodName;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [TalkingData trackPageBegin:selfControllerName];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [TalkingData trackPageEnd:selfControllerName];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initData];
    [self initUserInterface];
}

- (void)initData {
    [self.changeBtn.layer setCornerRadius:3];
    [self.continueBtn.layer setCornerRadius:3];
    
    [self.changeBtn.layer setMasksToBounds:YES];
    [self.continueBtn.layer setMasksToBounds:YES];
}
- (void)initUserInterface {
    
}

@end
