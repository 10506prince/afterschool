//
//  CityViewController.h
//  AfterSchool
//
//  Created by lg on 15/10/22.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGViewController.h"
#import "LGOrderInfo.h"
#import "OrderModelType.h"
#import "LGSceneInfo.h"

@interface PushOrderViewController : LGViewController

- (instancetype)initWithOrder:(LGOrderInfo *)order orderModelType:(OrderModelType)orderModelType sceneInfo:(LGSceneInfo *)sceneInfo;

@end
