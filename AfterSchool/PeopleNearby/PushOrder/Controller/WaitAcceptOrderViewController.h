//
//  WaitAcceptOrderViewController.h
//  AfterSchool
//
//  Created by lg on 16/1/7.
//  Copyright © 2016年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LGCountDownView.h"

@interface WaitAcceptOrderViewController : UIViewController

@property (nonatomic, weak) IBOutlet LGCountDownView * countDownView;
@property (nonatomic, copy) NSString * detail;

@end
