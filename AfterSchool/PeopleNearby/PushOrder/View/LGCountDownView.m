//
//  LGCountDownView.m
//  AfterSchool
//
//  Created by lg on 16/1/7.
//  Copyright © 2016年 Teson Draw. All rights reserved.
//

#import "LGCountDownView.h"
#import "Common.h"

@interface LGCountDownView ()

@property (nonatomic) float radius;///<最小50

@property (nonatomic) NSInteger timerCount;
@property (nonatomic) NSInteger currentCount;
@property (nonatomic, strong) NSTimer * timer;
@property (copy) LGCountDownViewFinishActionBlock finishActionBlock;

@property (nonatomic, strong) UIView * backView;
@property (nonatomic, strong) UILabel * countLabel;
@property (nonatomic, strong) UILabel * strLabel;

@end

IB_DESIGNABLE
@implementation LGCountDownView

- (void)dealloc
{
    NSLog(@"%s", __FUNCTION__);
}
- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setUp];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self setUp];
    }
    return self;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUp];
    }
    return self;
}
- (void)setUp {
    [self setBackgroundColor:[UIColor clearColor]];
    [self addSubview:self.backView];
    [self addSubview:self.countLabel];
    [self addSubview:self.strLabel];
    
    self.radius = 50;
    self.timerCount = 0;
    self.currentCount = 0;
    self.startCount = 30;
}
- (UILabel *)countLabel {
    if (!_countLabel) {
        _countLabel = [[UILabel alloc] init];
        [_countLabel setFont:[UIFont systemFontOfSize:50]];
        [_countLabel setTextColor:[UIColor orangeColor]];
    }
    return _countLabel;
}
- (UILabel *)strLabel {
    if (!_strLabel) {
        _strLabel = [[UILabel alloc] init];
        [_strLabel setFont:[UIFont systemFontOfSize:16]];
        [_strLabel setTextColor:LgColor(1, 56)];
        [_strLabel setText:@"秒"];
    }
    return _strLabel;
}
- (UIView *)backView {
    if (!_backView) {
        _backView = [[UIView alloc] init];
        [_backView setBackgroundColor:[UIColor whiteColor]];
        [self.backView.layer setMasksToBounds:YES];
    }
    return _backView;
}
- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat width = self.bounds.size.width;
    CGFloat height = self.bounds.size.height;
    
    [self.backView setBounds:CGRectMake(0, 0, self.radius*2, self.radius*2)];
    [self.backView setCenter:CGPointMake(self.bounds.size.width/2, self.bounds.size.height/2)];
    [self.backView.layer setCornerRadius:self.radius];
    
    [self.countLabel sizeToFit];
    [self.countLabel setCenter:CGPointMake(width/2, height/2 - self.countLabel.bounds.size.height/2 + 20)];
    
    [self.strLabel sizeToFit];
    [self.strLabel setCenter:CGPointMake(width/2, CGRectGetMaxY(self.countLabel.frame) + self.strLabel.bounds.size.height/2)];
}

#pragma mark - setValue

- (void)setRadius:(float)radius {
    _radius = radius > 50 ? radius : 50;
    
    [self setNeedsLayout];
}
- (void)setStartCount:(NSInteger)startCount {
    _startCount = startCount;
    
    self.currentCount = _startCount;
}

- (void)setCurrentCount:(NSInteger)currentCount {
    _currentCount = currentCount;
    
    [_countLabel setText:[NSString stringWithFormat:@"%ld", (long)_currentCount]];
    [self setNeedsLayout];
}

- (void)start {
    if (self.timer) {
        [self.timer setFireDate:[NSDate distantPast]];
    } else {
        self.timer = [NSTimer timerWithTimeInterval:0.5 target:self selector:@selector(timerAction:) userInfo:nil repeats:YES];
        [[NSRunLoop mainRunLoop] addTimer:self.timer forMode:NSRunLoopCommonModes];
    }
}
- (void)pause {
    [self.timer setFireDate:[NSDate distantFuture]];
}
- (void)stop {
    [self.timer invalidate];
    self.timer = nil;
    self.startCount = self.startCount;
}

- (void)addFinishActionBlock:(LGCountDownViewFinishActionBlock)finishActionBlock {
    self.finishActionBlock = finishActionBlock;
}

#pragma mark - methd
- (IBAction)timerAction:(id)sender {
//    NSLog(@"%@ %s",NSStringFromClass([self class]), __FUNCTION__);
    self.timerCount++;
    if (self.timerCount%2 == 0) {
        if (self.currentCount > 0) {
            [self setCurrentCount:self.currentCount - 1];
            
            float scale = 2.5f;
            
            CAShapeLayer * circleShape = [self getCircleLayer];
            [self.layer addSublayer:circleShape];
            
            CAAnimationGroup *groupAnimation = [self createFlashAnimationWithScale:scale duration:2.0f];
            
            /* Use KVC to remove layer to avoid memory leak */
            [groupAnimation setValue:circleShape forKey:@"circleShaperLayer"];
            [circleShape addAnimation:groupAnimation forKey:nil];
//            [circleShape setDelegate:self];
        } else {
            [self pause];
            LGCountDownViewFinishActionBlock block = self.finishActionBlock;
            if (block) {
                block();
            }
        }
    }
}

- (CAShapeLayer *)getCircleLayer
{
    CAShapeLayer *circleShape = [CAShapeLayer layer];
    circleShape.path = [UIBezierPath bezierPathWithArcCenter:self.backView.center radius:self.radius + 5 startAngle:0 endAngle:M_PI*2 clockwise:YES].CGPath;
//    circleShape.path = self getCirclePathWithRect:CGRectMake(-CGRectGetMidX(self.backView.frame), -CGRectGetMidY(self.bounds), width, height) radius:<#(CGFloat)#>
//    circleShape.position = self.back
//    self.layer.anchorPoint = CGPointMake(0.5, 0.5);
    //circleShape.position =
    circleShape.frame = self.backView.bounds;
    circleShape.anchorPoint = CGPointMake(0.5, 0.5);
    circleShape.fillColor = [UIColor clearColor].CGColor;
    circleShape.strokeColor = APPLightColor.CGColor;
    
    circleShape.opacity = 0;
    circleShape.lineWidth = 5;
    
    return circleShape;
}

- (CGPathRef)getCirclePathWithRect:(CGRect)rect radius:(CGFloat)radius {
    UIBezierPath * path = [UIBezierPath bezierPathWithRoundedRect:rect cornerRadius:radius];
    return path.CGPath;
}

#pragma mark - animation

- (CAAnimationGroup *)createFlashAnimationWithScale:(CGFloat)scale duration:(CGFloat)duration
{
    CABasicAnimation *scaleAnimation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    scaleAnimation.fromValue = [NSValue valueWithCATransform3D:CATransform3DIdentity];
    scaleAnimation.toValue = [NSValue valueWithCATransform3D:CATransform3DMakeScale(scale, scale, 1)];
    
    CABasicAnimation *alphaAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    alphaAnimation.fromValue = @1;
    alphaAnimation.toValue = @0;
    
    CAAnimationGroup *animation = [CAAnimationGroup animation];
    animation.animations = @[scaleAnimation, alphaAnimation];
    animation.delegate = self;
    animation.duration = duration;
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    
    return animation;
}

#pragma mark - CAAnimationDelegate
- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
    CALayer *layer = [anim valueForKey:@"circleShaperLayer"];
    if (layer) {
        [layer removeFromSuperlayer];
    }
}
@end
