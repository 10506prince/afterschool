//
//  LGCountDownView.h
//  AfterSchool
//
//  Created by lg on 16/1/7.
//  Copyright © 2016年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^LGCountDownViewFinishActionBlock)();

@interface LGCountDownView : UIView

@property (nonatomic) IBInspectable NSInteger startCount;

- (void)start;
- (void)pause;
- (void)stop;

- (void)addFinishActionBlock:(LGCountDownViewFinishActionBlock)finishActionBlock;

@end
