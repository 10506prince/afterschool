//
//  UserOrderStatusRootView.h
//  AfterSchool
//
//  Created by Teson Draw on 11/10/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavigationBarView.h"
#import "TDDateView.h"
#import "TDOrderStepView.h"
#import "TDUserBriefInfoView.h"

@interface UserOrderStatusRootView : UIView

@property (nonatomic, strong) NavigationBarView *navigationBarView;
@property (nonatomic, strong) TDUserBriefInfoView *userInfoView;
//@property (nonatomic, strong) UIView *portraitBackgroundView;
@property (nonatomic, strong) UIView *tableViewBackgroundView;
@property (nonatomic, strong) UIView *verticalLineView;
//
//
//@property (nonatomic, strong) UIImageView *portraitImageView;
//@property (nonatomic, strong) UIImageView *horoscopeImageView;
//@property (nonatomic, strong) UIImageView *rightArrowImageView;
//
//@property (nonatomic, strong) UILabel *nameLabel;
//@property (nonatomic, strong) UILabel *horoscopeLabel;
//@property (nonatomic, strong) UILabel *signatureLabel;

@property (nonatomic, strong) TDDateView *acceptOrderDateView;
@property (nonatomic, strong) TDDateView *waitToStartDateView;

@property (nonatomic, strong) TDOrderStepView *orderAcceptView;
@property (nonatomic, strong) TDOrderStepView *orderWaitToStartView;

//@property (nonatomic, strong) UIButton *bigShotInfoButton;

//@property (nonatomic, strong) UITableView *tableView;

@end
