//
//  UserOrderStatusRootView.m
//  AfterSchool
//
//  Created by Teson Draw on 11/10/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "UserOrderStatusRootView.h"
#import "MacrosDefinition.h"

@implementation UserOrderStatusRootView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithRed:235/255.f green:235/255.f blue:235/255.f alpha:1];
        [self addSubview:self.navigationBarView];
        [self addSubview:self.userInfoView];
        
        [self addSubview:self.tableViewBackgroundView];
        [_tableViewBackgroundView addSubview:self.verticalLineView];
        [_tableViewBackgroundView addSubview:self.acceptOrderDateView];
        [_tableViewBackgroundView addSubview:self.orderAcceptView];
        [_tableViewBackgroundView addSubview:self.waitToStartDateView];
        [_tableViewBackgroundView addSubview:self.orderWaitToStartView];
    }
    
    return self;
}

- (NavigationBarView *)navigationBarView {
    if (!_navigationBarView) {
        _navigationBarView = [[NavigationBarView alloc] initWithTitle:@"订单状态" titleColor:[UIColor whiteColor] backgroundColor:nil leftButtonImageName:@"navigation_bar_return_button" rightButtonImageName:nil];
    }
    return _navigationBarView;
}

- (TDUserBriefInfoView *)userInfoView {
    if (!_userInfoView) {
        _userInfoView = [[TDUserBriefInfoView alloc] initWithFrame:CGRectMake(0, 64, SCREEN_WIDTH, 110)];
    }
    return _userInfoView;
}

- (UIView *)tableViewBackgroundView {
    if (!_tableViewBackgroundView) {
        _tableViewBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_userInfoView.frame) + 16, SCREEN_WIDTH, SCREEN_HEIGHT - CGRectGetMaxY(_userInfoView.frame) - 16)];
        _tableViewBackgroundView.backgroundColor = [UIColor colorWithRed:244/255.f green:244/255.f blue:244/255.f alpha:1];
    }
    return _tableViewBackgroundView;
}

- (UIView *)verticalLineView {
    if (!_verticalLineView) {
        _verticalLineView = [[UIView alloc] initWithFrame:CGRectMake((_tableViewBackgroundView.bounds.size.width - 2) / 2, 0, 2, _tableViewBackgroundView.bounds.size.height)];
        _verticalLineView.backgroundColor = [UIColor colorWithRed:170/255.f green:170/255.f blue:170/255.f alpha:1];
    }
    return _verticalLineView;
}

- (TDDateView *)acceptOrderDateView {
    if (!_acceptOrderDateView) {
        _acceptOrderDateView = [[TDDateView alloc] initWithOrigin:CGPointMake((_tableViewBackgroundView.bounds.size.width / 2) - 72 + 4, 20) lanyardPosition:LanyardPositonRight];
    }
    return _acceptOrderDateView;
}

- (TDOrderStepView *)orderAcceptView {
    if (!_orderAcceptView) {
        _orderAcceptView = [[TDOrderStepView alloc] initWithOrigin:CGPointMake((_tableViewBackgroundView.bounds.size.width - 200) / 2 - 45, CGRectGetMaxY(_acceptOrderDateView.frame) + 20) backgroundColor:nil];
        _orderAcceptView.imageView.image = [UIImage imageNamed:@"order_step_cancel"];
        [_orderAcceptView addSubview:_orderAcceptView.button];
    }
    return _orderAcceptView;
}

- (TDDateView *)waitToStartDateView {
    if (!_waitToStartDateView) {
        _waitToStartDateView = [[TDDateView alloc] initWithOrigin:CGPointMake((_tableViewBackgroundView.bounds.size.width / 2) - 4, CGRectGetMaxY(_orderAcceptView.frame) + 20) lanyardPosition:LanyardPositonLeft];
    }
    return _waitToStartDateView;
}

- (TDOrderStepView *)orderWaitToStartView {
    if (!_orderWaitToStartView) {
        _orderWaitToStartView = [[TDOrderStepView alloc] initWithOrigin:CGPointMake((_tableViewBackgroundView.bounds.size.width - 200) / 2 + 45, CGRectGetMaxY(_waitToStartDateView.frame) + 20) backgroundColor:COLOR(193, 193, 193)];
    }
    return _orderWaitToStartView;
}

@end
