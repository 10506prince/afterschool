//
//  UserOrderStatusViewController.m
//  AfterSchool
//
//  Created by Teson Draw on 11/10/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "UserOrderStatusViewController.h"
#import "MacrosDefinition.h"
#import "UIImageView+WebCache.h"
#import "TDNetworking.h"
#import "TDSingleton.h"
#import "SVProgressHUD.h"

#import "TDDateConversion.h"
#import "PeopleGameDetailViewController.h"

#import "TDNetworkRequest.h"

@interface UserOrderStatusViewController () <UIAlertViewDelegate>

@end

@implementation UserOrderStatusViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initData];
    [self initUserInterface];
}

- (void)initData {
    [self requestOrderInfo];
}

- (void)initUserInterface {
    self.navigationController.navigationBarHidden = YES;
    [self.view addSubview:self.rootView];
    
    [_rootView.userInfoView.portraitImageView sd_setImageWithURL:[NSURL URLWithString:_bigShotInfo.headImageUrl]];
    _rootView.userInfoView.nameLabel.text = _bigShotInfo.nickName;
    
    if (_bigShotInfo.sex == 0) {
        _rootView.userInfoView.sexImageView.image = [UIImage imageNamed:@"sex0"];
    } else {
        _rootView.userInfoView.sexImageView.image = [UIImage imageNamed:@"sex1"];
    }
    
    _rootView.userInfoView.horoscopeLabel.text = _bigShotInfo.constellation;
    _rootView.userInfoView.signatureLabel.text = _bigShotInfo.signature.length == 0 ? @"这个家伙很懒，什么都没有留下" : _bigShotInfo.signature;
    
    [_rootView.orderAcceptView.button addTarget:self action:@selector(cancelOrderButtonClicked) forControlEvents:UIControlEventTouchUpInside];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (UserOrderStatusRootView *)rootView {
    if (!_rootView) {
        _rootView = [[UserOrderStatusRootView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
        [_rootView.navigationBarView.leftButton addTarget:self action:@selector(returnButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        [_rootView.userInfoView.button addTarget:self action:@selector(bigShotInfoButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    }
    return _rootView;
}

- (void)returnButtonClicked {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)bigShotInfoButtonClicked {
    NSLog(@"bigShotInfoButton Clicked");
    PeopleGameDetailViewController * vc = [[PeopleGameDetailViewController alloc] initWithDakaInfo:_bigShotInfo];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)requestOrderInfo {
    [SVProgressHUD showWithStatus:@"正在努力获取订单信息..." maskType:2];
    NSDictionary *parameters = @{@"action":@"getOrderById",
                                 @"sessionKey":[TDSingleton instance].sessionKey,
                                 @"orderId":_orderID};
    
    NSLog(@"requestOrderInfo parameters:%@", parameters);
    
    [TDNetworking requestWithURL:[TDSingleton instance].gatewayServerBusinessURL
                      parameters:parameters
                         success:^(id responseObject)
     {
         NSLog(@"submitOrderEvaluation result:%@", responseObject);
         if ([responseObject[@"result"] integerValue] == 1) {
             _rootView.acceptOrderDateView.dateLabel.text = [TDDateConversion dateFromNumber:[responseObject[@"order"][@"createTime"] longValue] / 1000
                                                                       dateFormat:@"MM月dd日"];
             _rootView.orderAcceptView.titleLabel.text = @"待确认";
             _rootView.orderAcceptView.detailLabel.text = [NSString stringWithFormat:@"请等待 %@ 联系你", responseObject[@"order"][@"receiverNickName"]];
             
             
             
             _rootView.orderAcceptView.timeLabel.text = [TDDateConversion dateFromNumber:[responseObject[@"order"][@"createTime"] longValue] / 1000
                                                                            dateFormat:@"HH:mm"];
             
             _rootView.waitToStartDateView.dateLabel.text = [TDDateConversion dateFromNumber:[responseObject[@"order"][@"planStartTime"] longValue] / 1000
         dateFormat:@"MM月dd日"];
             _rootView.orderWaitToStartView.titleLabel.text = @"等待开始";
             
             long planStartTime = [responseObject[@"order"][@"planStartTime"] longValue];
             long nowTimestamp = [[NSDate date] timeIntervalSince1970] * 1000;
             
             int hours = (int)((planStartTime - nowTimestamp) / 1000) / 3600;
             int minutes = (((planStartTime - nowTimestamp) / 1000) % 3600) / 60;
             
             if (hours == 0) {
                  _rootView.orderWaitToStartView.detailLabel.text = [NSString stringWithFormat:@"距离开始时间还有%d分钟", minutes];
             } else {
                  _rootView.orderWaitToStartView.detailLabel.text = [NSString stringWithFormat:@"距离开始时间还有%d小时%d分钟", hours, minutes];
             }
            
             
             [SVProgressHUD dismiss];
         } else {
             [SVProgressHUD dismissWithError:[NSString stringWithFormat:@"%@", responseObject[@"msg"]]];
         }
     } failure:^(NSError *error) {
         [SVProgressHUD dismissWithError:[NSString stringWithFormat:@"%@", error]];
         NSLog(@"requestUserInfoWithIdentifier error:%@", error);
     }];
}

- (void)cancelOrderButtonClicked {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"确定要取消该订单吗？" message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    [alertView show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {

        [TDNetworkRequest cancelOrderWithOrderID:_orderID timeout:@"0"
                                         success:^(NSString *message)
        {
            [SVProgressHUD dismissWithSuccess:@"取消订单成功！"];
        } failure:^(NSString *error) {
            [SVProgressHUD dismissWithSuccess:error];
        }];
    }
}

@end
