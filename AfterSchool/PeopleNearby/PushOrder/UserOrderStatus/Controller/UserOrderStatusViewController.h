//
//  UserOrderStatusViewController.h
//  AfterSchool
//
//  Created by Teson Draw on 11/10/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserOrderStatusRootView.h"
#import "LGDakaInfo.h"

@interface UserOrderStatusViewController : UIViewController

@property (nonatomic, strong) UserOrderStatusRootView *rootView;
@property (nonatomic, strong) LGDaKaInfo *bigShotInfo;
@property (nonatomic, strong) NSString *orderID;

@end
