//
//  OrderTimeoutViewController.m
//  AfterSchool
//
//  Created by Teson Draw on 11/30/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "OrderTimeoutViewController.h"
#import "MacrosDefinition.h"

@implementation OrderTimeoutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initUserInterface];
}

- (void)initUserInterface {
    [self.view addSubview:self.rootView];
}

- (OrderTimeoutRootView *)rootView {
    if (!_rootView) {
        _rootView = [[OrderTimeoutRootView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
        
        [_rootView.navigationBarView.leftButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        [_rootView.adjustLocationButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [_rootView.changeConditionButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [_rootView.changeTimeButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [_rootView.tryAgainButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return _rootView;
}

- (void)buttonClicked:(UIButton *)sender {
    switch (sender.tag) {
        case 100:
            [self.navigationController popToRootViewControllerAnimated:YES];
            break;
            
        case 101:
            [self.navigationController popToViewController:self.navigationController.viewControllers[1] animated:YES];
            break;
            
        case 102:
            [self.navigationController popToViewController:self.navigationController.viewControllers[2] animated:YES];
            break;
            
        case 103:
            [self.navigationController popViewControllerAnimated:YES];
            break;
            
        case 104:
            [self.navigationController popToRootViewControllerAnimated:YES];
            break;
    }
}

@end
