//
//  OrderTimeoutViewController.h
//  AfterSchool
//
//  Created by Teson Draw on 11/30/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderTimeoutRootView.h"

@interface OrderTimeoutViewController : UIViewController

@property (nonatomic, strong) OrderTimeoutRootView *rootView;

@end
