//
//  OrderTimeoutRootView.m
//  AfterSchool
//
//  Created by Teson Draw on 11/30/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "OrderTimeoutRootView.h"
#import "MacrosDefinition.h"
#import "UIButton+SetBackgroundColor.h"

@implementation OrderTimeoutRootView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        
        [self addSubview:self.navigationBarView];
        [self addSubview:self.tipBackgroundView];
        [_tipBackgroundView addSubview:self.tipLabel];
        
        [self addSubview:self.invertedTriangleImageView];
        
        [self addSubview:self.youCanLabel];
        [self addSubview:self.adjustLocationImageView];
        [_adjustLocationImageView addSubview:self.adjustLocationButton];
        
        [self addSubview:self.changeConditionImageView];
        [_changeConditionImageView addSubview:self.changeConditionButton];
        
        [self addSubview:self.changeTimeImageView];
        [_changeTimeImageView addSubview:self.changeTimeButton];
        
        [self addSubview:self.adjustLocationLabel];
        [self addSubview:self.changeConditionLabel];
        [self addSubview:self.changeTimeLabel];
        [self addSubview:self.separatorLineView];
        [self addSubview:self.orLabel];
        [self addSubview:self.tryAgainButton];
    }
    return self;
}

- (NavigationBarView *)navigationBarView {
    if (!_navigationBarView) {
        _navigationBarView = [[NavigationBarView alloc] initWithTitle:@"超时"
                                                           titleColor:nil
                                                      backgroundColor:nil
                                              leftButtonBgViewOriginX:0
                                                 leftButtonImageWidth:22
                                                 leftButtonTitleColor:[UIColor whiteColor]
                                                       leftButtonName:@"取消"
                                                  leftButtonImageName:@"navigation_bar_return_button_left_arrow" rightButtonName:nil
                                                 rightButtonImageName:nil];

        _navigationBarView.leftButton.tag = 104;
    }
    return _navigationBarView;
}

- (UIView *)tipBackgroundView {
    if (!_tipBackgroundView) {
        _tipBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 64, SCREEN_WIDTH, 92)];
        _tipBackgroundView.backgroundColor = [UIColor colorWithRed:70/255.f green:70/255.f blue:70/255.f alpha:1];
    }
    return _tipBackgroundView;
}

- (UILabel *)tipLabel {
    if (!_tipLabel) {
        _tipLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 92)];
        _tipLabel.text = @"没有找到约玩对象？";
        _tipLabel.textColor = [UIColor whiteColor];
        _tipLabel.font = [UIFont systemFontOfSize:22];
        _tipLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _tipLabel;
}

- (UILabel *)youCanLabel {
    if (!_youCanLabel) {
        _youCanLabel = [[UILabel alloc] initWithFrame:CGRectMake(28, CGRectGetMaxY(_tipBackgroundView.frame) + 38, 80, 16)];
        _youCanLabel.textColor = [UIColor colorWithRed:120/255.f green:120/255.f blue:120/255.f alpha:1];
        _youCanLabel.font = [UIFont systemFontOfSize:16];
    }
    return _youCanLabel;
}

- (UIImageView *)invertedTriangleImageView {
    if (!_invertedTriangleImageView) {
        _invertedTriangleImageView = [[UIImageView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - 100, CGRectGetMaxY(_tipBackgroundView.frame), 14, 10)];
        _invertedTriangleImageView.image = [UIImage imageNamed:@"separator_darkgray_inverted_triangle"];
    }
    return _invertedTriangleImageView;
}

- (UIImageView *)adjustLocationImageView {
    if (!_adjustLocationImageView) {
        _adjustLocationImageView = [[UIImageView alloc] initWithFrame:CGRectMake((SCREEN_WIDTH - 3 * 68 - 2 * 36) / 2, CGRectGetMaxY(_youCanLabel.frame) + 32, 68, 68)];
        _adjustLocationImageView.layer.cornerRadius = 34;
        _adjustLocationImageView.layer.masksToBounds = YES;
        _adjustLocationImageView.image = [UIImage imageNamed:@"order_timeout_adjust_location"];
        _adjustLocationImageView.userInteractionEnabled = YES;
    }
    return _adjustLocationImageView;
}

- (UIButton *)adjustLocationButton {
    if (!_adjustLocationButton) {
        _adjustLocationButton = [[UIButton alloc] initWithFrame:_adjustLocationImageView.bounds];
        _adjustLocationButton.tag = 100;
    }
    
    return _adjustLocationButton;
}

- (UIImageView *)changeConditionImageView {
    if (!_changeConditionImageView) {
        _changeConditionImageView = [[UIImageView alloc] initWithFrame:CGRectMake((SCREEN_WIDTH - 68) / 2, CGRectGetMaxY(_youCanLabel.frame) + 32, 68, 68)];
        _changeConditionImageView.layer.cornerRadius = 34;
        _changeConditionImageView.layer.masksToBounds = YES;
        _changeConditionImageView.image = [UIImage imageNamed:@"order_timeout_change_condition"];
        _changeConditionImageView.userInteractionEnabled = YES;
    }
    return _changeConditionImageView;
}

- (UIButton *)changeConditionButton {
    if (!_changeConditionButton) {
        _changeConditionButton = [[UIButton alloc] initWithFrame:_changeConditionImageView.bounds];
        _changeConditionButton.tag = 101;
    }
    
    return _changeConditionButton;
}

- (UIImageView *)changeTimeImageView {
    if (!_changeTimeImageView) {
        _changeTimeImageView = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_changeConditionImageView.frame) + 36, CGRectGetMaxY(_youCanLabel.frame) + 32, 68, 68)];
        _changeTimeImageView.layer.cornerRadius = 34;
        _changeTimeImageView.layer.masksToBounds = YES;
        _changeTimeImageView.image = [UIImage imageNamed:@"order_timeout_change_time"];
        _changeTimeImageView.userInteractionEnabled = YES;
    }
    return _changeTimeImageView;
}

- (UIButton *)changeTimeButton {
    if (!_changeTimeButton) {
        _changeTimeButton = [[UIButton alloc] initWithFrame:_changeTimeImageView.bounds];
        _changeTimeButton.tag = 102;
    }
    
    return _changeTimeButton;
}

- (UILabel *)adjustLocationLabel {
    if (!_adjustLocationLabel) {
        _adjustLocationLabel = [[UILabel alloc] initWithFrame:CGRectMake(_adjustLocationImageView.frame.origin.x, CGRectGetMaxY(_adjustLocationImageView.frame) + 16, 68, 16)];
        _adjustLocationLabel.textColor = [UIColor blackColor];
        _adjustLocationLabel.font = [UIFont systemFontOfSize:16];
        _adjustLocationLabel.text = @"调整游标";
        _adjustLocationLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _adjustLocationLabel;
}

- (UILabel *)changeConditionLabel {
    if (!_changeConditionLabel) {
        _changeConditionLabel = [[UILabel alloc] initWithFrame:CGRectMake(_changeConditionImageView.frame.origin.x, CGRectGetMaxY(_changeConditionImageView.frame) + 16, 68, 16)];
        _changeConditionLabel.textColor = [UIColor blackColor];
        _changeConditionLabel.font = [UIFont systemFontOfSize:16];
        _changeConditionLabel.text = @"切换情景";
        _changeConditionLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _changeConditionLabel;
}

- (UILabel *)changeTimeLabel {
    if (!_changeTimeLabel) {
        _changeTimeLabel = [[UILabel alloc] initWithFrame:CGRectMake(_changeTimeImageView.frame.origin.x, CGRectGetMaxY(_changeTimeImageView.frame) + 16, 68, 16)];
        _changeTimeLabel.textColor = [UIColor blackColor];
        _changeTimeLabel.font = [UIFont systemFontOfSize:16];
        _changeTimeLabel.text = @"修改时间";
        _changeTimeLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _changeTimeLabel;
}

- (UIView *)separatorLineView {
    if (!_separatorLineView) {
        _separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(14, CGRectGetMaxY(_adjustLocationLabel.frame) + 36, SCREEN_WIDTH - 28, 1)];
        _separatorLineView.backgroundColor = [UIColor colorWithRed:226/255.f green:226/255.f blue:226/255.f alpha:1];
    }
    return _separatorLineView;
}

- (UILabel *)orLabel {
    if (!_orLabel) {
        _orLabel = [[UILabel alloc] initWithFrame:CGRectMake(64, CGRectGetMaxY(_separatorLineView.frame) + 32, 20, 16)];
        _orLabel.textColor = [UIColor colorWithRed:120/255.f green:120/255.f blue:120/255.f alpha:1];
        _orLabel.font = [UIFont systemFontOfSize:16];
    }
    return _orLabel;
}

- (UIButton *)tryAgainButton {
    if (!_tryAgainButton) {
        _tryAgainButton = [[UIButton alloc] initWithFrame:CGRectMake(12, CGRectGetMaxY(_orLabel.frame) + 32, SCREEN_WIDTH - 24, 46)];
        [_tryAgainButton setTitle:@"重试一次" forState:UIControlStateNormal];
        [_tryAgainButton setBackgroundColor:[UIColor colorWithRed:41/255.f green:182/255.f blue:246/255.0 alpha:1]];
        [_tryAgainButton setBackgroundColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
        _tryAgainButton.layer.cornerRadius = 5;
        _tryAgainButton.layer.masksToBounds = YES;
        _tryAgainButton.titleLabel.font = [UIFont systemFontOfSize:18];
        _tryAgainButton.tag = 103;
    }
    
    return _tryAgainButton;
}

@end

