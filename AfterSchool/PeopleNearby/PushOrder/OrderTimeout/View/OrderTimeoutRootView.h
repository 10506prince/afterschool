//
//  OrderTimeoutRootView.h
//  AfterSchool
//
//  Created by Teson Draw on 11/30/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavigationBarView.h"

@interface OrderTimeoutRootView : UIView

@property (nonatomic, strong) NavigationBarView * navigationBarView;

@property (nonatomic, strong) UIView *tipBackgroundView;
@property (nonatomic, strong) UIView *separatorLineView;

@property (nonatomic, strong) UILabel *tipLabel;
@property (nonatomic, strong) UILabel *youCanLabel;
@property (nonatomic, strong) UILabel *orLabel;
@property (nonatomic, strong) UILabel *adjustLocationLabel;
@property (nonatomic, strong) UILabel *changeConditionLabel;
@property (nonatomic, strong) UILabel *changeTimeLabel;

@property (nonatomic, strong) UIImageView *invertedTriangleImageView;
@property (nonatomic, strong) UIImageView *adjustLocationImageView;
@property (nonatomic, strong) UIImageView *changeConditionImageView;
@property (nonatomic, strong) UIImageView *changeTimeImageView;

@property (nonatomic, strong) UIButton *adjustLocationButton;
@property (nonatomic, strong) UIButton *changeConditionButton;
@property (nonatomic, strong) UIButton *changeTimeButton;

@property (nonatomic, strong) UIButton *tryAgainButton;

@end
