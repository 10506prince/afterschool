//
//  DaKaInfoTableViewCell.m
//  AfterSchool
//
//  Created by lg on 15/10/8.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "DaKaInfoTableViewCell.h"
#import "Common.h"
#import "NSString+Null.h"
#import <UIImageView+WebCache.h>

#define DAKAINFO_FONTSIZE (14.0f)
#define DAKAINFO_TEXTCOLOR

@interface DaKaInfoTableViewCell ()

@property (nonatomic, strong) IBOutlet UIImageView * headImage;///<头像
@property (nonatomic, strong) IBOutlet UILabel * name;///<用户名
@property (nonatomic, strong) IBOutlet UIImageView * creditRating;///<信用等级
@property (nonatomic, strong) IBOutlet UILabel * distance;///<距离
//@property (nonatomic, strong) IBOutlet UILabel * sexAndAge;///<性别和年龄
@property (nonatomic, weak) IBOutlet UIImageView * sex;///<性别 （0-女 1-男）
@property (nonatomic, weak) IBOutlet UILabel * age;///<年龄
@property (nonatomic, strong) IBOutlet UILabel * constellation;///<星座
@property (nonatomic, strong) IBOutlet UILabel * signature;///<个性签名


@property (nonatomic, strong) IBOutlet UILabel * gameRank;///<游戏段位
@property (nonatomic, strong) IBOutlet UILabel * gameServer;///<游戏大区
@property (nonatomic, strong) IBOutlet UILabel * gameNumber;///<游戏局数
@property (nonatomic, strong) IBOutlet UILabel * gameFighting;///<游戏中战斗力


@property (nonatomic, weak) IBOutlet UIView * topView;
@property (nonatomic, weak) IBOutlet UIView * bottomView;

//@property (nonatomic, weak) IBOutlet UIButton * yueBtn;///<约玩按键
@end

@implementation DaKaInfoTableViewCell

- (void)awakeFromNib {

    UIView * line1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, LgScreenWidth, SINGLE_LINE_WIDTH)];
    [line1 setBackgroundColor:[UIColor colorWithWhite:0.8 alpha:1]];
    //[line1 setBackgroundColor:[UIColor lightGrayColor]];
    [self.topView addSubview:line1];
    [self.topView sendSubviewToBack:line1];
    
    
    UIView * line = [[UIView alloc] initWithFrame:CGRectMake(0, _bottomView.frame.origin.y + _bottomView.frame.size.height - SINGLE_LINE_ADJUST_OFFSET, LgScreenWidth, SINGLE_LINE_WIDTH)];
    [line setBackgroundColor:[UIColor colorWithWhite:0.8 alpha:1]];
    [line.layer setShadowColor:[UIColor blackColor].CGColor];
    [line.layer setShadowOffset:CGSizeMake(0, 1)];
    [line.layer setShadowOpacity:0.5];
    [self addSubview:line];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setDakaInfo:(LGDaKaInfo *)dakaInfo {
    _dakaInfo = dakaInfo;
    
    //头像
    if ([_dakaInfo.headImage isNull]) {
        [_headImage setImage:[UIImage imageNamed:@"head1"]];
    }else {
        [_headImage setImage:[UIImage imageNamed:_dakaInfo.headImage]];
    }
    
    //昵称
    if (_dakaInfo.name) {
        [_name setText:_dakaInfo.name];
    } else {[_name setText:nil];}
    
    //信用等级
    NSString * imageName = [NSString stringWithFormat:@"creditRating%ld", _dakaInfo.creditRating];
    [_creditRating setImage:[UIImage imageNamed:imageName]];
    
    [_distance setText:[NSString stringWithFormat:@"%.2fkm", _dakaInfo.distance]];
    
    //性别
    UIImage * image = [UIImage imageNamed:[NSString stringWithFormat:@"sex%ld", _dakaInfo.sex]];
    
    [_sex setImage:[image resizableImageWithCapInsets:UIEdgeInsetsMake(image.size.height/2, image.size.height/2 +1, image.size.width/2, image.size.width/2+1)]];
    
    //年龄
    [_age setText:[NSString stringWithFormat:@"%ld", _dakaInfo.age]];
    
    //星座
    if (_dakaInfo.constellation) {
        [_constellation setText:_dakaInfo.constellation];
    } else {
        [_constellation setText:nil];
    }
    
    //个性签名
    if ([_dakaInfo.signature isNull]) {
        [_signature setText:@"这个人很懒，什么都没有留下。。。"];
    } else {
        [_signature setText:_dakaInfo.signature];
    }
    
    //段位
    if (_dakaInfo.gameRank) {
        [_gameRank setText:_dakaInfo.gameRank];
    } else {
        [_gameRank setText:nil];
    }
    
    //服务器
    if (_dakaInfo.gameServer) {
        [_gameServer setText:[NSString stringWithFormat:@"%@，", _dakaInfo.gameServer]];
    } else {
        [_gameServer setText:nil];
    }
    
    //游戏局数
    [_gameNumber setText:[NSString stringWithFormat:@"%ld局", _dakaInfo.gameNumber]];
    
    [_gameFighting setText:[NSString stringWithFormat:@"%ld", _dakaInfo.gameFighting]];
    
}

@end
