//
//  LGFuBiaoAnnotationView.m
//  AfterSchool
//
//  Created by lg on 15/10/10.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGFuBiaoAnnotationView.h"

#define FuBiaoAnnotationViewDefaultWidth (44)
#define FuBiaoAnnotationViewDefaultHeight (44)

@implementation LGFuBiaoAnnotationView
- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setUp];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self setUp];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUp];
    }
    return self;
}

- (id)initWithAnnotation:(id<BMKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setUp];
    }
    return self;
}

- (void)setUp {
    self.image = nil;
    self.canShowCallout = NO;
    [self setBackgroundColor:[UIColor clearColor]];
    
    [self setBounds:CGRectMake(0, 0, FuBiaoAnnotationViewDefaultWidth, FuBiaoAnnotationViewDefaultHeight)];
    
//    UIView * maskView = [[UIView alloc] initWithFrame:self.bounds];
//    [maskView setBackgroundColor:[UIColor whiteColor]];
//    [maskView.layer setCornerRadius:AnnotationViewDefaultWidth/2];
//    [maskView.layer setBorderWidth:3];
//    [maskView.layer setBorderColor:[UIColor blackColor].CGColor];
//    
//    self.maskView = maskView;
    
//    UIImageView * imageView = [[UIImageView alloc] initWithFrame:self.bounds];
//    [imageView setBackgroundColor:[UIColor redColor]];
//    [self addSubview:imageView];
//    self.headImage = imageView;
    
}

- (void)setAnnotation:(id<BMKAnnotation>)annotation {
//    if (_headImage) {
//        NSURL * headImageUrl = [NSURL URLWithString:((LGPointAnnotation *)annotation).headImageUrl];
//        [self.headImage sd_setImageWithURL:headImageUrl placeholderImage:[UIImage imageNamed:@"location"]];
//        //        [self.headImage setImage:[UIImage imageNamed:@"location"]];
//    }
}

@end
