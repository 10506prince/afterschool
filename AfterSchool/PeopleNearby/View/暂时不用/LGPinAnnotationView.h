//
//  LGPinAnnotationView.h
//  baiduMap
//
//  Created by lg on 15/9/28.
//  Copyright © 2015年 李刚. All rights reserved.
//

#import <BaiduMapAPI_Map/BMKAnnotationView.h>
#import "LGPointAnnotation.h"
#import "LGPaopaoView.h"

//@class LGPointAnnotation;
//@class LGPaopaoView;

typedef enum {
    LGAnnotationViewAnimationTypeGrow,      ///< 伸展
    LGAnnotationViewAnimationTypeShrink,    ///< 收缩
} LGAnnotationViewAnimationType;            ///< 标注视图动画类型

typedef enum {
    LGAnnotationViewStateCollapsed,         ///< 收缩的
    LGAnnotationViewStateExpanded,          ///< 展开的
    LGAnnotationViewStateAnimating,         ///< 动画中
} LGAnnotationViewState;                    ///< 标注当前状态

@protocol LGAnnotationViewProtocol <NSObject>

- (void)didSelectAnnotationViewInMap:(BMKMapView *)mapView;
- (void)didDeselectAnnotationViewInMap:(BMKMapView *)mapView;

@end

@interface LGPinAnnotationView : BMKAnnotationView <LGAnnotationViewProtocol>

///自定义点击后显示泡泡
@property (nonatomic, strong) LGPaopaoView * myPaopaoView;

@end
