//
//  LGBuoyView.h
//  AfterSchool
//
//  Created by lg on 15/10/13.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

#define LGBUOY_DEFAULT_WIDTH (166.0f)
#define LGBUOY_DEFAULT_HEIGHT (48.0f)
#define LGBUOY_EXPAND_HEIGHT (18.0f)

#define LGBUOY_SHADOW_SHOW (NO)

@interface LGBuoyView : UIButton {
    CAShapeLayer *_shapeLayer;///<填充色挡板
    CAGradientLayer *_bubbleGradient;///<填充色
    CAShapeLayer *_strokeAndShadowLayer;///<边界和阴影
    
    UITapGestureRecognizer * _tapGestureRecognizer;
}

@property (nonatomic, copy) IBOutlet UILabel * label1;
@property (nonatomic, copy) IBOutlet UILabel * label2;
@property (nonatomic, copy) IBOutlet UIImageView * imageView1;


@property (nonatomic, assign, getter=isEnabled) BOOL enable;
@property (nonatomic, assign) NSUInteger userNumber;

+ (instancetype)sharedBuoy;

@end
