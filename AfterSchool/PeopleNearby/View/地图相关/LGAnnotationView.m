//
//  LGAnnotationView.m
//  AfterSchool
//
//  Created by lg on 15/10/7.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGAnnotationView.h"
#import "LGPointAnnotation.h"
#import "UIImage+View.h"
#import "NSString+Null.h"
#import "Common.h"
#import "UIImageView+WebCache.h"

#define AnnotationViewDefaultWidth (45)
#define AnnotationViewDefaultHeight (55)

@interface LGAnnotationView ()

@property (nonatomic, strong) UIImageView * headImage;
@property (nonatomic, strong) CAShapeLayer * backLayer;
@property (nonatomic, strong) CAShapeLayer * arrowLayer;

@end

@implementation LGAnnotationView

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setUp];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self setUp];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUp];
    }
    return self;
}

- (id)initWithAnnotation:(id<BMKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setUp];
    }
    return self;
}

- (void)setUp {
    self.image = nil;
    self.canShowCallout = NO;
    self.centerOffset = CGPointMake(0, -AnnotationViewDefaultHeight/2);
    
    [self setBackgroundColor:[UIColor clearColor]];
    
    [self setBounds:CGRectMake(0, 0, AnnotationViewDefaultWidth, AnnotationViewDefaultHeight)];
    
    self.backLayer = [self getLayer];
    self.arrowLayer = [self getLayer];
    
    [self.layer addSublayer:self.backLayer];
    [self.layer addSublayer:self.arrowLayer];
    
//    UIView * maskView = [[UIView alloc] initWithFrame:self.bounds];
//    [maskView setBackgroundColor:[UIColor whiteColor]];
//    [maskView.layer setCornerRadius:AnnotationViewDefaultWidth/2];
//    [maskView.layer setBorderWidth:3];
//    [maskView.layer setBorderColor:[UIColor blackColor].CGColor];
//    
//    self.maskView = maskView;
    
    UIImageView * imageView = [[UIImageView alloc] init];
    
    [imageView.layer setBorderWidth:1];
    [imageView.layer setBorderColor:[UIColor whiteColor].CGColor];
    [imageView setClipsToBounds:YES];
    
    [imageView setBackgroundColor:[UIColor redColor]];
    [self addSubview:imageView];
    self.headImage = imageView;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    _backLayer.path = [self getBackLayerPathWithRect:self.bounds];
    _arrowLayer.path = [self getArrowLayerPathWithRect:self.bounds];
    
    [self.headImage setFrame:CGRectMake(3, 3, self.bounds.size.width - 6, self.bounds.size.width - 6)];
    [self.headImage.layer setCornerRadius:self.headImage.bounds.size.width/2];
}

- (CAShapeLayer *)getLayer {
    CAShapeLayer * backLayer = [CAShapeLayer layer];
    [backLayer setFillColor:LgColorRGB(70, 70, 70, 1).CGColor];
    return backLayer;
}

- (CGPathRef)getBackLayerPathWithRect:(CGRect)rect {
    
    CGFloat radius = rect.size.width/2;
    
    UIBezierPath * backLayerPath = [UIBezierPath bezierPathWithArcCenter:CGPointMake(radius, radius) radius:radius startAngle:-M_PI endAngle:M_PI clockwise:YES];
    
    return backLayerPath.CGPath;
}

- (CGPathRef)getArrowLayerPathWithRect:(CGRect)rect {
    
    CGFloat radius = rect.size.width/2;
    
    CGFloat cosQ = radius/(rect.size.height - radius);
    CGFloat sinQ = sqrt(1-pow(cosQ, 2));
    
    CGFloat x = (1-sinQ)*radius;
    CGFloat y = (1+cosQ)*radius;
    
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathMoveToPoint(path, NULL, radius, rect.size.height);
    CGPathAddLineToPoint(path, NULL, x, y);
    CGPathAddLineToPoint(path, NULL, 2*radius - x, y);
    CGPathCloseSubpath(path);
    
    UIBezierPath * backLayerPath = [UIBezierPath bezierPathWithCGPath:path];
    CGPathRelease(path);
    
    return backLayerPath.CGPath;
}

- (void)setAnnotation:(id<BMKAnnotation>)annotation {
    [super setAnnotation:annotation];
    
    if (_headImage) {
        
        NSString * imageUrl = ((LGPointAnnotation *)annotation).dakaInfo.headImageUrl;
        if (imageUrl.length == 0) {
            [self.headImage setImage:[UIImage imageNamed:@"user_default_head"]];
        } else {
            NSURL * url = [NSURL URLWithString:imageUrl];
            [_headImage sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"user_default_head"]];
        }
    }
}
@end
