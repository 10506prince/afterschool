//
//  LGBuoyView.m
//  AfterSchool
//
//  Created by lg on 15/10/13.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGBuoyView.h"

@implementation LGBuoyView

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setUp];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self setUp];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUp];
    }
    return self;
}

+ (instancetype)sharedBuoy {
    static dispatch_once_t once;
    static LGBuoyView *sharedBuoy;
    dispatch_once(&once, ^ { sharedBuoy = [[LGBuoyView alloc] init]; });
    return sharedBuoy;
}

- (void)setUp {
    
    _label1 = [[UILabel alloc] initWithFrame:CGRectMake(0, 2, 100, 22)];
    [_label1 setText:@"立即约"];
    [_label1 setTextAlignment:NSTextAlignmentCenter];
    [_label1 setFont:[UIFont systemFontOfSize:18]];
    [_label1 setTextColor:[UIColor whiteColor]];
    
    _label2 = [[UILabel alloc] initWithFrame:CGRectMake(0, 24, 100, 24)];
    [_label2 setText:@"0 Players"];
    [_label2 setTextAlignment:NSTextAlignmentCenter];
    [_label2 setFont:[UIFont systemFontOfSize:14]];
    [_label2 setTextColor:[UIColor whiteColor]];
    
    _imageView1 = [[UIImageView alloc] initWithFrame:CGRectMake(LGBUOY_DEFAULT_WIDTH - 28 - 15, LGBUOY_DEFAULT_HEIGHT/2 - 14, 28, 28)];
    [_imageView1 setImage:[UIImage imageNamed:@"buoy_right"]];
 
    [self addSubview:_label1];
    [self addSubview:_label2];
    [self addSubview:_imageView1];
    
    [self setLayerProperties];
}

- (void)setLayerProperties {
    _strokeAndShadowLayer = [CAShapeLayer layer];
    _shapeLayer = [CAShapeLayer layer];
    
    CGPathRef _strokeAndShadowLayerPath = [self newBubbleWithRect:self.bounds andEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
    _strokeAndShadowLayer.path = _strokeAndShadowLayerPath;
    _shapeLayer.path = _strokeAndShadowLayerPath;
    CGPathRelease(_strokeAndShadowLayerPath);
    
    _strokeAndShadowLayer.fillColor = [UIColor clearColor].CGColor;

    //阴影
//    if (LGBUOY_SHADOW_SHOW) {
//        _strokeAndShadowLayer.shadowColor = [UIColor blackColor].CGColor;
//        _strokeAndShadowLayer.shadowOffset = CGSizeMake (0,0);
//        _strokeAndShadowLayer.shadowRadius = 5.0;
//        _strokeAndShadowLayer.shadowOpacity = 0.7;
//    }
    
    //边界
    _strokeAndShadowLayer.strokeColor = [UIColor colorWithWhite:0.22 alpha:1.0].CGColor;
    _strokeAndShadowLayer.lineWidth = 1.0;
    
    
    //填充色
    _bubbleGradient = [CAGradientLayer layer];
    CGRect frame = self.bounds;
    frame.size.height += LGBUOY_EXPAND_HEIGHT;
    _bubbleGradient.frame = frame;
    
    _bubbleGradient.colors = [NSArray arrayWithObjects:
                              (id)[UIColor colorWithWhite:0 alpha:.75].CGColor,
                              (id)[UIColor colorWithWhite:0 alpha:.75].CGColor,
                              (id)[UIColor colorWithWhite:0.13 alpha:.75].CGColor,
                              (id)[UIColor colorWithWhite:0.33 alpha:.75].CGColor,
                              nil];
    
    _bubbleGradient.locations = [NSArray arrayWithObjects:
                                 [NSNumber numberWithFloat:0],
                                 [NSNumber numberWithFloat:0.6],
                                 [NSNumber numberWithFloat:.61],
                                 [NSNumber numberWithFloat:1],
                                 nil];
    
    _bubbleGradient.startPoint = CGPointMake(0.0f, 1.0f);
    _bubbleGradient.endPoint = CGPointMake(0.0f, 0.0f);
    _bubbleGradient.mask = _shapeLayer;
    
    _shapeLayer.masksToBounds = NO;//填充色挡板图册
    _bubbleGradient.masksToBounds = NO;//填充色图层
    _strokeAndShadowLayer.masksToBounds = NO;//边界和阴影图层
    
    [_strokeAndShadowLayer addSublayer:_bubbleGradient];
    [self.layer insertSublayer:_strokeAndShadowLayer atIndex:0];
}

///获取显示框绘图路径
- (CGPathRef)newBubbleWithRect:(CGRect)rect {
    CGFloat stroke = 1.0;//边线宽度
    CGFloat radius = 7.0;//弧角半径
    CGMutablePathRef path = CGPathCreateMutable();
    
    //Determine Size 显示区域
    //    rect.size.width -= stroke + 14;//左右留边各 7.5
    //    rect.size.height -= stroke + 29;//上下留边各 15
    //    rect.origin.x += stroke / 2.0 + 7;//起始点x 7.5 + origin.x
    //    rect.origin.y += stroke / 2.0 + 7;//起始点y 7.5 + origin.y
    
    rect.size.width -= stroke;//左右留边各 7.5
    rect.size.height -= stroke;//上下留边各 15
    rect.origin.x += stroke / 2.0;//起始点x 7.5 + origin.x
    rect.origin.y += stroke / 2.0;//起始点y 7.5 + origin.y
    
    //Create Path For Callout Bubble
    CGPathMoveToPoint(path, NULL, rect.origin.x, rect.origin.y + radius);//起始点
    CGPathAddLineToPoint(path, NULL, rect.origin.x, rect.origin.y + rect.size.height - radius);//线
    CGPathAddArc(path, NULL, rect.origin.x + radius, rect.origin.y + rect.size.height - radius, radius, M_PI, M_PI_2, 1);//弧线 x,y为圆心
//    CGPathAddLineToPoint(path, NULL, rect.origin.x + rect.size.width - radius, rect.origin.y + rect.size.height);//线
    
    CGPathAddLineToPoint(path, NULL, rect.size.width/2 + rect.origin.x - LGBUOY_EXPAND_HEIGHT, rect.origin.y + rect.size.height);
    CGPathAddLineToPoint(path, NULL, rect.size.width/2 + rect.origin.x, rect.origin.y + rect.size.height + LGBUOY_EXPAND_HEIGHT);
    CGPathAddLineToPoint(path, NULL, rect.size.width/2 + rect.origin.x + LGBUOY_EXPAND_HEIGHT, rect.origin.y + rect.size.height);
    CGPathAddLineToPoint(path, NULL, rect.origin.x + rect.size.width - radius, rect.origin.y + rect.size.height);
    
    
    CGPathAddArc(path, NULL, rect.origin.x + rect.size.width - radius, rect.origin.y + rect.size.height - radius, radius, M_PI_2, 0.0f, 1);//弧线
    CGPathAddLineToPoint(path, NULL, rect.origin.x + rect.size.width, rect.origin.y + radius);
    CGPathAddArc(path, NULL, rect.origin.x + rect.size.width - radius, rect.origin.y + radius, radius, 0.0f, -M_PI_2, 1);
    CGPathAddLineToPoint(path, NULL, rect.origin.x + radius, rect.origin.y);
    CGPathAddArc(path, NULL, rect.origin.x + radius, rect.origin.y + radius, radius, -M_PI_2, M_PI, 1);
    CGPathCloseSubpath(path);
    
    return path;
}

- (CGPathRef)newBubbleWithRect:(CGRect)rect andOffset:(CGSize)offset {
    CGRect offsetRect = CGRectMake(rect.origin.x+offset.width, rect.origin.y+offset.height, rect.size.width, rect.size.height);
    return [self newBubbleWithRect:offsetRect];
}

- (CGPathRef)newBubbleWithRect:(CGRect)rect andEdgeInsets:(UIEdgeInsets)offset {
    CGRect offsetRect = CGRectMake(rect.origin.x+ offset.left, rect.origin.y+offset.top, rect.size.width - offset.left - offset.right, rect.size.height - offset.top - offset.bottom);
    return [self newBubbleWithRect:offsetRect];
}

#pragma mark - method

- (void)setEnable:(BOOL)enable {
    _enable = enable;
    if (self.enabled) {
        self.alpha = 1.0;
    } else {
        self.alpha = 0.3;
    }
}

- (void)setUserNumber:(NSUInteger)userNumber {
    _userNumber = userNumber;
    [_label2 setText:[NSString stringWithFormat:@"%ld Players", (unsigned long)_userNumber]];
}

@end
