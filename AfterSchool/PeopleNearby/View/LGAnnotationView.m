//
//  LGAnnotationView.m
//  AfterSchool
//
//  Created by lg on 15/10/7.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGAnnotationView.h"
#import "LGPointAnnotation.h"
#import "UIImage+View.h"
#import "NSString+Null.h"
#import "Common.h"
#import <SDWebImage/UIImageView+WebCache.h>

#define AnnotationViewDefaultWidth (40)
#define AnnotationViewDefaultHeight (40)

@interface LGAnnotationView ()

@property (nonatomic, strong) UIImageView * headImage;

@end

@implementation LGAnnotationView

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setUp];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self setUp];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUp];
    }
    return self;
}

- (id)initWithAnnotation:(id<BMKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setUp];
    }
    return self;
}

- (void)setUp {
    self.image = nil;
    self.canShowCallout = NO;
    [self setBackgroundColor:[UIColor clearColor]];
    
    [self setBounds:CGRectMake(0, 0, AnnotationViewDefaultWidth, AnnotationViewDefaultHeight)];
    
//    UIView * maskView = [[UIView alloc] initWithFrame:self.bounds];
//    [maskView setBackgroundColor:[UIColor whiteColor]];
//    [maskView.layer setCornerRadius:AnnotationViewDefaultWidth/2];
//    [maskView.layer setBorderWidth:3];
//    [maskView.layer setBorderColor:[UIColor blackColor].CGColor];
//    
//    self.maskView = maskView;
    
    UIImageView * imageView = [[UIImageView alloc] initWithFrame:self.bounds];
    [imageView.layer setCornerRadius:AnnotationViewDefaultWidth/2];
    [imageView.layer setBorderWidth:SINGLE_LINE_WIDTH];
    [imageView.layer setBorderColor:[UIColor blackColor].CGColor];
    [imageView setClipsToBounds:YES];
    
    [imageView setBackgroundColor:[UIColor redColor]];
    [self addSubview:imageView];
    self.headImage = imageView;
    
}

- (void)setAnnotation:(id<BMKAnnotation>)annotation {
    
    if (_headImage) {
//        NSURL * headImageUrl = [NSURL URLWithString:((LGPointAnnotation *)annotation).dakaInfo.headImageUrl];
//        [self.headImage sd_setImageWithURL:headImageUrl placeholderImage:[UIImage imageNamed:@"location"]];
        

        NSString * imageName = ((LGPointAnnotation *)annotation).dakaInfo.headImage;
        if ([imageName isNull]) {
            [self.headImage setImage:[UIImage imageNamed:@"head3"]];
        } else {
            [self.headImage setImage:[UIImage imageNamed:imageName]];
        }
    }
}
@end
