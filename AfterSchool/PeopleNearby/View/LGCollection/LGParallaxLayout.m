//
//  LGParallaxLayout.m
//  AfterSchool
//
//  Created by lg on 15/10/12.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGParallaxLayout.h"

@implementation LGParallaxLayout
- (id)init{
    
    if (self=[super init]) {
        
        self.minimumLineSpacing=0;
        self.minimumInteritemSpacing=0;
        self.scrollDirection=UICollectionViewScrollDirectionHorizontal;
    }
    
    return self;
}

- (CGSize)itemSize{
    return self.collectionView.frame.size;
}


- (BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds{
    return YES;
}
@end
