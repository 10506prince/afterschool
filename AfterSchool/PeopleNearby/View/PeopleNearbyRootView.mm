//
//  PeopleNearbyRootView.m
//  AfterSchool
//
//  Created by Teson Draw on 10/6/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "PeopleNearbyRootView.h"
//#import "UIView+UINib.h"
//#import "DaKaInfoTableViewCell.h"
#import "DakaInfoCollectionViewCell.h"
#import "UITableView+NibCell.h"
#import "LGParallaxLayout.h"
#import "Common.h"
#import "UIImage+ImageWithColor.h"
#import "MacrosDefinition.h"

@implementation PeopleNearbyRootView

- (void)dealloc
{
    NSLogSelfMethodName;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = LgColor(56, 1);
        
        [self addSubview:self.transformView];
        [self addSubview:self.navigationBarView];
    }
    return self;
}

- (NavigationBarView *)navigationBarView {
    if (!_navigationBarView) {
        
        _navigationBarView = [[NavigationBarView alloc] initWithTitle:@"约玩"
                                                           titleColor:nil
                                                      backgroundColor:APPLightColor
                                                  leftButtonImageName:@""
                                               leftButtonImageOriginX:10
                                                leftButtonImageHeight:37
                                                    leftButtonOriginX:0
                                                       leftButtonName:nil
                                                 leftButtonTitleColor:nil
                                                 rightButtonImageName:@"type_list"
                                              rightButtonImageOriginX:(self.bounds.size.width - 44 - 10)
                                               rightButtonImageHeight:44
                                                   rightButtonOriginX:(self.bounds.size.width - 44 - 10)
                                                      rightButtonName:nil
                                                rightButtonTitleColor:nil];
    }
    return _navigationBarView;
}

- (UIView *)transformView {
    if (_transformView == nil) {
        CGRect frame = self.bounds;
        //frame.origin.y = 64;
        //frame.size.height -= frame.origin.y;
        _transformView = [[UIView alloc] initWithFrame:frame];
//        [_transformView setBackgroundColor:[UIColor blackColor]];
    }
    
    return _transformView;
}


//- (LGBuoyView *)fubiaoView {
//    if (!_fubiaoView) {
//        _fubiaoView = [[LGBuoyView alloc] initWithFrame:CGRectMake((self.mapView.bounds.size.width - LGBUOY_DEFAULT_WIDTH)/2, MapCenterToTop - LGBUOY_DEFAULT_HEIGHT - LGBUOY_EXPAND_HEIGHT, LGBUOY_DEFAULT_WIDTH, LGBUOY_DEFAULT_HEIGHT)];
//    }
//    return _fubiaoView;
//}

@end
