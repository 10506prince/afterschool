//
//  PeopleNearbyRootView.h
//  AfterSchool
//
//  Created by Teson Draw on 10/6/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <BaiduMapAPI_Map/BMKMapComponent.h>
#import "NavigationBarView.h"
#import "LGBuoyView.h"
#import "PPiFlatSegmentedControl.h"
#import "NSString+FontAwesome.h"

#import "ListViewController.h"
#import "MapViewController.h"

@interface PeopleNearbyRootView : UIView

@property (nonatomic, strong) NavigationBarView * navigationBarView;

@property (nonatomic, strong) UIView * transformView;

@end



