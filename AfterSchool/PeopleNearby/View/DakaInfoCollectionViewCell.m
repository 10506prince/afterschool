//
//  DakaInfoCollectionViewCell.m
//  AfterSchool
//
//  Created by lg on 15/10/10.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "DakaInfoCollectionViewCell.h"
#import "Common.h"
#import "NSString+Null.h"

@interface DakaInfoCollectionViewCell ()

//头像
//名称
//信用等级
//距离
//性别
//年龄
//星座
//个性签名


//游戏段位
//游戏大区
//游戏局数
//战斗力

@property (nonatomic, weak) IBOutlet UIImageView * headImage;///<头像
@property (nonatomic, weak) IBOutlet UILabel * name;///<用户名
@property (nonatomic, weak) IBOutlet UIImageView * creditRating;///<信用等级
@property (nonatomic, weak) IBOutlet UILabel * distance;///<距离
@property (nonatomic, weak) IBOutlet UIImageView * sex;///<性别
@property (nonatomic, weak) IBOutlet UILabel * age;///<年龄
@property (nonatomic, weak) IBOutlet UILabel * constellation;///<星座
@property (nonatomic, weak) IBOutlet UILabel * signature;///<个性签名


@property (nonatomic, weak) IBOutlet UILabel * gameRank;///<游戏段位
@property (nonatomic, weak) IBOutlet UILabel * gameServer;///<游戏大区
@property (nonatomic, weak) IBOutlet UILabel * gameNumber;///<游戏局数
@property (nonatomic, weak) IBOutlet UILabel * gameFighting;///<游戏中战斗力


//@property (nonatomic, strong) IBOutlet UILabel * gameLevel;///<游戏中角色等级
//@property (nonatomic, strong) IBOutlet UILabel * gameNickName;///<游戏昵称



@end

@implementation DakaInfoCollectionViewCell

- (void)awakeFromNib {
    // Initialization code
    [_headImage.layer setCornerRadius:_headImage.bounds.size.width/2];
    [_headImage setClipsToBounds:YES];
    
    UIView * line = [[UIView alloc] initWithFrame:CGRectMake(64, 84 - SINGLE_LINE_ADJUST_OFFSET, LgScreenWidth - 64, SINGLE_LINE_WIDTH)];
    [line setBackgroundColor:[UIColor colorWithWhite:0.9 alpha:1]];
//    [line setBackgroundColor:[UIColor lightGrayColor]];
    [self addSubview:line];
}

- (void)setDakaInfo:(LGDaKaInfo *)dakaInfo {
    _dakaInfo = dakaInfo;
    
    //头像
    if ([_dakaInfo.headImage isNull]) {
        [_headImage setImage:[UIImage imageNamed:@"head1"]];
    }else {
        [_headImage setImage:[UIImage imageNamed:_dakaInfo.headImage]];
    }
    
    //昵称
    if (_dakaInfo.name) {
        [_name setText:_dakaInfo.name];
    } else {[_name setText:nil];}
    
    //信用等级
    NSString * imageName = [NSString stringWithFormat:@"creditRating%ld", _dakaInfo.creditRating];
    [_creditRating setImage:[UIImage imageNamed:imageName]];
    
    [_distance setText:[NSString stringWithFormat:@"%.2fkm", _dakaInfo.distance]];
    
    //性别
    UIImage * image = [UIImage imageNamed:[NSString stringWithFormat:@"sex%ld", _dakaInfo.sex]];

    [_sex setImage:[image resizableImageWithCapInsets:UIEdgeInsetsMake(image.size.height/2, image.size.height/2 +1, image.size.width/2, image.size.width/2+1)]];
    
    //年龄
    [_age setText:[NSString stringWithFormat:@"%ld", _dakaInfo.age]];
    
    //星座
    if (_dakaInfo.constellation) {
        [_constellation setText:_dakaInfo.constellation];
    } else {
        [_constellation setText:nil];
    }
    
    //个性签名
    if ([_dakaInfo.signature isNull]) {
        [_signature setText:@"这个人很懒，什么都没有留下。。。"];
    } else {
        [_signature setText:_dakaInfo.signature];
    }
    
    //段位
    if (_dakaInfo.gameRank) {
        [_gameRank setText:_dakaInfo.gameRank];
    } else {
        [_gameRank setText:nil];
    }
    
    //服务器
    if (_dakaInfo.gameServer) {
        [_gameServer setText:[NSString stringWithFormat:@"%@，", _dakaInfo.gameServer]];
    } else {
        [_gameServer setText:nil];
    }
    
    //游戏局数
    [_gameNumber setText:[NSString stringWithFormat:@"%ld局", _dakaInfo.gameNumber]];
    
    [_gameFighting setText:[NSString stringWithFormat:@"%ld", _dakaInfo.gameFighting]];

}
@end
