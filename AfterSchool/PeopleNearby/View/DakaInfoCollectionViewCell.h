//
//  DakaInfoCollectionViewCell.h
//  AfterSchool
//
//  Created by lg on 15/10/10.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LGDaKaInfo.h"

#define DaKaInfoCollectionCellHeight (186)

@interface DakaInfoCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) LGDaKaInfo *dakaInfo;

@end
