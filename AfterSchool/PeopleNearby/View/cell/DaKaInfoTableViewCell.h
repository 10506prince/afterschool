//
//  DaKaInfoTableViewCell.h
//  AfterSchool
//
//  Created by lg on 15/10/8.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITableView+NibCell.h"
#import "LGDaKaInfo.h"

#define DaKaInfoTableViewCellHeight (140)

typedef void(^DaKaInfoTableViewCellClickBtnBlock)();

@interface DaKaInfoTableViewCell : UITableViewCell

@property (nonatomic, strong) LGDaKaInfo * dakaInfo;

- (void)addAddFriendBlock:(DaKaInfoTableViewCellClickBtnBlock)addFriendBlock;
- (void)addInviteBlock:(DaKaInfoTableViewCellClickBtnBlock)inviteBlock;

@end
