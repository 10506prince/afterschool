//
//  DakaInfoGameInfoCellView.h
//  AfterSchool
//
//  Created by lg on 16/1/9.
//  Copyright © 2016年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DakaInfoGameInfoCellView : UIView

@property (nonatomic, copy) IBInspectable NSString * grade;
@property (nonatomic, copy) IBInspectable NSString * server;
@property (nonatomic, copy) IBInspectable NSString * attack;

@end
