//
//  DakaInfoCollectionViewCell.m
//  AfterSchool
//
//  Created by lg on 15/10/10.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "DakaInfoCollectionViewCell.h"
#import "Common.h"
#import "NSString+Null.h"
#import "UIImageView+WebCache.h"
#import "UIButton+SetBackgroundColor.h"
#import "UIImage+Scale.h"
#import "DakaInfoGameInfoCellView.h"

@interface DakaInfoCollectionViewCell ()

//头像
//名称
//信用等级
//距离
//性别
//年龄
//星座
//个性签名


//游戏段位
//游戏大区
//游戏局数
//战斗力

@property (nonatomic, weak) IBOutlet UIImageView * headImage;///<头像
@property (nonatomic, weak) IBOutlet UILabel * name;///<用户名
@property (nonatomic, weak) IBOutlet UIImageView * creditRating;///<信用等级
@property (nonatomic, weak) IBOutlet UIButton * distance;///<距离
@property (nonatomic, weak) IBOutlet UIImageView * sex;///<性别
@property (nonatomic, weak) IBOutlet UILabel * age;///<年龄
@property (nonatomic, weak) IBOutlet UILabel * constellation;///<星座
@property (nonatomic, weak) IBOutlet UILabel * signature;///<个性签名
@property (nonatomic, weak) IBOutlet UILabel * playCountLabel;///<约玩次数


@property (nonatomic, weak) IBOutlet DakaInfoGameInfoCellView * gameView;///<游戏数据View
//@property (nonatomic, weak) IBOutlet UILabel * gameRank;///<游戏段位
//@property (nonatomic, weak) IBOutlet UILabel * gameServer;///<游戏大区
//@property (nonatomic, weak) IBOutlet UILabel * gameNumber;///<游戏局数
//@property (nonatomic, weak) IBOutlet UILabel * gameFighting;///<游戏中战斗力

@property (nonatomic, weak) IBOutlet UIView * btnView;///<按键View
@property (nonatomic, weak) IBOutlet UIButton * addFriendBtn;///<添加好友Btn
@property (nonatomic, weak) IBOutlet UIButton * inviteBtn;///<邀约Btn


@property (copy) DakaInfoCollectionViewCellClickBtnBlock addFriendBlock;
@property (copy) DakaInfoCollectionViewCellClickBtnBlock inviteBlock;

@property (nonatomic, strong) UIView * line4;

@end

@implementation DakaInfoCollectionViewCell

- (void)awakeFromNib {
    
    self.line4 = [self getLine];
    [self.btnView addSubview:self.line4];
    
    
    [self.btnView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [self.btnView.layer setBorderWidth:1];
    
    
    [self.addFriendBtn setBackgroundColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
    [self.inviteBtn setBackgroundColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
    
    UIImage * inviteImage = [UIImage imageScaleWithOriginImage:[UIImage imageNamed:@"tabbar_about_play_selected"] toSize:CGSizeMake(20, 20)];
    [self.inviteBtn setImage:inviteImage forState:UIControlStateNormal];
    
    
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self.line4 setFrame:CGRectMake(self.btnView.bounds.size.width/2, 0, 1, self.btnView.bounds.size.height)];
    [self.btnView.layer setCornerRadius:self.btnView.bounds.size.height/2];
}

- (UIView *)getLine {
    UIView * line = [[UIView alloc] initWithFrame:CGRectZero];
    [line setBackgroundColor:[UIColor lightGrayColor]];
    
    return line;
}

#pragma mark - Action

- (IBAction)addFriendAction:(id)sender {
    DakaInfoCollectionViewCellClickBtnBlock block = self.addFriendBlock;
    if (block) {
        block();
    }
}

- (IBAction)inviteAction:(id)sender {
    DakaInfoCollectionViewCellClickBtnBlock block = self.inviteBlock;
    if (block) {
        block();
    }
}

- (void)setDakaInfo:(LGDaKaInfo *)dakaInfo {
    if (_dakaInfo == dakaInfo) {
        [self needUpdateDataEveryTime];
    } else {
        _dakaInfo = dakaInfo;
        [self needUpdateDataEveryTime];
        [self needUpdateData];
    }
}

- (void)needUpdateDataEveryTime {
    
}

- (void)needUpdateData {
    //头像
    if (_dakaInfo.headImageUrl.length == 0) {
        [_headImage setImage:[UIImage imageNamed:@"user_default_head"]];
    }else {
        NSURL * url = [NSURL URLWithString:_dakaInfo.headImageUrl];
        [_headImage sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"user_default_head"]];
    }
    
    //昵称
    if (_dakaInfo.nickName) {
        [_name setText:_dakaInfo.nickName];
    } else {[_name setText:nil];}
    
    //信用等级
    if (_dakaInfo.creditRating == 0) {
        [_creditRating setImage:[UIImage imageNamed:@"creditRating0"]];
    } else {
        NSString * imageName = [NSString stringWithFormat:@"creditRating%ld", (unsigned long)_dakaInfo.creditRating - 1];
        [_creditRating setImage:[UIImage imageNamed:imageName]];
    }
    
    ///距离
//    [_distance setText:[NSString stringWithFormat:@"%.2fkm", _dakaInfo.distance/1000.0]];
    [_distance setTitle:[NSString stringWithFormat:@"%.2fkm", _dakaInfo.distance/1000.0] forState:UIControlStateNormal];
    
    //性别
    UIImage * image = [UIImage imageNamed:[NSString stringWithFormat:@"sex%ld", (unsigned long)_dakaInfo.sex]];
    
    [_sex setImage:[image resizableImageWithCapInsets:UIEdgeInsetsMake(image.size.height/2, image.size.height/2 +1, image.size.width/2, image.size.width/2+1)]];
    
    //年龄
    [_age setText:[NSString stringWithFormat:@"%ld", (unsigned long)_dakaInfo.age]];
    
    //星座
    if (_dakaInfo.constellation) {
        [_constellation setText:[NSString stringWithFormat:@"%@座", _dakaInfo.constellation]];
    } else {
        [_constellation setText:nil];
    }
    
    //个性签名
    if (_dakaInfo.signature.length == 0) {
        [_signature setText:@"这个人很懒，什么都没有留下。。。"];
    } else {
        [_signature setText:_dakaInfo.signature];
    }
    
    //段位
    if (_dakaInfo.gameRank.length > 0) {
        [_gameView setGrade:_dakaInfo.gameRank];
    } else {
        [_gameView setGrade:@"无"];
    }
    
    //服务器
    if (_dakaInfo.gameServer.length > 0) {
        [_gameView setServer:_dakaInfo.gameServer];
    } else {
        [_gameView setServer:@"无"];
    }
    
    [_gameView setAttack:[NSString stringWithFormat:@"%ld", (unsigned long)_dakaInfo.gameFighting]];
    
    ///约玩次数
    [self.playCountLabel setText:[NSString stringWithFormat:@"接单%ld次", (unsigned long)_dakaInfo.playTotalCount]];
}

#pragma mark - Block
- (void)addAddFriendBlock:(DakaInfoCollectionViewCellClickBtnBlock)addFriendBlock {
    self.addFriendBlock = addFriendBlock;
}

- (void)addInviteBlock:(DakaInfoCollectionViewCellClickBtnBlock)inviteBlock {
    self.inviteBlock = inviteBlock;
}

@end
