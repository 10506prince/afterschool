//
//  DakaInfoGameInfoCellView.m
//  AfterSchool
//
//  Created by lg on 16/1/9.
//  Copyright © 2016年 Teson Draw. All rights reserved.
//

#import "DakaInfoGameInfoCellView.h"
#import "Common.h"
#import "CAShapeLayer+CustomPath.h"

#define Right_Offset 45

@interface DakaInfoGameInfoCellView ()

@property (nonatomic, strong) UILabel * gradeLabel;
@property (nonatomic, strong) UILabel * gradeValueLabel;
@property (nonatomic, strong) UILabel * serverLabel;
@property (nonatomic, strong) UILabel * serverValueLabel;
@property (nonatomic, strong) UILabel * attackLabel;
@property (nonatomic, strong) UILabel * attackValueLabel;

@property (nonatomic, strong) CAShapeLayer * line1;
@property (nonatomic, strong) CAShapeLayer * line2;
@property (nonatomic, strong) CAShapeLayer * line3;
@property (nonatomic, strong) CAShapeLayer * arrow;

@property (nonatomic, strong) IBInspectable UIColor * keyTextColor;
@property (nonatomic, strong) IBInspectable UIColor * valueTextColor;

@end

IB_DESIGNABLE
@implementation DakaInfoGameInfoCellView

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setUp];
    }
    return self;
}
- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self setUp];
    }
    return self;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUp];
    }
    return self;
}
- (void)setUp {
    self.gradeLabel = [self getNameLabel];
    self.serverLabel = [self getNameLabel];
    self.attackLabel = [self getNameLabel];
    
    [self.gradeLabel setText:@"段位"];
    [self.serverLabel setText:@"服务器"];
    [self.attackLabel setText:@"战斗力"];
    
    self.gradeValueLabel = [self getValueLabel];
    self.serverValueLabel = [self getValueLabel];
    self.attackValueLabel = [self getValueLabel];
    
    [self addSubview:self.gradeLabel];
    [self addSubview:self.gradeValueLabel];
    [self addSubview:self.serverLabel];
    [self addSubview:self.serverValueLabel];
    [self addSubview:self.attackLabel];
    [self addSubview:self.attackValueLabel];
    
    
    self.line1 = [self getLineLayer];
    self.line2 = [self getLineLayer];
    self.line3 = [self getLineLayer];
    self.arrow = [self getArrow];
    
    [self.layer addSublayer:self.line1];
    [self.layer addSublayer:self.line2];
    [self.layer addSublayer:self.line3];
    [self.layer addSublayer:self.arrow];
    
    
    [self setGrade:@"无"];
    [self setServer:@"无"];
    [self setAttack:@"0"];
    [self setKeyTextColor:LgColor(153, 1)];
    [self setValueTextColor:LgColor(153, 1)];
    
}

- (UILabel *)getNameLabel {
    UILabel * nameLabel = [[UILabel alloc] init];
    [nameLabel setFont:[UIFont systemFontOfSize:12]];
    
    return nameLabel;
}
- (UILabel *)getValueLabel {
    UILabel * valueLabel = [[UILabel alloc] init];
    [valueLabel setFont:[UIFont systemFontOfSize:15]];
    return valueLabel;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat width = self.bounds.size.width;
    
    [self.gradeLabel sizeToFit];
    [self.serverLabel sizeToFit];
    [self.attackLabel sizeToFit];
    
    [self.gradeValueLabel sizeToFit];
    [self.serverValueLabel sizeToFit];
    [self.attackValueLabel sizeToFit];
    
    CGFloat duanWith = (width - Right_Offset)/6;
    [self.gradeLabel setCenter:CGPointMake(duanWith, 8 + self.gradeLabel.bounds.size.height/2)];
    [self.serverLabel setCenter:CGPointMake(duanWith * 3, 8 + self.gradeLabel.bounds.size.height/2)];
    [self.attackLabel setCenter:CGPointMake(duanWith * 5, 8 + self.gradeLabel.bounds.size.height/2)];
    
    [self.gradeValueLabel setCenter:CGPointMake(duanWith, CGRectGetMaxY(self.gradeLabel.frame) + 5 + self.gradeValueLabel.bounds.size.height/2)];
    [self.serverValueLabel setCenter:CGPointMake(duanWith * 3, CGRectGetMaxY(self.serverLabel.frame) + 5 + self.serverValueLabel.bounds.size.height/2)];
    [self.attackValueLabel setCenter:CGPointMake(duanWith * 5, CGRectGetMaxY(self.attackLabel.frame) + 5 + self.attackValueLabel.bounds.size.height/2)];

    self.line1.path = [self getLinePathWithSuperRect:self.bounds duan:3 currentDuan:1];
    self.line2.path = [self getLinePathWithSuperRect:self.bounds duan:3 currentDuan:2];
    self.line3.path = [self getLinePathWithSuperRect:self.bounds duan:3 currentDuan:3];
    self.arrow.path = [self getArrowPathWithSuperRect:self.bounds];
}

- (CAShapeLayer *)getLineLayer {
    CAShapeLayer * line = [CAShapeLayer layer];
    [line setFillColor:[UIColor clearColor].CGColor];
    [line setStrokeColor:LgColor(204, 1).CGColor];
    [line setLineWidth:1/[UIScreen mainScreen].scale];
    
    return line;
}
- (CAShapeLayer *)getArrow {
    CAShapeLayer * arrow = [CAShapeLayer layer];
    [arrow setFillColor:[UIColor clearColor].CGColor];
    [arrow setStrokeColor:LgColor(204, 1).CGColor];
    [arrow setLineWidth:2/[UIScreen mainScreen].scale];
    
    return arrow;
}

- (CGPathRef)getLinePathWithSuperRect:(CGRect)rect duan:(NSInteger)duan currentDuan:(NSInteger)currentDuan{
    CGFloat width = (rect.size.width - Right_Offset)/duan;
    CGFloat height = rect.size.height;
    
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathMoveToPoint(path, NULL, width * currentDuan, 10);
    CGPathAddLineToPoint(path, NULL, width * currentDuan, height - 10);
    
    UIBezierPath * arrowPath = [UIBezierPath bezierPathWithCGPath:path];
    CGPathRelease(path);
    
    return arrowPath.CGPath;
}
- (CGPathRef)getArrowPathWithSuperRect:(CGRect)rect {
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathMoveToPoint(path, NULL, rect.size.width - 12 - 8, rect.size.height/2 - 8);
    CGPathAddLineToPoint(path, NULL, rect.size.width - 12, rect.size.height/2);
    CGPathAddLineToPoint(path, NULL, rect.size.width - 12 - 8, rect.size.height/2 + 8);
    
    UIBezierPath * arrowPath = [UIBezierPath bezierPathWithCGPath:path];
    CGPathRelease(path);
    
    return arrowPath.CGPath;
}

#pragma mark - setValue 
- (void)setGrade:(NSString *)grade {
    _grade = [grade copy];
    
    [self.gradeValueLabel setText:_grade];
    [self setNeedsLayout];
}
- (void)setServer:(NSString *)server {
    _server = [server copy];
    
    [self.serverValueLabel setText:_server];
    [self setNeedsLayout];
}
- (void)setAttack:(NSString *)attack {
    _attack = [attack copy];
    
    [self.attackValueLabel setText:_attack];
    [self setNeedsLayout];
}

- (void)setKeyTextColor:(UIColor *)keyTextColor {
    _keyTextColor = keyTextColor;
    
    [self.gradeLabel setTextColor:_keyTextColor];
    [self.serverLabel setTextColor:_keyTextColor];
    [self.attackLabel setTextColor:_keyTextColor];
}
- (void)setValueTextColor:(UIColor *)valueTextColor {
    _valueTextColor = valueTextColor;
    
    [self.gradeValueLabel setTextColor:_valueTextColor];
    [self.serverValueLabel setTextColor:_valueTextColor];
    [self.attackValueLabel setTextColor:_valueTextColor];
}
@end
