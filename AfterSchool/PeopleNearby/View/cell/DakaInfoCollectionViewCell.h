//
//  DakaInfoCollectionViewCell.h
//  AfterSchool
//
//  Created by lg on 15/10/10.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LGDaKaInfo.h"
#import "UITableView+NibCell.h"

#define DaKaInfoCollectionCellHeight (129)

typedef void(^DakaInfoCollectionViewCellClickBtnBlock)();

@interface DakaInfoCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) LGDaKaInfo *dakaInfo;

- (void)addAddFriendBlock:(DakaInfoCollectionViewCellClickBtnBlock)addFriendBlock;
- (void)addInviteBlock:(DakaInfoCollectionViewCellClickBtnBlock)inviteBlock;

@end
