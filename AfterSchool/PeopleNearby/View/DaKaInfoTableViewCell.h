//
//  DaKaInfoTableViewCell.h
//  AfterSchool
//
//  Created by lg on 15/10/8.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LGDaKaInfo.h"

#define DaKaInfoTableViewCellHeight (184)

@interface DaKaInfoTableViewCell : UITableViewCell

@property (nonatomic, strong) LGDaKaInfo * dakaInfo;

@end
