//
//  LGPinAnnotationView.m
//  baiduMap
//
//  Created by lg on 15/9/28.
//  Copyright © 2015年 李刚. All rights reserved.
//

#import "LGPinAnnotationView.h"
#import "UIImage+View.h"
#import "UIColor+RandomColor.h"

@implementation LGPinAnnotationView

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self initSet];
    }
    return self;
}

- (id)initWithAnnotation:(id<BMKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initSet];
    }
    
    return self;
}

- (void)initSet {
    self.image = [self customHead];
    self.paopaoView = [[BMKActionPaopaoView alloc] initWithCustomView:self.myPaopaoView];
    self.canShowCallout = NO;//不显示系统自带的泡泡
    
//    UIView * leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
//    [leftView setBackgroundColor:[UIColor redColor]];
//    UIView * rightView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
//    [rightView setBackgroundColor:[UIColor greenColor]];
//
//
//    UIView * paopaoView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 50, 30)];
//    [paopaoView setBackgroundColor:[UIColor cyanColor]];
//
//    //自定义泡泡
//    BMKActionPaopaoView * customPaopaoView = [[BMKActionPaopaoView alloc] initWithCustomView:paopaoView];
//    [annotationView setPaopaoView:customPaopaoView];
//    [annotationView setCalloutOffset:CGPointMake(0, 0)];
//
//    [annotationView setLeftCalloutAccessoryView:leftView];
//    [annotationView setRightCalloutAccessoryView:rightView];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (UIImage *)customHead {
    //自定义头像
    UIView * headView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    [headView setBackgroundColor:[UIColor redColor]];
    [headView.layer setCornerRadius:50];
    [headView.layer setBorderColor:[UIColor whiteColor].CGColor];
    [headView.layer setBorderWidth:10];
    [headView setClipsToBounds:YES];

    //        UILabel * label = [[UILabel alloc] initWithFrame:headView.bounds];
    //        [label setText:@"a"];
    //        [headView addSubview:label];

    UIImageView * headImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, headView.bounds.size.width - 10, headView.bounds.size.height - 10)];
    [headImage setImage:[UIImage imageNamed:@"location"]];
    [headView addSubview:headImage];
    
    return [UIImage imageFromView:headView];
}

- (LGPaopaoView *)myPaopaoView {
    if (_myPaopaoView == nil) {
        _myPaopaoView = [[LGPaopaoView alloc] initWithFrame:CGRectMake(0, 0, 100, 30)];
        [_myPaopaoView setBackgroundColor:[UIColor randomColorOne]];
    }
    
    return _myPaopaoView;
}

- (void)setSelected:(BOOL)selected {
    [super setSelected:selected];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    if (animated) {
//        CGRect selfFrame = self.frame;
//        UIView * superView = self.superview;
//        [self.myPaopaoView setFrame:selfFrame];
//        [superView insertSubview:self.myPaopaoView aboveSubview:self];
        
        CABasicAnimation *animation = [CABasicAnimation animation];
        animation.keyPath = @"transform.rotation";
        animation.duration = 4.f;
        animation.fromValue = @(0.f);
        animation.toValue = @(2 * M_PI);
        animation.repeatCount = INFINITY;
        [self.myPaopaoView.layer addAnimation:animation forKey:@"rotation"];
    } else {
        [self.myPaopaoView.layer removeAnimationForKey:@"rotation"];
//        [self.myPaopaoView removeFromSuperview];
    }
}

///确定选择
- (void)didSelectAnnotationViewInMap:(BMKMapView *)mapView {
    // Center map at annotation point
//    [mapView setCenterCoordinate:_coordinate animated:YES];
//    [self expand];//展开
}

///取消选择
- (void)didDeselectAnnotationViewInMap:(BMKMapView *)mapView {
//    [self shrink];//收缩
}


//- (void)bounce4AnimationStopped{
//    //CGPoint point = [_mapView convertCoordinate:selectedAV.annotation.coordinate toPointToView:selectedAV.superview];
//    //DLog(@"annotationPoint4:x=%.1f, y= %.1f", point.x, point.y);
//    //[self changeBubblePosition];
//    [UIView beginAnimations:nil context:nil];
//    [UIView setAnimationDuration:kTransitionDuration/6];
//    //[UIView setAnimationDelegate:self];
//    //[UIView setAnimationDidStopSelector:@selector(bounce5AnimationStopped)];
//    bubbleView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.0, 1.0);
//    [UIView commitAnimations];
//}
//
//- (void)bounce3AnimationStopped{
//    //CGPoint point = [_mapView convertCoordinate:selectedAV.annotation.coordinate toPointToView:selectedAV.superview];
//    //DLog(@"annotationPoint3:x=%.1f, y= %.1f", point.x, point.y);
//    //[self changeBubblePosition];
//    [UIView beginAnimations:nil context:nil];
//    [UIView setAnimationDuration:kTransitionDuration/6];
//    [UIView setAnimationDelegate:self];
//    [UIView setAnimationDidStopSelector:@selector(bounce4AnimationStopped)];
//    bubbleView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.95, 0.95);
//    [UIView commitAnimations];
//}
//
//- (void)bounce2AnimationStopped{
//    //CGPoint point = [_mapView convertCoordinate:selectedAV.annotation.coordinate toPointToView:selectedAV.superview];
//    //DLog(@"annotationPoint2:x=%.1f, y= %.1f", point.x, point.y);
//    //[self changeBubblePosition];
//    [UIView beginAnimations:nil context:nil];
//    [UIView setAnimationDuration:kTransitionDuration/6];
//    [UIView setAnimationDelegate:self];
//    [UIView setAnimationDidStopSelector:@selector(bounce3AnimationStopped)];
//    bubbleView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.05, 1.05);
//    [UIView commitAnimations];
//}
//
//- (void)bounce1AnimationStopped{
//    //CGPoint point = [_mapView convertCoordinate:selectedAV.annotation.coordinate toPointToView:selectedAV.superview];
//    //DLog(@"annotationPoint1:x=%.1f, y= %.1f", point.x, point.y);
//    //[self changeBubblePosition];
//    [UIView beginAnimations:nil context:nil];
//    [UIView setAnimationDuration:kTransitionDuration/6];
//    [UIView setAnimationDelegate:self];
//    [UIView setAnimationDidStopSelector:@selector(bounce2AnimationStopped)];
//    bubbleView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
//    [UIView commitAnimations];
//}



@end




