//
//  SceneViewController.m
//  AfterSchool
//
//  Created by lg on 15/12/29.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "SceneViewController.h"
#import "Common.h"
#import "UIButton+SetBackgroundColor.h"
#import "TalkingData.h"
#import "TDSingleton.h"

@interface SceneViewController ()

@property (nonatomic, weak) IBOutlet UIButton * sceneBtn1;
@property (nonatomic, weak) IBOutlet UIButton * sceneBtn2;
@property (nonatomic, weak) IBOutlet UIButton * sceneBtn3;
@property (nonatomic, weak) IBOutlet UIButton * sceneBtn4;
@property (nonatomic, weak) IBOutlet UIButton * sceneBtn5;
//@property (nonatomic, weak) IBOutlet UIButton * sceneBtn6;

@property (nonatomic, weak) IBOutlet UIButton * backBtn;

@property (copy) SceneViewControllerSelectBlock selectBlock;

@end

@implementation SceneViewController

- (void)dealloc
{
    NSLogSelfMethodName;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [TalkingData trackPageBegin:selfControllerName];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [TalkingData trackPageEnd:selfControllerName];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initData];
    [self initUserInterface];
}

- (void)initData {
    
}

- (void)initUserInterface {
    
    [self.view setClipsToBounds:YES];
    [self.view setBackgroundColor:APPLightColor];
    
    NSArray <LGSceneInfo *> * scenes = [[TDSingleton instance].scenes sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        LGSceneInfo * one = (LGSceneInfo *)obj1;
        LGSceneInfo * two = (LGSceneInfo *)obj2;
        if (one.order < two.order) {
            return NSOrderedAscending;
        } else if (one.order == two.order) {
            return NSOrderedSame;
        } else {
            return NSOrderedDescending;
        }
    }];
    
    NSArray * btns = @[self.sceneBtn1,self.sceneBtn2,self.sceneBtn3,self.sceneBtn4,self.sceneBtn5];
//    NSArray * btnNames = @[@"遇大神",@"保晋级",@"约妹子",@"来上分",@"随意约"];
    
    int i = 0;
    for (UIButton * btn in btns) {
        [btn.layer setBorderColor:APPBorderLightColor.CGColor];
        [btn.layer setBorderWidth:1];
        
        [btn.layer setCornerRadius:3];
        [btn.layer setMasksToBounds:YES];
        
        [btn setTitleColor:LgColor(238, 1) forState:UIControlStateNormal];
        
        [btn setBackgroundColor:LgColor(0, 0.2) forState:UIControlStateHighlighted];
        
        [btn addTarget:self action:@selector(mysceneBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        
        LGSceneInfo * scene = scenes[i];
        [btn setTitle:scene.name forState:UIControlStateNormal];
        [btn setTag:scene.sceneId];
        
        i++;
    }
    
    [self.backBtn addTarget:self action:@selector(expandAction:) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - Action

- (IBAction)mysceneBtnAction:(UIButton *)sender {
    //NSLog(@"选择了一个情景");
    
    NSInteger sceneId = sender.tag;
    for (LGSceneInfo * scene in [TDSingleton instance].scenes) {
        if (scene.sceneId == sceneId) {
            SceneViewControllerSelectBlock block = self.selectBlock;
            if (block) {
                block(scene);
            }
            return;
        }
    }
    
}

- (IBAction)expandAction:(UIButton *)sender {
    sender.selected = !sender.selected;
    
    CGRect frame = self.view.frame;
    if (sender.selected) {
        frame.size.height += 30 + 10;
    } else {
        frame.size.height -= 30 + 10;
    }
    [UIView animateWithDuration:0.3 animations:^{
        [self.view setFrame:frame];
    }];
}



#pragma mark - Block
- (void)addSelectBlock:(SceneViewControllerSelectBlock)selectBlock {
    self.selectBlock = selectBlock;
}


@end
