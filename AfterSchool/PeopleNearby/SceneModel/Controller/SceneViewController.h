//
//  SceneViewController.h
//  AfterSchool
//
//  Created by lg on 15/12/29.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LGSceneInfo.h"

typedef void(^SceneViewControllerSelectBlock)(LGSceneInfo * scene);

@interface SceneViewController : UIViewController

- (void)addSelectBlock:(SceneViewControllerSelectBlock)selectBlock;

@end
