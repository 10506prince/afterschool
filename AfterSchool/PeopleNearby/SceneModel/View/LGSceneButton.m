//
//  LGSceneButton.m
//  AfterSchool
//
//  Created by lg on 15/12/12.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGSceneButton.h"

@interface LGSceneButton ()

@property (nonatomic, assign) IBInspectable CGFloat borderWith;
@property (nonatomic, strong) IBInspectable UIColor * borderColor;
@property (nonatomic, assign) IBInspectable CGFloat cornerRadius;

@end

IB_DESIGNABLE
@implementation LGSceneButton

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setUp];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self setUp];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUp];
    }
    return self;
}

- (void)setUp {
    
    self.borderColor = [UIColor whiteColor];
    self.borderWith = 1.0f;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat width = self.bounds.size.width;
    CGFloat height = self.bounds.size.height;
    
    self.cornerRadius = width < height ? width/2 : height/2;
}

#pragma mark - setValue
- (void)setBorderColor:(UIColor *)borderColor {
    _borderColor = borderColor;

    self.layer.borderColor = _borderColor.CGColor;
}

- (void)setBorderWith:(CGFloat)borderWith {
    _borderWith = borderWith;
    
    self.layer.borderWidth = _borderWith;
}

- (void)setCornerRadius:(CGFloat)cornerRadius {
    _cornerRadius = cornerRadius;
    
    self.layer.cornerRadius = _cornerRadius;
}

@end
