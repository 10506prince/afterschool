//
//  CityRootView.h
//  AfterSchool
//
//  Created by lg on 15/10/22.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavigationBarView.h"
#import "LGScrollView.h"

#define BtnTag 100

#define CENTER_POINT CGPointMake(LgScreenWidth/2, 170)
#define BIG_RADIUS 127.0f
#define SMALL_RADIUS 80.0f

typedef enum : NSUInteger {
//    SceneModelTypeNone = 0,     ///<无
    SceneModelTypeRank,         ///<段位
    SceneModelTypeFighting,     ///<战斗力
    SceneModelTypeMan,          ///<男神
    SceneModelTypeWoman,        ///<女神
    SceneModelTypeboredom,      ///<无聊
} SceneModelType;

@interface SceneModelRootView : UIView

@property (nonatomic, strong) NavigationBarView * navigationBarView;

@property (nonatomic, strong) LGScrollView * scrollView;
@property (nonatomic, strong) UIView * roundView;
@property (nonatomic, strong) UIView * headViewShadow;
@property (nonatomic, strong) UIImageView * headImageView;
@property (nonatomic, strong) NSArray<UIButton *> * btns;
@property (nonatomic, strong) NSArray * controlPoints;
@property (nonatomic, strong) UIButton * unLikeBtn;
@property (nonatomic, strong) UIView * line;

@end
