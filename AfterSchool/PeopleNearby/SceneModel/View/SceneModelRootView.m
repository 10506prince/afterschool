//
//  CityRootView.m
//  AfterSchool
//
//  Created by lg on 15/10/22.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "SceneModelRootView.h"
#import "Common.h"
#import "UIColor+RGB.h"

#define SCENEMODELVIEW_DEFAULT_HEIGHT (568 -64)

#define BIG_OFFSET 8
#define SMALL_OFFSET 4

@implementation SceneModelRootView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUp];
    }
    return self;
}

- (void)setUp {
    [self addSubview:self.scrollView];
    
    [self addSubview:self.navigationBarView];
}

- (NavigationBarView *)navigationBarView {
    if (!_navigationBarView) {
        _navigationBarView = [[NavigationBarView alloc] initWithTitle:@"情景模式"
                                                           titleColor:nil
                                                      backgroundColor:nil
                                              leftButtonBgViewOriginX:0
                                                 leftButtonImageWidth:22
                                                 leftButtonTitleColor:[UIColor whiteColor]
                                                       leftButtonName:@"附近"
                                                  leftButtonImageName:@"navigation_bar_return_button_left_arrow" rightButtonName:nil
                                                 rightButtonImageName:nil];
        
    }
    return _navigationBarView;
}

- (LGScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[LGScrollView alloc] initWithFrame:self.bounds];
        [_scrollView setBackgroundType:LGScrollViewBackgroundTypeDark];
        [_scrollView setHeadType:LGScrollViewHeadTypeDark];
        
        [_scrollView setContentSize:CGSizeMake(self.bounds.size.width, SCENEMODELVIEW_DEFAULT_HEIGHT)];
        
        [_scrollView addSubview:self.roundView];
        [_scrollView addSubview:self.headViewShadow];
        [_scrollView addSubview:self.headImageView];
        
        [_scrollView addSubview:self.unLikeBtn];
        [_scrollView addSubview:self.line];
        [self addSceneModelBtnToView:_scrollView];
    }
    return _scrollView;
}

- (UIView *)roundView {
    if (!_roundView) {
        _roundView = [[UIView alloc] initWithFrame:self.bounds];
        [_roundView setBackgroundColor:[UIColor clearColor]];
        
        [self addRoundInView:_roundView];
    }
    return _roundView;
}

- (void)addEffectViewInView:(UIView *)view {
    UIBlurEffect * blur = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    UIVisualEffectView * effectView = [[UIVisualEffectView alloc] initWithEffect:blur];
    [effectView setFrame:view.bounds];
    
    [view addSubview:effectView];
}

- (void)addRoundInView:(UIView *)view {
    CAShapeLayer * bigRound = [CAShapeLayer layer];
    CAShapeLayer * smallRound = [CAShapeLayer layer];
    
    UIColor * strokeColor = [UIColor lightGrayColor];
    UIColor * fillColor = [UIColor clearColor];
    CGPoint center = CENTER_POINT;
    CGFloat lineWidth = 1.0f;
    
    [bigRound setStrokeColor:strokeColor.CGColor];
    [smallRound setStrokeColor:strokeColor.CGColor];
    
    [bigRound setFillColor:fillColor.CGColor];
    [smallRound setFillColor:fillColor.CGColor];
    
    //[bigRound setLineCap:kCALineCapSquare];
    
    bigRound.path = [self getRoundPathWithCenter:center radius:119.0f];
    smallRound.path = [self getRoundPathWithCenter:center radius:79.0f];
    
    [bigRound setLineWidth:2.5f];
    [smallRound setLineWidth:lineWidth];
    
    //[bigRound setStrokeStart:0.0f];
    //[bigRound setStrokeEnd:1.0];
    
    [view.layer addSublayer:bigRound];
    [view.layer addSublayer:smallRound];
}

- (CGPathRef)getRoundPathWithCenter:(CGPoint)center radius:(CGFloat)radius {
    
    // 贝塞尔曲线(创建一个圆)
    UIBezierPath *roundPath = [UIBezierPath bezierPathWithArcCenter:center
                                                        radius:radius
                                                    startAngle:0
                                                      endAngle:M_PI * 2
                                                     clockwise:YES];
     
    
    //UIBezierPath * roundPath = [UIBezierPath bezierPathWithRoundedRect:rect cornerRadius:radius];
    return roundPath.CGPath;
}

- (UIImageView *)headImageView {
    if (!_headImageView) {
        _headImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 80, 80)];
        [_headImageView setCenter:CENTER_POINT];
        [_headImageView setImage:[UIImage imageNamed:@"defaultHeadImage"]];
        
        [_headImageView.layer setCornerRadius:_headImageView.bounds.size.width/2];
        //[_headImageView.layer setMasksToBounds:YES];
        [_headImageView setClipsToBounds:YES];
    }
    return _headImageView;
}

- (UIView *)headViewShadow {
    if (!_headViewShadow) {
        _headViewShadow = [[UIView alloc] initWithFrame:self.headImageView.frame];
        [_headViewShadow setBackgroundColor:[UIColor redColor]];
        
        [_headViewShadow.layer setCornerRadius:_headViewShadow.bounds.size.height/2];
        
        [_headViewShadow.layer setShadowColor:[UIColor colorWithWhite:0.8 alpha:1].CGColor];
        [_headViewShadow.layer setShadowOffset:CGSizeMake(0, 0)];
        [_headViewShadow.layer setShadowRadius:40];
        [_headViewShadow.layer setShadowOpacity:0.8];
    }
    return _headViewShadow;
}

- (UIButton *)unLikeBtn {
    if (!_unLikeBtn) {
        _unLikeBtn = [UIButton buttonWithType:UIButtonTypeSystem];
        //[_unLikeBtn setFrame:CGRectMake(40, SCENEMODELVIEW_DEFAULT_HEIGHT - 130, self.bounds.size.width - 80, 40)];
        [_unLikeBtn setFrame:CGRectMake(self.bounds.size.width/2 - 122, SCENEMODELVIEW_DEFAULT_HEIGHT - 130, 244, 40)];
        
        [_unLikeBtn setTitle:@"不喜欢?" forState:UIControlStateNormal];
        [_unLikeBtn.titleLabel setFont:[UIFont systemFontOfSize:20]];
        [_unLikeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        [_unLikeBtn.layer setCornerRadius:_unLikeBtn.bounds.size.height/2];
        [_unLikeBtn setClipsToBounds:YES];
        
        //[_unLikeBtn setBackgroundColor:[UIColor getColor:@"1CA4F7"]];
        [_unLikeBtn setBackgroundColor:[UIColor getColorDec:@"041182246"]];
    }
    return _unLikeBtn;
}

- (UIView *)line {
    if (!_line) {
        _line = [[UIView alloc] initWithFrame:CGRectMake(25, SCENEMODELVIEW_DEFAULT_HEIGHT - 70 - SINGLE_LINE_ADJUST_OFFSET, self.bounds.size.width - 50, SINGLE_LINE_WIDTH)];

        [_line setBackgroundColor:[UIColor lightGrayColor]];
    }
    return _line;
}

- (void)addSceneModelBtnToView:(UIView *)view {
    
    CGFloat btnWidth = 44.0f;
    CGRect frames[5] = {
        CGRectMake(0, 0, btnWidth, btnWidth),
        CGRectMake(0, 0, 40, 40),
        CGRectMake(0, 0, btnWidth, btnWidth),
        CGRectMake(0, 0, btnWidth, btnWidth),
        CGRectMake(0, 0, 82, 30)
    };
    
    NSArray * backgroundColors = @[[UIColor getColorDec:@"255179016"],
                                   [UIColor getColorDec:@"196144191"],
                                   [UIColor getColorDec:@"059148211"],
                                   [UIColor getColorDec:@"255176204"],
                                   [UIColor getColorDec:@"137191086"],];
    
    CGFloat offsetX[5] = {-80.0,-100.0,-75.0,50.0,60.0};
    CGPoint centers[5] = {
        CGPointMake(CENTER_POINT.x + offsetX[0], CENTER_POINT.y - getAWithBAndRadius(offsetX[0],BIG_RADIUS - BIG_OFFSET)),
        CGPointMake(CENTER_POINT.x + offsetX[1], CENTER_POINT.y + getAWithBAndRadius(offsetX[1],BIG_RADIUS - BIG_OFFSET)),
        
        CGPointMake(CENTER_POINT.x + offsetX[2], CENTER_POINT.y + getAWithBAndRadius(offsetX[2],SMALL_RADIUS - SMALL_OFFSET)),
        CGPointMake(CENTER_POINT.x + offsetX[3], CENTER_POINT.y - getAWithBAndRadius(offsetX[3],SMALL_RADIUS - SMALL_OFFSET)),
        CGPointMake(CENTER_POINT.x + offsetX[4] + 20, CENTER_POINT.y + getAWithBAndRadius(offsetX[4],SMALL_RADIUS - SMALL_OFFSET))
    };
    
    CGFloat offsetXX[5] = {-160.0,-20.0,-35.0,30.0,100.0};
    CGPoint controlPoints[5] = {
        CGPointMake(CENTER_POINT.x + offsetXX[0], CENTER_POINT.y - getAWithBAndRadius(offsetXX[0],BIG_RADIUS * 1.414)),
        CGPointMake(CENTER_POINT.x + offsetXX[1], CENTER_POINT.y + getAWithBAndRadius(offsetXX[1],BIG_RADIUS * 1.414)),
        
        CGPointMake(CENTER_POINT.x + offsetXX[2], CENTER_POINT.y + getAWithBAndRadius(offsetXX[2],SMALL_RADIUS * 1.414)),
        CGPointMake(CENTER_POINT.x + offsetXX[3], CENTER_POINT.y - getAWithBAndRadius(offsetXX[3],SMALL_RADIUS * 1.414)),
        CGPointMake(CENTER_POINT.x + offsetXX[4], CENTER_POINT.y + getAWithBAndRadius(offsetXX[4],SMALL_RADIUS * 1.414))
    };
    
    NSMutableArray * array = [NSMutableArray array];
    for (int i = 0; i < 5; i++) {
        CGPoint point = controlPoints[i];
        NSValue * value = [NSValue valueWithCGPoint:point];
        [array addObject:value];
    }
    self.controlPoints = array;
    
    NSArray * btnImgNames = @[@"scene_qiecuo",@"scene_duanwei",@"scene_nan",@"scene_luoli",@"scene_wuliao"];
    SceneModelType type[5] = {SceneModelTypeFighting,SceneModelTypeRank,SceneModelTypeMan,SceneModelTypeWoman,SceneModelTypeboredom};
    
    NSMutableArray * btns = [NSMutableArray array];
    
    for (int i = 0; i < 5; i++) {
        UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setFrame:frames[i]];
        [btn setCenter:centers[i]];
        [btn setImage:[UIImage imageNamed:btnImgNames[i]] forState:UIControlStateNormal];
        
        [btn setBackgroundColor:backgroundColors[i]];
        
        [btn.layer setCornerRadius:btn.bounds.size.height/2];
        
        [btn setClipsToBounds:YES];
        
        if (i == 4) {
            [btn setTitle:@"无聊" forState:UIControlStateNormal];
            [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [btn.titleLabel setFont:[UIFont systemFontOfSize:16]];
        }
        
        btn.tag = type[i];
        
        [btns addObject:btn];
        [view addSubview:btn];
    }
    
    _btns = btns;
}

///通过勾股定理计算未知的一条边长
CGFloat getAWithBAndRadius(CGFloat b, CGFloat radius) {
    CGFloat result = 0.0f;
    
    CGFloat bb = fabs(b);
    
    if (bb>radius) {
        return result;
    }
    
    result = sqrt(pow(radius, 2) - pow(bb, 2));
    
    return round(result);
}

@end
