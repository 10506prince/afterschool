//
//  LGSearchPoiTestViewController.m
//  AfterSchool
//
//  Created by lg on 15/11/4.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGSearchPoiTestViewController.h"
#import <BaiduMapAPI_Cloud/BMKCloudSearchComponent.h>

@interface LGSearchPoiTestViewController () {
    BMKCloudSearch* _search;
}

@end
@implementation LGSearchPoiTestViewController

- (void)dealloc
{
    NSLog(@"%s", __FUNCTION__);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

//发起本地云检索
-(IBAction)onClickLocalSearch

{
    BMKCloudLocalSearchInfo *cloudLocalSearch = [[BMKCloudLocalSearchInfo alloc]init];
    cloudLocalSearch.ak = @"SkyQpwFfmNfc1AS9MfWoPSZX";
    cloudLocalSearch.geoTableId = 121218;
    cloudLocalSearch.pageIndex = 0;
    cloudLocalSearch.pageSize = 10;
    cloudLocalSearch.region = @"成都市";
    cloudLocalSearch.keyword = @"天府五街";
    BOOL flag = [_search localSearchWithSearchInfo:cloudLocalSearch];
    if(flag)
    {
        NSLog(@"本地云检索发送成功");
    }
    else
    {
        NSLog(@"本地云检索发送失败");
    }
}
//发起周边云检索
-(IBAction)onClickNearbySearch
{
    BMKCloudNearbySearchInfo *cloudNearbySearch = [[BMKCloudNearbySearchInfo alloc]init];
    cloudNearbySearch.ak = @"SkyQpwFfmNfc1AS9MfWoPSZX";
    cloudNearbySearch.geoTableId = 121218;
    cloudNearbySearch.pageIndex = 0;
    cloudNearbySearch.pageSize = 10;
    cloudNearbySearch.location = @"104.072939,30.544244";
    cloudNearbySearch.radius = 500;
    cloudNearbySearch.keyword = @"天府五街";
    BOOL flag = [_search nearbySearchWithSearchInfo:cloudNearbySearch];
    if(flag)
    {
        NSLog(@"周边云检索发送成功");
    }
    else
    {
        NSLog(@"周边云检索发送失败");
    }
    
}
//发起矩形云检索
-(IBAction)onClickBoundSearch
{
    BMKCloudBoundSearchInfo *cloudBoundSearch = [[BMKCloudBoundSearchInfo alloc]init];
    cloudBoundSearch.ak = @"SkyQpwFfmNfc1AS9MfWoPSZX";
    cloudBoundSearch.geoTableId = 121218;
    cloudBoundSearch.pageIndex = 0;
    cloudBoundSearch.pageSize = 10;
    cloudBoundSearch.bounds = @"116.30,36.20;118.30,40.20";
    cloudBoundSearch.keyword = @"天安门";
    BOOL flag = [_search boundSearchWithSearchInfo:cloudBoundSearch];
    if(flag)
    {
        NSLog(@"矩形云检索发送成功");
    }
    else
    {
        NSLog(@"矩形云检索发送失败");
    }
    
}
//发起详情云检索
-(IBAction)onClickDetailSearch
{
    BMKCloudDetailSearchInfo *cloudDetailSearch = [[BMKCloudDetailSearchInfo alloc]init];
    cloudDetailSearch.ak = @"SkyQpwFfmNfc1AS9MfWoPSZX";
    cloudDetailSearch.geoTableId = 121218;
    cloudDetailSearch.uid = @"1477554117";
    BOOL flag = [_search detailSearchWithSearchInfo:cloudDetailSearch];
    if(flag)
    {
        NSLog(@"详情云检索发送成功");
    }
    else
    {
        NSLog(@"详情云检索发送失败");
    }
    
}

@end
