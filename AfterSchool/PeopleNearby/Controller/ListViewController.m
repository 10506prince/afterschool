//
//  ListViewController.m
//  AfterSchool
//
//  Created by lg on 15/12/12.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "ListViewController.h"
#import "UIImage+ImageWithColor.h"
#import "Common.h"
#import "TalkingData.h"
#import "MJRefresh.h"
#import "CreatAssignOrderViewController.h"
#import "TDNetworkRequest.h"
#import "SVProgressHUD.h"
#import "LGCloudSearchService.h"
#import "LGDefineNetServer.h"
#import "LGLocationService.h"
#import "TDSingleton.h"
#import "PeopleGameDetailViewController.h"
#import <BaiduMapAPI_Utils/BMKUtilsComponent.h>

#import "DaKaInfoTableViewCell.h"
#import "PPiFlatSegmentedControl.h"
#import "NSString+FontAwesome.h"

@interface ListViewController () <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>

//@property (nonatomic, strong) NSMutableArray <LGDaKaInfo *> * searchResult;
//@property (nonatomic, strong) NSArray <LGDaKaInfo *> * dataSource;

@property (strong) NSMutableArray <LGDaKaInfo *> * nearByUsers;///<附近用户
@property (strong) NSMutableArray <LGDaKaInfo *> * lastestUsers;///<最新用户
@property (strong) NSMutableArray <LGDaKaInfo *> * hottestUsers;///<最热用户

@property (nonatomic, strong) PPiFlatSegmentedControl * segment;
@property (nonatomic, strong) UIScrollView * scrollView;
@property (nonatomic, strong) UITableView * tableViewNearBy;
@property (nonatomic, strong) UITableView * tableViewHotest;
@property (nonatomic, strong) UITableView * tableViewLastest;
//@property (nonatomic, strong) UISearchBar * searchBar;


@property (nonatomic, assign) BOOL firstLocation;
@property (nonatomic, assign) BOOL firstRefreshNearBy;
@property (nonatomic, assign) BOOL firstRefreshLastest;
@property (nonatomic, assign) BOOL firstRefreshHotest;

@property (nonatomic, assign) NSUInteger selectIndex;///选择的标签

@property (nonatomic, assign) NSUInteger nearByCurrentPage;///<附近当前页
@property (nonatomic, assign) NSUInteger nearByPageSize;///<附近每页数量
@property (nonatomic, assign) NSUInteger nearByPageReadSize;///<附近已获取数量
//@property (nonatomic, assign) NSUInteger nearByPageTotal;///<附近总页数

@property (nonatomic, assign) NSUInteger hotestCurrentPage;///<最热当前页
@property (nonatomic, assign) NSUInteger hotestPageSize;///<最热每页数量
@property (nonatomic, assign) NSUInteger hotestPageReadSize;///<附近已获取数量
//@property (nonatomic, assign) NSUInteger hotestPageTotal;///<最热总页数

@property (nonatomic, assign) NSUInteger lastestCurrentPage;///<最新当前页
@property (nonatomic, assign) NSUInteger lastestPageSize;///<最新每页数量
@property (nonatomic, assign) NSUInteger lastestPageReadSize;///<附近已获取数量
//@property (nonatomic, assign) NSUInteger lastestPageTotal;///<最新总页数

@end

#define TableViewNearByTag 100
#define TableViewHotestTag 101
#define TableViewLastestTag 102

@implementation ListViewController

- (void)dealloc
{
    NSLogSelfMethodName;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [TalkingData trackPageBegin:selfControllerName];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [TalkingData trackPageEnd:selfControllerName];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initData];
    [self initUserInterface];
    
    [self addLocServiceNotification];
}

- (void)initData {
//    self.searchResult = [NSMutableArray array];
    
    self.nearByUsers = [NSMutableArray array];
    self.hottestUsers = [NSMutableArray array];
    self.lastestUsers = [NSMutableArray array];
    

    self.nearByPageSize = 10;
    self.hotestPageSize = 10;
    self.lastestPageSize = 10;
    
    self.firstLocation = YES;
    self.firstRefreshHotest = YES;
    self.firstRefreshLastest = YES;
    self.firstRefreshNearBy = YES;
}

- (void)initUserInterface {

    [self.view addSubview:self.scrollView];
    [self.view addSubview:self.segment];
}

#pragma mark - UI

//- (UISearchBar *)searchBar
//{
//    if (!_searchBar) {
//        _searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 44)];
//        [_searchBar setPlaceholder:@"搜索"];
//        [_searchBar setBackgroundImage:nil];
//        [_searchBar setDelegate:self];
//        
//        [_searchBar setBackgroundImage:[UIImage imageWithColor:LgColor(235, 1)]];
//    }
//
//    return _searchBar;
//}

- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        
        [_scrollView setPagingEnabled:YES];
        [_scrollView setBounces:NO];
        [_scrollView setScrollEnabled:NO];
        [_scrollView setContentSize:CGSizeMake([UIScreen mainScreen].bounds.size.width * 3, [UIScreen mainScreen].bounds.size.height)];
        
        [_scrollView addSubview:self.tableViewNearBy];
        [_scrollView addSubview:self.tableViewHotest];
        [_scrollView addSubview:self.tableViewLastest];
        
        self.tableViewHotest.tag = TableViewHotestTag;
        self.tableViewLastest.tag = TableViewLastestTag;
        self.tableViewNearBy.tag = TableViewNearByTag;
    }
    return _scrollView;
}

- (UITableView *)tableViewNearBy {
    if (!_tableViewNearBy) {
        _tableViewNearBy = [[UITableView alloc] initWithFrame:[UIScreen mainScreen].bounds style:UITableViewStylePlain];
        [_tableViewNearBy setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        [_tableViewNearBy setBackgroundColor:LgColor(235, 1)];
        
        [_tableViewNearBy registerNibCellWithClass:[DaKaInfoTableViewCell class]];
        
        UIEdgeInsets ed = UIEdgeInsetsMake(35 + 64, 0, 49, 0);
        [_tableViewNearBy setScrollIndicatorInsets:ed];
        [_tableViewNearBy setContentInset:ed];
        
        [_tableViewNearBy setDelaysContentTouches:NO];
        
        //_tableView.tableHeaderView = self.searchBar;
        
        __weak typeof(self) weakSelf = self;
        MJRefreshAutoNormalFooter * footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
                [weakSelf getNearByUserMore];
        }];
        [footer setAutomaticallyHidden:YES];
        [footer setAutomaticallyChangeAlpha:YES];
        
        MJRefreshNormalHeader * header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
                [weakSelf getNearByUserReload];
        }];
        
        [header setTintColor:LgColorRGB(120, 120, 120, 1)];
        
        [footer setTitle:@"没有更多数据了..." forState:MJRefreshStateNoMoreData];
        [footer setTintColor:LgColorRGB(120, 120, 120, 1)];
        
        _tableViewNearBy.mj_footer = footer;
        _tableViewNearBy.mj_header = header;
        
        [_tableViewNearBy setDelegate:self];
        [_tableViewNearBy setDataSource:self];
    }
    return _tableViewNearBy;
}

- (UITableView *)tableViewLastest {
    if (!_tableViewLastest) {
        _tableViewLastest = [[UITableView alloc] initWithFrame:CGRectMake(LgScreenWidth*2, 0, LgScreenWidth, LgScreenHeigh) style:UITableViewStylePlain];
        [_tableViewLastest setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        [_tableViewLastest setBackgroundColor:LgColor(235, 1)];
        
        [_tableViewLastest registerNibCellWithClass:[DaKaInfoTableViewCell class]];
        
        UIEdgeInsets ed = UIEdgeInsetsMake(35 + 64, 0, 49, 0);
        [_tableViewLastest setScrollIndicatorInsets:ed];
        [_tableViewLastest setContentInset:ed];
        
        [_tableViewLastest setDelaysContentTouches:NO];
        
        //_tableView.tableHeaderView = self.searchBar;
        
        __weak typeof(self) weakSelf = self;
        MJRefreshAutoNormalFooter * footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
            [weakSelf getLastestUserMore];
        }];
        MJRefreshNormalHeader * header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            [weakSelf getLastestUserReload];
        }];
        
        [header setTintColor:LgColorRGB(120, 120, 120, 1)];
        
        [footer setTitle:@"没有更多数据了..." forState:MJRefreshStateNoMoreData];
        [footer setTintColor:LgColorRGB(120, 120, 120, 1)];
        
        _tableViewLastest.mj_footer = footer;
        _tableViewLastest.mj_header = header;
        
        [_tableViewLastest setDelegate:self];
        [_tableViewLastest setDataSource:self];
    }
    return _tableViewLastest;
}

- (UITableView *)tableViewHotest {
    if (!_tableViewHotest) {
        _tableViewHotest = [[UITableView alloc] initWithFrame:CGRectMake(LgScreenWidth, 0, LgScreenWidth, LgScreenHeigh) style:UITableViewStylePlain];
        [_tableViewHotest setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        [_tableViewHotest setBackgroundColor:LgColor(235, 1)];
        
        [_tableViewHotest registerNibCellWithClass:[DaKaInfoTableViewCell class]];
        
        UIEdgeInsets ed = UIEdgeInsetsMake(35 + 64, 0, 49, 0);
        [_tableViewHotest setScrollIndicatorInsets:ed];
        [_tableViewHotest setContentInset:ed];
        
        [_tableViewHotest setDelaysContentTouches:NO];
        
        //_tableView.tableHeaderView = self.searchBar;
        
        __weak typeof(self) weakSelf = self;
        MJRefreshAutoNormalFooter * footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
            [weakSelf getHotestUserMore];
        }];
        MJRefreshNormalHeader * header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            [weakSelf getHotestUserReload];
        }];
        
        [header setTintColor:LgColorRGB(120, 120, 120, 1)];
        
        [footer setTitle:@"没有更多数据了..." forState:MJRefreshStateNoMoreData];
        [footer setTintColor:LgColorRGB(120, 120, 120, 1)];
        
        _tableViewHotest.mj_footer = footer;
        _tableViewHotest.mj_header = header;
        
        [_tableViewHotest setDelegate:self];
        [_tableViewHotest setDataSource:self];
    }
    return _tableViewHotest;
}

- (PPiFlatSegmentedControl *)segment {
    if (!_segment) {
        _segment = [[PPiFlatSegmentedControl alloc] initWithFrame:CGRectMake(0, 64, [UIScreen mainScreen].bounds.size.width, 35)
                                                            items:@[
                                                    @{@"text":@"附近"},
                                                    @{@"text":@"最热"},
                                                    @{@"text":@"最新"},
//                                                      @{@"text":@"附近",@"icon":@"icon-facebook"},
//                                                      @{@"text":@"最热",@"icon":@"icon-linkedin"},
//                                                      @{@"text":@"最新",@"icon":@"icon-twitter"}
                                                      ]
                                                     iconPosition:IconPositionRight
                                                andSelectionBlock:^(NSUInteger segmentIndex) {
                                                    self.selectIndex = segmentIndex;
                                                    switch (segmentIndex) {
                                                        case 0:
                                                            [self showNearBy];
//                                                            [self getNearByUser];
                                                            break;
                                                        case 1:
                                                            [self showHottest];
//                                                            [self getHotestUser];
                                                            break;
                                                        case 2:
                                                            [self showLastest];
//                                                            [self getLastestUser];
                                                            break;
                                                            
                                                        default:
                                                            break;
                                                    }
                                                
                                                }];
        _segment.color=[UIColor colorWithRed:88.0f/255.0 green:88.0f/255.0 blue:88.0f/255.0 alpha:1];
//        _segment.borderWidth=0.5;
//        _segment.borderColor=[UIColor darkGrayColor];
        _segment.selectedColor=[UIColor orangeColor];
        _segment.textAttributes=@{NSFontAttributeName:[UIFont systemFontOfSize:13],
                                    NSForegroundColorAttributeName:[UIColor whiteColor]};
        _segment.selectedTextAttributes=@{NSFontAttributeName:[UIFont systemFontOfSize:13],
                                            NSForegroundColorAttributeName:[UIColor whiteColor]};
    }
    return _segment;
}

#pragma mark - 添加通知
///添加定位服务通知
- (void)addLocServiceNotification {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(firstGetNearUsers:) name:LGLocationUpdate object:nil];
}

#pragma mark - 定位服务通知
/**
 *用户位置更新后通知
 */
- (void)firstGetNearUsers:(NSNotification *)notification {
    if ([notification.object isKindOfClass:[BMKUserLocation class]] && self.firstLocation) {
        self.firstLocation = NO;
        [[NSNotificationCenter defaultCenter] removeObserver:self name:LGLocationUpdate object:nil];
        [self showNearBy];
    }
}


#pragma mark - 显示不同类型的数据
- (void)showNearBy {
    [self.scrollView scrollRectToVisible:CGRectMake(0, 0, LgScreenWidth, LgScreenHeigh) animated:YES];
    if (self.firstRefreshNearBy) {
        self.firstRefreshNearBy = NO;
        [self.tableViewNearBy.mj_header beginRefreshing];
    }
}
- (void)showLastest {
    [self.scrollView scrollRectToVisible:CGRectMake(LgScreenWidth*2, 0, LgScreenWidth, LgScreenHeigh) animated:YES];
    if (self.firstRefreshLastest) {
        self.firstRefreshLastest = NO;
        [self.tableViewLastest.mj_header beginRefreshing];
    }
}
- (void)showHottest {
    [self.scrollView scrollRectToVisible:CGRectMake(LgScreenWidth, 0, LgScreenWidth, LgScreenHeigh) animated:YES];
    if (self.firstRefreshHotest) {
        self.firstRefreshHotest = NO;
        [self.tableViewHotest.mj_header beginRefreshing];
    }
}

#pragma mark - UITableViewDataSource,UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (tableView.tag) {
        case TableViewNearByTag:
            return self.nearByUsers.count;
            break;
        case TableViewLastestTag:
            return self.lastestUsers.count;
            break;
        case TableViewHotestTag:
            return self.hottestUsers.count;
            break;
            
        default:
            return 0;
            break;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    DaKaInfoTableViewCell * cell = [tableView dequeueReusableCellWithClass:[DaKaInfoTableViewCell class]];
    
    LGDaKaInfo * dakaInfo = nil;
    
    switch (tableView.tag) {
        case TableViewNearByTag:
            dakaInfo = [self.nearByUsers objectAtIndex:indexPath.row];
            break;
        case TableViewLastestTag:
            dakaInfo = [self.lastestUsers objectAtIndex:indexPath.row];
            break;
        case TableViewHotestTag:
            dakaInfo = [self.hottestUsers objectAtIndex:indexPath.row];
            break;
            
        default:
            break;
    }
    
    cell.dakaInfo = dakaInfo;
    
    
    __weak typeof(self) weakSelf = self;
    
    [cell addAddFriendBlock:^{
        NSLog(@"点击了添加好友按键");
        [TDNetworkRequest requestWithAccount:dakaInfo.account
                                      action:AddFriend
                                     success:^(id responseObject)
         {
             [SVProgressHUD showSuccessWithStatus:@"添加好友成功！" duration:1.5 ];
             [[TDSingleton instance].userInfoModel.contacsListCache addObject:dakaInfo];
             NSMutableArray <LGDaKaInfo *> * blackList = [TDSingleton instance].userInfoModel.blackListCache;
             [[NSNotificationCenter defaultCenter] postNotificationName:TDUpdateFriends object:nil];
             
             for (LGDaKaInfo * userInfo in blackList) {
                 [blackList removeObject:userInfo];
                 [[NSNotificationCenter defaultCenter] postNotificationName:TDUpdateBlacklist object:nil];
                 break;
             }
         }];
    }];
    
    [cell addInviteBlock:^{
        NSLog(@"点击了邀约按键");
        
        if ([TDSingleton instance].userInfoModel.defaultPlayer.id != 0) {
            
            CreatAssignOrderViewController * orderVC = [[CreatAssignOrderViewController alloc] initWithDakaInfo:dakaInfo];
            
            [weakSelf.navigationController pushViewController:orderVC animated:YES];
        } else {
            [SVProgressHUD showErrorWithStatus:@"请绑定游戏账号后再来尝试"];
        }
    }];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    LGDaKaInfo * dakaInfo = nil;
    
    switch (tableView.tag) {
        case TableViewNearByTag:
            dakaInfo = [self.nearByUsers objectAtIndex:indexPath.row];
            break;
        case TableViewLastestTag:
            dakaInfo = [self.lastestUsers objectAtIndex:indexPath.row];
            break;
        case TableViewHotestTag:
            dakaInfo = [self.hottestUsers objectAtIndex:indexPath.row];
            break;
            
        default:
            return;
            break;
    }
    
    PeopleGameDetailViewController * nextVC = [[PeopleGameDetailViewController alloc] initWithDakaInfo:dakaInfo];
    [self.navigationController pushViewController:nextVC animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return DaKaInfoTableViewCellHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return DaKaInfoTableViewCellHeight;
}

#pragma mark - UISearchBarDelegate
//- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
//    [searchBar setShowsCancelButton:YES animated:YES];
//    return YES;
//}
//- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
//    [searchBar resignFirstResponder];
//    [searchBar setShowsCancelButton:NO animated:YES];
//    
//    searchBar.text = nil;
//    
//    switch (self.selectIndex) {
//        case 0:
//            self.dataSource = self.nearByUsers;
//            break;
//        case 1:
//            self.dataSource = self.lastestUsers;
//            break;
//        case 2:
//            self.dataSource = self.hottestUsers;
//            break;
//            
//        default:
//            break;
//    }
//    
//    [self.tableView reloadData];
//}

//- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
//    [searchBar resignFirstResponder];
//    [searchBar setShowsCancelButton:NO animated:YES];
//    
//    [self.searchResult removeAllObjects];
//    NSString * keyWord = searchBar.text;
//    
//    if (keyWord.length > 0) {
//        
//        switch (self.selectIndex) {
//            case 0:
//                self.dataSource = self.nearByUser;
//                break;
//            case 1:
//                self.dataSource = self.lastestUser;
//                break;
//            case 2:
//                self.dataSource = self.hottestUser;
//                break;
//                
//            default:
//                break;
//        }
//        
//        for (LGDaKaInfo * dakaInfo in self.dataSource) {
//            if ([dakaInfo.nickName containsString:keyWord] || [dakaInfo.gameRank containsString:keyWord] || [dakaInfo.gameServer containsString:keyWord]) {
//                [self.searchResult addObject:dakaInfo];
//            } else if ([keyWord containsString:@"男"] && dakaInfo.sex == 1) {
//                [self.searchResult addObject:dakaInfo];
//            } else if ([keyWord containsString:@"女"] && dakaInfo.sex == 0) {
//                [self.searchResult addObject:dakaInfo];
//            }
//        }
//    }
//    
//    self.dataSource = self.searchResult;
//    [self.tableView reloadData];
//}

#pragma mark - 获取云检索条件
///获取周边搜索参数
- (LGCloudNearbySearchInfo *)getCloudNearbySearchInfoWithPageIndex:(NSUInteger)pageIndex pageSize:(NSUInteger)pageSize {
    LGCloudNearbySearchInfo *cloudNearbySearch = [[LGCloudNearbySearchInfo alloc]init];
    cloudNearbySearch.pageIndex = pageIndex;
    cloudNearbySearch.pageSize = pageSize;
    
    CLLocationCoordinate2D currentPt = [LGLocationService sharedManager].userLocation.location.coordinate;
    
    cloudNearbySearch.location = [NSString stringWithFormat:@"%f,%f", currentPt.longitude, currentPt.latitude];
    cloudNearbySearch.radius = 10000000;
    cloudNearbySearch.sortby = @"distance:1";
    cloudNearbySearch.filter = @"role_type:1,2|state:0";
    
    return cloudNearbySearch;
}

///获取本地最热搜索参数
- (LGCloudLocalSearchInfo *)getHotestSearchInfoWithPageIndex:(NSUInteger)pageIndex pageSize:(NSUInteger)pageSize  {
    NSString * city = [TDSingleton instance].currentCityName;
    if (city.length <= 0) {
        city = @"全国";
    }
    
    LGCloudLocalSearchInfo *cloudLocalSearch = [[LGCloudLocalSearchInfo alloc]init];
    cloudLocalSearch.pageIndex = pageIndex;
    cloudLocalSearch.pageSize = pageSize;
    
    cloudLocalSearch.region = city;//@"成都市"
    //cloudLocalSearch.keyword = keyWord;//@"天府五街";
    
    cloudLocalSearch.sortby = @"playTotalCount:-1";//-1:降序 1:升序
    cloudLocalSearch.filter = @"role_type:1,2|state:0";
    
    return cloudLocalSearch;
}
///获取本地最新搜索参数
- (LGCloudLocalSearchInfo *)getLastestSearchInfoWithPageIndex:(NSUInteger)pageIndex pageSize:(NSUInteger)pageSize  {
    NSString * city = [TDSingleton instance].currentCityName;
    if (city.length <= 0) {
        city = @"全国";
    }
    
    LGCloudLocalSearchInfo *cloudLocalSearch = [[LGCloudLocalSearchInfo alloc]init];
    cloudLocalSearch.pageIndex = pageIndex;
    cloudLocalSearch.pageSize = pageSize;
    
    cloudLocalSearch.region = city;//@"成都市"
    //cloudLocalSearch.keyword = keyWord;//@"天府五街";
    
    cloudLocalSearch.sortby = @"playTotalCount:1";//-1:降序 1:升序
    cloudLocalSearch.filter = @"role_type:1,2|state:0";
    
    return cloudLocalSearch;
}

#pragma mark - 获取大咖数据

///降序处理
- (NSArray <LGDaKaInfo *> *)getDescendArrayFrom:(NSArray <LGDaKaInfo *>*)array {
    NSArray <LGDaKaInfo *> * sortArray = [array sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        LGDaKaInfo * one = (LGDaKaInfo*)obj1;
        LGDaKaInfo * two = (LGDaKaInfo*)obj2;
        if (one.playTotalCount > two.playTotalCount) {
            return NSOrderedAscending;
        } else if (one.playTotalCount == two.playTotalCount) {
            return NSOrderedSame;
        } else {
            return NSOrderedDescending;
        }
    }];
    
    return sortArray;
}

///升序处理
- (NSArray <LGDaKaInfo *> *)getAscendArrayFrom:(NSArray <LGDaKaInfo *>*)array {
    NSArray <LGDaKaInfo *> * sortArray = [array sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        LGDaKaInfo * one = (LGDaKaInfo*)obj1;
        LGDaKaInfo * two = (LGDaKaInfo*)obj2;
        if (one.playTotalCount > two.playTotalCount) {
            return NSOrderedDescending;
        } else if (one.playTotalCount == two.playTotalCount) {
            return NSOrderedSame;
        } else {
            return NSOrderedAscending;
        }
    }];
    
    return sortArray;
}

///刷新附近的大咖
- (void)getNearByUserReload {
    
    self.nearByCurrentPage = 0;
    
    __weak typeof(self) weakSelf = self;
    
    [LGCloudSearchService nearbySearchWithSearchParam:[self getCloudNearbySearchInfoWithPageIndex:self.nearByCurrentPage pageSize:self.nearByPageSize] success:^(NSArray<LGCloudPOIInfo *> *result, NSUInteger size, NSUInteger total) {
        
        weakSelf.nearByPageReadSize = size;
        if (weakSelf.nearByPageReadSize == total) {
            [weakSelf.tableViewNearBy.mj_footer endRefreshingWithNoMoreData];
        } else {
            [weakSelf.tableViewNearBy.mj_footer endRefreshing];
        }
        
        NSArray<LGCloudPOIInfo *> *dakas = result;
        NSMutableArray * accounts = [NSMutableArray array];
        
        for (LGCloudPOIInfo * daka in dakas) {
            NSString * account = daka.user_id;
            if ([account isEqualToString:[TDSingleton instance].userInfoModel.account]) {
                continue;
            }
            [accounts addObject:account];
        }
        
        if (accounts.count == 0) {
            [weakSelf.tableViewNearBy.mj_header endRefreshing];
            [weakSelf.tableViewNearBy.mj_footer endRefreshingWithNoMoreData];
            return ;
        }
        
        [LGDefineNetServer getUsersInfoWithAccounts:accounts success:^(id result) {
            if ([result isKindOfClass:[NSArray class]]) {
                NSArray <LGDaKaInfo *> * dakaInfos = [LGDaKaInfo dakaInofArrayWithDakaInfoHelpArray:result];
                
                for (LGDaKaInfo * dakaInfo in dakaInfos) {
                    for (LGCloudPOIInfo * daka in dakas) {
                        if ([daka.user_id isEqualToString:dakaInfo.account]) {
                            dakaInfo.distance = daka.distance;
                            break;
                        }
                    }
                }
                
                [weakSelf.nearByUsers removeAllObjects];
                [weakSelf.nearByUsers addObjectsFromArray:dakaInfos];
                [weakSelf.tableViewNearBy reloadData];
                
                [weakSelf.tableViewNearBy.mj_header endRefreshing];
            }
        } failure:^(NSString *msg) {
            NSLog(@"%@", msg);
            [SVProgressHUD showErrorWithStatus:msg];
            [weakSelf.tableViewNearBy.mj_header endRefreshing];
        }];
        
    } failure:^(NSString *msg) {
        [weakSelf.tableViewNearBy.mj_header endRefreshing];
        [weakSelf.tableViewNearBy.mj_footer endRefreshingWithNoMoreData];
        [SVProgressHUD showErrorWithStatus:msg];
    }];
}

///获取更多附近的大咖
- (void)getNearByUserMore {

    self.nearByCurrentPage++;
    __weak typeof(self) weakSelf = self;
    
    [LGCloudSearchService nearbySearchWithSearchParam:[self getCloudNearbySearchInfoWithPageIndex:self.nearByCurrentPage pageSize:self.nearByPageSize] success:^(NSArray<LGCloudPOIInfo *> *result, NSUInteger size, NSUInteger total) {
        
        weakSelf.nearByPageReadSize += size;
        if (weakSelf.nearByPageReadSize == total) {
            [weakSelf.tableViewNearBy.mj_footer endRefreshingWithNoMoreData];
        } else {
            [weakSelf.tableViewNearBy.mj_footer endRefreshing];
        }
        
        NSArray<LGCloudPOIInfo *> *dakas = result;
        NSMutableArray * accounts = [NSMutableArray array];
        
        for (LGCloudPOIInfo * daka in dakas) {
            NSString * account = daka.user_id;
            if ([account isEqualToString:[TDSingleton instance].userInfoModel.account]) {
                continue;
            }
            [accounts addObject:account];
        }
        
        if (accounts.count == 0) {
            [weakSelf.tableViewNearBy.mj_header endRefreshing];
            [weakSelf.tableViewNearBy.mj_footer endRefreshingWithNoMoreData];
            return ;
        }
        
        [LGDefineNetServer getUsersInfoWithAccounts:accounts success:^(id result) {
            if ([result isKindOfClass:[NSArray class]]) {
                NSArray <LGDaKaInfo *> * dakaInfos = [LGDaKaInfo dakaInofArrayWithDakaInfoHelpArray:result];
                
                for (LGDaKaInfo * dakaInfo in dakaInfos) {
                    for (LGCloudPOIInfo * daka in dakas) {
                        if ([daka.user_id isEqualToString:dakaInfo.account]) {
                            dakaInfo.distance = daka.distance;
                            break;
                        }
                    }
                }
                
                [weakSelf.nearByUsers addObjectsFromArray:dakaInfos];
                [weakSelf.tableViewNearBy reloadData];
                
                [weakSelf.tableViewNearBy.mj_header endRefreshing];
            }
        } failure:^(NSString *msg) {
            NSLog(@"%@", msg);
            [SVProgressHUD showErrorWithStatus:msg];
            [weakSelf.tableViewNearBy.mj_header endRefreshing];
        }];
        
    } failure:^(NSString *msg) {
        [weakSelf.tableViewNearBy.mj_header endRefreshing];
        [weakSelf.tableViewNearBy.mj_footer endRefreshingWithNoMoreData];
        [SVProgressHUD showErrorWithStatus:msg];
    }];
}

///刷新本地最新的大咖
- (void)getLastestUserReload {

    self.lastestCurrentPage = 0;
//    self.lastestPageReadSize = 0;
    
    __weak typeof(self) weakSelf = self;
    
    [LGCloudSearchService localSearchWithSearchParam:[self getLastestSearchInfoWithPageIndex:self.lastestCurrentPage pageSize:self.lastestPageSize] success:^(NSArray<LGCloudPOIInfo *> *result, NSUInteger size, NSUInteger total) {
        
        weakSelf.lastestPageReadSize = size;
        if (weakSelf.lastestPageReadSize == total) {
            [weakSelf.tableViewLastest.mj_footer endRefreshingWithNoMoreData];
        } else {
            [weakSelf.tableViewLastest.mj_footer endRefreshing];
        }
        
        NSArray <LGCloudPOIInfo *> * dakas = result;

        NSMutableArray * accounts = [NSMutableArray array];
        for (LGCloudPOIInfo * daka in dakas) {
            NSString * account = daka.user_id;
            if ([account isEqualToString:[TDSingleton instance].userInfoModel.account]) {
                continue;
            }
            [accounts addObject:account];
        }
        
        if (accounts.count == 0) {
            [weakSelf.tableViewLastest.mj_header endRefreshing];
            [weakSelf.tableViewLastest.mj_footer endRefreshingWithNoMoreData];
            return ;
        }
        
        [LGDefineNetServer getUsersInfoWithAccounts:accounts success:^(id result) {
            if ([result isKindOfClass:[NSArray class]]) {
                NSArray * dakaInfos = [LGDaKaInfo dakaInofArrayWithDakaInfoHelpArray:result];
                
                CLLocationCoordinate2D currentPt = [LGLocationService sharedManager].userLocation.location.coordinate;
                for (LGDaKaInfo * dakaInfo in dakaInfos) {
                    for (LGCloudPOIInfo * daka in dakas) {
                        if ([daka.user_id isEqualToString:dakaInfo.account]) {
                            
                            BMKMapPoint point1 = BMKMapPointForCoordinate(currentPt);
                            BMKMapPoint point2 = BMKMapPointForCoordinate(daka.locationValue);
                            CLLocationDistance distance = BMKMetersBetweenMapPoints(point1,point2);

                            dakaInfo.distance = distance;
                            break;
                        }
                    }
                }
                
                [weakSelf.lastestUsers removeAllObjects];
                [weakSelf.lastestUsers addObjectsFromArray:[self getAscendArrayFrom:dakaInfos]];
                [weakSelf.tableViewLastest reloadData];
                
                [weakSelf.tableViewLastest.mj_header endRefreshing];
            }
        } failure:^(NSString *msg) {
            NSLog(@"%@", msg);
            [SVProgressHUD showErrorWithStatus:msg];
            [weakSelf.tableViewLastest.mj_header endRefreshing];
        }];
        
    } failure:^(NSString *msg) {
        [weakSelf.tableViewLastest.mj_header endRefreshing];
        [weakSelf.tableViewLastest.mj_footer endRefreshingWithNoMoreData];
        [SVProgressHUD showErrorWithStatus:msg];
    }];
}
///获取更多本地最新的大咖
- (void)getLastestUserMore {
    
    self.lastestCurrentPage++;

    __weak typeof(self) weakSelf = self;
    
    [LGCloudSearchService localSearchWithSearchParam:[self getLastestSearchInfoWithPageIndex:self.lastestCurrentPage pageSize:self.lastestPageSize] success:^(NSArray<LGCloudPOIInfo *> *result, NSUInteger size, NSUInteger total) {
        
        weakSelf.lastestPageReadSize += size;
        if (weakSelf.lastestPageReadSize == total) {
            [weakSelf.tableViewLastest.mj_footer endRefreshingWithNoMoreData];
        } else {
            [weakSelf.tableViewLastest.mj_footer endRefreshing];
        }
        
        NSArray <LGCloudPOIInfo *> * dakas = result;
        
        NSMutableArray * accounts = [NSMutableArray array];
        for (LGCloudPOIInfo * daka in dakas) {
            NSString * account = daka.user_id;
            if ([account isEqualToString:[TDSingleton instance].userInfoModel.account]) {
                continue;
            }
            [accounts addObject:account];
        }
        
        if (accounts.count == 0) {
            [weakSelf.tableViewLastest.mj_header endRefreshing];
            [weakSelf.tableViewLastest.mj_footer endRefreshingWithNoMoreData];
            return ;
        }
        
        [LGDefineNetServer getUsersInfoWithAccounts:accounts success:^(id result) {
            if ([result isKindOfClass:[NSArray class]]) {
                NSArray * dakaInfos = [LGDaKaInfo dakaInofArrayWithDakaInfoHelpArray:result];
                
                CLLocationCoordinate2D currentPt = [LGLocationService sharedManager].userLocation.location.coordinate;
                for (LGDaKaInfo * dakaInfo in dakaInfos) {
                    for (LGCloudPOIInfo * daka in dakas) {
                        if ([daka.user_id isEqualToString:dakaInfo.account]) {
                            
                            BMKMapPoint point1 = BMKMapPointForCoordinate(currentPt);
                            BMKMapPoint point2 = BMKMapPointForCoordinate(daka.locationValue);
                            CLLocationDistance distance = BMKMetersBetweenMapPoints(point1,point2);
                            
                            dakaInfo.distance = distance;
                            break;
                        }
                    }
                }
                [weakSelf.lastestUsers addObjectsFromArray:[self getAscendArrayFrom:dakaInfos]];
                [weakSelf.tableViewLastest reloadData];
                [weakSelf.tableViewLastest.mj_header endRefreshing];
            }
        } failure:^(NSString *msg) {
            NSLog(@"%@", msg);
            [SVProgressHUD showErrorWithStatus:msg];
            [weakSelf.tableViewLastest.mj_header endRefreshing];
        }];
        
    } failure:^(NSString *msg) {
        [weakSelf.tableViewLastest.mj_header endRefreshing];
        [weakSelf.tableViewLastest.mj_footer endRefreshingWithNoMoreData];
        [SVProgressHUD showErrorWithStatus:msg];
    }];
}

///刷新本地最热的大咖
- (void)getHotestUserReload {

    self.hotestCurrentPage = 0;
//    self.hotestPageReadSize = 0;
    
    __weak typeof(self) weakSelf = self;
    
    [LGCloudSearchService localSearchWithSearchParam:[self getHotestSearchInfoWithPageIndex:self.hotestCurrentPage pageSize:self.hotestPageSize] success:^(NSArray<LGCloudPOIInfo *> *result, NSUInteger size, NSUInteger total) {
        
        weakSelf.hotestPageReadSize = size;
        if (weakSelf.hotestPageReadSize == total) {
            [weakSelf.tableViewHotest.mj_footer endRefreshingWithNoMoreData];
        } else {
            [weakSelf.tableViewHotest.mj_footer endRefreshing];
        }
        
        NSArray <LGCloudPOIInfo *> * dakas = result;
        NSMutableArray * accounts = [NSMutableArray array];
        
        for (LGCloudPOIInfo * daka in dakas) {
            NSString * account = daka.user_id;
            if ([account isEqualToString:[TDSingleton instance].userInfoModel.account]) {
                continue;
            }
            [accounts addObject:account];
        }
        
        if (accounts.count == 0) {
            [weakSelf.tableViewHotest.mj_footer endRefreshingWithNoMoreData];
            [weakSelf.tableViewHotest.mj_header endRefreshing];
            return ;
        }
        
        [LGDefineNetServer getUsersInfoWithAccounts:accounts success:^(id result) {
            if ([result isKindOfClass:[NSArray class]]) {
                NSArray * dakaInfos = [LGDaKaInfo dakaInofArrayWithDakaInfoHelpArray:result];
                
                CLLocationCoordinate2D currentPt = [LGLocationService sharedManager].userLocation.location.coordinate;
                for (LGDaKaInfo * dakaInfo in dakaInfos) {
                    for (LGCloudPOIInfo * daka in dakas) {
                        if ([daka.user_id isEqualToString:dakaInfo.account]) {
                            
                            BMKMapPoint point1 = BMKMapPointForCoordinate(currentPt);
                            BMKMapPoint point2 = BMKMapPointForCoordinate(daka.locationValue);
                            CLLocationDistance distance = BMKMetersBetweenMapPoints(point1,point2);
                            
                            dakaInfo.distance = distance;
                            break;
                        }
                    }
                }
                
                [weakSelf.hottestUsers removeAllObjects];
                [weakSelf.hottestUsers addObjectsFromArray:[self getDescendArrayFrom:dakaInfos]];
                [weakSelf.tableViewHotest reloadData];
                [weakSelf.tableViewHotest.mj_header endRefreshing];
            }
        } failure:^(NSString *msg) {
            NSLog(@"%@", msg);
            [SVProgressHUD showErrorWithStatus:msg];
            [weakSelf.tableViewHotest.mj_header endRefreshing];
        }];
    } failure:^(NSString *msg) {
        [weakSelf.tableViewHotest.mj_header endRefreshing];
        [weakSelf.tableViewHotest.mj_footer endRefreshingWithNoMoreData];
        [SVProgressHUD showErrorWithStatus:msg];
    }];
}

///获取本地最热的大咖
- (void)getHotestUserMore {
    
    self.hotestCurrentPage++;
    
    __weak typeof(self) weakSelf = self;
    
    [LGCloudSearchService localSearchWithSearchParam:[self getHotestSearchInfoWithPageIndex:self.hotestCurrentPage pageSize:self.hotestPageSize] success:^(NSArray<LGCloudPOIInfo *> *result, NSUInteger size, NSUInteger total) {
        
        weakSelf.hotestPageReadSize += size;
        if (weakSelf.hotestPageReadSize == total) {
            [weakSelf.tableViewHotest.mj_footer endRefreshingWithNoMoreData];
        } else {
            [weakSelf.tableViewHotest.mj_footer endRefreshing];
        }
        
        NSArray <LGCloudPOIInfo *> * dakas = result;
        NSMutableArray * accounts = [NSMutableArray array];
        
        for (LGCloudPOIInfo * daka in dakas) {
            NSString * account = daka.user_id;
            if ([account isEqualToString:[TDSingleton instance].userInfoModel.account]) {
                continue;
            }
            [accounts addObject:account];
        }
        
        if (accounts.count == 0) {
            [weakSelf.tableViewHotest.mj_footer endRefreshingWithNoMoreData];
            [weakSelf.tableViewHotest.mj_header endRefreshing];
            return ;
        }
        
        [LGDefineNetServer getUsersInfoWithAccounts:accounts success:^(id result) {
            if ([result isKindOfClass:[NSArray class]]) {
                NSArray * dakaInfos = [LGDaKaInfo dakaInofArrayWithDakaInfoHelpArray:result];
                
                CLLocationCoordinate2D currentPt = [LGLocationService sharedManager].userLocation.location.coordinate;
                for (LGDaKaInfo * dakaInfo in dakaInfos) {
                    for (LGCloudPOIInfo * daka in dakas) {
                        if ([daka.user_id isEqualToString:dakaInfo.account]) {
                            
                            BMKMapPoint point1 = BMKMapPointForCoordinate(currentPt);
                            BMKMapPoint point2 = BMKMapPointForCoordinate(daka.locationValue);
                            CLLocationDistance distance = BMKMetersBetweenMapPoints(point1,point2);
                            
                            dakaInfo.distance = distance;
                            break;
                        }
                    }
                }
                
                [weakSelf.hottestUsers addObjectsFromArray:[self getDescendArrayFrom:dakaInfos]];
                [weakSelf.tableViewHotest reloadData];
                [weakSelf.tableViewHotest.mj_header endRefreshing];
            }
        } failure:^(NSString *msg) {
            NSLog(@"%@", msg);
            [SVProgressHUD showErrorWithStatus:msg];
            
            [weakSelf.tableViewHotest.mj_header endRefreshing];
        }];
    } failure:^(NSString *msg) {
        [weakSelf.tableViewHotest.mj_header endRefreshing];
        [weakSelf.tableViewHotest.mj_footer endRefreshingWithNoMoreData];
        [SVProgressHUD showErrorWithStatus:msg];
    }];
}

@end
