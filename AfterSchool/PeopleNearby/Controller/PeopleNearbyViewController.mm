//
//  PeopleNearbyViewController.m
//  AfterSchool
//
//  Created by Teson Draw on 10/6/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "PeopleNearbyViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "AppDelegate.h"
#import "TalkingData.h"

//服务
#import "LGLocationService.h"
#import "LGCloudSearchService.h"
#import "LGDefineNetServer.h"
#import "LGGeoCodeService.h"

//百度地图头文件
#import <BaiduMapAPI_Location/BMKLocationComponent.h>
#import <BaiduMapAPI_Search/BMKSearchComponent.h>

#import "Common.h"
#import "MacrosDefinition.h"
#import "TDSingleton.h"

//自定义百度地图子类
#import "LGRadarNearbyInfo.h"
#import "LGPointAnnotation.h"
#import "LGAnnotationView.h"

//cell
#import "DaKaInfoTableViewCell.h"
#import "DakaInfoCollectionViewCell.h"

//下一级VC

//显示设置界面
#import "SettingWindowAnimation.h"

#import "UIImageView+WebCache.h"

typedef enum {
    PeopleNearByViewShowTypeStateMap,               ///< 地图模式
    PeopleNearByViewShowTypeStateList,              ///< 列表模式
} PeopleNearByViewShowTypeState;                    ///< 周围大咖信息界面当前状态

typedef enum {
    PeopleNearByViewAnimationStateAnimationstop,    ///< 动画停止
    PeopleNearByViewAnimationStateAnimating,        ///< 动画中
} PeopleNearByViewAnimationState;                   ///< 周围大咖信息界面动画状态

typedef enum {
    RadarSearchStop,                                ///< 搜索停止停止
    RadarSearching,                                 ///< 搜索中
} RadarSearchState;                                 ///< 雷达搜索状态状态

static CGFloat MainViewAnimationDuration = 1.0f;///< 显示模式转换动画时间

@interface PeopleNearbyViewController () <UIGestureRecognizerDelegate> {

    BOOL isMapView;
    
    PeopleNearByViewShowTypeState mainViewShowState;
    PeopleNearByViewAnimationState mainViewAnimationState;
    BOOL mainViewAnimationRestart;
}

@end

@interface PeopleNearbyViewController ()

@property (nonatomic, strong) MapViewController * mapVC;
@property (nonatomic, strong) ListViewController * listVC;

@end
@implementation PeopleNearbyViewController

#pragma mark - VCMethod VC自带的方法
- (void)dealloc {
    
    [self.mapVC stopUpdate];
    
    
    
    NSLog(@"PeopleNearbyViewController 附近控制器释放了");
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:LGSetCityName object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:LGSetUserIcon object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tabBarController.tabBar setHidden:NO];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [TalkingData trackPageBegin:selfControllerName];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [TalkingData trackPageEnd:selfControllerName];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    ///初始化数据
    [self initData];
    
    ///添加通知
    [self addUserIconChangeNotification];
    
    ///初始化界面
    [self initUserInterface];
}

#pragma mark - 自定义初始化方法
///初始化数据
- (void)initData {
    
    isMapView = YES;//默认显示地图界面
}

///初始化视图
- (void)initUserInterface {
    self.navigationController.navigationBarHidden = YES;
    
    [self.view addSubview:self.rootView];
    
    [self addChildViewController:self.listVC];
    [self addChildViewController:self.mapVC];
}

- (PeopleNearbyRootView *)rootView {
    if (!_rootView) {
        _rootView = [[PeopleNearbyRootView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];

        [_rootView.navigationBarView.rightButton addTarget:self action:@selector(typeTransformAction:) forControlEvents:UIControlEventTouchUpInside];
        
        [_rootView.navigationBarView.leftButton addTarget:self action:@selector(portraitClicked) forControlEvents:UIControlEventTouchUpInside];
        
        [_rootView.navigationBarView.leftButtonImageView sd_setImageWithURL:[NSURL URLWithString:[TDSingleton instance].userInfoModel.portraitURL] placeholderImage:[UIImage imageNamed:@"user_default_head"]];
        
//        [_rootView.mapVC.fubiaoView addTarget:self action:@selector(autoCall:) forControlEvents:UIControlEventTouchUpInside];

        
        //初始化默认显示界面
        mainViewAnimationState = PeopleNearByViewAnimationStateAnimationstop;
        if (isMapView) {
            mainViewShowState = PeopleNearByViewShowTypeStateMap;
            [self.rootView.transformView addSubview:self.listVC.view];
            [self.rootView.transformView addSubview:self.mapVC.view];
        } else {
            mainViewShowState = PeopleNearByViewShowTypeStateList;
            [self.rootView.transformView addSubview:self.mapVC.view];
            [self.rootView.transformView addSubview:self.listVC.view];
        }
        
        [self.mapVC.mapView viewWillAppear];
        
    }
    
    return _rootView;
}

- (ListViewController *)listVC {
    if (!_listVC) {
        _listVC = [[ListViewController alloc] init];
        [_listVC.view setFrame:[UIScreen mainScreen].bounds];
    }
    return _listVC;
}

- (MapViewController *)mapVC {
    if (!_mapVC) {
        _mapVC = [[MapViewController alloc] init];
        [_mapVC.view setFrame:[UIScreen mainScreen].bounds];
    }
    return _mapVC;
}

#pragma mark - 添加通知
///添加设置城市通知
//- (void)addSetCityNotification {
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setCityWithNotification:) name:LGSetCityName object:nil];
//}

///添加头像更新通知
- (void)addUserIconChangeNotification {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateUserIcon:) name:LGSetUserIcon object:nil];
}

#pragma mark - 操作流程方法
/**
 *  切换视图方式
 */
- (IBAction)typeTransformAction:(UIButton *)sender {
    
    //隐藏大咖摘要
    [self.mapVC deselectAnnotation];
    
    if (mainViewAnimationState == PeopleNearByViewAnimationStateAnimating) {//正在动画中
        
        mainViewAnimationRestart = YES;//设置动画重新开始标记
        
        [self.rootView.transformView.layer removeAnimationForKey:@"transition"];//移除正在执行的动画
    } else {
        //开始翻转动画
        [self startFilp];
    }
}

#pragma mark - 翻转动画
/**
 *  翻转动画
 *
 *  @param left 是否从左向右翻转
 */
- (CATransition *)flipAnimationDirection:(BOOL)left {
    
    CATransition * tran = [CATransition animation];
    tran.type = @"oglFlip";
    tran.subtype = left ? kCATransitionFromLeft : kCATransitionFromRight;
    
    tran.removedOnCompletion = YES;
    tran.duration = MainViewAnimationDuration;
    tran.delegate = self;
    
    return tran;
}

///开始翻转
- (void)startFilp {
    
    CATransition * transition;
    switch (mainViewShowState) {//确定动画切换方向
        case PeopleNearByViewShowTypeStateList:
            transition = [self flipAnimationDirection:YES];
            break;
        case PeopleNearByViewShowTypeStateMap:
            transition = [self flipAnimationDirection:NO];
            break;
            
        default:
            break;
    }
    [self.rootView.transformView.layer addAnimation:transition forKey:@"MainViewTransion"];
}

#pragma mark - 客户端创建模拟数据
///创建一组雷达搜索到的用户
- (NSArray<BMKRadarNearbyInfo *> *)customBMKRadarNearByResultInfoList {
    //    BMKRadarNearbyInfo * one = [[BMKRadarNearbyInfo alloc] init];
    //    ///用户id
    //    @property (nonatomic, strong) NSString* userId;
    //    ///地址坐标
    //    @property (nonatomic, assign) CLLocationCoordinate2D pt;
    //    ///距离
    //    @property (nonatomic, assign) NSUInteger distance;
    //    ///扩展信息
    //    @property (nonatomic, strong) NSString* extInfo;
    //    ///设备类型
    //    @property (nonatomic, strong) NSString* mobileType;
    //    ///设备系统
    //    @property (nonatomic, strong) NSString* osType;
    //    ///时间戳
    //    @property (nonatomic, assign) NSTimeInterval timeStamp;
    
    NSMutableArray * infoList = [NSMutableArray array];
    
    NSArray * userIds = @[@"13980557234",@"15196614939",@"15196614940",@"15196614941",@"15196614942"];
    NSArray * extInfos = @[@"扩展信息1",@"扩展信息2",@"扩展信息3",@"扩展信息4",@"扩展信息5"];
    NSArray * mobileTypes = @[@"iPhone",@"iPhone",@"Android",@"iPhone 6s",@"XiaoMi"];
    NSArray * osTypes = @[@"iOS8.1",@"iOS9.0",@"Android 5.1",@"iOS7",@"MIUI"];
    NSUInteger distances[] = {100,150,200,250,300};
    NSTimeInterval timeStamps[] = {30,50,209,40,37};
    
//    CGFloat width = self.rootView.mapView.bounds.size.width;
    CGFloat width = self.mapVC.mapView.bounds.size.width;
//    CGFloat height = self.rootView.mapView.bounds.size.height;
    CGFloat height = self.mapVC.mapView.bounds.size.height;
    
    
    CGPoint points[] = {
        {30,80},
        {40,height/2 -10},
        {width/2 -10,height/2 + 20},
        {width/2 + 100,height/2 - 100},
        {width/2 + 130,height/2 + 180}
    };
    
    int count = (int)userIds.count;
    
    for (int i = 0; i < count; i++) {
        BMKRadarNearbyInfo * info = [[BMKRadarNearbyInfo alloc] init];
        info.userId = userIds[i];
        info.extInfo = extInfos[i];
        info.mobileType = mobileTypes[i];
        info.osType = osTypes[i];
        info.distance = distances[i];
        info.timeStamp = timeStamps[i];
//        CLLocationCoordinate2D pt = [self.rootView.mapView convertPoint:points[i] toCoordinateFromView:self.rootView.mapView];
        CLLocationCoordinate2D pt = [self.mapVC.mapView convertPoint:points[i] toCoordinateFromView:self.mapVC.mapView];
        
        NSLog(@"%lf,%lf", pt.latitude, pt.longitude);
        info.pt =pt;
        
        [infoList addObject:info];
    }
    
    return infoList;
}

///通过雷达搜索到的用户创建一组包含大咖摘要信息的雷达用户
- (NSArray<LGRadarNearbyInfo *> *)customLGRadarNearByInfoFromBMKRadarNearByInfo:(NSArray<BMKRadarNearbyInfo *> *)array {
    NSMutableArray<LGRadarNearbyInfo *> * result = [NSMutableArray array];
    
    for (BMKRadarNearbyInfo * info in array) {
        LGRadarNearbyInfo * lgInfo = [LGRadarNearbyInfo lgRadarNearbyInfoWithBmkRadarNearbyInfo:info];
        lgInfo.dakaInfo = [self customDakaGameInfo];
        [result addObject:lgInfo];
    }
    
    return result;
}

///创建一个大咖摘要信息
- (LGDaKaInfo *)customDakaGameInfo {
    
    NSArray * headImages = @[@"head1",@"head2",@"head3",@"head4",@"head5",@"head6"];
    NSArray * gameNickNames = @[@"李刚",@"卓天成",@"刘江",@"大明湖畔的容嬷嬷",@"举头望明月",@"低头思姑娘",@"黄河之水天上来",@"恰似一江春水向东流"];
    NSUInteger starLevels[10] = {0,1,2,3,4,5,6,7,8,9};
    double distances[] = {12.23,344.13,849.34,84890.982};
    
    NSUInteger sexs[2] = {0,1};
    NSUInteger ages[4] = {10,20,40,58};
    NSArray * constellations = @[@"双鱼座",@"白羊座",@"金牛座",@"处女座",@"天蝎座",@"水瓶座"];
    NSArray * signatures = @[@"哥玩的不是游戏，哥玩的是豪情",@"赵日天再在此，不服来战",@"床前明月光，你喊没有女朋友",@"。。。。。。",@"良辰必有重谢",@"尼玛喊你回家吃放了。。。。。"];
    
    
    //    NSArray * gameLevels = @[@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10",
    //                             @"11",@"12",@"13",@"14",@"15",@"16",@"17",@"18",@"19",@"20",
    //                             @"21",@"22",@"23",@"24",@"25",@"26",@"27",@"28",@"29",@"30"];
    
    NSArray * gameServeNames = @[@"艾欧尼亚",@"祖安",@"诺克萨斯",@"班德尔城",@"皮尔特沃夫",@"战争学院",@"巨神峰",@"雷瑟守备",@"裁决之地",@"黑色玫瑰",@"暗影岛",@"钢铁烈阳",@"水晶之痕",@"均衡教派",@"影流",@"守望之海",@"征服之海",@"卡拉曼达",@"皮城警备",@"比尔吉沃特",@"德玛西亚",@"弗雷尔卓德",@"无畏先锋",@"恕瑞玛",@"扭曲丛林",@"巨龙之巢",@"教育网专区"];
    NSArray * gameRanks = @[@"青铜5",@"青铜4",@"青铜3",@"青铜2",@"青铜1",
                            @"黄金5",@"黄金4",@"黄金3",@"黄金2",@"黄金1",
                            @"白金5",@"白金4",@"白金3",@"白金2",@"白金1",
                            @"钻石5",@"钻石4",@"钻石3",@"钻石2",@"钻石1",
                            @"大师5",@"大师4",@"大师3",@"大师2",@"大师1",
                            @"最强王者"];
    
    NSUInteger gameNumbers[10] = {23,34,123,134,245,345,445,799,908,1258};
    NSUInteger gameFightings[5] = {1223,3554,5665,7878,14444};
    //头像
    //名称
    //信用等级
    //距离
    //性别
    //年龄
    //星座
    //个性签名
    
    
    //游戏段位
    //游戏大区
    //游戏局数
    //战斗力
    
    LGDaKaInfo * dakaInfo = [[LGDaKaInfo alloc] init];
    //dakaInfo.headImage = headImages[arc4random()%headImages.count];
    dakaInfo.headImageUrl = headImages[arc4random()%headImages.count];
    dakaInfo.nickName = gameNickNames[arc4random()%gameNickNames.count];
    dakaInfo.creditRating = starLevels[arc4random()%10];
    dakaInfo.distance = distances[arc4random()%4];
    dakaInfo.sex = sexs[arc4random()%2];
    dakaInfo.age = ages[arc4random()%4];
    dakaInfo.constellation = constellations[arc4random()%constellations.count];
    dakaInfo.signature = signatures[arc4random()%signatures.count];
    
    dakaInfo.gameRank = gameRanks[arc4random()%gameRanks.count];
    dakaInfo.gameServer = gameServeNames[arc4random()%gameServeNames.count];
    dakaInfo.gameNumber = gameNumbers[arc4random()%10];
    dakaInfo.gameFighting = gameFightings[arc4random()%5];
    
    return dakaInfo;
}

#pragma mark - AnimationDelegate 动画代理方法
///动画开始
- (void)animationDidStart:(CAAnimation *)anim {
    
    if ([anim isKindOfClass:[CATransition class]]) {
        mainViewAnimationState = PeopleNearByViewAnimationStateAnimating;//标记为正在动画中
        [self.rootView.transformView exchangeSubviewAtIndex:0 withSubviewAtIndex:1];
    }
}

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag {
    
    //翻转动画结束后执行
    if ([anim isKindOfClass:[CATransition class]]) {
        if (!flag) {//翻转异常停止
            //恢复原始状态
            [self.rootView.transformView exchangeSubviewAtIndex:0 withSubviewAtIndex:1];
            if (mainViewAnimationRestart) {//异常是由于要重新开始动画
                [self performSelector:@selector(startFilp) withObject:self afterDelay:0.000001];
            }
        } else {//动画正常完成
            mainViewAnimationRestart = NO;//重置 动画重新开始标记
            
            UIView * view = [self.rootView.transformView.subviews objectAtIndex:1];
            
            if (view == self.mapVC.view) {
                mainViewShowState = PeopleNearByViewShowTypeStateMap;
                
//                [self.rootView.navigationBarView.rightButton setBackgroundImage:[UIImage imageNamed:@"type_list"] forState:UIControlStateNormal];
                self.rootView.navigationBarView.rightButtonImageView.image = [UIImage imageNamed:@"type_list"];
            } else if (view == self.listVC.view) {
                mainViewShowState = PeopleNearByViewShowTypeStateList;
                
//                [self.rootView.navigationBarView.rightButton setBackgroundImage:[UIImage imageNamed:@"type_map"] forState:UIControlStateNormal];
                self.rootView.navigationBarView.rightButtonImageView.image = [UIImage imageNamed:@"type_map"];
            }
        }
        mainViewAnimationState = PeopleNearByViewAnimationStateAnimationstop;//标记为动画结束
    }
}
#pragma mark - 头像改变通知
- (void)didUpdateUserIcon:(NSNotification *)notifi {
    NSURL * url = [NSURL URLWithString:notifi.object];
    [self.rootView.navigationBarView.leftButtonImageView sd_setImageWithURL:url];
}

#pragma mark - 位置改变通知
//- (void)setCityWithNotification:(NSNotification *)notifi {
//    
//    NSString * city = [TDSingleton instance].cityName;
//    [LGGeoCodeService geocodeWithCity:city address:city success:^(id result) {
//        if ([result isKindOfClass:[LGGeoCodeResult class]]) {
//            LGGeoCodeResult * geoResult = (LGGeoCodeResult *)result;
//            
//            [self.rootView.mapVC.mapView setCenterCoordinate:geoResult.locationValue];
////            [self onClickLocalSearchWithCity:city keyWord:nil];
//        }
//    } failure:^{
//        
//    }];
//}

#pragma mark - 左滑
- (void)portraitClicked {
    
    /**
     *  swipe gesture
     */
    UISwipeGestureRecognizer *swipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(gestureAction:)];
    [swipeGesture setDirection:UISwipeGestureRecognizerDirectionLeft|UISwipeGestureRecognizerDirectionRight];
    
    /**
     *  tap gesture
     */
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gestureAction:)];
    tapGesture.numberOfTapsRequired = 1;
    
    /**
     *  屏蔽视图
     */
    UIView * maskView = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    [maskView setBackgroundColor:[UIColor clearColor]];
    maskView.tag = 1000;
    [maskView addGestureRecognizer:swipeGesture];
    [maskView addGestureRecognizer:tapGesture];

    [self.tabBarController.view addSubview:maskView];
    
    [SettingWindowAnimation showWithView:self.tabBarController.view];
}

- (void)gestureAction:(UIGestureRecognizer *)sender {
    
    [SettingWindowAnimation hideWithView:self.tabBarController.view];
    
    UIView * maskView = sender.view;
    
    [maskView removeGestureRecognizer:sender];
    [maskView removeFromSuperview];
}

@end









