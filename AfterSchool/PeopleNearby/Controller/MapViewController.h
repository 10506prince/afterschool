//
//  MapViewController.h
//  AfterSchool
//
//  Created by lg on 15/12/18.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <BaiduMapAPI_Map/BMKMapComponent.h>

//#define MapCenterToTop (([UIScreen mainScreen].bounds.size.height - 64 - 49 - 35) * 0.5)
#define MapCenterToTop ([UIScreen mainScreen].bounds.size.height/2)
//
//typedef void(^MapViewControllerActionBlock)(id object);

@interface MapViewController : UIViewController

@property (nonatomic, strong) BMKMapView * mapView;///<地图视图

///停止定时刷新地图数据
- (void)stopUpdate;

//取消选择大咖状态（其中调用了deselectDakaInfo）
- (void)deselectAnnotation;
@end
