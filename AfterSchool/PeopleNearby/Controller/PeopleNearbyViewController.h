//
//  PeopleNearbyViewController.h
//  AfterSchool
//
//  Created by Teson Draw on 10/6/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "LGViewController.h"
#import "PeopleNearbyRootView.h"

@interface PeopleNearbyViewController : LGViewController

@property (nonatomic, strong) PeopleNearbyRootView *rootView;

@end
