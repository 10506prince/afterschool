//
//  MapViewController.m
//  AfterSchool
//
//  Created by lg on 15/12/18.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "MapViewController.h"

#import "LGParallaxLayout.h"
#import "DakaInfoCollectionViewCell.h"
#import "LGBuoyView.h"
#import "LGPointAnnotation.h"
#import "SceneViewController.h"

#import "LGPointAnnotation.h"
#import "LGAnnotationView.h"
#import "LGRadarNearbyInfo.h"

#import "LGLocationService.h"
#import "LGGeoCodeService.h"
#import "LGDefineNetServer.h"
#import "LGCloudSearchService.h"

#import "TDSingleton.h"
#import "SceneViewController.h"
#import <BaiduMapAPI_Utils/BMKUtilsComponent.h>
#import "TalkingData.h"
#import "Common.h"
#import "SVProgressHUD.h"

#import "PeopleGameDetailViewController.h"

#import "CreatSceneModelOrderViewControllerOne.h"
#import "CreatSceneModelViewControllerTwo.h"
#import "TDNetworkRequest.h"
#import "CreatAssignOrderViewController.h"

static CGFloat DaKaInfoAnimationDuration = 0.3f;///< 地图界面大咖摘要显示隐藏动画时间

@interface MapViewController () <BMKMapViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout> {
}

@property (nonatomic, strong) NSArray <LGPointAnnotation *> *mapUsers;///< 周围搜索到的大咖缓存

@property (nonatomic, assign) BOOL mapViewLoadFinish;///<地图界面加载完成
@property (nonatomic, assign) BOOL isFirstGetLocation;//第一次定位 默认:YES(在第一次获取大咖信息后变为NO,确保能够获取到大咖数据)

//@property (nonatomic, strong) LGBuoyView * fubiaoView;
@property (nonatomic, strong) SceneViewController * sceneVC;
@property (nonatomic, strong) UICollectionView * dakaInfoCollectionView;
@property (nonatomic, strong) UIButton * backCurrentLocation;

@property (nonatomic, strong) NSTimer * timer;

@end

@implementation MapViewController

- (void)dealloc
{
    NSLogSelfMethodName;
    
    if (self.mapView) {
        [self.mapView viewWillDisappear];
        self.mapView.delegate = nil;
        self.mapView = nil;
    }
    
    if (self.timer) {
        [self.timer invalidate];
        self.timer = nil;
    }
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [TalkingData trackPageBegin:selfControllerName];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [TalkingData trackPageEnd:selfControllerName];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initData];
    [self initUserInterface];
}

- (void)initData {
    _mapViewLoadFinish = NO;
    _isFirstGetLocation = YES;
}

///开始定时刷新地图数据
- (void)startUpdate {
    [self.timer setFireDate:[NSDate distantPast]];
}
///停止定时刷新地图数据
- (void)stopUpdate {
    if (self.timer) {
        [self.timer invalidate];
    }
}
- (NSTimer *)timer {
    if (!_timer) {
        _timer = [NSTimer scheduledTimerWithTimeInterval:5 * 60 target:self selector:@selector(timerAction:) userInfo:nil repeats:YES];
        [_timer setFireDate:[NSDate distantFuture]];
    }
    return _timer;
}

- (void)initUserInterface {
    [self.view addSubview:self.mapView];
//    [self.view addSubview:self.fubiaoView];
    [self.view addSubview:self.backCurrentLocation];
    [self.view addSubview:self.dakaInfoCollectionView];
    [self.view addSubview:self.sceneVC.view];
}

///初始化情景
- (SceneViewController *)sceneVC {
    if (!_sceneVC) {
        _sceneVC = [[SceneViewController alloc] initWithNibName:@"SceneViewController" bundle:nil];
        [_sceneVC.view setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 64 + 30 + 10)];
        
        __weak typeof(self) weakSelf = self;
        ///情景模式响应
        [_sceneVC addSelectBlock:^(LGSceneInfo *scene) {
            if ([TDSingleton instance].userInfoModel.defaultPlayer.id != 0) {
                if (scene.sceneId == 5) {
                    CreatSceneModelViewControllerTwo * creatOrderVC = [[CreatSceneModelViewControllerTwo alloc] initFromSceneInfo:scene];
                    
                    [weakSelf.navigationController pushViewController:creatOrderVC animated:YES];
                } else {
                    CreatSceneModelOrderViewControllerOne * creatOrderVC = [[CreatSceneModelOrderViewControllerOne alloc] initFromSceneInfo:scene];
                    
                    [weakSelf.navigationController pushViewController:creatOrderVC animated:YES];
                }
            } else {
                [SVProgressHUD showErrorWithStatus:@"请绑定游戏账号后再来尝试"];
            }
        }];
        
        [self.view addSubview:_sceneVC.view];
        [self addChildViewController:_sceneVC];
    }
    return _sceneVC;
}

///初始化地图视图
- (BMKMapView *)mapView {
    if (_mapView == nil) {
        _mapView = [[BMKMapView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        
        //[_mapView setBuildingsEnabled:NO];//3d路况
        [_mapView setTrafficEnabled:NO];//路况
        [_mapView setBaiduHeatMapEnabled:NO];//热力图
        [_mapView setZoomEnabled:YES];//多点缩放
        [_mapView setZoomEnabledWithTap:NO];//点击缩放
        [_mapView setScrollEnabled:YES];//移动地图
        [_mapView setOverlookEnabled:NO];//仰俯视
        [_mapView setRotateEnabled:NO];//旋转
        [_mapView setShowMapScaleBar:NO];//比例尺
        [_mapView setChangeWithTouchPointCenterEnabled:NO];//以手势中心点为轴进行旋转和缩放
        
        //[_mapView setShowsUserLocation:YES];//显示定位图层
        [_mapView setIsSelectedAnnotationViewFront:YES];//总是让选中的标注显示在最前面
        
        [_mapView setUserTrackingMode:BMKUserTrackingModeNone];
        [_mapView setZoomLevel:14];//默认视图级别
        
        [_mapView setMapCenterToScreenPt:CGPointMake([UIScreen mainScreen].bounds.size.width/2, MapCenterToTop)];
        
        [_mapView setDelegate:self];
    }
    
    return _mapView;
}

///初始化返回当前位置按键
- (UIButton *)backCurrentLocation {
    if (!_backCurrentLocation) {
        _backCurrentLocation = [UIButton buttonWithType:UIButtonTypeCustom];
        [_backCurrentLocation setFrame:CGRectMake(10, [UIScreen mainScreen].bounds.size.height - 49 - 30 - 40, 40, 40)];
        [_backCurrentLocation setImage:[UIImage imageNamed:@"map_back_current_loc"] forState:UIControlStateNormal];
        [_backCurrentLocation addTarget:self action:@selector(backCurrentLocationAction:) forControlEvents:UIControlEventTouchUpInside];
        
//        [_backCurrentLocation.layer setBorderColor:[UIColor lightGrayColor].CGColor];
//        [_backCurrentLocation.layer setBorderWidth:1/[UIScreen mainScreen].scale];
//        
//        [_backCurrentLocation.layer setShadowColor:[UIColor blackColor].CGColor];
//        [_backCurrentLocation.layer setShadowOffset:CGSizeZero];
//        [_backCurrentLocation.layer setShadowOpacity:1];
        
        [_backCurrentLocation.layer setCornerRadius:3];
        [_backCurrentLocation.layer setMasksToBounds:YES];
    }
    return _backCurrentLocation;
}
//- (LGBuoyView *)fubiaoView {
//    if (!_fubiaoView) {
//        _fubiaoView = [[LGBuoyView alloc] initWithFrame:CGRectMake((self.mapView.bounds.size.width - LGBUOY_DEFAULT_WIDTH)/2, MapCenterToTop - LGBUOY_DEFAULT_HEIGHT - LGBUOY_EXPAND_HEIGHT, LGBUOY_DEFAULT_WIDTH, LGBUOY_DEFAULT_HEIGHT)];
//    }
//    return _fubiaoView;
//}

///初始化摘要显示视图
- (UICollectionView *)dakaInfoCollectionView {
    if (!_dakaInfoCollectionView) {
        CGFloat width = [UIScreen mainScreen].bounds.size.width;
        CGFloat height = DaKaInfoCollectionCellHeight;
        
        LGParallaxLayout * layout = [[LGParallaxLayout alloc] init];
        
        _dakaInfoCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, [UIScreen mainScreen].bounds.size.height, width, height) collectionViewLayout:layout];
        _dakaInfoCollectionView.showsHorizontalScrollIndicator = NO;
        _dakaInfoCollectionView.pagingEnabled = YES;
        _dakaInfoCollectionView.backgroundColor = [UIColor whiteColor];
        
        [_dakaInfoCollectionView registerNibCellWithClass:[DakaInfoCollectionViewCell class]];
        
        [_dakaInfoCollectionView setDelegate:self];
        [_dakaInfoCollectionView setDataSource:self];
    }
    
    return _dakaInfoCollectionView;
}

#pragma mark - 添加通知
///添加定位服务通知
- (void)addLocServiceNotification {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateUserLocation:) name:LGLocationUpdate object:nil];
}

#pragma mark - 定位服务通知
/**
 *用户位置更新后通知，添加定位通知在地图加载完成后
 */
- (void)didUpdateUserLocation:(NSNotification *)notification {
    
    if ([notification.object isKindOfClass:[BMKUserLocation class]]) {
        
        BMKUserLocation * userLocation = (BMKUserLocation *)notification.object;
        
        //百度地图实时更新我的位置
        [self.mapView updateLocationData:userLocation];
        
        CLLocationCoordinate2D myLoc = userLocation.location.coordinate;
        
        if (self.isFirstGetLocation && myLoc.latitude != 0 && myLoc.longitude != 0) {
            //回到我的位置
            [self.mapView setCenterCoordinate:myLoc animated:YES];
        }
    }
}


#pragma mark - 设置数据
///设置地图显示大咖
- (void)setMapUsers:(NSArray<LGPointAnnotation *> *)mapUsers {
    _mapUsers = mapUsers;
    
    [self.mapView removeAnnotations:self.mapView.annotations];
    [self.mapView addAnnotations:_mapUsers];
    [self.mapView showAnnotations:self.mapView.annotations animated:YES];
    
    [self.dakaInfoCollectionView reloadData];
}

#pragma mark - Action
///返回自己位置
- (IBAction)backCurrentLocationAction:(id)sender {
    [self.mapView setCenterCoordinate:[LGLocationService sharedManager].userLocation.location.coordinate animated:YES];
}
///定时刷新地图数据
- (IBAction)timerAction:(id)sender {
    NSLog(@"%@ %s",NSStringFromClass([self class]), __FUNCTION__);
    [self getLocalUser];
}

#pragma mark - Other
///显示自己位置
- (void)showMyLocation {

    self.mapView.showsUserLocation = NO;//先关闭显示的定位图层
    self.mapView.userTrackingMode = BMKUserTrackingModeNone;//设置定位的状态
    self.mapView.showsUserLocation = YES;//显示定位图层
    [[LGLocationService sharedManager] updateUserLocationService];
}

- (void)updateDakaInfo:(LGPointAnnotation *)info {
    
    NSInteger index = [self.mapUsers indexOfObject:info];
    
    if (index < self.mapUsers.count) {
        NSIndexPath * indexPath = [NSIndexPath indexPathForItem:index inSection:0];
        
        [self.dakaInfoCollectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionNone animated:YES];
    }
}

///显示大咖摘要信息
- (void)showDaKaInfoAnimated:(BOOL)animated {
    
    UIView * view = self.dakaInfoCollectionView;
    
    CGRect startFrame = view.frame;
    
    if (startFrame.origin.y != [UIScreen mainScreen].bounds.size.height) {//已经显示了
        return;
    }
    
    startFrame.origin.y -= view.bounds.size.height + 49;
    CGRect endFrame = startFrame;
    
    if (animated) {
        [UIView animateWithDuration:DaKaInfoAnimationDuration animations:^{
            [view setFrame:endFrame];
        }];
    } else {
        [view setFrame:endFrame];
    }
}

///隐藏大咖摘要信息
- (void)hideDakaInfoAnimated:(BOOL)animated {
    
    UIView * view = self.dakaInfoCollectionView;
    
    CGRect startFrame = view.frame;
    if (startFrame.origin.y == [UIScreen mainScreen].bounds.size.height) {//已经隐藏了
        return;
    }
    
    startFrame.origin.y = [UIScreen mainScreen].bounds.size.height;
    CGRect endFrame = startFrame;
    
    if (animated) {
        [UIView animateWithDuration:DaKaInfoAnimationDuration animations:^{
            [view setFrame:endFrame];
        }];
    } else {
        [view setFrame:endFrame];
    }
}

///取消选择大咖状态(点击地图空白处调用)
- (void)deselectDakaInfo {
    //浮标使能
//    [self.fubiaoView setEnable:YES];
    
    //隐藏大咖摘要信息界面
    [self hideDakaInfoAnimated:YES];
}

//取消选择大咖状态（其中调用了deselectDakaInfo）
- (void)deselectAnnotation {
    //隐藏大咖摘要信息界面
    
    for (LGPointAnnotation * annotation in self.mapView.annotations) {
        [self.mapView deselectAnnotation:annotation animated:NO];
    }
    [self deselectDakaInfo];
}


#pragma mark - BMKMapViewDelegate 地图代理
/**
 *地图初始化完毕时会调用此接口
 *@param mapview 地图View
 */
- (void)mapViewDidFinishLoading:(BMKMapView *)mapView {
    self.mapViewLoadFinish = YES;
    
    ///添加定位服务通知
    [self addLocServiceNotification];
    
    ///打开显示自己位置图层，并发送位置更新通知
    [self showMyLocation];
}

/**
 *根据anntation生成对应的View
 *@param mapView 地图View
 *@param annotation 指定的标注
 *@return 生成的标注View
 */
- (BMKAnnotationView *)mapView:(BMKMapView *)view viewForAnnotation:(id <BMKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[LGPointAnnotation class]]) {
        // 生成重用标示identifier
        static NSString *AnnotationViewID = @"FXHRadarMark";
        
        // 检查是否有重用的缓存
        LGAnnotationView * annotationView = (LGAnnotationView *)[view dequeueReusableAnnotationViewWithIdentifier:AnnotationViewID];
        
        // 缓存没有命中，自己构造一个，一般首次添加annotation代码会运行到此处
        if (annotationView == nil) {
            annotationView = [[LGAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:AnnotationViewID];
        }
        
        annotationView.annotation = annotation;
        return annotationView;
    }
    return nil;
}

/**
 *当选中一个annotation views时，调用此接口
 *@param mapView 地图View
 *@param views 选中的annotation views
 */
- (void)mapView:(BMKMapView *)mapView didSelectAnnotationView:(BMKAnnotationView *)view {
    
    if ([view isKindOfClass:[LGAnnotationView class]]) {
        LGAnnotationView * pinView = (LGAnnotationView *)view;
        //[pinView setSelected:YES animated:YES];
        //NSLog(@"%d", pinView.selected);
        
        //选中的标注移动到中心
        
        NSLog(@"%f,%f", pinView.annotation.coordinate.latitude, pinView.annotation.coordinate.longitude);
        [self.mapView setCenterCoordinate:pinView.annotation.coordinate animated:YES];
        
        //更新大咖摘要（collection 中的信息）
        [self updateDakaInfo:(LGPointAnnotation *)pinView.annotation];
        
        //显示大咖摘要
        [self showDaKaInfoAnimated:YES];
        
        //浮标禁用
//        [self.fubiaoView setEnable:NO];
    }
}

/**
 *当取消选中一个annotation views时，调用此接口
 *@param mapView 地图View
 *@param views 取消选中的annotation views
 */
- (void)mapView:(BMKMapView *)mapView didDeselectAnnotationView:(BMKAnnotationView *)view {
}

/**
 *点中底图空白处会回调此接口
 *@param mapview 地图View
 *@param coordinate 空白处坐标点的经纬度
 */
- (void)mapView:(BMKMapView *)mapView onClickedMapBlank:(CLLocationCoordinate2D)coordinate {
    [self deselectDakaInfo];
}

/**
 *地图区域改变完成后会调用此接口
 *@param mapview 地图View
 *@param animated 是否动画
 */
- (void)mapView:(BMKMapView *)mapView regionDidChangeAnimated:(BOOL)animated {
    
    if (_isFirstGetLocation) {///第一次定位，获取本地大咖
        //获取当前位置城市信息
        CLLocationCoordinate2D currentPt = [LGLocationService sharedManager].userLocation.location.coordinate;
        
        if (currentPt.latitude != 0 && currentPt.longitude != 0) {
            __weak typeof(self) weakSelf = self;
            
            [LGGeoCodeService reverseGeocodeWithPt:currentPt success:^(id result) {
                if ([result isKindOfClass:[LGReverseGeoCodeResult class]]) {
                    LGReverseGeoCodeResult * revGeoCode = (LGReverseGeoCodeResult *)result;
                    
                    //更新系统单例中当前城市名字
                    [TDSingleton instance].currentCityName = revGeoCode.addressComponent.city;
                    
//                    [weakSelf getLocalUser];
                    [weakSelf startUpdate];
                    
                    //向服务器报告自己位置信息
                    NSString * position = [NSString stringWithFormat:@"%@,%lf,%lf",revGeoCode.addressComponent.city,revGeoCode.location.lat,revGeoCode.location.lng];
                    [LGDefineNetServer reportUsersInfoPosition:position success:^(id result) {
                    } failure:^(NSString *msg) {
                        [SVProgressHUD showErrorWithStatus:msg duration:1];
                    }];
                }
            } failure:^(NSString *msg) {
            }];
            _isFirstGetLocation = NO;
        }
    }
}
#pragma mark - UICollectionDataSource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.mapUsers.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    DakaInfoCollectionViewCell * cell = [collectionView dequeueReusableCellWithClass:[DakaInfoCollectionViewCell class] forIndexPath:indexPath];

    LGPointAnnotation * pointAnnotation = [self.mapUsers objectAtIndex:indexPath.item];
    cell.dakaInfo = pointAnnotation.dakaInfo;

    
    __weak typeof(self) weakSelf = self;
    LGDaKaInfo * dakaInfo = pointAnnotation.dakaInfo;
    
    [cell addAddFriendBlock:^{
        NSLog(@"点击了添加好友按键");
        [TDNetworkRequest requestWithAccount:dakaInfo.account
                                      action:AddFriend
                                     success:^(id responseObject)
         {
             [SVProgressHUD showSuccessWithStatus:@"添加好友成功！" duration:1.5 ];
             [[TDSingleton instance].userInfoModel.contacsListCache addObject:dakaInfo];
             NSMutableArray <LGDaKaInfo *> * blackList = [TDSingleton instance].userInfoModel.blackListCache;
             [[NSNotificationCenter defaultCenter] postNotificationName:TDUpdateFriends object:nil];
             
             for (LGDaKaInfo * userInfo in blackList) {
                 [blackList removeObject:userInfo];
                 [[NSNotificationCenter defaultCenter] postNotificationName:TDUpdateBlacklist object:nil];
                 break;
             }
         }];
    }];
    
    [cell addInviteBlock:^{
        NSLog(@"点击了邀约按键");
        
        if ([TDSingleton instance].userInfoModel.defaultPlayer.id != 0) {
            
            CreatAssignOrderViewController * orderVC = [[CreatAssignOrderViewController alloc] initWithDakaInfo:dakaInfo];
            
            [weakSelf.navigationController pushViewController:orderVC animated:YES];
        } else {
            [SVProgressHUD showErrorWithStatus:@"请绑定游戏账号后再来尝试"];
        }
    }];
    
    return cell;
}
#pragma mark - UICollectionViewDelegateFlowLayout

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    LGPointAnnotation * pointAnnotation = [self.mapUsers objectAtIndex:indexPath.item];
    LGDaKaInfo * dakaInfo = pointAnnotation.dakaInfo;
    
    PeopleGameDetailViewController * nextVC = [[PeopleGameDetailViewController alloc] initWithDakaInfo:dakaInfo];
    [self.navigationController pushViewController:nextVC animated:YES];
    
    [self deselectAnnotation];
}


#pragma mark - 获取地图数据搜索条件
/////获取周边搜索参数
//- (LGCloudNearbySearchInfo *)getCloudNearbySearchInfo {
//    LGCloudNearbySearchInfo *cloudNearbySearch = [[LGCloudNearbySearchInfo alloc]init];
//    cloudNearbySearch.pageIndex = 0;
//    cloudNearbySearch.pageSize = 500;
//    
//    CLLocationCoordinate2D currentPt = [LGLocationService sharedManager].userLocation.location.coordinate;
//    
//    cloudNearbySearch.location = [NSString stringWithFormat:@"%f,%f", currentPt.longitude, currentPt.latitude];
//    cloudNearbySearch.radius = 10000;
//    cloudNearbySearch.sortby = @"distance:1";
////    cloudNearbySearch.filter = @"role_type:1|online_state:1";
//    cloudNearbySearch.filter = @"role_type:1|state:0";
//    
//    return cloudNearbySearch;
//}

///获取本地检索参数
- (LGCloudLocalSearchInfo *)getCloudLocalSearchInfoWithRegion:(NSString *)city keyWord:(NSString *)keyWord {
    LGCloudLocalSearchInfo *cloudLocalSearch = [[LGCloudLocalSearchInfo alloc]init];
    cloudLocalSearch.pageIndex = 0;
    cloudLocalSearch.pageSize = 500;
    
    cloudLocalSearch.region = city;//@"成都市"
    cloudLocalSearch.keyword = keyWord;//@"天府五街";
    cloudLocalSearch.filter = @"role_type:1,2|state:0";
    
    return cloudLocalSearch;
}
#pragma mark - 获取大咖数据
///获取大咖摘要
- (void)getDakaInfoWithCloudPoiResults:(NSArray<LGCloudPOIInfo *> *)poiInfos {
    
    NSMutableArray * accounts = [NSMutableArray array];
    
    for (LGCloudPOIInfo * poiInfo in poiInfos) {
        NSString * account = poiInfo.user_id;
        if ([account isEqualToString:[TDSingleton instance].userInfoModel.account]) {
            continue;
        }
        [accounts addObject:account];
    }
    
    if (accounts.count == 0) {
        return;
    }
    
    ///获取大咖摘要
    [LGDefineNetServer getUsersInfoWithAccounts:accounts success:^(NSArray<LGDaKaInfoHelp *> *result) {
        NSArray <LGDaKaInfo *> * dakaInfos = [LGDaKaInfo dakaInofArrayWithDakaInfoHelpArray:result];
        
        //创建标注信息
        NSMutableArray <LGPointAnnotation *> *annotations = [NSMutableArray array];
        CLLocationCoordinate2D currentPt = [LGLocationService sharedManager].userLocation.location.coordinate;
        for (LGDaKaInfo * dakaInfo in dakaInfos) {
            for (LGCloudPOIInfo * poiInfo in poiInfos) {
                if ([dakaInfo.account isEqualToString:poiInfo.user_id]) {
                    BMKMapPoint point1 = BMKMapPointForCoordinate(currentPt);
                    BMKMapPoint point2 = BMKMapPointForCoordinate(poiInfo.locationValue);
                    CLLocationDistance distance = BMKMetersBetweenMapPoints(point1,point2);
                    dakaInfo.distance = distance;
                    
                    LGPointAnnotation * annotation = [LGPointAnnotation lgpointAnnotationWithLGCloudPoiInfo:poiInfo];
                    annotation.dakaInfo = dakaInfo;
                    [annotations addObject:annotation];
                    
                    break;
                }
            }
        }
        self.mapUsers = annotations;
    } failure:^(NSString *msg) {
    }];
}

///获取附近的大咖
//- (void)getNearByUser {
//    LGCloudNearbySearchInfo * cloudNearbySearchInfo = [self getCloudNearbySearchInfo];
//    
//    __weak typeof(self) weakSelf = self;
//    [LGCloudSearchService nearbySearchWithSearchParam:cloudNearbySearchInfo success:^(NSArray<LGCloudPOIInfo *> *result, NSUInteger size, NSUInteger total) {
//        [weakSelf getDakaInfoWithCloudPoiResults:result];
//    } failure:^{
//    }];
//}


///获取本地的大咖
- (void)getLocalUser {
    
    NSString * city = [TDSingleton instance].currentCityName;
    if (city.length <= 0) {
        return;
    }
    
    LGCloudLocalSearchInfo *cloudLocalSearch = [self getCloudLocalSearchInfoWithRegion:city keyWord:@""];
    
    __weak typeof(self) weakSelf = self;
    [LGCloudSearchService localSearchWithSearchParam:cloudLocalSearch success:^(NSArray<LGCloudPOIInfo *> *result, NSUInteger size, NSUInteger total) {
        [weakSelf getDakaInfoWithCloudPoiResults:result];
    } failure:^(NSString *msg) {
    }];
}

//发起矩形云检索
//-(IBAction)onClickBoundSearch
//{
////    BMKCoordinateRegion region = [self.rootView.mapView region];
//    BMKCoordinateRegion region = [self.rootView.mapVC.mapView region];
//
//    LGCloudBoundSearchInfo *cloudBoundSearch = [[LGCloudBoundSearchInfo alloc]init];
//    cloudBoundSearch.pageIndex = 0;
//    cloudBoundSearch.pageSize = 50;
//
//    //cloudBoundSearch.bounds = @"116.30,36.20;118.30,40.20";
//    cloudBoundSearch.bounds = [NSString stringWithFormat:@"%.2f,%.2f;%.2f,%.2f", region.center.longitude - region.span.longitudeDelta/2, region.center.latitude - region.span.latitudeDelta/2, region.center.longitude + region.span.longitudeDelta/2, region.center.latitude + region.span.latitudeDelta/2];
//    cloudBoundSearch.keyword = @"";
//
//    //cloudBoundSearch.sortby = @"distance:1";//矩形检索结果没有距离
//    cloudBoundSearch.filter = @"role_type:1";
//    //NSLog(@"%@", cloudBoundSearch.bounds);
//
//    __weak typeof(self) weakSelf = self;
//
//    [LGCloudSearchService boundSearchWithSearchParam:cloudBoundSearch success:^(id result) {
//        [weakSelf getDakaInfoWithCloudPoiResults:result];
//    } failure:^{
//    }];
//}

@end
