//
//  LGSearchPoiTestViewController.h
//  AfterSchool
//
//  Created by lg on 15/11/4.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGViewController.h"
#import "AFHTTPSessionManager.h"
#import "AFNetworkActivityIndicatorManager.h"

//增、删、改、查
static NSString * writeOneData = @"http://api.map.baidu.com/geodata/v3/poi/create";
//static NSString * readOneData = @"";
static NSString * modifyOneData = @"http://api.map.baidu.com/geodata/v3/poi/update";
//static NSString * deleteOneData = @"http://api.map.baidu.com/geodata/v3/poi/delete";
//static NSString *

@interface LGSearchPoiTestViewController : LGViewController

@end
