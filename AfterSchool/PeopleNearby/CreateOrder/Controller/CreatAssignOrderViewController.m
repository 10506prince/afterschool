//
//  CreatAssignOrderViewController.m
//  AfterSchool
//
//  Created by lg on 16/1/5.
//  Copyright © 2016年 Teson Draw. All rights reserved.
//

#import "CreatAssignOrderViewController.h"
#import "LGButton.h"
#import "NavigationBarView.h"
#import "LGScrollView.h"
#import "OrderDisplayViewTwo.h"
#import "UIButton+SetBackgroundColor.h"
#import "Common.h"
#import "TalkingData.h"

#import "LGCloudSearchService.h"
#import "LGUserWallet.h"
#import "LGOrderInfo.h"
#import "LGDatePickerSelectViewController.h"
#import "LGPickerSelectViewController.h"
#import "LGSelectAddressViewController.h"
#import "LGPayViewController.h"
#import "TDSingleton.h"
#import "SVProgressHUD.h"
#import "LGCommonAddress.h"
#import "LGDefineNetServer.h"
#import "PushOrderViewController.h"
#import "LGUIImageComponent.h"


#define MODIFY_ADDRESS_TAG 101
#define RECHARGE_TAG 102



@interface CreatAssignOrderViewController ()

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *playAddressBtnHeight;

@property (nonatomic, strong) NavigationBarView * navigationBarView;

@property (nonatomic, weak) IBOutlet LGScrollView * scrollView;

@property (nonatomic, weak) IBOutlet OrderDisplayViewTwo * moneyView;///<支付金额构成 视图

@property (nonatomic, weak) IBOutlet LGButton * gameAccountBtn;///<约玩账号
@property (nonatomic, weak) IBOutlet LGButton * startTimeBtn;///<开始时间
@property (nonatomic, weak) IBOutlet LGButton * playTimeBtn;///<约玩时间
@property (nonatomic, weak) IBOutlet LGButton * playTypeBtn;///约玩方式
@property (nonatomic, weak) IBOutlet LGButton * playAddress;///<约玩地点

@property (nonatomic, weak) IBOutlet UIButton * creatOrderBtn;///<创建订单


@property (nonatomic, strong) LGDatePickerSelectViewController * startTimeSelectVC;///<开始时间选择器
@property (nonatomic, strong) LGPickerSelectViewController * pickerSelectVC;///<约玩时间选择器
@property (nonatomic, strong) LGPayViewController * payVC;


@property (nonatomic, strong) NSMutableArray<LGCommonAddress *> * commonAddress;///<常用地址
@property (nonatomic, strong) LGDaKaInfo * assignDakaInfo;

@property (strong) LGUserWallet * wallet;///<用户钱包状况(进入界面立即获取，加入KVO显示实时状态)
@property (nonatomic, copy) LGOrderInfo * currentOrderInfo;///<当前订单（本地当前订单状态）


@property (nonatomic, strong) NSArray <LGGamePlayerInfo *>* gamePlayeList;///<玩家绑定游戏账号列表
@property (nonatomic, strong) LGGamePlayerInfo * selectGamePlayer;///<已选择的账号，用来搜素符合条件的大咖


@end

@implementation CreatAssignOrderViewController

- (NavigationBarView *)navigationBarView {
    if (!_navigationBarView) {
        _navigationBarView = [[NavigationBarView alloc] initWithTitle:@"创建订单"
                                                           titleColor:nil
                                                      backgroundColor:nil
                                              leftButtonBgViewOriginX:0
                                                 leftButtonImageWidth:22
                                                 leftButtonTitleColor:[UIColor whiteColor]
                                                       leftButtonName:@"返回"
                                                  leftButtonImageName:@"navigation_bar_return_button_left_arrow"
                                                      rightButtonName:nil
                                                 rightButtonImageName:nil];
    }
    return _navigationBarView;
}

- (instancetype)init
{
    return nil;
}
- (instancetype)initWithDakaInfo:(LGDaKaInfo *)dakaInfo {
    self = [[CreatAssignOrderViewController alloc] initWithNibName:@"CreatAssignOrderViewController" bundle:nil];
    if (self) {
        self.assignDakaInfo = dakaInfo;
    }
    return self;
}

- (void)dealloc
{
    NSArray * orderkeys = [LGOrderInfo propertys];
    
    for (NSString * key in orderkeys) {
        [_currentOrderInfo removeObserver:self forKeyPath:key];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [TalkingData trackPageBegin:selfControllerName];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [TalkingData trackPageEnd:selfControllerName];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initData];
    [self initUserInterface];
}

- (void)initData {
    //常用地址
    _commonAddress = [NSMutableArray array];
    
    //钱包
    _wallet = [[LGUserWallet alloc] init];
    
    //本地当前订单
    _currentOrderInfo = [[LGOrderInfo alloc] init];
    _currentOrderInfo.masters = self.assignDakaInfo.account;
    
    NSArray * orderkeys = [LGOrderInfo propertys];
    for (NSString * key in orderkeys) {
        [_currentOrderInfo addObserver:self forKeyPath:key options:NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew context:nil];
    }
    
    ///获取钱包装态
    [self getMoneyInfoSuccess:^{
        [self initDefaultSetting];
    } failure:^{
    }];
    
    __weak typeof(self) weakSelf = self;
    ///获取常用地址
    if ([TDSingleton instance].userInfoModel.commonAddresses.count == 0) {
        [LGDefineNetServer getCommonAddressSuccess:^(NSArray<LGCommonAddress *> *addresses) {
            [TDSingleton instance].userInfoModel.commonAddresses = addresses;
            [weakSelf.commonAddress addObjectsFromArray:addresses];
        } failure:^(NSString *msg) {
            [SVProgressHUD showErrorWithStatus:msg];
        }];
    } else {
        [self.commonAddress addObjectsFromArray:[TDSingleton instance].userInfoModel.commonAddresses];
    }
    
    if ([TDSingleton instance].userInfoModel.gamePlayers.count == 0) {
        [LGDefineNetServer getRoleBindingsSuccess:^(NSArray<LGGamePlayerInfo *> *result, long long defaultAccountId) {
            [TDSingleton instance].userInfoModel.gamePlayers = result;
            weakSelf.gamePlayeList = result;
        } failure:^(NSString *msg) {
            [SVProgressHUD showErrorWithStatus:@"加载错误"];
        }];
    } else {
        self.gamePlayeList = [TDSingleton instance].userInfoModel.gamePlayers;
    }
}
- (void)initUserInterface {
    [self.view addSubview:self.navigationBarView];
    
    [self.navigationBarView.leftButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.scrollView setDelaysContentTouches:YES];
    [self.scrollView setBackgroundColor:[UIColor whiteColor]];
    
    [self.creatOrderBtn setBackgroundColor:[UIColor orangeColor] forState:UIControlStateNormal];
    [self.creatOrderBtn setBackgroundColor:[UIColor lightGrayColor] forState:UIControlStateDisabled];
    [self.creatOrderBtn.layer setCornerRadius:3];
    [self.creatOrderBtn.layer setMasksToBounds:YES];
    
    
    __weak typeof(self) weakSelf = self;
    [self.moneyView addPriceChangeBlock:^(float price) {
        [weakSelf setSpendMoney:price];
    }];
    
    //添加开始时间选择器
    LGDatePickerSelectViewController * startTimeSelectVC = [[LGDatePickerSelectViewController alloc] initWithNibName:@"LGDatePickerSelectViewController" bundle:nil];
    startTimeSelectVC.view.frame = [UIScreen mainScreen].bounds;
    [self addChildViewController:startTimeSelectVC];
    self.startTimeSelectVC = startTimeSelectVC;
    
    ///约玩时间选择器
    LGPickerSelectViewController * pickerSelectVC = [[LGPickerSelectViewController alloc] initWithNibName:@"LGPickerSelectViewController" bundle:nil];
    pickerSelectVC.view.frame = [UIScreen mainScreen].bounds;
    [self addChildViewController:pickerSelectVC];
    self.pickerSelectVC = pickerSelectVC;
    
    //充值
    LGPayViewController * payVC = [[LGPayViewController alloc] initWithNibName:@"LGPayViewController" bundle:nil];
    payVC.view.frame = [UIScreen mainScreen].bounds;
    [self addChildViewController:payVC];
    self.payVC = payVC;
}

- (void)initDefaultSetting {
    
    ///设置默认账号
    LGGamePlayer * gamePalyer = [TDSingleton instance].userInfoModel.defaultPlayer;
    [self setGameAccount:gamePalyer displayName:[NSString stringWithFormat:@"%@(%@)",gamePalyer.playerName,gamePalyer.serverName]];
    
    ///设置默认开始时间
    NSDate * date = [self.startTimeSelectVC defaultDate];
    [self setPlayStartTime:date];
    
    ///设置默认约玩时长
    [self setPlayTime:2 displayName:@"2小时"];
    
    ///设置默认约玩方式
    [self setPlayType:NO];
}


#pragma mark - Action 按键操作
- (void)backAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onLineOrOffLineAction:(UIButton *)sender {
    sender.selected = !sender.selected;
    
    [self setPlayType:sender.selected];
}

- (IBAction)creatOrderAction:(id)sender {
    
    __weak typeof(self) weakSelf = self;
    
    if (self.currentOrderInfo.offLine) {//线下，找附近
        if (self.currentOrderInfo.address.length == 0) {
            [SVProgressHUD showErrorWithStatus:@"请设置约玩地点"];
            return;
        } else {
            [weakSelf creatOrder];
        }
    } else {//线上，随便找
        [weakSelf creatOrder];
    }
}

///设置账户
- (IBAction)gameAccountAction:(UIButton *)sender {
    [self.view endEditing:YES];
    
    NSMutableArray <LGDataSource *> * gameList = [NSMutableArray array];
    for (LGGamePlayerInfo * gameInfo in self.gamePlayeList) {
        LGDataSource * dataSource = [[LGDataSource alloc] init];
        dataSource.name = [NSString stringWithFormat:@"%@(%@)", gameInfo.playerName,gameInfo.serverName];
        dataSource.value = gameInfo;
        
        [gameList addObject:dataSource];
    }
    
    self.pickerSelectVC.dataSource = gameList;
    
    __weak typeof(self) weakSelf = self;
    [self.pickerSelectVC addConfirmClickBlock:^(LGDataSource *data) {
        
        LGGamePlayerInfo * gameInfo = data.value;
        
        [weakSelf setGameAccount:gameInfo displayName:data.name];
        [weakSelf.pickerSelectVC.view removeFromSuperview];
    }];
    [self.pickerSelectVC showInView:self.view];
}

///设置开始时间
- (IBAction)setStartTimeAction:(id)sender {
    NSLog(@"设置开始时间");
    [self.view endEditing:YES];
    
    [self.startTimeSelectVC showInView:self.view];
    
    __weak typeof(self) weakSelf = self;
    
    [self.startTimeSelectVC addConfirmClickBlock:^(NSDate *date) {
        
        [weakSelf setPlayStartTime:date];
    }];
}

///设置约玩时间
- (IBAction)setPlayTimeAction:(id)sender {
    NSLog(@"设置约玩时间");
    [self.view endEditing:YES];
    
    NSMutableArray <LGDataSource *> * dataSourceArray = [NSMutableArray array];
    
    for (int i = 1; i <= 10; i++) {
        LGDataSource * dataSource = [[LGDataSource alloc] init];
        dataSource.name = [NSString stringWithFormat:@"%d小时", i];
        dataSource.value = [NSString stringWithFormat:@"%d", i];
        [dataSourceArray addObject:dataSource];
    }
    
    [self.pickerSelectVC setDataSource:dataSourceArray];
    [self.pickerSelectVC showInView:self.view];
    
    __weak typeof(self) weakSelf = self;
    
    [self.pickerSelectVC addConfirmClickBlock:^(LGDataSource *data) {
        
        
        int hour = [data.value intValue];
        [weakSelf setPlayTime:hour displayName:data.name];
        [weakSelf.pickerSelectVC.view removeFromSuperview];
    }];
    
    [self.pickerSelectVC addCancelClickBlock:^(LGDataSource *data) {
    }];
}

///设置约玩地点
- (IBAction)setAddressAction:(id)sender {
    NSLog(@"设置约玩地点");
    [self.view endEditing:YES];
    
    LGSelectAddressViewController * nextVC = [[LGSelectAddressViewController alloc] init];
    nextVC.commonAddress = _commonAddress;
    
    
    __weak typeof(self) weakSelf = self;
    
    nextVC.clickEnterActionBlock = ^(LGSelectAddressResult * address) {
        
        if (address == nil) {
            return;
        }
        
        NSString * addressName = address.name;///地址名称
        if (addressName.length > 0) {
            
            ///更新订单位置信息
            weakSelf.currentOrderInfo.address = addressName;
            weakSelf.currentOrderInfo.location = address.pt;
            
            ///更新设置地址按键
            [weakSelf.playAddress setDetail:addressName];
        }
    };
    
    [self.navigationController pushViewController:nextVC animated:YES];
}

#pragma mark - KVO
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    
    //1.改变创建订单按键状态（判断条件：开始时间，和陪玩时间）
    if ([object isKindOfClass:[LGOrderInfo class]]) {///订单状态改变
        LGOrderInfo * order = (LGOrderInfo *)object;
        
        ///判断金钱是否充足
        if (order.planStartTime && order.planKeepTime) {
            
            [self.creatOrderBtn setEnabled:YES];
        } else {
            [self.creatOrderBtn setEnabled:NO];
        }
    }
}

#pragma mark - UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == MODIFY_ADDRESS_TAG) {///更新地址
        LGCommonAddress * commonAddress = _commonAddress[buttonIndex];
        commonAddress.address = _currentOrderInfo.address;
        commonAddress.position = [NSString stringWithFormat:@"%f,%f", _currentOrderInfo.location.longitude, _currentOrderInfo.location.latitude];
        
        [LGDefineNetServer updateCommonAddress:commonAddress success:^(NSString *msg) {
        } failure:^(NSString *msg) {
        }];
        
    } else if (alertView.tag == RECHARGE_TAG) {///充值
        
        UITextField * textField = [alertView textFieldAtIndex:0];
        [textField resignFirstResponder];
        
        if (buttonIndex == 1) {
            double money = [textField.text doubleValue];
            [SVProgressHUD showWithStatus:@"正在充值..."];
            [LGDefineNetServer reChargeWithMoney:money tokenMoney:0 success:^(id result) {
                NSUInteger status = [[result objectForKey:@"result"] integerValue];
                if (status == 1) {
                    _wallet.money += money;
                    [SVProgressHUD dismissWithSuccess:@"充值成功"];
                } else {
                    NSString * msg = [result objectForKey:@"msg"];
                    NSLog(@"%@", msg);
                    [SVProgressHUD dismissWithError:msg];
                }
                
            } failure:^(NSString *msg) {
                [SVProgressHUD dismissWithError:msg];
            }];
        }
    }
}

#pragma mark - setSetting
///设置约玩账号
- (void)setGameAccount:(LGGamePlayerInfo *)gameAccount displayName:(NSString *)name {
    ///改变界面
    [self.gameAccountBtn setDetail:name];
    
//    if (gameAccount.id != [TDSingleton instance].userInfoModel.defaultPlayer.id) {
//        [LGDefineNetServer updateDefaultRoleBinding:gameAccount success:^(id result) {
//            [TDSingleton instance].userInfoModel.defaultPlayer = result;
//        } failure:^{
//        }];
//    }
    
    ///设置数据
    self.selectGamePlayer = gameAccount;
    self.currentOrderInfo.selectGameAccount = gameAccount;
}
///设置约玩开始时间
- (void)setPlayStartTime:(NSDate *)date {
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy年M月d日,H点"];
    NSString * str = [dateFormatter stringFromDate:date];
    
    NSTimeInterval time = [date timeIntervalSince1970];
    
    self.currentOrderInfo.planStartTime = time * 1000;
    [self.startTimeBtn setDetail:str];
}
///设置约玩时长
- (void)setPlayTime:(int)time displayName:(NSString *)name {
    [self.playTimeBtn setDetail:name];
    
    [self.currentOrderInfo setPlanKeepTime:time];
    
    double price = self.currentOrderInfo.offLine ? self.assignDakaInfo.priceOffline : self.assignDakaInfo.priceOnline;
    self.moneyView.advisePrice = price * self.currentOrderInfo.planKeepTime;
}
///设置约玩方式
- (void)setPlayType:(BOOL)offLine {
    
    self.currentOrderInfo.offLine = offLine;
    [self.playTypeBtn setDetail:offLine ? @"线下" : @"线上"];
    
    double price = offLine ? self.assignDakaInfo.priceOffline : self.assignDakaInfo.priceOnline;
    self.moneyView.advisePrice = price * self.currentOrderInfo.planKeepTime;
    
    self.playAddressBtnHeight.constant = offLine ? 44 : 0;
    [self.view setNeedsUpdateConstraints];
}
///设置约玩地点
- (void)setPlayAddress {
    
}

#pragma mark - Money
- (void)setSpendMoney:(double)money {
    
    _wallet.spendMoney = money;
    [self updateOrderInfo];
}

- (void)updateOrderInfo {
    _currentOrderInfo.money = _wallet.orderMoney;
    _currentOrderInfo.tokenMoney = _wallet.orderTokenMoney;
    _currentOrderInfo.rewardMoney = _wallet.orderRewardMoney;
    
    if (_wallet.balanceFull) {
        [self.creatOrderBtn setTitle:@"创建订单" forState:UIControlStateNormal];
    } else {
        [self.creatOrderBtn setTitle:@"充值并创建订单" forState:UIControlStateNormal];
    }
}

#pragma mark - NetWorking
///获取金钱信息
- (void)getMoneyInfoSuccess:(void(^)())success failure:(void(^)())failure {
    [LGDefineNetServer getWalletInfoSuccess:^(id result) {
        NSLog(@"获取金额成功 %@", result);
        _wallet.money = [[result objectForKey:@"money"] doubleValue];
        _wallet.tokenMoney = [[result objectForKey:@"tokenMoney"] doubleValue];
        success();
    } failure:^(NSString *msg) {
        [SVProgressHUD showErrorWithStatus:msg];
        failure();
    }];
}

///获取常用地址
- (void)getCommonAddressList {
    
    [LGDefineNetServer getCommonAddressSuccess:^(id result) {
        if ([result isKindOfClass:[NSArray class]]) {
            
            [_commonAddress removeAllObjects];
            [_commonAddress addObjectsFromArray:result];
        }
        
    } failure:^(NSString *msg) {
    }];
}
///创建订单
- (void)creatOrder {
    __weak typeof(self) weakSelf = self;
    
    if (self.wallet.isBalanceFull) {
        if ([_currentOrderInfo.masters isEqualToString:@""]) {

            PushOrderViewController * pushOrderVC = [[PushOrderViewController alloc] initWithOrder:nil orderModelType:OrderModelTypeMaster sceneInfo:nil];
            
            [self.navigationController pushViewController:pushOrderVC animated:YES];
        } else {
            [SVProgressHUD showWithStatus:@"创建订单中..." maskType:SVProgressHUDMaskTypeBlack];
            [LGDefineNetServer creatDirectivityOrder:self.currentOrderInfo success:^(id result) {
                NSLog(@"指定约订单创建成功");
                NSLog(@"creatOrderAction:%@", result);
                [SVProgressHUD dismiss];
                
                weakSelf.currentOrderInfo.id = [result[@"orderId"] longLongValue];
                weakSelf.currentOrderInfo.timeOut = [result[@"timeOut"] longLongValue];
                
                PushOrderViewController * pushOrderVC = [[PushOrderViewController alloc] initWithOrder:self.currentOrderInfo orderModelType:OrderModelTypeMaster sceneInfo:nil];
                
                [weakSelf.navigationController pushViewController:pushOrderVC animated:YES];
            } failure:^(NSString *msg) {
                NSLog(@"%@",msg);
                [SVProgressHUD dismissWithError:msg];
            }];
        }
    } else {
        ///充值后支付
        
        [self.payVC addFinishBlock:^(BOOL success) {
            if (success) {
                [weakSelf getMoneyInfoSuccess:^{
                    [weakSelf creatOrderAction:nil];
                } failure:^{
                }];
            }
        }];
        
        [weakSelf.payVC showInView:weakSelf.view money:weakSelf.wallet.orderNeedMoney];
        
        [weakSelf.payVC addFinishBlock:^(BOOL result) {
            if (result) {
                [SVProgressHUD showWithStatus:@"正在处理..."];
                [weakSelf getMoneyInfoSuccess:^{
                    [SVProgressHUD dismiss];
                    [weakSelf creatOrderAction:nil];
                } failure:^{
                    [SVProgressHUD dismiss];
                }];
            }
        }];
    }
}

- (void)getGameList {
    
    __weak typeof(self) weakSelf = self;
    
    [LGDefineNetServer getRoleBindingsSuccess:^(NSArray<LGGamePlayerInfo *> *result, long long defaultAccountId) {
        weakSelf.gamePlayeList = result;
    } failure:^(NSString *msg) {
    }];
}

@end
