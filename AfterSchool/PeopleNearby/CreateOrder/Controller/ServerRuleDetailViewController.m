//
//  ServerRuleDetailViewController.m
//  AfterSchool
//
//  Created by lg on 16/1/14.
//  Copyright © 2016年 Teson Draw. All rights reserved.
//

#import "ServerRuleDetailViewController.h"
#import "NavigationBarView.h"
#import "Common.h"

@interface ServerRuleDetailViewController ()

@property (nonatomic, strong) NavigationBarView * navigationBarView;

@property (nonatomic, strong) UITextView * textView;

@end

@implementation ServerRuleDetailViewController

- (void)dealloc
{
    NSLogSelfMethodName;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initData];
    [self initUserInterface];
}

- (void)initData {
    NSString *file = [[NSBundle mainBundle] pathForResource:@"PromotionServerRule" ofType:@"txt"];
    
    NSFileHandle *fileHandle=[NSFileHandle fileHandleForReadingAtPath:file];
    NSData *data = [fileHandle readDataToEndOfFile];
    NSString *str = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    [self.textView setText:str];
}
- (void)initUserInterface {
    
    [self.view addSubview:self.textView];
    [self.view addSubview:self.navigationBarView];
}

- (NavigationBarView *)navigationBarView {
    if (!_navigationBarView) {
        _navigationBarView = [[NavigationBarView alloc] initWithTitle:@"保晋级服务细则"
                                                           titleColor:[UIColor whiteColor]
                                                      backgroundColor:nil
                                                  leftButtonImageName:@"navigation_bar_return_button"
                                                 rightButtonImageName:nil];
        
        [_navigationBarView.leftButton addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _navigationBarView;
}

- (UITextView *)textView {
    if (!_textView) {
        _textView = [[UITextView alloc] initWithFrame:CGRectMake(0, 64, LgScreenWidth, LgScreenHeigh - 64)];
        _textView.editable = NO;
    }
    return _textView;
}

#pragma mark - Action
- (IBAction)backAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

@end
