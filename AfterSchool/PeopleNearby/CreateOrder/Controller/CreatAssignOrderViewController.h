//
//  CreatAssignOrderViewController.h
//  AfterSchool
//
//  Created by lg on 16/1/5.
//  Copyright © 2016年 Teson Draw. All rights reserved.
//

#import "LGViewController.h"
#import "LGDaKaInfo.h"

@interface CreatAssignOrderViewController : LGViewController

- (instancetype)init __deprecated_msg("使用“initWithDakaInfo”代替");
- (instancetype)initWithDakaInfo:(LGDaKaInfo *)dakaInfo;

@end
