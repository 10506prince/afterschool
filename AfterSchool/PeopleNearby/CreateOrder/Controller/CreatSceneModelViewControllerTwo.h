//
//  CreatSceneModelViewControllerTwo.h
//  AfterSchool
//
//  Created by lg on 15/12/25.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGViewController.h"
#import "LGSceneInfo.h"

@interface CreatSceneModelViewControllerTwo : LGViewController

- (instancetype)init __deprecated_msg("使用“initFromSceneModel”代替");
- (instancetype)initFromSceneInfo:(LGSceneInfo *)sceneInfo;

@end
