//
//  CreatSceneModelOrderViewControllerOne.m
//  AfterSchool
//
//  Created by lg on 15/12/24.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "CreatSceneModelOrderViewControllerOne.h"
#import "Common.h"
#import "LGDaKaInfo.h"
#import "TalkingData.h"
#import "SVProgressHUD.h"
#import "CreatSceneModelOrderRootView.h"

//数据模型
#import "LGOrderInfo.h"
#import "LGUserWallet.h"
#import "TDSingleton.h"

//服务
#import "LGDefineNetServer.h"
#import "LGCloudSearchService.h"

#import "LGPickerSelectViewController.h"
#import "LGDatePickerSelectViewController.h"
#import "LGPayViewController.h"
#import "LGSelectAddressViewController.h"

#import "PushOrderViewController.h"

#define MODIFY_ADDRESS_TAG 101
#define RECHARGE_TAG 102

@interface CreatSceneModelOrderViewControllerOne () <UIAlertViewDelegate> {
    
    LGSceneInfo * _currentSceneInfo;///<当前情景模式
    
    NSMutableArray <LGCommonAddress *> * _commonAddress;///<常用地址
}

@property (nonatomic, strong) LGSceneInfo * currentSceneInfo;///<当前情景模式
@property (nonatomic, strong) NSMutableArray <LGCommonAddress *> * commonAddress;
@property (nonatomic, strong) NSArray <LGGamePlayerInfo *>* gamePlayeList;///<玩家绑定游戏账号列表

@property (strong) LGUserWallet * wallet;///<用户钱包状况(进入界面立即获取，加入KVO显示实时状态)
@property (nonatomic, strong) LGGamePlayerInfo * selectGamePlayer;///<已选择的账号，用来搜素符合条件的大咖

@property (nonatomic, copy) LGOrderInfo * currentOrderInfo;///<当前订单（本地当前订单状态）


#pragma mark - UI
@property (nonatomic, strong) CreatSceneModelOrderRootView * rootView;

@property (nonatomic, strong) LGDatePickerSelectViewController * startTimeSelectVC;///<开始时间选择器
@property (nonatomic, strong) LGPickerSelectViewController * pickerSelectVC;///<约玩时间选择器
@property (nonatomic, strong) LGPayViewController * payVC;


@end

@implementation CreatSceneModelOrderViewControllerOne

- (void)dealloc
{
    NSArray * orderkeys = [LGOrderInfo propertys];
    
    for (NSString * key in orderkeys) {
        [_currentOrderInfo removeObserver:self forKeyPath:key];
    }
}

- (instancetype)init
{
    NSAssert(0, @"此方法已废除，使用“initFromSceneModel”代替");
    return nil;
}

- (instancetype)initFromSceneInfo:(LGSceneInfo *)sceneInfo {
    self = [super init];
    if (self) {
        self.currentSceneInfo = sceneInfo;
        self.title = sceneInfo.name;
    }
    return self;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [TalkingData trackPageBegin:selfControllerName];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    [TalkingData trackPageEnd:selfControllerName];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initData];
    [self initUserInterface];
}

- (void)initData {
    
    //常用地址
    _commonAddress = [NSMutableArray array];
    
    //钱包
    _wallet = [[LGUserWallet alloc] init];

    
    //本地当前订单
    _currentOrderInfo = [[LGOrderInfo alloc] init];
    _currentOrderInfo.info = self.currentSceneInfo.name;
    _currentOrderInfo.sceneId = self.currentSceneInfo.sceneId;
    
    NSArray * orderkeys = [LGOrderInfo propertys];
    for (NSString * key in orderkeys) {
        [_currentOrderInfo addObserver:self forKeyPath:key options:NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew context:nil];
    }
    
    __weak typeof(self) weakSelf = self;
    
    ///获取钱包装态
    [LGDefineNetServer getWalletInfoSuccess:^(id result) {
        NSLog(@"获取金额成功 %@", result);
        weakSelf.wallet.money = [[result objectForKey:@"money"] doubleValue];
        weakSelf.wallet.tokenMoney = [[result objectForKey:@"tokenMoney"] doubleValue];
        [weakSelf initDefaultSetting];
    } failure:^(NSString *msg) {
        [SVProgressHUD showErrorWithStatus:msg];
    }];
    
    ///获取常用地址
    if ([TDSingleton instance].userInfoModel.commonAddresses.count == 0) {
        [LGDefineNetServer getCommonAddressSuccess:^(NSArray<LGCommonAddress *> *addresses) {
            [TDSingleton instance].userInfoModel.commonAddresses = addresses;
            [weakSelf.commonAddress addObjectsFromArray:addresses];
            [SVProgressHUD dismiss];
        } failure:^(NSString *msg) {
            [SVProgressHUD showErrorWithStatus:msg];
        }];
    } else {
        [self.commonAddress addObjectsFromArray:[TDSingleton instance].userInfoModel.commonAddresses];
    }
    
    ///获取绑定角色
    if ([TDSingleton instance].userInfoModel.gamePlayers.count == 0) {
        [LGDefineNetServer getRoleBindingsSuccess:^(NSArray<LGGamePlayerInfo *> *result, long long defaultAccountId) {
            [TDSingleton instance].userInfoModel.gamePlayers = result;
            weakSelf.gamePlayeList = result;
        } failure:^(NSString *msg) {
            [SVProgressHUD showErrorWithStatus:@"加载错误"];
        }];
    } else {
        self.gamePlayeList = [TDSingleton instance].userInfoModel.gamePlayers;
    }
}

- (void)initUserInterface {
    
    //添加根视图
    [self.view addSubview:self.rootView];
    
    //添加开始时间选择器
    LGDatePickerSelectViewController * startTimeSelectVC = [[LGDatePickerSelectViewController alloc] initWithNibName:@"LGDatePickerSelectViewController" bundle:nil];
    startTimeSelectVC.view.frame = [UIScreen mainScreen].bounds;
    [self addChildViewController:startTimeSelectVC];
    self.startTimeSelectVC = startTimeSelectVC;
    
    ///约玩时间选择器
    LGPickerSelectViewController * pickerSelectVC = [[LGPickerSelectViewController alloc] initWithNibName:@"LGPickerSelectViewController" bundle:nil];
    pickerSelectVC.view.frame = [UIScreen mainScreen].bounds;
    [self addChildViewController:pickerSelectVC];
    self.pickerSelectVC = pickerSelectVC;
    
    //充值
    LGPayViewController * payVC = [[LGPayViewController alloc] initWithNibName:@"LGPayViewController" bundle:nil];
    payVC.view.frame = [UIScreen mainScreen].bounds;
    [self addChildViewController:payVC];
    self.payVC = payVC;
}

- (CreatSceneModelOrderRootView *)rootView {
    if (!_rootView) {
        
        _rootView = [CreatSceneModelOrderRootView viewFromNibWithoutOwner];
        
        [_rootView.navigationBarView.titleLabel setText:self.title];
        
        ///添加返回操作
        [_rootView.navigationBarView.leftButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        
        [_rootView.gameAccount addTarget:self action:@selector(gameAccountAction:) forControlEvents:UIControlEventTouchUpInside];
        
        ///添加设定开始时间操作
        [_rootView.startTimeBtn addTarget:self action:@selector(setStartTimeAction:) forControlEvents:UIControlEventTouchUpInside];
        
        ///添加设定约玩时间操作
        [_rootView.playTimeBtn addTarget:self action:@selector(setPlayTimeAction:) forControlEvents:UIControlEventTouchUpInside];
        
        ///添加设置线上线下操作
        [_rootView.playType addTarget:self action:@selector(onLineOrOffLineAction:) forControlEvents:UIControlEventTouchUpInside];
        
        ///添加设置地址操作
        [_rootView.playAddress addTarget:self action:@selector(setAddressAction:) forControlEvents:UIControlEventTouchUpInside];
        
        ///添加创建订单操作
        [_rootView.creatOrderBtn addTarget:self action:@selector(creatOrderAction:) forControlEvents:UIControlEventTouchUpInside];
        
        __weak typeof(self) weakSelf = self;
        [_rootView.moneyView addPriceChangeBlock:^(float price) {
            [weakSelf setSpendMoney:floorf(price)];
        }];
    }
    
    return _rootView;
}

- (void)initDefaultSetting {
    
    ///设置默认账号
    LGGamePlayer * gamePalyer = [TDSingleton instance].userInfoModel.defaultPlayer;
    [self setGameAccount:gamePalyer displayName:[NSString stringWithFormat:@"%@(%@)",gamePalyer.playerName,gamePalyer.serverName]];
    
    ///设置默认开始时间
    NSDate * date = [self.startTimeSelectVC defaultDate];
    [self setPlayStartTime:date];
    
    ///设置默认约玩时长
    [self setPlayTime:2 displayName:@"2小时"];
    
    ///设置默认约玩方式
    [self setPlayType:NO];
}


#pragma mark - Action 按键操作
- (void)backAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onLineOrOffLineAction:(UIButton *)sender {
    sender.selected = !sender.selected;
    
    [self setPlayType:sender.selected];
}

- (IBAction)creatOrderAction:(id)sender {
    [self.view endEditing:YES];
    
    __weak typeof(self) weakSelf = self;
    
    if (self.currentOrderInfo.offLine) {//线下，找附近
        if (self.currentOrderInfo.address.length == 0) {
            [SVProgressHUD showErrorWithStatus:@"请设置约玩地点"];
            return;
        } else {
            
            [LGCloudSearchService nearbySearchWithSearchParam:[self getCloudNearbySearchInfo] success:^(NSArray<LGCloudPOIInfo *> *result, NSUInteger size, NSUInteger total) {
                NSMutableString * accounts = [NSMutableString stringWithFormat:@""];
                
                for (LGCloudPOIInfo * daka in result) {
                    NSString * str = daka.user_id;
                    if ([str isEqualToString:[TDSingleton instance].userInfoModel.account]) {
                        continue;
                    }
                    [accounts appendString:str];
                    [accounts appendString:@","];
                }
                weakSelf.currentOrderInfo.masters = accounts;
                
                [weakSelf creatOrder];
            } failure:^(NSString *msg) {
                [SVProgressHUD showErrorWithStatus:msg];
            }];
        }
    } else {//线上，随便找
        
        [LGCloudSearchService localSearchWithSearchParam:[self getCloudLocalSearchInfo] success:^(NSArray<LGCloudPOIInfo *> *result, NSUInteger size, NSUInteger total) {
            NSMutableString * accounts = [NSMutableString stringWithFormat:@""];
            
            for (LGCloudPOIInfo * daka in result) {
                NSString * str = daka.user_id;
                if ([str isEqualToString:[TDSingleton instance].userInfoModel.account]) {
                    continue;
                }
                [accounts appendString:str];
                [accounts appendString:@","];
            }
            
            weakSelf.currentOrderInfo.masters = accounts;
            
            [weakSelf creatOrder];
        } failure:^(NSString *msg) {
            [SVProgressHUD showErrorWithStatus:msg];
        }];
    }
}

///设置账户
- (IBAction)gameAccountAction:(UIButton *)sender {
    [self.view endEditing:YES];
    
    NSMutableArray <LGDataSource *> * gameList = [NSMutableArray array];
    for (LGGamePlayerInfo * gameInfo in self.gamePlayeList) {
        LGDataSource * dataSource = [[LGDataSource alloc] init];
        dataSource.name = [NSString stringWithFormat:@"%@(%@)", gameInfo.playerName,gameInfo.serverName];
        dataSource.value = gameInfo;
        
        [gameList addObject:dataSource];
    }
    
    self.pickerSelectVC.dataSource = gameList;
    
    __weak typeof(self) weakSelf = self;
    [self.pickerSelectVC addConfirmClickBlock:^(LGDataSource *data) {
        
        LGGamePlayerInfo * gameInfo = data.value;
        
        [weakSelf setGameAccount:gameInfo displayName:data.name];
        [weakSelf.pickerSelectVC.view removeFromSuperview];
    }];
    [self.pickerSelectVC showInView:self.view];
}

///设置开始时间
- (IBAction)setStartTimeAction:(id)sender {
    NSLog(@"设置开始时间");
    [self.view endEditing:YES];
    
    [self.startTimeSelectVC showInView:self.view];
    
    __weak typeof(self) weakSelf = self;
    
    [self.startTimeSelectVC addConfirmClickBlock:^(NSDate *date) {
        
        [weakSelf setPlayStartTime:date];
    }];
}

///设置约玩时间
- (IBAction)setPlayTimeAction:(id)sender {
    NSLog(@"设置约玩时间");
    [self.view endEditing:YES];
    
    NSMutableArray <LGDataSource *> * dataSourceArray = [NSMutableArray array];
    
    for (int i = 1; i <= 10; i++) {
        LGDataSource * dataSource = [[LGDataSource alloc] init];
        dataSource.name = [NSString stringWithFormat:@"%d小时", i];
        dataSource.value = [NSString stringWithFormat:@"%d", i];
        [dataSourceArray addObject:dataSource];
    }
    
    [self.pickerSelectVC setDataSource:dataSourceArray];
    [self.pickerSelectVC showInView:self.view];
    
    __weak typeof(self) weakSelf = self;
    
    [self.pickerSelectVC addConfirmClickBlock:^(LGDataSource *data) {
        
        
        int hour = [data.value intValue];
        [weakSelf setPlayTime:hour displayName:data.name];
        [weakSelf.pickerSelectVC.view removeFromSuperview];
    }];
    
    [self.pickerSelectVC addCancelClickBlock:^(LGDataSource *data) {
    }];
}

///设置约玩地点
- (IBAction)setAddressAction:(id)sender {
    NSLog(@"设置约玩地点");
    [self.view endEditing:YES];
    
    LGSelectAddressViewController * nextVC = [[LGSelectAddressViewController alloc] init];
    nextVC.commonAddress = _commonAddress;
    
    __weak typeof(self) weakSelf = self;
    
    nextVC.clickEnterActionBlock = ^(LGSelectAddressResult * address) {
        
        if (address == nil) {
            return;
        }
        
        NSString * addressName = address.name;///地址名称
        if (addressName.length > 0) {
            
            ///更新订单位置信息
            weakSelf.currentOrderInfo.address = addressName;
            weakSelf.currentOrderInfo.location = address.pt;
            
            ///更新设置地址按键
            [weakSelf.rootView.playAddress setDetail:addressName];
        }
    };
    
    [self.navigationController pushViewController:nextVC animated:YES];
}

#pragma mark - KVO
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    
    //1.改变创建订单按键状态（判断条件：开始时间，和陪玩时间）
    if ([object isKindOfClass:[LGOrderInfo class]]) {///订单状态改变
        LGOrderInfo * order = (LGOrderInfo *)object;
        
        ///判断金钱是否充足
        if (order.planStartTime && order.planKeepTime) {
            
            [_rootView.creatOrderBtn setEnabled:YES];
        } else {
            [_rootView.creatOrderBtn setEnabled:NO];
        }
    }
}

#pragma mark - UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == MODIFY_ADDRESS_TAG) {///更新地址
        LGCommonAddress * commonAddress = _commonAddress[buttonIndex];
        commonAddress.address = _currentOrderInfo.address;
        commonAddress.position = [NSString stringWithFormat:@"%f,%f", _currentOrderInfo.location.longitude, _currentOrderInfo.location.latitude];
        
        [LGDefineNetServer updateCommonAddress:commonAddress success:^(NSString *msg) {
        } failure:^(NSString *msg) {
        }];
        
    } else if (alertView.tag == RECHARGE_TAG) {///充值
        
        UITextField * textField = [alertView textFieldAtIndex:0];
        [textField resignFirstResponder];
        
        if (buttonIndex == 1) {
            double money = [textField.text doubleValue];
            [SVProgressHUD showWithStatus:@"正在充值..."];
            [LGDefineNetServer reChargeWithMoney:money tokenMoney:0 success:^(id result) {
                NSUInteger status = [[result objectForKey:@"result"] integerValue];
                if (status == 1) {
                    _wallet.money += money;
                    [SVProgressHUD dismissWithSuccess:@"充值成功"];
                } else {
                    NSString * msg = [result objectForKey:@"msg"];
                    [SVProgressHUD dismissWithError:msg];
                }
                
            } failure:^(NSString *msg) {
                [SVProgressHUD dismissWithError:@"充值失败"];
            }];
        }
    }
}

#pragma mark - setSetting
///设置约玩账号
- (void)setGameAccount:(LGGamePlayerInfo *)gameAccount displayName:(NSString *)name {
    ///改变界面
    [self.rootView.gameAccount setDetail:name];
    
//    if (gameAccount.id != [TDSingleton instance].userInfoModel.defaultPlayer.id) {
//        [LGDefineNetServer updateDefaultRoleBinding:gameAccount success:^(id result) {
//            [TDSingleton instance].userInfoModel.defaultPlayer = result;
//        } failure:^{
//        }];
//    }
    
    ///设置数据
    self.selectGamePlayer = gameAccount;
    self.currentOrderInfo.selectGameAccount = gameAccount;
}
///设置约玩开始时间
- (void)setPlayStartTime:(NSDate *)date {
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy年M月d日,H点"];
    NSString * str = [dateFormatter stringFromDate:date];
    
    NSTimeInterval time = [date timeIntervalSince1970];
    
    self.currentOrderInfo.planStartTime = time * 1000;
    [self.rootView.startTimeBtn setDetail:str];
}
///设置约玩时长
- (void)setPlayTime:(int)time displayName:(NSString *)name {
    [self.rootView.playTimeBtn setDetail:name];
    
    [self.currentOrderInfo setPlanKeepTime:time];
    
    double price = self.currentOrderInfo.offLine ? self.currentSceneInfo.priceOffline : self.currentSceneInfo.priceOnline;
    self.rootView.moneyView.advisePrice = price * self.currentOrderInfo.planKeepTime;
}
///设置约玩方式
- (void)setPlayType:(BOOL)offLine {
    
    self.currentOrderInfo.offLine = offLine;
    [self.rootView.playType setDetail:offLine ? @"线下" : @"线上"];
    
    double price = offLine ? self.currentSceneInfo.priceOffline : self.currentSceneInfo.priceOnline;
    self.rootView.moneyView.advisePrice = price * self.currentOrderInfo.planKeepTime;
    
    self.rootView.playAddressBtnHeight.constant = offLine ? 44 : 0;
    [self.rootView setNeedsUpdateConstraints];
}
///设置约玩地点
- (void)setPlayAddress {
    
}

#pragma mark - Money
- (void)setSpendMoney:(double)money {
    
    _wallet.spendMoney = money;
    [self updateOrderInfo];
}

- (void)updateOrderInfo {
    _currentOrderInfo.money = _wallet.orderMoney;
    _currentOrderInfo.tokenMoney = _wallet.orderTokenMoney;
    _currentOrderInfo.rewardMoney = _wallet.orderRewardMoney;
    
    if (_wallet.balanceFull) {
        [self.rootView.creatOrderBtn setTitle:@"创建订单" forState:UIControlStateNormal];
    } else {
        [self.rootView.creatOrderBtn setTitle:@"充值并创建订单" forState:UIControlStateNormal];
    }
}

#pragma mark - NetWorking
///获取金钱信息

///获取常用地址

#pragma mark - 百度云检索
///获取周边搜索参数
- (LGCloudNearbySearchInfo *)getCloudNearbySearchInfo {
    LGCloudNearbySearchInfo *cloudNearbySearch = [[LGCloudNearbySearchInfo alloc]init];
    cloudNearbySearch.pageIndex = 0;
    cloudNearbySearch.pageSize = 500;
    
    cloudNearbySearch.location = [NSString stringWithFormat:@"%f,%f", _currentOrderInfo.location.longitude, _currentOrderInfo.location.latitude];//@"104.072939,30.544244";
    cloudNearbySearch.radius = 10000;
    
    cloudNearbySearch.sortby = @"distance:1";
    
    NSMutableString * filter = [NSMutableString stringWithString:_currentSceneInfo.filter];
    
    NSArray * attackMinAndMax = [_currentSceneInfo.attack componentsSeparatedByString:@","];
    if (attackMinAndMax.count == 2) {
        NSInteger min = [[attackMinAndMax objectAtIndex:0] integerValue];
        NSInteger max = [[attackMinAndMax objectAtIndex:1] integerValue];
//        int selfAttack = [TDSingleton instance].userInfoModel.defaultPlayer.attack;
        NSInteger selfAttack = self.selectGamePlayer.attack;
        
        min = selfAttack + min;
        max = selfAttack + max;
        
        if (max > 65536) {
            max = 65536;
        }
        
        [filter appendFormat:@"|attack:%d,%d", (int)min, (int)max];
    }
    
    NSArray * gradeMinAndMax = [_currentSceneInfo.grade componentsSeparatedByString:@","];
    if (gradeMinAndMax.count == 2) {
        NSInteger min = [[gradeMinAndMax objectAtIndex:0] integerValue];
        NSInteger max = [[gradeMinAndMax objectAtIndex:1] integerValue];
//        NSInteger selfGrade = [TDSingleton instance].userInfoModel.defaultPlayer.gradeId;
        NSInteger selfGrade = self.selectGamePlayer.gradeId;
        
        min = selfGrade + min*10;
        max = selfGrade + max*10;
        
        [filter appendFormat:@"|grade:%ld,%ld", (long)min, (long)max];
    }
    
    cloudNearbySearch.filter = filter;
    cloudNearbySearch.keyword = _currentSceneInfo.keyWord;
    
    if (_currentSceneInfo.sortby.length > 0) {
        cloudNearbySearch.sortby = _currentSceneInfo.sortby;
    }
    
    return cloudNearbySearch;
}

///获取本地检索参数
- (LGCloudLocalSearchInfo *)getCloudLocalSearchInfo {
    LGCloudLocalSearchInfo *cloudLocalSearch = [[LGCloudLocalSearchInfo alloc]init];
    cloudLocalSearch.pageIndex = 0;
    cloudLocalSearch.pageSize = 500;
    
    cloudLocalSearch.region = @"全国";
    
    NSMutableString * filter = [NSMutableString stringWithString:_currentSceneInfo.filter];
    
    NSArray * attackMinAndMax = [_currentSceneInfo.attack componentsSeparatedByString:@","];
    if (attackMinAndMax.count == 2) {
        NSInteger min = [[attackMinAndMax objectAtIndex:0] integerValue];
        NSInteger max = [[attackMinAndMax objectAtIndex:1] integerValue];
//        NSInteger selfAttack = [TDSingleton instance].userInfoModel.defaultPlayer.attack;
        NSInteger selfAttack = self.selectGamePlayer.attack;
        
        min = selfAttack + min;
        max = selfAttack + max;
        
        if (max > 65536) {
            max = 65536;
        }
        
        [filter appendFormat:@"|attack:%ld,%ld", (long)min, (long)max];
    }
    
    NSArray * gradeMinAndMax = [_currentSceneInfo.grade componentsSeparatedByString:@","];
    if (gradeMinAndMax.count == 2) {
        NSInteger min = [[gradeMinAndMax objectAtIndex:0] integerValue];
        NSInteger max = [[gradeMinAndMax objectAtIndex:1] integerValue];
//        NSInteger selfGrade = [TDSingleton instance].userInfoModel.defaultPlayer.gradeId;
        NSInteger selfGrade = self.selectGamePlayer.gradeId;
        
        min = selfGrade + min*10;
        max = selfGrade + max*10;
        
        [filter appendFormat:@"|grade:%ld,%ld", (long)min, (long)max];
    }
    
    cloudLocalSearch.filter = filter;
    cloudLocalSearch.keyword = _currentSceneInfo.keyWord;
    
    if (_currentSceneInfo.sortby.length > 0) {
        cloudLocalSearch.sortby = _currentSceneInfo.sortby;
    }
    
    return cloudLocalSearch;
}

///创建订单
- (void)creatOrder {
    __weak typeof(self) weakSelf = self;
    
    if (self.wallet.isBalanceFull) {
        if ([_currentOrderInfo.masters isEqualToString:@""]) {

            PushOrderViewController * pushOrderVC = [[PushOrderViewController alloc] initWithOrder:nil orderModelType:OrderModelTypeScene sceneInfo:self.currentSceneInfo];
            
            [self.navigationController pushViewController:pushOrderVC animated:YES];
        } else {
            
            [SVProgressHUD showWithStatus:@"创建订单中..." maskType:SVProgressHUDMaskTypeBlack];
            [LGDefineNetServer creatDistributeOrder:weakSelf.currentOrderInfo success:^(id result) {
                NSLog(@"一键约订单创建成功");
                [SVProgressHUD dismissWithSuccess:@"订单创建成功"];
                
                weakSelf.currentOrderInfo.id = [result[@"orderId"] longLongValue];
                weakSelf.currentOrderInfo.timeOut = [result[@"timeOut"] longLongValue];
                
                PushOrderViewController * pushOrderVC = [[PushOrderViewController alloc] initWithOrder:self.currentOrderInfo orderModelType:OrderModelTypeScene sceneInfo:self.currentSceneInfo];
                
                [weakSelf.navigationController pushViewController:pushOrderVC animated:YES];
            } failure:^(NSString *msg) {
                NSLog(@"%@",msg);
                [SVProgressHUD dismissWithError:msg];
            }];
        }
    } else {
        ///充值后支付
        [self.payVC addFinishBlock:^(BOOL success) {
            if (success) {
                [LGDefineNetServer getWalletInfoSuccess:^(id result) {
                    NSLog(@"获取金额成功 %@", result);
                    weakSelf.wallet.money = [[result objectForKey:@"money"] doubleValue];
                    weakSelf.wallet.tokenMoney = [[result objectForKey:@"tokenMoney"] doubleValue];
                    [weakSelf creatOrderAction:nil];
                } failure:^(NSString *msg) {
                    [SVProgressHUD showErrorWithStatus:msg];
                }];
            }
        }];
        [self.payVC showInView:self.view money:self.wallet.orderNeedMoney];
    }
}

- (void)getGameList {
    
    __weak typeof(self) weakSelf = self;
    
    [LGDefineNetServer getRoleBindingsSuccess:^(NSArray<LGGamePlayerInfo *> *result, long long defaultAccountId) {
        
        weakSelf.gamePlayeList = result;
    } failure:^(NSString *msg) {
    }];
}
@end
