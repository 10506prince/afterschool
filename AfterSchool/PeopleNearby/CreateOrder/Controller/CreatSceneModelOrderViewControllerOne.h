//
//  CreatSceneModelOrderViewControllerOne.h
//  AfterSchool
//
//  Created by lg on 15/12/24.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGViewController.h"
#import "LGSceneInfo.h"

/**
 适用模式：无聊，约妹子，约男神，上积分
 */

@interface CreatSceneModelOrderViewControllerOne : LGViewController

- (instancetype)init __deprecated_msg("使用“initFromSceneModel”代替");
- (instancetype)initFromSceneInfo:(LGSceneInfo *)sceneInfo;

@end
