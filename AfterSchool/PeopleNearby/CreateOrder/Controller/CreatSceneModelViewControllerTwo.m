//
//  CreatSceneModelViewControllerTwo.m
//  AfterSchool
//
//  Created by lg on 15/12/25.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "CreatSceneModelViewControllerTwo.h"
#import "CreatSceneModelOrderRootViewTwo.h"
#import "Common.h"
#import "SVProgressHUD.h"
#import "LGDaKaInfo.h"
#import "TalkingData.h"

//数据模型
#import "LGOrderInfo.h"
#import "LGUserWalletNew.h"
#import "LGUserWallet.h"
#import "TDSingleton.h"

//服务
#import "LGDefineNetServer.h"
#import "LGCloudSearchService.h"

#import "LGPickerSelectViewController.h"
#import "LGDatePickerSelectViewController.h"
#import "LGPayViewController.h"
#import "LGSelectAddressViewController.h"

#import "ServerRuleDetailViewController.h"
#import "PushOrderViewController.h"

#define REWARD_TAG 100
#define MODIFY_ADDRESS_TAG 101
#define RECHARGE_TAG 102


@interface CreatSceneModelViewControllerTwo () <UIAlertViewDelegate> {
    
    NSMutableArray <LGCommonAddress *> * _commonAddress;///<常用地址
    LGSceneInfo * _currentSceneInfo;///<当前情景模式
}

@property (nonatomic, strong) LGSceneInfo * currentSceneInfo;///<当前情景模式
@property (nonatomic, strong) NSMutableArray <LGCommonAddress *> * commonAddress;
@property (strong) LGUserWalletNew * wallet;///<用户钱包状况(进入界面立即获取，加入KVO显示实时状态)(用来计算建议价格)
@property (strong) LGUserWallet * realWallet;///<用户钱包状况(进入界面立即获取，加入KVO显示实时状态)

@property (nonatomic, copy) LGOrderInfo * currentOrderInfo;///<当前订单（本地当前订单状态）

@property (nonatomic, strong) CreatSceneModelOrderRootViewTwo * rootView;

@property (nonatomic, strong) LGDatePickerSelectViewController * startTimeSelectVC;///<开始时间选择器
@property (nonatomic, strong) LGPickerSelectViewController * pickerSelectVC;///<约玩时间选择器
@property (nonatomic, strong) LGPayViewController * payVC;

@property (nonatomic, strong) NSArray <LGGamePlayerInfo *>* gamePlayeList;///<玩家绑定游戏账号列表
@property (nonatomic, strong) NSArray <LGLevelingPrice *>* gradePriceList;///<价格配置列表（可选段位）

@property (nonatomic, strong) LGGamePlayerInfo * selectGamePlayer;///<已选择的账号，用来搜素符合条件的大咖

@end

@implementation CreatSceneModelViewControllerTwo

- (void)dealloc
{
    NSArray * orderkeys = [LGOrderInfo propertys];
    
    for (NSString * key in orderkeys) {
        [_currentOrderInfo removeObserver:self forKeyPath:key];
    }
    
    NSArray * walletkeys = [LGUserWallet propertys];
    
    for (NSString * key in walletkeys) {
        [_realWallet removeObserver:self forKeyPath:key];
    }
    NSLogSelfMethodName;
}

- (instancetype)init
{
    NSAssert(0, @"此方法已废除，使用“initFromSceneModel”代替");
    return nil;
}

- (instancetype)initFromSceneInfo:(LGSceneInfo *)sceneInfo {
    self = [super init];
    if (self) {
        self.currentSceneInfo = sceneInfo;
        self.title = sceneInfo.name;
    }
    return self;
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [TalkingData trackPageBegin:selfControllerName];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [TalkingData trackPageEnd:selfControllerName];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initData];
    [self initUserInterface];
    
    [self getGameList];
    
    __weak typeof(self) weakSelf = self;
    ///获取常用地址
    if ([TDSingleton instance].userInfoModel.commonAddresses.count == 0) {
//        [SVProgressHUD showWithStatus:@"加载中..." maskType:SVProgressHUDMaskTypeClear];
        [LGDefineNetServer getCommonAddressSuccess:^(NSArray<LGCommonAddress *> *addresses) {
            [TDSingleton instance].userInfoModel.commonAddresses = addresses;
            [weakSelf.commonAddress addObjectsFromArray:addresses];
            [SVProgressHUD dismiss];
        } failure:^(NSString *msg) {
            [SVProgressHUD showErrorWithStatus:msg];
        }];
    } else {
        [self.commonAddress addObjectsFromArray:[TDSingleton instance].userInfoModel.commonAddresses];
    }
    
    if ([TDSingleton instance].userInfoModel.gamePlayers.count == 0) {
//        [SVProgressHUD showWithStatus:@"加载中..." maskType:SVProgressHUDMaskTypeClear];
        [LGDefineNetServer getRoleBindingsSuccess:^(NSArray<LGGamePlayerInfo *> *result, long long defaultAccountId) {
            [TDSingleton instance].userInfoModel.gamePlayers = result;
            weakSelf.gamePlayeList = result;
            [SVProgressHUD dismiss];
        } failure:^(NSString *msg) {
            [SVProgressHUD showErrorWithStatus:msg];
        }];
    } else {
        self.gamePlayeList = [TDSingleton instance].userInfoModel.gamePlayers;
    }
    
//    [SVProgressHUD showWithStatus:@"加载中..." maskType:SVProgressHUDMaskTypeBlack];
    [LGDefineNetServer getWalletInfoSuccess:^(id result) {
        NSLog(@"获取金额成功 %@", result);
//        [SVProgressHUD dismiss];
        
        double money = [[result objectForKey:@"money"] doubleValue];
        double tokenMoney = [[result objectForKey:@"tokenMoney"] doubleValue];
        
        weakSelf.wallet.money = money;
        weakSelf.wallet.tokenMoney = tokenMoney;
        
        weakSelf.realWallet.money = money;
        weakSelf.realWallet.tokenMoney = tokenMoney;
        
        [weakSelf initDefaultSetting];
        
    } failure:^(id result) {
        NSLog(@"获取金额失败");
//        [SVProgressHUD dismissWithError:@"加载失败"];
    }];
}

- (void)initData {
    
    _commonAddress = [NSMutableArray array];
    
    //钱包
    _wallet = [[LGUserWalletNew alloc] init];
    _realWallet = [[LGUserWallet alloc] init];
    
    _wallet.gradePriceList = [TDSingleton instance].levelingPrices;
    
    NSArray * realWalletkeys = [LGUserWallet propertys];
    for (NSString * key in realWalletkeys) {
        [_realWallet addObserver:self forKeyPath:key options:NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew context:nil];
    }
    
    //本地当前订单
    _currentOrderInfo = [[LGOrderInfo alloc] init];
    _currentOrderInfo.offLine = NO;
    _currentOrderInfo.info = self.currentSceneInfo.name;
    _currentOrderInfo.sceneId = self.currentSceneInfo.sceneId;
    
    NSArray * orderkeys = [LGOrderInfo propertys];
    for (NSString * key in orderkeys) {
        [_currentOrderInfo addObserver:self forKeyPath:key options:NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew context:nil];
    }
}

- (void)initUserInterface {
    
    //添加根视图
    [self.view addSubview:self.rootView];
    
    ///约玩时间选择器
    LGPickerSelectViewController * pickerSelectVC = [[LGPickerSelectViewController alloc] initWithNibName:@"LGPickerSelectViewController" bundle:nil];
    pickerSelectVC.view.frame = [UIScreen mainScreen].bounds;
    [self addChildViewController:pickerSelectVC];
    self.pickerSelectVC = pickerSelectVC;
    
    //充值
    LGPayViewController * payVC = [[LGPayViewController alloc] initWithNibName:@"LGPayViewController" bundle:nil];
    payVC.view.frame = [UIScreen mainScreen].bounds;
    [self addChildViewController:payVC];
    self.payVC = payVC;
}

- (CreatSceneModelOrderRootViewTwo *)rootView {
    if (!_rootView) {

        _rootView = [CreatSceneModelOrderRootViewTwo viewFromNibWithoutOwner];
        
        [_rootView.navigationBarView.titleLabel setText:self.title];
        
        [_rootView.navigationBarView.leftButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];

        [_rootView.creatOrderBtn addTarget:self action:@selector(creatOrderAction:) forControlEvents:UIControlEventTouchUpInside];
        
        [_rootView.gameAccountBtn addTarget:self action:@selector(gameAccountAction:) forControlEvents:UIControlEventTouchUpInside];
        
        [_rootView.playGradeBtn addTarget:self action:@selector(gradeAction:) forControlEvents:UIControlEventTouchUpInside];
        
        [_rootView.playTimeBtn addTarget:self action:@selector(setPlayTimeAction:) forControlEvents:UIControlEventTouchUpInside];
        
//        [_rootView.slider addTarget:self action:@selector(sliderValueChangedAction:) forControlEvents:UIControlEventValueChanged];
        
        [_rootView.serverRule addTarget:self action:@selector(serverRuleAction:) forControlEvents:UIControlEventTouchUpInside];
        
        [_rootView.serverRuleDetailBtn addTarget:self action:@selector(serverRuleDetailAction:) forControlEvents:UIControlEventTouchUpInside];
        
        __weak typeof(self) weakSelf = self;
        [_rootView.moneyView addPriceChangeBlock:^(float price) {
            [weakSelf setSpendMoney:floorf(price)];
        }];
    }
    
    return _rootView;
}

- (void)initDefaultSetting {
    
    ///设置初始化账号和保级目标
    LGGamePlayer * gamePalyer = [TDSingleton instance].userInfoModel.defaultPlayer;
    [self setGameAccount:gamePalyer displayName:[NSString stringWithFormat:@"%@(%@)",gamePalyer.playerName,gamePalyer.serverName]];
    
    CGFloat price = _wallet.displayTotalMoney;
    self.rootView.moneyView.inPrice = price;
}

#pragma mark - Action
- (void)backAction {
    [self.navigationController popViewControllerAnimated:YES];
}

///设置账户
- (IBAction)gameAccountAction:(UIButton *)sender {
    [self.view endEditing:YES];
    
    NSMutableArray <LGDataSource *> * gameList = [NSMutableArray array];
    for (LGGamePlayerInfo * gameInfo in self.gamePlayeList) {
        LGDataSource * dataSource = [[LGDataSource alloc] init];
        dataSource.name = [NSString stringWithFormat:@"%@(%@)", gameInfo.playerName,gameInfo.serverName];
        dataSource.value = gameInfo;
        
        [gameList addObject:dataSource];
    }

    self.pickerSelectVC.dataSource = gameList;
    
    __weak typeof(self) weakSelf = self;
    [self.pickerSelectVC addConfirmClickBlock:^(LGDataSource *data) {
        LGGamePlayerInfo * gameInfo = data.value;
        
        [weakSelf setGameAccount:gameInfo displayName:data.name];
        
        [weakSelf.pickerSelectVC.view removeFromSuperview];
    }];
    [self.pickerSelectVC showInView:self.view];
}

///设置目标段位
- (IBAction)gradeAction:(UIButton *)sender {
    [self.view endEditing:YES];
    
    NSMutableArray <LGDataSource *> * gameList = [NSMutableArray array];
    
    for (LGLevelingPrice * gradePrice in self.gradePriceList) {
        LGDataSource * dataSource = [LGDataSource alloc];
        dataSource.name = gradePrice.targetGrade;
        dataSource.value = gradePrice;
        
        [gameList addObject:dataSource];
    }
    
    self.pickerSelectVC.dataSource = gameList;
    
    __weak typeof(self) weakSelf = self;
    [self.pickerSelectVC addConfirmClickBlock:^(LGDataSource *data) {
        
        ///改变界面
        [weakSelf.rootView.playGradeBtn setDetail:data.name];
        
        LGLevelingPrice * gradePrice = data.value;
        [weakSelf setEndGradeId:gradePrice.targetGradeId];
        
        [weakSelf.pickerSelectVC.view removeFromSuperview];
    }];
    [self.pickerSelectVC showInView:self.view];
}

///创建订单
- (IBAction)creatOrderAction:(id)sender {
    
    __weak typeof(self) weakSelf = self;
    
    [LGCloudSearchService localSearchWithSearchParam:[self getCloudLocalSearchInfo] success:^(NSArray<LGCloudPOIInfo *> *result, NSUInteger size, NSUInteger total) {
        NSMutableString * accounts = [NSMutableString stringWithFormat:@""];
        
        for (LGCloudPOIInfo * daka in result) {
            NSString * str = daka.user_id;
            if ([str isEqualToString:[TDSingleton instance].userInfoModel.account]) {
                continue;
            }
            [accounts appendString:str];
            [accounts appendString:@","];
        }
        
        weakSelf.currentOrderInfo.masters = accounts;
        
        [weakSelf creatOrder];
    } failure:^(NSString *msg) {
        [SVProgressHUD showErrorWithStatus:msg];
    }];
}

///设置约玩时间
- (IBAction)setPlayTimeAction:(id)sender {
    NSLog(@"设置约玩时间");
    [self.view endEditing:YES];
    
    NSMutableArray <LGDataSource *> * dataSourceArray = [NSMutableArray array];
    NSUInteger values[5] = {6,12,24,48,72};
    for (int i = 0; i < 5; i++) {
        LGDataSource * dataSource = [[LGDataSource alloc] init];
        dataSource.name = [NSString stringWithFormat:@"%lu小时", (unsigned long)values[i]];
        dataSource.value = [NSString stringWithFormat:@"%lu", (unsigned long)values[i]];
        [dataSourceArray addObject:dataSource];
    }
    
    [self.pickerSelectVC setDataSource:dataSourceArray];
    [self.pickerSelectVC showInView:self.view];
    
    __weak typeof(self) weakSelf = self;
    
    [self.pickerSelectVC addConfirmClickBlock:^(LGDataSource *data) {
        
        [weakSelf setPlayTime:[data.value intValue] displayName:data.name];
        
        [weakSelf.pickerSelectVC.view removeFromSuperview];
    }];
    
    [self.pickerSelectVC addCancelClickBlock:^(LGDataSource *data) {
    }];
}

- (IBAction)serverRuleAction:(UIButton *)sender {
    sender.selected = !sender.selected;
}
- (IBAction)serverRuleDetailAction:(UIButton *)sender {
    ServerRuleDetailViewController * serverRuleDetailVC = [[ServerRuleDetailViewController alloc] init];
    [serverRuleDetailVC.view setFrame:[UIScreen mainScreen].bounds];
    [self presentViewController:serverRuleDetailVC animated:YES completion:^{
        
    }];
}
#pragma mark - setSetting
///设置约玩账号
- (void)setGameAccount:(LGGamePlayerInfo *)gameAccount displayName:(NSString *)name {
    ///改变界面
    [self.rootView.gameAccountBtn setDetail:name];
    
//    if (gameAccount.id != [TDSingleton instance].userInfoModel.defaultPlayer.id) {
//        [LGDefineNetServer updateDefaultRoleBinding:gameAccount success:^(id result) {
//            [TDSingleton instance].userInfoModel.defaultPlayer = result;
//        } failure:^{
//        }];
//    }
    
    ///确定可选目标段位列表并清除已选目标段位
    NSMutableArray <LGLevelingPrice *> * array = [NSMutableArray array];
    
    for (LGLevelingPrice * gradePrice in [TDSingleton instance].levelingPrices) {
        
        if (gameAccount.gradeId == 0) {
            
            if (gradePrice.targetGradeId == 1) {
                [array addObject:gradePrice];
                break;
            }
        } else if (gradePrice.targetGradeId > gameAccount.gradeId) {
            [array addObject:gradePrice];
        }
    }
    
    self.gradePriceList = array;///确定目标段位列表
    ///设置数据
    [self setStartGradeId:gameAccount.gradeId];
    
    if (array.count > 0) {
        LGLevelingPrice * defaultGradePrice = array[0];
        [self setEndGradeId:defaultGradePrice.targetGradeId];
        [self.rootView.playGradeBtn setDetail:defaultGradePrice.targetGrade];
    }

    ///设置数据
    self.selectGamePlayer = gameAccount;
    self.currentOrderInfo.selectGameAccount = gameAccount;
}

///设置约玩时长
- (void)setPlayTime:(NSInteger)time displayName:(NSString *)name {
    
    [self.rootView.playTimeBtn setDetail:[NSString stringWithFormat:@"(建议时长%lu小时) %@", (unsigned long)self.wallet.displayTime ,name]];
    
    [self.currentOrderInfo setPlanKeepTime:time];
}


#pragma mark - KVO
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    
    //1.改变创建订单按键状态（判断条件：开始时间，和陪玩时间）
    if ([object isKindOfClass:[LGOrderInfo class]]) {///订单状态改变
        LGOrderInfo * order = (LGOrderInfo *)object;
        
        ///判断金钱是否充足
        if (order.planStartTime && order.planKeepTime) {
            
            [_rootView.creatOrderBtn setEnabled:YES];
        } else {
            [_rootView.creatOrderBtn setEnabled:NO];
        }
    } else if([object isKindOfClass:[LGUserWallet class]]) {///钱包状态改变
        LGUserWallet * wallet = (LGUserWallet *)object;
        if ([keyPath isEqualToString:@"startGrade"] || [keyPath isEqualToString:@"endGrade"]) {//
        }
    }
}

#pragma mark - UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == MODIFY_ADDRESS_TAG) {///更新地址
        LGCommonAddress * commonAddress = _commonAddress[buttonIndex];
        commonAddress.address = _currentOrderInfo.address;
        commonAddress.position = [NSString stringWithFormat:@"%f,%f", _currentOrderInfo.location.longitude, _currentOrderInfo.location.latitude];
        
        [LGDefineNetServer updateCommonAddress:commonAddress success:^(NSString *msg) {
        } failure:^(NSString *msg) {
        }];
        
    } else if (alertView.tag == RECHARGE_TAG) {///充值
        
        UITextField * textField = [alertView textFieldAtIndex:0];
        [textField resignFirstResponder];
        
        if (buttonIndex == 1) {
            double money = [textField.text doubleValue];
            [SVProgressHUD showWithStatus:@"正在充值..."];
            [LGDefineNetServer reChargeWithMoney:money tokenMoney:0 success:^(id result) {
                NSUInteger status = [[result objectForKey:@"result"] integerValue];
                if (status == 1) {
                    _wallet.money += money;
                    [SVProgressHUD dismissWithSuccess:@"充值成功"];
                } else {
                    NSString * msg = [result objectForKey:@"msg"];
                    [SVProgressHUD dismissWithError:msg];
                }
                
            } failure:^(NSString *msg) {
                [SVProgressHUD showErrorWithStatus:msg];
            }];
        }
    }
}

#pragma mark - AdviseMoney

- (void)setStartGradeId:(NSUInteger)gradeId {
    self.wallet.startGrade = gradeId;
    
    [self updateDisplay];
}

- (void)setEndGradeId:(NSUInteger)gradeId {
    self.wallet.endGrade = gradeId;
    
    [self updateDisplay];
}

- (void)updateDisplay {
    self.rootView.moneyView.advisePrice = _wallet.displayTotalMoney;
    
    [self setPlayTime:self.wallet.displayTime displayName:[NSString stringWithFormat:@"%lu小时", (unsigned long)self.wallet.displayTime]];
}

#pragma mark - Money
- (void)setSpendMoney:(double)money {
    
    _realWallet.spendMoney = money;
    [self updateOrderInfo];
}

- (void)updateOrderInfo {
    
    _currentOrderInfo.money = _realWallet.orderMoney;
    _currentOrderInfo.tokenMoney = _realWallet.orderTokenMoney;
    _currentOrderInfo.rewardMoney = _realWallet.orderRewardMoney;
    
    if (_realWallet.balanceFull) {
        [self.rootView.creatOrderBtn setTitle:@"创建订单" forState:UIControlStateNormal];
    } else {
        [self.rootView.creatOrderBtn setTitle:@"充值并创建订单" forState:UIControlStateNormal];
    }
}

#pragma mark - NetWorking
///获取金钱信息
- (void)getMoneyInfo {
    
    __weak typeof(self) weakSelf = self;
    
    [LGDefineNetServer getWalletInfoSuccess:^(id result) {
        NSLog(@"获取金额成功 %@", result);
        
        weakSelf.wallet.money = [[result objectForKey:@"money"] doubleValue];
        weakSelf.wallet.tokenMoney = [[result objectForKey:@"tokenMoney"] doubleValue];
    } failure:^(id result) {
        NSLog(@"获取金额失败");
    }];
}

#pragma mark - 百度云检索
///获取周边搜索参数
//- (LGCloudNearbySearchInfo *)getCloudNearbySearchInfo {
//    LGCloudNearbySearchInfo *cloudNearbySearch = [[LGCloudNearbySearchInfo alloc]init];
//    cloudNearbySearch.pageIndex = 0;
//    cloudNearbySearch.pageSize = 500;
//    
//    cloudNearbySearch.location = [NSString stringWithFormat:@"%f,%f", _currentOrderInfo.location.longitude, _currentOrderInfo.location.latitude];//@"104.072939,30.544244";
//    cloudNearbySearch.radius = 10000;
//    
//    cloudNearbySearch.sortby = @"distance:1";
//    
//    LGSceneInfo * sceneInfo = [[TDSingleton instance].scenes objectAtIndex:(_currentSceneModelType - 1)];
//    
//    NSMutableString * filter = [NSMutableString stringWithString:sceneInfo.filter];
//    
//    NSArray * attackMinAndMax = [sceneInfo.attack componentsSeparatedByString:@","];
//    if (attackMinAndMax.count == 2) {
//        NSInteger min = [[attackMinAndMax objectAtIndex:0] integerValue];
//        NSInteger max = [[attackMinAndMax objectAtIndex:1] integerValue];
//        NSInteger selfAttack = [TDSingleton instance].userInfoModel.defaultPlayer.attack;
//        
//        min = selfAttack + min;
//        max = selfAttack + max;
//        
//        [filter appendFormat:@"|attack:%ld,%ld", (long)min, (long)max];
//    }
//    
//    NSArray * gradeMinAndMax = [sceneInfo.grade componentsSeparatedByString:@","];
//    if (gradeMinAndMax.count == 2) {
//        NSInteger min = [[gradeMinAndMax objectAtIndex:0] integerValue];
//        NSInteger max = [[gradeMinAndMax objectAtIndex:1] integerValue];
//        NSInteger selfgrade = [TDSingleton instance].userInfoModel.defaultPlayer.gradeId;
//        
//        min = selfgrade + min*10;
//        max = selfgrade + max*10;
//        
//        [filter appendFormat:@"|grade:%ld,%ld", (long)min, (long)max];
//    }
//    
//    cloudNearbySearch.filter = filter;
//    cloudNearbySearch.keyword = sceneInfo.keyWord;
//    
//    return cloudNearbySearch;
//}

///获取本地检索参数
- (LGCloudLocalSearchInfo *)getCloudLocalSearchInfo {
    LGCloudLocalSearchInfo *cloudLocalSearch = [[LGCloudLocalSearchInfo alloc]init];
    cloudLocalSearch.pageIndex = 0;
    cloudLocalSearch.pageSize = 500;
    
    cloudLocalSearch.region = @"全国";
    
    LGSceneInfo * sceneInfo = self.currentSceneInfo;
    
    NSMutableString * filter = [NSMutableString stringWithString:sceneInfo.filter];
    
    NSArray * attackMinAndMax = [sceneInfo.attack componentsSeparatedByString:@","];
    if (attackMinAndMax.count == 2) {
        NSInteger min = [[attackMinAndMax objectAtIndex:0] integerValue];
        NSInteger max = [[attackMinAndMax objectAtIndex:1] integerValue];
        NSInteger selfAttack = self.selectGamePlayer.attack;
        
        min = selfAttack + min;
        max = selfAttack + max;
        
        if (max > 65536) {
            max = 65536;
        }
        
        [filter appendFormat:@"|attack:%d,%d", (int)min, (int)max];
    }
    
    NSArray * gradeMinAndMax = [sceneInfo.grade componentsSeparatedByString:@","];
    if (gradeMinAndMax.count == 2) {
        NSInteger min = [[gradeMinAndMax objectAtIndex:0] integerValue];
        NSInteger max = [[gradeMinAndMax objectAtIndex:1] integerValue];
        NSInteger selfgrade = self.selectGamePlayer.gradeId;
        
        min = selfgrade + min*10;
        max = selfgrade + max*10;
        
        [filter appendFormat:@"|grade:%ld,%ld", (long)min, (long)max];
    }
    
    cloudLocalSearch.filter = filter;
    cloudLocalSearch.keyword = sceneInfo.keyWord;
    
    return cloudLocalSearch;
}

///创建订单
- (void)creatOrder {
    __weak typeof(self) weakSelf = self;
    
    if (self.wallet.isBalanceFull) {
        if ([_currentOrderInfo.masters isEqualToString:@""]) {

            PushOrderViewController * pushOrderVC = [[PushOrderViewController alloc] initWithOrder:nil orderModelType:OrderModelTypeScene sceneInfo:self.currentSceneInfo];
            
            [self.navigationController pushViewController:pushOrderVC animated:YES];
        } else {
            [SVProgressHUD showWithStatus:@"创建订单中..." maskType:SVProgressHUDMaskTypeBlack];
            [LGDefineNetServer creatDistributeOrder:weakSelf.currentOrderInfo success:^(id result) {
                [SVProgressHUD dismiss];
                
                weakSelf.currentOrderInfo.id = [result[@"orderId"] longLongValue];
                weakSelf.currentOrderInfo.timeOut = [result[@"timeOut"] longLongValue];
                
                PushOrderViewController * pushOrderVC = [[PushOrderViewController alloc] initWithOrder:self.currentOrderInfo orderModelType:OrderModelTypeScene sceneInfo:self.currentSceneInfo];
                
                [weakSelf.navigationController pushViewController:pushOrderVC animated:YES];
            } failure:^(NSString *msg) {
                NSLog(@"%@", msg);
                [SVProgressHUD dismissWithError:msg];
            }];
        }
    } else {
        ///充值后支付
        
        [self.payVC addFinishBlock:^(BOOL success) {
            if (success) {
                [SVProgressHUD showWithStatus:@"正在处理..."];
                [LGDefineNetServer getWalletInfoSuccess:^(id result) {
                    NSLog(@"获取金额成功 %@", result);
                    [SVProgressHUD dismiss];
                    weakSelf.wallet.money = [[result objectForKey:@"money"] doubleValue];
                    weakSelf.wallet.tokenMoney = [[result objectForKey:@"tokenMoney"] doubleValue];
                    
                    [weakSelf creatOrderAction:nil];
                } failure:^(id result) {
                    NSLog(@"获取金额失败");
                    [SVProgressHUD dismiss];
                }];
            }
        }];
        
        [self.payVC showInView:weakSelf.view money:weakSelf.wallet.displayNeedMoney];
    }
}

#pragma mark - Network
- (void)getGameList {
    
    __weak typeof(self) weakSelf = self;
    
    [LGDefineNetServer getRoleBindingsSuccess:^(NSArray<LGGamePlayerInfo *> *result, long long defaultAccountId) {
        
        weakSelf.gamePlayeList = result;
    } failure:^(NSString *msg) {
        [SVProgressHUD showErrorWithStatus:msg];
    }];
}

@end
