//
//  OrderDisplayView.h
//  AfterSchool
//
//  Created by lg on 15/12/7.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIView+UINib.h"

@interface OrderDisplayView : UIView

@property (nonatomic, assign) double spendTotal;///<花费总金额
@property (nonatomic, assign) double spendMoney;///<花费金钱
@property (nonatomic, assign) double spendTokenMoney;///<花费代币
@property (nonatomic, assign) double spendRewardMoney;///<打赏金额

@property (nonatomic, assign) double money;///<剩余金额
@property (nonatomic, assign) double tokenMoney;///<剩余代币
@property (nonatomic, assign) double needRechargeMoney;///<需充值金额

@end
