//
//  CityRootView.h
//  AfterSchool
//
//  Created by lg on 15/10/22.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavigationBarView.h"


#import "LGButton.h"
#import "TDDateTimePickerView.h"

#import "OrderModelType.h"

#import "LGScrollView.h"
#import "OrderDisplayView.h"

@interface CreateOrderRootView : UIView

@property (nonatomic, strong) NavigationBarView * navigationBarView;

@property (nonatomic, strong) LGScrollView * scrollView;

@property (nonatomic, strong) OrderDisplayView * moneyView;///<支付金额构成 视图

@property (nonatomic, strong) LGButton * startTimeBtn;///<开始时间
@property (nonatomic, strong) LGButton * playTimeBtn;///<约玩时间
@property (nonatomic, strong) LGButton * rewardBtn;///<打赏
@property (nonatomic, strong) UIButton * addressBtn;///<地点设置
@property (nonatomic, strong) UIButton * creatOrderBtn;///<创建订单

@property (nonatomic, strong) TDDateTimePickerView * planStartTimePickerView;


- (void)updateAddressBtnSize;

@end
