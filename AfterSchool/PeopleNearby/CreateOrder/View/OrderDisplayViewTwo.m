//
//  OrderDisplayViewTwo.m
//  AfterSchool
//
//  Created by lg on 16/1/4.
//  Copyright © 2016年 Teson Draw. All rights reserved.
//

#import "OrderDisplayViewTwo.h"
#import "LGUIImageComponent.h"
#import "Common.h"

@interface OrderDisplayViewTwo () <UITextFieldDelegate>

@property (nonatomic, strong) UIView * backView;
@property (nonatomic, strong) UILabel * titleLabel;
@property (nonatomic, strong) UITextField * priceTextField;
@property (nonatomic, strong) UILabel * otherLabel;

@property (nonatomic, strong) UISlider * slider;

@property (nonatomic, copy) void(^priceChangeBlock)(float price);

@end

IB_DESIGNABLE
@implementation OrderDisplayViewTwo
- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setUp];
    }
    return self;
}
- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self setUp];
    }
    return self;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUp];
    }
    return self;
}

- (void)setUp {
    [self addSubview:self.backView];
    [self addSubview:self.titleLabel];
    [self addSubview:self.priceTextField];
    [self addSubview:self.otherLabel];
    [self addSubview:self.slider];
    
    [self setBackViewColor:APPDarkColor];
    [self setMinimumColor:[UIColor orangeColor]];
    [self setMaximumColor:LgColor(56, 1)];
    
    [self setAdvisePrice:0];
    [self setInPrice:0];
    [self setMaxPrice:1000];
    
    [self.priceTextField setDelegate:self];
    [self.slider addTarget:self action:@selector(sliderValueChangedAction:) forControlEvents:UIControlEventValueChanged];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat width = self.bounds.size.width;
    
    CGFloat height = self.bounds.size.height - 30;
    [self.backView setFrame:CGRectMake(0, 0, width, height)];
    [self.slider setFrame:CGRectMake(0, height - 10, width, 30)];
    
    
    [self.titleLabel sizeToFit];
    [self.titleLabel setFrame:CGRectMake(20, 20, self.titleLabel.bounds.size.width, self.titleLabel.bounds.size.height)];

    [self.priceTextField setCenter:CGPointMake(width/2, height/2)];
    
    [self.otherLabel sizeToFit];
    [self.otherLabel setCenter:CGPointMake(width/2, height - 15 - self.otherLabel.bounds.size.height/2)];
}

- (UIView *)backView {
    if (!_backView) {
        _backView = [[UIView alloc] init];
    }
    return _backView;
}
- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        [_titleLabel setText:@"约玩悬赏（元）"];
        [_titleLabel setTextColor:[UIColor whiteColor]];
        [_titleLabel setFont:[UIFont systemFontOfSize:16]];
    }
    return _titleLabel;
}

- (UITextField *)priceTextField {
    if (!_priceTextField) {
        _priceTextField = [[UITextField alloc] init];
        [_priceTextField setText:@"0"];
        [_priceTextField setFont:[UIFont systemFontOfSize:60]];
        [_priceTextField setTextColor:[UIColor whiteColor]];
        [_priceTextField setKeyboardType:UIKeyboardTypeNumberPad];
        [_priceTextField setBounds:CGRectMake(0, 0, 300, 72)];
        [_priceTextField setTextAlignment:NSTextAlignmentCenter];
    }
    return _priceTextField;
}

- (UILabel *)otherLabel {
    if (!_otherLabel) {
        _otherLabel = [[UILabel alloc] init];
        [_otherLabel setTextColor:[UIColor whiteColor]];
        [_otherLabel setFont:[UIFont systemFontOfSize:12]];
//        [_otherLabel setText:[NSString stringWithFormat:@"建议不低于0元，滑条取值不足，可手动输入"]];
    }
    return _otherLabel;
}

- (UISlider *)slider {
    if (!_slider) {
        
        _slider = [[UISlider alloc] init];
    }
    return _slider;
}

#pragma mark - setInValue

- (void)setMaxPrice:(float)maxPrice {
    _maxPrice = maxPrice;
    
    [self.slider setValue:_inPrice/_maxPrice animated:YES];
    [self sliderValueChangedAction:_slider];
}

- (void)setAdvisePrice:(float)advisePrice {
    if (advisePrice < 1) {
        advisePrice = 1;
    }
    _advisePrice = advisePrice;
    
    if (_outPrice < _advisePrice) {
        self.inPrice = _advisePrice;
    }
    
    NSString * price = [NSString stringWithFormat:@"%.f", _advisePrice];
    NSString * str = [NSString stringWithFormat:@"建议不低于%@元，滑条取值不足，可手动输入", price];
    
    NSMutableAttributedString * attributedStr = [[NSMutableAttributedString alloc] initWithString:str
                                                                                       attributes:@{
                                                                                                    NSFontAttributeName : [UIFont systemFontOfSize:12],        NSForegroundColorAttributeName : (self.otherTextColor ? self.otherTextColor : [UIColor whiteColor]),
                                                                                                                   }];
    [attributedStr setAttributes:@{NSForegroundColorAttributeName : [UIColor orangeColor]} range:NSMakeRange(5, price.length)];
    
    [self.otherLabel setAttributedText:attributedStr];
    
    [self setNeedsLayout];
}

- (void)setInPrice:(float)inPrice {
    _inPrice = inPrice;
    
    [self.slider setValue:_inPrice/_maxPrice animated:YES];
    [self sliderValueChangedAction:_slider];
}

#pragma mark - setOutValue
- (void)setOutPrice:(float)outPrice {
    _outPrice = outPrice;
    
    [self.priceTextField setText:[NSString stringWithFormat:@"%.f", outPrice]];
    
    void (^block)(float price) = self.priceChangeBlock;
    
    if (block) {
        block(outPrice);
    }
}


#pragma mark - setProperty
- (void)setBackViewColor:(UIColor *)backViewColor {
    _backViewColor = backViewColor;
    
    [self.backView setBackgroundColor:_backViewColor];
}
- (void)setPriceTextColor:(UIColor *)priceTextColor {
    _priceTextColor = priceTextColor;
    
    [self.priceTextField setTextColor:_priceTextColor];
}
- (void)setOtherTextColor:(UIColor *)otherTextColor {
    _otherTextColor = otherTextColor;
    
    [self setAdvisePrice:self.advisePrice];
}
- (void)setTitleTextColor:(UIColor *)titleTextColor {
    _titleTextColor = titleTextColor;
    
    [self.titleLabel setTextColor:_titleTextColor];
}

- (void)setThumbImage:(UIImage *)thumbImage {
    _thumbImage = thumbImage;
    
    [self.slider setThumbImage:_thumbImage forState:UIControlStateNormal];
}

- (void)setMinimumColor:(UIColor *)minimumColor {
    _minimumColor = minimumColor;
    
    UIImage * minimumImage = [UIImage imageWithColor:_minimumColor size:CGSizeMake(10, 11)];
    [self.slider setMinimumTrackImage:[minimumImage resizableImageWithCapInsets:UIEdgeInsetsMake(3, 3, 3, 3) resizingMode:UIImageResizingModeStretch] forState:UIControlStateNormal];
}

- (void)setMaximumColor:(UIColor *)maximumColor {
    _maximumColor = maximumColor;
    
    UIImage * maximumImage = [UIImage imageWithColor:_maximumColor size:CGSizeMake(10, 11)];
    
    [self.slider setMaximumTrackImage:[maximumImage resizableImageWithCapInsets:UIEdgeInsetsMake(3, 3, 3, 3) resizingMode:UIImageResizingModeStretch] forState:UIControlStateNormal];
}

#pragma mark - touch
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.priceTextField resignFirstResponder];
}

#pragma mark - block
- (void)addPriceChangeBlock:(void (^)(float))priceChangeBlock {
    self.priceChangeBlock = priceChangeBlock;
}


#pragma mark - Action
- (IBAction)sliderValueChangedAction:(UISlider *)sender {
    float value = sender.value;
    float adviseValue = _advisePrice/_maxPrice;
    
    if (value < adviseValue) {
        sender.value = adviseValue;
        [self setOutPrice:_advisePrice];
    } else {
        [self setOutPrice:value*_maxPrice];
    }
}

#pragma mark - delegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (range.location > 3) {
        return NO;
    }
    return YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    float price = [textField.text floatValue];
    
    if (price < _advisePrice) {
        price = _advisePrice;
    } else if (price > _maxPrice) {
        price = _maxPrice;
    }
    
    [self.slider setValue:price/_maxPrice animated:YES];
    [self sliderValueChangedAction:self.slider];
}
@end
