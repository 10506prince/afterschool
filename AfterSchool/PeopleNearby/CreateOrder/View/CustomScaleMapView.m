//
//  CustomView.m
//  AfterSchool
//
//  Created by lg on 15/12/7.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "CustomScaleMapView.h"

#define LgColor(c,alp) [UIColor colorWithRed:(c)/255.0 green:(c)/255.0 blue:(c)/255.0 alpha:alp]
#define LgColorRGB(r,g,b,alp) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:alp]

IB_DESIGNABLE

@interface CustomScaleMapView ()

@property (nonatomic, assign) IBInspectable CGFloat lineWidth;

@property (nonatomic, strong) IBInspectable UIColor * strokeColor;
@property (nonatomic, strong) IBInspectable UIColor * fillColor;
@property (nonatomic, strong) IBInspectable UIColor * firstColor;
@property (nonatomic, strong) IBInspectable UIColor * secondColor;
@property (nonatomic, strong) IBInspectable UIColor * thirdColor;
@property (nonatomic, strong) IBInspectable UIImage * btnImage;


@property (nonatomic, strong) CAShapeLayer * backgroundRound;
@property (nonatomic, strong) CAShapeLayer * firstArc;
@property (nonatomic, strong) CAShapeLayer * secondArc;
@property (nonatomic, strong) CAShapeLayer * thirdArc;

@property (nonatomic, strong) UILabel * totalLabel;
@property (nonatomic, strong) UILabel * labelStr;
@property (nonatomic, strong) UIButton * btn;

@end

@implementation CustomScaleMapView

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setUp];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUp];
    }
    return self;
}
- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self setUp];
    }
    return self;
}
- (void)setUp {
    
    self.backgroundRound = [self getBackgroundRoundLayer];
    self.firstArc = [self getArclayerWithStrokeColor:self.firstColor];
    self.secondArc = [self getArclayerWithStrokeColor:self.secondColor];
    self.thirdArc = [self getArclayerWithStrokeColor:self.thirdColor];
    
    [self.layer addSublayer:self.backgroundRound];
    [self.layer addSublayer:self.firstArc];
    [self.layer addSublayer:self.secondArc];
    [self.layer addSublayer:self.thirdArc];
    
    self.totalLabel = [[UILabel alloc] init];
    [self addSubview:self.totalLabel];
    
    self.labelStr = [[UILabel alloc] init];
    [self.labelStr setFont:[UIFont systemFontOfSize:14]];
    [self.labelStr setTextColor:LgColor(120, 1)];
    [self.labelStr setTextAlignment:NSTextAlignmentCenter];
    [self.labelStr setText:@"本次需支付金额"];
    //[self.labelStr setBackgroundColor:[UIColor orangeColor]];
    [self addSubview:self.labelStr];
    
    self.btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.btn addTarget:self action:@selector(btnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.btn];
    
    self.total = 0;
    self.first = 0;
    self.second = 0;
    self.third = 0;
    self.lineWidth = 8.0;
    
    self.strokeColor = [UIColor groupTableViewBackgroundColor];
    self.fillColor = [UIColor whiteColor];
    self.firstColor = LgColorRGB(41, 128, 246, 1);
    self.secondColor = LgColorRGB(254, 182, 17, 1);
    self.thirdColor = LgColorRGB(253, 123, 28, 1);
}

- (void)layoutSubviews {
    self.backgroundRound.path = [self getRoundPathWithRect:self.bounds];
    self.firstArc.path = [self getRoundPathWithRect:self.bounds];
    self.secondArc.path = [self getRoundPathWithRect:self.bounds];
    self.thirdArc.path = [self getRoundPathWithRect:self.bounds];
    
    [self.totalLabel sizeToFit];
    CGRect frame = self.totalLabel.frame;
    if (frame.size.width > self.bounds.size.width - self.lineWidth * 2 - 20) {
        frame.size.width = self.bounds.size.width - self.lineWidth * 2 - 20;
        self.totalLabel.frame = frame;
    }
    [self.totalLabel setCenter:CGPointMake(self.bounds.size.width/2, self.bounds.size.height/2 - self.totalLabel.bounds.size.height/2)];
    
    [self.labelStr sizeToFit];
    [self.labelStr setCenter:CGPointMake(self.bounds.size.width/2, self.bounds.size.height/2 + self.labelStr.bounds.size.height/2)];
    
    [self.btn setBounds:CGRectMake(0, 0, 30, 30)];
    [self.btn setCenter:CGPointMake(self.bounds.size.width/2, CGRectGetMaxY(self.labelStr.frame) + 15)];

    [self.btn.imageView.layer setCornerRadius:self.btn.imageView.bounds.size.height/2];
    [self.btn.imageView.layer setMasksToBounds:YES];
}

#pragma mark - Action

- (IBAction)btnAction:(id)sender {
    CustomScaleMapViewClickBlock block = self.clickBlock;
    if (block) {
        block();
    }
}

///获取背景图层
- (CAShapeLayer *)getBackgroundRoundLayer {
    
    CAShapeLayer * layer = [CAShapeLayer layer];
    layer.fillColor = [UIColor whiteColor].CGColor;
    layer.strokeColor = [UIColor groupTableViewBackgroundColor].CGColor;
    layer.lineWidth = 8.0;
    layer.fillRule = kCAFillRuleEvenOdd;
    
    return layer;
}

///获取进度条图层
- (CAShapeLayer *)getArclayerWithStrokeColor:(UIColor *)color {
    CAShapeLayer * layer = [CAShapeLayer layer];
    layer.fillColor = [UIColor clearColor].CGColor;
    layer.strokeColor = color.CGColor;
    layer.lineWidth = 8.0;
    
    return layer;
}

//@property IBInspectable CGFloat miterLimit;
- (void)setMiterLimit:(CGFloat)miterLimit {
    _miterLimit = miterLimit;
    
    _firstArc.miterLimit = _miterLimit;
    _secondArc.miterLimit = _miterLimit;
    _thirdArc.miterLimit = _miterLimit;
}
//@property(copy) IBInspectable NSString *lineCap;
- (void)setLineCap:(NSString *)lineCap {
    _lineCap = lineCap;
    _firstArc.lineCap = _lineCap;
    _secondArc.lineCap = _lineCap;
    _thirdArc.lineCap = _lineCap;
}
//@property(copy) IBInspectable NSString *lineJoin;
- (void)setLineJoin:(NSString *)lineJoin {
    _lineJoin = lineJoin;
    _firstArc.lineJoin = _lineJoin;
    _secondArc.lineJoin = _lineJoin;
    _thirdArc.lineJoin = _lineJoin;
}
//@property IBInspectable CGFloat lineDashPhase;
- (void)setLineDashPhase:(CGFloat)lineDashPhase {
    _lineDashPhase = lineDashPhase;
    _firstArc.lineDashPhase = _lineDashPhase;
    _secondArc.lineDashPhase = _lineDashPhase;
    _thirdArc.lineDashPhase = _lineDashPhase;
}

///获取路径
- (CGPathRef)getRoundPathWithRect:(CGRect)rect {
    
    CGFloat edgeWidth = rect.size.height < rect.size.width ? rect.size.height : rect.size.width;
    edgeWidth -= self.lineWidth;
    CGFloat edgeHor = rect.size.width/2 - edgeWidth/2;
    CGFloat edgeVer = rect.size.height/2 - edgeWidth/2;
    
    CGRect roundedRect = CGRectMake(edgeHor, edgeVer, edgeWidth, edgeWidth);
    UIBezierPath * path = [UIBezierPath bezierPathWithRoundedRect:roundedRect cornerRadius:edgeWidth];
    
    return path.CGPath;
}

#pragma mark - UIColor
- (void)setStrokeColor:(UIColor *)strokeColor {
    _strokeColor = strokeColor;
}
- (void)setFirstColor:(UIColor *)firstColor {
    _firstColor = firstColor;
    
    self.firstArc.strokeColor = _firstColor.CGColor;
}

- (void)setSecondColor:(UIColor *)secondColor {
    _secondColor = secondColor;
    
    self.secondArc.strokeColor = _secondColor.CGColor;
}

- (void)setThirdColor:(UIColor *)thirdColor {
    _thirdColor = thirdColor;
    
    self.thirdArc.strokeColor = _thirdColor.CGColor;
}

- (void)setBtnImage:(UIImage *)btnImage {
    _btnImage = btnImage;
    
    [self.btn setImage:_btnImage forState:UIControlStateNormal];
}

#pragma mark - Value

- (void)setLineWidth:(CGFloat)lineWidth {
    _lineWidth = lineWidth > 0 ? lineWidth : 0;
    
    self.backgroundRound.lineWidth = _lineWidth;
    self.firstArc.lineWidth = _lineWidth;
    self.secondArc.lineWidth = _lineWidth;
    self.thirdArc.lineWidth = _lineWidth;
}

- (void)setTotal:(double)total {
    _total = total > 0 ? total : 0;
    
    NSString * str = [NSString stringWithFormat:@"%.0f元",total > 0 ? total : 0];
    NSDictionary * attributedOptionDic = @{NSFontAttributeName:[UIFont systemFontOfSize:40],NSForegroundColorAttributeName:[UIColor blackColor]};
    NSDictionary * attributedOptionDic1 = @{NSFontAttributeName:[UIFont systemFontOfSize:14],NSForegroundColorAttributeName:LgColor(120, 1)};
    NSMutableAttributedString * attributeStr = [[NSMutableAttributedString alloc] initWithString:str attributes:attributedOptionDic];
    [attributeStr addAttributes:attributedOptionDic1 range:NSMakeRange(str.length - 1, 1)];
    
    [self.totalLabel setAttributedText:attributeStr];
    
    [self update];
    [self setNeedsLayout];
}

- (void)setFirst:(double)first {
    _first = first > 0 ? first : 0;

    [self update];
}

- (void)setSecond:(double)second {
    _second = second > 0 ? second : 0;
    
    [self update];
}

- (void)setThird:(double)third {
    _third = third;
    
    [self update];
}

- (void)update {
    
    double scale1 = _total > 0 ? _first/_total : 0;
    double scale2 = _total > 0 ? (_second + _first) /_total : 0;
    double scale3 = _total > 0 ? (_third + _second + _first) /_total : 0;
    
    self.firstArc.strokeStart = 0;
    self.firstArc.strokeEnd = scale1 > 1 ? 1 : scale1;

    self.secondArc.strokeStart = self.firstArc.strokeEnd;
    self.secondArc.strokeEnd = scale2 > 1 ? 1 : scale2;
    
    self.thirdArc.strokeStart = self.secondArc.strokeEnd;
    self.thirdArc.strokeEnd = scale3 > 1 ? 1 : scale3;
}
@end
