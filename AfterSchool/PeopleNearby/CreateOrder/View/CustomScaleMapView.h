//
//  CustomView.h
//  AfterSchool
//
//  Created by lg on 15/12/7.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^CustomScaleMapViewClickBlock)(void);

@interface CustomScaleMapView : UIView

@property (nonatomic, assign) IBInspectable double total;
@property (nonatomic, assign) IBInspectable double first;
@property (nonatomic, assign) IBInspectable double second;
@property (nonatomic, assign) IBInspectable double third;

@property (nonatomic, assign) IBInspectable CGFloat miterLimit;
@property (nonatomic, copy) IBInspectable NSString *lineCap;
@property (nonatomic, copy) IBInspectable NSString *lineJoin;
@property (nonatomic, assign) IBInspectable CGFloat lineDashPhase;


@property (copy) CustomScaleMapViewClickBlock clickBlock;

- (void)update;

@end
