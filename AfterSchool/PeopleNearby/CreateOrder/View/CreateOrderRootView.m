//
//  CityRootView.m
//  AfterSchool
//
//  Created by lg on 15/10/22.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "CreateOrderRootView.h"
#import "UIColor+RGB.h"
#import "UIButton+SetBackgroundColor.h"
#import "UIImage+Scale.h"
#import "Common.h"


#define ORDERVIEW_DEFAULT_HEIGHT (568.0 - 64)

@implementation CreateOrderRootView

- (instancetype)init
{
    self = [super initWithFrame:[UIScreen mainScreen].bounds];
    if (self) {
        [self setUp];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self setFrame:[UIScreen mainScreen].bounds];
        [self setUp];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUp];
    }
    return self;
}

- (void)setUp {
    [self addSubview:self.scrollView];
    [self addSubview:self.navigationBarView];
}

- (NavigationBarView *)navigationBarView {
    if (!_navigationBarView) {
        _navigationBarView = [[NavigationBarView alloc] initWithTitle:@"创建订单"
                                                           titleColor:nil
                                                      backgroundColor:nil
                                              leftButtonBgViewOriginX:0
                                                 leftButtonImageWidth:22
                                                 leftButtonTitleColor:[UIColor whiteColor]
                                                       leftButtonName:@"返回"
                                                  leftButtonImageName:@"navigation_bar_return_button_left_arrow"
                                                      rightButtonName:nil
                                                 rightButtonImageName:nil];
    }
    return _navigationBarView;
}

- (LGScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[LGScrollView alloc] initWithFrame:self.bounds];
        [_scrollView setHeadType:LGScrollViewHeadTypeDark];
        [_scrollView setDelaysContentTouches:YES];
        
        [_scrollView addSubview:self.moneyView];
        
        [_scrollView addSubview:self.startTimeBtn];
        [_scrollView addSubview:self.playTimeBtn];
        [_scrollView addSubview:self.rewardBtn];
        
        [_scrollView addSubview:self.addressBtn];
        
        [_scrollView addSubview:self.creatOrderBtn];
        
        [_scrollView setContentSize:CGSizeMake(self.bounds.size.width, CGRectGetMaxY(self.creatOrderBtn.frame) + 26)];
    }
    return _scrollView;
}

- (LGButton *)startTimeBtn {
    if (!_startTimeBtn) {
        _startTimeBtn = [[LGButton alloc] initWithFrame:CGRectMake(0, 240, self.bounds.size.width, 50)];
        [_startTimeBtn setTitle:@"起始时间"];
        [_startTimeBtn setDetail:@"选择约玩时间"];
        //[_startTimeBtn setDetail:@"2015年12月28日，16点"];
    }
    return _startTimeBtn;
}
- (LGButton *)playTimeBtn {
    if (!_playTimeBtn) {
        _playTimeBtn = [[LGButton alloc] initWithFrame:CGRectMake(0, 300, self.bounds.size.width, 50)];
        [_playTimeBtn setTitle:@"约玩时间"];
        [_playTimeBtn setDetail:@"选择约玩时长"];
        //[_playTimeBtn setDetail:@"2小时"];
    }
    return _playTimeBtn;
}
- (LGButton *)rewardBtn {
    if (!_rewardBtn) {
        _rewardBtn = [[LGButton alloc] initWithFrame:CGRectMake(0, 360, self.bounds.size.width, 50)];
        [_rewardBtn setTitle:@"我要打赏"];
        [_rewardBtn setDetail:@"提高约玩成功的几率"];
    }
    return _rewardBtn;
}

- (UIButton *)addressBtn {
    if (!_addressBtn) {
        _addressBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_addressBtn setFrame:CGRectMake(15, 420, 100, 20)];
        [_addressBtn.titleLabel setFont:[UIFont systemFontOfSize:14]];
        [_addressBtn setTitleColor:LgColor(202, 1) forState:UIControlStateNormal];

        UIImage * image = [UIImage originImage:[UIImage imageNamed:@"tabar_nearby_normal"] scaleToSize:CGSizeMake(20, 40)];
        [_addressBtn setImage:image forState:UIControlStateNormal];
        [_addressBtn setTitle:@"地点设置" forState:UIControlStateNormal];
        
        [_addressBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    }
    return _addressBtn;
}

- (void)updateAddressBtnSize {
    
    CGRect frame = _addressBtn.frame;
    CGFloat maxWidth = self.bounds.size.width - 30;
    CGSize size = CGSizeMake(maxWidth, 20);
    CGSize bounds = [_addressBtn sizeThatFits:size];
    frame.size = CGSizeMake(bounds.width < maxWidth ? bounds.width : maxWidth, 20);
    [_addressBtn setFrame:frame];
}

- (UIButton *)creatOrderBtn {
    if (!_creatOrderBtn) {
        _creatOrderBtn = [UIButton buttonWithType:UIButtonTypeSystem];
        [_creatOrderBtn setFrame:CGRectMake(12, 460, self.bounds.size.width - 24, 44)];
        
        [_creatOrderBtn setTitle:@"创建订单" forState:UIControlStateNormal];
        [_creatOrderBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_creatOrderBtn.titleLabel setFont:[UIFont systemFontOfSize:18]];
        
        [_creatOrderBtn setBackgroundColor:[UIColor getColor:@"25A6F4"] forState:UIControlStateNormal];
        [_creatOrderBtn setBackgroundColor:[UIColor getColor:@"DBDBDB"] forState:UIControlStateDisabled];
        
        [_creatOrderBtn.layer setCornerRadius:3.0];
        [_creatOrderBtn setClipsToBounds:YES];
        
        [_creatOrderBtn setEnabled:NO];
    }
    return _creatOrderBtn;
}

- (OrderDisplayView *)moneyView {
    if (!_moneyView) {
        
        _moneyView = [OrderDisplayView viewFromNibWithoutOwner];
        [_moneyView setFrame:CGRectMake(0, 0, self.bounds.size.width, 228)];
    }
    return _moneyView;
}


- (TDDateTimePickerView *)planStartTimePickerView {
    if (!_planStartTimePickerView) {
        
        NSDate * today = [NSDate date];
        NSCalendar * calendar = [NSCalendar currentCalendar];
        
        NSDateComponents * dateCom = [calendar componentsInTimeZone:[NSTimeZone systemTimeZone] fromDate:today];
        dateCom.hour++;
        //NSLog(@"%@", dateCom);
        
        _planStartTimePickerView = [[TDDateTimePickerView alloc] initWithFrame:CGRectMake(LgScreenWidth/2 - 150, LgScreenHeigh, 300, 244) dateComponents:dateCom targetPosition:CGPointMake((LgScreenWidth - 300) / 2, LgScreenHeigh/2 - 122)];
    }
    return _planStartTimePickerView;
}
@end
