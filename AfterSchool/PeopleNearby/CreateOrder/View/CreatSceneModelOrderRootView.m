//
//  CreatSceneModelOrderRootView.m
//  AfterSchool
//
//  Created by lg on 15/12/24.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "CreatSceneModelOrderRootView.h"
#import "Common.h"
#import "LGUIImageComponent.h"
#import "UIButton+SetBackgroundColor.h"

@implementation CreatSceneModelOrderRootView

- (void)awakeFromNib {
    [self setUp];
}

- (instancetype)init
{
    self = [super initWithFrame:[UIScreen mainScreen].bounds];
    if (self) {
        [self setUp];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self setFrame:[UIScreen mainScreen].bounds];
        [self setUp];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUp];
    }
    return self;
}

- (void)setUp {

    [self addSubview:self.navigationBarView];
    
    [self.scrollView setDelaysContentTouches:YES];
    [self.scrollView setBackgroundColor:[UIColor whiteColor]];
    
    [self.creatOrderBtn setBackgroundColor:[UIColor orangeColor] forState:UIControlStateNormal];
    [self.creatOrderBtn setBackgroundColor:[UIColor lightGrayColor] forState:UIControlStateDisabled];
    [self.creatOrderBtn.layer setCornerRadius:3];
    [self.creatOrderBtn.layer setMasksToBounds:YES];
}

- (NavigationBarView *)navigationBarView {
    if (!_navigationBarView) {
        _navigationBarView = [[NavigationBarView alloc] initWithTitle:@"创建订单"
                                                           titleColor:nil
                                                      backgroundColor:nil
                                              leftButtonBgViewOriginX:0
                                                 leftButtonImageWidth:22
                                                 leftButtonTitleColor:[UIColor whiteColor]
                                                       leftButtonName:@"返回"
                                                  leftButtonImageName:@"navigation_bar_return_button_left_arrow"
                                                      rightButtonName:nil
                                                 rightButtonImageName:nil];
    }
    return _navigationBarView;
}
@end
