//
//  OrderDisplayView.m
//  AfterSchool
//
//  Created by lg on 15/12/7.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "OrderDisplayView.h"
#import "CustomScaleMapView.h"

#define LgColor(c,alp) [UIColor colorWithRed:(c)/255.0 green:(c)/255.0 blue:(c)/255.0 alpha:alp]
#define LgColorRGB(r,g,b,alp) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:alp]

@interface OrderDisplayView ()

@property (nonatomic, weak) IBOutlet CustomScaleMapView * scaleMapView;

@property (nonatomic, weak) IBOutlet UILabel * spendMoneyLabel;///<花费金钱
@property (nonatomic, weak) IBOutlet UILabel * spendTokenMoneyLabel;///<花费代币
@property (nonatomic, weak) IBOutlet UILabel * spendRewardMoneyLabel;///<打赏金钱

@property (nonatomic, weak) IBOutlet UILabel * moneyLabel;///<剩余金钱
@property (nonatomic, weak) IBOutlet UILabel * tokenMoneyLabel;///<剩余代币
@property (nonatomic, weak) IBOutlet UILabel * needRechargeMoneyLabel;///<需要充值


@end

@implementation OrderDisplayView
//@synthesize spendTotal;
//@synthesize spendMoney;
//@synthesize spendTokenMoney;
//@synthesize spendRewardMoney;
//@synthesize money;
//@synthesize tokenMoney;
//@synthesize needRechargeMoney;

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)setSpendTotal:(double)spendTotal {
    _spendTotal = spendTotal;
    self.scaleMapView.total = _spendTotal;
}

- (void)setSpendMoney:(double)spendMoney {
    _spendMoney = spendMoney;
    
    [self.spendMoneyLabel setText:[NSString stringWithFormat:@"需要零钱 %.0f元", _spendMoney > 0 ? _spendMoney : 0]];
    self.scaleMapView.first = _spendMoney;
}

- (void)setSpendTokenMoney:(double)spendTokenMoney {
    _spendTokenMoney = spendTokenMoney;
    
    [self.spendTokenMoneyLabel setText:[NSString stringWithFormat:@"游戏币抵扣 %.0f元", _spendTokenMoney > 0 ? _spendTokenMoney : 0]];
    self.scaleMapView.second = _spendTokenMoney;
}

- (void)setSpendRewardMoney:(double)spendRewardMoney {
    _spendRewardMoney = spendRewardMoney;
    
    [self.spendRewardMoneyLabel setText:[NSString stringWithFormat:@"打赏金额 %.0f元", _spendRewardMoney > 0 ? _spendRewardMoney : 0]];
    self.scaleMapView.third = _spendRewardMoney;
}

- (void)setTokenMoney:(double)tokenMoney {
    _tokenMoney = tokenMoney;
    
    [self.tokenMoneyLabel setText:[NSString stringWithFormat:@"钱包游戏币 %.0f枚", _tokenMoney > 0 ? _tokenMoney : 0]];
}
- (void)setMoney:(double)money {
    _money = money;
    
    [self.moneyLabel setText:[NSString stringWithFormat:@"钱包零钱 %.0f元", _money > 0 ? _money : 0]];
}
- (void)setNeedRechargeMoney:(double)needRechargeMoney {
    _needRechargeMoney = needRechargeMoney;
    
    [self.needRechargeMoneyLabel setText:[NSString stringWithFormat:@"需要充值 %.0f元", _needRechargeMoney > 0 ? _needRechargeMoney : 0]];
}

@end
