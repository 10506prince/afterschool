//
//  LGButton.h
//  AfterSchool
//
//  Created by lg on 15/10/29.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LGButton : UIControl

@property (nonatomic, strong) NSDate * date;

@property (nonatomic, copy) IBInspectable NSString * title;
@property (nonatomic, copy) IBInspectable NSString * detail;

@end
