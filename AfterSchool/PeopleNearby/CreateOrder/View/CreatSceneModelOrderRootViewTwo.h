//
//  CreatSceneModelOrderRootViewTwo.h
//  AfterSchool
//
//  Created by lg on 15/12/25.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavigationBarView.h"
#import "LGScrollView.h"
#import "LGButton.h"
#import "OrderDisplayViewTwo.h"
#import "UIView+UINib.h"

@interface CreatSceneModelOrderRootViewTwo : UIView

@property (nonatomic, strong) NavigationBarView * navigationBarView;

@property (nonatomic, weak) IBOutlet LGScrollView * scrollView;

@property (nonatomic, weak) IBOutlet UIButton * serverRule;///<服务细则
@property (nonatomic, weak) IBOutlet UIButton * serverRuleDetailBtn;///<服务细则详情
@property (nonatomic, weak) IBOutlet OrderDisplayViewTwo * moneyView;///<支付金额构成 视图
//@property (nonatomic, weak) IBOutlet UISlider * slider;

@property (nonatomic, weak) IBOutlet LGButton * gameAccountBtn;///<选择账号
@property (nonatomic, weak) IBOutlet LGButton * playGradeBtn;///<目标段位
@property (nonatomic, weak) IBOutlet LGButton * playTimeBtn;///<约玩时间

@property (nonatomic, weak) IBOutlet UIButton * creatOrderBtn;///<创建订单

@end
