//
//  LGButton.m
//  AfterSchool
//
//  Created by lg on 15/10/29.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGButton.h"
#import "Common.h"
#import "CAShapeLayer+CustomPath.h"

#define LgColor(c,alp) [UIColor colorWithRed:(c)/255.0 green:(c)/255.0 blue:(c)/255.0 alpha:alp]
#define LgColorRGB(r,g,b,alp) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:alp]

@interface LGButton ()

@property (nonatomic, strong) CAShapeLayer * line;
@property (nonatomic, strong) CAShapeLayer * arrow;

@property (nonatomic, strong) UILabel * titleLabel;
@property (nonatomic, strong) UILabel * detailLabel;

@end

IB_DESIGNABLE
@implementation LGButton

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setUp];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self setUp];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUp];
    }
    return self;
}

- (void)setUp {
    
    [self setClipsToBounds:YES];
    
    self.line = [self getLineLayer];
    self.arrow = [self getArrowLayer];
    
    [self.layer addSublayer:self.line];
    [self.layer addSublayer:self.arrow];
    
    [self addSubview:self.titleLabel];
    [self addSubview:self.detailLabel];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.line.path = [CAShapeLayer linePathWithRect:self.bounds edgeInsets:UIEdgeInsetsMake(0, 20, 0, 20)];
    self.arrow.path = [CAShapeLayer arrowPathWithRect:self.bounds edgeInsets:UIEdgeInsetsMake(0, 0, 0, 20)];
    
    [self.titleLabel sizeToFit];
    [self.titleLabel setFrame:CGRectMake(20, 0, self.titleLabel.bounds.size.width, self.bounds.size.height)];
    
    [self.detailLabel sizeToFit];
    [self.detailLabel setFrame:CGRectMake(20 + self.titleLabel.bounds.size.width + 20, 0, self.bounds.size.width - self.titleLabel.bounds.size.width - 40 - 40, self.bounds.size.height)];
}

- (CAShapeLayer *)getLineLayer {
    CAShapeLayer * line = [CAShapeLayer layer];
    line.lineWidth = 1/[UIScreen mainScreen].scale;
    line.fillColor = [UIColor clearColor].CGColor;
    line.strokeColor = [UIColor lightGrayColor].CGColor;
    
    return line;
}

- (CAShapeLayer *)getArrowLayer {
    CAShapeLayer * arrow = [CAShapeLayer layer];
    arrow.lineWidth = 2/[UIScreen mainScreen].scale;
    arrow.fillColor = [UIColor clearColor].CGColor;
    arrow.strokeColor = [UIColor lightGrayColor].CGColor;
    
    return arrow;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        [_titleLabel setFont:[UIFont systemFontOfSize:14]];
        [_titleLabel setTextColor:LgColor(56, 1)];
    }
    return _titleLabel;
}

- (UILabel *)detailLabel {
    if (!_detailLabel) {
        _detailLabel = [[UILabel alloc] init];
        [_detailLabel setFont:[UIFont systemFontOfSize:14]];
        [_detailLabel setTextAlignment:NSTextAlignmentRight];
        [_detailLabel setTextColor:APPLightColor];
        [_detailLabel setText:@"未设置"];
    }
    return _detailLabel;
}

- (void)setTitle:(NSString *)title {
    _title = [title copy];
    
    [self.titleLabel setText:_title];
    [self setNeedsLayout];
}

- (void)setDetail:(NSString *)detail {
    _detail = [detail copy];
    [self.detailLabel setText:_detail];
    [self setNeedsLayout];
}

@end
