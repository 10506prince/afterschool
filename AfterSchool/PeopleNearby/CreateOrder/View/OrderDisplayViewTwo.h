//
//  OrderDisplayViewTwo.h
//  AfterSchool
//
//  Created by lg on 16/1/4.
//  Copyright © 2016年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>

///
@interface OrderDisplayViewTwo : UIView

@property (nonatomic) IBInspectable float inPrice;///<设定价格
@property (nonatomic) IBInspectable float advisePrice;///<建议价格
@property (nonatomic) IBInspectable float maxPrice;///<最高价格

@property (nonatomic, strong) IBInspectable UIImage * thumbImage;
@property (nonatomic, strong) IBInspectable UIColor * minimumColor;
@property (nonatomic, strong) IBInspectable UIColor * maximumColor;

@property (nonatomic, strong) IBInspectable UIColor * backViewColor;
@property (nonatomic, strong) IBInspectable UIColor * priceTextColor;
@property (nonatomic, strong) IBInspectable UIColor * otherTextColor;
@property (nonatomic, strong) IBInspectable UIColor * titleTextColor;

@property (nonatomic, assign, readonly) float outPrice;

- (void)addPriceChangeBlock:(void(^)(float price))priceChangeBlock;

@end



