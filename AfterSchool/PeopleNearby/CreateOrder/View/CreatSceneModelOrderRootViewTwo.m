//
//  CreatSceneModelOrderRootViewTwo.m
//  AfterSchool
//
//  Created by lg on 15/12/25.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "CreatSceneModelOrderRootViewTwo.h"
#import "Common.h"
#import "UIButton+SetBackgroundColor.h"
#import "LGUIImageComponent.h"

@interface CreatSceneModelOrderRootViewTwo ()
@end

@implementation CreatSceneModelOrderRootViewTwo

- (void)awakeFromNib {
    [self setUp];
}

- (instancetype)init
{
    self = [super initWithFrame:[UIScreen mainScreen].bounds];
    if (self) {
        [self setUp];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self setFrame:[UIScreen mainScreen].bounds];
        [self setUp];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUp];
    }
    return self;
}

- (void)setUp {

    [self addSubview:self.navigationBarView];
    
    [self.scrollView setDelaysContentTouches:YES];
    [self.scrollView setBackgroundColor:LgColor(230, 1)];
    
    [self.creatOrderBtn setBackgroundColor:[UIColor orangeColor] forState:UIControlStateNormal];
    [self.creatOrderBtn setBackgroundColor:[UIColor lightGrayColor] forState:UIControlStateDisabled];
    [self.creatOrderBtn.layer setCornerRadius:3];
    [self.creatOrderBtn.layer setMasksToBounds:YES];
    
//    UIImage * thumbImage = [UIImage imageNamed:@"order_slider_thumb"];
//    UIImage * minimumImage = [UIImage imageWithColor:[UIColor orangeColor] size:CGSizeMake(10, 11)];
//    UIImage * maximumImage = [UIImage imageWithColor:LgColor(56, 1) size:CGSizeMake(10, 11)];
//    
//    [self.slider setThumbImage:thumbImage forState:UIControlStateNormal];
//    [self.slider setMinimumTrackImage:[minimumImage resizableImageWithCapInsets:UIEdgeInsetsMake(3, 3, 3, 3) resizingMode:UIImageResizingModeStretch] forState:UIControlStateNormal];
//    [self.slider setMaximumTrackImage:[maximumImage resizableImageWithCapInsets:UIEdgeInsetsMake(3, 3, 3, 3) resizingMode:UIImageResizingModeStretch] forState:UIControlStateNormal];
}

- (NavigationBarView *)navigationBarView {
    if (!_navigationBarView) {
        _navigationBarView = [[NavigationBarView alloc] initWithTitle:@"创建订单"
                                                           titleColor:nil
                                                      backgroundColor:nil
                                              leftButtonBgViewOriginX:0
                                                 leftButtonImageWidth:22
                                                 leftButtonTitleColor:[UIColor whiteColor]
                                                       leftButtonName:@"返回"
                                                  leftButtonImageName:@"navigation_bar_return_button_left_arrow"
                                                      rightButtonName:nil
                                                 rightButtonImageName:nil];
    }
    return _navigationBarView;
}

//- (LGScrollView *)scrollView {
//    if (!_scrollView) {
//        _scrollView = [[LGScrollView alloc] initWithFrame:self.bounds];
//        [_scrollView setHeadType:LGScrollViewHeadTypeDark];
//        [_scrollView setDelaysContentTouches:YES];
//        
//        [_scrollView addSubview:self.moneyView];
//        
//        [_scrollView addSubview:self.gameAccountBtn];
//        [_scrollView addSubview:self.gradeBtn];
//        
//        [_scrollView addSubview:self.playTimeBtn];
//        
//        [_scrollView addSubview:self.onlineOrOffLineBtn];
//        
//        [_scrollView addSubview:self.creatOrderBtn];
//        
//        [_scrollView setContentSize:CGSizeMake(self.bounds.size.width, CGRectGetMaxY(self.creatOrderBtn.frame) + 26)];
//    }
//    return _scrollView;
//}

//- (UIButton *)gameAccountBtn {
//    if (!_gameAccountBtn) {
//        _gameAccountBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//        [_gameAccountBtn setFrame:CGRectMake(0, 240, self.bounds.size.width, 50)];
//        [_gameAccountBtn setTitle:@"游戏账号" forState:UIControlStateNormal];
//        
//        [_gameAccountBtn setTitleColor:LgColor(120, 1) forState:UIControlStateNormal];
//    }
//    return _gameAccountBtn;
//}
//- (UIButton *)gradeBtn {
//    if (!_gradeBtn) {
//        _gradeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//        [_gradeBtn setFrame:CGRectMake(0, 300, self.bounds.size.width, 50)];
//        [_gradeBtn setTitle:@"段位" forState:UIControlStateNormal];
//        
//        [_gradeBtn setTitleColor:LgColor(120, 1) forState:UIControlStateNormal];
//    }
//    return _gradeBtn;
//}

//- (LGButton *)playTimeBtn {
//    if (!_playTimeBtn) {
//        _playTimeBtn = [[LGButton alloc] initWithFrame:CGRectMake(0, 360, self.bounds.size.width, 50)];
//        [_playTimeBtn setTitle:@"预计时长"];
//        [_playTimeBtn setDetail:@"0小时"];
//    }
//    return _playTimeBtn;
//}

//- (UIButton *)onlineOrOffLineBtn {
//    if (!_onlineOrOffLineBtn) {
//        _onlineOrOffLineBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//        [_onlineOrOffLineBtn setFrame:CGRectMake(0, 420, self.bounds.size.width, 50)];
//        [_onlineOrOffLineBtn setTitle:@"线上" forState:UIControlStateNormal];
//        //[_onlineOrOffLineBtn setTitle:@"线下" forState:UIControlStateSelected];
//        [_onlineOrOffLineBtn setTitleColor:LgColor(120, 1) forState:UIControlStateNormal];
//        
//        [_onlineOrOffLineBtn setEnabled:NO];
//    }
//    return _onlineOrOffLineBtn;
//}

//- (UIButton *)creatOrderBtn {
//    if (!_creatOrderBtn) {
//        _creatOrderBtn = [UIButton buttonWithType:UIButtonTypeSystem];
//        [_creatOrderBtn setFrame:CGRectMake(12, 480, self.bounds.size.width - 24, 44)];
//        
//        [_creatOrderBtn setTitle:@"创建订单" forState:UIControlStateNormal];
//        [_creatOrderBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//        [_creatOrderBtn.titleLabel setFont:[UIFont systemFontOfSize:18]];
//        
//        [_creatOrderBtn setBackgroundColor:LGColorFromHexRGB(0x25A6F4, 1) forState:UIControlStateNormal];
//        [_creatOrderBtn setBackgroundColor:LGColorFromHexRGB(0xDBDBDB, 1) forState:UIControlStateDisabled];
//        
//        [_creatOrderBtn.layer setCornerRadius:3.0];
//        [_creatOrderBtn setClipsToBounds:YES];
//        
//        [_creatOrderBtn setEnabled:NO];
//    }
//    return _creatOrderBtn;
//}

//- (OrderDisplayView *)moneyView {
//    if (!_moneyView) {
//        
//        _moneyView = [OrderDisplayView viewFromNibWithoutOwner];
//        [_moneyView setFrame:CGRectMake(0, 0, self.bounds.size.width, 228)];
//    }
//    return _moneyView;
//}


@end
