//
//  CreatSceneModelOrderRootView.h
//  AfterSchool
//
//  Created by lg on 15/12/24.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIView+UINib.h"
#import "NavigationBarView.h"

#import "LGButton.h"

#import "OrderModelType.h"

#import "LGScrollView.h"
#import "OrderDisplayViewTwo.h"

@interface CreatSceneModelOrderRootView : UIView

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *playAddressBtnHeight;

@property (nonatomic, strong) NavigationBarView * navigationBarView;

@property (nonatomic, weak) IBOutlet LGScrollView * scrollView;

@property (nonatomic, weak) IBOutlet OrderDisplayViewTwo * moneyView;///<支付金额构成 视图

@property (nonatomic, weak) IBOutlet LGButton * gameAccount;///<约玩账号
@property (nonatomic, weak) IBOutlet LGButton * startTimeBtn;///<开始时间
@property (nonatomic, weak) IBOutlet LGButton * playTimeBtn;///<约玩时间
@property (nonatomic, weak) IBOutlet LGButton * playType;///约玩方式
@property (nonatomic, weak) IBOutlet LGButton * playAddress;///<约玩地点

@property (nonatomic, weak) IBOutlet UIButton * creatOrderBtn;///<创建订单

@end
