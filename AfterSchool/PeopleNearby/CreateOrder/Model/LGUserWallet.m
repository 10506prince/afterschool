//
//  LGUserWallet.m
//  AfterSchool
//
//  Created by lg on 15/11/26.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGUserWallet.h"

@implementation LGUserWallet

#pragma mark - Init
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.money = 0;
        self.tokenMoney = 0;
        
        self.spendMoney = 0;
        self.rewardMoney = 0;
    }
    return self;
}

#pragma mark - Out
- (BOOL)isBalanceFull {
    return _money + _tokenMoney >= _spendMoney + _rewardMoney;
}

///订单部分
- (double)isOrderRewardMoney {
    return _rewardMoney;
}

- (double)isOrderTokenMoney {
    double totalSpend = _spendMoney + _rewardMoney;
    
    //
    return _tokenMoney < totalSpend ? _tokenMoney : totalSpend;
}

- (double)isOrderMoney {
    double totalSpend = _spendMoney + _rewardMoney;
    double totalSpendMoney = totalSpend - self.orderTokenMoney;
    
    //
    return _money < totalSpendMoney ? _money : totalSpendMoney;
}
- (double)isOrderNeedMoney {
    double totalMoney = _money + _tokenMoney;
    double totalSpend = _spendMoney + _rewardMoney;
    
    return totalMoney >= totalSpend ? 0 : totalSpend - totalMoney;
}


///显示部分
//- (double)isDisplayTokenMoney {
//    //
//    return _tokenMoney < _spendMoney ? _tokenMoney : _spendMoney;
//}
//- (double)isDisplayMoney {
//
//    double displaySpendMoney = _spendMoney - self.displayTokenMoney;
//    
//    //
//    return _money < displaySpendMoney ? _money : displaySpendMoney;
//}
//
//- (double)isDisplayRewardMoney {
//    return _rewardMoney;
//}
//- (double)isDisplayNeedMoney {
//    return self.orderNeedMoney;
//}



@end
