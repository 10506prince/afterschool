//
//  LGUserWallet.h
//  AfterSchool
//
//  Created by lg on 15/11/26.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <Foundation/Foundation.h>

///计算订单花费金额类
@interface LGUserWallet : NSObject

#pragma mark - In
//用户钱包相关
@property (nonatomic, assign) double money;///<现有零钱
@property (nonatomic, assign) double tokenMoney;///<现有代币

//以下属性主要用来计算订单金额和订单显示用

//创建订单相关
@property (nonatomic, assign) double spendMoney;///<订单花费金额(不包含打赏金额)
@property (nonatomic, assign) double rewardMoney;///<打赏金额

#pragma mark - Out

@property (nonatomic, readonly, getter=isBalanceFull) BOOL balanceFull;///<标志是否需要充值

///订单相关
@property (nonatomic, readonly, getter=isOrderMoney) double orderMoney;///<订单花费零钱
@property (nonatomic, readonly, getter=isOrderTokenMoney) double orderTokenMoney;///<订单花费代币
@property (nonatomic, readonly, getter=isOrderRewardMoney) double orderRewardMoney;///<订单打赏
@property (nonatomic, readonly, getter=isOrderNeedMoney) double orderNeedMoney;

///显示相关
//@property (nonatomic, readonly, getter=isDisplayMoney) double displayMoney;///<显示花费零钱
//@property (nonatomic, readonly, getter=isDisplayTokenMoney) double displayTokenMoney;///<显示花费代币
//@property (nonatomic, readonly, getter=isDisplayRewardMoney) double displayRewardMoney;///<显示打赏金额
//@property (nonatomic, readonly, getter=isDisplayNeedMoney) double displayNeedMoney;///<显示需要充值金额

@end
