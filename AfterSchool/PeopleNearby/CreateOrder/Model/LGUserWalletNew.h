//
//  LGUserWalletNew.h
//  AfterSchool
//
//  Created by lg on 15/12/25.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LGLevelingPrice.h"

///保晋级模式计算订单花费金额类
@interface LGUserWalletNew : NSObject

#pragma mark - In
//用户钱包相关
@property (nonatomic, assign) double money;///<现有零钱
@property (nonatomic, assign) double tokenMoney;///<现有代币

//以下属性主要用来计算订单金额和订单显示用
@property (nonatomic, strong) NSArray <LGLevelingPrice *> * gradePriceList;
@property (nonatomic, assign) NSUInteger startGrade;///<当前段位
@property (nonatomic, assign) NSUInteger endGrade;///<目标段位

#pragma mark - Out
///订单相关
@property (nonatomic, readonly, getter=isOrderMoney) double orderMoney;///<订单花费零钱
@property (nonatomic, readonly, getter=isOrderTokenMoney) double orderTokenMoney;///<订单花费代币
@property (nonatomic, readonly, getter=isOrderRewardMoney) double orderRewardMoney;///<订单打赏

///显示相关
@property (nonatomic, readonly, getter=isDisplayTargetGradeName) NSString * displayTargetGradeName;///<目标段位名字
@property (nonatomic, readonly, getter=isDisplayUseTime) NSUInteger displayTime;///<预计时长
@property (nonatomic, readonly, getter=isDisplayTotalMoney) double displayTotalMoney;///<显示花费总额
@property (nonatomic, readonly, getter=isDisplayMoney) double displayMoney;///<显示花费零钱
@property (nonatomic, readonly, getter=isDisplayTokenMoney) double displayTokenMoney;///<显示花费代币
@property (nonatomic, readonly, getter=isDisplayRewardMoney) double displayRewardMoney;///<显示打赏金额
@property (nonatomic, readonly, getter=isNeedMoney) double displayNeedMoney;///<显示需要充值金额
@property (nonatomic, readonly, getter=isBalanceFull) BOOL balanceFull;///<标志是否需要充值

@end
