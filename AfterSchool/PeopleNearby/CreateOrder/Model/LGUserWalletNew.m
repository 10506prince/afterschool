//
//  LGUserWalletNew.m
//  AfterSchool
//
//  Created by lg on 15/12/25.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGUserWalletNew.h"

@interface LGUserWalletNew ()

@property (nonatomic, assign) double spendMoney;
@property (nonatomic, assign) NSUInteger spendTime;

@property (nonatomic, copy) NSString * targetGradeName;

@end

@implementation LGUserWalletNew

#pragma mark - Init
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.money = 0;
        self.tokenMoney = 0;
        
        self.spendMoney = 0;
        self.spendTime = 0;
        
        self.startGrade = 0;
        self.endGrade = 0;
    }
    return self;
}

#pragma mark - setIn
- (void)setStartGrade:(NSUInteger)startGrade {
    _startGrade = startGrade;
    [self updateSpendMoneyAndTime];
}

- (void)setEndGrade:(NSUInteger)endGrade {
    _endGrade = endGrade;
    [self updateSpendMoneyAndTime];
}

- (void)updateSpendMoneyAndTime {
    double totalPrice = 0;
    double totalTime = 0;
    for (LGLevelingPrice * gradePrice in self.gradePriceList) {
        if (gradePrice.targetGradeId > self.startGrade && gradePrice.targetGradeId <= self.endGrade) {
            totalPrice += gradePrice.reward;
            totalTime += gradePrice.useTime;
            
            if (gradePrice.targetGradeId == self.endGrade) {
                self.targetGradeName = gradePrice.targetGrade;
            }
        }
    }
    
    self.spendMoney = totalPrice;
    self.spendTime = totalTime;
}

#pragma mark - out
- (BOOL)isBalanceFull {
    return _money + _tokenMoney >= self.spendMoney;
}

- (double)isNeedMoney {
    double totalMoney = _money + _tokenMoney;
    double totalSpend = self.spendMoney;
    
    return totalMoney >= totalSpend ? 0 : totalSpend - totalMoney;
}

///显示部分
- (NSString *)isDisplayTargetGradeName {
    return self.targetGradeName;
}

- (NSUInteger)isDisplayUseTime {
    return self.spendTime;
}

- (double)isDisplayTotalMoney {
    return self.spendMoney;
}

- (double)isDisplayTokenMoney {
    //
    return _tokenMoney < self.spendMoney ? _tokenMoney : self.spendMoney;
}

- (double)isDisplayMoney {
    
    double displaySpendMoney = self.spendMoney - self.displayTokenMoney;
    
    //
    return _money < displaySpendMoney ? _money : displaySpendMoney;
}

- (double)isDisplayRewardMoney {
    return 0;
}


///订单部分
- (double)isOrderRewardMoney {
    return 0;
}

- (double)isOrderTokenMoney {
    double totalSpend = self.spendMoney;
    
    //
    return _tokenMoney < totalSpend ? _tokenMoney : totalSpend;
}

- (double)isOrderMoney {
    double totalSpend = self.spendMoney;
    double totalSpendMoney = totalSpend - self.orderTokenMoney;
    
    //
    return _money < totalSpendMoney ? _money : totalSpendMoney;
}

@end
