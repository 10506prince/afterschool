//
//  SearchChatObjectViewController.h
//  AfterSchool
//
//  Created by Teson Draw on 12/17/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchChatObjectRootView.h"

@interface SearchChatObjectViewController : UIViewController <UISearchBarDelegate, UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) SearchChatObjectRootView *rootView;
//@property (nonatomic, strong) NSMutableDictionary *dataSource;

@property (nonatomic, strong) NSDictionary *chatObjectDic;
@property (nonatomic, strong) NSArray *keysArray;

@property (nonatomic, strong) NSMutableArray *dataSource;

@end
