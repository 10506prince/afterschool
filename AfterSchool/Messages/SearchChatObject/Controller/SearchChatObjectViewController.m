//
//  SearchChatObjectViewController.m
//  AfterSchool
//
//  Created by Teson Draw on 12/17/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "SearchChatObjectViewController.h"
#import "MacrosDefinition.h"
#import "NotificationName.h"
#import "TDSingleton.h"

#import "Common.h"
#import "TalkingData.h"

@implementation SearchChatObjectViewController 
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [TalkingData trackPageBegin:selfControllerName];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [TalkingData trackPageEnd:selfControllerName];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initData];
    [self initUserInterface];
}

- (void)initData {
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    _chatObjectDic = [userDefault dictionaryForKey:@"UserInfo"][[TDSingleton instance].account][@"ChatObject"];
    NSLog(@"dataSource:%@", _chatObjectDic);
    _dataSource = [[NSMutableArray alloc] init];
//    _keysArray = [_chatObjectDic allKeys];
}

- (void)initUserInterface {
    self.view.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - 49);
    [self.view addSubview:self.rootView];
    self.automaticallyAdjustsScrollViewInsets = NO;
}

- (SearchChatObjectRootView *)rootView {
    if (!_rootView) {
        _rootView = [[SearchChatObjectRootView alloc] initWithFrame:self.view.bounds];
        _rootView.searchBar.delegate = self;
        [_rootView.searchBar becomeFirstResponder];
        
        _rootView.tableView.delegate = self;
        _rootView.tableView.dataSource = self;
        
        [_rootView.tapGestureRecognizer addTarget:self action:@selector(removeSelf)];
//        _rootView.layer.borderColor = [UIColor blueColor].CGColor;
//        _rootView.layer.borderWidth = 1;
    }
    return _rootView;
}

#pragma mark - UISearchBarDelegate
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    [searchBar setShowsCancelButton:YES animated:YES];
    
    return YES;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
//    [searchBar setShowsCancelButton:NO animated:YES];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
    [searchBar setShowsCancelButton:NO animated:YES];
    searchBar.text = @"";
    [self removeSelf];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    if (searchText.length > 0) {
        [_dataSource removeAllObjects];
        
        NSLog(@"searchText:%@", searchText);
        BOOL found = NO;
        for (NSString *key in _chatObjectDic) {
            NSLog(@"key:%@", key);
            NSDictionary *dic = _chatObjectDic[key];
            NSLog(@"dic:%@", dic);
            NSString *name = [NSString stringWithFormat:@"%@", dic[@"Name"]];
            
            if ([name containsString:searchText]) {
                NSDictionary *element = @{@"Account":key,
                                          @"Name":name,
                                          @"PortraitURL":dic[@"PortraitURL"]};
                [_dataSource addObject:element];
                found = YES;
            }
        }
        
        if (found) {
            self.rootView.tableView.opaque = YES;
//            [self.rootView insertSubview:self.rootView.tableView aboveSubview:self.rootView.maskView];
            
        } else {
//            [self.rootView insertSubview:self.rootView.maskView aboveSubview:self.rootView.tableView];
            self.rootView.tableView.opaque = NO;
        }
    } else {
        [_dataSource removeAllObjects];
        self.rootView.tableView.opaque = NO;
//        [self.rootView insertSubview:self.rootView.maskView aboveSubview:self.rootView.tableView];
    }
    
    [self.rootView.tableView reloadData];
}

#pragma mark UITableView Delegate Method
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *reuseIdentifier = @"resuseIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];

    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    }
    
    cell.textLabel.text = _dataSource[indexPath.row][@"Name"];

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *chatObject = _dataSource[indexPath.row];
    [[NSNotificationCenter defaultCenter] postNotificationName:TDDidSelectChatObject object:chatObject];
    
    [self removeSelf];
}

- (void)dealloc {
    NSLog(@"SearchChatObjectViewController dealloced!");
}

- (void)removeSelf {
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
    [[NSNotificationCenter defaultCenter] postNotificationName:TDRemoveMessageSearchBar object:nil];
}

@end
