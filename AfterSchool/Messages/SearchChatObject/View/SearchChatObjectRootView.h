//
//  SearchChatObjectRootView.h
//  AfterSchool
//
//  Created by Teson Draw on 12/17/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchChatObjectRootView : UIView

@property (nonatomic, strong) UIView *maskView;
@property (nonatomic, strong) UISearchBar *searchBar;
@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) UITapGestureRecognizer *tapGestureRecognizer;

@end
