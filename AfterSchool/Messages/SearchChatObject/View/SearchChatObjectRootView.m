//
//  SearchChatObjectRootView.m
//  AfterSchool
//
//  Created by Teson Draw on 12/17/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "SearchChatObjectRootView.h"
#import "MacrosDefinition.h"
#import "UIImage+ImageWithColor.h"

@implementation SearchChatObjectRootView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithRed:56/255.f green:55/255.f blue:60/255.f alpha:0.90];
        [self addSubview:self.searchBar];
        [self addSubview:self.maskView];
        [self addSubview:self.tableView];
    }
    return self;
}

- (UISearchBar *)searchBar {
    if (!_searchBar) {
        _searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 20, self.bounds.size.width, 44)];
        _searchBar.placeholder = @"搜索";
        _searchBar.keyboardType = UIKeyboardTypeDefault;
        _searchBar.autocapitalizationType = UITextAutocapitalizationTypeNone;
        _searchBar.tintColor = [UIColor blueColor];
        _searchBar.barTintColor = [UIColor colorWithRed:244/255.f green:244/255.f blue:244/255.f alpha:1];
        
        UIImage *image = [UIImage imageWithColor:[UIColor colorWithRed:244/255.f green:244/255.f blue:244/255.f alpha:1]];
        
        _searchBar.backgroundImage = image;
        _searchBar.showsCancelButton = YES;
    }
    return _searchBar;
}

- (UIView *)maskView {
    if (!_maskView) {
        _maskView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_searchBar.frame), self.bounds.size.width, self.bounds.size.height - 64)];
        _maskView.backgroundColor = [UIColor lightGrayColor];
        [_maskView addGestureRecognizer:self.tapGestureRecognizer];
        _maskView.alpha = 0.5;
    }
    
    return _maskView;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 64, self.bounds.size.width, self.bounds.size.height - 64) style:UITableViewStylePlain];
//        _tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectZero];
//        _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
//        _tableView.layer.borderColor = [UIColor blackColor].CGColor;
//        _tableView.layer.borderWidth = 1;
//        _tableView.alpha = 0.5;
//        _tableView.opaque = NO;
    }
    return _tableView;
}

- (UITapGestureRecognizer *)tapGestureRecognizer {
    if (!_tapGestureRecognizer) {
        _tapGestureRecognizer = [[UITapGestureRecognizer alloc] init];
    }
    
    return _tapGestureRecognizer;
}

@end
