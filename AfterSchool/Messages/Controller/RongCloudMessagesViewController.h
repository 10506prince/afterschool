//
//  MessagesViewController.h
//  AfterSchool
//
//  Created by Teson Draw on 10/6/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <RongIMKit/RongIMKit.h>
#import "NavigationBarView.h"

@interface RongCloudMessagesViewController : RCConversationListViewController

@property (nonatomic, strong) NavigationBarView *navigationBarView;
@property (nonatomic, strong) UISearchBar *searchBar;

@property (nonatomic, strong) UIView *tableHeadView;
@property (nonatomic, strong) UIView *maskView;

@property (nonatomic, strong) UITapGestureRecognizer *tapGestureRecognizer;

@property (nonatomic, strong) NSDictionary *chatObjectDic;

//@property (nonatomic, strong) NSMutableDictionary *dataSource;
//@property (nonatomic, strong) NSMutableDictionary *backupDataSource;

@end
