//
//  MessagesViewController.m
//  AfterSchool
//
//  Created by Teson Draw on 10/6/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "RongCloudMessagesViewController.h"
#import "MacrosDefinition.h"
#import "SendMessageViewController.h"
#import "UIImage+ImageWithColor.h"
#import "ConversationListTableViewCell.h"

#import "TDNumberToDate.h"
#import "UIImageView+WebCache.h"
#import "TDSingleton.h"
#import "TDNetworking.h"

#import "SVProgressHUD.h"
#import "LGDaKaInfo.h"
#import "NotificationName.h"

#import "SearchChatObjectViewController.h"
#import "SettingWindowAnimation.h"

#import "Common.h"
#import "TalkingData.h"

#import "AppDelegate.h"

@interface RongCloudMessagesViewController () <UIAlertViewDelegate, UISearchBarDelegate>

@end

@implementation RongCloudMessagesViewController
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [TalkingData trackPageBegin:selfControllerName];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [TalkingData trackPageEnd:selfControllerName];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setAutomaticallyAdjustsScrollViewInsets:NO];
    
    NSLog(@"RongCloudMessagesViewController didLoad");
    [self initData];
    [self initUserInterface];

    [self setDisplayConversationTypes:@[@(ConversationType_PRIVATE), @(ConversationType_PUSHSERVICE), @(ConversationType_SYSTEM)]];
    
    [self addUserIconChangeNotification];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.tabBarController.tabBar setHidden:NO];
    [self updateBadgeValueForTabBarItem];
}

- (void)initData {
//    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
//    _chatObjectDic = [userDefault dictionaryForKey:@"UserInfo"][[TDSingleton instance].account][@"ChatObject"];
    
//    NSLog(@"RongCloudMessagesViewController initData:%@", [userDefault dictionaryForKey:@"UserInfo"]);
//    
//    NSLog(@"RongCloudMessagesViewController _chatObjectDic:%@", _chatObjectDic);
    
    [self updateBadgeValueForTabBarItem];
}

- (void)initUserInterface {
    self.view.backgroundColor = [UIColor whiteColor];
//    [[RCIM sharedRCIM] setUserInfoDataSource:self];
    
    self.navigationController.navigationBarHidden = YES;
    [self.view addSubview:self.navigationBarView];
    
    [RCIM sharedRCIM].globalConversationAvatarStyle = RC_USER_AVATAR_CYCLE;

    self.conversationListTableView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - 49);
    self.conversationListTableView.tableFooterView = [UIView new];
    self.conversationListTableView.tableHeaderView = self.tableHeadView;
    self.conversationListTableView.separatorStyle = UITableViewCellSeparatorStyleNone;

    NSLog(@"self.emptyConversationView.frame:%@", NSStringFromCGRect(self.emptyConversationView.frame));
    CGRect frame = self.emptyConversationView.frame;
    
    CGPoint orgin = CGPointMake((SCREEN_WIDTH  - frame.size.width) /  2, (SCREEN_HEIGHT - 108 - 49 - frame.size.height) / 2 + 44);
    
    frame = CGRectMake(orgin.x, orgin.y, frame.size.width, frame.size.height);
    
    self.emptyConversationView.frame = frame;
    
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(chatWithBigShotInfo:)
//                                                 name:TDChatWithBigShot
//                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(removeSearchBarNotification)
                                                 name:TDRemoveMessageSearchBar
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didSelectChatObject:)
                                                 name:TDDidSelectChatObject
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(deleteChatRecordsNotification:)
                                                 name:TDDeleteChatRecords
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(deleteContactsNotification:)
                                                 name:TDDeleteChatContacts
                                               object:nil];
}

- (NavigationBarView *)navigationBarView {
    if (!_navigationBarView) {

        _navigationBarView = [[NavigationBarView alloc] initWithTitle:@"消息"
                                                           titleColor:nil
                                                      backgroundColor:nil
                                                  leftButtonImageName:@""
                                               leftButtonImageOriginX:10
                                                leftButtonImageHeight:37
                                                    leftButtonOriginX:14
                                                       leftButtonName:nil
                                                 leftButtonTitleColor:nil];
        
        [_navigationBarView.leftButtonImageView sd_setImageWithURL:[NSURL URLWithString:[TDSingleton instance].userInfoModel.portraitURL] placeholderImage:[UIImage imageNamed:@"user_default_head"]];
        
        [_navigationBarView.rightButton addTarget:self action:@selector(addFriendButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        [_navigationBarView.leftButton addTarget:self action:@selector(portraitClicked) forControlEvents:UIControlEventTouchUpInside];
    }
    return _navigationBarView;
}

- (UIView *)tableHeadView {
    if (!_tableHeadView) {
        _tableHeadView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 108)];
        [_tableHeadView addSubview:self.searchBar];
    }
    return _tableHeadView;
}

- (UISearchBar *)searchBar {
    if (!_searchBar) {
        _searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 64, SCREEN_WIDTH, 44)];
        _searchBar.placeholder = @"搜索";
        _searchBar.keyboardType = UIKeyboardTypeDefault;
        _searchBar.autocapitalizationType = UITextAutocapitalizationTypeNone;
        _searchBar.tintColor = [UIColor blueColor];
        _searchBar.barTintColor = [UIColor colorWithRed:244/255.f green:244/255.f blue:244/255.f alpha:1];
        
        UIImage *image = [UIImage imageWithColor:[UIColor colorWithRed:244/255.f green:244/255.f blue:244/255.f alpha:1]];
        
        _searchBar.backgroundImage = image;
        _searchBar.delegate = self;
    }
    return _searchBar;
}

- (UIView *)maskView {
    if (!_maskView) {
        _maskView = [[UIView alloc] initWithFrame:CGRectMake(0, 108, SCREEN_WIDTH, SCREEN_HEIGHT - 108 - 5)];
        _maskView.backgroundColor = [UIColor lightGrayColor];
        _maskView.alpha = 0.5;
//        _maskView.layer.borderColor = [UIColor redColor].CGColor;
//        _maskView.layer.borderWidth = 1;
    }
    
    return _maskView;
}

- (UITapGestureRecognizer *)tapGestureRecognizer {
    if (!_tapGestureRecognizer) {
        _tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(searchBarMoveBack)];
    }
    
    return _tapGestureRecognizer;
}

- (void)addFriendButtonClicked {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"请输入对方账号" message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alertView show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    UITextField *textField = [alertView textFieldAtIndex:0];
    
    if (buttonIndex == 1) {

        NSLog(@"tf:%@", textField.text);
        
        NSString *targetId = [textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
        if (![[TDSingleton instance].account isEqualToString:targetId]) {
            [self requestUserInfoWithIdentifier:targetId];
        }
    }
    
    [textField resignFirstResponder];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

/**
 *  表格选中事件，子类从重写这个方法来监听点击事件并做相应的业务处理
 *
 *  @param conversationModelType 数据模型类型
 *  @param model                 数据模型
 *  @param indexPath             索引
 */
-(void)onSelectedTableRow:(RCConversationModelType)conversationModelType conversationModel:(RCConversationModel *)model atIndexPath:(NSIndexPath *)indexPath
{
    if (model.conversationModelType == RC_CONVERSATION_MODEL_TYPE_NORMAL)
    {
        NSLog(@"RC_CONVERSATION_MODEL_TYPE_NORMAL");
    }
    else if(model.conversationModelType == RC_CONVERSATION_MODEL_TYPE_COLLECTION)
    {
        RongCloudMessagesViewController *listVC = [[RongCloudMessagesViewController alloc] initWithDisplayConversationTypes:@[[NSNumber numberWithInt:ConversationType_GROUP]]collectionConversationType:nil];
        
        listVC.hidesBottomBarWhenPushed = YES;
        
        [self.navigationController pushViewController:listVC animated:YES];
    }
    else if(model.conversationModelType == RC_CONVERSATION_MODEL_TYPE_CUSTOMIZATION)
    {
        NSLog(@"RC_CONVERSATION_MODEL_TYPE_CUSTOMIZATION");
        SendMessageViewController *conversationVC = [[SendMessageViewController alloc] init];
        
        conversationVC.entryMode = 1;
        conversationVC.conversationType = model.conversationType;
        
        conversationVC.targetId = model.targetId;
        
        RCUserInfo *user = (RCUserInfo *)model.extend;
        if (user) {
            conversationVC.userName = user.name;
        } else {
            conversationVC.userName = model.targetId;
        }
        
        conversationVC.hidesBottomBarWhenPushed = YES;
#warning message
//        [self getUserInfoWithUserId:model.targetId completion:^(RCUserInfo *userInfo) {
//            
//        }];
        
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        [appDelegate getUserInfoWithUserId:model.targetId completion:^(RCUserInfo *userInfo) {
            
        }];
        
        [self.navigationController pushViewController:conversationVC animated:YES];
    }
    else if(model.conversationModelType == RC_CONVERSATION_MODEL_TYPE_PUBLIC_SERVICE)
    {
        NSLog(@"RC_CONVERSATION_MODEL_TYPE_PUBLIC_SERVICE");
    }
}

/**
 *  将要加载table数据，子类通过重写该方法添加自定义的数据源，然后通过属性  conversationListDataSource 来读取
 *
 *  @param dataSource 数据源数组
 *
 *  @return 数据源数组，可以添加自己定义的数据源item
 */
- (NSMutableArray *)willReloadTableData:(NSMutableArray *)dataSource
{
    for (int i=0; i<dataSource.count; i++) {
        
        RCConversationModel *model = [dataSource objectAtIndex:i];
        
        NSLog(@"model.targetId:%@", model.targetId);
        if ([[_chatObjectDic allKeys] containsObject:model.targetId]) {
            NSDictionary *dic = [_chatObjectDic objectForKey:model.targetId];
            
            RCUserInfo *user = [[RCUserInfo alloc]init];
            user.userId = model.targetId;
            user.name = [NSString stringWithFormat:@"%@", dic[@"Name"]];
            user.portraitUri = [NSString stringWithFormat:@"%@", dic[@"PortraitURL"]];
            model.extend = user;
        }
        
        model.conversationModelType = RC_CONVERSATION_MODEL_TYPE_CUSTOMIZATION;
    }
    
    return dataSource;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (CGFloat)rcConversationListTableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 86;
}

- (RCConversationBaseCell *)rcConversationListTableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *reuseIdentifier = @"cellidentifier";
    
    RCConversationModel *model = self.conversationListDataSource[indexPath.row];

    ConversationListTableViewCell *cell = [[ConversationListTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    
    if (!cell) {
        cell = [[ConversationListTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    }

    RCUserInfo *user = (RCUserInfo *)model.extend;
    
    cell.nameLabel.text = user.name;
    
    if ([user.userId isEqualToString:@"system"]) {
        [cell.portraitImageView sd_setImageWithURL:[NSURL URLWithString:user.portraitUri]];
        cell.nameLabel.text = @"系统消息";
    } else {
        if (user) {
            if (user.portraitUri.length == 0) {
                cell.portraitImageView.image = [UIImage imageNamed:@"user_default_head"];
            } else {
                [cell.portraitImageView sd_setImageWithURL:[NSURL URLWithString:user.portraitUri]];
            }
        } else {
            cell.portraitImageView.image = [UIImage imageNamed:@"user_default_head"];
        }
    }
    
    cell.nameLabel.text = user.name;
    cell.timeLabel.text = [TDNumberToDate dateFromNumber:model.sentTime];
    
    
    if ([model.lastestMessage isKindOfClass:[RCTextMessage class]]) {
        RCTextMessage *textMsg = (RCTextMessage *)model.lastestMessage;
        cell.lastMessageLabel.text = textMsg.content;
    } else if ([model.lastestMessage isKindOfClass:[RCImageMessage class]]) {
        cell.lastMessageLabel.text = @"[图片]";
    } else if ([model.lastestMessage isKindOfClass:[RCVoiceMessage class]]) {
        cell.lastMessageLabel.text = @"[语音]";
    } else if ([model.lastestMessage isKindOfClass:[RCLocationMessage class]]) {
        cell.lastMessageLabel.text = @"[位置]";
    } else if ([model.lastestMessage isKindOfClass:[RCDiscussionNotificationMessage class]]) {
        cell.lastMessageLabel.text = @"通知";
    } else {
        cell.lastMessageLabel.text = @"";
    }

    
    NSLog(@"model.unreadMessageCount:%ld", (long)model.unreadMessageCount);
    
    if (model.unreadMessageCount == 0) {
//        [cell addSubview:cell.messageNumberBackgroundImageView];
        [cell.messageNumberBackgroundImageView removeFromSuperview];
//        [cell.messageNumberLabel removeFromSuperview];
    } else {
        [cell addSubview:cell.messageNumberBackgroundImageView];
        [cell.messageNumberBackgroundImageView addSubview:cell.messageNumberLabel];
        
        cell.messageNumberLabel.text = [NSString stringWithFormat:@"%ld", (long)model.unreadMessageCount];
//        [cell.messageNumberBackgroundImageView addSubview:cell.messageNumberLabel];
    }

    return cell;
}

#pragma mark - 收到消息监听
- (void)didReceiveMessageNotification:(NSNotification *)notification
{
    __weak typeof(&*self) blockSelf_ = self;
    //处理好友请求
    RCMessage *message = notification.object;
    

    NSLog(@"message.targetId:%@", message.targetId);
    NSLog(@"message.senderUserId:%@", message.senderUserId);

    if ([message.targetId isEqualToString:@"系统消息"]) {
        
        NSURL *imageURL;
        
        if (iPhone6Plus) {
            imageURL = [[NSBundle mainBundle] URLForResource:@"message_system_message@3x" withExtension:@"png"];
        } else {
            imageURL = [[NSBundle mainBundle] URLForResource:@"message_system_message@2x" withExtension:@"png"];
        }
        
        NSDictionary *userInfoDic = @{@"account":message.targetId,
                                      @"nickName":message.targetId,
                                      @"icon":[NSString stringWithFormat:@"%@", imageURL]};
        [self processDataWithDictionary:userInfoDic];
        
        RCUserInfo *user = [[RCUserInfo alloc] init];
        user.userId = message.targetId;
        user.name = message.targetId;

        user.portraitUri = [NSString stringWithFormat:@"%@", imageURL];
        
        RCConversationModel *customModel = [RCConversationModel new];
        customModel.conversationModelType = RC_CONVERSATION_MODEL_TYPE_CUSTOMIZATION;
        customModel.extend = user;
        customModel.senderUserId = message.senderUserId;
        customModel.lastestMessage = message.content;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            //调用父类刷新未读消息数
            [blockSelf_ refreshConversationTableViewWithConversationModel:customModel];
//            [blockSelf_ resetConversationListBackgroundViewIfNeeded];
            [self updateBadgeValueForTabBarItem];
            
            NSNumber *left = [notification.userInfo objectForKey:@"left"];
            if (0 == left.integerValue) {
                [super refreshConversationTableViewIfNeeded];
            }
        });
        
    } else if ([message.targetId isEqualToString:@"订单推送"]) {
        NSURL *imageURL;
        
        if (iPhone6Plus) {
            imageURL = [[NSBundle mainBundle] URLForResource:@"message_order@3x" withExtension:@"png"];
        } else {
            imageURL = [[NSBundle mainBundle] URLForResource:@"message_order@2x" withExtension:@"png"];
        }
        
        NSDictionary *userInfoDic = @{@"account":message.targetId,
                                      @"nickName":message.targetId,
                                      @"icon":[NSString stringWithFormat:@"%@", imageURL]};
        
        [self processDataWithDictionary:userInfoDic];
        
        RCUserInfo *user = [[RCUserInfo alloc] init];
        user.userId = message.targetId;
        user.name = message.targetId;
//        user.name = @"约单推送";
        user.portraitUri = [NSString stringWithFormat:@"%@", imageURL];
        
        RCConversationModel *customModel = [RCConversationModel new];
        customModel.conversationModelType = RC_CONVERSATION_MODEL_TYPE_CUSTOMIZATION;
        customModel.extend = user;
        customModel.senderUserId = message.senderUserId;
        customModel.lastestMessage = message.content;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            //调用父类刷新未读消息数
            [blockSelf_ refreshConversationTableViewWithConversationModel:customModel];
//            [blockSelf_ resetConversationListBackgroundViewIfNeeded];
            [self updateBadgeValueForTabBarItem];
            
            NSNumber *left = [notification.userInfo objectForKey:@"left"];
            if (0 == left.integerValue) {
                [super refreshConversationTableViewIfNeeded];
            }
        });
    } else {
        NSDictionary *parameters = @{@"action":@"getUserSimple",
                                     @"user":message.targetId,
                                     @"sessionKey":[TDSingleton instance].sessionKey};
        
        [TDNetworking requestWithURL:[TDSingleton instance].gatewayServerBusinessURL
                          parameters:parameters
                             success:^(id responseObject)
         {
             if ([responseObject[@"result"] integerValue] == 1) {
                 [SVProgressHUD dismiss];
                 NSDictionary *userInfoDic = responseObject[@"userInfo"];
                 [self processDataWithDictionary:userInfoDic];
                 
                 RCUserInfo *user = [[RCUserInfo alloc] init];
                 user.userId = message.targetId;
//                 user.name = [NSString stringWithFormat:@"%@", userInfoDic[@"Name"]];
//                 user.portraitUri = [NSString stringWithFormat:@"%@", userInfoDic[@"PortraitURL"]];
                 user.name = [NSString stringWithFormat:@"%@", userInfoDic[@"nickName"]];
                 user.portraitUri = [NSString stringWithFormat:@"%@", userInfoDic[@"icon"]];
                 
                 RCConversationModel *customModel = [RCConversationModel new];
                 customModel.conversationModelType = RC_CONVERSATION_MODEL_TYPE_CUSTOMIZATION;
                 customModel.extend = user;
                 customModel.senderUserId = message.senderUserId;
                 customModel.lastestMessage = message.content;
                 
                 dispatch_async(dispatch_get_main_queue(), ^{
                     //调用父类刷新未读消息数
                     [blockSelf_ refreshConversationTableViewWithConversationModel:customModel];
//                     [blockSelf_ resetConversationListBackgroundViewIfNeeded];
                     [self updateBadgeValueForTabBarItem];
                     
                     NSNumber *left = [notification.userInfo objectForKey:@"left"];
                     if (0 == left.integerValue) {
                         [super refreshConversationTableViewIfNeeded];
                     }
                 });
                 
             } else {
                 [SVProgressHUD dismissWithError:[NSString stringWithFormat:@"%@", responseObject[@"msg"]]];
             }
         } failure:^(NSError *error) {
             NSLog(@"didReceiveMessageNotification error:%@", error);
             [SVProgressHUD dismissWithError:[NSString stringWithFormat:@"%@", error.description]];
         }];
    }
}

//- (void)getUserInfoWithUserId:(NSString *)userId
//                   completion:(void (^)(RCUserInfo *))completion {
//    NSLog(@"userId:%@", userId);
//
//    if ([[_chatObjectDic allKeys] containsObject:userId]) {
//        NSDictionary *dic = [_chatObjectDic objectForKey:userId];
//        
//        RCUserInfo *user = [[RCUserInfo alloc]init];
//        user.userId = userId;
//        user.name = [NSString stringWithFormat:@"%@", dic[@"Name"]];
//        user.portraitUri = [NSString stringWithFormat:@"%@", dic[@"PortraitURL"]];
//        
//        return completion(user);
//    } else if ([[TDSingleton instance].account isEqualToString:userId]) {
//        RCUserInfo *user = [[RCUserInfo alloc]init];
//        user.userId = userId;
//        
//        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
//        
//        NSMutableDictionary *userInfoDic = [[NSMutableDictionary alloc] initWithDictionary:[userDefault dictionaryForKey:@"UserInfo"]];
//        
//        NSMutableDictionary *accountDic = [[NSMutableDictionary alloc] initWithDictionary:userInfoDic[[TDSingleton instance].account]];
//        
//        user.name = [NSString stringWithFormat:@"%@", accountDic[@"Name"]];
//        user.portraitUri = [NSString stringWithFormat:@"%@", accountDic[@"PortraitURL"]];
//        
//        return completion(user);
//    }
//    
//    return completion(nil);
//}

- (void)requestUserInfoWithIdentifier:(NSString *)identifier {
    [SVProgressHUD showWithStatus:@"正在查询，请等待" maskType:2];
    
    NSDictionary *parameters = @{@"action":@"getUserSimple",
                                 @"user":identifier,
                                 @"sessionKey":[TDSingleton instance].sessionKey};
    
    NSLog(@"requestUserInfoWithIdentifier parameters:%@", parameters);
    NSLog(@"requestUserInfoWithIdentifier url:%@", [TDSingleton instance].gatewayServerBusinessURL);
    [TDNetworking requestWithURL:[TDSingleton instance].gatewayServerBusinessURL
                      parameters:parameters
                         success:^(id responseObject)
     {
         NSLog(@"requestUserInfoWithIdentifier result:%@", responseObject);
         if ([responseObject[@"result"] integerValue] == 1) {
             [SVProgressHUD dismiss];
             NSDictionary *userInfoDic = responseObject[@"userInfo"];
             [self processDataWithDictionary:userInfoDic];
             
             NSString *account = [NSString stringWithFormat:@"%@", userInfoDic[@"account"]];
             NSString *name = [NSString stringWithFormat:@"%@", userInfoDic[@"nickName"]];
             [self enterChatWindowWithTargetId:account userName:name];
         } else {
             [SVProgressHUD dismissWithError:[NSString stringWithFormat:@"%@", responseObject[@"msg"]]];
         }
     } failure:^(NSError *error) {
         [SVProgressHUD dismissWithError:[NSString stringWithFormat:@"%@", error]];
         NSLog(@"requestUserInfoWithIdentifier error:%@", error);
     }];
}

- (void)processDataWithDictionary:(NSDictionary *)dic {
    NSString *account = [NSString stringWithFormat:@"%@", dic[@"account"]];
    NSString *name = [NSString stringWithFormat:@"%@", dic[@"nickName"]];
    NSString *portraitURL = [NSString stringWithFormat:@"%@", dic[@"icon"]];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    
    NSMutableDictionary *userInfoDic = [[NSMutableDictionary alloc] initWithDictionary:[userDefault dictionaryForKey:@"UserInfo"]];
    
    NSMutableDictionary *accountDic = [[NSMutableDictionary alloc] initWithDictionary:userInfoDic[[TDSingleton instance].account]];
    
    
    NSMutableDictionary *chatObjectDic = [[NSMutableDictionary alloc] initWithDictionary:accountDic[@"ChatObject"]];
    
    NSDictionary *userDic = @{@"Name":name,
                          @"PortraitURL":portraitURL};
    [chatObjectDic setObject:userDic forKey:account];
    
    [accountDic setObject:chatObjectDic forKey:@"ChatObject"];
    
    [userInfoDic setObject:accountDic forKey:[TDSingleton instance].account];
    
    [userDefault setObject:userInfoDic forKey:@"UserInfo"];
    
    [userDefault synchronize];
    
    _chatObjectDic = chatObjectDic;
    
    NSLog(@"processDataWithDictionary _chatObjectDic:%@", _chatObjectDic);
}

- (void)enterChatWindowWithTargetId:(NSString *)targetId userName:(NSString *)userName {
    SendMessageViewController *vc = [[SendMessageViewController alloc]init];
    vc.conversationType = ConversationType_PRIVATE;
    vc.targetId = targetId;
    vc.userName = userName;
    
    vc.entryMode = 1;
    
    [self.navigationController pushViewController:vc animated:YES];
}

//左滑删除
-(void)rcConversationListTableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [super rcConversationListTableView:tableView commitEditingStyle:editingStyle forRowAtIndexPath:indexPath];
    //可以从数据库删除数据
    RCConversationModel *model = self.conversationListDataSource[indexPath.row];
    
    /**
     *  从本地数据库删除
     */
     
    [self deleteUserDefaultsWithIdentifier:model.targetId];
    
    /**
     *  从融云数据库删除
     */
    if ([model.targetId isEqualToString:@"system"] || [model.targetId isEqualToString:@"message_order"]) {
        [[RCIMClient sharedRCIMClient] removeConversation:ConversationType_SYSTEM targetId:model.targetId];
        [[RCIMClient sharedRCIMClient] clearMessages:ConversationType_SYSTEM targetId:model.targetId];
    } else {
        [[RCIMClient sharedRCIMClient] removeConversation:ConversationType_PRIVATE targetId:model.targetId];
        [[RCIMClient sharedRCIMClient] clearMessages:ConversationType_PRIVATE targetId:model.targetId];
    }

    [self.conversationListDataSource removeObjectAtIndex:indexPath.row];
    [self.conversationListTableView reloadData];
}

- (void)deleteUserDefaultsWithIdentifier:(NSString *)identifier {
    if ([[_chatObjectDic allKeys] containsObject:identifier]) {
        NSMutableDictionary *tempDic = [[NSMutableDictionary alloc] initWithDictionary:_chatObjectDic];
        [tempDic removeObjectForKey:identifier];
        _chatObjectDic = tempDic;
        
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        NSLog(@"before deleteUserDefaultsWithIdentifier userDefault:%@", [userDefault dictionaryForKey:@"UserInfo"]);
        
        
        NSMutableDictionary *userInfoDic = [[NSMutableDictionary alloc] initWithDictionary:[userDefault dictionaryForKey:@"UserInfo"]];
        
        NSMutableDictionary *accountDic = [[NSMutableDictionary alloc] initWithDictionary:userInfoDic[[TDSingleton instance].account]];
        
        [accountDic setObject:_chatObjectDic forKey:@"ChatObject"];

        [userInfoDic setObject:accountDic forKey:[TDSingleton instance].account];
        
        [userDefault setObject:userInfoDic forKey:@"UserInfo"];
        [userDefault synchronize];
        
        NSLog(@"after deleteUserDefaultsWithIdentifier userDefault:%@", [userDefault dictionaryForKey:@"UserInfo"]);
    }
}

- (void)deleteChatRecordsNotification:(NSNotification *)notification {
    NSLog(@"deleteChatRecordWithNotification:%@", notification.object);
    NSString *account = notification.object;
    
    if ([account isEqualToString:@"system"] || [account isEqualToString:@"message_order"]) {
        [[RCIMClient sharedRCIMClient] clearMessages:ConversationType_SYSTEM targetId:account];
    } else {
        [[RCIMClient sharedRCIMClient] clearMessages:ConversationType_PRIVATE targetId:account];
    }
}

- (void)deleteContactsNotification:(NSNotification *)notification {
    NSLog(@"deleteChatRecordWithNotification:%@", notification.object);
    NSString *account = notification.object;
    
    /**
     *  从融云数据库删除
     */
    if ([account isEqualToString:@"system"] || [account isEqualToString:@"message_order"]) {
        [[RCIMClient sharedRCIMClient] removeConversation:ConversationType_SYSTEM targetId:account];
        [[RCIMClient sharedRCIMClient] clearMessages:ConversationType_SYSTEM targetId:account];
    } else {
        [[RCIMClient sharedRCIMClient] removeConversation:ConversationType_PRIVATE targetId:account];
        [[RCIMClient sharedRCIMClient] clearMessages:ConversationType_PRIVATE targetId:account];
    }
    
    [self deleteUserDefaultsWithIdentifier:account];
    [self.conversationListTableView reloadData];
}

- (void)updateBadgeValueForTabBarItem
{
    int count = [[RCIMClient sharedRCIMClient]getUnreadCount:self.displayConversationTypeArray];
    
    if (count > 0) {
        [[[[self.tabBarController tabBar] items] objectAtIndex:1] setBadgeValue:[[NSString alloc]initWithFormat:@"%d", count]];
    } else {
        [[[[self.tabBarController tabBar] items] objectAtIndex:1] setBadgeValue:nil];
    }
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.searchBar resignFirstResponder];
}

//- (void)chatWithBigShotInfo:(NSNotification *)sender {
//    LGDaKaInfo *data = sender.object;
//
//    NSDictionary *dic = @{@"account":data.account,
//                          @"nickName":data.nickName,
//                          @"icon":data.headImageUrl};
//    
//    [self processDataWithDictionary:dic];
//    
//    [self enterChatWindowWithTargetId:data.account userName:data.nickName];
//
//#warning message
////    [self getUserInfoWithUserId:data.account completion:^(RCUserInfo *userInfo) {
////        
////    }];
//}

#pragma mark - UISearchBarDelegate
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    [searchBar setShowsCancelButton:YES animated:YES];
    [self searchBarMoveToTop];
    
    return YES;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
    [searchBar setShowsCancelButton:NO animated:YES];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [searchBar setShowsCancelButton:NO animated:YES];
    [self searchBarMoveBack];
}

- (void)searchBarMoveToTop {
    [UIView beginAnimations:@"SearchBarMoveToTop" context:nil];
    [UIView setAnimationDuration:0.3];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(addSearchChatObjectVC)];
    
    self.conversationListTableView.frame = CGRectMake(0, -44, SCREEN_WIDTH, SCREEN_HEIGHT - 49 + 44);
    [self.conversationListTableView addSubview:self.maskView];

    self.navigationBarView.frame = CGRectMake(0, -64, SCREEN_WIDTH, 64);
    _tableHeadView.backgroundColor = [UIColor colorWithRed:56/255.f green:55/255.f blue:60/255.f alpha:0.90];
    
    [UIView commitAnimations];
}

- (void)searchBarMoveBack {
    [self.searchBar resignFirstResponder];
    [self.searchBar setShowsCancelButton:NO animated:YES];
    [UIView beginAnimations:@"SearchBarMoveToTop" context:nil];
    [UIView setAnimationDuration:0.3];
    self.conversationListTableView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - 49);
    
    self.navigationBarView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 64);
    _tableHeadView.backgroundColor = [UIColor clearColor];
    
    [UIView commitAnimations];
}

- (void)addSearchChatObjectVC {
    [self.maskView removeFromSuperview];
    SearchChatObjectViewController *vc = [[SearchChatObjectViewController alloc] init];
    [self addChildViewController:vc];
    [self.view addSubview:vc.view];
}

- (void)didSelectChatObject:(NSNotification *)notification {
    NSLog(@"object:%@", notification.object);
    NSDictionary *chatObject = notification.object;

    NSString *account = chatObject[@"Account"];
    NSString *name = chatObject[@"Name"];
    
    [self enterChatWindowWithTargetId:account userName:name];

#warning message
//    [self getUserInfoWithUserId:account completion:^(RCUserInfo *userInfo) {
//        
//    }];
}

- (void)removeSearchBarNotification {
    [self searchBarMoveBack];
}

//- (void)portraitClicked {
//    
//    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureAction)];
//    [self.tabBarController.view addGestureRecognizer:tapGesture];
//    
//    [SettingWindowAnimation showWithView:self.tabBarController.view];
//}
//
//- (void)tapGestureAction {
//
//    [SettingWindowAnimation hideWithView:self.tabBarController.view];
//    
//    for (UITapGestureRecognizer *recognizer in [self.tabBarController.view gestureRecognizers]) {
//        [self.tabBarController.view removeGestureRecognizer:recognizer];
//    }
//}

#pragma mark - 左滑
- (void)portraitClicked {
    
    /**
     *  swipe gesture
     */
    UISwipeGestureRecognizer *swipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(gestureAction:)];
    [swipeGesture setDirection:UISwipeGestureRecognizerDirectionLeft|UISwipeGestureRecognizerDirectionRight];
    
    /**
     *  tap gesture
     */
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gestureAction:)];
    tapGesture.numberOfTapsRequired = 1;
    
    /**
     *  屏蔽视图
     */
    UIView * maskView = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    [maskView setBackgroundColor:[UIColor clearColor]];
    maskView.tag = 1000;
    [maskView addGestureRecognizer:swipeGesture];
    [maskView addGestureRecognizer:tapGesture];
    
    [self.tabBarController.view addSubview:maskView];
    
    [SettingWindowAnimation showWithView:self.tabBarController.view];
}

- (void)gestureAction:(UIGestureRecognizer *)sender {
    
    [SettingWindowAnimation hideWithView:self.tabBarController.view];
    
    UIView * maskView = sender.view;
    
    [maskView removeGestureRecognizer:sender];
    [maskView removeFromSuperview];
}

///添加定位服务通知
- (void)addUserIconChangeNotification {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateUserIcon:) name:LGSetUserIcon object:nil];
}

#pragma mark - 头像改变通知
- (void)didUpdateUserIcon:(NSNotification *)notifi {
    NSURL * url = [NSURL URLWithString:notifi.object];
    [self.navigationBarView.leftButtonImageView sd_setImageWithURL:url];
}
@end
