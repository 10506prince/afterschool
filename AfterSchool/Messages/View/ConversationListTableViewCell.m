//
//  ConversationListTableViewCell.m
//  AfterSchool
//
//  Created by Teson Draw on 10/24/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "ConversationListTableViewCell.h"
#import "MacrosDefinition.h"

@implementation ConversationListTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
//        self.layer.borderColor = [UIColor blackColor].CGColor;
//        self.layer.borderWidth = 1;
        
        [self addSubview:self.portraitBackgroundImageView];
        [_portraitBackgroundImageView addSubview:self.portraitImageView];
        
        [self addSubview:self.nameLabel];
        [self addSubview:self.lastMessageLabel];
        [self addSubview:self.timeLabel];
        [self addSubview:self.messageNumberBackgroundImageView];
        [self addSubview:self.separatorView];
    }
    return self;
}

- (UIImageView *)portraitBackgroundImageView {
    if (!_portraitBackgroundImageView) {
        _portraitBackgroundImageView = [[UIImageView alloc] initWithFrame:CGRectMake(20, (86 - 50) / 2, 50, 50)];
        _portraitBackgroundImageView.userInteractionEnabled = YES;
        
        _portraitBackgroundImageView.layer.cornerRadius = 6;
        _portraitBackgroundImageView.layer.masksToBounds = YES;
        
        _portraitBackgroundImageView.backgroundColor = [UIColor whiteColor];
        
        _portraitBackgroundImageView.layer.borderColor = [UIColor colorWithRed:224/255.f green:224/255.f blue:224/255.f alpha:1].CGColor;
        _portraitBackgroundImageView.layer.borderWidth = 0.5;
    }
    return _portraitBackgroundImageView;
}

- (UIImageView *)portraitImageView {
    if (!_portraitImageView) {
        _portraitImageView = [[UIImageView alloc] initWithFrame:CGRectMake(2, 2, 46, 46)];
        _portraitImageView.userInteractionEnabled = YES;
        
        _portraitImageView.layer.cornerRadius = 4;
        _portraitImageView.layer.masksToBounds = YES;
    }
    return _portraitImageView;
}

- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_portraitBackgroundImageView.frame) + 11, 25, 170, 18)];
        _nameLabel.font = [UIFont systemFontOfSize:16];
        _nameLabel.textColor = [UIColor colorWithRed:34/255.f green:34/255.f blue:34/255.f alpha:1];
//        _nameLabel.layer.borderColor = [UIColor blackColor].CGColor;
//        _nameLabel.layer.borderWidth = 1;
    }
    return _nameLabel;
}

- (UILabel *)lastMessageLabel {
    if (!_lastMessageLabel) {
        _lastMessageLabel = [[UILabel alloc] initWithFrame:CGRectMake(_nameLabel.frame.origin.x, CGRectGetMaxY(_nameLabel.frame) + 10, SCREEN_WIDTH - _nameLabel.frame.origin.x - 20 - 70, 20)];
        _lastMessageLabel.font = [UIFont systemFontOfSize:14];
        _lastMessageLabel.textColor = [UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1];
//        _lastMessageLabel.layer.borderColor = [UIColor blackColor].CGColor;
//        _lastMessageLabel.layer.borderWidth = 1;
    }
    return _lastMessageLabel;
}

- (UILabel *)timeLabel {
    if (!_timeLabel) {
        _timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - 20 - 70, 28, 70, 14)];
        _timeLabel.font = [UIFont systemFontOfSize:10];
        _timeLabel.textColor = [UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1];
        _timeLabel.textAlignment = NSTextAlignmentRight;
//        _timeLabel.layer.borderColor = [UIColor blackColor].CGColor;
//        _timeLabel.layer.borderWidth = 1;
    }
    return _timeLabel;
}

- (UILabel *)messageNumberLabel {
    if (!_messageNumberLabel) {
        _messageNumberLabel = [[UILabel alloc] initWithFrame:self.messageNumberBackgroundImageView.bounds];
        _messageNumberLabel.font = [UIFont systemFontOfSize:12];
        _messageNumberLabel.textColor = [UIColor whiteColor];
        _messageNumberLabel.backgroundColor = [UIColor clearColor];
//        _messageNumberLabel.layer.cornerRadius = 9;
//        _messageNumberLabel.layer.masksToBounds = YES;
        _messageNumberLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _messageNumberLabel;
}

- (UIImageView *)messageNumberBackgroundImageView {
    if (!_messageNumberBackgroundImageView) {
        _messageNumberBackgroundImageView = [[UIImageView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - 20 - 19, 86 - 19 - 17.5, 18, 18)];
        _messageNumberBackgroundImageView.layer.cornerRadius = 9;
        _messageNumberBackgroundImageView.layer.masksToBounds = YES;
        _messageNumberBackgroundImageView.backgroundColor = [UIColor colorWithRed:241/255.f green:5/255.f blue:5/255.f alpha:1];
    }
    return _messageNumberBackgroundImageView;
}

//- (UIView *)messageNumberBackgroundView {
//    if (!_messageNumberBackgroundView) {
//        _messageNumberBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_portraitBackgroundImageView.frame) - 9, _portraitBackgroundImageView.frame.origin.y - 9, 18, 18)];
//    }
//    return _messageNumberBackgroundView;
//}


- (UIImageView *)noDisturbImageView {
    if (!_noDisturbImageView) {
        _noDisturbImageView = [[UIImageView alloc] initWithFrame:self.messageNumberBackgroundImageView.bounds];
        _noDisturbImageView.userInteractionEnabled = YES;
    }
    return _noDisturbImageView;
}

- (UIView *)separatorView {
    if (!_separatorView) {
        _separatorView = [[UIView alloc] initWithFrame:CGRectMake(20, 86 - 1, SCREEN_WIDTH - 20, 1)];
        _separatorView.backgroundColor = [UIColor colorWithRed:234/255.f green:234/255.f blue:234/255.f alpha:1];
    }
    return _separatorView;
}

@end
