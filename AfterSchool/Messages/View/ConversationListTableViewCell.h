//
//  ConversationListTableViewCell.h
//  AfterSchool
//
//  Created by Teson Draw on 10/24/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <RongIMKit/RCConversationCell.h>

@interface ConversationListTableViewCell : RCConversationBaseCell

@property (nonatomic, strong) UIImageView *portraitBackgroundImageView;
@property (nonatomic, strong) UIImageView *portraitImageView;

@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *lastMessageLabel;
@property (nonatomic, strong) UILabel *timeLabel;


@property (nonatomic, strong) UILabel *messageNumberLabel;
@property (nonatomic, strong) UIImageView *messageNumberBackgroundImageView;
//@property (nonatomic, strong) UIView  *messageNumberBackgroundView;

@property (nonatomic, strong) UIImageView *noDisturbImageView;//免打扰图片视图

@property (nonatomic, strong) UIView *separatorView;

@end
