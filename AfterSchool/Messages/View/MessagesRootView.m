//
//  MessagesRootView.m
//  AfterSchool
//
//  Created by Teson Draw on 10/6/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "MessagesRootView.h"
#import "MacrosDefinition.h"
#import "UIImage+ImageWithColor.h"

@implementation MessagesRootView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        
        [self addSubview:self.navigationBarView];
        [self addSubview:self.tableView];
    }
    return self;
}

- (NavigationBarView *)navigationBarView {
    if (!_navigationBarView) {
        _navigationBarView = [[NavigationBarView alloc] initWithTitle:@"消息" titleColor:[UIColor whiteColor] backgroundColor:[UIColor colorWithRed:56/255.f green:55/255.f blue:60/255.f alpha:0.90] leftButtonName:nil leftButtonTitleColor:[UIColor whiteColor]  leftButtonImageName:nil rightButtonName:nil rightButtonImageName:@"cross_add_button"];
    }
    return _navigationBarView;
}

- (UISearchBar *)searchBar {
    if (!_searchBar) {
        _searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 44)];
        _searchBar.placeholder = @"搜索";
        _searchBar.keyboardType = UIKeyboardTypeDefault;
        _searchBar.autocapitalizationType = UITextAutocapitalizationTypeNone;
        _searchBar.barTintColor = [UIColor colorWithRed:244/255.f green:244/255.f blue:244/255.f alpha:1];
        
        UIImage *image = [UIImage imageWithColor:[UIColor colorWithRed:244/255.f green:244/255.f blue:244/255.f alpha:1]];

        _searchBar.backgroundImage = image;
    }
    return _searchBar;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 64, SCREEN_WIDTH, SCREEN_HEIGHT - 64 - 49)];
        _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.tableHeaderView = self.searchBar;
    }
    return _tableView;
}

@end
