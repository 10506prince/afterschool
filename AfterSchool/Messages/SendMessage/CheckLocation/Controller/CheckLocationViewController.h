//
//  CheckLocationViewController.h
//  AfterSchool
//
//  Created by Teson Draw on 11/4/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CheckLocationRootView.h"
#import <RongIMKit/RongIMKit.h>

@interface CheckLocationViewController : UIViewController

@property (nonatomic, strong) CheckLocationRootView *rootView;
@property (nonatomic, strong) RCLocationMessage * locationMessageContent;

@property (nonatomic, strong) NSArray *annotatians;

@end
