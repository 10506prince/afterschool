//
//  CheckLocationViewController.m
//  AfterSchool
//
//  Created by Teson Draw on 11/4/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "CheckLocationViewController.h"
#import "MacrosDefinition.h"

#import "Common.h"
#import "TalkingData.h"

@interface CheckLocationViewController ()

@end

@implementation CheckLocationViewController
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [TalkingData trackPageBegin:selfControllerName];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [TalkingData trackPageEnd:selfControllerName];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initData];
    [self initUserInterface];
}

- (void)initData {
    
    MAPointAnnotation *pointAnnotation = [[MAPointAnnotation alloc] init];
    
    pointAnnotation.coordinate = _locationMessageContent.location;
    pointAnnotation.title = @"";
    
//    BMKPointAnnotation *pointAnnotation = [[BMKPointAnnotation alloc] init];
//    
//    pointAnnotation.coordinate = _locationMessageContent.location;
//    pointAnnotation.title = @"";
    
    _annotatians = [[NSArray alloc] initWithObjects:pointAnnotation, nil];
}

- (void)initUserInterface {
    [self.view addSubview:self.rootView];
    _rootView.mapView.centerCoordinate = _locationMessageContent.location;
    _rootView.mapView.zoomLevel = 14.85;
    _rootView.mapView.showsUserLocation = YES;
    
    MAPointAnnotation *pointAnnotation = [[MAPointAnnotation alloc] init];
    pointAnnotation.coordinate = _locationMessageContent.location;
    pointAnnotation.title = @"在这里";
    
//    BMKPointAnnotation *pointAnnotation = [[BMKPointAnnotation alloc] init];
//    pointAnnotation.coordinate = _locationMessageContent.location;
//    pointAnnotation.title = @"在这里";
    
    [_rootView.mapView addAnnotation:pointAnnotation];
//    [_rootView.mapView showAnnotations:_annotatians animated:YES];
}

- (CheckLocationRootView *)rootView {
    if (!_rootView) {
        _rootView = [[CheckLocationRootView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
        [_rootView.navigationBarView.leftButton addTarget:self action:@selector(returnButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    }
    return _rootView;
}

- (void)returnButtonClicked {
    _rootView.mapView.showsUserLocation = NO;
    [_rootView.mapView removeAnnotations:_rootView.mapView.annotations];

    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

@end
