//
//  CheckLocationRootView.h
//  AfterSchool
//
//  Created by Teson Draw on 11/4/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavigationBarView.h"
//#import <BaiduMapAPI_Map/BMKMapComponent.h>
#import <MAMapKit/MAMapKit.h>

@interface CheckLocationRootView : UIView

@property (nonatomic, strong) NavigationBarView *navigationBarView;
@property (nonatomic, strong) MAMapView *mapView;//地图视图
//@property (nonatomic, strong) BMKMapView *mapView;//地图视图

@end
