//
//  CheckLocationRootView.m
//  AfterSchool
//
//  Created by Teson Draw on 11/4/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "CheckLocationRootView.h"


@implementation CheckLocationRootView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithRed:240/255.f green:239/255.f blue:244/255.f alpha:1];
        [self addSubview:self.navigationBarView];
        [self addSubview:self.mapView];
    }
    
    return self;
}

- (NavigationBarView *)navigationBarView {
    if (!_navigationBarView) {
        _navigationBarView = [[NavigationBarView alloc] initWithTitle:@"位置信息" titleColor:[UIColor whiteColor] backgroundColor:nil leftButtonImageName:@"navigation_bar_return_button" rightButtonImageName:nil];
    }
    return _navigationBarView;
}

- (MAMapView *)mapView {
    if (_mapView == nil) {
        _mapView = [[MAMapView alloc] initWithFrame:CGRectMake(0, 64, self.bounds.size.width, self.bounds.size.height - 64)];
        
        //        [_mapView setTrafficEnabled:NO];//路况
        //        [_mapView setBaiduHeatMapEnabled:NO];//热力图
        //        [_mapView setZoomEnabled:YES];//多点缩放
        //        [_mapView setZoomEnabledWithTap:NO];//点击缩放
        //        [_mapView setScrollEnabled:YES];//移动地图
        //        [_mapView setOverlookEnabled:NO];//仰俯视
        //        [_mapView setRotateEnabled:NO];//旋转
        //        [_mapView setShowMapScaleBar:NO];//比例尺
        //        [_mapView setChangeWithTouchPointCenterEnabled:NO];//以手势中心点为轴进行旋转和缩放
        //
        //        [_mapView setShowsUserLocation:YES];//显示定位图层
        //        [_mapView setIsSelectedAnnotationViewFront:YES];//总是让选中的标注显示在最前面
        //
        //        [_mapView setUserTrackingMode:BMKUserTrackingModeNone];
        //        [_mapView setZoomLevel:16];//默认视图级别
        //
        //        [_mapView setMapCenterToScreenPt:CGPointMake(self.bounds.size.width / 2, (self.bounds.size.height - 64) / 2)];
    }
    
    return _mapView;
}

//- (BMKMapView *)mapView {
//    if (_mapView == nil) {
//        _mapView = [[BMKMapView alloc] initWithFrame:CGRectMake(0, 64, self.bounds.size.width, self.bounds.size.height - 64)];
//        
////        [_mapView setTrafficEnabled:NO];//路况
////        [_mapView setBaiduHeatMapEnabled:NO];//热力图
////        [_mapView setZoomEnabled:YES];//多点缩放
////        [_mapView setZoomEnabledWithTap:NO];//点击缩放
////        [_mapView setScrollEnabled:YES];//移动地图
////        [_mapView setOverlookEnabled:NO];//仰俯视
////        [_mapView setRotateEnabled:NO];//旋转
////        [_mapView setShowMapScaleBar:NO];//比例尺
////        [_mapView setChangeWithTouchPointCenterEnabled:NO];//以手势中心点为轴进行旋转和缩放
////        
////        [_mapView setShowsUserLocation:YES];//显示定位图层
////        [_mapView setIsSelectedAnnotationViewFront:YES];//总是让选中的标注显示在最前面
////        
////        [_mapView setUserTrackingMode:BMKUserTrackingModeNone];
////        [_mapView setZoomLevel:16];//默认视图级别
////        
////        [_mapView setMapCenterToScreenPt:CGPointMake(self.bounds.size.width / 2, (self.bounds.size.height - 64) / 2)];
//    }
//    
//    return _mapView;
//}

@end
