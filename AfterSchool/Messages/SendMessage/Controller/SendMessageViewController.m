//
//  SendMessageViewController.m
//  AfterSchool
//
//  Created by Teson Draw on 10/20/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "SendMessageViewController.h"
#import "MacrosDefinition.h"
#import "UserDetailViewController.h"
#import "TDLocationPickerViewController.h"
#import "CheckLocationViewController.h"
#import "TDNetworking.h"
#import "TDSingleton.h"

#import "LGDefineNetServer.h"
#import "PeopleGameDetailViewController.h"

#import "NSString+ConsecutiveNumbers.h"

#import "NotStartedOrderDetailViewController.h"

#import "TalkingData.h"
#import "Common.h"
#import "SVProgressHUD.h"

#import "NSString+JSON.h"

@interface SendMessageViewController () <RCLocationPickerViewControllerDelegate>

@end

@implementation SendMessageViewController

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [TalkingData trackPageBegin:selfControllerName];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [TalkingData trackPageEnd:selfControllerName];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initData];
    [self initUserInterface];
}

- (void)initData {
    [self getUserInfo];
}

- (void)initUserInterface {
    
    [self.view addSubview:self.navigationBarView];
    
    self.displayUserNameInCell = NO;
    self.navigationBarView.titleLabel.text = self.userName;
    self.conversationMessageCollectionView.frame = CGRectMake(0, 64, SCREEN_WIDTH, SCREEN_HEIGHT - 64 - 49);
    
    [self scrollToBottomAnimated:NO];
}

- (NavigationBarView *)navigationBarView {
    if (!_navigationBarView) {
        
        NSString *rightButtonImageName;
        if (_entryMode == 2) {
            rightButtonImageName = nil;
        } else {
            rightButtonImageName = @"message_user_detail";
        }
//        _navigationBarView = [[NavigationBarView alloc] initWithTitle:@"消息"
//                                                           titleColor:[UIColor whiteColor]
//                                                      backgroundColor:nil
//                                                       leftButtonName:nil
//                                                 leftButtonTitleColor:nil
//                                                  leftButtonImageName:@"navigation_bar_return_button"
//                                                      rightButtonName:nil
//                                                 rightButtonImageName:nil];
        
        _navigationBarView = [[NavigationBarView alloc] initWithTitle:@"消息"
                                                           titleColor:nil
                                                      backgroundColor:nil
                                              leftButtonBgViewOriginX:0
                                                 leftButtonImageWidth:22
                                                 leftButtonTitleColor:[UIColor whiteColor]
                                                       leftButtonName:@"返回"
                                                  leftButtonImageName:@"navigation_bar_return_button_left_arrow"
                                                      rightButtonName:nil
                                                 rightButtonImageName:nil];
        
        [_navigationBarView.leftButton addTarget:self action:@selector(returnButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        [_navigationBarView.rightButton addTarget:self action:@selector(settingButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    }
    return _navigationBarView;
}

- (void)returnButtonClicked {
    self.tabBarController.tabBar.hidden = NO;
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)settingButtonClicked {
    UserDetailViewController *vc = [[UserDetailViewController alloc] init];
    vc.targetId = self.targetId;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.tabBarController.tabBar.hidden = YES;
    self.navigationController.navigationBarHidden = YES;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)presentImagePreviewController:(RCMessageModel *)model;
{
    RCImagePreviewController *_imagePreviewVC =
    [[RCImagePreviewController alloc] init];
    _imagePreviewVC.messageModel = model;
    _imagePreviewVC.title = @"图片预览";

    UINavigationController *nav = [[UINavigationController alloc]
                                   initWithRootViewController:_imagePreviewVC];
    nav.navigationBar.barTintColor = [UIColor blackColor];
    nav.navigationBar.barStyle = UIBarStyleBlack;
    
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],
                                NSForegroundColorAttributeName,
                                [UIFont systemFontOfSize:20],
                                NSFontAttributeName, nil];
    
    [nav.navigationBar setTitleTextAttributes:attributes];
    
    [self presentViewController:nav animated:YES completion:nil];
}


#pragma mark 头像点击事件
- (void)didTapCellPortrait:(NSString *)userId {
    NSLog(@"userID:%@", userId);
    if (![userId isEqualToString:@"system"] || ![userId isEqualToString:@"message_order"]) {
//        UserDetailViewController *vc = [[UserDetailViewController alloc] init];
//        vc.model = _model;
//        [self.navigationController pushViewController:vc animated:YES];
        
        [self viewDetailWithAccount:userId];
    }
}

#pragma mark 消息点击事件
- (void)didTapMessageCell:(RCMessageModel *)model {
    [super didTapMessageCell:model];
    NSLog(@"RCMessageModel extra:%@", model.extra);

    if ([model.content isMemberOfClass:[RCTextMessage class]]) {
        RCTextMessage *msg = (RCTextMessage *)model.content;
        NSLog(@"msg.content:%@", msg.content);
        NSLog(@"msg.extra:%@", msg.extra);
        
        if (msg.extra) {
            NSDictionary *extraDic = [NSString dictionaryFromJSONString:msg.extra];
            if (extraDic) {
                NSLog(@"extraDic: %@", extraDic);
                NSString *orderNumber = [NSString stringWithFormat:@"%@", extraDic[@"orderId"]];
                
                if (orderNumber.length >= 15) {
                    [self requestOrderStepInfoWithOrderNumber:orderNumber];
                }
            }
        }
    }
}

- (void)requestOrderStepInfoWithOrderNumber:(NSString *)orderNumber {
    [SVProgressHUD showWithStatus:@""];
    
    NSDictionary *parameters = @{@"action":@"getOrderById",
                                 @"sessionKey":[TDSingleton instance].sessionKey,
                                 @"orderId":orderNumber,
                                 @"needProcess":@"1"};
    
    [TDNetworking requestWithURL:[TDSingleton instance].gatewayServerBusinessURL
                      parameters:parameters
                         success:^(id responseObject)
     {
         NSLog(@"getOrderById:%@", responseObject);
         if ([responseObject[@"result"] integerValue] == 1) {
             
             [SVProgressHUD dismiss];
             [self viewOrderDetailWithOrderInfo:responseObject[@"order"]];
             
         } else {
             [SVProgressHUD dismissWithError:[NSString stringWithFormat:@"%@", responseObject[@"msg"]]];
         }
     } failure:^(NSError *error) {
         NSLog(@"error:%@", error);
     }];
}

- (void)viewOrderDetailWithOrderInfo:(NSDictionary *)orderInfo {
    
    NotStartedOrderDetailViewController *vc = [[NotStartedOrderDetailViewController alloc] init];
    vc.orderInfo = orderInfo;
    [self.navigationController pushViewController:vc animated:YES];
}


- (void)viewDetailWithAccount:(NSString *)account {
    
//    [SVProgressHUD showWithStatus:@"搜索中..." maskType:SVProgressHUDMaskTypeGradient];
    
    [LGDefineNetServer getUsersInfoWithAccounts:@[account] success:^(id result) {
//        [SVProgressHUD dismiss];
        
        if ([result isKindOfClass:[NSArray class]]) {
            NSArray *array = result;
            
            if (array.count > 0) {
                LGDaKaInfo *bigShotInfo = [[LGDaKaInfo alloc] initWithDakaInfoHelp:result[0]];
                
                PeopleGameDetailViewController * vc = [[PeopleGameDetailViewController alloc] initWithDakaInfo:bigShotInfo];
                [self.navigationController pushViewController:vc animated:YES];
            }
        }
    } failure:^(NSString *msg) {
//        [SVProgressHUD dismiss];
    }];
}

-(void)pluginBoardView:(RCPluginBoardView*)pluginBoardView clickedItemWithTag:(NSInteger)tag {
    if (tag == 1003) {
        TDLocationPickerViewController *vc = [[TDLocationPickerViewController alloc] init];
        vc.delegate = self;
        [self.navigationController pushViewController:vc animated:YES];
    } else {
        [super pluginBoardView:pluginBoardView clickedItemWithTag:tag];
    }
}

- (void)locationPicker:(RCLocationPickerViewController *)locationPicker
     didSelectLocation:(CLLocationCoordinate2D)location
          locationName:(NSString *)locationName
         mapScreenShot:(UIImage *)mapScreenShot {
    RCLocationMessage *locationMessage =
    [RCLocationMessage messageWithLocationImage:mapScreenShot
                                       location:location
                                   locationName:locationName];
    [self sendMessage:locationMessage pushContent:@"sss"];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)presentLocationViewController:(RCLocationMessage *)locationMessageContent {

    CheckLocationViewController *vc = [[CheckLocationViewController alloc] init];
    vc.locationMessageContent = locationMessageContent;
    [self.navigationController presentViewController:vc animated:YES completion:^{
        
    }];
}

- (void)getUserInfo {
    
    if (![self.targetId isEqualToString:@"system"] || ![self.targetId isEqualToString:@"message_order"]) {
        
        NSDictionary *parameters = @{@"action":@"getUserSimple",
                                     @"user":self.targetId,
                                     @"sessionKey":[TDSingleton instance].sessionKey};
        
        [TDNetworking requestWithURL:[TDSingleton instance].gatewayServerBusinessURL
                          parameters:parameters
                             success:^(id responseObject)
         {
             if ([responseObject[@"result"] integerValue] == 1) {
                 _model = [[UserInfoModel alloc] initWithData:responseObject[@"userInfo"]];
             } else {
                 NSLog(@"SendMessageViewController getUserInfo msg:%@", [NSString stringWithFormat:@"%@", responseObject[@"msg"]]);
             }
         } failure:^(NSError *error) {
             NSLog(@"didReceiveMessageNotification error:%@", error);
         }];
    }
}

- (void)willDisplayMessageCell:(RCMessageBaseCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"RCMessageBaseCell: %@", cell.model.extra);
    
    if ([cell.model.content isMemberOfClass:[RCTextMessage class]]) {
        RCTextMessage *msg = (RCTextMessage *)cell.model.content;
        NSLog(@"msg.content:%@", msg.content);
        NSLog(@"msg.extra:%@", msg.extra);
        
        if (msg.extra) {
            NSDictionary *extraDic = [NSString dictionaryFromJSONString:msg.extra];//[msg.extra dictionaryWithValuesForKeys];
            if (extraDic) {
                NSLog(@"extraDic: %@", extraDic);
                NSString *orderNumber = [NSString stringWithFormat:@"%@", extraDic[@"orderId"]];
                
                if (orderNumber.length >= 15) {
                    if ([cell isMemberOfClass:RCTextMessageCell.class]) {
                        RCTextMessageCell *textCell = (RCTextMessageCell *)cell;
                        textCell.textLabel.textColor = [UIColor colorWithRed:30/255.f green:176/255.f blue:1.f alpha:1];
                    }
                }
            }
        }
    }
    
    
//    if ([cell isMemberOfClass:RCTextMessageCell.class]) {
    
        
//        RCTextMessageCell *textCell = (RCTextMessageCell *)cell;
//        textCell.textLabel.textColor = [UIColor colorWithRed:30/255.f green:176/255.f blue:1.f alpha:1];
//    }
    
}

@end
