//
//  SendMessageViewController.h
//  AfterSchool
//
//  Created by Teson Draw on 10/20/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <RongIMKit/RongIMKit.h>
#import "NavigationBarView.h"
#import "UserInfoModel.h"

@interface SendMessageViewController : RCConversationViewController

@property (nonatomic, strong) NavigationBarView *navigationBarView;
@property (nonatomic, strong) UserInfoModel *model;

/**
 *  进入聊天界面方式
 *
 *  1、消息界面进入
 *  2、订单界面进入 (导航栏无右上角的消息设置按钮)
 *
 */
@property (nonatomic, assign) NSInteger entryMode;

@end
