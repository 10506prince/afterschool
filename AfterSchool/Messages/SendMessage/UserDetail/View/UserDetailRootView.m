//
//  UserDetailRootView.m
//  AfterSchool
//
//  Created by Teson Draw on 10/29/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "UserDetailRootView.h"
#import "MacrosDefinition.h"
#import "UIButton+SetBackgroundColor.h"
#import "TDShapeLayer.h"

@implementation UserDetailRootView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithRed:240/255.f green:239/255.f blue:244/255.f alpha:1];
        
        [self addSubview:self.scrollView];
        [self addSubview:self.navigationBarView];
        
//        [_scrollView addSubview:self.portraitBackgroundView];
//        [_portraitBackgroundView addSubview:self.portraitImageView];
//        [_portraitBackgroundView addSubview:self.nameLabel];
//        [_portraitBackgroundView addSubview:self.signatureLabel];
//        [_portraitBackgroundView.layer addSublayer:[TDShapeLayer setArrowLayerWithWidth:_portraitBackgroundView.bounds.size.width height:_portraitBackgroundView.bounds.size.height]];
//        [_portraitBackgroundView addSubview:self.userDetailButton];
////        [_portraitBackgroundView.layer addSublayer:self.rightArrowLayer];
////        [_portraitBackgroundView addSubview:self.nextOperationImageView];
//        
//        [_scrollView addSubview:self.setTopChatOptionView];
//        [_scrollView addSubview:self.noDisturbOptionView];
//        [_scrollView addSubview:self.setChatBackgroundOptionView];
        
        
        [_scrollView addSubview:self.clearChatRecordOptionView];
        [_scrollView addSubview:self.deleteContactButton];
    }
    
    return self;
}

- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
        _scrollView.contentSize = CGSizeMake(SCREEN_WIDTH, SCREEN_HEIGHT + 40);
        _scrollView.showsVerticalScrollIndicator = NO;
//        if (iPhone4s) {
//            _scrollView.contentSize = CGSizeMake(SCREEN_WIDTH, SCREEN_HEIGHT + 40);
//        } else {
//            _scrollView.contentSize = CGSizeMake(SCREEN_WIDTH, SCREEN_HEIGHT + 20);
//        }
    }
    return _scrollView;
}

- (NavigationBarView *)navigationBarView {
    if (!_navigationBarView) {
        _navigationBarView = [[NavigationBarView alloc] initWithTitle:@"聊天详情"
                                                           titleColor:[UIColor whiteColor]
                                                      backgroundColor:nil
                                                  leftButtonImageName:@"navigation_bar_return_button" rightButtonImageName:nil];
    }
    return _navigationBarView;
}

- (UIView *)portraitBackgroundView {
    if (!_portraitBackgroundView) {
        _portraitBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 64 + 20, SCREEN_WIDTH, 76)];
        _portraitBackgroundView.backgroundColor = [UIColor whiteColor];
    }
    return _portraitBackgroundView;
}

- (UIImageView *)portraitImageView {
    if (!_portraitImageView) {
        _portraitImageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 44, 44)];
        _portraitImageView.layer.cornerRadius = 22;
        _portraitImageView.layer.masksToBounds = YES;
    }
    return _portraitImageView;
}

- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_portraitImageView.frame) + 8, 14, SCREEN_WIDTH - CGRectGetMaxX(_portraitImageView.frame) - 8 - 24, 16)];
        _nameLabel.textColor = [UIColor blackColor];
        _nameLabel.font = [UIFont systemFontOfSize:16];
        _nameLabel.text = @"你的王祖小蓝蓝";
    }
    return _nameLabel;
}

- (UILabel *)signatureLabel {
    if (!_signatureLabel) {
        _signatureLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_portraitImageView.frame) + 8, 76 - 16 - 14, SCREEN_WIDTH - CGRectGetMaxX(_portraitImageView.frame) - 8 - 24, 14)];
        _signatureLabel.textColor = [UIColor colorWithRed:153/255.f green:153/255.f blue:153/255.f alpha:1];
        _signatureLabel.font = [UIFont systemFontOfSize:14];
        _signatureLabel.text = @"自信打不死的心态活到老，哦哦哦，...";
    }
    return _signatureLabel;
}

//- (TDShapeLayer *)rightArrowLayer {
//    if (!_rightArrowLayer) {
//        _rightArrowLayer = [[TDShapeLayer alloc] initWithWidth:_portraitBackgroundView.bounds.size.width height:_portraitBackgroundView.bounds.size.height];
//    }
//    return _rightArrowLayer;
//}

- (UIImageView *)nextOperationImageView {
    if (!_nextOperationImageView) {
        _nextOperationImageView = [[UIImageView alloc] initWithFrame:CGRectMake(_portraitBackgroundView.bounds.size.width - 16 - 12, (_portraitBackgroundView.bounds.size.height - 16) / 2, 12, 16)];
        _nextOperationImageView.image = [UIImage imageNamed:@"gray_color_right_arrow"];
    }
    return _nextOperationImageView;
}

- (UIButton *)userDetailButton {
    if (!_userDetailButton) {
        _userDetailButton = [[UIButton alloc] initWithFrame:_portraitBackgroundView.bounds];
    }
    
    return _userDetailButton;
}

- (OptionView *)setTopChatOptionView {
    if (!_setTopChatOptionView) {
        _setTopChatOptionView = [[OptionView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_portraitBackgroundView.frame) + 20, SCREEN_WIDTH, 50) title:@"置顶聊天" titleTextColor:nil titleFontSize:18 backgroundColor:nil showArrow:NO showSwitch:YES];

        UIView *separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(12, 49, _setTopChatOptionView.bounds.size.width - 12, 1)];
        separatorLineView.backgroundColor = [UIColor colorWithRed:237/255.f green:239/255.f blue:239/255.f alpha:1];
        [_setTopChatOptionView addSubview:separatorLineView];
    }
    return _setTopChatOptionView;
}

- (OptionView *)noDisturbOptionView {
    if (!_noDisturbOptionView) {
        _noDisturbOptionView = [[OptionView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_setTopChatOptionView.frame), SCREEN_WIDTH, 50) title:@"消息免打扰" titleTextColor:nil titleFontSize:18 backgroundColor:nil showArrow:NO showSwitch:YES];
    }
    return _noDisturbOptionView;
}

- (OptionView *)setChatBackgroundOptionView {
    if (!_setChatBackgroundOptionView) {
        _setChatBackgroundOptionView = [[OptionView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_noDisturbOptionView.frame) + 20, SCREEN_WIDTH, 50) title:@"设置当前聊天背景" titleTextColor:nil titleFontSize:18 backgroundColor:nil showArrow:YES showSwitch:NO];
    }
    return _setChatBackgroundOptionView;
}

- (OptionView *)clearChatRecordOptionView {
    if (!_clearChatRecordOptionView) {
        _clearChatRecordOptionView = [[OptionView alloc] initWithFrame:CGRectMake(0, 64 + 20, SCREEN_WIDTH, 50)
                                                                 title:@"清空聊天记录"
                                                        titleTextColor:[UIColor colorWithRed:0/255.f green:171/255.f blue:239/255.f alpha:1]
                                                         titleFontSize:18
                                                       backgroundColor:nil
                                                             showArrow:NO
                                                            showSwitch:NO];
    }
    return _clearChatRecordOptionView;
}

- (UIButton *)deleteContactButton {
    if (!_deleteContactButton) {
        _deleteContactButton = [[UIButton alloc] initWithFrame:CGRectMake(12, CGRectGetMaxY(_clearChatRecordOptionView.frame) + 46, SCREEN_WIDTH - 24, 46)];
        [_deleteContactButton setTitle:@"删除联系人" forState:UIControlStateNormal];
        [_deleteContactButton setBackgroundColor:[UIColor colorWithRed:252/255.f green:97/255.f blue:86/255.0 alpha:1]];
        [_deleteContactButton setBackgroundColor:[UIColor colorWithRed:153/255.f green:46/255.f blue:34/255.f alpha:1] forState:UIControlStateHighlighted];
        _deleteContactButton.layer.cornerRadius = 5;
        _deleteContactButton.layer.masksToBounds = YES;
        _deleteContactButton.titleLabel.font = [UIFont systemFontOfSize:20];
    }
    
    return _deleteContactButton;
}

@end
