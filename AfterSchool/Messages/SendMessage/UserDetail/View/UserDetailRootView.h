//
//  UserDetailRootView.h
//  AfterSchool
//
//  Created by Teson Draw on 10/29/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavigationBarView.h"
#import "OptionView.h"


@interface UserDetailRootView : UIView

@property (nonatomic, strong) NavigationBarView *navigationBarView;

@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UIImageView *portraitImageView;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *signatureLabel;
@property (nonatomic, strong) UIView *portraitBackgroundView;

@property (nonatomic, strong) OptionView *setTopChatOptionView;
@property (nonatomic, strong) OptionView *noDisturbOptionView;
@property (nonatomic, strong) OptionView *setChatBackgroundOptionView;
@property (nonatomic, strong) OptionView *clearChatRecordOptionView;

@property (nonatomic, strong) UIButton *deleteContactButton;

//@property (nonatomic, strong) TDShapeLayer *rightArrowLayer;
@property (nonatomic, strong) UIImageView *nextOperationImageView;

@property (nonatomic, strong) UIButton *userDetailButton;

@end
