//
//  UserDetailViewController.h
//  AfterSchool
//
//  Created by Teson Draw on 10/29/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserDetailRootView.h"
#import "UserInfoModel.h"

@interface UserDetailViewController : UIViewController

@property (nonatomic, strong) UserDetailRootView *rootView;

@property (nonatomic, strong) NSString *targetId;
@property (nonatomic, strong) UserInfoModel *model;

@end
