//
//  UserDetailViewController.m
//  AfterSchool
//
//  Created by Teson Draw on 10/29/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "UserDetailViewController.h"
#import "MacrosDefinition.h"
#import "UIImageView+WebCache.h"
#import "SVProgressHUD.h"
#import "LGDefineNetServer.h"
#import "PeopleGameDetailViewController.h"
#import "NotificationName.h"

#import "Common.h"
#import "TalkingData.h"

@interface UserDetailViewController ()

@end

@implementation UserDetailViewController

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [TalkingData trackPageBegin:selfControllerName];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [TalkingData trackPageEnd:selfControllerName];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initUserInterface];
}

- (void)initUserInterface {
    self.tabBarController.tabBar.hidden = YES;
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    [self.view addSubview:self.rootView];
    
    if (_model) {
        _rootView.nameLabel.text = _model.nickName;
        [_rootView.portraitImageView sd_setImageWithURL:[NSURL URLWithString:_model.portraitURL] placeholderImage:[UIImage imageNamed:@"user_default_head"]];
        
        if (_model.info.length == 0) {
            _rootView.signatureLabel.text = @"这家伙很懒,什么都没有留下。";
        } else {
            _rootView.signatureLabel.text = _model.info;
        }
    }
}

- (UserDetailRootView *)rootView {
    if (!_rootView) {
        _rootView = [[UserDetailRootView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];

        [_rootView.navigationBarView.leftButton addTarget:self action:@selector(returnButtonClicked) forControlEvents:UIControlEventTouchUpInside];
//        _rootView.noDisturbOptionView.switchButton.on = YES;
//        [_rootView.userDetailButton addTarget:self action:@selector(userDetailButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        
        [_rootView.deleteContactButton addTarget:self action:@selector(deleteContactsButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        
        [_rootView.clearChatRecordOptionView.optionButton addTarget:self action:@selector(deleteChatRecordsButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    }
    return _rootView;
}

- (void)returnButtonClicked {
    [self.navigationController popViewControllerAnimated:YES];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)deleteChatRecordsButtonClicked {
//    NSLog(@"%@", self.navigationController.viewControllers);
    [[NSNotificationCenter defaultCenter] postNotificationName:TDDeleteChatRecords object:_targetId];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)deleteContactsButtonClicked {
//    NSLog(@"%@", self.navigationController.viewControllers);
    [[NSNotificationCenter defaultCenter] postNotificationName:TDDeleteChatContacts object:_targetId];
    [self.navigationController popToRootViewControllerAnimated:YES];
}



//- (void)userDetailButtonClicked {
//    [self viewDetailWithAccount:_model.account];
//}

//- (void)viewDetailWithAccount:(NSString *)account {
//    
//    [SVProgressHUD showWithStatus:@"搜索中..." maskType:SVProgressHUDMaskTypeGradient];
//    
//    [LGDefineNetServer getUsersInfoWithAccounts:@[account] success:^(id result) {
//        [SVProgressHUD dismiss];
//        
//        if ([result isKindOfClass:[NSArray class]]) {
//            NSArray *array = result;
//            
//            if (array.count > 0) {
//                LGDaKaInfo *bigShotInfo = [[LGDaKaInfo alloc] initWithDakaInfoHelp:result[0]];
//                
//                PeopleGameDetailViewController * vc = [[PeopleGameDetailViewController alloc] initWithDakaInfo:bigShotInfo menuItemType:MenuItemNoChat];
//                [self.navigationController pushViewController:vc animated:YES];
//            }
//        }
//    } failure:^{
//        [SVProgressHUD dismiss];
//    }];
//}

@end
