//
//  TDLocationPickerViewController.m
//  AfterSchool
//
//  Created by Teson Draw on 10/29/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "TDLocationPickerViewController.h"
#import "Common.h"
#import "TalkingData.h"

@interface TDLocationPickerViewController ()

@end

@implementation TDLocationPickerViewController
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [TalkingData trackPageBegin:selfControllerName];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [TalkingData trackPageEnd:selfControllerName];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initUserInterface];
}

- (void)initUserInterface {
//    self.navigationController.navigationBarHidden = NO;
    [self.view addSubview:self.navigationBarView];
}

- (NavigationBarView *)navigationBarView {
    if (!_navigationBarView) {
        _navigationBarView = [[NavigationBarView alloc] initWithTitle:@"选取位置"
                                                           titleColor:[UIColor whiteColor]
                                                      backgroundColor:nil
                                                       leftButtonName:@"取消"
                                                 leftButtonTitleColor:nil
                                                  leftButtonImageName:nil
                                                      rightButtonName:@"完成"
                                                 rightButtonImageName:nil];
        [_navigationBarView.leftButton addTarget:self action:@selector(leftBarButtonItemPressed:) forControlEvents:UIControlEventTouchUpInside];
        [_navigationBarView.rightButton addTarget:self action:@selector(rightBarButtonItemPressed:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _navigationBarView;
}

- (void)cancelButtonClicked {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)okButtonClicked {
    
}

- (void)leftBarButtonItemPressed:(id)sender {
    [super leftBarButtonItemPressed:sender];
}

- (void)rightBarButtonItemPressed:(id)sender {
    [super rightBarButtonItemPressed:sender];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
