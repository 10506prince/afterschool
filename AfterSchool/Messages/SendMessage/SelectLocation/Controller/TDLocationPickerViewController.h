//
//  TDLocationPickerViewController.h
//  AfterSchool
//
//  Created by Teson Draw on 10/29/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <RongIMKit/RongIMKit.h>
#import "NavigationBarView.h"

@interface TDLocationPickerViewController : RCLocationPickerViewController

@property (nonatomic, strong) NavigationBarView *navigationBarView;

@end
