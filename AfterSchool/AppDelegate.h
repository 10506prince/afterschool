//
//  AppDelegate.h
//  AfterSchool
//
//  Created by Teson Draw on 9/30/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <BaiduMapAPI_Base/BMKBaseComponent.h>
#import "Common.h"
#import "BigShotOrderModel.h"
#import "TodayOrderModel.h"

#import <RongIMKit/RongIMKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate,BMKGeneralDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (nonatomic, strong) UINavigationController *rootNC;

@property (nonatomic, strong) BigShotOrderModel *orderModel;

@property (nonatomic, strong) TodayOrderModel *todayOrderModel;

@property (nonatomic, strong) NSDictionary *chatObjectDic;

- (void)logout;


- (void)getUserInfoWithUserId:(NSString *)userId
                   completion:(void (^)(RCUserInfo *))completion;

@end

