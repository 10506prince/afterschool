//
//  RelationshipsViewController.m
//  AfterSchool
//
//  Created by lg on 15/10/22.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "RelationshipsViewController.h"
#import "Common.h"
#import "TDSwitchButtonView.h"
#import "MacrosDefinition.h"
#import "TDSingleton.h"
#import "UIImageView+WebCache.h"
#import "SettingWindowAnimation.h"
#import "NotificationName.h"

#import "Common.h"
#import "TalkingData.h"

@interface RelationshipsViewController () <TDSwitchButtonViewDelegate> {
    NSArray * _dataSource;
}

@end

@implementation RelationshipsViewController

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [TalkingData trackPageBegin:selfControllerName];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [TalkingData trackPageEnd:selfControllerName];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initData];
    [self initUserInterface];
    [self addUserIconChangeNotification];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.tabBarController.tabBar setHidden:NO];
}

- (void)initData {
    
}

- (void)initUserInterface {
    [self.view addSubview:self.rootView];
    
    _blackListVC = [[BlacklistViewController alloc] init];
    _contactsListVC = [[ContactsListViewController alloc] init];

    [self addChildViewController:_blackListVC];
    [self addChildViewController:_contactsListVC];
    
    [self.view addSubview:_blackListVC.view];
    [self.view addSubview:_contactsListVC.view];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.navigationController.navigationBarHidden = YES;
}

- (RelationshipsRootView *)rootView {
    if (!_rootView) {
        _rootView = [[RelationshipsRootView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
        [_rootView.navigationBarView.leftButton addTarget:self action:@selector(portraitClicked) forControlEvents:UIControlEventTouchUpInside];
        
        [_rootView.navigationBarView.leftButtonImageView sd_setImageWithURL:[NSURL URLWithString:[TDSingleton instance].userInfoModel.portraitURL] placeholderImage:[UIImage imageNamed:@"user_default_head"]];
        
        _rootView.switchButtonView.delegate = self;
    }
    
    return _rootView;
}

- (void)backAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)switchButtonViewButtonClicked:(UIButton *)sender {
    switch (sender.tag) {
        case 100:
            [self.view insertSubview:_contactsListVC.view aboveSubview:_blackListVC.view];
            break;

        case 101:
            [self.view insertSubview:_blackListVC.view aboveSubview:_contactsListVC.view];
            break;
    }
}

#pragma mark - 左滑
- (void)portraitClicked {
    
    /**
     *  swipe gesture
     */
    UISwipeGestureRecognizer *swipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(gestureAction:)];
    [swipeGesture setDirection:UISwipeGestureRecognizerDirectionLeft|UISwipeGestureRecognizerDirectionRight];
    
    /**
     *  tap gesture
     */
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gestureAction:)];
    tapGesture.numberOfTapsRequired = 1;
    
    /**
     *  屏蔽视图
     */
    UIView * maskView = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    [maskView setBackgroundColor:[UIColor clearColor]];
    maskView.tag = 1000;
    [maskView addGestureRecognizer:swipeGesture];
    [maskView addGestureRecognizer:tapGesture];
    
    [self.tabBarController.view addSubview:maskView];
    
    [SettingWindowAnimation showWithView:self.tabBarController.view];
}

- (void)gestureAction:(UIGestureRecognizer *)sender {
    
    [SettingWindowAnimation hideWithView:self.tabBarController.view];
    
    UIView * maskView = sender.view;
    
    [maskView removeGestureRecognizer:sender];
    [maskView removeFromSuperview];
}

///添加定位服务通知
- (void)addUserIconChangeNotification {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateUserIcon:) name:LGSetUserIcon object:nil];
}

#pragma mark - 头像改变通知
- (void)didUpdateUserIcon:(NSNotification *)notifi {
    NSURL * url = [NSURL URLWithString:notifi.object];
    [self.rootView.navigationBarView.leftButtonImageView sd_setImageWithURL:url];
}
@end
