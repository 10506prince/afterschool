//
//  BlacklistViewController.m
//  AfterSchool
//
//  Created by Teson Draw on 11/24/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "BlacklistViewController.h"
#import "MacrosDefinition.h"

#import "UIImageView+WebCache.h"
#import "TDNetworkRequest.h"
#import "ContactsTableViewCell.h"

#import "NotificationName.h"
#import "SVProgressHUD.h"

#import "NSString+PhoneNumber.h"
#import "LGDefineNetServer.h"

#import "PeopleGameDetailViewController.h"

#import "Common.h"
#import "TalkingData.h"

#import "MJRefresh.h"
#import "TDSingleton.h"

@interface BlacklistViewController () <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>

@end

@implementation BlacklistViewController

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [TalkingData trackPageBegin:selfControllerName];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [TalkingData trackPageEnd:selfControllerName];
}
- (void)viewDidLoad {
    [super viewDidLoad];

    [self initData];
    [self initUserInterface];
}

- (void)initData {
    _dataSource = [[NSMutableDictionary alloc] init];
    _backupDataSource = [[NSMutableDictionary alloc] init];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateBlacklist) name:TDUpdateBlacklist object:nil];
    
    [self updateBlacklist];
}

- (void)initUserInterface {
    self.view.frame = CGRectMake(0, 64, SCREEN_WIDTH, SCREEN_HEIGHT - 64 - 49);
    [self.view addSubview:self.rootView];
    self.automaticallyAdjustsScrollViewInsets = NO;
}

- (BlacklistRootView *)rootView {
    if (!_rootView) {
        _rootView = [[BlacklistRootView alloc] initWithFrame:self.view.bounds];
        _rootView.tableView.dataSource = self;
        _rootView.tableView.delegate = self;
        
        /**
         *  下拉刷新
         */
        __weak BlacklistViewController * weakSelf = self;
        
        MJRefreshNormalHeader * header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            [weakSelf updateBlacklist];
        }];
        
        [header setTintColor:[UIColor colorWithRed:120/255.f green:120/255.f blue:120/255.f alpha:1]];
        _rootView.tableView.mj_header = header;
        
        [_rootView.searchBar setDelegate:self];
    }
    return _rootView;
}

//- (void)requestContacts {
////    [TDNetworkRequest requestWithContactsAction:GetBlacklist
////                                        success:^(id responseObject)
////     {
////         NSLog(@"%@", responseObject);
////         
////         NSArray *result = responseObject[@"blackList"];
////         if (result.count > 0) {
////             [self processData:result];
////         }
////     }];
////    [self processData:[NSDictionary friendOrBlackListFromUserList:[TDSingleton instance].userInfoModel.blackListCache]];
//}

- (void)updateBlacklist {
    [self processData:[NSDictionary friendOrBlackListFromUserList:[TDSingleton instance].userInfoModel.blackListCache]];
    
    [self performSelector:@selector(endRefresh) withObject:nil afterDelay:2];
}

- (void)endRefresh {
    [_rootView.tableView.mj_header endRefreshing];
}

- (void)processData:(NSArray *)data {
    if (data.count > 0) {

        [_dataSource removeAllObjects];
        
        NSMutableArray *processedData = [[NSMutableArray alloc] initWithArray:data];
        
        NSString *intialFirstLetter = [[[NSString stringWithFormat:@"%@", processedData[0][@"nickNameAbb"]] substringToIndex:1] uppercaseString];
        
        NSMutableArray *initialData = [[NSMutableArray alloc] init];
        [initialData addObject:processedData[0]];
        
        if ([intialFirstLetter isNumber]) {
            [_dataSource setObject:initialData forKey:@"#"];
        } else {
            [_dataSource setObject:initialData forKey:intialFirstLetter];
        }
        
        
        for (int i = 1; i < processedData.count; i++) {
            
            NSString *firstLetter = [[[NSString stringWithFormat:@"%@", processedData[i][@"nickNameAbb"]] substringToIndex:1] uppercaseString];
            
            if ([[_dataSource allKeys] containsObject:firstLetter]) {
                NSMutableArray *data = [_dataSource objectForKey:firstLetter];
                [data addObject:processedData[i]];
                [_dataSource setObject:data forKey:firstLetter];
            } else {
                if ([firstLetter isNumber]) {
                    if ([[_dataSource allKeys] containsObject:@"#"]) {
                        NSMutableArray *data = [_dataSource objectForKey:@"#"];
                        [data addObject:processedData[i]];
                        [_dataSource setObject:data forKey:@"#"];
                    } else {
                        NSMutableArray *data = [[NSMutableArray alloc] init];
                        [data addObject:processedData[i]];
                        [_dataSource setObject:data forKey:@"#"];
                    }
                } else {
                    NSMutableArray *data = [[NSMutableArray alloc] init];
                    [data addObject:processedData[i]];
                    [_dataSource setObject:data forKey:firstLetter];
                }
            }
        }
        
        NSLog(@"_dataSource:%@", _dataSource);
        _backupDataSource = [_dataSource mutableCopy];
        
        _sectionsArray = [_dataSource allKeys];
        
        _sectionsArray = [_sectionsArray sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            return [obj1 compare:obj2 options:NSAnchoredSearch];
        }];
        
        [_rootView.tableView reloadData];
    }
}

#pragma mark TableView Protocol Method
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _sectionsArray.count;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return _sectionsArray[section];
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    return _sectionsArray;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return ((NSArray *)_dataSource[_sectionsArray[section]]).count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 56;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString * reuseIdentifier = @"reuseIdentifier";
    ContactsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    
    if (!cell) {
        cell = [[ContactsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    }
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@", ((NSDictionary *)((NSArray *)_dataSource[_sectionsArray[indexPath.section]])[indexPath.row])[@"icon"]]];
    
    [cell.portraitImageView sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"user_default_head"]];
    
    cell.nameLabel.text = [NSString stringWithFormat:@"%@", ((NSDictionary *)((NSArray *)_dataSource[_sectionsArray[indexPath.section]])[indexPath.row])[@"nickName"]];
    
    return cell;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewCellEditingStyleDelete;
}

- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath {
    return @"删除";
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        NSLog(@"点击了删除");

        NSString *account = [NSString stringWithFormat:@"%@", ((NSDictionary *)((NSArray *)_dataSource[_sectionsArray[indexPath.section]])[indexPath.row])[@"userName"]];
        
        [self deleteBlacklistWithAccount:account];
        
        NSMutableArray * dataSource = _dataSource[_sectionsArray[indexPath.section]];
        [dataSource removeObjectAtIndex:indexPath.row];
        
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *account = [NSString stringWithFormat:@"%@", ((NSDictionary *)((NSArray *)_dataSource[_sectionsArray[indexPath.section]])[indexPath.row])[@"userName"]];
    [self viewDetailWithAccount:account];
}


#pragma mark - UISearchBarDelegate
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    [searchBar setShowsCancelButton:YES animated:YES];
    return YES;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
    [searchBar setShowsCancelButton:NO animated:YES];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
    [searchBar setShowsCancelButton:NO animated:YES];
    searchBar.text = @"";
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    if (searchText.length > 0) {
        [_dataSource removeAllObjects];
        
        for (NSString *key in _backupDataSource) {
            NSArray * array = _backupDataSource[key];
            for (int i = 0; i < array.count; i++) {
                //name
                if ([[NSString stringWithFormat:@"%@", array[i][@"nickName"]] containsString:searchText]) {
                    
                    if ([[_dataSource allKeys] containsObject:key]) {
                        NSMutableArray *object = [_dataSource objectForKey:key];
                        [object addObject:array[i]];
                        [_dataSource setObject:object forKey:key];
                    } else {
                        NSMutableArray *object = [[NSMutableArray alloc] init];
                        [object addObject:array[i]];
                        [_dataSource setObject:object forKey:key];
                    }
                    continue;
                }
                
                //name abbreviation(卓天成 -> ztc)
                if ([[NSString stringWithFormat:@"%@", array[i][@"nickNameAbb"]] containsString:searchText]) {
                    
                    if ([[_dataSource allKeys] containsObject:key]) {
                        NSMutableArray *object = [_dataSource objectForKey:key];
                        [object addObject:array[i]];
                        [_dataSource setObject:object forKey:key];
                    } else {
                        NSMutableArray *object = [[NSMutableArray alloc] init];
                        [object addObject:array[i]];
                        [_dataSource setObject:object forKey:key];
                    }
//                    continue;
                }
                
//                //account
//                if ([[NSString stringWithFormat:@"%@", array[i][@"userName"]] containsString:searchText]) {
//                    
//                    if ([[_dataSource allKeys] containsObject:key]) {
//                        NSMutableArray *object = [_dataSource objectForKey:key];
//                        [object addObject:array[i]];
//                        [_dataSource setObject:object forKey:key];
//                    } else {
//                        NSMutableArray *object = [[NSMutableArray alloc] init];
//                        [object addObject:array[i]];
//                        [_dataSource setObject:object forKey:key];
//                    }
//                }
            }
        }
        
        _sectionsArray = [_dataSource allKeys];
        
        _sectionsArray = [_sectionsArray sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            return [obj1 compare:obj2 options:NSAnchoredSearch];
        }];
        [_rootView.tableView reloadData];
    } else {
        _dataSource = [_backupDataSource mutableCopy];
        _sectionsArray = [_dataSource allKeys];
        _sectionsArray = [_sectionsArray sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            return [obj1 compare:obj2 options:NSAnchoredSearch];
        }];
        [_rootView.tableView reloadData];
    }
}


#pragma mark - other
- (void)deleteBlacklistWithAccount:(NSString *)account {
    [TDNetworkRequest requestWithAccount:account
                                  action:DeleteBlacklist
                                 success:^(id responseObject)
    {
        [SVProgressHUD dismiss];
        
        /**
         *  修改本地数据源
         */
        for (LGDaKaInfo *userInfo in [TDSingleton instance].userInfoModel.blackListCache) {
            
            if ([userInfo.account isEqualToString:account]) {
                [[TDSingleton instance].userInfoModel.blackListCache removeObject:userInfo];
                break;
            }
        }
        
         NSLog(@"deleteBlacklistWithAccount responseObject:%@", responseObject);
    }];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDetailWithAccount:(NSString *)account {
    
    [SVProgressHUD showWithStatus:@"搜索中..." maskType:SVProgressHUDMaskTypeGradient];
    
    [LGDefineNetServer getUsersInfoWithAccounts:@[account] success:^(id result) {
        [SVProgressHUD dismiss];
        
        if ([result isKindOfClass:[NSArray class]]) {
            NSArray *array = result;
            
            if (array.count > 0) {
                LGDaKaInfo *bigShotInfo = [[LGDaKaInfo alloc] initWithDakaInfoHelp:result[0]];
                PeopleGameDetailViewController * vc = [[PeopleGameDetailViewController alloc] initWithDakaInfo:bigShotInfo];
                [self.navigationController pushViewController:vc animated:YES];
            }
        }
    } failure:^(NSString *msg) {
//        [SVProgressHUD dismissWithError:msg];
        [SVProgressHUD dismiss];
    }];
}

@end
