//
//  ContactsListView.h
//  AfterSchool
//
//  Created by Teson Draw on 11/24/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactsListRootView : UIView

@property (nonatomic, strong) UISearchBar *searchBar;
@property (nonatomic, strong) UITableView *tableView;

@end
