//
//  ContactsTableViewCell.m
//  AfterSchool
//
//  Created by Teson Draw on 11/24/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "ContactsTableViewCell.h"
#import "MacrosDefinition.h"

@implementation ContactsTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self addSubview:self.portraitBackgroundView];
        [_portraitBackgroundView addSubview:self.portraitImageView];
        [self addSubview:self.nameLabel];
        [self addSubview:self.bottomLineView];
    }
    return self;
}

- (UIView *)portraitBackgroundView {
    if (!_portraitBackgroundView) {
        _portraitBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(20, 9.5, 37, 37)];
        _portraitBackgroundView.backgroundColor = [UIColor whiteColor];
        
        _portraitBackgroundView.layer.cornerRadius = 5;
        _portraitBackgroundView.layer.borderColor = [UIColor colorWithRed:222/255.f green:222/255.f blue:222/255.f alpha:1].CGColor;
        _portraitBackgroundView.layer.borderWidth = 0.5;
    }
    return _portraitBackgroundView;
}

- (UIImageView *)portraitImageView {
    if (!_portraitImageView) {
        _portraitImageView = [[UIImageView alloc] initWithFrame:CGRectMake(2.5, 2.5, 32, 32)];

        _portraitImageView.layer.cornerRadius = 4;
        _portraitImageView.layer.masksToBounds = YES;
    }
    return _portraitImageView;
}

- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_portraitBackgroundView.frame) + 10, 0, 160, 50)];
        _nameLabel.font = [UIFont systemFontOfSize:16];
        _nameLabel.textColor = [UIColor colorWithRed:56/255.f green:56/255.f blue:56/255.f alpha:1];
    }
    return _nameLabel;
}

- (UIView *)bottomLineView {
    if (!_bottomLineView) {
        _bottomLineView = [[UIView alloc] initWithFrame:CGRectMake(20, 56 - 1, SCREEN_WIDTH - 20, 1)];
        _bottomLineView.backgroundColor = [UIColor colorWithRed:230/255.f green:230/255.f blue:230/255.f alpha:1];
    }
    return _bottomLineView;
}

@end
