//
//  ContactsListView.m
//  AfterSchool
//
//  Created by Teson Draw on 11/24/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "ContactsListRootView.h"
#import "UIImage+ImageWithColor.h"

@implementation ContactsListRootView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubview:self.searchBar];
        [self addSubview:self.tableView];
    }
    return self;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_searchBar.frame), self.bounds.size.width, self.bounds.size.height - CGRectGetMaxY(_searchBar.frame))];
        _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = [UIColor colorWithRed:230/255.f green:230/255.f blue:230/255.f alpha:1];
    }
    return _tableView;
}

- (UISearchBar *)searchBar {
    if (!_searchBar) {
        _searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, 44)];
        _searchBar.placeholder = @"搜索";
        _searchBar.keyboardType = UIKeyboardTypeDefault;
        _searchBar.autocapitalizationType = UITextAutocapitalizationTypeNone;
        _searchBar.tintColor = [UIColor blueColor];
        _searchBar.barTintColor = [UIColor colorWithRed:244/255.f green:244/255.f blue:244/255.f alpha:1];
        
        UIImage *image = [UIImage imageWithColor:[UIColor colorWithRed:244/255.f green:244/255.f blue:244/255.f alpha:1]];
        
        _searchBar.backgroundImage = image;
    }
    return _searchBar;
}

@end
