//
//  AboutMeRootView.h
//  AfterSchool
//
//  Created by Teson Draw on 10/6/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavigationBarView.h"

#import "UITableView+NibCell.h"
#import "LGAboutMeTableViewCellOne.h"
#import "LGAboutMeTableViewCellTwo.h"

@interface AboutMeRootView : UIView

@property (nonatomic, strong) NavigationBarView *navigationBarView;
@property (nonatomic, strong) UIButton *logoutButton;

@property (nonatomic, strong) UITableView * tableView;

//@property (nonatomic, strong) UIVisualEffectView *effectView;

@end
