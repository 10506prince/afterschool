//
//  LGSetupTableViewCell.h
//  AfterSchool
//
//  Created by lg on 15/10/21.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>

#define LGAboutMeTableViewCellTwoHeight 48.0f

@interface LGAboutMeTableViewCellTwo : UITableViewCell

@property (nonatomic, strong) IBOutlet UIImageView * img;
@property (nonatomic, strong) IBOutlet UILabel * name;
@property (nonatomic, strong) IBOutlet UILabel * detail;

@end
