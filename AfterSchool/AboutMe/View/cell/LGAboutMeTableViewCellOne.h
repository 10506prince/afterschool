//
//  LGUserInfoTableViewCell.h
//  AfterSchool
//
//  Created by lg on 15/10/21.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>

#define LGAboutMeTableViewCellOneHeight 76.0f

@interface LGAboutMeTableViewCellOne : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView * img;
@property (nonatomic, weak) IBOutlet UILabel * name;
@property (nonatomic, weak) IBOutlet UILabel * detail;

@end
