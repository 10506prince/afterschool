//
//  LGUserInfoTableViewCell.m
//  AfterSchool
//
//  Created by lg on 15/10/21.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGAboutMeTableViewCellOne.h"

@implementation LGAboutMeTableViewCellOne

- (void)awakeFromNib {
    [self.img.layer setCornerRadius:self.img.bounds.size.height/2];
    [self.img setClipsToBounds:YES];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
