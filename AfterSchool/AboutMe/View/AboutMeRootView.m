//
//  AboutMeRootView.m
//  AfterSchool
//
//  Created by Teson Draw on 10/6/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "AboutMeRootView.h"
#import "UIButton+SetBackgroundColor.h"
#import "MacrosDefinition.h"
//#import ""

@implementation AboutMeRootView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        
        [self addSubview:self.tableView];
        
        [self addSubview:self.navigationBarView];
    }
    return self;
}

- (NavigationBarView *)navigationBarView {
    if (!_navigationBarView) {
        _navigationBarView = [[NavigationBarView alloc] initWithTitle:@"我"
                                                           titleColor:[UIColor whiteColor]
                                                      backgroundColor:nil
                                                       leftButtonName:nil
                                                 leftButtonTitleColor:[UIColor whiteColor]
                                                  leftButtonImageName:nil
                                                      rightButtonName:nil
                                                 rightButtonImageName:nil];
    }
    return _navigationBarView;
}

- (UITableView *)tableView {
    if (!_tableView) {
        CGRect frame = self.bounds;

        _tableView = [[UITableView alloc] initWithFrame:frame style:UITableViewStyleGrouped];
        
        UIEdgeInsets ed = UIEdgeInsetsMake(64, 0, 49, 0);
        [_tableView setScrollIndicatorInsets:ed];
        [_tableView setContentInset:ed];
        [_tableView setDelaysContentTouches:NO];

        [_tableView registerNibCellWithClass:[LGAboutMeTableViewCellOne class]];
        [_tableView registerNibCellWithClass:[LGAboutMeTableViewCellTwo class]];
    }
    return _tableView;
}

//- (UIVisualEffectView *)effectView {
//    if (!_effectView) {
//        UIBlurEffect * blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight];
//        _effectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
//        _effectView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 64);
////        _effectView.contentView.backgroundColor = [UIColor colorWithRed:56/255.f green:55/255.f blue:60/255.f alpha:0.90]]
//    }
//    return _effectView;
//}

@end
