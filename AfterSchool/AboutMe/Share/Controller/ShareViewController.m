//
//  ShareViewController.m
//  AfterSchool
//
//  Created by lg on 15/10/22.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "ShareViewController.h"
#import "Common.h"
#import <ShareSDK/ShareSDK.h>
#import "SVProgressHUD.h"

#import "Common.h"
#import "TalkingData.h"

@implementation ShareViewController
- (void)dealloc
{
    NSLogSelfMethodName;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [TalkingData trackPageBegin:selfControllerName];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [TalkingData trackPageEnd:selfControllerName];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initData];
    [self initUserInterface];
}

- (void)initData {

}

- (void)initUserInterface {
    [self.view addSubview:self.rootView];
}

- (ShareRootView *)rootView {
    if (!_rootView) {
        _rootView = [[ShareRootView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        
        [_rootView.navigationBarView.leftButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        
        for (UIButton * btn in _rootView.shareBtns) {
            [btn addTarget:self action:@selector(shareBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        }
    }
    return _rootView;
}
#pragma mark - Action
- (void)backAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)shareBtnAction:(UIButton *)sender {
    ShareBtnType shareType = sender.tag;
    switch (shareType) {
        case ShareBtnTypeqqFriend:
        {
            [self creatShareWithShareType:ShareTypeQQ];
        }
            break;
        case ShareBtnTypeqZone:
        {
            [self creatShareWithShareType:ShareTypeQQSpace];
        }
            break;
        case ShareBtnTypeWechatTimeline:
        {
            [self creatShareWithShareType:ShareTypeWeixiTimeline];
        }
            break;
        case ShareBtnTypeWechatSession:
        {
            [self creatShareWithShareType:ShareTypeWeixiSession];
        }
            break;
        case ShareBtnTypeSinaWeibo:
        {
            [self creatShareWithShareType:ShareTypeSinaWeibo];
        }
            break;
            
        default:
            break;
    }
}

- (id<ISSContent>) getShareContentWithShareType:(ShareType)shareType {
    
    NSString *imagePath = [[NSBundle mainBundle] pathForResource:@"app_icon_180x180" ofType:@"png"];
    
    //构造分享内容
    id<ISSContent> shareContent = [ShareSDK content:@"你还在为上分难犯愁吗，你还在为找不到妹子彷徨吗，你还在为独自玩游戏感到无聊吗，上放学约，你的烦恼从此不在！"
                                       defaultContent:@"你还在为上分难犯愁吗，你还在为找不到妹子彷徨吗，你还在为独自玩游戏感到无聊吗，上放学约，你的烦恼从此不在！"
                                                image:[ShareSDK imageWithPath:imagePath]
                                                title:@"放学约"
                                                  url:@"http://www.fangxueyue.com/"
                                          description:@"你还在为上分难犯愁吗，你还在为找不到妹子彷徨吗，你还在为独自玩游戏感到无聊吗，上放学约，你的烦恼从此不在！"
                                            mediaType:SSPublishContentMediaTypeNews];
    
//    SSPublishContentMediaTypeText = 0, /**< 文本 */
//    SSPublishContentMediaTypeImage = 1, /**< 图片 */
//    SSPublishContentMediaTypeNews = 2, /**< 新闻 */
//    SSPublishContentMediaTypeMusic = 3, /**< 音乐 */
//    SSPublishContentMediaTypeVideo = 4, /**< 视频 */
//    SSPublishContentMediaTypeApp = 5, /**< 应用,仅供微信使用 */
//    SSPublishContentMediaTypeNonGif = 6, /**< 非Gif消息,仅供微信使用 */
//    SSPublishContentMediaTypeGif = 7 /**< Gif消息,仅供微信使用 */
    
    return shareContent;
}

- (void)creatShareWithShareType:(ShareType)shareType {
    
    //构造分享内容
    id<ISSContent> publishContent = [self getShareContentWithShareType:shareType];

    [ShareSDK shareContent:publishContent type:shareType authOptions:nil shareOptions:nil statusBarTips:YES result:^(ShareType type, SSResponseState state, id<ISSPlatformShareInfo> statusInfo, id<ICMErrorInfo> error, BOOL end) {
        if (state == SSPublishContentStateSuccess)
        {
            NSLog(NSLocalizedString(@"TEXT_SHARE_SUC", @"发表成功"));
            [SVProgressHUD showSuccessWithStatus:@"分享成功"];
        } else if (state == SSPublishContentStateFail) {
            [SVProgressHUD showErrorWithStatus:@"分享失败"];
        } else if (state == SSPublishContentStateCancel) {
            [SVProgressHUD showErrorWithStatus:@"取消分享"];
        }
    }];
}

@end
