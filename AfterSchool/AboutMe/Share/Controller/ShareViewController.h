//
//  ShareViewController.h
//  AfterSchool
//
//  Created by lg on 15/10/22.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGViewController.h"
#import "ShareRootView.h"

@interface ShareViewController : LGViewController

@property (nonatomic, strong) ShareRootView * rootView;

@end
