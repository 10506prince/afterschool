//
//  ShareRootView.h
//  AfterSchool
//
//  Created by lg on 15/10/22.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavigationBarView.h"
#import "LGScrollView.h"

typedef NS_ENUM(NSUInteger, ShareBtnType) {
    ShareBtnTypeSinaWeibo,///<新浪分享
    ShareBtnTypeWechatSession,///<微信好友分享
    ShareBtnTypeWechatTimeline,///<微信朋友圈分享
    ShareBtnTypeqqFriend,///<QQ好友分享
    ShareBtnTypeqZone///<QQ空间分享
};

@interface ShareRootView : UIView

@property (nonatomic, strong) NavigationBarView * navigationBarView;
@property (nonatomic, strong) LGScrollView * scrollView;

@property (nonatomic, strong) NSArray <UIButton *> * shareBtns;

@end
