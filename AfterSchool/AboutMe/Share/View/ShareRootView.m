//
//  ShareRootView.m
//  AfterSchool
//
//  Created by lg on 15/10/22.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "ShareRootView.h"

@implementation ShareRootView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self addSubview:self.scrollView];
        [self addSubview:self.navigationBarView];
    }
    return self;
}

- (NavigationBarView *)navigationBarView {
    if (!_navigationBarView) {

        _navigationBarView = [[NavigationBarView alloc] initWithTitle:@"分享"
                                                           titleColor:nil
                                                      backgroundColor:nil
                                              leftButtonBgViewOriginX:0
                                                 leftButtonImageWidth:22
                                                 leftButtonTitleColor:[UIColor whiteColor]
                                                       leftButtonName:@"返回"
                                                  leftButtonImageName:@"navigation_bar_return_button_left_arrow" rightButtonName:nil
                                                 rightButtonImageName:nil];
    }
    return _navigationBarView;
}

- (LGScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[LGScrollView alloc] initWithFrame:self.bounds];
        [_scrollView setBounces:NO];
        
        NSMutableArray * btns = [NSMutableArray array];
        NSArray * names = @[@"新浪微博",@"微信",@"朋友圈",@"QQ好友",@"QQ空间"];
        NSArray * imageNames = @[@"share_weibo",@"share_weixin",@"share_moments",@"share_QQ",@"share_QQ_zone"];
        CGFloat height = 0;
        int tags[5] = {ShareBtnTypeSinaWeibo,ShareBtnTypeWechatSession,ShareBtnTypeWechatTimeline,ShareBtnTypeqqFriend,ShareBtnTypeqZone};
        
        CGFloat btnWeight = 80;
        CGFloat btnHeight = 80;
        
        int lineBtnMaxNumber = 3;
        
        UIEdgeInsets ed = UIEdgeInsetsMake(22, 12, 22, 12);//上下左右
        CGFloat lineSpacing = 22;//行间距
        CGFloat interitemSpacing = lineBtnMaxNumber > 1 ? (self.bounds.size.width - ed.left - ed.right - btnWeight * lineBtnMaxNumber)/(lineBtnMaxNumber -1) : 0;//列间距
        
        for (int i = 0; i < names.count; i++) {
            UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
            CGFloat xx = ed.left;
            CGFloat yy = ed.top;
            
            int lie = i%lineBtnMaxNumber;
            int hang = i/lineBtnMaxNumber;
            
            xx += lie * (btnWeight + interitemSpacing);
            yy += hang * (btnWeight + lineSpacing);
            
            [btn setFrame:CGRectMake(xx, yy, btnWeight, btnHeight)];
            //[btn setTitle:names[i] forState:UIControlStateNormal];
            [btn setTag:tags[i]];
            //[btn setBackgroundColor:[UIColor orangeColor]];
            //[btn setBackgroundImage:[UIImage imageNamed:imageNames[i]] forState:UIControlStateNormal | UIControlStateHighlighted];
            [btn setImage:[UIImage imageNamed:imageNames[i]] forState:UIControlStateNormal];
            
            [btns addObject:btn];
            [_scrollView addSubview:btn];
            
            height = btn.frame.origin.y + btn.bounds.size.height + 22;
        }
        
        self.shareBtns = [NSArray arrayWithArray:btns];
        [_scrollView setContentSize:CGSizeMake(self.bounds.size.width, height)];
    }
    return _scrollView;
}


@end
