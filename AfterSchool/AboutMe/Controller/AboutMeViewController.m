//
//  AboutMeViewController.m
//  AfterSchool
//
//  Created by Teson Draw on 10/6/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "AboutMeViewController.h"
#import "MacrosDefinition.h"
#import "UIViewController+UINib.h"
#import "Common.h"
#import "UIColor+RGB.h"
#import "UIImageView+WebCache.h"
#import "TDSingleton.h"

#import "UserCenterViewController.h"
#import "WalletViewController.h"
#import "RelationshipsViewController.h"
#import "OrdersViewController.h"
#import "CityViewController.h"
#import "ShareViewController.h"
#import "GameAccountViewController.h"
#import "SetupViewController.h"

#import "SVProgressHUD.h"

#import "TalkingData.h"

@interface AboutMeViewController () <UITableViewDataSource, UITableViewDelegate> {
    NSArray * _dataSource;
    
    NSArray*	_cellNameArray;
    NSArray*    _cellImageNameArray;
    NSArray*	_viewControllerArray;
    NSArray*	_viewControllerTitleArray;
}

@end

@implementation AboutMeViewController

- (void)dealloc
{
    NSLog(@"AboutMeViewController 控制器释放了");
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tabBarController.tabBar setHidden:NO];
    
    [self.rootView.tableView reloadData];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [TalkingData trackPageBegin:selfControllerName];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [TalkingData trackPageEnd:selfControllerName];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initData];
    [self initUserInterface];
}

- (void)initData {
    _dataSource = @[
  @[@"用户"],
  @[@"钱包", @"关系", @"订单"],
  @[@"城市", @"分享"],
  @[@"游戏账号"],
  @[@"设置"],
  ];
    
    _cellNameArray = @[@"", @"钱包", @"关系", @"订单", @"城市", @"分享", @"游戏账号", @"设置"];
    _cellImageNameArray = @[@"aboutme_usercenter",
                            @"aboutme_wallet",
                            @"aboutme_relationships",
                            @"aboutme_order",
                            @"aboutme_city",
                            @"aboutme_share",
                            @"aboutme_game",
                            @"aboutme_setup",
                            ];
    
    _viewControllerTitleArray = @[];
    
    _viewControllerArray = @[@"UserCenterViewController",
                             @"WalletViewController",
                             @"RelationshipsViewController",
                             @"OrdersViewController",
                             @"CityViewController",
                             @"ShareViewController",
                             @"GameAccountViewController",
                             @"SetupViewController",
                             ];
    
}

- (void)initUserInterface {
    self.navigationController.navigationBarHidden = YES;
    [self.view addSubview:self.rootView];
}

- (AboutMeRootView *)rootView {
    if (!_rootView) {
        _rootView = [[AboutMeRootView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        
        [_rootView.tableView setDelegate:self];
        [_rootView.tableView setDataSource:self];
    }
    return _rootView;
}

#pragma mark - UITableViewDataSource 列表数据代理
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _dataSource.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (_dataSource.count == 0 || _dataSource.count < section + 1) {
        return 0;
    }
    
    NSArray * array = [_dataSource objectAtIndex:section];
    
    return array.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSUInteger index = [self getIndexFromIndexPath:indexPath];
    NSString * name = _cellNameArray[index];
    NSString * imageName = _cellImageNameArray[index];
    
    switch (indexPath.section) {
        case 0://用户基本信息
        {
            LGAboutMeTableViewCellOne * cell = [tableView dequeueReusableCellWithClass:[LGAboutMeTableViewCellOne class]];
            
            NSString * userName = [TDSingleton instance].userInfoModel.nickName;
            NSString * userInfo = [TDSingleton instance].userInfoModel.info;
            NSString * headImg = [TDSingleton instance].userInfoModel.portraitURL;
            NSURL * headUrl = [NSURL URLWithString:headImg];
            
            
            [cell.img sd_setImageWithURL:headUrl placeholderImage:[UIImage imageNamed:@"user_default_head"]];
            [cell.name setText:userName];
            if (userInfo.length == 0) {
                [cell.detail setText:@"这个人很懒，什么都没有留下..."];
            } else {
                [cell.detail setText:userInfo];
            }
            
            return cell;
        }
            break;
        case 1://订单相关
        {
            LGAboutMeTableViewCellTwo * cell = [tableView dequeueReusableCellWithClass:[LGAboutMeTableViewCellTwo class]];
            [cell.img setImage:[UIImage imageNamed:imageName]];
            [cell.name setText:name];
            
            return cell;
        }
            break;
        case 2://常用设置
        {
            LGAboutMeTableViewCellTwo * cell = [tableView dequeueReusableCellWithClass:[LGAboutMeTableViewCellTwo class]];
            [cell.img setImage:[UIImage imageNamed:imageName]];
            [cell.name setText:name];
            
            return cell;
        }
            break;
        case 4://其他设置
        {
            LGAboutMeTableViewCellTwo * cell = [tableView dequeueReusableCellWithClass:[LGAboutMeTableViewCellTwo class]];
            [cell.img setImage:[UIImage imageNamed:imageName]];
            [cell.name setText:name];
            
            return cell;
        }
            break;
        case 3://关联账号
        {
            LGAboutMeTableViewCellTwo * cell = [tableView dequeueReusableCellWithClass:[LGAboutMeTableViewCellTwo class]];
            [cell.img setImage:[UIImage imageNamed:imageName]];
            [cell.name setText:name];
            
            return cell;
        }
            break;
            
        default:
            return nil;
            break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case 0:
            return LGAboutMeTableViewCellOneHeight;
            break;
        case 1:
        case 2:
        case 3:
        case 4:
            return LGAboutMeTableViewCellTwoHeight;
            break;
            
        default:
            return SINGLE_LINE_WIDTH;
            break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    switch (section) {
        case 0:
        case 1:
        case 2:
        case 3:
        case 4:
            return 22.0f;
            break;
            
        default:
            return SINGLE_LINE_WIDTH;
            break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    switch (section) {
        case 4:
            return 22;
            break;
            
        default:
            return SINGLE_LINE_WIDTH;
            break;
    }
}

#pragma mark - UITableViewDelegate 列表控制代理
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath  {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSUInteger index = [self getIndexFromIndexPath:indexPath];
    
    if (index < _viewControllerArray.count) {

        UIViewController * nextPageVC = [[NSClassFromString([_viewControllerArray objectAtIndex:index]) alloc] init];
        [self.navigationController pushViewController:nextPageVC animated:YES];
    }
}

#pragma mark - 页面切换
- (NSUInteger)getIndexFromIndexPath:(NSIndexPath *)indexPath {
    NSUInteger index = 0;
    for (int i = 0; i < indexPath.section; i++) {
        NSUInteger row = [self tableView:self.rootView.tableView numberOfRowsInSection:i];
        index += row;
    }
    index += indexPath.row;
    
    return index;
}


@end
