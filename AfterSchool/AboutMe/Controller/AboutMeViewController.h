//
//  AboutMeViewController.h
//  AfterSchool
//
//  Created by Teson Draw on 10/6/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "LGViewController.h"
#import "AboutMeRootView.h"

@interface AboutMeViewController : LGViewController

@property (nonatomic, strong) AboutMeRootView *rootView;

@end
