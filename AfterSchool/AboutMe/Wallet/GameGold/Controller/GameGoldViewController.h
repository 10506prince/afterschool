//
//  GameGoldViewController.h
//  AfterSchool
//
//  Created by Teson Draw on 11/12/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GameGoldRootView.h"

@interface GameGoldViewController : UIViewController

@property (nonatomic, strong) GameGoldRootView *rootView;
@property (nonatomic, strong) NSString *gameGold;

@end
