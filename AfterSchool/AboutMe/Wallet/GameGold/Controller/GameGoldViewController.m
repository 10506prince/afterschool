//
//  GameGoldViewController.m
//  AfterSchool
//
//  Created by Teson Draw on 11/12/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "GameGoldViewController.h"
#import "MacrosDefinition.h"
#import "Toast+UIView.h"

#import "TDMessageBox.h"

#import "LGShareViewController.h"

#import "Common.h"
#import "TalkingData.h"

@interface GameGoldViewController ()

@property (nonatomic, strong) LGShareViewController * shareVC;

@end

@implementation GameGoldViewController
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [TalkingData trackPageBegin:selfControllerName];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [TalkingData trackPageEnd:selfControllerName];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initUserInterface];
    
    LGShareViewController * shareVC = [[LGShareViewController alloc] initWithNibName:@"LGShareViewController" bundle:nil];
    shareVC.publishContentMediaType = SSPublishContentMediaTypeNews;
    shareVC.view.frame = [UIScreen mainScreen].bounds;
    self.shareVC = shareVC;
    [self addChildViewController:shareVC];
}

- (void)initUserInterface {
    [self.view addSubview:self.rootView];
    _rootView.amountLabel.text = [NSString stringWithFormat:@"%@枚", _gameGold];
}

- (GameGoldRootView *)rootView {
    if (!_rootView) {
        _rootView = [[GameGoldRootView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
        
        [_rootView.navigationBarView.leftButton addTarget:self action:@selector(returnButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        [_rootView.tipButton addTarget:self action:@selector(tipButtonTouchDown) forControlEvents:UIControlEventTouchDown];
        [_rootView.tipButton addTarget:self action:@selector(tipButtonTouchUpInside) forControlEvents:UIControlEventTouchUpInside];
        [_rootView.tipButton addTarget:self action:@selector(tipButtonTouchUpOutside) forControlEvents:UIControlEventTouchUpOutside];
        
        [_rootView.detailButton addTarget:self action:@selector(detailButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        
        [_rootView.inviteButton addTarget:self action:@selector(inviteAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _rootView;
}

- (void)returnButtonClicked {
    [self.navigationController popViewControllerAnimated:YES];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)tipButtonTouchDown {
    [self.rootView addSubview:_rootView.tipView];
}

- (void)tipButtonTouchUpInside {
    [_rootView.tipView removeFromSuperview];
    _rootView.tipView = nil;
}

- (void)tipButtonTouchUpOutside {
    [_rootView.tipView removeFromSuperview];
    _rootView.tipView = nil;
}

- (void)detailButtonClicked {
    [TDMessageBox show];
}

- (IBAction)inviteAction:(id)sender {
    [self.view addSubview:self.shareVC.view];
}

@end
