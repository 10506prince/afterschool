//
//  GameGoldRootView.m
//  AfterSchool
//
//  Created by Teson Draw on 11/12/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "GameGoldRootView.h"
#import "MacrosDefinition.h"
#import "UIButton+SetBackgroundColor.h"

@implementation GameGoldRootView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithRed:240/255.f green:239/255.f blue:244/255.f alpha:1];
        [self addSubview:self.navigationBarView];
        [self addSubview:self.logoImageView];
        [self addSubview:self.titleLabel];
        [self addSubview:self.tipImageView];
        [self addSubview:self.tipButton];
        [self addSubview:self.amountLabel];
        [self addSubview:self.descriptionLabel];
        [self addSubview:self.descriptionTrailerLabel];
//        [_descriptionTrailerLabel addSubview:self.detailButton];
        [self addSubview:self.inviteButton];
    }
    
    return self;
}

- (NavigationBarView *)navigationBarView {
    if (!_navigationBarView) {
//        _navigationBarView = [[NavigationBarView alloc] initWithTitle:@"游戏币" titleColor:[UIColor whiteColor] backgroundColor:nil leftButtonImageName:@"navigation_bar_return_button" rightButtonImageName:nil];
        _navigationBarView = [[NavigationBarView alloc] initWithTitle:@"游戏币"
                                                           titleColor:nil
                                                      backgroundColor:nil
                                              leftButtonBgViewOriginX:0
                                                 leftButtonImageWidth:22
                                                 leftButtonTitleColor:[UIColor whiteColor]
                                                       leftButtonName:@"返回"
                                                  leftButtonImageName:@"navigation_bar_return_button_left_arrow" rightButtonName:nil
                                                 rightButtonImageName:nil];
    }
    return _navigationBarView;
}

- (UIImageView *)logoImageView {
    if (!_logoImageView) {
        _logoImageView = [[UIImageView alloc] initWithFrame:CGRectMake((SCREEN_WIDTH - 112) / 2, 64 + 40, 112, 112)];
        
        if (iPhone4s) {
            _logoImageView = [[UIImageView alloc] initWithFrame:CGRectMake((SCREEN_WIDTH - 112) / 2, 64 + 20, 112, 112)];
        }
        
        _logoImageView.image = [UIImage imageNamed:@"about_me_game_gold_logo"];
        _logoImageView.layer.cornerRadius = 56;
        _logoImageView.layer.masksToBounds = YES;
    }
    return _logoImageView;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake((SCREEN_WIDTH - 80) / 2, CGRectGetMaxY(_logoImageView.frame) + 16, 80, 16)];
        _titleLabel.textColor = [UIColor blackColor];
        _titleLabel.font = [UIFont systemFontOfSize:16];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.text = @"我的游戏币";
//        _titleLabel.layer.borderColor = [UIColor blackColor].CGColor;
//        _titleLabel.layer.borderWidth = 1;
    }
    return _titleLabel;
}

- (UIImageView *)tipImageView {
    if (!_tipImageView) {
        _tipImageView = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_titleLabel.frame) + 4, _titleLabel.frame.origin.y, 14, 14)];
        _tipImageView.image = [UIImage imageNamed:@"about_me_game_gold_exclamation_mark"];
        _tipImageView.layer.cornerRadius = 7;
        _tipImageView.layer.masksToBounds = YES;
        
//        _tipImageView.layer.borderColor = [UIColor blackColor].CGColor;
//        _tipImageView.layer.borderWidth = 1;
    }
    return _tipImageView;
}

- (UIButton *)tipButton {
    if (!_tipButton) {
        _tipButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
        _tipButton.center = _tipImageView.center;
//        _tipButton.layer.borderColor = [UIColor blackColor].CGColor;
//        _tipButton.layer.borderWidth = 1;
    }
    
    return _tipButton;
}

- (UILabel *)amountLabel {
    if (!_amountLabel) {
        _amountLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_titleLabel.frame) + 20, SCREEN_WIDTH, 32)];
        _amountLabel.textColor = [UIColor blackColor];
        _amountLabel.font = [UIFont systemFontOfSize:32];
        _amountLabel.textAlignment = NSTextAlignmentCenter;
        _amountLabel.text = @"0 枚";
    }
    return _amountLabel;
}

- (UILabel *)descriptionLabel {
    if (!_descriptionLabel) {
        _descriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, CGRectGetMaxY(_amountLabel.frame) + 40, SCREEN_WIDTH - 60, 14)];
        _descriptionLabel.textColor = [UIColor blackColor];
        _descriptionLabel.font = [UIFont systemFontOfSize:14];
        _descriptionLabel.textAlignment = NSTextAlignmentCenter;
        _descriptionLabel.text = @"";
//        _descriptionLabel.layer.borderColor = [UIColor blackColor].CGColor;
//        _descriptionLabel.layer.borderWidth = 1;
//        _descriptionLabel.numberOfLines = 0;
    }
    return _descriptionLabel;
}

- (UILabel *)descriptionTrailerLabel {
    if (!_descriptionTrailerLabel) {
        _descriptionTrailerLabel = [[UILabel alloc] initWithFrame:CGRectMake((SCREEN_WIDTH - 120) / 2, CGRectGetMaxY(_descriptionLabel.frame) + 6, 120, 15)];
        _descriptionTrailerLabel.textColor = [UIColor blackColor];
        _descriptionTrailerLabel.font = [UIFont systemFontOfSize:15];
        _descriptionTrailerLabel.textAlignment = NSTextAlignmentLeft;
        _descriptionTrailerLabel.text = @"";
        _descriptionTrailerLabel.userInteractionEnabled = YES;
//        _descriptionTrailerLabel.layer.borderColor = [UIColor redColor].CGColor;
//        _descriptionTrailerLabel.layer.borderWidth = 1;
//        _descriptionTrailerLabel.numberOfLines = 0;
    }
    return _descriptionTrailerLabel;
}

- (UIUnderlinedButton *)detailButton {
    if (!_detailButton) {
        _detailButton = [[UIUnderlinedButton alloc] initWithFrame:CGRectMake(_descriptionTrailerLabel.bounds.size.width - 40, -14.5, 44, 44)];
        [_detailButton setTitle:@"详情" forState:UIControlStateNormal];
        [_detailButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
        [_detailButton setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];

        _detailButton.titleLabel.font = [UIFont systemFontOfSize:15];
//        _detailButton.layer.borderColor = [UIColor blackColor].CGColor;
//        _detailButton.layer.borderWidth = 1;
    }
    
    return _detailButton;
}

- (UIButton *)inviteButton {
    if (!_inviteButton) {
        _inviteButton = [[UIButton alloc] initWithFrame:CGRectMake(12, CGRectGetMaxY(_descriptionLabel.frame) + 60, SCREEN_WIDTH - 24, 46)];
        [_inviteButton setTitle:@"分享" forState:UIControlStateNormal];
        [_inviteButton setBackgroundColor:[UIColor colorWithRed:37/255.f green:183/255.f blue:237/255.0 alpha:1]];
        [_inviteButton setBackgroundColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
        _inviteButton.layer.cornerRadius = 5;
        _inviteButton.layer.masksToBounds = YES;
        _inviteButton.titleLabel.font = [UIFont systemFontOfSize:20];
    }
    
    return _inviteButton;
}

- (GameGoldTipView *)tipView {
    if (!_tipView) {
        _tipView = [[GameGoldTipView alloc] initWithOrigin:CGPointMake((SCREEN_WIDTH - 200) / 2, 84)];
    }
    
    return _tipView;
}

@end
