//
//  GameGoldRootView.h
//  AfterSchool
//
//  Created by Teson Draw on 11/12/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavigationBarView.h"
#import "UIUnderlinedButton.h"
#import "GameGoldTipView.h"

//#import "TDMessageBox.h"

@interface GameGoldRootView : UIView

@property (nonatomic, strong) NavigationBarView *navigationBarView;
@property (nonatomic, strong) UIImageView *logoImageView;

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *amountLabel;
@property (nonatomic, strong) UILabel *descriptionLabel;
@property (nonatomic, strong) UILabel *descriptionTrailerLabel;

@property (nonatomic, strong) UIUnderlinedButton *detailButton;
@property (nonatomic, strong) UIButton *inviteButton;

@property (nonatomic, strong) UIImageView *tipImageView;
@property (nonatomic, strong) UIButton *tipButton;

@property (nonatomic, strong) GameGoldTipView *tipView;

//@property (nonatomic, strong) TDMessageBox *detailMessageBox;

@end
