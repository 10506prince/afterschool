//
//  WalletViewController.m
//  AfterSchool
//
//  Created by lg on 15/10/22.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "WalletViewController.h"
#import "Common.h"
#import "SVProgressHUD.h"
#import "LGDefineNetServer.h"

#import "MoneyViewController.h"
#import "GameGoldViewController.h"

#import "LGDefineNetServer.h"

#import "Common.h"
#import "TalkingData.h"

@interface WalletViewController () {
//    NSMutableArray * _dataSource;
    NSString *_money;
    NSString *_gameGold;
}

@end

@implementation WalletViewController

- (void)dealloc
{
    NSLogSelfMethodName;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [TalkingData trackPageBegin:selfControllerName];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [TalkingData trackPageEnd:selfControllerName];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initUserInterface];
    [self initData];
}

- (void)initData {
    [self getMoney];
    
//    _dataSource = [[NSMutableArray alloc] initWithObjects:@"零钱", @"游戏币", nil];
    
}


- (void)initUserInterface {
    [self.view addSubview:self.rootView];
}

- (WalletRootView *)rootView {
    if (!_rootView) {
        _rootView = [[WalletRootView alloc] initWithFrame:self.view.bounds];
        [_rootView.navigationBarView.leftButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        [_rootView.moneyOptionView.optionButton addTarget:self action:@selector(moneyButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        [_rootView.gameGoldOptionView.optionButton addTarget:self action:@selector(gameGoldButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        
    }
    
    return _rootView;
}

#pragma mark - Action
- (void)backAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)moneyButtonClicked {
    MoneyViewController *vc = [[MoneyViewController alloc] init];
    vc.money = _money;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)gameGoldButtonClicked {
    GameGoldViewController *vc = [[GameGoldViewController alloc] init];
    vc.gameGold = _gameGold;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)getMoney {
    [SVProgressHUD showWithStatus:@"拼命加载数据..." maskType:SVProgressHUDMaskTypeClear];
    [LGDefineNetServer getWalletInfoSuccess:^(id result) {
        [SVProgressHUD dismiss];
        
        NSLog(@"getMoney info:%@", result);
        _money = [NSString stringWithFormat:@"%.2f", [result[@"money"] floatValue]];
        _gameGold = [NSString stringWithFormat:@"%@", result[@"tokenMoney"]];

        [self refreshUI];
    } failure:^(NSString *msg) {
        [SVProgressHUD dismissWithError:msg];
    }];
}

- (void)refreshUI {
    _rootView.moneyOptionView.contentLabel.text = _money;
    _rootView.gameGoldOptionView.contentLabel.text = _gameGold;
}

//#pragma mark - UITableViewDataSource 列表数据代理
//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//    return 1;
//}
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    return _dataSource.count;
//}
//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//    return nil;
//}
//#pragma mark - UITableViewDelegate 列表代理
//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    
//}
//
////- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
////    return 44.0f;
////}
//
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    return 44.0f;
//}
@end
