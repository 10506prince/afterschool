//
//  WalletViewController.h
//  AfterSchool
//
//  Created by lg on 15/10/22.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGViewController.h"
#import "WalletRootView.h"

@interface WalletViewController : LGViewController

@property (nonatomic, strong) WalletRootView * rootView;

@end
