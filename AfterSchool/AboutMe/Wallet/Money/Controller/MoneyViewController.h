//
//  MoneyViewController.h
//  AfterSchool
//
//  Created by Teson Draw on 11/12/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MoneyRootView.h"

@interface MoneyViewController : UIViewController

@property (nonatomic, strong) MoneyRootView *rootView;
@property (nonatomic, strong) NSString *money;

@end
