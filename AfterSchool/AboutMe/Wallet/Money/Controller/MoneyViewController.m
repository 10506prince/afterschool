//
//  MoneyViewController.m
//  AfterSchool
//
//  Created by Teson Draw on 11/12/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "MoneyViewController.h"
#import "MacrosDefinition.h"
#import "PaymentMethodViewController.h"
#import "NotificationName.h"
#import "LGDefineNetServer.h"

#import "RechargeViewController.h"
#import "Pingpp.h"
//#import "WXApi.h"
#import "WXApiManager.h"
#import "TDNetworking.h"
#import "TDSingleton.h"
#import "WithdrawCashViewController.h"

#import "Common.h"
#import "SVProgressHUD.h"
#import "TalkingData.h"

@interface MoneyViewController () <WXApiManagerDelegate>

@end

@implementation MoneyViewController
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [TalkingData trackPageBegin:selfControllerName];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [TalkingData trackPageEnd:selfControllerName];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initData];
    [self initUserInterface];
}

- (void)initData {
    [WXApiManager sharedManager].delegate = self;
}

- (void)initUserInterface {
    [self.view addSubview:self.rootView];
    _rootView.amountLabel.text = [NSString stringWithFormat:@"￥%@", _money];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(chargeSuccess) name:TDChargeSuccess object:nil];
    
    NSLog(@"Ping++ Version:%@", [Pingpp version]);
}

- (MoneyRootView *)rootView {
    if (!_rootView) {
        _rootView = [[MoneyRootView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
        
        [_rootView.navigationBarView.leftButton addTarget:self action:@selector(returnButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        
        [_rootView.chargeButton addTarget:self action:@selector(chargeButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        [_rootView.withdrawCashButton addTarget:self action:@selector(withdrawCashButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    }
    return _rootView;
}

- (void)returnButtonClicked {
    [self.navigationController popViewControllerAnimated:YES];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)chargeButtonClicked {
//    PaymentMethodViewController *vc = [[PaymentMethodViewController alloc] init];
//    vc.amount = 0;
//    [self.navigationController pushViewController:vc animated:YES];
    
    __weak typeof(self) weakSelf = self;
    
    RechargeViewController * vc = [RechargeViewController rechargeViewControllerWithFinishBlock:^(BOOL success) {
        if (success) {
            [weakSelf chargeSuccess];
            [weakSelf.navigationController popViewControllerAnimated:YES];
        }
    }];
                                    
    
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)withdrawCashButtonClicked {
    NSString *kAuthScope = @"snsapi_userinfo";//snsapi_base//snsapi_message,snsapi_userinfo,snsapi_friend,snsapi_contact
    NSString *kAuthState = @"withdrawCash";
    NSString *kAuthOpenID = @"";//@"0c806938e2413ce73eef92cc3";//fangxuekeji//wxa333febe202f7ac7//wx9fdbe0ccbadfe82c

    SendAuthReq* req = [[SendAuthReq alloc] init];
    req.scope = kAuthScope;
    req.state = kAuthState;
    req.openID = kAuthOpenID;
    
    __weak typeof(self) weakSelf = self;
    
    [WXApi sendAuthReq:req viewController:weakSelf delegate:[WXApiManager sharedManager]];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)chargeSuccess {
    [LGDefineNetServer getWalletInfoSuccess:^(id result) {
        NSLog(@"getMoney info:%@", result);
        _money = [NSString stringWithFormat:@"%.2f", [result[@"money"] floatValue]];
        _rootView.amountLabel.text = _money;//[NSString stringWithFormat:@"￥%@", _money];

    } failure:^(NSString *msg) {
        [SVProgressHUD dismissWithError:msg];
    }];
}

- (void)managerDidRecvAuthResponse:(SendAuthResp *)response {
    if (response.errCode == 0) {
        WithdrawCashViewController *vc = [[WithdrawCashViewController alloc] init];
        vc.money = _money;
        vc.code = response.code;
        [self.navigationController pushViewController:vc animated:YES];
//        [self requestWithCode:response.code];
    } else {
        [SVProgressHUD showErrorWithStatus:response.errStr];
    }
}

//- (void)requestWithCode:(NSString *)code {
//    //移动应用
//    NSDictionary *parameters = @{@"appid":@"wx9fdbe0ccbadfe82c",
//                                 @"secret":@"b6d4922288992d36c54f3c93903d518a",
//                                 @"code":code,
//                                 @"grant_type":@"authorization_code"};
//    
//    //公众号
////    NSDictionary *parameters = @{@"appid":@"wxa333febe202f7ac7",
////                                 @"secret":@"d4624c36b6795d1d99dcf0547af5443d",
////                                 @"code":code,
////                                 @"grant_type":@"authorization_code"};
//    
//    NSString *url = @"https://api.weixin.qq.com/sns/oauth2/access_token";
//    
//    NSLog(@"parameters:%@", parameters);
//    
//    [TDNetworking requestWithURL:url
//                      parameters:parameters
//                         success:^(id responseObject)
//     {
//         NSLog(@"requestWithCode responseObject:%@", responseObject);
////         if ([responseObject[@"result"] integerValue] == 1) {
////             [SVProgressHUD dismiss];
////         } else {
////             [SVProgressHUD dismissWithError:[NSString stringWithFormat:@"%@", responseObject[@"msg"]]];
////         }
//     } failure:^(NSError *error) {
//         NSLog(@"error:%@", error);
//     }];
//}

@end
