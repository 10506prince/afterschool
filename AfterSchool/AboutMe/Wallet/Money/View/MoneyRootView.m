//
//  MoneyRootView.m
//  AfterSchool
//
//  Created by Teson Draw on 11/12/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "MoneyRootView.h"
#import "MacrosDefinition.h"
#import "UIButton+SetBackgroundColor.h"

@implementation MoneyRootView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithRed:240/255.f green:239/255.f blue:244/255.f alpha:1];
        [self addSubview:self.navigationBarView];
        [self addSubview:self.logoImageView];
        [self addSubview:self.titleLabel];
        [self addSubview:self.amountLabel];
        [self addSubview:self.chargeButton];
        [self addSubview:self.withdrawCashButton];
    }
    
    return self;
}

- (NavigationBarView *)navigationBarView {
    if (!_navigationBarView) {
//        _navigationBarView = [[NavigationBarView alloc] initWithTitle:@"零钱" titleColor:[UIColor whiteColor] backgroundColor:nil leftButtonImageName:@"navigation_bar_return_button" rightButtonImageName:nil];
        _navigationBarView = [[NavigationBarView alloc] initWithTitle:@"零钱"
                                                           titleColor:nil
                                                      backgroundColor:nil
                                              leftButtonBgViewOriginX:0
                                                 leftButtonImageWidth:22
                                                 leftButtonTitleColor:[UIColor whiteColor]
                                                       leftButtonName:@"返回"
                                                  leftButtonImageName:@"navigation_bar_return_button_left_arrow" rightButtonName:nil
                                                 rightButtonImageName:nil];
    }
    return _navigationBarView;
}

- (UIImageView *)logoImageView {
    if (!_logoImageView) {
        _logoImageView = [[UIImageView alloc] initWithFrame:CGRectMake((SCREEN_WIDTH - 112) / 2, 64 + 40, 112, 112)];
        if (iPhone4s) {
            _logoImageView = [[UIImageView alloc] initWithFrame:CGRectMake((SCREEN_WIDTH - 112) / 2, 64 + 20, 112, 112)];
        }
        _logoImageView.image = [UIImage imageNamed:@"about_me_money_logo"];
        _logoImageView.layer.cornerRadius = 56;
        _logoImageView.layer.masksToBounds = YES;
    }
    return _logoImageView;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_logoImageView.frame) + 16, SCREEN_WIDTH, 16)];
        _titleLabel.textColor = [UIColor blackColor];
        _titleLabel.font = [UIFont systemFontOfSize:16];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.text = @"我的零钱";
//        _titleLabel.layer.borderColor = [UIColor blackColor].CGColor;
//        _titleLabel.layer.borderWidth = 1;
    }
    return _titleLabel;
}

- (UILabel *)amountLabel {
    if (!_amountLabel) {
        _amountLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_titleLabel.frame) + 24, SCREEN_WIDTH, 32)];
        _amountLabel.textColor = [UIColor blackColor];
        _amountLabel.font = [UIFont systemFontOfSize:32];
        _amountLabel.textAlignment = NSTextAlignmentCenter;
        _amountLabel.text = @"￥00.00";
//        _amountLabel.layer.borderColor = [UIColor blackColor].CGColor;
//        _amountLabel.layer.borderWidth = 1;
    }
    return _amountLabel;
}

- (UIButton *)chargeButton {
    if (!_chargeButton) {
        _chargeButton = [[UIButton alloc] initWithFrame:CGRectMake(12, CGRectGetMaxY(_amountLabel.frame) + 56, SCREEN_WIDTH - 24, 46)];
        [_chargeButton setTitle:@"充值" forState:UIControlStateNormal];
        [_chargeButton setBackgroundColor:[UIColor colorWithRed:41/255.f green:182/255.f blue:246/255.0 alpha:1]];
        [_chargeButton setBackgroundColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
        _chargeButton.layer.cornerRadius = 5;
        _chargeButton.layer.masksToBounds = YES;
        _chargeButton.titleLabel.font = [UIFont systemFontOfSize:18];
    }
    
    return _chargeButton;
}

- (UIButton *)withdrawCashButton {
    if (!_withdrawCashButton) {
        _withdrawCashButton = [[UIButton alloc] initWithFrame:CGRectMake(12, CGRectGetMaxY(_chargeButton.frame) + 32, SCREEN_WIDTH - 24, 46)];
        [_withdrawCashButton setTitle:@"提现" forState:UIControlStateNormal];
        [_withdrawCashButton setBackgroundColor:[UIColor whiteColor]];
        [_withdrawCashButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_withdrawCashButton setBackgroundColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
        _withdrawCashButton.layer.cornerRadius = 5;
        _withdrawCashButton.layer.masksToBounds = YES;
        _withdrawCashButton.titleLabel.font = [UIFont systemFontOfSize:18];
    }
    
    return _withdrawCashButton;
}

@end
