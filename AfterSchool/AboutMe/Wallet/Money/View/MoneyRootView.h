//
//  MoneyRootView.h
//  AfterSchool
//
//  Created by Teson Draw on 11/12/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavigationBarView.h"

@interface MoneyRootView : UIView

@property (nonatomic, strong) NavigationBarView *navigationBarView;
@property (nonatomic, strong) UIImageView *logoImageView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *amountLabel;

@property (nonatomic, strong) UIButton *chargeButton;
@property (nonatomic, strong) UIButton *withdrawCashButton;

@end
