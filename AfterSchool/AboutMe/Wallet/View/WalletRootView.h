//
//  WalletRootView.h
//  AfterSchool
//
//  Created by lg on 15/10/22.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavigationBarView.h"
#import "OptionView.h"

@interface WalletRootView : UIView

@property (nonatomic, strong) NavigationBarView * navigationBarView;
//@property (nonatomic, strong) UITableView * tableView;
@property (nonatomic, strong) UIScrollView *scrollView;

@property (nonatomic, strong) OptionView *moneyOptionView;
@property (nonatomic, strong) OptionView *gameGoldOptionView;

@end
