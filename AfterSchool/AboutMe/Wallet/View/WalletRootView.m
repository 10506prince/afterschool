//
//  WalletRootView.m
//  AfterSchool
//
//  Created by lg on 15/10/22.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "WalletRootView.h"
#import "MacrosDefinition.h"

@implementation WalletRootView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
//        [self addSubview:self.tableView];
        [self addSubview:self.scrollView];
        [_scrollView addSubview:self.moneyOptionView];
        [_scrollView addSubview:self.gameGoldOptionView];
        
        [self addSubview:self.navigationBarView];
    }
    return self;
}

- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
        _scrollView.contentSize = CGSizeMake(SCREEN_WIDTH, SCREEN_HEIGHT + 20);
        _scrollView.showsVerticalScrollIndicator = NO;
    }
    return _scrollView;
}

- (NavigationBarView *)navigationBarView {
    if (!_navigationBarView) {

        _navigationBarView = [[NavigationBarView alloc] initWithTitle:@"钱包"
                                                           titleColor:nil
                                                      backgroundColor:nil
                                              leftButtonBgViewOriginX:0
                                                 leftButtonImageWidth:22
                                                 leftButtonTitleColor:[UIColor whiteColor]
                                                       leftButtonName:@"我"
                                                  leftButtonImageName:@"navigation_bar_return_button_left_arrow" rightButtonName:nil
                                                 rightButtonImageName:nil];
    }
    return _navigationBarView;
}

- (OptionView *)moneyOptionView {
    if (!_moneyOptionView) {
        _moneyOptionView = [[OptionView alloc] initCellWithFrame:CGRectMake(0, 64 + 20, SCREEN_WIDTH, 50) title:@"零钱" titleTextColor:nil titleFontSize:16 backgroundColor:nil contentText:@"￥00.00" contentTextColor:[UIColor colorWithRed:120/255.f green:120/255.f blue:120/255.f alpha:1] contentFontSize:16 showArrow:YES showBottomLine:YES];
    }
    
    return _moneyOptionView;
}

- (OptionView *)gameGoldOptionView {
    if (!_gameGoldOptionView) {
        _gameGoldOptionView = [[OptionView alloc] initCellWithFrame:CGRectMake(0, CGRectGetMaxY(_moneyOptionView.frame), SCREEN_WIDTH, 50) title:@"游戏币" titleTextColor:nil titleFontSize:16 backgroundColor:nil contentText:@"0枚" contentTextColor:[UIColor colorWithRed:120/255.f green:120/255.f blue:120/255.f alpha:1] contentFontSize:16 showArrow:YES showBottomLine:NO];
    }
    
    return _gameGoldOptionView;
}

//- (UITableView *)tableView {
//    if (!_tableView) {
//        _tableView = [[UITableView alloc] initWithFrame:self.bounds style:UITableViewStyleGrouped];
//        
//        UIEdgeInsets ed = UIEdgeInsetsMake(64, 0, 0, 0);
//        [_tableView setScrollIndicatorInsets:ed];
//        [_tableView setContentInset:ed];
//        
//        [_tableView setDelaysContentTouches:NO];
//    }
//    return _tableView;
//}

@end
