//
//  CityViewController.m
//  AfterSchool
//
//  Created by lg on 15/10/22.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "CityViewController.h"
#import "Common.h"
#import "ZYPinYinSearch.h"
#import "PinYinForObjc.h"
#import "TDSingleton.h"
#import "LGDefineNetServer.h"
#import "NotificationName.h"

#import "Common.h"
#import "TalkingData.h"

@interface CityViewController () <UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate> {
    NSArray * _dataSource;
    
    UIImageView   *_bgImageView;
    UIView        *_tipsView;
    UILabel       *_tipsLab;
    NSTimer       *_timer;
}

@property (strong, nonatomic) NSMutableDictionary *searchResultDic;

@property (strong, nonatomic) NSMutableDictionary *cities;///<城市数据(从plist中读取到)
@property (strong, nonatomic) NSMutableArray *keys; ///<城市首字母
@property (strong, nonatomic) NSMutableArray *arrayCitys;   ///<城市数据

@property (nonatomic, copy) CityViewControllerFinishBlock finishBlock;

@end

@implementation CityViewController
- (void)dealloc
{
    NSLogSelfMethodName;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [TalkingData trackPageBegin:selfControllerName];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [TalkingData trackPageEnd:selfControllerName];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initData];
    [self initUserInterface];
}

- (void)initData {
//    self.arrayLocatingCity   = [NSMutableArray array];
    self.cities = [NSMutableDictionary dictionary];//从plist文件中读取后会替换掉
    self.keys = [NSMutableArray array];
    self.arrayCitys = [NSMutableArray array];
    
    [self getCityData];
}

- (void)initUserInterface {
    [self.view addSubview:self.rootView];
}

- (CityRootView *)rootView {
    if (!_rootView) {
        _rootView = [[CityRootView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        [_rootView.navigationBarView.leftButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        
        _rootView.tableView.dataSource = self;
        _rootView.tableView.delegate = self;
        
        _rootView.searchBar.delegate = self;
    }
    
    return _rootView;
}

- (void)backAction {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Block
- (void)addFinishBlock:(CityViewControllerFinishBlock)finishBlock {
    self.finishBlock = finishBlock;
}

#pragma mark - UITableViewDataSource 列表数据代理
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _keys.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    if (section == 0) {
//        return 1;
//    }

    NSString *key = [_keys objectAtIndex:section];
    NSArray *citySection = [_cities objectForKey:key];
    
    if ([key isEqualToString:@"#"]) {
        return 1;
    }
    return [citySection count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString * key = [_keys objectAtIndex:indexPath.section];

    if ([key isEqualToString:@"#"]) {
        CityListTableViewCellTwo * cell = [tableView dequeueReusableCellWithClass:[CityListTableViewCellTwo class]];
        
//        if ([TDSingleton instance].cityName.length == 0) {
//            cell.currentCityName = [TDSingleton instance].currentCityName;
//        } else {
//            cell.currentCityName = [TDSingleton instance].cityName;
//        }
        
        __weak typeof(self) weakSelf = self;
        
        cell.clickBlock = ^(id city) {
            [weakSelf setCityWithStr:city];
        };
        
        return cell;
    } else {
        CityListTableViewCellOne * cell = [tableView dequeueReusableCellWithClass:[CityListTableViewCellOne class]];
        
        NSArray * citys = [_cities objectForKey:key];
        cell.city = citys[indexPath.row];
        
        return cell;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    NSString * key = _keys[section];
    if ([key isEqualToString:@"#"]) {
        UIView * view = [UIView alloc];
        return view;
    } else {
        CityListTableHeaderView * header = [tableView dequeueReusableHeaderFooterViewWithClass:[CityListTableHeaderView class]];
        header.title = key;
        return header;
    }
}

- (NSArray<NSString *> *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    return [NSArray arrayWithArray:_keys];
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index {
    [self showTipsWithTitle:title];
    return index;
}
#pragma mark - UITableViewDelegate 列表代理
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSString * key = _keys[indexPath.section];
    if ([key isEqualToString:@"#"]) {
        return;
    } else {
        NSArray * citys = [_cities objectForKey:key];
        [self setCityWithStr:citys[indexPath.row]];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    NSString * key = _keys[section];
    if ([key isEqualToString:@"#"]) {
        return 0;
    } else {
        return CityListTableHeaderViewHeight;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString * key = _keys[indexPath.section];
    if ([key isEqualToString:@"#"]) {
        return CityListTableViewCellTwoHeight;
    } else {
        return CityListTableViewCellOneHeight;
    }
}

#pragma mark - 获取城市数据
-(void)getCityData
{
    NSString *path=[[NSBundle mainBundle] pathForResource:@"CityList"
                                                   ofType:@"plist"];
    self.cities = [NSMutableDictionary dictionaryWithContentsOfFile:path];
    
    [_keys addObjectsFromArray:[[self.cities allKeys] sortedArrayUsingSelector:@selector(compare:)]];
    
//    [_keys insertObject:[_keys lastObject] atIndex:0];
//    [_keys removeLastObject];
    
    //    //添加热门城市
    //    NSString *strHot = @"#";
    //    [self.keys insertObject:strHot atIndex:0];
    //    [self.cities setObject:_arrayHotCity forKey:strHot];
    
    NSArray *allValuesAry = [self.cities allValues];
    for (NSArray*oneAry in allValuesAry) {
        
        for (NSString *cityName in oneAry) {
            [_arrayCitys addObject:cityName];
        }
    }
    [self.rootView.tableView reloadData];
}

- (void)showTipsWithTitle:(NSString*)title
{
    
    //获取当前屏幕window
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    //添加黑色透明背景
    //    if (!_bgImageView) {
    //        _bgImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, window.frame.size.width, window.frame.size.height)];
    //        _bgImageView.backgroundColor = [UIColor blackColor];
    //        _bgImageView.alpha = 0.1;
    //        [window addSubview:_bgImageView];
    //    }
    if (!_tipsView) {
        //添加字母提示框
        _tipsView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 80, 80)];
        _tipsView.center = window.center;
        _tipsView.backgroundColor = [UIColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:0.8];
        //设置提示框圆角
        _tipsView.layer.masksToBounds = YES;
        _tipsView.layer.cornerRadius  = _tipsView.frame.size.width/20;
        _tipsView.layer.borderColor   = [UIColor whiteColor].CGColor;
        _tipsView.layer.borderWidth   = 2;
        [window addSubview:_tipsView];
    }
    if (!_tipsLab) {
        //添加提示字母lable
        _tipsLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, _tipsView.frame.size.width, _tipsView.frame.size.height)];
        //设置背景为透明
        _tipsLab.backgroundColor = [UIColor clearColor];
        _tipsLab.font = [UIFont boldSystemFontOfSize:50];
        _tipsLab.textAlignment = NSTextAlignmentCenter;
        
        [_tipsView addSubview:_tipsLab];
    }
    _tipsLab.text = title;//设置当前显示字母
    
    _timer = [NSTimer timerWithTimeInterval:1 target:self selector:@selector(hiddenTipsView) userInfo:nil repeats:NO];
    [[NSRunLoop mainRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
    
}

- (void)hiddenTipsView
{
    
    [UIView animateWithDuration:0.2 animations:^{
        _bgImageView.alpha = 0;
        _tipsView.alpha = 0;
    } completion:^(BOOL finished) {
        [_bgImageView removeFromSuperview];
        [_tipsView removeFromSuperview];
        _bgImageView = nil;
        _tipsLab     = nil;
        _tipsView    = nil;
    }];
}

#pragma mark - UISearchBarDelegate 搜索代理
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    [self filterContentForSearchText:searchText];
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    [searchBar setShowsCancelButton:NO animated:YES];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
    [searchBar setShowsCancelButton:NO animated:YES];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
    [searchBar setShowsCancelButton:NO animated:YES];
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    [searchBar setShowsCancelButton:YES animated:YES];
    return YES;
}

/**
 *  通过搜索条件过滤得到搜索结果
 *
 *  @param searchText 关键词
 *  @param scope      范围
 */
- (void)filterContentForSearchText:(NSString*)searchText {
    
    if (searchText.length > 0) {
        _searchResultDic = nil;
        _searchResultDic = [[NSMutableDictionary alloc]init];
        
        //搜索数组中是否含有关键字
        NSArray *resultAry  = [ZYPinYinSearch searchWithOriginalArray:_arrayCitys andSearchText:searchText andSearchByPropertyName:@""];
        //     NSLog(@"搜索结果:%@",resultAry) ;
        
        for (NSString*city in resultAry) {
            //获取字符串拼音首字母并转为大写
            NSString *pinYinHead = [PinYinForObjc chineseConvertToPinYinHead:city].uppercaseString;
            NSString *firstHeadPinYin = [pinYinHead substringToIndex:1]; //拿到字符串第一个字的首字母
            //        NSLog(@"pinYin = %@",firstHeadPinYin);
            
            
            NSMutableArray *cityAry = [NSMutableArray arrayWithArray:[_searchResultDic objectForKey:firstHeadPinYin]]; //取出首字母数组
            
            if (cityAry != nil) {
                
                [cityAry addObject:city];
                [cityAry sortedArrayUsingFunction:cityNameSort context:NULL];
                [_searchResultDic setObject:cityAry forKey:firstHeadPinYin];
                
            }else
            {
                cityAry= [[NSMutableArray alloc]init];
                [cityAry addObject:city];
                [_searchResultDic setObject:cityAry forKey:firstHeadPinYin];
            }
        }
        //    NSLog(@"dic = %@",dic);
        
//        if (resultAry.count>0) {
            _cities = nil;
            _cities = _searchResultDic;
            [_keys removeAllObjects];
            //按字母升序排列
            [_keys addObjectsFromArray:[[self.cities allKeys] sortedArrayUsingSelector:@selector(compare:)]] ;
//            _tableView.tableHeaderView = nil;
            [self.rootView.tableView reloadData];
//        }
    }else
    {
        //当字符串清空时 回到初始状态
        _cities = nil;
        [_keys removeAllObjects];
        [_arrayCitys removeAllObjects];
        [self getCityData];
//        _tableView.tableHeaderView = _tableHeaderView;
        [self.rootView.tableView reloadData];
    }
    
    
}

NSInteger cityNameSort(id str1, id str2, void *context)
{
    NSString *string1 = (NSString*)str1;
    NSString *string2 = (NSString*)str2;
    
    return  [string1 localizedCompare:string2];
}

- (void)setCityWithStr:(NSString *)cityStr {
    NSLog(@"设置城市为：%@", cityStr);

    CityViewControllerFinishBlock block = self.finishBlock;
    if (block) {
        block(cityStr);
    }
    
    [self backAction];
//    //更新系统单例中选择的城市名字,并且发送消息通知
////    [TDSingleton instance].cityName = cityStr;
//    [[NSNotificationCenter defaultCenter] postNotificationName:LGSetCityName object:cityStr];
//    
//    //刷新UI
//    NSIndexPath * indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
//    [self.rootView.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    
    NSLog(@"设置城市成功");
}
@end
