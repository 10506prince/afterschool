//
//  CityViewController.h
//  AfterSchool
//
//  Created by lg on 15/10/22.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGViewController.h"
#import "CityRootView.h"

typedef void(^CityViewControllerFinishBlock)(NSString * city);

@interface CityViewController : LGViewController

@property (nonatomic, strong) CityRootView * rootView;

- (void)addFinishBlock:(CityViewControllerFinishBlock)finishBlock;

@end
