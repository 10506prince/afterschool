//
//  CityListTableViewCellTwo.m
//  AfterSchool
//
//  Created by lg on 15/11/23.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "CityListTableViewCellTwo.h"
#import "Common.h"

#import "LGDefineNetServer.h"
#import "LGLocationService.h"
#import "LGGeoCodeService.h"
#import "UIButton+SetBackgroundColor.h"

@interface CityListTableViewCellTwo ()

@property (nonatomic, weak) IBOutlet UIButton * currentCityBtn;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView * activityView;

@property (nonatomic, weak) IBOutlet UIView * currentView;
@property (nonatomic, weak) IBOutlet UIView * hotCitySlot;

@property (nonatomic, weak) IBOutlet UILabel * currentCity;
@property (nonatomic, weak) IBOutlet UIButton * shangHai;
@property (nonatomic, weak) IBOutlet UIButton * beiJing;
@property (nonatomic, weak) IBOutlet UIButton * guangZhou;
@property (nonatomic, weak) IBOutlet UIButton * shenZhen;
@property (nonatomic, weak) IBOutlet UIButton * chengDu;
@property (nonatomic, weak) IBOutlet UIButton * chongQing;
@property (nonatomic, weak) IBOutlet UIButton * hangZhou;
@property (nonatomic, weak) IBOutlet UIButton * nanJing;
@property (nonatomic, weak) IBOutlet UIButton * suZhou;

@end

@implementation CityListTableViewCellTwo

- (void)awakeFromNib {
    [self setUp];
}

- (void)setUp {
    
    UIView * line = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1000, SINGLE_LINE_WIDTH)];
    [line setBackgroundColor:[UIColor lightGrayColor]];
    [self.hotCitySlot addSubview:line];
    
    NSArray * hotCityBtns = @[_shangHai,_beiJing,_guangZhou,_shenZhen,_chengDu,_chongQing,_hangZhou,_nanJing,_suZhou];
    for (UIButton * btn in hotCityBtns) {
        [btn.layer setBorderColor:LgColor(196, 1).CGColor];
        [btn.layer setCornerRadius:3.0];
        [btn.layer setBorderWidth:SINGLE_LINE_WIDTH];
        [btn.layer setMasksToBounds:YES];
        //[btn setTitleColor:LgColorRGB(41, 182, 246, 1) forState:UIControlStateNormal];
        //[btn setBackgroundColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
        [btn setBackgroundColor:LgColorRGB(41, 182, 246, 0.5) forState:UIControlStateHighlighted];
        
        [btn addTarget:self action:@selector(clickCityBtn:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    [_currentCityBtn addTarget:self action:@selector(clickCurrentCityBtn:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    
}

- (void)setCurrentCityName:(NSString *)currentCityName {
    [self.currentCity setText:currentCityName];
}

- (NSString *)currentCityName {
    return self.currentCity.text;
}

#pragma mark - 公共方法
#pragma mark 设置回调对象和回调方法
- (void)setClickTarget:(id)target clickAction:(SEL)action {
    self.clickTarget = target;
    self.clickAction = action;
}

- (IBAction)clickCityBtn:(id)sender {
    NSString * city = [(UIButton *)sender titleLabel].text;
    
    __weak CityListTableViewCellTwo * weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        CityListTableViewCellTwoClickBlock block = weakSelf.clickBlock;
        if (block) {
            block(city);
        } else if ([weakSelf.clickTarget respondsToSelector:weakSelf.clickAction]) {
            LGMsgSend(LGMsgTarget(weakSelf.clickTarget), weakSelf.clickAction, weakSelf);
        }
    });
}

- (IBAction)clickCurrentCityBtn:(id)sender {
    
    [self.activityView startAnimating];
    [self.currentCity setHidden:YES];
    
    __weak typeof(self) weakSelf = self;
    CityListTableViewCellTwoClickBlock block = self.clickBlock;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        CLLocationCoordinate2D pt = [LGLocationService sharedManager].userLocation.location.coordinate;
        
        [LGGeoCodeService reverseGeocodeWithPt:pt success:^(id result) {
            if ([result isKindOfClass:[LGReverseGeoCodeResult class]]) {
                LGReverseGeoCodeResult * location = (LGReverseGeoCodeResult *)result;
                NSString * cityName = location.addressComponent.city;
                [weakSelf.currentCity setText:cityName];
                [weakSelf.currentCity setHidden:NO];
                [weakSelf.activityView stopAnimating];
                block(cityName);
            } else {
                [weakSelf.currentCity setText:@"定位失败"];
                [weakSelf.currentCity setHidden:NO];
                [weakSelf.activityView stopAnimating];
            }
        } failure:^(NSString *msg) {
            [weakSelf.currentCity setText:@"定位失败"];
            [weakSelf.currentCity setHidden:NO];
            [weakSelf.activityView stopAnimating];
        }];
    });
}
@end
