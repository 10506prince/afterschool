//
//  CityListTableHeaderView.m
//  AfterSchool
//
//  Created by lg on 15/11/23.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "CityListTableHeaderView.h"
#import "Common.h"

@interface CityListTableHeaderView ()

@property (nonatomic, strong) UILabel * titleLabel;

@end

@implementation CityListTableHeaderView

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setUp];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUp];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self setUp];
    }
    return self;
}

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithReuseIdentifier:reuseIdentifier];
    [self setUp];
    return self;
}

- (void)setUp {
    [self.contentView setBackgroundColor:[UIColor clearColor]];
    
    _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(26, 0, 100, CityListTableHeaderViewHeight)];
    [_titleLabel setFont:[UIFont systemFontOfSize:16]];
    [_titleLabel setTextColor:LgColor(120, 1)];
    
    [self.contentView addSubview:_titleLabel];
}

- (void)setTitle:(NSString *)title {
    [self.titleLabel setText:title];
}
- (NSString *)title {
    return self.titleLabel.text;
}
@end
