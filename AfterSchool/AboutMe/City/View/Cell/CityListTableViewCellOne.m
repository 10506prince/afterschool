//
//  CityListTableViewCellOne.m
//  AfterSchool
//
//  Created by lg on 15/11/23.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "CityListTableViewCellOne.h"

@interface CityListTableViewCellOne ()

@property (nonatomic, weak) IBOutlet UILabel * cityLabel;

@end

@implementation CityListTableViewCellOne

- (void)awakeFromNib {
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)setCity:(NSString *)city {
    [self.cityLabel setText:city];
}

- (NSString *)city {
    return self.cityLabel.text;
}
@end
