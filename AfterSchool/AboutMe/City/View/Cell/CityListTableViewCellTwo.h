//
//  CityListTableViewCellTwo.h
//  AfterSchool
//
//  Created by lg on 15/11/23.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>

#define CityListTableViewCellTwoHeight (50.0 + 22.0 + 176.0)

/** 进入刷新状态的回调 */
typedef void (^CityListTableViewCellTwoClickBlock)(NSString * city);

@interface CityListTableViewCellTwo : UITableViewCell

@property (nonatomic, copy) NSString * currentCityName;

/** 正在刷新的回调 */
@property (copy) CityListTableViewCellTwoClickBlock clickBlock;
/** 设置回调对象和回调方法 */
- (void)setClickTarget:(id)target clickAction:(SEL)action;
/** 回调对象 */
@property (weak, nonatomic) id clickTarget;
/** 回调方法 */
@property (assign, nonatomic) SEL clickAction;


@end



