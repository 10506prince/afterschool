//
//  CityListTableViewCellOne.h
//  AfterSchool
//
//  Created by lg on 15/11/23.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>

#define CityListTableViewCellOneHeight 50.0f

@interface CityListTableViewCellOne : UITableViewCell

@property (nonatomic, copy) NSString * city;

@end
