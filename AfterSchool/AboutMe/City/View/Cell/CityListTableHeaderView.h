//
//  CityListTableHeaderView.h
//  AfterSchool
//
//  Created by lg on 15/11/23.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>

#define CityListTableHeaderViewHeight 22.0f

@interface CityListTableHeaderView : UITableViewHeaderFooterView

@property (nonatomic, copy) NSString * title;

@end
