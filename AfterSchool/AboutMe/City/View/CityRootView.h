//
//  CityRootView.h
//  AfterSchool
//
//  Created by lg on 15/10/22.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavigationBarView.h"
#import "LGTableView.h"
#import "CityListTableViewCellOne.h"
#import "CityListTableViewCellTwo.h"
#import "CityListTableHeaderView.h"

@interface CityRootView : UIView

@property (nonatomic, strong) NavigationBarView * navigationBarView;
@property (nonatomic, strong) LGTableView * tableView;
@property (nonatomic, strong) UISearchBar * searchBar;

@end
