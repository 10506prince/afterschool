//
//  CityRootView.m
//  AfterSchool
//
//  Created by lg on 15/10/22.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "CityRootView.h"
#import "Common.h"
#import "UIImage+ImageWithColor.h"

@implementation CityRootView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self addSubview:self.tableView];
        
        [self addSubview:self.navigationBarView];
    }
    return self;
}

- (NavigationBarView *)navigationBarView {
    if (!_navigationBarView) {
    
        _navigationBarView = [[NavigationBarView alloc] initWithTitle:@"城市"
                                                           titleColor:nil
                                                      backgroundColor:nil
                                              leftButtonBgViewOriginX:0
                                                 leftButtonImageWidth:22
                                                 leftButtonTitleColor:[UIColor whiteColor]
                                                       leftButtonName:@"返回"
                                                  leftButtonImageName:@"navigation_bar_return_button_left_arrow"
                                                      rightButtonName:nil
                                                 rightButtonImageName:nil];
    }
    return _navigationBarView;
}

- (LGTableView *)tableView {
    if (!_tableView) {
        _tableView = [[LGTableView alloc] initWithFrame:self.bounds style:UITableViewStylePlain];
        
        [_tableView registerNibCellWithClass:[CityListTableViewCellOne class]];
        [_tableView registerNibCellWithClass:[CityListTableViewCellTwo class]];
        [_tableView registerClass:[CityListTableHeaderView class] forHeaderFooterViewReuseIdentifier:NSStringFromClass([CityListTableHeaderView class])];
        
        _tableView.tableHeaderView = self.searchBar;
        _tableView.sectionIndexColor = LgColor(120, 1);
    }
    return _tableView;
}

- (UISearchBar *)searchBar {
    if (!_searchBar) {
        _searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, 44)];
        [_searchBar setPlaceholder:@"请输入城市名称或首字母查询"];
        
        [_searchBar setBackgroundImage:nil];
        //[_searchBar setBarTintColor:[UIColor getColor:@"ebebeb"]];
        //[_searchBar setBackgroundColor:[UIColor getColor:@"ebebeb"]];
        
        [_searchBar setBackgroundImage:[UIImage imageWithColor:[UIColor groupTableViewBackgroundColor]]];
    }
    return _searchBar;
}
@end
