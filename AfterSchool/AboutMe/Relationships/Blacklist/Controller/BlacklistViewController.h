//
//  BlacklistViewController.h
//  AfterSchool
//
//  Created by Teson Draw on 11/24/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BlacklistRootView.h"

@interface BlacklistViewController : UIViewController

@property (nonatomic, strong) BlacklistRootView *rootView;

@property (nonatomic, strong) NSArray *sectionsArray;

@property (nonatomic, strong) NSMutableDictionary *dataSource;
@property (nonatomic, strong) NSMutableDictionary *backupDataSource;

@end
