//
//  ContactsListView.m
//  AfterSchool
//
//  Created by Teson Draw on 11/24/15.
//  Copyright © 2015 Teson Draw. All rights reserved.
//

#import "ContactsListRootView.h"
#import "UIImage+ImageWithColor.h"

@implementation ContactsListRootView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubview:self.tableView];
    }
    return self;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height)];
        _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        _tableView.tableHeaderView = self.searchBar;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _tableView;
}

- (UISearchBar *)searchBar {
    if (!_searchBar) {
        _searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, 44)];
        _searchBar.placeholder = @"搜索";
        _searchBar.keyboardType = UIKeyboardTypeDefault;
        _searchBar.autocapitalizationType = UITextAutocapitalizationTypeNone;
        _searchBar.tintColor = [UIColor blueColor];
        _searchBar.barTintColor = [UIColor colorWithRed:244/255.f green:244/255.f blue:244/255.f alpha:1];
        
        UIImage *image = [UIImage imageWithColor:[UIColor colorWithRed:244/255.f green:244/255.f blue:244/255.f alpha:1]];
        
        _searchBar.backgroundImage = image;
    }
    return _searchBar;
}

@end
