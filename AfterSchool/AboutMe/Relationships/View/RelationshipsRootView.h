//
//  RelationshipsRootView.h
//  AfterSchool
//
//  Created by lg on 15/10/22.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavigationBarView.h"
#import "TDSwitchButtonView.h"
//#import "ContactsListView.h"
//#import "BlacklistView.h"

@interface RelationshipsRootView : UIView

@property (nonatomic, strong) NavigationBarView  * navigationBarView;
@property (nonatomic, strong) TDSwitchButtonView *switchButtonView;

//@property (nonatomic, strong) UITableView        * tableView;

//@property (nonatomic, strong) ContactsListView   *contactsListView;
//@property (nonatomic, strong) BlacklistView      *blacklistView;

@end
