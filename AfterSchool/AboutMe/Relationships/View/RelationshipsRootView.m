//
//  RelationshipsRootView.m
//  AfterSchool
//
//  Created by lg on 15/10/22.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "RelationshipsRootView.h"
#import "MacrosDefinition.h"

@implementation RelationshipsRootView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubview:self.navigationBarView];
        [_navigationBarView addSubview:self.switchButtonView];
    }
    return self;
}

- (NavigationBarView *)navigationBarView {
    if (!_navigationBarView) {
    
        _navigationBarView = [[NavigationBarView alloc] initWithTitle:nil
                                                           titleColor:nil
                                                      backgroundColor:nil
                                              leftButtonBgViewOriginX:0
                                                 leftButtonImageWidth:22
                                                 leftButtonTitleColor:[UIColor whiteColor]
                                                       leftButtonName:@"我"
                                                  leftButtonImageName:@"navigation_bar_return_button_left_arrow" rightButtonName:nil
                                                 rightButtonImageName:nil];
    }
    return _navigationBarView;
}

- (TDSwitchButtonView *)switchButtonView {
    if (!_switchButtonView) {

        _switchButtonView = [[TDSwitchButtonView alloc] initWithFrame:CGRectMake((_navigationBarView.bounds.size.width - 132) / 2, 20, 132, 44)
                                                  leftButtonImageName:@"relationship_contacts"
                                                 rightButtonImageName:@"relationship_blacklist"];
    }
    return _switchButtonView;
}

@end
