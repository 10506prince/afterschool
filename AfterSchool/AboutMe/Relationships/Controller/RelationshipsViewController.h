//
//  RelationshipsViewController.h
//  AfterSchool
//
//  Created by lg on 15/10/22.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "LGViewController.h"
#import "RelationshipsRootView.h"
#import "BlacklistViewController.h"
#import "ContactsListViewController.h"

@interface RelationshipsViewController : LGViewController

@property (nonatomic, strong) RelationshipsRootView * rootView;

@property (nonatomic, strong) BlacklistViewController *blackListVC;
@property (nonatomic, strong) ContactsListViewController *contactsListVC;

@end
