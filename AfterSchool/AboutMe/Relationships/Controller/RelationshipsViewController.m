//
//  RelationshipsViewController.m
//  AfterSchool
//
//  Created by lg on 15/10/22.
//  Copyright © 2015年 Teson Draw. All rights reserved.
//

#import "RelationshipsViewController.h"
#import "Common.h"
#import "TDSwitchButtonView.h"


@interface RelationshipsViewController () <TDSwitchButtonViewDelegate> {
    NSArray * _dataSource;
}

@end

@implementation RelationshipsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initData];
    [self initUserInterface];
}

- (void)initData {
    
}

- (void)initUserInterface {
    [self.view addSubview:self.rootView];
    
    _blackListVC = [[BlacklistViewController alloc] init];
    _contactsListVC = [[ContactsListViewController alloc] init];

    [self addChildViewController:_blackListVC];
    [self addChildViewController:_contactsListVC];
    
    [self.view addSubview:_blackListVC.view];
    [self.view addSubview:_contactsListVC.view];
}

- (RelationshipsRootView *)rootView {
    if (!_rootView) {
        _rootView = [[RelationshipsRootView alloc] initWithFrame:self.view.bounds];
        [_rootView.navigationBarView.leftButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        
        _rootView.switchButtonView.delegate = self;
    }
    
    return _rootView;
}

- (void)backAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)switchButtonViewButtonClicked:(UIButton *)sender {
    switch (sender.tag) {
        case 100:
            [self.view insertSubview:_contactsListVC.view aboveSubview:_blackListVC.view];
            break;

        case 101:
            [self.view insertSubview:_blackListVC.view aboveSubview:_contactsListVC.view];
            break;
    }
}

//UITableViewDataSource, UITableViewDelegate,

//#pragma mark - UITableViewDataSource 列表数据代理
//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//    return _dataSource.count;
//}
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    return 0;
//}
//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//    return nil;
//}
//#pragma mark - UITableViewDelegate 列表代理
//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    
//}
//- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    return 44.0f;
//}
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    return 44.0f;
//}
@end
